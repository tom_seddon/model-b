#ifndef HOSTINPSHARED_H_
#define HOSTINPSHARED_H_

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////////////////////////////
//
struct HostInpState;
typedef struct HostInpState HostInpState;

enum {
	HostInp_bad_key=-1,
	HostInp_num_joystick_buttons=32,
};


struct HostInp_JoyState {
	float x,y,z;
	float rx,ry,rz;
	float sliders[2];
	unsigned char buttons[HostInp_num_joystick_buttons];
};
typedef struct HostInp_JoyState HostInp_JoyState;

//Stop input system.
void HostInp_Stop(HostInpState *state);

//Poll devices.
void HostInp_Poll(HostInpState *state);

//Get array of keyboard keydown flags.
void HostInp_GetKeyboardState(HostInpState *state,unsigned char *keyflags);

//Get array of flags indicating keys newly pressed last Poll.
void HostInp_GetKeyboardDownFlags(HostInpState *state,unsigned char *downflags);

//Get name of key from its code, or 0 if no such key.
const char *HostInp_KeyNameFromCode(unsigned key_code);

//Get code for key given its name, or HostInp_bad_key if no such key.
int HostInp_CodeFromKeyName(const char *key_name);

//Whether joystick support is available.
int HostInp_SupportsJoysticks(HostInpState *state);

//Get number of joysticks
int HostInp_NumJoysticks(HostInpState *state);

//Get state for joy_indexth joystick
int HostInp_GetJoystickState(HostInpState *state,int joy_index,HostInp_JoyState *joy_state);

//Get name for the given joystick. Joystick names contain only alphanumeric,
//space, and underscore characters.
const char *HostInp_JoystickName(HostInpState *state,int joy_index);

#ifdef __cplusplus
}
#endif

#endif
