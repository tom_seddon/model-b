#ifndef HOSTTMRSHARED_H_
#define HOSTTMRSHARED_H_

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////////////////////////////
// Timer

//Starts the timer services.
void HostTmr_Start();

//Stops the timer services.
void HostTmr_Stop();

//Standard millisecond timer -- sets *value to the number of milliseconds
//elapsed since whenever.
void HostTmr_ReadMilliseconds(int *value);

#ifdef __cplusplus
}
#endif

#endif
