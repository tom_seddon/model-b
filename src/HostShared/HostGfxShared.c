#include "../port.h"
#include "HostGfxShared.h"
#include <stdlib.h>
#include <string.h>
#ifdef _CRTDBG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#endif
#include <assert.h>//TODO HOST_ASSERT!

void HostGfxBufferFormat_SetPixelElement(HostGfxBufferFormat *self,int elem_idx,unsigned mask) {
	HostGfxBufferFormatPixelElement *elem;

	elem=&self->elems[elem_idx];

	elem->mask=mask;
	elem->shift=0;
	elem->nbits=0;
	if(mask!=0) {
		while(!(mask&(1<<elem->shift))) {
			++elem->shift;
		}

		while(mask&(1<<(elem->shift+elem->nbits))) {
			++elem->nbits;
		}
	}
}

void HostGfxBufferFormat_Reset(HostGfxBufferFormat *self) {
	self->fixed_bits=0;
	HostGfxBufferFormat_SetPixelElement(self,0,1);
	HostGfxBufferFormat_SetPixelElement(self,1,2);
	HostGfxBufferFormat_SetPixelElement(self,2,4);
}

int HostGfxBufferFormat_Equal(const HostGfxBufferFormat *a,const HostGfxBufferFormat *b) {
	int i;

	if(a->fixed_bits!=b->fixed_bits) {
		return 0;
	}

	for(i=0;i<3;++i) {
		if(a->elems[i].mask!=b->elems[i].mask) {
			return 0;
		}
		assert(a->elems[i].shift==b->elems[i].shift);//TODO HOST_ASSERT!
		assert(a->elems[i].nbits==b->elems[i].nbits);//TODO HOST_ASSERT!
	}

	return 1;
}

void HostGfxBuffer_Init(HostGfxBuffer *self) {
	self->w=0;
	self->h=0;
	self->pitch=0;
	self->bitmap=0;
	HostGfxBufferFormat_Reset(&self->fmt);
}

void HostGfxBuffer_Shutdown(HostGfxBuffer *self) {
	free(self->bitmap);

	self->w=0;
	self->h=0;
	self->pitch=0;
	self->bitmap=0;
	HostGfxBufferFormat_Reset(&self->fmt);
}

void HostGfxBuffer_Reset(HostGfxBuffer *self) {
	HostGfxBuffer_Shutdown(self);
	HostGfxBuffer_Init(self);
}

void HostGfxBuffer_SetDetails(HostGfxBuffer *self,int w,int h,const HostGfxBufferFormat *fmt) {
	HostGfxBuffer_Reset(self);

	self->w=w;
	self->h=h;
	self->fmt=*fmt;

	self->pitch=self->w*self->fmt.bypp;;
	self->pitch+=3;
	self->pitch&=~3;

	self->bitmap=malloc(self->pitch*self->h);

	memset(self->bitmap,fmt->fixed_bits,self->pitch*self->h);
}

static unsigned char GetByteValue(unsigned value,const HostGfxBufferFormatPixelElement *elem) {
	unsigned char r;
	unsigned i;

	r=0;
	for(i=0;i<8;++i) {
		r|=((value>>(elem->shift+i%elem->nbits))&1)<<(7-i);
	}

	return r;
}

void HostGfxBuffer_GetRgb(const HostGfxBuffer *self,int x,int y,unsigned char *r,unsigned char *g,unsigned char *b) {
	unsigned value;
	
	value=*(unsigned *)(self->bitmap+y*self->pitch+x*self->fmt.bypp);
	*r=GetByteValue(value,&self->fmt.elems[0]);
	*g=GetByteValue(value,&self->fmt.elems[1]);
	*b=GetByteValue(value,&self->fmt.elems[2]);
}

void HostGfxBuffer_Clear(HostGfxBuffer *self) {
// 	unsigned char clrbyte;
// 
// 	clrbyte=self->fmt.fixed_bits;
// 	memset(self->bitmap,clrbyte,self->h*self->pitch);

	unsigned i,*dest,n;

	dest=(unsigned *)self->bitmap;
	n=(self->h*self->pitch)/4;

	for(i=0;i<n;++i)
		dest[i]=self->fmt.fixed_bits;
}
