#ifndef HOSTSNDSHARED_H_
#define HOSTSNDSHARED_H_

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////////////////////////////
// Sound
struct HostSndState;
typedef struct HostSndState HostSndState;

//Destroy the given HostSndState.
void HostSnd_Stop(HostSndState *state);

//Set HostSndState parameters -- freq, nbits, num channels and length of buffer.
//If these are the same as the existing ones, it should reset play position and
//clear buffer.
void HostSnd_SetSoundParameters(HostSndState *state,int hz,int bits,int channels,
								int len_samples);

//Start and stop playing -- like pause and resume, PlayStart doesn't reset play
//position or anything.
void HostSnd_PlayStart(HostSndState *state);
void HostSnd_PlayStop(HostSndState *state);

//Get the area that needs writing data to.
//<0 indicates error. (Flush pending sound and try again later.)
//0 indicates no bytes to be written. (Try again later.)
//>0 indicates bytes to be written. (p[12] and len[12] filled in relevantly.)
int HostSnd_GetWriteArea(HostSndState *state,void **p1,unsigned *len1,void **p2,unsigned *len2);

//After writing data to the write area, call this function to commit it.
void HostSnd_CommitWriteArea(HostSndState *state);

//Set volume of sound.
//0<=volume<=100, 0 being quiet and 100 being loud.
void HostSnd_SetVolume(HostSndState *state,int volume);
/*
int HostSnd_SampleLoad(HostSndState *state,const char *filename);
void HostSnd_SamplePlay(HostSndState *state,int sample,int looping,float mult=1.f);
void HostSnd_SampleStop(HostSndState *state,int sample);
void HostSnd_SampleUnload(HostSndState *state,int sample);
*/

#ifdef __cplusplus
}
#endif

#endif
