#ifndef HOSTGFXSHARED_H_
#define HOSTGFXSHARED_H_

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//
// All these are defined in HostGfxShared.c
//

//////////////////////////////////////////////////////////////////////////
// HostGfx 3bpp pixel format.

struct HostGfxBufferFormatPixelElement {
	unsigned shift,nbits,mask;
};
typedef struct HostGfxBufferFormatPixelElement HostGfxBufferFormatPixelElement;

struct HostGfxBufferFormat {
	unsigned fixed_bits;
	int bypp;//bytes per pixel
	HostGfxBufferFormatPixelElement elems[3];
};
typedef struct HostGfxBufferFormat HostGfxBufferFormat;

struct HostGfxBuffer {
	unsigned char *bitmap;
	int w,h;
	unsigned pitch;
	HostGfxBufferFormat fmt;
};
typedef struct HostGfxBuffer HostGfxBuffer;

void HostGfxBufferFormat_Reset(HostGfxBufferFormat *self);
int HostGfxBufferFormat_Equal(const HostGfxBufferFormat *a,const HostGfxBufferFormat *b);
void HostGfxBufferFormat_SetPixelElement(HostGfxBufferFormat *self,int elem_idx,unsigned mask);

void HostGfxBuffer_Init(HostGfxBuffer *self);
void HostGfxBuffer_Shutdown(HostGfxBuffer *self);
void HostGfxBuffer_Reset(HostGfxBuffer *self);
void HostGfxBuffer_SetDetails(HostGfxBuffer *self,int w,int h,const HostGfxBufferFormat *fmt);
void HostGfxBuffer_GetRgb(const HostGfxBuffer *self,int x,int y,unsigned char *r,unsigned char *g,unsigned char *b);
void HostGfxBuffer_Clear(HostGfxBuffer *self);

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//
// All these are defined in the platform-specific HostGfx.c.
//

//////////////////////////////////////////////////////////////////////////
// HostGfx state object.
struct HostGfxState;
typedef struct HostGfxState HostGfxState;

//////////////////////////////////////////////////////////////////////////
//Destroy the given HostGfxState.
void HostGfx_Stop(HostGfxState *state);

//Set new display contents. Bitmap is 3 bit FFFFFBGR format. FFFFF are
//the fixed bits that you know are suitable for the current display.
//
//Pops up on screen ASAP.
//Should clip against anything on top of the window, if required.
//
//y_first is Y coordinate of first scanline.
//line_height is height of each source line. data duplicated on each.
//gap_height is height of gap (if any) between source line.
//present_hints are hints about what you want.
//
//y_first must be <= gap_height. it can move lines only into the gaps.
//
//effect is index into special effects array.
enum HostGfx_PresentHints {
	//If set, the emulator wants a tidy display. If not present,
	//optimize for speed.
	HostGfx_PRESENT_HINT_TIDY=1,

	//If set, emulator wants present to sync to vsync, if this is possible.
	HostGfx_PRESENT_HINT_SYNC=2,
};
void HostGfx_Present(HostGfxState *state,const HostGfxBuffer *buffer,int y_first,int line_height,
	int gap_height,unsigned present_hints,uint64_t *convert_time,uint64_t *present_time);

//Sets the palette. Each palette entry is a float triplet. 1.f is full on that colour,
//0.f is none, or anything inbetween. Of course values may end up quantized, but that's
//not your problem.
void HostGfx_SetPalette(HostGfxState *state,const float new_palette[8][3]);//RGB

//Request full screen mode. This may or may not succeed, possibly immediately or maybe
//some time in the future. Check HostGfx_IsFullScreen to see what's going on.
void HostGfx_SetFullScreen(HostGfxState *state,int full_screen,int full_screen_refresh_rate);

//Get list of refresh rates. This always ends with 0, indicating a default rate.
//const int *HostGfx_FullScreenRefreshRates(HostGfxState *state);

//Whether full screen or not.
int HostGfx_IsFullScreen(HostGfxState *state);

//Clear the screen.
void HostGfx_Cls(HostGfxState *state);

const HostGfxBufferFormat *HostGfx_BestBufferFormat(HostGfxState *state);

//////////////////////////////////////////////////////////////////////////
// Device enumeration
//
// devices. 

enum {
	HostGfx_ADAPTER_NAME_SIZE=100,//pretty arbitrary.
};

struct HostGfx_Adapter {
	int can_windowed,can_full_screen;
	int num_refresh_rates;
	int *refresh_rates;
	char device_name[HostGfx_ADAPTER_NAME_SIZE];
	int default_device;
};
typedef struct HostGfx_Adapter HostGfx_Adapter;

//returned pointer is constant for a given HostGfxState, until state is restarted or stopped etc.
const HostGfx_Adapter *const *HostGfx_GetAdapters(HostGfxState *state);
int HostGfx_NumAdapters(HostGfxState *state);
void HostGfx_SetAdapter(HostGfxState *state,int adapter_index);

#ifdef __cplusplus
}
#endif

#endif

