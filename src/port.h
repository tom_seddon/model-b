#ifndef PORT_H_
#define PORT_H_

//////////////////////////////////////////////////////////////////////////

// Slightly mucky portability stuff.
// Everything includes this.
//
// Some of the options are interdependent, or handled strangely.

// Also:
// PORT_TEMPLATE -- "template" or "", depending on whether the compiler needs the keyword
// "template" before (I think) template classes that depend on another template.
// 
// PORT_USE_WX_KEYBOARD -- define to use wxWidgets key events rather than HostInp. Seems
// to be the only way under X.
//
// PORT_X -- defined if this is the Unix/X port.

//////////////////////////////////////////////////////////////////////////

// This is really annoying to set up in the IDE.

#ifdef _MSC_VER
#define PORT_INT64_TYPE __int64
#define PORT_UINT64_TYPE unsigned __int64

#define PORT_INT64_FMT "%I64"

#define PORT_TEMPLATE template

// I'm such a swine!
#define _CRT_SECURE_NO_DEPRECATE
#define _SCL_SECURE_NO_DEPRECATE
#define _CRT_NONSTDC_NO_DEPRECATE

#endif

//////////////////////////////////////////////////////////////////////////

#ifdef PORT_DEFINE__SNPRINTF
// VC++ uses _snprintf, which is conformant for the ANSI version it supports,
// but not for the latest standard.
//
// model-b code uses _snprintf.
//
// Define for Unix.
#define _snprintf snprintf
#define _vsnprintf vsnprintf
#endif

#ifdef PORT_STRICMP
#define stricmp PORT_STRICMP
#endif

#ifdef PORT__STRTOI64
#define _strtoi64 PORT__STRTOI64
#endif

#ifdef PORT_HAS_STDINT_H

#include <stdint.h>

#else//PORT_HAS_STDINT_H

#ifdef PORT_INT64_TYPE
typedef PORT_INT64_TYPE int64_t;
#endif

#ifdef PORT_UINT64_TYPE
typedef PORT_UINT64_TYPE uint64_t;
#endif

#endif//PORT_HAS_STDINT_H

#ifdef _MSC_VER

#define SoftBreak() __debugbreak()

#endif

#ifndef _MSC_VER

#ifdef PORT_X86

#define SoftBreak() asm("int $3")

#else//PORT_X86

#define SoftBreak() assert(0&&"model-b SoftBreak")

#endif//PORT_X86

#endif//_MSC_VER

#endif

