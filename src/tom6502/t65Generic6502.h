#ifndef GENERIC6502_H_
#define GENERIC6502_H_

#include "t65.h"
#include "t65WarningsPush.h"
//#include <typeinfo>

//Define to use table of NZ settings rather than calculating by hand.
//(Probably bad because it exercises the data cache.)
#define t65_USE_NZTABLE

//Set V flag like BeebEm in ADC/SBC
//#define t65_BEEBEM_VFLAG

//Do BCD like BeebEm
//#define t65_BEEBEM_BCD

//Define to use table of function pointers rather than switch() case for each opcode.
#define t65_USE_FUNCTION_TABLE

#ifdef t65_USE_FUNCTION_TABLE
#include "t65OpcodeTable.h"
#endif

namespace t65 {
	template<class ConfigType>
	struct Generic6502 {
	public://Was previously protected, seems vc.net doesn't like that though? no idea why, am sure it should work that way.
		// These are used as the last parameter of the WriteByte/ReadByte
		// functions.
		// TODO	make these typedefs, change WriteByte/ReadByte to take unnamed
		//		defaulted const &, sod visual C++ (not a moment too soon).
		static const t65TYPENAME ConfigType::ReadZP *catReadZP;
		static const t65TYPENAME ConfigType::WriteZP *catWriteZP;
		static const t65TYPENAME ConfigType::ReadStack *catReadStack;
		static const t65TYPENAME ConfigType::WriteStack *catWriteStack;
		static const t65TYPENAME ConfigType::FetchInstr *catFetchInstr;
		static const t65TYPENAME ConfigType::FetchAddress *catFetchAddress;
		static const t65TYPENAME ConfigType::ReadOperand *catReadOperand;
		static const t65TYPENAME ConfigType::WriteOperand *catWriteOperand;
		static const t65TYPENAME ConfigType::ReadDebug *catReadDebug;
		static const t65TYPENAME ConfigType::WriteDebug *catWriteDebug;
	protected:
		//////////////////////////////////////////////////////////////////////////
		// weird null execute class
		//
		// TODO work out what this is for :-/
		template<class T>
		struct NullExecute {
			static FINLINE void t65_CALL Execute(T &) {
			}

			static FINLINE void t65_CALL Execute(const T &) {
			}
		};

		//////////////////////////////////////////////////////////////////////////
		// oh my poor fingers.
		typedef ConfigType C;
	public:
		//////////////////////////////////////////////////////////////////////////
		// Disassembly helpers
		static char *AddHexByteChars(char *buf,byte val) {
			static const char hex_digits[]="0123456789ABCDEF";

			*buf++=hex_digits[val>>4];
			*buf++=hex_digits[val&0xf];
			*buf=0;
			return buf;
		}

		static char *AddHexByte(char *buf,byte val) {
			*buf++='&';
			return AddHexByteChars(buf,val);
		}

		static char *AddHexWord(char *buf,Word val) {
			*buf++='&';
			buf=AddHexByteChars(buf,val.h);
			buf=AddHexByteChars(buf,val.l);
			return buf;
		}

		static char *AddIndex(char *buf,char reg) {
			*buf++=',';
			*buf++=reg;
			*buf=0;
			return buf;
		}

		//////////////////////////////////////////////////////////////////////////
		// useful(ish) user accesible typedef
		typedef ConfigType Config;
		typedef t65TYPENAME ConfigType::MachineType MachineType;
		
		//////////////////////////////////////////////////////////////////////////
		// pointer to an execute function for an instruction
		typedef void (t65_CALL *InstrExecuteFn)(MachineType &state);
		
		//////////////////////////////////////////////////////////////////////////
		//
		static byte DebugRead(MachineType &state,word ptr) {
			Word tmp;
			tmp.w=ptr;
			return state.ReadByte(tmp,catReadDebug);
		}

		static void DebugWrite(MachineType &state,word ptr,byte val) {
			Word tmp;
			tmp.w=ptr;
			state.WriteByte(tmp,val,catWriteDebug);
		}

		//////////////////////////////////////////////////////////////////////////
		// Does the common stages of an interrupt -- the last 5 cycles
		//
		// suitable for brk, nmi, irq, reset.
		static FINLINE void t65_CALL Interrupt(MachineType &state,Word ptr) {
			Push(state,state.cpustate.pc.h);
			++state.cpucycles;
			Push(state,state.cpustate.pc.l);
			++state.cpucycles;
			Push(state,state.cpustate.p);
			++state.cpucycles;
			state.cpustate.pc.l=state.ReadByte(ptr,catFetchAddress);
			++state.cpucycles;
			++ptr.w;
			state.cpustate.pc.h=state.ReadByte(ptr,catFetchAddress);
			++state.cpucycles;
		}

		//////////////////////////////////////////////////////////////////////////
		// nztable[i] has the Z bit set if i is zero, and the N bit set if i
		// is negative.
		//
#ifdef t65_USE_NZTABLE
		static byte nztable[256];
#endif

		//////////////////////////////////////////////////////////////////////////
		// The vectors
		static const Word nmivector;
		static const Word resetvector;
		static const Word irqvector;
		
		//////////////////////////////////////////////////////////////////////////
		// IRQ
		static FINLINE unsigned IRQ(MachineType &state) {
			//if(!(state.cpustate.p&I_MASK)) {//removed 9/3/04, see comment in bbcModelGeneric.h and bbcModel.h
			state.cpucycles+=2;//I don't know what happens in these 2 cycles
			state.cpustate.p&=~B_MASK;//clear B bit (it's an IRQ)
			Interrupt(state,irqvector);
//			state.cpustate.p|=B_MASK;//reinstate B bit (it's always set)
			//I'm pretty sure this is required, otherwise the 6502 will keep getting interrupted.
			state.cpustate.p|=I_MASK;
			return 7;
			//}
			//return 0;
		}

		//////////////////////////////////////////////////////////////////////////
		// NMI
		static FINLINE void t65_CALL NMI(MachineType &state) {
			//state.cpustate.pc.w+=2;//I don't know what happens in these 2 cycles
			state.cpucycles+=2;//I think this is what I meant :-) [1/3/2003]
			Interrupt(state,nmivector);

			//64doc says this isn't the case, but VICE does it -- Opus DDOS at least
			//doesn't disable interrupts in its NMI handler, and I don't see how that
			//can be safe unless NMI sets I, particularly as interrupt timings are
			//somewhat quantized under emulation
			state.cpustate.p|=I_MASK;
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Pushes a value on to the stack
		static FINLINE void t65_CALL Push(MachineType &state,byte val) {
			state.WriteByte(state.cpustate.s,val,catWriteStack);
			--state.cpustate.s.l;
#ifdef t65_DEBUGGER
			state.ice.OnChangeS();
#endif//t65_DEBUGGER
		}

		//////////////////////////////////////////////////////////////////////////
		// Pops the value from the stack
		static FINLINE byte Pop(MachineType &state) {
			++state.cpustate.s.l;
#ifdef t65_DEBUGGER
			state.ice.OnChangeS();
#endif//t65_DEBUGGER
			return state.ReadByte(state.cpustate.s,catReadStack);
		}	

		//////////////////////////////////////////////////////////////////////////
		// ORs in the N+Z settings for the given value. Doesn't clear old values.
		// This way can call this from other code and change from nztable to
		// something else as desired.
		static FINLINE void t65_CALL DoOrNZFlags(MachineType &state,byte val) {
#ifdef t65_USE_NZTABLE
			state.cpustate.p|=nztable[val];
#else
			state.cpustate.p|=(val==0)<<1;//note true/false thingy
			state.cpustate.p|=val&0x80;
#endif
#ifdef t65_DEBUGGER
			state.ice.OnChangeP();
#endif//t65_DEBUGGER
		}
		
		//////////////////////////////////////////////////////////////////////////
		// set state's n+z based on the value 'val'
		static FINLINE void t65_CALL SetNZ(MachineType &state,byte val) {
			state.cpustate.p&=~(N_MASK|Z_MASK);
			DoOrNZFlags(state,val);
		}

		//////////////////////////////////////////////////////////////////////////
		// Loads a value into a register, setting the flags as it does so
		static FINLINE void t65_CALL LoadRegister(MachineType &state,byte &dest,byte src) {
			dest=src;
			SetNZ(state,dest);
		}

		//////////////////////////////////////////////////////////////////////////
		//
		//5/12/2004 -- removing InitialiseCpuState (suspect real 6502 starts up with gibberish)
//		static void InitialiseCPUState(MachineType &state);

		//////////////////////////////////////////////////////////////////////////
		// Reset
		static FINLINE void t65_CALL Reset(MachineType &state) {
			//state.cpustate.pc.w+=2;//I don't know what happens in these 2 cycles
			state.cpucycles+=2;//I think this is what I meant :-) [15/9/2002]
			Interrupt(state,resetvector);
			state.cpustate.p|=I_MASK;
			state.cpustate.s.h=0x01;
			// all other registers indeterminate
		}

		//////////////////////////////////////////////////////////////////////////
		// Set up the nztable, and anything else required.
		static void Init();

		//////////////////////////////////////////////////////////////////////////
		// Addressing modes
		//
		struct ModeIMM {
			enum {operand_size=1,};
			
			static char *Disassemble(char *buf,const byte *bytes,Word) {
				*buf++='#';
				return AddHexByte(buf,bytes[0]);
			}

			static const char *AddrModeName() {
				return "IMM";
			}

			//My guess is it takes no time to calculate the EA for an
			//immediate instruction...
			static FINLINE Word ReadModeEA(MachineType &state) {
				Word old=state.cpustate.pc;
				++state.cpustate.pc.w;
				return old;
			}

			static FINLINE byte ReadOperand(MachineType &state,Word addr) {
				byte b=state.ReadByte(addr,catFetchInstr);
				++state.cpucycles;
				return b;
			}
		};

		struct ModeZPG {
			enum {operand_size=1,};
			
			static char *Disassemble(char *buf,const byte *bytes,Word=Word()) {
				return AddHexByte(buf,bytes[0]);
			}
			
			static const char *AddrModeName() {
				return "ZPG";
			}
			
			static FINLINE Word ReadModeEA(MachineType &state) {
				Word addr;

				addr.w=state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;
				++state.cpucycles;
				return addr;
			}

			static FINLINE Word WriteModeEA(MachineType &state) {
				return ReadModeEA(state);
			}

			static FINLINE byte ReadOperand(MachineType &state,Word addr) {
				byte b=state.ReadByte(addr,catReadZP);
				++state.cpucycles;
				return b;
			}

			static FINLINE void t65_CALL WriteOperand(MachineType &state,Word addr,byte val) {
				state.WriteByte(addr,val,catWriteZP);
				++state.cpucycles;
			}
		};

		struct ModeZPX:
		public ModeZPG
		{
			static char *Disassemble(char *buf,const byte *bytes,Word) {
				return AddIndex(ModeZPG::Disassemble(buf,bytes),'X');
			}
			
			static const char *AddrModeName() {
				return "ZPX";
			}

			static FINLINE Word ReadModeEA(MachineType &state) {
				Word addr;
				
				addr.w=state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;
				++state.cpucycles;
				state.ReadByte(addr,catReadZP);
				addr.l+=state.cpustate.x;
				++state.cpucycles;
				return addr;
			}
			
			static FINLINE Word WriteModeEA(MachineType &state) {
				return ReadModeEA(state);
			}
		};

		struct ModeZPY:
		public ModeZPG
		{
			static char *Disassemble(char *buf,const byte *bytes,Word) {
				return AddIndex(ModeZPG::Disassemble(buf,bytes),'Y');
			}

			static const char *AddrModeName() {
				return "ZPY";
			}

			static FINLINE Word ReadModeEA(MachineType &state) {
				Word addr;
				
				addr.w=state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;
				++state.cpucycles;
				state.ReadByte(addr,catReadZP);
				addr.l+=state.cpustate.y;
				++state.cpucycles;
				return addr;
			}
			
			static FINLINE Word WriteModeEA(MachineType &state) {
				return ReadModeEA(state);
			}
		};
		
		struct ModeABS {
			enum {operand_size=2,};

			static char *Disassemble(char *buf,const byte *bytes,Word=Word()) {
				return AddHexWord(buf,MakeWord(bytes[0],bytes[1]));
			}

			static const char *AddrModeName() {
				return "ABS";
			}

			static FINLINE Word ReadModeEA(MachineType &state) {
				Word addr;
				
				addr.l=state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;
				++state.cpucycles;
				addr.h=state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;
				++state.cpucycles;
				return addr;
			}

			static FINLINE Word WriteModeEA(MachineType &state) {
				return ReadModeEA(state);
			}

			static FINLINE byte ReadOperand(MachineType &state,Word addr) {
				byte b=state.ReadByte(addr,catReadOperand);
				++state.cpucycles;
				return b;
			}

			static FINLINE void t65_CALL WriteOperand(MachineType &state,Word addr,byte val) {
				state.WriteByte(addr,val,catWriteOperand);
				++state.cpucycles;
			}
		};

		struct ModeABX:
		public ModeABS
		{
			static char *Disassemble(char *buf,const byte *bytes,Word) {
				return AddIndex(ModeABS::Disassemble(buf,bytes),'X');
			}

			static const char *AddrModeName() {
				return "ABX";
			}

			static FINLINE Word ReadModeEA(MachineType &state) {
				Word addr,newaddr;
				addr.l=state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;
				++state.cpucycles;
				addr.h=state.ReadByte(state.cpustate.pc,catFetchInstr);
				newaddr.w=addr.l+state.cpustate.x;
				addr.l=newaddr.l;
				++state.cpustate.pc.w;
				++state.cpucycles;
				if(newaddr.h) {
					state.ReadByte(addr,catReadOperand);
					++addr.h;
					++state.cpucycles;
				}
				return addr;
			}

			static FINLINE Word WriteModeEA(MachineType &state) {
				Word addr,newaddr;
				
				addr.l=state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;
				++state.cpucycles;
				addr.h=state.ReadByte(state.cpustate.pc,catFetchInstr);
				newaddr.w=addr.w;
				newaddr.l+=state.cpustate.x;
				++state.cpustate.pc.w;
				++state.cpucycles;
				state.ReadByte(newaddr,catReadOperand);
				newaddr.w=addr.w+state.cpustate.x;
				++state.cpucycles;
				return newaddr;
			}
		};
		
		struct ModeABY:
		public ModeABS
		{
			static char *Disassemble(char *buf,const byte *bytes,Word) {
				return AddIndex(ModeABS::Disassemble(buf,bytes),'Y');
			}

			static const char *AddrModeName() {
				return "ABY";
			}

			static FINLINE Word ReadModeEA(MachineType &state) {
				Word addr,lowbyteresult;
				addr.l=state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;
				++state.cpucycles;
				addr.h=state.ReadByte(state.cpustate.pc,catFetchInstr);
				lowbyteresult.w=addr.l+state.cpustate.y;//NOTE 9/4/2003 eliminate C4244
				addr.l=lowbyteresult.l;
				++state.cpustate.pc.w;
				++state.cpucycles;
				if(lowbyteresult.h) {
					state.ReadByte(addr,catReadOperand);
					++addr.h;
					++state.cpucycles;
				}
				return addr;
			}

			static FINLINE Word WriteModeEA(MachineType &state) {
				Word addr,newaddr;
				
				addr.l=state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;
				++state.cpucycles;
				addr.h=state.ReadByte(state.cpustate.pc,catFetchInstr);
				newaddr.w=addr.w;
				newaddr.l+=state.cpustate.y;
				++state.cpustate.pc.w;
				++state.cpucycles;
				state.ReadByte(newaddr,catReadOperand);
				newaddr.w=addr.w+state.cpustate.y;
				++state.cpucycles;
				return newaddr;
			}
		};
		  
		struct ModeINX {
			enum {operand_size=1,};

			static char *Disassemble(char *buf,const byte *bytes,Word) {
				*buf++='(';
				buf=AddHexByte(buf,bytes[0]);
				buf=AddIndex(buf,'X');
				*buf++=')';
				*buf=0;
				return buf;
			}

			static const char *AddrModeName() {
				return "INX";
			}

			static FINLINE Word ReadModeEA(MachineType &state) {
				Word ptr,addr;

				ptr.w=state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;
				++state.cpucycles;
				state.ReadByte(ptr,catReadZP);
				ptr.l+=state.cpustate.x;
				++state.cpucycles;
				addr.l=state.ReadByte(ptr,catReadZP);
				++state.cpucycles;
				++ptr.l;
				addr.h=state.ReadByte(ptr,catReadZP);
				++state.cpucycles;
				return addr;
			}
			
			static FINLINE Word WriteModeEA(MachineType &state) {
				Word ptr,addr;
				
				ptr.w=state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;
				++state.cpucycles;
				state.ReadByte(ptr,catReadZP);
				ptr.l+=state.cpustate.x;
				++state.cpucycles;
				addr.l=state.ReadByte(ptr,catReadZP);
				++ptr.l;
				++state.cpucycles;
				addr.h=state.ReadByte(ptr,catReadZP);
				++state.cpucycles;
				return addr;
			}
			
			static FINLINE byte ReadOperand(MachineType &state,Word addr) {
				byte b=state.ReadByte(addr,catReadOperand);
				++state.cpucycles;
				return b;
			}
			
			static FINLINE void t65_CALL WriteOperand(MachineType &state,Word addr,byte val) {
				state.WriteByte(addr,val,catWriteOperand);
				++state.cpucycles;
			}
		};

		struct ModeINY {
			enum {operand_size=1,};

			static char *Disassemble(char *buf,const byte *bytes,Word) {
				*buf++='(';
				buf=AddHexByte(buf,bytes[0]);
				*buf++=')';
				buf=AddIndex(buf,'Y');
				return buf;
			}

			static const char *AddrModeName() {
				return "INY";
			}

			static FINLINE Word ReadModeEA(MachineType &state) {
				Word ptr,addr,lsbresult;

				ptr.w=state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;
				++state.cpucycles;
				addr.l=state.ReadByte(ptr,catReadZP);
				++state.cpucycles;
				++ptr.l;
				addr.h=state.ReadByte(ptr,catReadZP);
				lsbresult.w=addr.l+state.cpustate.y;
				addr.l=lsbresult.l;//9/4/2003 eliminate C4244
				++state.cpucycles;
				if(lsbresult.h) {
					state.ReadByte(addr,catReadOperand);
					++addr.h;
					++state.cpucycles;
				}
				return addr;
			}
			
			static FINLINE Word WriteModeEA(MachineType &state) {
				Word ptr,addr,lsbresult;

				ptr.w=state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;
				++state.cpucycles;
				addr.l=state.ReadByte(ptr,catReadZP);
				++ptr.l;
				++state.cpucycles;
				addr.h=state.ReadByte(ptr,catReadZP);
				lsbresult.w=addr.l+state.cpustate.y;
				addr.l=lsbresult.l;//9/4/2003 eliminate C4244
				++state.cpucycles;
				state.ReadByte(addr,catReadOperand);
				addr.h+=lsbresult.h;
				++state.cpucycles;
				return addr;
			}
			
			static FINLINE byte ReadOperand(MachineType &state,Word addr) {
				byte b=state.ReadByte(addr,catReadOperand);
				++state.cpucycles;
				return b;
			}
			
			static FINLINE void t65_CALL WriteOperand(MachineType &state,Word addr,byte val) {
				state.WriteByte(addr,val,catWriteOperand);
				++state.cpucycles;
			}
		};

		struct ModeIND {
			enum {operand_size=2,};

			static char *Disassemble(char *buf,const byte *bytes,Word) {
				*buf++='(';
				buf=AddHexWord(buf,MakeWord(bytes[0],bytes[1]));
				*buf++=')';
				*buf=0;
				return buf;
			}

			static const char *AddrModeName() {
				return "IND";
			}

			static FINLINE Word ReadModeEA(MachineType &state) {
				Word ptr,addr;
				
				ptr.l=state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;//2
				++state.cpucycles;
				ptr.h=state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;//3
				++state.cpucycles;
				addr.l=state.ReadByte(ptr,catFetchAddress);
				++ptr.l;
				++state.cpucycles;//4
				addr.h=state.ReadByte(ptr,catFetchAddress);
				++state.cpucycles;//5
				return addr;
			}
		};

		//////////////////////////////////////////////////////////////////////////
		// other addressing modes -- these are needed for the templates, but
		// do not actually do anything useful. sometimes other templates are
		// specialized using these types.
		struct ModeIMP {
			// Implied
			enum {operand_size=0,};

			static char *Disassemble(char *buf,const byte *,Word) {
				*buf=0;
				return buf;
			}

			static const char *AddrModeName() {
				return "IMP";
			}
		};
		
		struct ModeREL {
			enum {operand_size=1,};

			// Relative
			static char *Disassemble(char *buf,const byte *bytes,Word bytes_base) {
//				return AddHexWord(buf,MakeWord(bytes_base.w+MakeWord(bytes[0],bytes[1]).w));
				return AddHexWord(buf,MakeWord(bytes_base.w+2+int8(bytes[0])));
			}

			static const char *AddrModeName() {
				return "REL";
			}
		};

		struct ModeACC {
			enum {operand_size=0,};
			// Accumulator
			static char *Disassemble(char *buf,const byte *,Word) {
				*buf++='A';
				*buf=0;
				return buf;
			}

			static const char *AddrModeName() {
				return "ACC";
			}

			//Xbox: it can't take RMW specialization
			//see comment for RMW
			static FINLINE Word WriteModeEA(MachineType &) {
				static Word dummy={0};
				return dummy;
			}

			static FINLINE byte ReadOperand(MachineType &state,Word) {
				//Accumulator operand -- should be clear enough.
				//we increment the cycles here too, as this bit only happens 1x in a RMW ACC instr
				//and it's got to happen somewhere.
				++state.cpucycles;
				return state.cpustate.a;
			}

			//Write value back to A
			static FINLINE void t65_CALL WriteOperand(MachineType &state,Word,byte val) {
				state.cpustate.a=val;
#ifdef t65_DEBUGGER
				state.ice.OnChangeA();
#endif//t65_DEBUGGER
			}
		};

		// This is an artefact of the automatic code generation stuff.
		// TODO sort out maketable.bas etc. so this goes away.
		typedef ModeIMP ModeILL;

		//////////////////////////////////////////////////////////////////////////
		// Instruction types
		template<class Instr,class AddrMode>
		struct Write {
			typedef Instr InstrType;
			typedef AddrMode AddrModeType;
			static FINLINE void t65_CALL t65_CALL Execute(MachineType &state) {
				Word addr=AddrMode::WriteModeEA(state);
				byte writeval=Instr::Execute(state);
				AddrMode::WriteOperand(state,addr,writeval);
			}
		};
		
		template<class Instr,class AddrMode>
		struct Read {
			typedef Instr InstrType;
			typedef AddrMode AddrModeType;
			static FINLINE void t65_CALL t65_CALL Execute(MachineType &state) {
				Word addr=AddrMode::ReadModeEA(state);
				byte operand=AddrMode::ReadOperand(state,addr);
				Instr::Execute(state,operand);
			}
		};

		template<class Instr,class AddrMode>
		struct RMW {
			typedef Instr InstrType;
			typedef AddrMode AddrModeType;
			static FINLINE void t65_CALL t65_CALL Execute(MachineType &state) {
				Word addr=AddrMode::WriteModeEA(state);
				byte val=AddrMode::ReadOperand(state,addr);
				AddrMode::WriteOperand(state,addr,val);
				val=Instr::Execute(state,val);
				AddrMode::WriteOperand(state,addr,val);
			}
		};

		//Instead of being overly clever these days I just ignore the AddrMode type
		//and assume it's ModeIMP as it should be. (The above was only for
		//making sure I got it right, anyway.)
		template<class Instr,class AddrMode>
		struct Implied {
			typedef Instr InstrType;
			typedef ModeIMP AddrModeType;
			static FINLINE void t65_CALL t65_CALL Execute(MachineType &state) {
				state.ReadByte(state.cpustate.pc,catFetchInstr);
				Instr::Execute(state);
				++state.cpucycles;
			}
		};

		template<class Instr,class AddrMode>
		struct Relative {
			typedef Instr InstrType;
			typedef AddrMode AddrModeType;
			static FINLINE void t65_CALL t65_CALL Execute(MachineType &state) {
				Instr::ExecuteRelative(state);
			}
		};

		template<class Instr,class AddrMode>
		struct Special {
			typedef Instr InstrType;
			typedef AddrMode AddrModeType;
			static FINLINE void t65_CALL t65_CALL Execute(MachineType &state) {
				Instr::Execute(state,AddrMode());
			}
		};

		//////////////////////////////////////////////////////////////////////////
		//
		struct InstrILL {
			static const char *Mnemonic() {
				return "ILL";
			}
//			static FINLINE void t65_CALL Execute(MachineType &state) {
//				logErr.logf("ILL instruction at &%04X\n",state.cpustate.pc.w-1);
//				WriteHistory();
//				SoftBreak();
//			}
		};
		
		//////////////////////////////////////////////////////////////////////////
		// HLT
		struct InstrHLT {
			static const char *Mnemonic() {
				return "HLT";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				//This instruction locks the CPU.
				state.cpustate.p|=I_MASK;
				//Instruction fetch has incremented the PC.
				//Decrement it so the CPU keeps fetching this opcode.
				--state.cpustate.pc.w;
			}
		};

		//////////////////////////////////////////////////////////////////////////
		// BRK
		struct InstrBRK {
			static const char *Mnemonic() {
				return "BRK";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;
				++state.cpucycles;
				state.cpustate.p|=B_MASK;
				Interrupt(state,irqvector);
				state.cpustate.p|=I_MASK;//6502 test suite says I should be set, so...!
			}
		};

		//////////////////////////////////////////////////////////////////////////
		// Store instructions
		struct InstrSTA {
			static const char *Mnemonic() {
				return "STA";
			}
			static FINLINE byte Execute(MachineType &state) {
				return state.cpustate.a;
			}
		};
		
		struct InstrSTX {
			static const char *Mnemonic() {
				return "STX";
			}
			static FINLINE byte Execute(MachineType &state) {
				return state.cpustate.x;
			}
		};
		
		struct InstrSTY {
			static const char *Mnemonic() {
				return "STY";
			}
			static FINLINE byte Execute(MachineType &state) {
				return state.cpustate.y;
			}
		};
		
		//////////////////////////////////////////////////////////////////////////
		// Load instructions
		struct InstrLDA {
			static const char *Mnemonic() {
				return "LDA";
			}
			static FINLINE void t65_CALL Execute(MachineType &state,byte val) {
				LoadRegister(state,state.cpustate.a,val);
#ifdef t65_DEBUGGER
			state.ice.OnChangeA();
#endif//t65_DEBUGGER
			}
		};

		struct InstrLDX {
			static const char *Mnemonic() {
				return "LDX";
			}
			static FINLINE void t65_CALL Execute(MachineType &state,byte val) {
				LoadRegister(state,state.cpustate.x,val);
#ifdef t65_DEBUGGER
				state.ice.OnChangeX();
#endif//t65_DEBUGGER
			}
		};

		struct InstrLDY {
			static const char *Mnemonic() {
				return "LDY";
			}
			static FINLINE void t65_CALL Execute(MachineType &state,byte val) {
				LoadRegister(state,state.cpustate.y,val);
#ifdef t65_DEBUGGER
				state.ice.OnChangeY();
#endif//t65_DEBUGGER
			}
		};

		//////////////////////////////////////////////////////////////////////////
		// Index register 'arithmetic' instructions
		// TODO	this is really an implied form of the INC/DEC stuff: sort it out!
		//		Can use for INA/DEA as well.
		struct InstrINX {
			static const char *Mnemonic() {
				return "INX";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				++state.cpustate.x;
#ifdef t65_DEBUGGER
				state.ice.OnChangeX();
#endif//t65_DEBUGGER
				SetNZ(state,state.cpustate.x);
				//++state.cpucycles;
			}
		};
		
		struct InstrINY {
			static const char *Mnemonic() {
				return "INY";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				++state.cpustate.y;
#ifdef t65_DEBUGGER
				state.ice.OnChangeY();
#endif//t65_DEBUGGER
				SetNZ(state,state.cpustate.y);
				//++state.cpucycles;
			}
		};
		
		struct InstrDEX {
			static const char *Mnemonic() {
				return "DEX";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				--state.cpustate.x;
#ifdef t65_DEBUGGER
				state.ice.OnChangeX();
#endif//t65_DEBUGGER
				SetNZ(state,state.cpustate.x);
				//++state.cpucycles;
			}
		};
		
		struct InstrDEY {
			static const char *Mnemonic() {
				return "DEY";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				--state.cpustate.y;
#ifdef t65_DEBUGGER
				state.ice.OnChangeY();
#endif//t65_DEBUGGER
				SetNZ(state,state.cpustate.y);
				//++state.cpucycles;
			}
		};
		
		//////////////////////////////////////////////////////////////////////////
		// Transfer instructions
		struct InstrTAX {
			static const char *Mnemonic() {
				return "TAX";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				LoadRegister(state,state.cpustate.x,state.cpustate.a);
#ifdef t65_DEBUGGER
				state.ice.OnChangeX();
#endif//t65_DEBUGGER
			}
		};

		struct InstrTXA {
			static const char *Mnemonic() {
				return "TXA";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				LoadRegister(state,state.cpustate.a,state.cpustate.x);
#ifdef t65_DEBUGGER
				state.ice.OnChangeA();
#endif//t65_DEBUGGER
			}
		};

		struct InstrTAY {
			static const char *Mnemonic() {
				return "TAY";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				LoadRegister(state,state.cpustate.y,state.cpustate.a);
#ifdef t65_DEBUGGER
				state.ice.OnChangeY();
#endif//t65_DEBUGGER
			}
		};

		struct InstrTYA {
			static const char *Mnemonic() {
				return "TYA";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				LoadRegister(state,state.cpustate.a,state.cpustate.y);
#ifdef t65_DEBUGGER
				state.ice.OnChangeA();
#endif//t65_DEBUGGER
			}
		};

		struct InstrTSX {
			static const char *Mnemonic() {
				return "TSX";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				LoadRegister(state,state.cpustate.x,state.cpustate.s.l);
#ifdef t65_DEBUGGER
				state.ice.OnChangeX();
#endif//t65_DEBUGGER
			}
		};
		
		struct InstrTXS {
			static const char *Mnemonic() {
				return "TXS";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				state.cpustate.s.l=state.cpustate.x;
#ifdef t65_DEBUGGER
				state.ice.OnChangeS();
#endif//t65_DEBUGGER
			}
		};

		//////////////////////////////////////////////////////////////////////////
		// Stack ops


		static FINLINE void t65_CALL PushRegContentsInstr(MachineType &state,byte value) {
			state.ReadByte(state.cpustate.pc,catFetchInstr);
			++state.cpucycles;
			Push(state,value);
			++state.cpucycles;
		}

		//pops value, no NZ
		static FINLINE byte PopValueInstr(MachineType &state) {
			state.ReadByte(state.cpustate.pc,catFetchInstr);
			++state.cpucycles;
			state.ReadByte(state.cpustate.s,catReadStack);
			++state.cpucycles;
			byte val=Pop(state);
			//SetNZ(state,val);
			++state.cpucycles;
			return val;
		}

		struct InstrPHP {
			static const char *Mnemonic() {
				return "PHP";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				// 8/2/04 -- first cycle (read dummy byte from PC) done by Implied instruction type, d'oh!
				//state.ReadByte(state.cpustate.pc,catFetchInstr);
				//++state.cpucycles;
				Push(state,state.cpustate.p|B_MASK|U_MASK);
				++state.cpucycles;
			}
		};

		struct InstrPHA {
			static const char *Mnemonic() {
				return "PHA";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				// 8/2/04 -- first cycle (read dummy byte from PC) done by Implied instruction type, d'oh!
				//state.ReadByte(state.cpustate.pc,catFetchInstr);
				//++state.cpucycles;
				Push(state,state.cpustate.a);
				++state.cpucycles;
			}
		};
		
		struct InstrPLP {
			static const char *Mnemonic() {
				return "PLP";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
#ifdef t65CPU_MANAGES_I_FLAG
				t65::byte old=state.cpustate.p&I_MASK;
#endif
				// 8/2/04 -- first cycle (read dummy byte from PC) done by Implied instruction type, d'oh!
				//state.ReadByte(state.cpustate.pc,catFetchInstr);
				//++state.cpucycles;
				state.ReadByte(state.cpustate.s,catReadStack);
				++state.cpucycles;
				state.cpustate.p=Pop(state)|U_MASK|B_MASK;
				++state.cpucycles;
#ifdef t65CPU_MANAGES_I_FLAG
				if(old&&!(state.cpustate.p&I_MASK)) {
					state.IrqsReenabled();
				}
#endif
#ifdef t65_DEBUGGER
				state.ice.OnChangeP();
#endif//t65_DEBUGGER
			}
		};
		
		struct InstrPLA {
			static const char *Mnemonic() {
				return "PLA";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				// 8/2/04 -- first cycle (read dummy byte from PC) done by Implied instruction type, d'oh!
				//state.ReadByte(state.cpustate.pc,catFetchInstr);
				//++state.cpucycles;
				state.ReadByte(state.cpustate.s,catReadStack);
				++state.cpucycles;
				state.cpustate.a=Pop(state);
				SetNZ(state,state.cpustate.a);
				++state.cpucycles;
#ifdef t65_DEBUGGER
				state.ice.OnChangeA();
#endif//t65_DEBUGGER
			}
		};
		
		//////////////////////////////////////////////////////////////////////////
		// INC and DEC
		struct InstrINC {
			static const char *Mnemonic() {
				return "INC";
			}
			static FINLINE byte Execute(MachineType &state,byte val) {
				++val;
				SetNZ(state,val);
				return val;
			}
		};

		struct InstrDEC {
			static const char *Mnemonic() {
				return "DEC";
			}
			static FINLINE byte Execute(MachineType &state,byte val) {
				--val;
				SetNZ(state,val);
				return val;
			}
		};
		
		//////////////////////////////////////////////////////////////////////////
		// Shifts/rotates
		struct InstrASL {
			static const char *Mnemonic() {
				return "ASL";
			}
			static FINLINE byte Execute(MachineType &state,byte val) {
				state.cpustate.p&=~C_MASK;
				state.cpustate.p|=val>>7;
				val<<=1;
				SetNZ(state,val);
				return val;
			}
		};

		struct InstrLSR {
			static const char *Mnemonic() {
				return "LSR";
			}
			static FINLINE byte Execute(MachineType &state,byte val) {
				state.cpustate.p&=~(N_MASK|Z_MASK|C_MASK);
				state.cpustate.p|=val&1;
				val>>=1;
				state.cpustate.p|=(!val)<<1;//Z_MASK=2
				return val;
			}
		};

		struct InstrROR {
			static const char *Mnemonic() {
				return "ROR";
			}
			static FINLINE byte Execute(MachineType &state,byte val) {
				Word rotate;
				
				rotate.h=state.cpustate.p&1;
				state.cpustate.p&=~1;
				state.cpustate.p|=val&1;
				rotate.l=val;
				rotate.w>>=1;
				SetNZ(state,rotate.l);
				return rotate.l;
			}
		};
		
		struct InstrROL {
			static const char *Mnemonic() {
				return "ROL";
			}
			static FINLINE byte Execute(MachineType &state,byte val) {
				Word rotate;

				rotate.w=val<<1;
				rotate.l|=state.cpustate.p&1;
				state.cpustate.p&=~1;
				state.cpustate.p|=rotate.h;
				SetNZ(state,rotate.l);
				return rotate.l;
			}
		};

		//////////////////////////////////////////////////////////////////////////
		// Comparison
		static FINLINE void t65_CALL Compare(MachineType &state,byte reg,byte val) {
			state.cpustate.p&=~(N_MASK|Z_MASK|C_MASK);
			state.cpustate.p|=reg>=val;//C_MASK
			state.cpustate.p|=(reg==val)<<1;//Z_MASK
			state.cpustate.p|=(reg-val)&0x80;//N_MASK
#ifdef t65_DEBUGGER
			state.ice.OnChangeP();
#endif//t65_DEBUGGER
		}

		struct InstrCMP {
			static const char *Mnemonic() {
				return "CMP";
			}
			static FINLINE void t65_CALL Execute(MachineType &state,byte val) {
				return Compare(state,state.cpustate.a,val);
			}
		};
		
		struct InstrCPX {
			static const char *Mnemonic() {
				return "CPX";
			}
			static FINLINE void t65_CALL Execute(MachineType &state,byte val) {
				return Compare(state,state.cpustate.x,val);
			}
		};

		struct InstrCPY {
			static const char *Mnemonic() {
				return "CPY";
			}
			static FINLINE void t65_CALL Execute(MachineType &state,byte val) {
				return Compare(state,state.cpustate.y,val);
			}
		};

		//////////////////////////////////////////////////////////////////////////
		// Logical
		struct InstrAND {
			static const char *Mnemonic() {
				return "AND";
			}
			static FINLINE void t65_CALL Execute(MachineType &state,byte val) {
				state.cpustate.a&=val;
#ifdef t65_DEBUGGER
				state.ice.OnChangeA();
#endif//t65_DEBUGGER
				SetNZ(state,state.cpustate.a);
			}
		};

		struct InstrEOR {
			static const char *Mnemonic() {
				return "EOR";
			}
			static FINLINE void t65_CALL Execute(MachineType &state,byte val) {
				state.cpustate.a^=val;
#ifdef t65_DEBUGGER
				state.ice.OnChangeA();
#endif//t65_DEBUGGER
				SetNZ(state,state.cpustate.a);
			}
		};
		
		struct InstrORA {
			static const char *Mnemonic() {
				return "ORA";
			}
			static FINLINE void t65_CALL Execute(MachineType &state,byte val) {
				state.cpustate.a|=val;
#ifdef t65_DEBUGGER
				state.ice.OnChangeA();
#endif//t65_DEBUGGER
				SetNZ(state,state.cpustate.a);
			}
		};
		
		//////////////////////////////////////////////////////////////////////////
		// Set/clear P flags
		template<byte mask>
		struct InstrSetP {
			static const char *Mnemonic() {
				return "SetP";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				state.cpustate.p|=mask;
#ifdef t65_DEBUGGER
				state.ice.OnChangeP();
#endif//t65_DEBUGGER
			}
		};

		template<byte mask>
		struct InstrClearP {
			static const char *Mnemonic() {
				return "ClearP";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
				state.cpustate.p&=~mask;
#ifdef t65_DEBUGGER
				state.ice.OnChangeP();
#endif//t65_DEBUGGER
			}
		};

		struct InstrSED:
		public InstrSetP<D_MASK>
		{
			static const char *Mnemonic() {
				return "SED";
			}
		};
		struct InstrSEI:
		public InstrSetP<I_MASK>
		{
			static const char *Mnemonic() {
				return "SEI";
			}
		};
		struct InstrSEC:
		public InstrSetP<C_MASK>
		{
			static const char *Mnemonic() {
				return "SEC";
			}
		};
		struct InstrCLV:
		public InstrClearP<V_MASK>
		{
			static const char *Mnemonic() {
				return "CLV";
			}
		};
		struct InstrCLD:
		public InstrClearP<D_MASK>
		{
			static const char *Mnemonic() {
				return "CLD";
			}
		};
		struct InstrCLI {
			static const char *Mnemonic() {
				return "CLI";
			}
			static FINLINE void t65_CALL Execute(MachineType &state) {
#ifdef t65CPU_MANAGES_I_FLAG
				t65::byte old=state.cpustate.p&t65::I_MASK;
#endif
				state.cpustate.p&=~I_MASK;
#ifdef t65_DEBUGGER
				state.ice.OnChangeP();
#endif//t65_DEBUGGER
#ifdef t65CPU_MANAGES_I_FLAG
				if(old) {
					state.IrqsReenabled();
				}
#endif
			}
		};
		struct InstrCLC:
		public InstrClearP<C_MASK>
		{
			static const char *Mnemonic() {
				return "CLC";
			}
		};

		//////////////////////////////////////////////////////////////////////////
		// Branch & jump instrutcions
		//
		// Branch is weird, it looks like the 6502 keeps fetching the next
		// instruction whilst doing the final bits of the instruction. Bizarro.
		// TODO Take care when putting in catReadInternal, because of that.
		template<byte mask,byte check>
		struct InstrBranch {
			static const char *Mnemonic() {
				return "Branch";
			}
			static FINLINE void t65_CALL ExecuteRelative(MachineType &state) {
				Word lsbresult;

				// 2nd cycle -- fetch operand, increment PC
				int8 operand=int8(state.ReadByte(state.cpustate.pc,catFetchInstr));
				++state.cpustate.pc.w;
				++state.cpucycles;

				// If branch not taken, bail out right here. 3rd cycle PC read
				// happens in main loop (see 64doc). Otherwise, 3rd cycle PC
				// read happens here.
				if((state.cpustate.p&mask)==check) {
					// 3rd cycle -- read PC, add operand to PCL
					state.ReadByte(state.cpustate.pc,catFetchInstr);
					lsbresult.w=state.cpustate.pc.w+operand;
					state.cpustate.pc.l=lsbresult.l;
					++state.cpucycles;

					// If PC needs a fixup, read from (incorrect) PC whilst fixing
					// it up. If PC doesn't need fixing, we drop out here and let
					// the main loop do the PC read.
					if(lsbresult.h!=state.cpustate.pc.h) {
						// 4th cycle -- read PC, fix PCH, ++PC.
						state.ReadByte(state.cpustate.pc,catFetchInstr);
						state.cpustate.pc.h=lsbresult.h;
						++state.cpucycles;
						//++state.cpustate.pc.w;
					}

					//64doc says 5 cycles in total if branch to new page. That breaks
					//Frogman, so I assume it's incorrect.
				}
			}
		};

//		template<byte mask>
//		struct InstrBranchIfSet:
//		public InstrBranch<mask,mask>
//		{
//		};
//
//		template<byte mask>
//		struct InstrBranchIfClear:
//		public InstrBranch<mask,0>
//		{
//		};
		
		struct InstrBMI:
		public InstrBranch<N_MASK,N_MASK>
		{
			static const char *Mnemonic() {
				return "BMI";
			}
		};
		struct InstrBVS:
		public InstrBranch<V_MASK,V_MASK>
		{
			static const char *Mnemonic() {
				return "BVS";
			}
		};
		struct InstrBEQ:
		public InstrBranch<Z_MASK,Z_MASK>
		{
			static const char *Mnemonic() {
				return "BEQ";
			}
		};
		struct InstrBCS:
		public InstrBranch<C_MASK,C_MASK>
		{
			static const char *Mnemonic() {
				return "BCS";
			}
		};

		// Borland C++ seems to prefer it this way.
		enum {
			ZERO=0,
		};
		
		struct InstrBPL:
		public InstrBranch<N_MASK,ZERO>
		{
			static const char *Mnemonic() {
				return "BPL";
			}
		};
		struct InstrBVC:
		public InstrBranch<V_MASK,ZERO>
		{
			static const char *Mnemonic() {
				return "BVC";
			}
		};
		struct InstrBNE:
		public InstrBranch<Z_MASK,ZERO>
		{
			static const char *Mnemonic() {
				return "BNE";
			}
		};
		struct InstrBCC:
		public InstrBranch<C_MASK,ZERO>
		{
			static const char *Mnemonic() {
				return "BCC";
			}
		};

		struct InstrJMP {
			static const char *Mnemonic() {
				return "JMP";
			}
			template<class AddrMode>
			static FINLINE void t65_CALL Execute(MachineType &state,const AddrMode &) {
				state.cpustate.pc=AddrMode::ReadModeEA(state);
			}
		};

		struct InstrJSR {
			static const char *Mnemonic() {
				return "JSR";
			}
			// no more specialization [4/18/2003]
			template<class AddrMode>
			static FINLINE void t65_CALL Execute(MachineType &state,const AddrMode &) {
				byte lsb;
//#ifdef bbcHACKING_URIDIUM
//				t65::word old_pc=state.cpustate.pc.w;
//#endif
				
				lsb=state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;
				++state.cpucycles;//2
				state.ReadByte(state.cpustate.s,catReadStack);//internal operation, according to 64doc.
				++state.cpucycles;//3
				Push(state,state.cpustate.pc.h);
				++state.cpucycles;//4
				Push(state,state.cpustate.pc.l);
				++state.cpucycles;//5
				state.cpustate.pc.h=state.ReadByte(state.cpustate.pc,catFetchInstr);
				state.cpustate.pc.l=lsb;
				++state.cpucycles;//6
//#ifdef bbcHACKING_URIDIUM
//				if(old_pc==0x2544) {//0x2543 - +1 due to instruction fetch
//					state.cpustate.pc.w=old_pc+2;
//					printf("skipped uridium's osrdch call. cpucycles=%d.\n",state.cpucycles);
//				}
//#endif
			}
		};

		//////////////////////////////////////////////////////////////////////////
		//
		struct InstrRTI {
			static const char *Mnemonic() {
				return "RTI";
			}
			// no more specialization [4/18/2003]
			template<class AddrMode>
			static FINLINE void t65_CALL Execute(MachineType &state,const AddrMode &) {
#ifdef t65CPU_MANAGES_I_FLAG
				t65::byte old=state.cpustate.p&t65::I_MASK;
#endif
				state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpucycles;//2
				state.ReadByte(state.cpustate.s,catReadStack);
				++state.cpucycles;//3
				state.cpustate.p=Pop(state);
				++state.cpucycles;//4
				state.cpustate.pc.l=Pop(state);
				++state.cpucycles;//5
				state.cpustate.pc.h=Pop(state);
				++state.cpucycles;//6
#ifdef t65CPU_MANAGES_I_FLAG
				if(old&&!(state.cpustate.p&t65::I_MASK)) {
					state.IrqsReenabled();
				}
#endif
			}
		};
				
		//////////////////////////////////////////////////////////////////////////
		// RTS
		struct InstrRTS {
			static const char *Mnemonic() {
				return "RTS";
			}
			// no more specialization [4/18/2003]
			template<class AddrMode>
			static FINLINE void t65_CALL Execute(MachineType &state,const AddrMode &) {
				state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpucycles;//2
				state.ReadByte(state.cpustate.s,catReadStack);
				++state.cpucycles;//3
				state.cpustate.pc.l=Pop(state);
				++state.cpucycles;//4
				state.cpustate.pc.h=Pop(state);
				++state.cpucycles;//5
				// Don't know if this is the right category...
				state.ReadByte(state.cpustate.pc,catFetchInstr);
				++state.cpustate.pc.w;
				++state.cpucycles;//6
			}
		};

		//////////////////////////////////////////////////////////////////////////
		// NOP
		struct InstrNOP {
			static const char *Mnemonic() {
				return "NOP";
			}
			//NOP is a read instruction. There's a defaulted val parameter so
			//it may be used as one as well as an implied instruction.
			static FINLINE void t65_CALL Execute(MachineType &,byte=0) {
				//do nothing... it's a nop...
			}
		};

		//////////////////////////////////////////////////////////////////////////
		// BIT
		struct InstrBIT {
			static const char *Mnemonic() {
				return "BIT";
			}
			static FINLINE void t65_CALL Execute(MachineType &state,byte val) {
				state.cpustate.p&=~(N_MASK|Z_MASK|V_MASK);
				state.cpustate.p|=val&0xC0;//set N+V
				state.cpustate.p|=((state.cpustate.a&val)==0)<<1;//set Z
#ifdef t65_DEBUGGER
				state.ice.OnChangeP();
#endif//t65_DEBUGGER
			}
		};

		//////////////////////////////////////////////////////////////////////////
		// ADC/SBC
		// See 64doc for more about these two!
		struct InstrADC {
			static const char *Mnemonic() {
				return "ADC";
			}
			static FINLINE void t65_CALL Execute(MachineType &state,byte val) {
				if(!(state.cpustate.p&D_MASK)) {
					Word result;

					result.w=state.cpustate.a+val+(state.cpustate.p&1);//&1=C
					state.cpustate.p&=~(N_MASK|Z_MASK|V_MASK|C_MASK);
					state.cpustate.p|=result.h;//do carry
					//state.cpustate.p|=nztable[result.l];//do n+z
					DoOrNZFlags(state,result.l);
#ifdef t65_BEEBEM_VFLAG
					int16 sresult=int8(state.cpustate.a)+int8(val)+(state.cpustate.p&1);
					if(((result.l&128)>0)^(sresult<0)) {
						state.cpustate.p|=V_MASK;
					}
#else
					//retro 1996 code from model-b: (think it works -- this was
					//what fixed cholo and os1.20 sound system.)
					//V=(~(A^(j)) & (A^ADCResult) & 0x80)>>1;
					state.cpustate.p|=(~(state.cpustate.a^val)&(state.cpustate.a^result.l)&0x80)>>1;
#endif
					state.cpustate.a=result.l;
				} else {
#ifdef t65_BEEBEM_BCD
					//BCD code from BeebEm
					int ZFlag=0,NFlag=0,CFlag=0,VFlag=0;
					int TmpResult,TmpCarry=0;
					int ln,hn;
					
					// Z flag determined from 2's compl result, not BCD result!
					TmpResult=(state.cpustate.a)+(val)+(state.cpustate.p&1);
					ZFlag=((TmpResult & 0xff)==0);
					
					ln=((state.cpustate.a) & 0xf)+((val) & 0xf)+(state.cpustate.p&1);
					if (ln>9) {
						ln += 6;
						ln &= 0xf;
						TmpCarry=0x10;
					}
					hn=((state.cpustate.a) & 0xf0)+((val) & 0xf0)+TmpCarry;
					// N and V flags are determined before high nibble is adjusted.
					// NOTE: V is not always correct
					NFlag=hn & 128;
					VFlag=(hn ^ (state.cpustate.a)) & 128 && !(((state.cpustate.a) ^ (val)) & 128);
					if (hn>0x90) {
						hn += 0x60;
						hn &= 0xf0;
						CFlag=1;
					}
					(state.cpustate.a)=hn|ln;
					ZFlag=((state.cpustate.a)==0);
					NFlag=((state.cpustate.a)&128);
					//SetPSR(FlagC | FlagZ | FlagV | FlagN,CFlag,ZFlag,0,0,0,VFlag,NFlag);
					state.cpustate.p&=~(N_MASK|V_MASK|Z_MASK|C_MASK);
					state.cpustate.p|=CFlag?C_MASK:0;
					state.cpustate.p|=ZFlag?Z_MASK:0;
					state.cpustate.p|=VFlag?V_MASK:0;
					state.cpustate.p|=NFlag?N_MASK:0;
#else//t65_BEEBEM_BCD
					//VICE (passes 6502 test suite)
					//					tmp = (reg_a & 0xf) + (tmp_value & 0xf) + (reg_p & 0x1);            
					//					if (tmp > 0x9)                                                      
					//						tmp += 0x6;                                                     
					//					if (tmp <= 0x0f)                                                    
					//						tmp = (tmp & 0xf) + (reg_a & 0xf0) + (tmp_value & 0xf0);        
					//					else                                                                
					//						tmp = (tmp & 0xf) + (reg_a & 0xf0) + (tmp_value & 0xf0) + 0x10; 
					//					LOCAL_SET_ZERO(!((reg_a + tmp_value + (reg_p & 0x1)) & 0xff));      
					//					LOCAL_SET_SIGN(tmp & 0x80);                                         
					//					LOCAL_SET_OVERFLOW(((reg_a ^ tmp) & 0x80)                           
					//						&& !((reg_a ^ tmp_value) & 0x80));              
					//					if ((tmp & 0x1f0) > 0x90)                                           
					//						tmp += 0x60;                                                    
					//					LOCAL_SET_CARRY((tmp & 0xff0) > 0xf0);                              
					unsigned tmp=(state.cpustate.a&0xf)+(val&0xf)+(state.cpustate.p&1);
					if(tmp>9) {
						tmp+=6;
					}
					if(tmp<=0xf) {
						tmp=(tmp&0xf)+(state.cpustate.a&0xf0)+(val&0xf0);
					} else {
						tmp=(tmp&0xf)+(state.cpustate.a&0xf0)+(val&0xf0)+0x10;
					}
					state.cpustate.p&=~(Z_MASK|V_MASK|N_MASK);
					if(((state.cpustate.a+val+(state.cpustate.p&1))&0xff)==0) {
						state.cpustate.p|=Z_MASK;
					}
					state.cpustate.p|=tmp&0x80;//N_MASK
					if(((state.cpustate.a^tmp)&0x80)&&!((state.cpustate.a^val)&0x80)) {
						state.cpustate.p|=V_MASK;
					}
					if((tmp&0x1f0)>0x90) {
						tmp+=0x60;
					}
					state.cpustate.p&=~C_MASK;
					state.cpustate.p|=(tmp&0xff0)>0xf0;//C_MASK
					state.cpustate.a=tmp;
#endif//t65_BEEBEM_BCD
				}
#ifdef t65_DEBUGGER
				state.ice.OnChangeA();
#endif//t65_DEBUGGER
			}
		};

		struct InstrSBC {
			static const char *Mnemonic() {
				return "SBC";
			}
			//like ADC but add ~val.
			static FINLINE void t65_CALL Execute(MachineType &state,byte val) {
				Word result;
				result.w=state.cpustate.a+byte(~val)+(state.cpustate.p&1);//&1=C
				if(!(state.cpustate.p&D_MASK)) {
					state.cpustate.p&=~(N_MASK|Z_MASK|V_MASK|C_MASK);
					state.cpustate.p|=result.h;//do carry (think this is right?)
					//state.cpustate.p|=nztable[result.l];//do n+z
					DoOrNZFlags(state,result.l);
#ifdef t65_BEEBEM_VFLAG
					//((Accumulator & 128)>0) ^ ((TmpResultV & 256)!=0)
					int16 sresult=int8(state.cpustate.a)-int8(val)-((~state.cpustate.p)&1);
					if(((result.l&128)>0)^((sresult&256)!=0)) {
						state.cpustate.p|=V_MASK;
					}
#else
					//retro 1996 code from model-b: (think it works -- this was
					//what fixed cholo and os1.20 sound system.)
					//V=(~(A^(j)) & (A^ADCResult) & 0x80)>>1;
					state.cpustate.p|=((state.cpustate.a^val)&(state.cpustate.a^result.l)&0x80)>>1;
#endif
					state.cpustate.a=result.l;
				} else {
#ifdef t65_BEEBEM_BCD
					//BCD code from BeebEm
					//(state.cpustate.a)=Accumulator
					//(state.cpustate.p&1)=GETCFLAG
					//(val)=operand
					int TmpResultV;
					unsigned char nhn,nln;
					int ZFlag=0,NFlag=0,CFlag=1,VFlag=0;
					int TmpResult,TmpCarry=0;
					int ln,hn,oln,ohn;
					nhn=((state.cpustate.a)>>4)&15; nln=(state.cpustate.a) & 15;
					
					/* Z flag determined from 2's compl result, not BCD result! */
					TmpResult=(state.cpustate.a)-(val)-(1-(state.cpustate.p&1));
					ZFlag=((TmpResult & 0xff)==0);
					
					ohn=(val) & 0xf0; oln = (val) & 0xf;
					if ((oln>9) && (((state.cpustate.a)&15)<10)) { oln-=10; ohn+=0x10; } 
					// promote the lower nibble to the next ten, and increase the higher nibble
					ln=((state.cpustate.a) & 0xf)-oln-(1-(state.cpustate.p&1));
					if (ln<0) {
						if (((state.cpustate.a) & 15)<10) ln-=6;
						ln&=0xf;
						TmpCarry=0x10;
					}
					hn=((state.cpustate.a) & 0xf0)-ohn-TmpCarry;
					/* N and V flags are determined before high nibble is adjusted.
					NOTE: V is not always correct */
					NFlag=hn & 128;
					TmpResultV=(signed char)(state.cpustate.a)-(signed char)(val)-(1-(state.cpustate.p&1));
					if ((TmpResultV<-128)||(TmpResultV>127)) VFlag=1; else VFlag=0;
					if (hn<0) {
						hn-=0x60;
						hn&=0xf0;
						CFlag=0;
					}
					(state.cpustate.a)=hn|ln;
					if ((state.cpustate.a)==0) ZFlag=1;
					NFlag=(hn &128);
					CFlag=(TmpResult&256)==0;
					//SetPSR(FlagC | FlagZ | FlagV | FlagN,CFlag,ZFlag,0,0,0,VFlag,NFlag);
					state.cpustate.p&=~(N_MASK|V_MASK|Z_MASK|C_MASK);
					state.cpustate.p|=CFlag?C_MASK:0;
					state.cpustate.p|=ZFlag?Z_MASK:0;
					state.cpustate.p|=VFlag?V_MASK:0;
					state.cpustate.p|=NFlag?N_MASK:0;
#else//t65_BEEBEM_BCD
					//VICE code (passes 6502 test suite)
//					unsigned int tmp_a;                                                
//					tmp_a = (reg_a & 0xf) - (src & 0xf) - ((reg_p & P_CARRY) ? 0 : 1); 
//					if (tmp_a & 0x10)                                                  
//						tmp_a = ((tmp_a - 6) & 0xf)                                    
//							| ((reg_a & 0xf0) - (src & 0xf0) - 0x10);             
//					else                                                               
//						tmp_a = (tmp_a & 0xf) | ((reg_a & 0xf0) - (src & 0xf0));       
//					if (tmp_a & 0x100)                                                 
//						tmp_a -= 0x60;                                                 
//					LOCAL_SET_CARRY(tmp < 0x100);                                      
//					LOCAL_SET_NZ(tmp & 0xff);                                          
//					LOCAL_SET_OVERFLOW(((reg_a ^ tmp) & 0x80)                          
//						&& ((reg_a ^ src) & 0x80));                     
//					reg_a = (BYTE) tmp_a;                                              
					unsigned tmp=(state.cpustate.a&0xf)-(val&0xf)-((state.cpustate.p&C_MASK)^1);
					if(tmp&0x10) {
						tmp=((tmp-6)&0xf)|((state.cpustate.a&0xf0)-(val&0xf0)-0x10);
					} else {
						tmp=(tmp&0xf)|((state.cpustate.a&0xf0)-(val&0xf0));
					}
					if(tmp&0x100) {
						tmp-=0x60;
					}
					state.cpustate.p&=~(V_MASK|C_MASK);
					SetNZ(state,result.l);
					state.cpustate.p^=result.h;
//					if(result.h&1) {
//						state.cpustate.p|=C_MASK;
//					}
					if(((state.cpustate.a^result.l)&0x80)&&((state.cpustate.a^val)&0x80)) {
						state.cpustate.p|=V_MASK;
					}
					state.cpustate.a=tmp;
#endif//t65_BEEBEM_BCD
				}
#ifdef t65_DEBUGGER
				state.ice.OnChangeA();
#endif//t65_DEBUGGER
			}
		};

		template<class InstructionSet>
		static void t65_CALL RunSingleInstruction(MachineType &state,const InstructionSet &) {
			byte op=state.ReadByte(state.cpustate.pc,catFetchInstr);
			++state.cpustate.pc.w;
			++state.cpucycles;
#ifdef t65_COUNT_INSTRUCTION_FREQUENCIES
			++instr_freqs[op];
#endif
#ifdef t65_USE_FUNCTION_TABLE
			(*OpcodeTable<InstrExecuteFn,InstructionSet>::opcode_table[op])(state);
#else
#error "t65_USE_FUNCTION_TABLE obligatory"
#endif
		}

#ifdef t65_COUNT_INSTRUCTION_FREQUENCIES
		static unsigned instr_freqs[256];
#endif
	};

	template<class C>
	const Word Generic6502<C>::nmivector={0xFFFA};
	template<class C>
	const Word Generic6502<C>::resetvector={0xFFFC};
	template<class C>
	const Word Generic6502<C>::irqvector={0xFFFE};

	//5/12/2004 -- removing InitialiseCpuState (suspect real 6502 starts up with gibberish)
//	template<class C>
//	void Generic6502<C>::InitialiseCPUState(MachineType &state) {
//		state.cpustate.a=0;
//		state.cpustate.x=0;
//		state.cpustate.y=0;
//		state.cpustate.s.h=1;
//		state.cpustate.s.l=0xFF;
//		state.cpustate.pc.w=0;
//		//state.cpucycles=0;//resetting cycle count is not the CPU's job!! [2/8/2003]
//		state.cpustate.p=B_MASK|U_MASK;
//	}

	template<class C>
	const t65TYPENAME C::ReadZP *Generic6502<C>::catReadZP=0;

	template<class C>
	const t65TYPENAME C::WriteZP *Generic6502<C>::catWriteZP=0;

	template<class C>
	const t65TYPENAME C::ReadStack *Generic6502<C>::catReadStack=0;

	template<class C>
	const t65TYPENAME C::WriteStack *Generic6502<C>::catWriteStack=0;

	template<class C>
	const t65TYPENAME C::FetchInstr *Generic6502<C>::catFetchInstr=0;

	template<class C>
	const t65TYPENAME C::FetchAddress *Generic6502<C>::catFetchAddress=0;

	template<class C>
	const t65TYPENAME C::ReadOperand *Generic6502<C>::catReadOperand=0;

	template<class C>
	const t65TYPENAME C::WriteOperand *Generic6502<C>::catWriteOperand=0;

	template<class C>
	const t65TYPENAME C::ReadDebug *Generic6502<C>::catReadDebug=0;

	template<class C>
	const t65TYPENAME C::WriteDebug *Generic6502<C>::catWriteDebug=0;

#ifdef t65_COUNT_INSTRUCTION_FREQUENCIES
	template<class C>
	unsigned Generic6502<C>::instr_freqs[256];
#endif

#ifdef t65_USE_NZTABLE
	template<class C>
	byte Generic6502<C>::nztable[256];
#endif

	template<class C>
	void Generic6502<C>::Init() {
#ifdef t65_USE_NZTABLE
		for(unsigned i=0;i<256;++i) {
			byte p=0;

			if(i==0) {
				p|=Z_MASK;
			}
			if(i&0x80) {
				p|=N_MASK;
			}
			Generic6502<C>::nztable[i]=p;
		}
//		printf("Generic6502<%s>::Init() done\n",
//			typeid(C).name());
#endif
#ifdef t65_COUNT_INSTRUCTION_FREQUENCIES
		for(unsigned i=0;i<256;++i) {
			instr_freqs[i]=0;
		}
#endif
	}
}

#include "t65WarningsPop.h"

#endif
