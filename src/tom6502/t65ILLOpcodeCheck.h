#ifndef t65ILLOPCODECHECK_H_
#define t65ILLOPCODECHECK_H_

#include "t65.h"

//By instantiating an object of this class, one may see (via the error messages) which
//opcodes are still InstrILL.

namespace t65 {
	template<class InstructionSet>
	struct IllOpcodeCheck {
#ifdef _MSC_VER

		//Well this one has me totally lost -- gcc says "wibble wibble
		//wibble at line 284" (or words to that effect).
		//i don't doubt it's something really obvious, but this
		//class is simply not important enough to warrant the effort.

		template<class Instr>
		struct IsIllegal {
			enum {
				magic=1,
			};
		};

		template<>
		struct IsIllegal<InstructionSet::P::InstrILL> {
			enum {
				magic=-1,
			};
		};

		//seems icl accepts 0-sized arrays. gcc definitely does. so force
		//the value to <0 or >0 -- negative arrays are DEFINITELY wrong!

		int IsOpcode00Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode00::InstrType>::magic];
		int IsOpcode01Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode01::InstrType>::magic];
		int IsOpcode02Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode02::InstrType>::magic];
		int IsOpcode03Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode03::InstrType>::magic];
		int IsOpcode04Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode04::InstrType>::magic];
		int IsOpcode05Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode05::InstrType>::magic];
		int IsOpcode06Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode06::InstrType>::magic];
		int IsOpcode07Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode07::InstrType>::magic];
		int IsOpcode08Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode08::InstrType>::magic];
		int IsOpcode09Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode09::InstrType>::magic];
		int IsOpcode0AOk[IsIllegal<t65TYPENAME InstructionSet::Opcode0A::InstrType>::magic];
		int IsOpcode0BOk[IsIllegal<t65TYPENAME InstructionSet::Opcode0B::InstrType>::magic];
		int IsOpcode0COk[IsIllegal<t65TYPENAME InstructionSet::Opcode0C::InstrType>::magic];
		int IsOpcode0DOk[IsIllegal<t65TYPENAME InstructionSet::Opcode0D::InstrType>::magic];
		int IsOpcode0EOk[IsIllegal<t65TYPENAME InstructionSet::Opcode0E::InstrType>::magic];
		int IsOpcode0FOk[IsIllegal<t65TYPENAME InstructionSet::Opcode0F::InstrType>::magic];
		int IsOpcode10Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode10::InstrType>::magic];
		int IsOpcode11Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode11::InstrType>::magic];
		int IsOpcode12Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode12::InstrType>::magic];
		int IsOpcode13Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode13::InstrType>::magic];
		int IsOpcode14Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode14::InstrType>::magic];
		int IsOpcode15Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode15::InstrType>::magic];
		int IsOpcode16Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode16::InstrType>::magic];
		int IsOpcode17Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode17::InstrType>::magic];
		int IsOpcode18Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode18::InstrType>::magic];
		int IsOpcode19Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode19::InstrType>::magic];
		int IsOpcode1AOk[IsIllegal<t65TYPENAME InstructionSet::Opcode1A::InstrType>::magic];
		int IsOpcode1BOk[IsIllegal<t65TYPENAME InstructionSet::Opcode1B::InstrType>::magic];
		int IsOpcode1COk[IsIllegal<t65TYPENAME InstructionSet::Opcode1C::InstrType>::magic];
		int IsOpcode1DOk[IsIllegal<t65TYPENAME InstructionSet::Opcode1D::InstrType>::magic];
		int IsOpcode1EOk[IsIllegal<t65TYPENAME InstructionSet::Opcode1E::InstrType>::magic];
		int IsOpcode1FOk[IsIllegal<t65TYPENAME InstructionSet::Opcode1F::InstrType>::magic];
		int IsOpcode20Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode20::InstrType>::magic];
		int IsOpcode21Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode21::InstrType>::magic];
		int IsOpcode22Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode22::InstrType>::magic];
		int IsOpcode23Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode23::InstrType>::magic];
		int IsOpcode24Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode24::InstrType>::magic];
		int IsOpcode25Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode25::InstrType>::magic];
		int IsOpcode26Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode26::InstrType>::magic];
		int IsOpcode27Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode27::InstrType>::magic];
		int IsOpcode28Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode28::InstrType>::magic];
		int IsOpcode29Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode29::InstrType>::magic];
		int IsOpcode2AOk[IsIllegal<t65TYPENAME InstructionSet::Opcode2A::InstrType>::magic];
		int IsOpcode2BOk[IsIllegal<t65TYPENAME InstructionSet::Opcode2B::InstrType>::magic];
		int IsOpcode2COk[IsIllegal<t65TYPENAME InstructionSet::Opcode2C::InstrType>::magic];
		int IsOpcode2DOk[IsIllegal<t65TYPENAME InstructionSet::Opcode2D::InstrType>::magic];
		int IsOpcode2EOk[IsIllegal<t65TYPENAME InstructionSet::Opcode2E::InstrType>::magic];
		int IsOpcode2FOk[IsIllegal<t65TYPENAME InstructionSet::Opcode2F::InstrType>::magic];
		int IsOpcode30Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode30::InstrType>::magic];
		int IsOpcode31Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode31::InstrType>::magic];
		int IsOpcode32Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode32::InstrType>::magic];
		int IsOpcode33Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode33::InstrType>::magic];
		int IsOpcode34Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode34::InstrType>::magic];
		int IsOpcode35Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode35::InstrType>::magic];
		int IsOpcode36Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode36::InstrType>::magic];
		int IsOpcode37Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode37::InstrType>::magic];
		int IsOpcode38Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode38::InstrType>::magic];
		int IsOpcode39Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode39::InstrType>::magic];
		int IsOpcode3AOk[IsIllegal<t65TYPENAME InstructionSet::Opcode3A::InstrType>::magic];
		int IsOpcode3BOk[IsIllegal<t65TYPENAME InstructionSet::Opcode3B::InstrType>::magic];
		int IsOpcode3COk[IsIllegal<t65TYPENAME InstructionSet::Opcode3C::InstrType>::magic];
		int IsOpcode3DOk[IsIllegal<t65TYPENAME InstructionSet::Opcode3D::InstrType>::magic];
		int IsOpcode3EOk[IsIllegal<t65TYPENAME InstructionSet::Opcode3E::InstrType>::magic];
		int IsOpcode3FOk[IsIllegal<t65TYPENAME InstructionSet::Opcode3F::InstrType>::magic];
		int IsOpcode40Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode40::InstrType>::magic];
		int IsOpcode41Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode41::InstrType>::magic];
		int IsOpcode42Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode42::InstrType>::magic];
		int IsOpcode43Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode43::InstrType>::magic];
		int IsOpcode44Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode44::InstrType>::magic];
		int IsOpcode45Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode45::InstrType>::magic];
		int IsOpcode46Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode46::InstrType>::magic];
		int IsOpcode47Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode47::InstrType>::magic];
		int IsOpcode48Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode48::InstrType>::magic];
		int IsOpcode49Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode49::InstrType>::magic];
		int IsOpcode4AOk[IsIllegal<t65TYPENAME InstructionSet::Opcode4A::InstrType>::magic];
		int IsOpcode4BOk[IsIllegal<t65TYPENAME InstructionSet::Opcode4B::InstrType>::magic];
		int IsOpcode4COk[IsIllegal<t65TYPENAME InstructionSet::Opcode4C::InstrType>::magic];
		int IsOpcode4DOk[IsIllegal<t65TYPENAME InstructionSet::Opcode4D::InstrType>::magic];
		int IsOpcode4EOk[IsIllegal<t65TYPENAME InstructionSet::Opcode4E::InstrType>::magic];
		int IsOpcode4FOk[IsIllegal<t65TYPENAME InstructionSet::Opcode4F::InstrType>::magic];
		int IsOpcode50Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode50::InstrType>::magic];
		int IsOpcode51Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode51::InstrType>::magic];
		int IsOpcode52Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode52::InstrType>::magic];
		int IsOpcode53Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode53::InstrType>::magic];
		int IsOpcode54Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode54::InstrType>::magic];
		int IsOpcode55Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode55::InstrType>::magic];
		int IsOpcode56Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode56::InstrType>::magic];
		int IsOpcode57Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode57::InstrType>::magic];
		int IsOpcode58Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode58::InstrType>::magic];
		int IsOpcode59Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode59::InstrType>::magic];
		int IsOpcode5AOk[IsIllegal<t65TYPENAME InstructionSet::Opcode5A::InstrType>::magic];
		int IsOpcode5BOk[IsIllegal<t65TYPENAME InstructionSet::Opcode5B::InstrType>::magic];
		int IsOpcode5COk[IsIllegal<t65TYPENAME InstructionSet::Opcode5C::InstrType>::magic];
		int IsOpcode5DOk[IsIllegal<t65TYPENAME InstructionSet::Opcode5D::InstrType>::magic];
		int IsOpcode5EOk[IsIllegal<t65TYPENAME InstructionSet::Opcode5E::InstrType>::magic];
		int IsOpcode5FOk[IsIllegal<t65TYPENAME InstructionSet::Opcode5F::InstrType>::magic];
		int IsOpcode60Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode60::InstrType>::magic];
		int IsOpcode61Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode61::InstrType>::magic];
		int IsOpcode62Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode62::InstrType>::magic];
		int IsOpcode63Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode63::InstrType>::magic];
		int IsOpcode64Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode64::InstrType>::magic];
		int IsOpcode65Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode65::InstrType>::magic];
		int IsOpcode66Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode66::InstrType>::magic];
		int IsOpcode67Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode67::InstrType>::magic];
		int IsOpcode68Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode68::InstrType>::magic];
		int IsOpcode69Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode69::InstrType>::magic];
		int IsOpcode6AOk[IsIllegal<t65TYPENAME InstructionSet::Opcode6A::InstrType>::magic];
		int IsOpcode6BOk[IsIllegal<t65TYPENAME InstructionSet::Opcode6B::InstrType>::magic];
		int IsOpcode6COk[IsIllegal<t65TYPENAME InstructionSet::Opcode6C::InstrType>::magic];
		int IsOpcode6DOk[IsIllegal<t65TYPENAME InstructionSet::Opcode6D::InstrType>::magic];
		int IsOpcode6EOk[IsIllegal<t65TYPENAME InstructionSet::Opcode6E::InstrType>::magic];
		int IsOpcode6FOk[IsIllegal<t65TYPENAME InstructionSet::Opcode6F::InstrType>::magic];
		int IsOpcode70Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode70::InstrType>::magic];
		int IsOpcode71Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode71::InstrType>::magic];
		int IsOpcode72Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode72::InstrType>::magic];
		int IsOpcode73Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode73::InstrType>::magic];
		int IsOpcode74Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode74::InstrType>::magic];
		int IsOpcode75Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode75::InstrType>::magic];
		int IsOpcode76Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode76::InstrType>::magic];
		int IsOpcode77Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode77::InstrType>::magic];
		int IsOpcode78Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode78::InstrType>::magic];
		int IsOpcode79Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode79::InstrType>::magic];
		int IsOpcode7AOk[IsIllegal<t65TYPENAME InstructionSet::Opcode7A::InstrType>::magic];
		int IsOpcode7BOk[IsIllegal<t65TYPENAME InstructionSet::Opcode7B::InstrType>::magic];
		int IsOpcode7COk[IsIllegal<t65TYPENAME InstructionSet::Opcode7C::InstrType>::magic];
		int IsOpcode7DOk[IsIllegal<t65TYPENAME InstructionSet::Opcode7D::InstrType>::magic];
		int IsOpcode7EOk[IsIllegal<t65TYPENAME InstructionSet::Opcode7E::InstrType>::magic];
		int IsOpcode7FOk[IsIllegal<t65TYPENAME InstructionSet::Opcode7F::InstrType>::magic];
		int IsOpcode80Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode80::InstrType>::magic];
		int IsOpcode81Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode81::InstrType>::magic];
		int IsOpcode82Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode82::InstrType>::magic];
		int IsOpcode83Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode83::InstrType>::magic];
		int IsOpcode84Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode84::InstrType>::magic];
		int IsOpcode85Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode85::InstrType>::magic];
		int IsOpcode86Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode86::InstrType>::magic];
		int IsOpcode87Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode87::InstrType>::magic];
		int IsOpcode88Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode88::InstrType>::magic];
		int IsOpcode89Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode89::InstrType>::magic];
		int IsOpcode8AOk[IsIllegal<t65TYPENAME InstructionSet::Opcode8A::InstrType>::magic];
		int IsOpcode8BOk[IsIllegal<t65TYPENAME InstructionSet::Opcode8B::InstrType>::magic];
		int IsOpcode8COk[IsIllegal<t65TYPENAME InstructionSet::Opcode8C::InstrType>::magic];
		int IsOpcode8DOk[IsIllegal<t65TYPENAME InstructionSet::Opcode8D::InstrType>::magic];
		int IsOpcode8EOk[IsIllegal<t65TYPENAME InstructionSet::Opcode8E::InstrType>::magic];
		int IsOpcode8FOk[IsIllegal<t65TYPENAME InstructionSet::Opcode8F::InstrType>::magic];
		int IsOpcode90Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode90::InstrType>::magic];
		int IsOpcode91Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode91::InstrType>::magic];
		int IsOpcode92Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode92::InstrType>::magic];
		int IsOpcode93Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode93::InstrType>::magic];
		int IsOpcode94Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode94::InstrType>::magic];
		int IsOpcode95Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode95::InstrType>::magic];
		int IsOpcode96Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode96::InstrType>::magic];
		int IsOpcode97Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode97::InstrType>::magic];
		int IsOpcode98Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode98::InstrType>::magic];
		int IsOpcode99Ok[IsIllegal<t65TYPENAME InstructionSet::Opcode99::InstrType>::magic];
		int IsOpcode9AOk[IsIllegal<t65TYPENAME InstructionSet::Opcode9A::InstrType>::magic];
		int IsOpcode9BOk[IsIllegal<t65TYPENAME InstructionSet::Opcode9B::InstrType>::magic];
		int IsOpcode9COk[IsIllegal<t65TYPENAME InstructionSet::Opcode9C::InstrType>::magic];
		int IsOpcode9DOk[IsIllegal<t65TYPENAME InstructionSet::Opcode9D::InstrType>::magic];
		int IsOpcode9EOk[IsIllegal<t65TYPENAME InstructionSet::Opcode9E::InstrType>::magic];
		int IsOpcode9FOk[IsIllegal<t65TYPENAME InstructionSet::Opcode9F::InstrType>::magic];
		int IsOpcodeA0Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeA0::InstrType>::magic];
		int IsOpcodeA1Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeA1::InstrType>::magic];
		int IsOpcodeA2Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeA2::InstrType>::magic];
		int IsOpcodeA3Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeA3::InstrType>::magic];
		int IsOpcodeA4Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeA4::InstrType>::magic];
		int IsOpcodeA5Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeA5::InstrType>::magic];
		int IsOpcodeA6Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeA6::InstrType>::magic];
		int IsOpcodeA7Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeA7::InstrType>::magic];
		int IsOpcodeA8Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeA8::InstrType>::magic];
		int IsOpcodeA9Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeA9::InstrType>::magic];
		int IsOpcodeAAOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeAA::InstrType>::magic];
		int IsOpcodeABOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeAB::InstrType>::magic];
		int IsOpcodeACOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeAC::InstrType>::magic];
		int IsOpcodeADOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeAD::InstrType>::magic];
		int IsOpcodeAEOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeAE::InstrType>::magic];
		int IsOpcodeAFOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeAF::InstrType>::magic];
		int IsOpcodeB0Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeB0::InstrType>::magic];
		int IsOpcodeB1Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeB1::InstrType>::magic];
		int IsOpcodeB2Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeB2::InstrType>::magic];
		int IsOpcodeB3Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeB3::InstrType>::magic];
		int IsOpcodeB4Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeB4::InstrType>::magic];
		int IsOpcodeB5Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeB5::InstrType>::magic];
		int IsOpcodeB6Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeB6::InstrType>::magic];
		int IsOpcodeB7Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeB7::InstrType>::magic];
		int IsOpcodeB8Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeB8::InstrType>::magic];
		int IsOpcodeB9Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeB9::InstrType>::magic];
		int IsOpcodeBAOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeBA::InstrType>::magic];
		int IsOpcodeBBOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeBB::InstrType>::magic];
		int IsOpcodeBCOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeBC::InstrType>::magic];
		int IsOpcodeBDOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeBD::InstrType>::magic];
		int IsOpcodeBEOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeBE::InstrType>::magic];
		int IsOpcodeBFOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeBF::InstrType>::magic];
		int IsOpcodeC0Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeC0::InstrType>::magic];
		int IsOpcodeC1Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeC1::InstrType>::magic];
		int IsOpcodeC2Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeC2::InstrType>::magic];
		int IsOpcodeC3Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeC3::InstrType>::magic];
		int IsOpcodeC4Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeC4::InstrType>::magic];
		int IsOpcodeC5Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeC5::InstrType>::magic];
		int IsOpcodeC6Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeC6::InstrType>::magic];
		int IsOpcodeC7Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeC7::InstrType>::magic];
		int IsOpcodeC8Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeC8::InstrType>::magic];
		int IsOpcodeC9Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeC9::InstrType>::magic];
		int IsOpcodeCAOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeCA::InstrType>::magic];
		int IsOpcodeCBOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeCB::InstrType>::magic];
		int IsOpcodeCCOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeCC::InstrType>::magic];
		int IsOpcodeCDOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeCD::InstrType>::magic];
		int IsOpcodeCEOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeCE::InstrType>::magic];
		int IsOpcodeCFOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeCF::InstrType>::magic];
		int IsOpcodeD0Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeD0::InstrType>::magic];
		int IsOpcodeD1Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeD1::InstrType>::magic];
		int IsOpcodeD2Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeD2::InstrType>::magic];
		int IsOpcodeD3Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeD3::InstrType>::magic];
		int IsOpcodeD4Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeD4::InstrType>::magic];
		int IsOpcodeD5Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeD5::InstrType>::magic];
		int IsOpcodeD6Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeD6::InstrType>::magic];
		int IsOpcodeD7Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeD7::InstrType>::magic];
		int IsOpcodeD8Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeD8::InstrType>::magic];
		int IsOpcodeD9Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeD9::InstrType>::magic];
		int IsOpcodeDAOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeDA::InstrType>::magic];
		int IsOpcodeDBOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeDB::InstrType>::magic];
		int IsOpcodeDCOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeDC::InstrType>::magic];
		int IsOpcodeDDOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeDD::InstrType>::magic];
		int IsOpcodeDEOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeDE::InstrType>::magic];
		int IsOpcodeDFOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeDF::InstrType>::magic];
		int IsOpcodeE0Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeE0::InstrType>::magic];
		int IsOpcodeE1Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeE1::InstrType>::magic];
		int IsOpcodeE2Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeE2::InstrType>::magic];
		int IsOpcodeE3Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeE3::InstrType>::magic];
		int IsOpcodeE4Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeE4::InstrType>::magic];
		int IsOpcodeE5Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeE5::InstrType>::magic];
		int IsOpcodeE6Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeE6::InstrType>::magic];
		int IsOpcodeE7Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeE7::InstrType>::magic];
		int IsOpcodeE8Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeE8::InstrType>::magic];
		int IsOpcodeE9Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeE9::InstrType>::magic];
		int IsOpcodeEAOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeEA::InstrType>::magic];
		int IsOpcodeEBOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeEB::InstrType>::magic];
		int IsOpcodeECOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeEC::InstrType>::magic];
		int IsOpcodeEDOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeED::InstrType>::magic];
		int IsOpcodeEEOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeEE::InstrType>::magic];
		int IsOpcodeEFOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeEF::InstrType>::magic];
		int IsOpcodeF0Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeF0::InstrType>::magic];
		int IsOpcodeF1Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeF1::InstrType>::magic];
		int IsOpcodeF2Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeF2::InstrType>::magic];
		int IsOpcodeF3Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeF3::InstrType>::magic];
		int IsOpcodeF4Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeF4::InstrType>::magic];
		int IsOpcodeF5Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeF5::InstrType>::magic];
		int IsOpcodeF6Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeF6::InstrType>::magic];
		int IsOpcodeF7Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeF7::InstrType>::magic];
		int IsOpcodeF8Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeF8::InstrType>::magic];
		int IsOpcodeF9Ok[IsIllegal<t65TYPENAME InstructionSet::OpcodeF9::InstrType>::magic];
		int IsOpcodeFAOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeFA::InstrType>::magic];
		int IsOpcodeFBOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeFB::InstrType>::magic];
		int IsOpcodeFCOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeFC::InstrType>::magic];
		int IsOpcodeFDOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeFD::InstrType>::magic];
		int IsOpcodeFEOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeFE::InstrType>::magic];
		int IsOpcodeFFOk[IsIllegal<t65TYPENAME InstructionSet::OpcodeFF::InstrType>::magic];
#endif
	};
}

#endif
