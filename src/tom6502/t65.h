#ifndef H_6502TYPES_H_
#define H_6502TYPES_H_

#include "t65Defs.h"

#define t65CPU_MANAGES_I_FLAG

namespace t65 {
	//////////////////////////////////////////////////////////////////////////
	// NV-BDIZC
	//
	// masks for 6502 status register bytes.
	enum {
		N_MASK=128,		// Negative bit
		V_MASK=64,		// Overflow bit
		U_MASK=32,		// Unused bit (always set)
		B_MASK=16,		// BRK bit (always set, except when P stacked for
		// processing maskable IRQ)
		D_MASK=8,		// Decimal mode bit
		I_MASK=4,		// Interrupt disable bit
		Z_MASK=2,		// Zero bit
		C_MASK=1,		// Carry bit
	};
	
	struct State65xx {
		byte a,x,y,p;
		Word pc,s;
	};
	
	struct AddressingModesText {
		const char *pre,*post;
		int size;
	};
	
	extern AddressingModesText addrmodestext[];
}

#endif
