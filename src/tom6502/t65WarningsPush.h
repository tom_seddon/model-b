#ifdef _MSC_VER

#pragma warning(push)

//'+=' : conversion from 'int' to 't65::byte', possible loss of data
//
// crops up a lot in code like this:
//
//	t65::word a,b;
//	a+=b;
//
#pragma warning(disable:4244)

// warning C4100: 'state' : unreferenced formal parameter
//
// seems to occur all over the place, though it's not clear why as the variable
// 'state' is always used.
#pragma warning(disable:4100)

#endif//_MSC_VER
