#ifndef TOM6502DEFS_H_
#define TOM6502DEFS_H_

#include "../port.h"

//////////////////////////////////////////////////////////////////////////
// How to say "this WILL be inline"
#if (defined _MSC_VER)
#define FINLINE 
//__forceinline//13/3/04 let the compiler decide!
#else
#define FINLINE inline
#endif

//////////////////////////////////////////////////////////////////////////

// legacy
#define t65TYPENAME typename

// VC++ only
#ifdef _MSC_VER
#define t65_CALL __fastcall
#else
#define t65_CALL
#endif

namespace t65 {
	typedef signed char int8;
	typedef unsigned char byte;
	
	typedef signed short int16;
	typedef unsigned short word;
	
	typedef signed long int32;
	typedef unsigned long dword;

	typedef int64_t int64;
	typedef uint64_t qword;
	
	//no constructor for this, because I want the compiler to know it's 100% a
	//pod.
	union Word {
		word w;
		struct {
#ifdef t65_WRONG_ENDIAN
			byte h,l;
#else
			byte l,h;
#endif
		};
	};

	inline Word MakeWord(word w) {
		Word r;
		r.w=w;
		return r;
	}

	inline Word MakeWord(byte l,byte h) {
		Word r;
		r.l=l;
		r.h=h;
		return r;
	}
}

#endif
