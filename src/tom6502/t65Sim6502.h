#ifndef T65_6502_HELPER_H_
#define T65_6502_HELPER_H_

#include "t65.h"
#include "t65Generic6502.h"
#include "t65Disassembler6502.h"

namespace t65 {
	//Brings together CPU type and instruction set in an easier-to-use fashion.
	template<class tCPUType,class tInstrSetType>
	struct Sim6502 {
		typedef tCPUType CPUType;
		typedef tInstrSetType InstrSetType;
		typedef t65TYPENAME tCPUType::MachineType MachineType;
		typedef t65TYPENAME MachineType::StateType StateType;
		typedef Disassembler6502<tInstrSetType> DisassemblerType;

		//Pointer to the run function. Allows a table of functions effectively polymorphic 
		//over cpu and instruction set type, though not machine type. (So, can use several
		//different CPU types, provided they are all templated on the same MachineType.)
		//for example,
		//
		//static const Run6502Fn run_fns[]={
		//	&Sim6502<Sy6502A<IOP>,Sy6502AInstrSet<Sy6502A<IOP> > >::Run6502,
		//	&Sim6502<R65C102<IOP>,R65C102InstrSet<R65C102<IOP> > >::Run6502,
		//};
		//
		//and so on. then pick the desired one like (*run_fns[cur_cpu])(state,num_cycles) blah blah.

		typedef int (*Run6502Fn)(MachineType &state,int);

		static FINLINE void Init() {
			CPUType::Init();
		}

		//5/12/2004 -- removing InitialiseCpuState (suspect real 6502 starts up with gibberish)
//		static FINLINE void InitialiseCpuState(MachineType &state) {
//			CPUType::InitialiseCPUState(state);
//		}

		static FINLINE void Reset6502(MachineType &state) {
			CPUType::Reset(state);
		}
		
		static FINLINE int Run6502(MachineType &state,int maxcycles) {
			return CPUType::Run6502(state,maxcycles,InstrSetType());
		}

		static FINLINE void t65_CALL RunSingleInstruction(MachineType &state) {
			CPUType::RunSingleInstruction(state,InstrSetType());
		}

		struct ByteReader {
			MachineType *state;
			FINLINE byte operator()(Word w) {
				return state->ReadByte(w,CPUType::catReadDebug);
			}
		};

		static FINLINE Word Disassemble(MachineType &state,Word base,char *opcode_buf,char *operand_buf,byte *bytes_buf) {
			ByteReader byte_reader;
			byte_reader.state=&state;
			return DisassemblerType::Disassemble(byte_reader,base,opcode_buf,operand_buf,bytes_buf);
		}
	};
}

#endif
