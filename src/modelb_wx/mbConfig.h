#ifndef mbCONFIG_H_
#define mbCONFIG_H_

//#include <bbcComputer.h>
#include "mbKeyMap.h"
//#include <Host.h>
#include "mbConfigFile.h"
#include <map>

enum mbMonitorType {
	mbMONITOR_COLOUR=0,
	mbMONITOR_BNW,
	mbMONITOR_AMBER,
	mbMONITOR_GREEN,

	mbMONITOR_NUM_TYPES,
};

enum mbScanlinesMode {
	mbSCANLINES_SINGLE,
	mbSCANLINES_INTERLACE,
	mbSCANLINES_DOUBLE,
};

// (std::string for proposed removal of dependency on wxwidgets)

//////////////////////////////////////////////////////////////////////////
// Profiles

struct mbProfileRom {
	bool override;
	std::string filename;
	bool ram;

	mbProfileRom();
};

struct mbProfile {
	std::string parent;
	std::string name;
	std::string model;
	std::string fdc;
	std::string os;
	mbProfileRom roms[16];

	void ResetToDefaults();
	void InheritSettingsFrom(const mbProfile &parent);
	void Load(const char *section,mbConfigFile *cfg);
	void Save(const char *section,mbConfigFile *cfg) const;
};

struct mbProfilesConfig {
	std::vector<mbProfile> profiles;
	std::string current;

	mbProfile *ProfileByName(const std::string &name);
	bool GetFullProfileByName(const std::string &name,mbProfile *dest);
	void Load(mbConfigFile *cfg);
	void Save(mbConfigFile *cfg) const;
};

//////////////////////////////////////////////////////////////////////////
// ROM configuration
struct mbRomsConfig {
	//Configuration for a ROM slot
	struct Rom {
		std::string filename;
		bool ram;

		Rom();
	};
	struct BaseModelRomSet {
		std::string os;
		Rom roms[16];
		int fs_slot;
		std::string disc_interface;//No, this doesn't really belong here.

		BaseModelRomSet();
	};
	wxRect dlg_rect;
	std::map<std::string,std::string> fs_roms;
	typedef std::map<std::string,BaseModelRomSet> RomSets;
	RomSets rom_sets;

	mbRomsConfig();
	void Load(mbConfigFile *cfg);
	void Save(mbConfigFile *cfg) const;
private:
//	void LoadRom(mbConfigFile *cfg,const std::string &key,Rom *rom);
//	void SaveRom(mbConfigFile *cfg,const std::string &key,const Rom *rom) const;
};

//////////////////////////////////////////////////////////////////////////
// Sound configuration
struct mbSoundConfig {
	bool enabled;
	bool stereo;
	int hz,bits;
	int len_frames;
	bool is_stereo_left[4],is_stereo_right[4];
	wxRect dlg_rect;
	int volume;

	mbSoundConfig();
	
	void Load(mbConfigFile *cfg);
	void Save(mbConfigFile *cfg) const;
};

//////////////////////////////////////////////////////////////////////////
// Keyboard configuration
struct mbKeyboardConfig {
	bool keylinks[8];
	mbKeymap::C keymaps;
	std::string selected_keymap;
	wxRect dlg_rect;

	mbKeyboardConfig();
	
	void Load(mbConfigFile *cfg);
	void Save(mbConfigFile *cfg) const;
};

//////////////////////////////////////////////////////////////////////////
//
struct mbVideoConfig {
	static const int max_frame_skip;//min is 1, of course

	int width,height;
	int frame_skip;
	int cleanup_frequency;
	bool full_screen;
	int full_screen_refresh_rate;
	bool full_screen_vsync;
	std::string full_screen_adapter;
	mbScanlinesMode full_screen_scanlines;
	mbScanlinesMode windowed_scanlines;
	mbMonitorType monitor_type;
	wxRect dlg_rect;
	
	mbVideoConfig();
	
	void Load(mbConfigFile *cfg);
	void Save(mbConfigFile *cfg) const;
};

//////////////////////////////////////////////////////////////////////////
//
struct mbMiscConfig {
	enum {
		max_recent_discs=25,
	};
	bool limit_speed;
//	std::string disc_interface;
	bool show_status_bar;
	bool show_menu;
	bool fast_forward_disc_access;
	std::string model;
	wxRect window_rect;
	bool window_is_maximized;
	wxRect hardware_dlg_rect;//hardly the best place for it, but I suppose it kind of goes with the model and disc interface settings.
	std::vector<std::string> recent_discs;

	mbMiscConfig();
	
	void Load(mbConfigFile *cfg);
	void Save(mbConfigFile *cfg) const;

	void AddRecentDisc(const std::string &filename);
};

//////////////////////////////////////////////////////////////////////////
//
struct mbQuickstartConfig {
	wxRect dialog_pos;
	std::vector<std::string> dirs;
	
	mbQuickstartConfig();

	void Load(mbConfigFile *cfg);
	void Save(mbConfigFile *cfg) const;
};

//////////////////////////////////////////////////////////////////////////
//
struct mbJoysticksConfig {
	enum Axis {
		X,Y,Z,RX,RY,RZ,INVALID_AXIS,
	};
//	static float HostInp_JoyState::*const hostinp_axes[6];

	struct BeebStick {
		std::string device_name;
		bool enabled;
		//int joystick_index;
		Axis x_axis;
		Axis y_axis;
		int button;

		BeebStick();
	};

	BeebStick joysticks[2];

	mbJoysticksConfig();

	void Load(mbConfigFile *cfg);
	void Save(mbConfigFile *cfg) const;
};

//////////////////////////////////////////////////////////////////////////
//
struct mbCmosConfig {
	t65::byte contents[50];//#include optimisation //bbcComputer::cmos_size];

	mbCmosConfig();

	void Load(mbConfigFile *cfg);
	void Save(mbConfigFile *cfg) const;
};

//////////////////////////////////////////////////////////////////////////
//
struct mbDebugConfig {
	bool show_visual_profile;

	mbDebugConfig();

	void Load(mbConfigFile *cfg);
	void Save(mbConfigFile *cfg) const;
};

//////////////////////////////////////////////////////////////////////////
//
struct mbFileDlgConfig {
	std::string default_folder;
	int filter_idx;

	mbFileDlgConfig();
	mbFileDlgConfig(const char *default_folder,int filter_idx);
};

//////////////////////////////////////////////////////////////////////////
//
struct mbConfig {
	mbRomsConfig roms;
	mbSoundConfig sound;
	mbKeyboardConfig keyboard;
	mbVideoConfig video;
	mbMiscConfig misc;
	mbQuickstartConfig quickstart;
	mbJoysticksConfig joysticks;
	mbCmosConfig cmos;
	mbProfilesConfig profiles;
	mbDebugConfig debug;
	std::map<std::string,mbFileDlgConfig> file_dlgs;

	std::string file_name;
	bool read_only;
	
	mbConfig();
	void SetFilename(const std::string &config_file_name,bool is_read_only);
	void Load(const std::string &config_file_name);
	void Save() const;
	void GetConfig(mbConfigFile *cfg) const;
};

#endif
