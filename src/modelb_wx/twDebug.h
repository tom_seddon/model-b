#ifndef TOMDEBUG_H_
#define TOMDEBUG_H_

#define twIMAGEHLP_OK

//In a debug build, an improved version of new is installed. This supplies
//correct (hopefully) line/file info to the debug library, and enables the
//dumping of memory leaks on exit.

//If true, symbols will be loaded from the program's debug info. This will
//enable line/file info in the debug output. If there are lots of symbols,
//this can take a while.
//If false, no symbols will be loaded, and all allocations will be marked
//as if not found in the debug info. They will still be dumped out,
//however.
extern const bool tw_track_heap_allocations;

//Each unknown allocation is assigned a number. This is a count of how
//many unknown allocations have been performed so far. If this value
//is positive, the program will halt on the corresponding unknown
//allocation, to aid in tracking them down.
//
//Set to a negative number to prevent any breaks.
extern const int tw_unknown_allocation_break;//or -ve for no break.

void twVerifyAllocations();
long twGetTotalNumAllocations();
	
#endif
