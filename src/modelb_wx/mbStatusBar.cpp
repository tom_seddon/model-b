#include "pch.h"
#include "mbStatusBar.h"
#include "mbMisc.h"

DEFINE_LOCAL_EVENT_TYPE(mbEVT_VOLUME)
DEFINE_LOCAL_EVENT_TYPE(mbEVT_DROPFILE)

const wxWindowID id_volume=mbNewId();

enum {
	mhz_panel=0,

#ifdef bbcDEBUG_ENABLE
	pc_panel,
#endif
	size_panel,
	vol_panel,

	num_panels,

	num_panels_total=num_panels+bbcFdd::num_drives,
};

#if wxUSE_DRAG_AND_DROP
mbStatusBar::mbFileDropTarget::mbFileDropTarget(mbStatusBar *owner):
owner_(owner)
{
}

wxDragResult mbStatusBar::mbFileDropTarget::OnDragOver(wxCoord x,wxCoord y,wxDragResult def)
{
	(void)def;

	bool ok=this->DriveFromPoint(x,y)>=0;
	return ok?wxDragCopy:wxDragNone;
}

int mbStatusBar::mbFileDropTarget::DriveFromPoint(wxCoord x,wxCoord y) {
	int i;

	for(i=0;i<bbcFdd::num_drives;++i) {
		if(owner_->drive_states_[i].rect.Contains(wxPoint(x,y))) {
			break;
		}
	}
	if(i==bbcFdd::num_drives) {
		return -1;
	}
	if(bbcFdd::DriveStatus(i)==bbcFdd::DRIVE_SIDE2) {
		return -1;
	}
	return i;
}

bool mbStatusBar::mbFileDropTarget::OnDropFiles(wxCoord x,wxCoord y,
	const wxArrayString &filenames)
{
	int drive=this->DriveFromPoint(x,y);
	if(filenames.Count()!=1||drive<0) {
		return false;
	}

	wxCommandEvent event(mbEVT_DROPFILE,owner_->GetId());
	event.SetInt(drive);
	event.SetString(filenames[0]);
	//printf("DND: drive %d: \"%s\"\n",event.m_commandInt,event.m_commandString.c_str());
	owner_->ProcessEvent(event);
	return true;
}
#endif//wxUSE_DRAG_AND_DROP

mbStatusBar::DriveState::DriveState():
action(bbc1770::ACTION_IDLE),
track(0),
sector(0),
rect(0,0,0,0)
{
}

mbStatusBar::mbStatusBar(wxWindow *parent,int id):
wxStatusBar(parent,id,0)//,wxST_SIZEGRIP)
{
	volume_=new wxSlider(this,id_volume,0,0,100);
	this->SetVolumeEnabled(false);
#if wxUSE_DRAG_AND_DROP
	this->SetDropTarget(new mbFileDropTarget(this));
#endif
}

void mbStatusBar::SetScreenSize(int width,int height) {
	this->SetStatusText(wxString::Format("%dx%d",width,height),size_panel);
}

void mbStatusBar::SetMhz(float mhz) {
	this->SetStatusText(wxString::Format("%.2fMHz",mhz),mhz_panel);
}

void mbStatusBar::SetFdcStatus(bbc1770::FdcAction action,int drive,int track,
	int sector)
{
	const char *action_name="unknown";
	switch(action) {
	case bbc1770::ACTION_IDLE:
		action_name="-";
		break;
	case bbc1770::ACTION_OTHER:
		action_name="A";
		break;
	case bbc1770::ACTION_READ:
		action_name="R";
		break;
	case bbc1770::ACTION_WRITE:
		action_name="W";
		break;
	}
	for(int i=0;i<4;++i) {
		int field=num_panels+i;
		if(i!=drive||action==bbc1770::ACTION_IDLE) {
			this->SetStatusText(wxString::Format("%d: idle",i),field);
		} else {
			this->SetStatusText(
				wxString::Format("%d: %s T%d S%d",i,action_name,track,sector),
				field);
		}
	}
}

void mbStatusBar::OnSize(wxSizeEvent &event) {
	int widths[num_panels+bbcFdd::num_drives];
	int w=event.GetSize().GetWidth();
	int third=w/3;
	int rest=w-third;
	wxRect r;
	int i;

	for(i=0;i<num_panels;++i) {
		widths[i]=third/num_panels;
	}

	for(i=0;i<bbcFdd::num_drives;++i) {
		widths[num_panels+i]=rest/bbcFdd::num_drives;
	}
	wxASSERT(unsigned(i)<sizeof widths/sizeof widths[0]);

	this->SetFieldsCount(num_panels_total);
	this->SetStatusWidths(num_panels_total,widths);

	this->GetFieldRect(vol_panel,r);
	volume_->SetSize(r);

	for(i=0;i<bbcFdd::num_drives;++i) {
		this->GetFieldRect(num_panels+i,drive_states_[i].rect);
	}
}

void mbStatusBar::OnVolume(wxScrollEvent &event) {
	(void)event;

	wxCommandEvent evt(mbEVT_VOLUME,this->GetId());
	evt.SetExtraLong(volume_->GetValue());
//	wxLogDebug("mbStatusBar::OnVolume: value=%ld\n",evt.m_extraLong);
	this->ProcessEvent(evt);//AddPendingEvent(evt);
}

void mbStatusBar::SetVolume(int volume) {
	wxASSERT(volume>=0&&volume<=100);
	volume_->SetValue(volume);
}

void mbStatusBar::SetVolumeEnabled(bool enabled) {
	volume_->Enable(enabled);
}

#ifdef bbcDEBUG_ENABLE
void mbStatusBar::SetPc(t65::Word pc) {
	this->SetStatusText(wxString::Format("@ %04X",pc.w),pc_panel);
}
#endif

BEGIN_EVENT_TABLE(mbStatusBar,wxStatusBar)
	EVT_SIZE(mbStatusBar::OnSize)
	EVT_COMMAND_SCROLL(id_volume,mbStatusBar::OnVolume)
END_EVENT_TABLE()
