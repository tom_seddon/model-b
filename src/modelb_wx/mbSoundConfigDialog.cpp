#include "pch.h"
#include "mbSoundConfigDialog.h"
#include "mbMisc.h"
#include <wx/valgen.h>

const wxWindowID id_stereo_left_base=mbNewIds(4);
const wxWindowID id_stereo_right_base=mbNewIds(4);
const wxWindowID id_base=mbNewId();
const wxWindowID id_enabled=mbNewId();
const wxWindowID id_stereo=mbNewId();
const wxWindowID id_bits=mbNewId();
const wxWindowID id_freqs=mbNewId();
const wxWindowID id_ok=mbNewId();

static const int border_size=6;

mbSoundConfigDialog::mbSoundConfigDialog(wxWindow *parent,const mbSoundConfig &cfg):
wxDialog(parent,-1,"Configure Sound",wxDefaultPosition,wxDefaultSize,
	wxDEFAULT_DIALOG_STYLE),//|wxRESIZE_BORDER),
cfg_(cfg),
bits_cb_(0),
freqs_cb_(0)
{
	this->SetExtraStyle(this->GetExtraStyle()|wxWS_EX_VALIDATE_RECURSIVELY);
	const unsigned tlr_gaps=wxTOP|wxLEFT|wxRIGHT;
	bits_.push_back(8);
	bits_.push_back(16);

	freqs_.push_back(8000);
	freqs_.push_back(11025);
	freqs_.push_back(22050);
	freqs_.push_back(44100);
	freqs_.push_back(48000);

	wxBoxSizer *all_sz=new wxBoxSizer(wxVERTICAL);

	//Sound
	wxStaticBox *sound_box=new wxStaticBox(this,-1,"Sound");
	wxStaticBoxSizer *sound_sz=new wxStaticBoxSizer(sound_box,wxVERTICAL);
	
	wxCheckBox *enabled=new wxCheckBox(this,id_enabled,"&Enabled");
	enabled->SetValidator(wxGenericValidator(&cfg_.enabled));

	wxCheckBox *stereo=new wxCheckBox(this,id_stereo,"&Stereo");
	stereo->SetValidator(wxGenericValidator(&cfg_.stereo));

	wxBoxSizer *bits_sz=new wxBoxSizer(wxHORIZONTAL);
	wxStaticText *bits_tx=new wxStaticText(this,-1,"&Bits");
	bits_cb_=new wxComboBox(this,id_bits,"",wxDefaultPosition,wxDefaultSize,
		0,0,wxCB_DROPDOWN|wxCB_READONLY);
	this->FillCombobox(bits_,bits_cb_);
	this->SetComboboxSelection(bits_cb_,cfg_.bits);
	bits_sz->Add(bits_tx,1,tlr_gaps|wxGROW,border_size);
	bits_sz->Add(bits_cb_,2,tlr_gaps|wxGROW,border_size);
	
	wxBoxSizer *freqs_sz=new wxBoxSizer(wxHORIZONTAL);
	wxStaticText *freqs_tx=new wxStaticText(this,-1,"&Frequency");
	freqs_cb_=new wxComboBox(this,id_freqs,"",wxDefaultPosition,wxDefaultSize,
		0,0,wxCB_DROPDOWN|wxCB_READONLY);
	this->FillCombobox(freqs_,freqs_cb_);
	this->SetComboboxSelection(freqs_cb_,cfg_.hz);
	freqs_sz->Add(freqs_tx,1,tlr_gaps|wxGROW,border_size);
	freqs_sz->Add(freqs_cb_,2,tlr_gaps|wxGROW,border_size);
	
	sound_sz->Add(enabled,0,wxLEFT|wxRIGHT|wxTOP,border_size);
	sound_sz->Add(stereo,0,tlr_gaps,border_size);
	sound_sz->Add(bits_sz,0,0);
	sound_sz->Add(freqs_sz,0,0);

	sound_controls_.push_back(stereo);
	sound_controls_.push_back(bits_tx);
	sound_controls_.push_back(bits_cb_);
	sound_controls_.push_back(freqs_tx);
	sound_controls_.push_back(freqs_cb_);
	
	//Stereo
	wxStaticBox *stereo_box=new wxStaticBox(this,-1,"Stereo");
	wxStaticBoxSizer *stereo_sz=new wxStaticBoxSizer(stereo_box,wxVERTICAL);
	stereo_controls_.push_back(stereo_box);
	for(unsigned i=0;i<4;++i) {
		wxString label=i<3?wxString::Format("Tone %u",i):wxString("Noise");
		wxStaticText *text=new wxStaticText(this,-1,label);
		wxCheckBox *left_check=new wxCheckBox(this,id_stereo_left_base+i,"Left");
		wxCheckBox *right_check=new wxCheckBox(this,id_stereo_right_base+i,"Right");

		left_check->SetValidator(wxGenericValidator(&cfg_.is_stereo_left[i]));
		right_check->SetValidator(wxGenericValidator(&cfg_.is_stereo_right[i]));

		wxBoxSizer *channel_sz=new wxBoxSizer(wxHORIZONTAL);
		channel_sz->Add(text,1,wxGROW|tlr_gaps,border_size);
		channel_sz->Add(left_check,1,wxGROW|tlr_gaps,border_size);
		channel_sz->Add(right_check,1,wxGROW|tlr_gaps,border_size);

		stereo_sz->Add(channel_sz,0,0);

		stereo_controls_.push_back(text);
		stereo_controls_.push_back(left_check);
		stereo_controls_.push_back(right_check);
	}

	//Buttons
	wxButton *ok=new wxButton(this,id_ok,"OK");
	wxButton *cancel=new wxButton(this,wxID_CANCEL,"Cancel");
		
	ok->SetDefault();
		
	wxBoxSizer *btns_sz=new wxBoxSizer(wxHORIZONTAL);
	btns_sz->Add(ok,0,wxALL|wxALIGN_RIGHT,border_size);
	btns_sz->Add(cancel,0,wxALL|wxALIGN_RIGHT,border_size);
	
	//Put it all together
	all_sz->Add(sound_sz,1,wxGROW|wxALL,border_size);
	all_sz->Add(stereo_sz,1,wxGROW|wxALL,border_size);
	all_sz->Add(btns_sz,0,wxALIGN_RIGHT);

	this->SetSizer(all_sz);
	all_sz->SetSizeHints(this);

	this->TransferDataToWindow();
	this->RefreshDialog();

	mbSetWindowPos(this,cfg_.dlg_rect.GetTopLeft());//this->Move(cfg_.dlg_rect.GetLeft(),cfg_.dlg_rect.GetTop());
}

//What a crock, wxComboBox::Append is non-const and it's a void * grrr...
//Make sure the given vector doesn't move about :)
void mbSoundConfigDialog::FillCombobox(std::vector<int> &values,wxComboBox *combo) 
	const
{
	combo->Clear();
	for(unsigned i=0;i<values.size();++i) {
		combo->Append(wxString::Format("%d",values[i]),&values[i]);
	}
}

void mbSoundConfigDialog::SetComboboxSelection(wxComboBox *combo,int value) {
	int closest_delta=INT_MAX;
	int closest=-1;

	for(int i=0;(unsigned)i<combo->GetCount();++i) {
		int *data=static_cast<int *>(combo->GetClientData(i));

		if(data) {
			int this_delta=abs(value-*data);
			if(this_delta<closest_delta) {
				closest_delta=this_delta;
				closest=i;
			}
		}
	}
	if(closest<0) {
		closest=0;
	}
	combo->SetSelection(closest);
}

void mbSoundConfigDialog::DoRefreshDialog(wxCommandEvent &) {
	this->RefreshDialog();
}

void mbSoundConfigDialog::RefreshDialog() {
	unsigned i;

	this->TransferDataFromWindow();
	for(i=0;i<stereo_controls_.size();++i) {
		stereo_controls_[i]->Enable(cfg_.enabled);//&&cfg_.stereo);
	}
	for(i=0;i<sound_controls_.size();++i) {
		sound_controls_[i]->Enable(cfg_.enabled);
	}
}

void mbSoundConfigDialog::OnOk(wxCommandEvent &) {
	cfg_.dlg_rect=this->GetRect();
	this->TransferDataFromWindow();
	int bits_sel=bits_cb_->GetSelection();
	int freqs_sel=freqs_cb_->GetSelection();

	if(bits_sel>=0) {
		wxASSERT(bits_cb_->GetClientData(bits_sel));
		cfg_.bits=*static_cast<int *>(bits_cb_->GetClientData(bits_sel));
	}
	if(freqs_sel>=0) {
		wxASSERT(freqs_cb_->GetClientData(freqs_sel));
		cfg_.hz=*static_cast<int *>(freqs_cb_->GetClientData(freqs_sel));
	}
	this->EndModal(wxID_OK);
}

void mbSoundConfigDialog::GetResult(mbSoundConfig *cfg) const {
	wxASSERT(cfg);
	*cfg=cfg_;
}

BEGIN_EVENT_TABLE(mbSoundConfigDialog,wxDialog)
	EVT_CHECKBOX(id_stereo,mbSoundConfigDialog::DoRefreshDialog)
	EVT_CHECKBOX(id_enabled,mbSoundConfigDialog::DoRefreshDialog)
	EVT_BUTTON(id_ok,mbSoundConfigDialog::OnOk)
END_EVENT_TABLE()
