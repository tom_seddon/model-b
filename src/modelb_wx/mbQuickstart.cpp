#include "pch.h"
#include "mbQuickstart.h"
#include "mbConfig.h"
#include "mbMisc.h"
#include "mbDiscFormat.h"
#include <wx/progdlg.h>
#include <algorithm>
#include <base/log.h>

//////////////////////////////////////////////////////////////////////////

B_LOG_DEF(QStart);

//////////////////////////////////////////////////////////////////////////

const wxWindowID id_base=mbNewId();
const wxWindowID id_ok=mbNewId();
const wxWindowID id_games_list=mbNewId();
const wxWindowID id_add=mbNewId();
const wxWindowID id_remove=mbNewId();

//////////////////////////////////////////////////////////////////////////
// mbQuickstartResult
//
// Result of a Quickstart operation.
//
// this->file describes the selected file;
// this->allow_save indicates whether 'Save' should be allowed for this disc
// image.
mbQuickstartResult::mbQuickstartResult():
allow_save(false)
{
}

//////////////////////////////////////////////////////////////////////////
// Quickstart Dialog
//
// Lists games, allows user to select one a la MAME.
class QuickstartDialog:
public wxDialog
{
public:
	QuickstartDialog(wxWindow *parent,const std::vector<std::string> &dirs);
	~QuickstartDialog();

	bool GetResult(mbQuickstartResult *result) const;
	void GetDirs(std::vector<std::string> *dirs) const;
protected:
private:
	bool result_valid_;
	mbQuickstartResult result_;
	wxListCtrl *games_list_;
	std::vector<wxString> dirs_;

	struct Game {
		Game(const wxFileName &name);

		const wxFileName name;
		const wxString name_str;
		const wxString ext_lc;
		bool is_ds;
		wxFileName side2name;
		wxString display_name;

		//typedef int (wxCALLBACK *wxListCtrlCompare)(long item1, long item2, long sortData);
		static int wxCALLBACK ListCtrlCompare(long i1,long i2,long sort_data);
	private:
		Game(const Game &);
		Game &operator=(const Game &);
	};

	std::vector<Game *> games_;

	void GetAllGamesRecursive(const wxFileName &root,bool is_first_time);
	void RefreshGames();
	void RefreshGamesList();
	void ResetGames();
	void SetGamesListStyle(int flags);

	void OnAdd(wxCommandEvent &event);
	void OnRemove(wxCommandEvent &event);
	void OnOk(wxCommandEvent &event);
	void OnListItemActivated(wxListEvent &event);

	DECLARE_EVENT_TABLE()
};

//////////////////////////////////////////////////////////////////////////
//
QuickstartDialog::Game::Game(const wxFileName &name_in):
name(name_in),
name_str(name_in.GetName()),
ext_lc(name_in.GetExt().Lower()),
is_ds(false),
display_name(name_in.GetName())
{
}


//////////////////////////////////////////////////////////////////////////
// Compares two items in the games list.
//
// Their Data items are Game *.
int wxCALLBACK QuickstartDialog::Game::ListCtrlCompare(long i1,long i2,long) {
	const Game *lhs=reinterpret_cast<const Game *>(i1);
	const Game *rhs=reinterpret_cast<const Game *>(i2);

	wxASSERT(lhs&&rhs);
	return lhs->display_name.CmpNoCase(rhs->display_name);
}

//////////////////////////////////////////////////////////////////////////
//
QuickstartDialog::QuickstartDialog(wxWindow *parent,const std::vector<std::string> &dirs):
wxDialog(parent,-1,"Quickstart",wxDefaultPosition,wxDefaultSize,wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER),
result_valid_(false),
games_list_(0)
{
	dirs_.reserve(dirs.size());
	for(unsigned i=0;i<dirs.size();++i) {
		dirs_.push_back(dirs[i].c_str());
	}

	wxStaticBox *static_box=new wxStaticBox(this,-1,"Available &games");
	wxStaticBoxSizer *game_col=new wxStaticBoxSizer(static_box,wxVERTICAL);
	wxBoxSizer *both_col=new wxBoxSizer(wxVERTICAL);
	wxBoxSizer *buttons_row=new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer *game_buttons_row=new wxBoxSizer(wxHORIZONTAL);
	static const int border_size=5;
	
	//list and statics
	games_list_=new wxListCtrl(this,id_games_list,wxDefaultPosition,wxDefaultSize,
		wxLC_LIST|wxLC_SINGLE_SEL);//SMALL_ICON);
	games_list_->SetColumnWidth(-1,wxLIST_AUTOSIZE);
	game_col->Add(games_list_,1,wxEXPAND|wxALL,border_size);
	
	game_buttons_row->Add(new wxButton(this,id_add,"&Add dir..."),0,wxALIGN_RIGHT|wxALL,border_size);
	game_buttons_row->Add(new wxButton(this,id_remove,"&Remove dir..."),0,wxALIGN_RIGHT|wxALL,border_size);
	
	game_col->Add(game_buttons_row,0,wxALIGN_RIGHT|wxLEFT|wxRIGHT,border_size);
	
	//buttons
	wxButton *ok=new wxButton(this,id_ok,"OK");
	ok->SetDefault();
	wxButton *cancel=new wxButton(this,wxID_CANCEL,"Cancel");
	buttons_row->Add(ok,0,wxALIGN_RIGHT|wxALL,border_size);
	buttons_row->Add(cancel,0,wxALIGN_RIGHT|wxALL,border_size);
	
	//both_col has game_col on top, buttons_row below
	both_col->Add(game_col,1,wxEXPAND|wxALL,border_size);
	both_col->Add(buttons_row,0,wxALIGN_RIGHT|wxALIGN_BOTTOM,border_size);
	
	this->SetSizer(both_col);
	both_col->SetSizeHints(this);

	this->RefreshGames();
}

//////////////////////////////////////////////////////////////////////////
//
QuickstartDialog::~QuickstartDialog() {
	this->ResetGames();
}

//////////////////////////////////////////////////////////////////////////
// Adds to this->games_ all the games found in the dir tree starting at
// 'root'.
#define ISTOPCODE(C) (((C)=='0'?1:0)|((C)=='A'?2:0)|((C)=='a'?4:0))
#define ISBOTCODE(C) (((C)=='1'?1:0)|((C)=='B'?2:0)|((C)=='b'?4:0))
#define ISSEPCODE(C) (((C)=='_'?1:0)|((C)=='-'?2:0))

void QuickstartDialog::GetAllGamesRecursive(const wxFileName &root,bool is_first_time) {
	std::vector<wxFileName> dirs;

	B_LOG(QStart,"Looking in \"%s\"...\n",MB_STR(root.GetFullPath()));
	B_LOGI_N(QStart,4);
	
	// Files first
	std::vector<Game *> found;
	{
		std::vector<wxString> exts;
		mbDiscFormatGetAllExtensions(&exts);

		std::vector<wxFileName> files;
		mbGetMatchingFileNames(root,"*.*",&files);

		B_LOG(QStart,"Found %u files.\n",(unsigned)files.size());

		for(unsigned i=0;i<files.size();++i) {
			wxString ext=files[i].GetExt().Lower();
			B_LOG(QStart,"%u. \"%s\": ",i,MB_STR(files[i].GetFullPath()));

			if(std::binary_search(exts.begin(),exts.end(),ext)) {
				B_LOG(QStart,"valid.");
				found.push_back(new Game(files[i]));
			} else {
				B_LOG(QStart,"not a disc image.");
			}

			B_LOG(QStart,"\n");
		}
	}

	//sides
	std::sort(found.begin(),found.end());
	for(unsigned i=0;i<found.size();++i) {
		unsigned j=i+1;
		if(i>=found.size()||j>=found.size()||!found[i]) {
			continue;
		}
		if(found[i]->ext_lc!=found[j]->ext_lc) {
			continue;
		}
		const wxString &iname=found[i]->name_str;
		const wxString &jname=found[j]->name_str;
		unsigned ilen=iname.size();
		unsigned jlen=jname.size();
		if(ilen!=jlen||ilen<3||jlen<3) {
			continue;
		}
		if(ISSEPCODE(iname[ilen-2])==0||
			ISSEPCODE(iname[ilen-2])!=ISSEPCODE(jname[jlen-2]))
		{
			continue;
		}
		if(ISTOPCODE(iname[ilen-1])==0||
			ISTOPCODE(iname[ilen-2])!=ISBOTCODE(jname[jlen-2]))
		{
			continue;
		}

		mbDiscFormat ifmt,jfmt;
#ifdef __WXDEBUG__
		bool iok=mbDiscFormatGetFromFileName(found[i]->name,&ifmt);
		bool jok=mbDiscFormatGetFromFileName(found[j]->name,&jfmt);
		wxASSERT(iok&&jok);
#endif
		if(ifmt.sides!=1||jfmt.sides!=1) {
			continue;
		}
		
		//found[i] is side 0
		//found[j] is side 2
		//remove found[j].

		found[i]->is_ds=true;
		found[i]->side2name=found[j]->name;
		found[i]->display_name+=" (DS)";

		delete found[j];
		found[j]=0;
	}

	//add to list
	for(unsigned i=0;i<found.size();++i) {
		if(found[i]) {
			games_.push_back(found[i]);
		}
	}

	//Directories
	mbGetDirectoryNames(root,&dirs);

	//activate progress dialog first time we get into a dir with >N subdirectories in it.
	//otherwise, if the first looked-at dir contains only one dir, we get a pointless
	//progress dialog (even if further subdirectories contain tons of stuff.)
	//this also prevents the progress dialog popoping up if there's not too many files.
	wxProgressDialog *progress=0;
	if(is_first_time&&dirs.size()>25) {
		progress=new wxProgressDialog("Scanning...","",dirs.size(),this);
		is_first_time=false;
	}

	for(unsigned i=0;i<dirs.size();++i) {
		if(progress) {
			const wxArrayString &components=dirs[i].GetDirs();
			progress->Update(int(i),components[components.GetCount()-1]);
		}
		this->GetAllGamesRecursive(dirs[i],is_first_time);
	}

	delete progress;
}

//////////////////////////////////////////////////////////////////////////
// Resets the list of games.
void QuickstartDialog::ResetGames() {
	for(unsigned i=0;i<games_.size();++i) {
		delete games_[i];
	}
	games_.clear();
}

//////////////////////////////////////////////////////////////////////////
// Refreshes the list of games by calling  this->GetAllGamesRecursive for
// each of the dirs.
void QuickstartDialog::RefreshGames() {
	B_LOG(QStart,"%s.\n",__FUNCTION__);

	this->ResetGames();

	//B_LOGI(QStart,"%u root dirs.",(unsigned)dirs_.size());

	for(size_t i=0;i<dirs_.size();++i) {
		wxFileName f(dirs_[i].c_str());

//		wxLogDebug("%s: dirs_[%u]=\"%s\" (%s)",__FUNCTION__,i,dirs_[i].c_str(),wxFileName(dirs_[i].c_str()).GetFullPath().c_str());
		//B_LOG(QStart,"%u. \"%s\"\n",(unsigned)i,dirs_[i].c_str());
		this->GetAllGamesRecursive(f,true);
	}
	this->RefreshGamesList();
}

//////////////////////////////////////////////////////////////////////////
// Clears the list control, then adds the names of all the games to it,
// then sorts the list.
void QuickstartDialog::RefreshGamesList() {
	unsigned i;

	games_list_->ClearAll();
	for(i=0;i<games_.size();++i) {
		long index=games_list_->InsertItem(0,games_[i]->display_name);
		games_list_->SetItemData(index,long(games_[i]));
	}
	games_list_->SortItems(&Game::ListCtrlCompare,0);
}


//////////////////////////////////////////////////////////////////////////
// user clicked 'Add'
void QuickstartDialog::OnAdd(wxCommandEvent &) {
	wxDirDialog dlg(this);
	if(dlg.ShowModal()==wxID_OK) {
		wxFileName name(wxFileName::DirName(dlg.GetPath()));
		wxLogDebug("chose %s\n",name.GetFullPath().c_str());
		if(name.IsOk()) {
			wxString str=name.GetFullPath();
			if(std::find(dirs_.begin(),dirs_.end(),str)==dirs_.end()) {
				dirs_.push_back(str);
			}
		}
		this->RefreshGames();
	}
}

//////////////////////////////////////////////////////////////////////////
// user clicked 'Remove'
//

void QuickstartDialog::OnRemove(wxCommandEvent &) {
	wxArrayInt indices;
	wxArrayString strings;
	unsigned i;
	
	for(i=0;i<dirs_.size();++i) {
		strings.Add(dirs_[i].c_str());
	}
	wxGetSelectedChoices(indices,"Select dir(s) to remove","Remove dirs",strings,this);
	
	//Remove the indices starting at the greatest, for obvious reasons,
	//regrettably std::vector will delete only by iterator hence the
	//palaver.
	std::sort(indices.begin(),indices.end());

	for(i=0;i<indices.GetCount();++i) {
		int index=indices[indices.GetCount()-1-i];
		dirs_.erase(dirs_.begin()+index);
	}

	this->RefreshGames();

// 	std::vector<wxString>::iterator end=dirs_.end();
// 	for(i=indices.GetCount();i;--i) {
// 		int idx=indices[i-1];
// 		std::swap(*--end,dirs_[idx]);
// 	}
// 	dirs_.erase(end,dirs_.end());
// 	this->RefreshGames();
}

//////////////////////////////////////////////////////////////////////////
// clicked 'OK'
// double clicked item in list
//
// Fetch selected item, if there is one, and set up 
void QuickstartDialog::OnOk(wxCommandEvent &event) {
	int n=games_list_->GetSelectedItemCount();

	result_valid_=false;

	//If the user double clicks, and there's nothing selected, don't stop.
	if(n==0&&event.GetEventType()==wxEVT_COMMAND_LIST_ITEM_ACTIVATED) {
		return;
	}
	if(n==1) {
		long index=games_list_->GetNextItem(-1,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED);
		if(index>=0) {
			const Game *game=reinterpret_cast<Game *>(games_list_->GetItemData(index));
			if(game) {
				result_valid_=true;
				result_.allow_save=false;
				result_.drive0=game->name;
				if(!game->is_ds) {
					result_.drive2.Clear();
				} else {
					result_.drive2=game->side2name;
				}
			}
		}
	}
	this->EndModal(wxID_OK);
}

void QuickstartDialog::OnListItemActivated(wxListEvent &event) {
	(void)event;

	wxCommandEvent evt(wxEVT_COMMAND_LIST_ITEM_ACTIVATED,id_games_list);
	this->OnOk(evt);
}

//////////////////////////////////////////////////////////////////////////
// Get result
//
// Returns true if there's a result to return. It might be that the user
// clicked 'Ok' without having selected a game -- that counts as no result.
bool QuickstartDialog::GetResult(mbQuickstartResult *result) const {
	if(result_valid_&&result) {
		*result=result_;
	}
	return result_valid_;
}

//////////////////////////////////////////////////////////////////////////
// 
void QuickstartDialog::GetDirs(std::vector<std::string> *dirs) const {
	wxASSERT(dirs);

	dirs->clear();
	dirs->reserve(dirs_.size());
	for(unsigned i=0;i<dirs_.size();++i) {
		dirs->push_back(std::string(dirs_[i].c_str()));
	}
}

//////////////////////////////////////////////////////////////////////////
// wx event table
BEGIN_EVENT_TABLE(QuickstartDialog,wxDialog)
	EVT_BUTTON(id_add,QuickstartDialog::OnAdd)
	EVT_BUTTON(id_remove,QuickstartDialog::OnRemove)
	EVT_BUTTON(id_ok,QuickstartDialog::OnOk)
	EVT_LIST_ITEM_ACTIVATED(id_games_list,QuickstartDialog::OnListItemActivated)
END_EVENT_TABLE()

//////////////////////////////////////////////////////////////////////////
// Does a quick start dialog. 'parent' is the parent window for the dialog.
// 'result' points to a mbQuickstartResult to be filled in.
//
// Returns true (*result filled in) if user selected a game.
// Returns false (*result untouched) if user didn't.
//
// cfg updated with current set of dirs if used clicked OK.
bool mbQuickstart(wxWindow *parent,mbQuickstartConfig &cfg,
	mbQuickstartResult *result)
{
	QuickstartDialog dlg(parent,cfg.dirs);
	dlg.SetSize(cfg.dialog_pos);
	if(dlg.ShowModal()==wxID_OK) {
		dlg.GetDirs(&cfg.dirs);
		cfg.dialog_pos=dlg.GetRect();
		if(dlg.GetResult(result)) {
			wxASSERT(result);
			return true;
		}
	}
	return false;
}
