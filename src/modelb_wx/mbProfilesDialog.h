#ifndef MBPROFILESDIALOG_H_
#define MBPROFILESDIALOG_H_

#include "mbConfig.h"
#include "mbModel.h"
#include "mbDiscInterface.h"

class mbProfilesDialog:
public wxDialog
{
public:
	mbProfilesDialog(wxWindow *parent,wxWindowID id,const mbProfilesConfig &cfg);

	void GetResult(mbProfilesConfig *cfg) const;
protected:
private:
	struct RomCtrls {
		wxSizer *sz;
		wxCheckBox *ovr_chk;
		wxStaticText *slot_sta;
		wxTextCtrl *fname_txt;
		wxButton *fname_bt;
		wxCheckBox *ram_chk;

//		void Refresh(const mbProfileRom *rom);
	};

	mbProfilesConfig cfg_;
	
	std::string cur_name_;
	mbProfile cur_pro_;
	mbProfile par_fpro_;
	//mbProfile cur_fpro_;
	bool cur_changed_;

	void SetCurrent(const std::string &new_current);
//	mbProfile *CurrentProfile();

	void CreateWidgets();
	void Refresh(bool first_time=false);
	wxSizer *CreateRomsArea();
	void RefreshSlot(int slot);

	void OnProfilesList(wxCommandEvent &event);
	void OnModelsRadioBox(wxCommandEvent &event);
	void OnDiscInterfaceRadioBox(wxCommandEvent &event);
	void OnRomOverride(wxCommandEvent &event);
	void OnRomRam(wxCommandEvent &event);
	void OnRomSelectFile(wxCommandEvent &event);
	void OnOsOverride(wxCommandEvent &event);
	void OnOsSelectFile(wxCommandEvent &event);
	void OnModelsCheckBox(wxCommandEvent &event);
	void OnDiscInterfaceCheckBox(wxCommandEvent &event);

	wxListBox *profiles_lst_;

	wxComboBox *parent_cb_;

	wxRadioBox *models_rdb_;
	wxCheckBox *models_chk_;
	std::vector<const mbModel *> models_;

	wxRadioBox *difs_rdb_;
	wxCheckBox *os_chk_;
	std::vector<const mbDiscInterface *> difs_;

	RomCtrls roms_[16];

	wxTextCtrl *os_txt_;
	wxButton *os_bt_;
	wxCheckBox *dif_chk_;

	int SlotFromCtrl(wxObject *ctrl);
	void SelectRomFile(std::string *str,const wxString &title,bool warn_if_not_16k);

	DECLARE_EVENT_TABLE()
};

#endif
