#include "pch.h"
#include "mbRomsConfigDialog.h"
#include "mbMisc.h"
#include <wx/valgen.h>

static const char *help_text=
	"These options take effect at the next reboot.\n"
	"Use \"Apply and Reset\" to save the settings and reboot straight away.";

const wxWindowID id_fsels=mbNewId();
const wxWindowID id_ok=mbNewId();
const wxWindowID id_apply_reset=mbNewId();
const wxWindowID id_is_fs=mbNewId();

mbRomsConfigDialog::BaseModelTab::BaseModelTab():
//page(0),
base_model(0)
{
	for(int i=0;i<16;++i) {
//		this->rom_controls[i]=0;
		this->rom_is_fs[i]=0;
	}
}

mbRomsConfigDialog::RomControls::RomControls():
slot(-1),
slot_number(0),
file_name(0),
select_file(0),
is_ram(0)
{
}

#ifdef __WXDEBUG__
bool mbRomsConfigDialog::RomControls::AllOk() const {
	return this->slot_number&&this->file_name&&this->select_file&&this->is_ram;
}
#endif


mbRomsConfigDialog::mbRomsConfigDialog(wxWindow *parent,wxWindowID id,mbRomsConfig &cfg):
wxDialog(parent,id,"Configure ROMs",wxDefaultPosition,wxDefaultSize,wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER),
cfg_(cfg)
{
//	for(unsigned i=0;i<16;++i) {
//		rom_is_fs_[i]=0;
//		rom_controls_[i]=0;
//	}
	this->SetExtraStyle(this->GetExtraStyle()|wxWS_EX_VALIDATE_RECURSIVELY);
	this->CreateRomControls();
	mbSetWindowRect(this,cfg_.dlg_rect);//this->SetSize(cfg_.dlg_rect);
}

mbRomsConfigDialog::~mbRomsConfigDialog() {
	this->DeleteRomControls();
}

void mbRomsConfigDialog::CreateRomControls() {
	static const int border_size=6;

	std::vector<const mbBaseModel *> bms;
	mbModelGetAllBaseModels(&bms);

	wxNotebook *notebook=new wxNotebook(this,-1);

	for(unsigned bm_idx=0;bm_idx<bms.size();++bm_idx) {
		const mbBaseModel *bm=bms[bm_idx];
		BaseModelTab *tab=&*tabs_.insert(tabs_.end(),BaseModelTab());

		tab->page=new wxPanel(notebook,-1);
		tab->base_model=bm;
		tab->rom_set=&cfg_.rom_sets[bm->short_name];

//		wxScrolledWindow *scrollw=new wxScrolledWindow(notebook,-1);

		wxWindow *parent=tab->page;

		wxFlexGridSizer *roms_sizer=new wxFlexGridSizer(5);
		roms_sizer->AddGrowableCol(1);

		for(int i=0;i<16;++i) {
			RomControls *rc=&tab->rom_controls[i];
			rc->slot=15-i;
			mbRomsConfig::Rom *rom=&tab->rom_set->roms[rc->slot];

			//BAsic control setup
			rc->file_name=new wxTextCtrl(parent,-1);
			rc->select_file=new wxButton(parent,id_fsels,"...",wxDefaultPosition,wxDefaultSize,wxBU_EXACTFIT);
			//wxLogDebug("%s: bm_idx=%u i=%d rc->select_file=%p",__FUNCTION__,bm_idx,i,rc->select_file);
			rc->is_ram=new wxCheckBox(parent,-1,"");//RAM");
			tab->rom_is_fs[rc->slot]=new wxRadioButton(parent,id_is_fs,"FS",wxDefaultPosition,wxDefaultSize,wxRB_SINGLE);
			unsigned slotmask=1<<rc->slot;
			if(!(bm->b_rom_mask&slotmask)) {
				//M128 style ROM
				rc->file_name->Enable(false);
				rc->select_file->Enable(false);
				tab->rom_is_fs[rc->slot]->Enable(false);
				rc->is_ram->SetValue(false);
				rc->is_ram->Enable(false);
			} 
			rc->slot_number=new wxStaticText(parent,-1,wxString::Format("%X",rc->slot));

			//Sizers
			int border=wxLEFT|wxRIGHT|wxTOP;
			if(i==15) {
				border|=wxBOTTOM;
			}
			roms_sizer->Add(rc->slot_number,0,border|wxALIGN_CENTRE_VERTICAL,border_size);
			roms_sizer->Add(rc->file_name,1,wxEXPAND|wxTOP|wxLEFT,border_size);
			roms_sizer->Add(rc->select_file,0,wxRIGHT|wxTOP,border_size);
			roms_sizer->Add(rc->is_ram,0,border|wxALIGN_CENTRE_VERTICAL,border_size);
			roms_sizer->Add(tab->rom_is_fs[rc->slot],0,border|wxALIGN_CENTRE_VERTICAL,border_size);

			//Validators
			mbSTLStringValidator file_name_vd(wxFILTER_NONE,&rom->filename);
			rc->file_name->SetValidator(file_name_vd);
			
			wxGenericValidator is_ram_vd(&rom->ram);
			rc->is_ram->SetValidator(is_ram_vd);
		}

//		scrollw->SetScrollbars(1,1,50,50);
		tab->page->SetSizer(roms_sizer);
		roms_sizer->SetSizeHints(tab->page);

		notebook->AddPage(tab->page,bm->long_name);
	}

	notebook->SetSelection(0);
	//Buttons
	wxBoxSizer *btns_sz;
	{
		wxButton *apply=new wxButton(this,id_apply_reset,"&Apply and Reset");
		wxButton *ok=new wxButton(this,id_ok,"OK");
		wxButton *cancel=new wxButton(this,wxID_CANCEL,"Cancel");

		ok->SetDefault();
		
		btns_sz=new wxBoxSizer(wxHORIZONTAL);
		btns_sz->Add(apply,0,wxALL|wxALIGN_RIGHT,border_size);
		btns_sz->Add(ok,0,wxALL|wxALIGN_RIGHT,border_size);
		btns_sz->Add(cancel,0,wxALL|wxALIGN_RIGHT,border_size);
	}

	//column containing everything
	wxBoxSizer *all_sz=new wxBoxSizer(wxVERTICAL);
	wxStaticText *help_caption=new wxStaticText(this,0,help_text);
	all_sz->Add(help_caption,0,wxALL,border_size);
	all_sz->Add(notebook,1,wxGROW|wxALL,border_size);
//	all_sz->Add(os_sz,0,wxGROW|wxALL,border_size);
	all_sz->Add(btns_sz,0,wxALL|wxALIGN_RIGHT,border_size);
	
	this->SetSizer(all_sz);
	all_sz->SetSizeHints(this);

	this->RefreshDialog();
}

void mbRomsConfigDialog::DeleteRomControls() {
	for(unsigned i=0;i<tabs_.size();++i) {
	}
//	unsigned i;
//
//	for(i=0;i<16;++i) {
//		rom_is_fs_[i]=0;
//	}
//	if(scrollw_) {
//		scrollw_->Destroy();
//		scrollw_=0;
//	}
//	delete os_file_name_vd_;
//	os_file_name_vd_=0;
//	this->SetSizer(0);
//	roms_sizer_=0;
//	for(i=0;i<16;++i) {
//		RomControls *rc=rom_controls_[i];
//		if(rc) {
//			delete rc->file_name_vd;
//			delete rc->is_ram_vd;
//			delete rc;
//		}
//	}

//	rom_controls_.clear();
}

void mbRomsConfigDialog::OnFileSelect(wxCommandEvent &event) {
	wxObject *obj=event.GetEventObject();
	wxLogDebug("%s: obj=%p tabs_.size()=%u",__FUNCTION__,obj,tabs_.size());
	std::vector<BaseModelTab>::iterator tab;
	int slot;
	for(tab=tabs_.begin();tab!=tabs_.end();++tab) {
		for(slot=0;slot<16;++slot) {
			if(obj==tab->rom_controls[slot].select_file) {
				goto found;
			}
		}
	}
	return;//erm...
found://erm...
	//wxString *rom_name;
	//wxString fsel_msg;

	//rom_name=&->filename;//&roms_[rom_controls_[i]->slot].filename;
	wxString fsel_msg=wxString::Format("Select ROM for %s slot %u",tab->base_model->long_name,slot);
	std::string *rom_filename=&tab->rom_set->roms[tab->rom_controls[slot].slot].filename;

	wxString old_dir,old_name;
	wxFileName old=wxFileName::FileName(rom_filename->c_str());
	if(old.IsOk()) {
		old_name=old.GetFullName();
		old_dir=old.GetPath();
	}

	wxFileDialog fd(this,fsel_msg,old_dir,old_name,"All files|*.*",wxFD_OPEN);
	if(fd.ShowModal()==wxID_OK) {
		*rom_filename=fd.GetPath();
		this->RefreshDialog();
	}
}

void mbRomsConfigDialog::RefreshDialog() {
	this->TransferDataToWindow();
	for(std::vector<BaseModelTab>::iterator tab=tabs_.begin();tab!=tabs_.end();++tab) {
		for(int i=0;i<16;++i) {
			tab->rom_is_fs[i]->SetValue(i==tab->rom_set->fs_slot);
		}
	}
}

void mbRomsConfigDialog::OnOk(wxCommandEvent &) {
	this->DataOk(wxID_OK);
}

void mbRomsConfigDialog::OnApplyReset(wxCommandEvent &) {
	this->DataOk(wxID_APPLY);
}

void mbRomsConfigDialog::GetResult(mbRomsConfig *cfg) const {//,mbRomsConfig::Rom **roms) const {
	wxASSERT(cfg);
	*cfg=cfg_;
//	for(unsigned i=0;i<16;++i) {
//		*roms[i]=roms_[i];
//	}
}

void mbRomsConfigDialog::DataOk(int result) {
	cfg_.dlg_rect=this->GetRect();
	this->TransferDataFromWindow();
	for(std::vector<BaseModelTab>::iterator tab=tabs_.begin();tab!=tabs_.end();++tab) {
		tab->rom_set->fs_slot=-1;
		for(int i=0;i<16;++i) {
			if(tab->rom_is_fs[i]->GetValue()) {
				tab->rom_set->fs_slot=i;
				break;
			}
		}
	}
	this->EndModal(result);
}

void mbRomsConfigDialog::OnIsFs(wxCommandEvent &event) {
	for(std::vector<BaseModelTab>::iterator tab=tabs_.begin();tab!=tabs_.end();++tab) {
		int slot;

		for(slot=0;slot<16;++slot) {
			if(event.GetEventObject()==tab->rom_is_fs[slot]) {
				break;
			}
		}
		
		if(slot<16) {
			for(int i=0;i<16;++i) {
				tab->rom_is_fs[i]->SetValue(i==tab->rom_set->fs_slot);
			}
			break;
		}
	}
}

BEGIN_EVENT_TABLE(mbRomsConfigDialog,wxDialog)
	EVT_BUTTON(id_fsels,mbRomsConfigDialog::OnFileSelect)
	EVT_BUTTON(id_ok,mbRomsConfigDialog::OnOk)
	EVT_BUTTON(id_apply_reset,mbRomsConfigDialog::OnApplyReset)
//	EVT_BUTTON(id_cancel,mbRomsConfigDialog::OnCancel)
	EVT_RADIOBUTTON(id_is_fs,mbRomsConfigDialog::OnIsFs)
END_EVENT_TABLE()
