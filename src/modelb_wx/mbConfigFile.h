#ifndef MBCONFIGFILE_H_
#define MBCONFIGFILE_H_

// represents an INI file style affair --
// flat sequence of sections.  each section contains items. each item has name and value.
// sections are represented by integer ids. each section has a unique id.
// items are represented by a section id and an integer id, representing that item in that section.
// each item's id is not necessarily unique across all sections.
class mbConfigFile {
public:
	virtual ~mbConfigFile();

	// get id of first section
	virtual int GetFirstSection()=0;

	// get id of section following the given section
	virtual int GetNextSection(int section)=0;

	// get name of given section
	virtual const char *GetSectionName(int section)=0;

	// adds a new section. new section has the given name. returns id of new section.
	virtual int AppendSection(const char *section_name)=0;

	// delete given section. this will potentially rearrange section ids.
	// (so don't use in a loop for example)
	virtual void DeleteSection(int section)=0;

	// get id of first item within given section
	virtual int GetFirstItem(int section)=0;

	// get id of item following the given item in the given section
	virtual int GetNextItem(int section,int item)=0;

	// get name of given item in given section
	virtual const char *GetItemName(int section,int item)=0;

	// get value of given item in given section
	virtual const char *GetItemValue(int section,int item)=0;

	// set name and/or value of given item in given section.
	// if either of new_name or new_value is 0, original name/value will be left.
	virtual void SetItem(int section,int item,const char *new_name,const char *new_value)=0;

	// append new item with given name to given section. returns id of new item.
	virtual int AppendItem(int section,const char *item_name)=0;

	// delete given item in given section. this will potentially rearrange item and section ids.
	// (TODO... this does make this function very useless.)
	virtual void DeleteItem(int section,int item)=0;

	int GetSectionByName(const char *section_name);
	void DeleteSectionByName(const char *section_name);
	void DeleteItemByName(const char *section_name,const char *item_name);

	bool Read(const char *section,const char *name,bool *dest);
	bool Read(const char *section,const char *name,long *dest);
	bool Read(const char *section,const char *name,int *dest);
	bool Read(const char *section,const char *name,std::string *dest);

	void Write(const char *section,const char *name,const bool &value);
	void Write(const char *section,const char *name,const long &value);
	void Write(const char *section,const char *name,const int &value);
	void Write(const char *section,const char *name,const char *value);
	void Write(const char *section,const char *name,const std::string &value);
protected:
	const char *GetValueByName(const char *section,const char *name);
	void GetItemLocationByName(const char *section_name,const char *item_name,int *section,int *item,bool insert);
private:
	template<class T>
	void WriteNumberHelper(const char *section_name,const char *item_name,const char *fmt,const T &value);

	template<class T>
	bool ReadNumberHelper(const char *value,T *dest);
};

class mbIniFile:
public mbConfigFile
{
public:
	mbIniFile();
	bool Load(const char *filename);
	bool Save(const char *filename);

	int GetFirstSection();
	int GetNextSection(int section);
	const char *GetSectionName(int section);
	int AppendSection(const char *section_name);
	void DeleteSection(int section);

	int GetFirstItem(int section);
	int GetNextItem(int section,int item);

	const char *GetItemName(int section,int item);
	const char *GetItemValue(int section,int item);
	void SetItem(int section,int item,const char *new_name,const char *new_value);
	int AppendItem(int section,const char *item_name);
	void DeleteItem(int section,int item);
	
	void DebugDump();
protected:
private:
	struct Line {
		enum Type {
			COMMENT,
			ITEM,
			SECTION,
			EMPTY,
		};
		Type type;
		std::string name;//name if ITEM. text if COMMENT or SECTION.
		std::string value;//value if ITEM. section name if SECTION. blank if COMMENT.
	};
	std::vector<Line> lines_;
	std::vector<int> sections_;

	bool LoadInternal(const char *filename);
#ifndef NDEBUG
	bool IsValidSection(int section);
	bool IsValidItem(int section,int item);
#endif
	void Check();
};

#endif
