#include "pch.h"
#include "mbDiscFormat.h"
#include <algorithm>

struct DiscGeometry {
	const char *description;
	int sides,tracks,sectors;
	bool is_double_density;
	const char **exts;
};

const char *exts_ss_sd[]={
	"ssd","bbc","img",0,
};

const char *exts_ds_sd[]={
	"dsd",0,
};

const char *exts_ss_dd[]={
	"sdd",0,
};

const char *exts_ds_dd[]={
	"ddd",0,
};

const char *exts_adfs_dd[]={
	"adl",0,
};

const char *exts_adfs_sd[]={
	"adm",0,
};

const DiscGeometry geom_ss_sd={"Single sided single density",1,80,10,false,exts_ss_sd};
const DiscGeometry geom_ds_sd={"Double sided single density",2,80,10,false,exts_ds_sd};
const DiscGeometry geom_ss_dd={"Single sided double density",1,-1,-1,true,exts_ss_dd};
const DiscGeometry geom_ds_dd={"Double sided double density",2,-1,-1,true,exts_ds_dd};
const DiscGeometry geom_adfs_sd={"Single sided ADFS",1,80,16,true,exts_adfs_sd};
const DiscGeometry geom_adfs_dd={"Double sided ADFS",2,80,16,true,exts_adfs_dd};

const DiscGeometry *geometries[]={
	&geom_ss_sd,
	&geom_ds_sd,
	&geom_ss_dd,
	&geom_ds_dd,
	&geom_adfs_sd,
	&geom_adfs_dd,
	0,
};

static void Convert(const DiscGeometry *src,mbDiscFormat *dest) {
	dest->sides=src->sides;
	dest->tracks=src->tracks;
	dest->sectors=src->sectors;
	dest->description=src->description;
	dest->density=src->is_double_density?bbcFdd::DENSITY_DOUBLE:bbcFdd::DENSITY_SINGLE;
	//dest->is_dd=src->is_double_density;

	dest->extensions.clear();
	for(unsigned i=0;src->exts[i];++i) {
		dest->extensions.push_back(src->exts[i]);
	}
}

static bool FixGeometryFromFileSize(unsigned file_size,mbDiscFormat *fmt) {
	static const int num_tracks[2]={40,80};
	static const int num_sectors[2]={16,18};

	for(unsigned i=0;i<2;++i) {
		for(unsigned j=0;j<2;++j) {
			if(file_size==fmt->sides*num_tracks[i]*num_sectors[j]*256U) {
				fmt->tracks=num_tracks[i];
				fmt->sectors=num_sectors[j];
				return true;
			}
		}
	}

	return false;
}

bool mbDiscFormatGetFromFileName(const wxFileName &file_name,mbDiscFormat *fmt) {
	wxString file_name_ext=file_name.GetExt();
	
	for(unsigned i=0;geometries[i];++i) {
		for(unsigned j=0;geometries[i]->exts[j];++j) {
			if(file_name_ext.CmpNoCase(geometries[i]->exts[j])==0) {
				Convert(geometries[i],fmt);

				//But we can't determine # sectors or tracks. (Well, you can, but...)
				fmt->tracks=-1;
				fmt->sectors=-1;
				
				return true;
			}
		}
	}
	return false;
}

bool mbDiscFormatGetFromFile(const wxFileName &file_name,unsigned file_size,mbDiscFormat *fmt) {
	wxString file_name_ext=file_name.GetExt();
	
	for(unsigned i=0;geometries[i];++i) {
		for(unsigned j=0;geometries[i]->exts[j];++j) {
			if(file_name_ext.CmpNoCase(geometries[i]->exts[j])==0) {
				Convert(geometries[i],fmt);

				//DDD and SDD are detected based on the size of the image... hmm... !
				if(fmt->tracks<0||fmt->sectors<0) {
					if(!FixGeometryFromFileSize(file_size,fmt)) {
						//Bad dd disc size
						return false;
					}
				}

				return true;
			}
		}
	}
	return false;
}

void mbDiscFormatGetAll(std::vector<mbDiscFormat> *all_fmts) {
	wxASSERT(all_fmts);
	all_fmts->clear();
	for(unsigned i=0;geometries[i];++i) {
		all_fmts->push_back(mbDiscFormat());
		Convert(geometries[i],&all_fmts->back());
	}
}

void mbDiscFormatGetAllExtensions(std::vector<wxString> *all_exts) {
	wxASSERT(all_exts);
	all_exts->clear();
	for(unsigned i=0;geometries[i];++i) {
		for(unsigned j=0;geometries[i]->exts[j];++j) {
			all_exts->push_back(geometries[i]->exts[j]);
			wxASSERT(wxString(geometries[i]->exts[j]).Lower()==geometries[i]->exts[j]);
		}
	}
	std::sort(all_exts->begin(),all_exts->end());
}
