#ifndef _OPTION_H_
#define _OPTION_H_

// ye olde Kommande Line proceffinge classes!
// updated for wxetc. [29/5/2003]

// Option look n feel (unfinished)
class twArgLnf {
public:
	twArgLnf();
	twArgLnf(const wxString &inShortForm,const wxString &inLongForm="");
	inline const wxString &ShortForm() const {
		return this->shortForm;
	}
	inline const wxString &LongForm() const {
		return this->longForm;
	}
private:
	wxString longForm;
	wxString shortForm;
};

class twArg {
public:
	static bool twArg::ProcessAll(const std::vector<wxString> &args,
		const std::vector<twArg *> &optList);
	virtual bool Perform(const std::vector<wxString> &args)=0;
	inline int NumArgs() const {
		return this->numArgs;
	}
	inline const wxString &Str() const {
		return this->str;
	}
	virtual ~twArg()=0;
protected:
	twArg(const wxString &inStr,int numArgs);
	twArg(const char *inStr,int inNumArgs);
	inline void SetNumArgs(int newNumArgs) {
		this->numArgs=newNumArgs;
	}
private:
	wxString str;
	int numArgs;
};

//////////////////////////////////////////////////////////////////////////
// Calls function if argument seen
class twArgFnCall:
public twArg
{
public:
	typedef bool (*FuncType)(void *);
	twArgFnCall(const wxString &inStr,FuncType inFunc,void *inContext=0);
	bool Perform(const std::vector<wxString> &args);
protected:
private:
	FuncType func;
	void *context;
};

//////////////////////////////////////////////////////////////////////////
// Sets flag to true or false
class twArgPresent:
public twArg
{
public:
	twArgPresent(const char *inStr,bool *inFlag,bool inSetting=true);
	bool Perform(const std::vector<wxString> &args);
private:
	bool *flag;
	bool setting;
};

//////////////////////////////////////////////////////////////////////////
// Gets list of N strings
class twArgStrings:
public twArg
{
public:
	typedef std::vector<wxString> VarRefType;
	twArgStrings(const char *inStr,std::vector<wxString> *inVar,unsigned inNumArgs);
	bool Perform(const std::vector<wxString> &args);
private:
	std::vector<wxString> *var;
};

//////////////////////////////////////////////////////////////////////////
// Gets single string
class twArgString:
public twArg
{
public:
	typedef wxString VarRefType;
	twArgString(const char *inStr,wxString *inSetting,const wxString &inDefault="");
	bool Perform(const std::vector<wxString> &args);
private:
	wxString *setting;
};

//////////////////////////////////////////////////////////////////////////
// Gets single float
class twArgFloat:
public twArg
{
public:
	typedef wxString VarRefType;
	twArgFloat(const char *inStr,float *inVarRef);
	bool Perform(const std::vector<wxString> &args);
private:
	float *varRef;
};

//////////////////////////////////////////////////////////////////////////
// Gets state -- yes|on|true or no|off|false
class twArgState:
public twArg
{
public:
	typedef bool VarRefType;
	twArgState(const char *inStr,bool *inFlag,bool inDefault=false);
	bool Perform(const std::vector<wxString> &args);
private:
	bool *flag;
};

//////////////////////////////////////////////////////////////////////////
//gets uint
class twArgUint:
public twArg
{
public:
	typedef unsigned VarRefType;
	twArgUint(const char *inStr,unsigned *inVarRef);
	twArgUint(const char *inStr,unsigned *inVarRef,unsigned inMin,unsigned inMax);
	bool Perform(const std::vector<wxString> &args);
private:
	unsigned minimum,maximum;
	bool hasMinMax;
	unsigned *varRef;
};

//////////////////////////////////////////////////////////////////////////
// sets a var to a value
template<class T>
class twArgSetVariable:
public twArg
{
public:
	twArgSetVariable(const char *inStr,T &inVar,const T &inSetting);
	bool Perform(const std::vector<wxString> &args);
private:
	T *var;
	T setting;
};

template<class T>
twArgSetVariable<T>::twArgSetVariable(const char *inStr,T &inVar,const T &inSetting):
twArg(inStr,0),
var(inVar),
setting(inSetting)
{
}

template<class T>
bool twArgSetVariable<T>::Perform(const std::vector<wxString> &args) {
	this->var=this->setting;
	return true;
}
	
//////////////////////////////////////////////////////////////////////////
// allows option to be specified multiple times
//
// provided your option class has a VarRefType typedef, and
// its constructor can becaled like:
//
//		ConstructorName(const char *inStr,VarRefType &varToStoreValueIn)
//
// it can be used with this.
template<class T>
class twArgMulti:
public twArg
{
public:
	twArgMulti(const char *inStr,std::vector<typename T::VarRefType> *inVector,unsigned inNum=1);
	twArgMulti(T *pOption,std::vector<typename T::VarRefType> *inVector);
	~twArgMulti();
	bool Perform(const std::vector<wxString> &args);
private:
	typename T::VarRefType tmpVar;
	T *internalOption;
	std::vector<typename T::VarRefType> *vec;
	unsigned num;
};

template<class T>
twArgMulti<T>::~twArgMulti() {
	delete this->internalOption;
}


template<class T>
twArgMulti<T>::twArgMulti(T *pOption,std::vector<typename T::VarRefType> *inVector):
twArg(pOption->Str(),pOption->NumArgs()),
tmpVar(),
internalOption(new T(pOption->Str(),this->tmpVar,pOption->NumArgs())),
//internalOption(pOption),
vec(inVector),
num(pOption->NumArgs())
{
	wxASSERT(this->vec);
	wxASSERT(this->num>=1);
	this->SetNumArgs(this->num*this->internalOption->NumArgs());
}

template<class T>
twArgMulti<T>::twArgMulti(const char *inStr,std::vector<typename T::VarRefType> *inVector,unsigned inNum):
twArg(inStr,inNum),
tmpVar(),
internalOption(new T(inStr,&tmpVar)),//missing this-> hmm VC++
vec(inVector),
num(inNum)
{
	wxASSERT(this->vec);
	wxASSERT(this->num>=1);
	this->SetNumArgs(this->num*this->internalOption->NumArgs());
}

template<class T>
bool twArgMulti<T>::Perform(const std::vector<wxString> &args) {
	std::vector<wxString>::const_iterator arg=args.begin();
	for(unsigned i=0;i<this->num;++i) {
		// extract args for this instance of the option
		std::vector<wxString> thisArgs;
		for(int j=0;j<internalOption->NumArgs();++j) {
			wxASSERT(arg!=args.end());
			thisArgs.push_back(*arg);
			++arg;
		}

		// make the option interpret that into tmpVar
		if(!internalOption->Perform(args)) {
			return false;
		}

		// stick tmpVar on the end of the vector
		this->vec->push_back(tmpVar);
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////



#endif
