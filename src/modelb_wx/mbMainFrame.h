#ifndef mbMAINFRAME_H_
#define mbMAINFRAME_H_

struct mbModel;//#include "mbModel.h"
#include "mbJoyKeys.h"
#include "Host.h"
#include "mbConfig.h"
class mbStatusBar;//#include "mbStatusBar.h"
#ifdef bbcDEBUG_PANELS
#include "mbDebugPanel.h"
#endif
#include <t65.h>
#include <bbcFdd.h>
//#include <deque>
//#include <bbcDebugTrace.h>
//#include <bbcVideo.h>
class bbcProfileRecord;
struct mbDiscInterface;
class BeebRomsConfigDialog;
#ifdef bbcENABLE_BEEB_ICE
#include "mbIceFrame.h"
#endif
#include <wx/filename.h>

class NullPanel;

//TODO move this crap to mbApp, there is only ever one Beeb window.
//There's just far too much going on in here.
class mbMainFrame:
public wxFrame
{
public:
	mbMainFrame(const std::vector<wxString> &argv);

	bool Ok() const;
	void PowerOnBeeb();
	void SetAutoPaint(bool auto_paint);
	void SetSoundEnabled(bool new_sound_enabled);
	void GetConfig(mbConfig *cfg) const;
protected:
	void OnClose(wxCloseEvent &event);
	void OnIdle(wxIdleEvent &event);
private:
	void PowerOnBeebPostSetup();

	struct DriveUi {
//		bool loaded;
		wxFileName disc_image;
		bool no_save;

		wxString last_path;
		int last_filter_index;

		DriveUi();
	};

	struct Key {
		enum Flags
		{
			F_IS_JOYSTICK=1,
			F_AUTOFIRE=2,
		};

		unsigned char flags;
		unsigned char index;
		unsigned short code;
	};
	
	bool auto_paint_;
	bool ok_;
	std::vector<mbJoyKeyState> joykey_states_;
	std::vector<Key> keymap_[256];
	mbConfig cfg_;
	HostGfxState *hostgfx_;
	HostInpState *hostinp_;
	HostSndState *hostsnd_;
	
	HostTmr_HfValue hf_freq_;
	
	void DoBeebTick();
	bool ApplyConfig();
	
	unsigned num_frames_;
	
	int next_update_input_;
	int next_update_sys_;
	int next_update_sound_;
	int last_update_sys_time_;
	
	bool debouncing_break_;

	HostTmr_HfValue last_update_sound_hf_time_;

	int joystick_indexes_[2];
	int key_down_counts_[256];

	//
	void SetKeymap(const std::string &keymap_name);
	void DoUpdateInput();
	void DoUpdateSound();
	void DoUpdateSys();
	void InitMenuBar();

	//Menu handlers
	void OnMenuOpen(wxMenuEvent &event);
	void OnMenuClose(wxMenuEvent &event);
	void OnExit(wxCommandEvent &event);
	void OnToggleSound(wxCommandEvent &event);
	void OnToggleLimitSpeed(wxCommandEvent &event);
	void OnToggleToolbar(wxCommandEvent &event);
	void OnShowAboutBox(wxCommandEvent &event);
	void OnQuickstart(wxCommandEvent &event);
/*
	mbMainFrame_DECLARE_DRIVE_HANDLERS(0);
	mbMainFrame_DECLARE_DRIVE_HANDLERS(1);
	mbMainFrame_DECLARE_DRIVE_HANDLERS(2);
	mbMainFrame_DECLARE_DRIVE_HANDLERS(3);
*/
	void OnDriveXCommand(wxCommandEvent &event);
	void OnTest(wxCommandEvent &event);
	void OnRomsDialog(wxCommandEvent &event);
	void OnSoundDialog(wxCommandEvent &event);
	void OnKeymapDialog(wxCommandEvent &event);
	void OnKeymap(wxCommandEvent &event);
	void OnToggleFastForwardDiscAccess(wxCommandEvent &event);
	void OnHardwareDialog(wxCommandEvent &event);
	
	//Drive handlers
	void OnDriveLoad(wxCommandEvent &event,int drive);
	void OnDriveLoadInternal(wxCommandEvent &event,int drive);
	void OnDriveSave(wxCommandEvent &event,int drive);
	void OnDriveSaveAs(wxCommandEvent &event,int drive);
	void OnDriveUnload(wxCommandEvent &event,int drive);

#ifdef bbcENABLE_SAVE_STATE
	void OnLoadState(wxCommandEvent &event);
	void OnSaveState(wxCommandEvent &event);
#endif
	
	//Other handlers
	void OnPaint(wxPaintEvent &event);
	void OnEraseBackground(wxEraseEvent &event);
	void OnSize(wxSizeEvent &event);
	void OnMove(wxMoveEvent &event);
#ifdef BEEB_DEBUG_SAVE_CHALLENGER_RAM
	void OnSaveChallengerRam(wxCommandEvent &event);
#endif
#ifdef bbcDEBUG_ENABLE
	void OnDisassembleRam(wxCommandEvent &event);
#endif
	void OnResolution(wxCommandEvent &event);
	void OnScreenshotSaveAs(wxCommandEvent &event);
	void OnToggleStatusBar(wxCommandEvent &event);
#ifdef bbcDEBUG_PANELS
	void OnToggleDbgpanelSysvia(wxCommandEvent &event);
	void OnToggleDbgpanelUsrvia(wxCommandEvent &event);
	void OnToggleDbgpanelVideo(wxCommandEvent &event);
	void OnToggleDbgpanelAdc(wxCommandEvent &event);
#endif
#if (defined bbcDEBUG_TRACE_EVENTS)
	void OnStartDisassembly(wxCommandEvent &event);
	void OnStopDisassembly(wxCommandEvent &event);
	void OnRebootWithDisassembly(wxCommandEvent &event);
	void ResetDisassembly();
#endif
#ifdef bbcDEBUG_VIDEO
	void OnToggleShowAll(wxCommandEvent &event);
	void OnLogVideoFrames(wxCommandEvent &event);
	void OnToggleShowT1Timeouts(wxCommandEvent &event);
#endif
#ifdef t65_COUNT_INSTRUCTION_FREQUENCIES
	void OnShowInstructionFrequencies(wxCommandEvent &event);
#endif
#ifdef bbcDEBUG_COUNT_MMIO_ACCESSES
	void OnShowSheilaAccessCounts(wxCommandEvent &event);
#endif
	void OnSoundRecordStart(wxCommandEvent &event);
	void OnSoundRecordStop(wxCommandEvent &event);
	void OnHardReset(wxCommandEvent &event);
	void OnSetScreenType(wxCommandEvent &event);
	void OnVolume(wxCommandEvent &event);
	void OnDropFile(wxCommandEvent &event);
//	void OnToggleVertical2x(wxCommandEvent &event);
	void OnActivate(wxActivateEvent &event);
	void OnToggleFullScreen(wxCommandEvent &event);
	void OnScanlines(wxCommandEvent &event);
	void OnJoysticksDialog(wxCommandEvent &event);
	void OnRightUp(wxMouseEvent &event);
	void OnVideoDialog(wxCommandEvent &event);
#ifdef bbcENABLE_PROFILER
	void OnDisplayProfilerResults(wxCommandEvent &event);
#endif
#ifdef mbVISUAL_PROFILER
	void OnShowVisualProfile(wxCommandEvent &event);
#endif
#ifdef bbcENABLE_BEEB_ICE
	void OnIceWindowToggle(wxCommandEvent &event);
#endif
	void ModalBackground();
	bool CloseConfirmed();

	//with a user defined modal dialog
	//showing, idle events are still sent to the mbMainFrame and the BBC
	//continues to run in the background. Set this to true to pause the
	//beeb.
	//
	//It is not set by ModalBackground(), as there aren't too many user
	//defined modal dialogs in this program.
	bool beeb_paused_;

	void SetMenuItemEnabled(int menu_item_id,bool enabled);
	void SetMenuItemCheck(int menu_item_id,bool checked);
	void SetMenuItemText(int menu_item_id,const wxString &text);
	void SetHostGfxAdapter();
	void RefreshWindow();
	void RefreshCfgWindowState();

	DriveUi drive_uis_[bbcFdd::num_drives];

	//Ui things do a complete user interface type sequence (msgboxs etc.)
	bool UiLoadDisc(int drive,wxFileName file_name);

//	void ResetBreak();
//	void ResetHard();

	void RefreshKeymapsSubmenu(wxMenuItem *item);
	//wxMenu *keymaps_submenu_;
	wxSize ResolutionFromId(int id);

	//Screenshot stuff.
	//
	//Screenshots can only be taken when the frame has been fully drawn,
	//i.e. when VideoSystem::buffer_dirty, so this uses a notification
	//rather than saving straight away.
	//The screenshot countdown is really lame.
	//It starts at N.
	//If >1, background is cleaned
	//If ==1, a screenshot is taken.
	//If ==0, nothing happens.
	//Then, If >0 it is decremented.
	int screenshot_countdown_;//screenshot countdown
	wxString screenshot_name_;//name of the screenshot.
	void DoScreenshot();

//	typedef void (mbMainFrame::*DriveCommandMfn)(wxCommandEvent &,int);
//	static const DriveCommandMfn drive_subactions[];
	
	//User interface for unloading drive 'drive'
	//Dialogs if contents changed.
	//Returns true if drive should be unloaded.
	//false if not.
	bool UiUnloadDrive(int drive);

	struct SbState {
		struct Drive {
			char what;
			int track,sector;
		};
		Drive drives[bbcFdd::num_drives];
		bool caps,shift;

		SbState();
	};

	SbState state_;

	void UpdateStatusBar();

	NullPanel *beeb_screen_;
	mbStatusBar *status_bar_;
	wxMenu *drive_submenus_[bbcFdd::num_drives];

	void SetBeebScreenSize(const wxSize &screen_size);

	bool beeb_fastforward_;

	bool DoCommandLine(const std::vector<wxString> &argv);

	//Command line options -- cl_whatever_ are specified via command line.
	bool cl_select_ini_;
	wxString cl_drive_mounts_[bbcFdd::num_drives];
	bool cl_autoboot_;
	wxString cl_ini_file_;
	bool cl_ini_save_;
	std::vector<wxString> cl_ini_override_files_;
	bool cl_verbose_;
#if (defined bbcDEBUG_ENABLE)&&(defined bbcDEBUG_TRACE_EVENTS)
	bool cl_with_disassembly_;
#endif
#ifdef PORT_X
	bool cl_no_shm_;
	bool cl_no_xv_;
#endif

#ifdef mbVISUAL_PROFILER
	bool first_frame_drawn_yet_;
	uint64_t last_frame_tsc_;
	int DrawProfilerText(const HostGfxBuffer *buffer,int strx,int stry,int colour,const char *str);
	bbcProfileRecord *DrawProfileRecord(const HostGfxBuffer *buffer,int x,int *y,bbcProfileRecord *pr,float container_tsc,
		int colour,const wxPoint *pt);
	t65::qword profiler_font_data_[96][10];
	void MakeProfilerFontData();
	void DrawVisualProfile(const HostGfxBuffer *buffer,bbcProfileRecord *pr);
	void DrawVisualProfileBar(const HostGfxBuffer *buffer,int y,const bbcProfileRecord *pr,const bbcProfileRecord *query,
		int *leftx,int *rightx);
#endif

	//menu
	wxMenuBar *menu_bar_;
	wxMenu popup_menu_;
	
	const mbModel *model_;
	const mbDiscInterface *disc_interface_;
	bool update_window_state_;//whether to update window size and position when it moves

	const HostGfxBuffer *last_video_buffer_;

	void PresentLastVideoBuffer(mbScanlinesMode scanlines_mode,int present_hints,int frame_number,uint64_t *convert_time,uint64_t *present_time);

#ifdef bbcENABLE_BEEB_ICE
	mbIceFrame *ice_frame_;
	mbIceConfig ice_cfg_;
#endif
	bool recent_discs_dirty_;

	bbcSaveState::LoadStatus LoadSaveState(const bbcSaveState *save_state);
	bbcSaveState::SaveStatus SaveSaveState(bbcSaveState *save_state);

#ifdef bbcTEST_SAVE_STATE
	enum TssState {
		TSSS_NOT_STARTED,
		TSSS_STARTED,
		TSSS_RUNNING_AGAIN,
	};
	TssState tss_state;
	int tss_start_cycles_,tss_end_cycles_;
	std::vector<t65::byte> tss_start_data_,tss_end_data_;
	enum {
		tss_freq_cycles_=500000,// 1/4sec
	};
#endif//bbcTEST_SAVE_STATE

	DECLARE_EVENT_TABLE()
};

#endif
