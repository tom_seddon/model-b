#ifndef MBJOYKEYS_H_
#define MBJOYKEYS_H_

#include "mbKeyMap.h"
#include <HostInp.h>

enum mbJoyKey {
	mbJOYKEY_TX_NEG,
	mbJOYKEY_TX_POS,
	mbJOYKEY_TY_NEG,
	mbJOYKEY_TY_POS,
	mbJOYKEY_TZ_NEG,
	mbJOYKEY_TZ_POS,
	mbJOYKEY_RX_NEG,
	mbJOYKEY_RX_POS,
	mbJOYKEY_RY_NEG,
	mbJOYKEY_RY_POS,
	mbJOYKEY_RZ_NEG,
	mbJOYKEY_RZ_POS,
	mbJOYKEY_BUTTON_0,
	mbJOYKEY_BUTTON_LAST=mbJOYKEY_BUTTON_0+HostInp_num_joystick_buttons,
	mbJOYKEY_NUM,
	mbJOYKEY_INVALID,
};

struct mbJoyKeyState {
	int down_count[mbJOYKEY_NUM];
};

mbJoyKey mbJoyKeyFromKeyName(const std::string &name);
void mbJoyKeyGetIndexAndKey(HostInpState *hostinp,const mbKeymap::Key &key,int *joy_index,
	mbJoyKey *joy_key);
const std::string &mbJoyKeyNameFromKey(mbJoyKey key);
void mbJoyKeyGetKeyStates(const HostInp_JoyState *state,mbJoyKeyState *key_states);

#endif

