#ifndef mbSOUNDCONFIGDIALOG_H_
#define mbSOUNDCONFIGDIALOG_H_

#include "mbConfig.h"

class mbSoundConfigDialog:
public wxDialog
{
public:
	mbSoundConfigDialog(wxWindow *parent,const mbSoundConfig &cfg);

	void GetResult(mbSoundConfig *cfg) const;
protected:
private:
	void OnOk(wxCommandEvent &event);
	void OnClose(wxCloseEvent &event);
	void DoRefreshDialog(wxCommandEvent &event);

	mbSoundConfig cfg_;

	std::vector<int> bits_;
	std::vector<int> freqs_;

	//used for enabling/disabling
	std::vector<wxWindow *> sound_controls_;
	std::vector<wxWindow *> stereo_controls_;

	//
	wxComboBox *bits_cb_;
	wxComboBox *freqs_cb_;

	void FillCombobox(std::vector<int> &values,wxComboBox *combo) const;
	void SetComboboxSelection(wxComboBox *combo,int value);
	void RefreshDialog();

	DECLARE_EVENT_TABLE()
};

#endif
