#ifndef mbMODEL_H_
#define mbMODEL_H_

#include <bbcModel.h>
//#include "mbConfig.h"

struct mbModelRomsLoader;

struct mbBaseModel {
	enum Flags {
		M_NO_FSSLOT=1,
	};
	const char *long_name;
	const char *short_name;
	const char *const *valid_disc_interfaces;
	t65::word b_rom_mask;
	unsigned flags;

	bool IsValidDiscInterface(const char *disc_interface) const;
};

static const int mb_model_max_short_name_size=16;

struct mbModel {
	const char *long_name;
	char short_name[mb_model_max_short_name_size];
	const bbcModel *model;
	const mbBaseModel *base_model;
	t65::word force_swram_mask;
};

const mbModel *mbModelFromShortName(const std::string &short_name);
void mbModelGetAll(std::vector<const mbModel *> *all_models);
void mbModelGetAllBaseModels(std::vector<const mbBaseModel *> *all_base_models);

#endif
