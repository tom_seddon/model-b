#include "pch.h"
#include "mbConfig.h"
#include "mbModel.h"
#include "mbKeyMap.h"
#include "Host.h"
#include "mbMisc.h"
#include "mbKeys.h"
#include "mbDiscInterface.h"
#include <bbcVideo.h>
#include <bbcSound.h>
#include <iostream>
#include <sstream>
#include <list>
#include <bbcComputer.h>

#define DIALOG_RECT_NAME "DialogRect"
#define MISC_SECTION_NAME "Misc"
#define ROMS_SECTION_NAME "Roms_v2"
#define OLD_ROMS_SECTION_NAME "Roms"
#define SOUND_SECTION_NAME "Sound"
#define SOUND_STEREO_SECTION_NAME SOUND_SECTION_NAME "/Stereo"
#define KEYBOARD_SECTION_NAME "Keyboard"
#define QUICKSTART_SECTION_NAME "Quickstart"
#define VIDEO_SECTION_NAME "Video"
#define DEBUG_SECTION_NAME "Debug"
#define FILE_DLG_PREFIX "FileDlg/"
#define FILE_DLG_PREFIX_LEN (strlen(FILE_DLG_PREFIX))

//a bit stupid...
#define KEYBOARD_SECTION_NAME_LEN (strlen(KEYBOARD_SECTION_NAME))

//////////////////////////////////////////////////////////////////////////
template<class T>
static void ClampValue(const std::string &name,T *value,const T &mini,const T &maxi) {
	T old_value=*value;
	if(*value<mini) {
		*value=mini;
	} else if(*value>maxi&&maxi>mini) {
		*value=maxi;
	}
	if(*value!=old_value) {
		std::ostringstream old_val,new_val;
		old_val<<old_value<<std::ends;
		new_val<<*value<<std::ends;
		wxLogWarning("%s: invalid value %s, set to %s",name.c_str(),old_val.str().c_str(),
			new_val.str().c_str());
	}
}

static std::string CfgSafeName(const std::string &str) {
	std::string r=str;

	for(unsigned i=0;i<str.length();++i) {
		if(!isalnum(r[i])) {
			r[i]='_';
		}
	}
	return r;
}

//////////////////////////////////////////////////////////////////////////
//

mbProfileRom::mbProfileRom():
override(false),
ram(false)
{
}

void mbProfile::ResetToDefaults() {
	std::vector<const mbModel *> models;
	std::vector<const mbDiscInterface *> ifaces;

	mbModelGetAll(&models);
	mbDiscInterfaceGetAll(&ifaces);

	this->parent.clear();
	this->model=models.front()->short_name;
	this->fdc=ifaces.front()->short_name;
	this->os.clear();
	for(int i=0;i<16;++i) {
		this->roms[i]=mbProfileRom();
	}
}

void mbProfile::InheritSettingsFrom(const mbProfile &parent) {
	if(this->model.empty()) {
		this->model=parent.model;
	}

	if(this->fdc.empty()) {
		this->fdc=parent.fdc;
	}

	if(this->os.empty()) {
		this->os=parent.os;
	}

	for(int i=0;i<16;++i) {
		if(!this->roms[i].override) {
			this->roms[i]=parent.roms[i];
			this->roms[i].override=true;
		}
	}
}

void mbProfile::Load(const char *section,mbConfigFile *cfg) {
	cfg->Read(section,"parent",&this->parent);
	cfg->Read(section,"name",&this->name);
	cfg->Read(section,"model",&this->model);
	cfg->Read(section,"fdc",&this->fdc);
	cfg->Read(section,"os",&this->os);
	for(unsigned i=0;i<16;++i) {
		std::string section_name=mbStrPrintf("%s/Roms",section);
		std::string item_name=mbStrPrintf("%u",i);
		std::string text;
		if(cfg->Read(section_name.c_str(),item_name.c_str(),&text)) {
			mbProfileRom *rom=&this->roms[i];

			rom->ram=false;
			size_t comma=text.find_last_of(',');
			if(comma!=std::string::npos) {
				if(stricmp(text.substr(comma).c_str(),",ram")==0) {
					rom->ram=true;
				}
				text=text.substr(0,comma);
			}
			rom->filename=text;

			rom->override=true;
		}
	}
}

void mbProfile::Save(const char *section,mbConfigFile *cfg) const {
	cfg->Write(section,"parent",this->parent);
	cfg->Write(section,"name",this->name);
	cfg->Write(section,"model",this->model);
	cfg->Write(section,"fdc",this->fdc);
	cfg->Write(section,"os",this->os);
	for(unsigned i=0;i<16;++i) {
		const mbProfileRom *rom=&this->roms[i];
		if(rom->override) {
			std::string text=rom->filename;
			if(rom->ram) {
				text+=",ram";
			}
			std::string section_name=mbStrPrintf("%s/Roms",section);
			std::string item_name=mbStrPrintf("%u",i);
			cfg->Write(section_name.c_str(),item_name.c_str(),text);
		}
	}
}

mbProfile *mbProfilesConfig::ProfileByName(const std::string &name) {
	if(!name.empty()) {
		for(unsigned i=0;i<this->profiles.size();++i) {
			if(stricmp(this->profiles[i].name.c_str(),name.c_str())==0) {
				return &this->profiles[i];
			}
		}
	}
	return 0;
}

bool mbProfilesConfig::GetFullProfileByName(const std::string &name,mbProfile *dest) {
	wxASSERT(dest);

	mbProfile *named=this->ProfileByName(name);
	if(!named) {
		return false;
	}

	mbProfile *cur=named;
	std::list<mbProfile *> hier;
	while(cur) {
		hier.push_front(cur);
		cur=this->ProfileByName(cur->parent);
	}

	dest->ResetToDefaults();
	for(std::list<mbProfile *>::const_iterator it=hier.begin();it!=hier.end();++it) {
		mbProfile *pro=*it;

		if(!pro->model.empty()) {
			dest->model=pro->model;
		}

		if(!pro->fdc.empty()) {
			dest->fdc=pro->fdc;
		}

		if(!pro->os.empty()) {
			dest->os=pro->os;
		}

		for(int i=0;i<16;++i) {
			if(pro->roms[i].override) {
				dest->roms[i]=pro->roms[i];
			}
		}
	}

	//but override flags come from mostderived profile (obviously)
	for(int i=0;i<16;++i) {
		dest->roms[i].override=named->roms[i].override;
	}

	return true;
}

static bool strstartswith(const char *str,const char *start) {
	return strncmp(str,start,strlen(start))==0;
}

void mbProfilesConfig::Load(mbConfigFile *cfg) {
	std::string key_name;

	cfg->Read("Profiles","Current",&this->current);

	this->profiles.clear();
	for(int i=cfg->GetFirstSection();i>=0;i=cfg->GetNextSection(i)) {
		const char *section_name=cfg->GetSectionName(i);
		const char *prefix="Profiles/";
		if(strstartswith(section_name,prefix)) {
			this->profiles.push_back(mbProfile());
			this->profiles.back().Load(section_name,cfg);
		}
	}

	if(this->profiles.empty()) {
		mbProfile def;

		def.name="Default";
		def.ResetToDefaults();

		this->profiles.push_back(def);
		this->current=this->profiles.front().name;

		wxLogWarning("No profiles found.\nAdded default profile \"%s\"",def.name.c_str());
	}
}

void mbProfilesConfig::Save(mbConfigFile *cfg) const {
	bool done=false;
	while(!done) {
		int i;
		for(i=cfg->GetFirstSection();i>=0;i=cfg->GetNextSection(i)) {
			const char *section_name=cfg->GetSectionName(i);
			const char *prefix="Profiles/";
			if(strstartswith(section_name,prefix)) {
				cfg->DeleteSection(i);
				break;
			}
		}
		done=i<0;
	}

	//cfg->DeleteGroup("/Profiles"); doesn't work! ends up with assert
	//in wxwindows. suspect wxwindows bug...?
//	std::string key_name;
//	long key_index;
//	std::string old_path=cfg->GetPath();
//	cfg->SetPath("/Profiles");
//	if(cfg->GetFirstGroup(key_name,key_index)) {
//		do {
//			cfg->DeleteGroup("/Profiles/"+key_name);
//		} while(cfg->GetNextGroup(key_name,key_index));
//	}
//	cfg->SetPath(old_path);
//	//cfg->DeleteGroup("/Profiles");
//
	for(unsigned i=0;i<this->profiles.size();++i) {
		const mbProfile *pro=&this->profiles[i];
		std::string section_name="Profiles/"+CfgSafeName(pro->name);
		
		pro->Save(section_name.c_str(),cfg);
	}
	cfg->Write("Profiles","Current",this->current);
}

//////////////////////////////////////////////////////////////////////////
//
mbRomsConfig::Rom::Rom():
ram(false)
{
}

mbRomsConfig::BaseModelRomSet::BaseModelRomSet():
fs_slot(-1)
{
}

mbRomsConfig::mbRomsConfig():
dlg_rect(-1,-1,-1,-1)
{
}

void mbRomsConfig::Load(mbConfigFile *cfg) {
//	cfg->Read("/Roms/Os",&this->os_filename);
	std::vector<const mbBaseModel *> bms;
	std::vector<std::string> bm_section_names;

	mbModelGetAllBaseModels(&bms);
	for(unsigned i=0;i<bms.size();++i) {
		bm_section_names.push_back(mbStrPrintf(ROMS_SECTION_NAME "/%s",bms[i]->short_name));
	}

	std::vector<const mbDiscInterface *> ifs;
	mbDiscInterfaceGetAll(&ifs);

	// See if there is an old-style configuration
	// If so, rearrange the configuration file to turn it into a new one.
	int old_section=cfg->GetSectionByName(OLD_ROMS_SECTION_NAME);
	if(old_section>=0) {
		for(int i=0;i<16;++i) {
			char key[11];
			_snprintf(key,sizeof key,"%d",i);
			std::string b_rom;

			if(cfg->Read(OLD_ROMS_SECTION_NAME,key,&b_rom)) {
				for(unsigned j=0;j<bms.size();++j) {
					const mbBaseModel *bm=bms[j];
					if(bm->b_rom_mask&(1<<i)) {
						cfg->Write(bm_section_names[j].c_str(),key,b_rom);
					}
				}
			}
		}

		int fs_slot;
		if(!cfg->Read(OLD_ROMS_SECTION_NAME,"FsSlot",&fs_slot)) {
			fs_slot=-1;
		}

		for(unsigned i=0;i<ifs.size();++i) {
			std::string item_name=mbStrPrintf("fs_rom_%s",ifs[i]->short_name);
			std::string fs_rom;
			if(cfg->Read(OLD_ROMS_SECTION_NAME,item_name.c_str(),&fs_rom)) {
				cfg->Write(ROMS_SECTION_NAME,item_name.c_str(),fs_rom);
			}
		}

		for(unsigned i=0;i<bms.size();++i) {
			const mbBaseModel *bm=bms[i];
			for(int j=0;j<16;++j) {
				if(!(bm->b_rom_mask&(1<<j))) {
					std::string item_name=mbStrPrintf("%s_%d",bm->short_name,j);
					std::string non_b_rom;
					if(cfg->Read(OLD_ROMS_SECTION_NAME,item_name.c_str(),&non_b_rom)) {
						cfg->Write(bm_section_names[i].c_str(),mbStrPrintf("%d",j).c_str(),non_b_rom);
					}
				}
			}
			
			std::string item_name=mbStrPrintf("%s_os",bm->short_name);
			std::string os;
			if(cfg->Read(OLD_ROMS_SECTION_NAME,item_name.c_str(),&os)) {
				cfg->Write(bm_section_names[i].c_str(),"os",os);
			}
			
			if(fs_slot>=0) {
				cfg->Write(bm_section_names[i].c_str(),"FsSlot",fs_slot);
			}
		}

		cfg->DeleteSection(old_section);
	}

	wxRect old_rect;
	if(mbLoadRect(cfg,OLD_ROMS_SECTION_NAME "/" DIALOG_RECT_NAME,&old_rect)) {
		mbSaveRect(cfg,ROMS_SECTION_NAME "/" DIALOG_RECT_NAME,old_rect);
	}
	cfg->DeleteSectionByName(OLD_ROMS_SECTION_NAME "/" DIALOG_RECT_NAME);

	//static_cast<mbIniFile *>(cfg)->DebugDump();

	// Read new-style configuration.
	for(unsigned i=0;i<bms.size();++i) {
		const mbBaseModel *bm=bms[i];

		BaseModelRomSet *rom_set=&this->rom_sets[bm->short_name];

		cfg->Read(bm_section_names[i].c_str(),"os",&rom_set->os);
		for(int j=0;j<16;++j) {
			char key[11];
			_snprintf(key,sizeof key,"%d",j);
			std::string value;

			Rom *rom=&rom_set->roms[j];
			rom->ram=false;
			if(cfg->Read(bm_section_names[i].c_str(),key,&value)) {
				size_t comma=value.find_last_of(',');
				if(comma!=std::string::npos) {
					if(stricmp(value.substr(comma).c_str(),",ram")==0) {
						rom->ram=true;
					}
					value=value.substr(0,comma);
				}
				rom->filename=value;
			}
		}

		if(!cfg->Read(bm_section_names[i].c_str(),"FsSlot",&rom_set->fs_slot)) {
			rom_set->fs_slot=-1;
		}

		if(!cfg->Read(bm_section_names[i].c_str(),"DiscInterface",&rom_set->disc_interface)) {
			rom_set->disc_interface.clear();
		}
	}

	// Conversion of Misc/DiscInterface is done later.

	mbLoadRect(cfg,ROMS_SECTION_NAME "/" DIALOG_RECT_NAME,&this->dlg_rect);

	this->fs_roms.clear();
	for(unsigned i=0;i<ifs.size();++i) {
		std::string rom;
		std::string item_name=mbStrPrintf("fs_rom_%s",ifs[i]->short_name);

		if(cfg->Read(ROMS_SECTION_NAME,item_name.c_str(),&rom)) {
			this->fs_roms[ifs[i]->short_name]=rom;
		}
	}
}

void mbRomsConfig::Save(mbConfigFile *cfg) const {
	// delete old-style config
	cfg->DeleteSectionByName(OLD_ROMS_SECTION_NAME);
	cfg->DeleteSectionByName(OLD_ROMS_SECTION_NAME "/" DIALOG_RECT_NAME);

	// save new-style
	std::vector<const mbBaseModel *> bms;
	std::vector<std::string> bm_section_names;

	mbModelGetAllBaseModels(&bms);
	for(unsigned i=0;i<bms.size();++i) {
		bm_section_names.push_back(mbStrPrintf(ROMS_SECTION_NAME "/%s",bms[i]->short_name));
	}

	std::vector<const mbDiscInterface *> ifs;
	mbDiscInterfaceGetAll(&ifs);

	for(unsigned i=0;i<bms.size();++i) {
		RomSets::const_iterator it=this->rom_sets.find(bms[i]->short_name);
		if(it!=this->rom_sets.end()) {
			const BaseModelRomSet *rom_set=&it->second;
			
			wxString os_name=mbRelativeFileName(rom_set->os.c_str(),".");
			cfg->Write(bm_section_names[i].c_str(),"os",os_name.c_str());

			if(rom_set->fs_slot>=0) {
				cfg->Write(bm_section_names[i].c_str(),"FsSlot",rom_set->fs_slot);
			}

			if(!rom_set->disc_interface.empty()) {
				cfg->Write(bm_section_names[i].c_str(),"DiscInterface",rom_set->disc_interface);
			}

			for(int j=0;j<16;++j) {
				const Rom *rom=&rom_set->roms[j];
				char key[11];
				_snprintf(key,sizeof key,"%d",j);
				std::string value=mbRelativeFileName(rom->filename.c_str(),".").c_str();
				if(rom->ram) {
					value+=",ram";
				}
				cfg->Write(bm_section_names[i].c_str(),key,value.c_str());
			}
		}
	}

	mbSaveRect(cfg,ROMS_SECTION_NAME "/" DIALOG_RECT_NAME,this->dlg_rect);

	for(std::map<std::string,std::string>::const_iterator it=this->fs_roms.begin();it!=this->fs_roms.end();++it) {
		if(!it->second.empty()) {
			cfg->Write(ROMS_SECTION_NAME,("fs_rom_"+it->first).c_str(),mbRelativeFileName(it->second.c_str(),".").c_str());
		}
	}
}

//	std::vector<const mbModel *> models;
//	mbModelGetAll(&models);
//	for(unsigned i=0;i<models.size();++i) {
//		const std::string mbRomsConfig::*os_name=UniqueModelOsName(models,i);
//		if(os_name) {
//			std::string item_name=mbStrPrintf("%s_os",models[i]->short_name);
//			cfg->Write(ROMS_SECTION_NAME,item_name.c_str(),
//				mbRelativeFileName((this->*os_name).c_str(),".").c_str());
//		}
//
//		//const Rom *roms[16];
//		//models[i]->GetRomsPtrs(*this,roms);
//		for(int j=0;j<16;++j) {
//			std::string key;
//
//			if((models[i]->base_model->b_rom_mask)&(1<<j)) {
//				//B style ROM
//				key=mbStrPrintf("%d",j);
//			} else {
//				//M128 style ROM
//				key=mbStrPrintf("%s_%d",models[i]->short_name,j);
//			}
//			
//			const Rom *rom=models[i]->CfgRomPtr(*this,j);
//			this->SaveRom(cfg,key,rom);
//		}
//	}
//
//	mbSaveRect(cfg,ROMS_SECTION_NAME "/" DIALOG_RECT_NAME,this->dlg_rect);
//	cfg->Write(ROMS_SECTION_NAME,"FsSlot",this->fs_slot);
//
//	for(std::map<std::string,std::string>::const_iterator it=this->fs_roms.begin();it!=this->fs_roms.end();++it) {
//		if(!it->second.empty()) {
//			cfg->Write(ROMS_SECTION_NAME,("fs_rom_"+it->first).c_str(),mbRelativeFileName(it->second.c_str(),".").c_str());
//		}
//	}
//}

//////////////////////////////////////////////////////////////////////////
//

mbSoundConfig::mbSoundConfig():
enabled(false),
stereo(false),
hz(44100),
bits(2),
len_frames(4),
dlg_rect(-1,-1,-1,-1),
volume(100)
{
	for(unsigned i=0;i<4;++i) {
		this->is_stereo_left[i]=true;
		this->is_stereo_right[i]=true;
	}
}

void mbSoundConfig::Load(mbConfigFile *cfg) {
	cfg->Read(SOUND_SECTION_NAME,"Enabled",&this->enabled);
	cfg->Read(SOUND_SECTION_NAME,"Hz",&this->hz);
	cfg->Read(SOUND_SECTION_NAME,"Bits",&this->bits);
	cfg->Read(SOUND_SECTION_NAME,"Stereo",&this->stereo);
	cfg->Read(SOUND_SECTION_NAME,"LenFrames",&this->len_frames);
	for(unsigned i=0;i<4;++i) {
		std::string left_item_name=mbStrPrintf("%uLeft",i);
		std::string right_item_name=mbStrPrintf("%uRight",i);

		cfg->Read(SOUND_STEREO_SECTION_NAME,left_item_name.c_str(),&this->is_stereo_left[i]);
		cfg->Read(SOUND_STEREO_SECTION_NAME,right_item_name.c_str(),&this->is_stereo_right[i]);
	}

	mbLoadRect(cfg,SOUND_SECTION_NAME "/" DIALOG_RECT_NAME,&this->dlg_rect);
	cfg->Read(SOUND_SECTION_NAME,"Volume",&this->volume);

	ClampValue("Sound/LenFrames",&this->len_frames,3,int(bbcSound::max_len_frames));
}

void mbSoundConfig::Save(mbConfigFile *cfg)  const{
	cfg->Write(SOUND_SECTION_NAME,"Enabled",this->enabled);
	cfg->Write(SOUND_SECTION_NAME,"Hz",this->hz);
	cfg->Write(SOUND_SECTION_NAME,"Bits",this->bits);
	cfg->Write(SOUND_SECTION_NAME,"Stereo",this->stereo);
	cfg->Write(SOUND_SECTION_NAME,"LenFrames",this->len_frames);
	for(unsigned i=0;i<4;++i) {
		std::string left_item_name=mbStrPrintf("%uLeft",i);
		std::string right_item_name=mbStrPrintf("%uRight",i);

		cfg->Write(SOUND_STEREO_SECTION_NAME,left_item_name.c_str(),this->is_stereo_left[i]);
		cfg->Write(SOUND_STEREO_SECTION_NAME,right_item_name.c_str(),this->is_stereo_right[i]);
	}
	mbSaveRect(cfg,SOUND_SECTION_NAME "/" DIALOG_RECT_NAME,this->dlg_rect);
	cfg->Write(SOUND_SECTION_NAME,"Volume",this->volume);
}

//////////////////////////////////////////////////////////////////////////
//
mbKeyboardConfig::mbKeyboardConfig():
dlg_rect(-1,-1,-1,-1)
{
	std::fill(this->keylinks,this->keylinks+8,false);
}

void mbKeyboardConfig::Load(mbConfigFile *cfg) {
	//Keyboard links
	std::string binlinks;
	if(cfg->Read(KEYBOARD_SECTION_NAME,"Keylinks",&binlinks)) {
		if(binlinks.length()!=8) {
			wxLogWarning("Bad keylinks settings; setting to defaults.\n");
			for(unsigned i=0;i<8;++i) {
				this->keylinks[i]=false;
			}
		} else {
			for(unsigned i=0;i<8;++i) {
				if(binlinks[i]!='0'&&binlinks[i]!='1') {
					wxLogWarning("Bad character '%c' in keylinks.",binlinks[i]);
				} else {
					this->keylinks[i]=binlinks[i]=='1';
				}
				wxLogVerbose("link %u (bit %u, 0x%X) is %s\n",1+i,7-i,1<<(7-i),
					this->keylinks[i]?"set":"clear");
			}
		}
	}

	std::vector<int> keymap_sections;
	for(int i=cfg->GetFirstSection();i>=0;i=cfg->GetNextSection(i)) {
		const char *name=cfg->GetSectionName(i);

		if(strcmp(name,KEYBOARD_SECTION_NAME "/" DIALOG_RECT_NAME)!=0) {
			if(strstartswith(name,KEYBOARD_SECTION_NAME)) {
				if(name[KEYBOARD_SECTION_NAME_LEN]=='/'&&name[KEYBOARD_SECTION_NAME_LEN+1]!=0) {
					keymap_sections.push_back(i);
					wxLogDebug("%s: group \"%s\"",__FUNCTION__,cfg->GetSectionName(i));
				}
			}
		}
	}

	for(unsigned i=0;i<keymap_sections.size();++i) {
		int section=keymap_sections[i];
		wxLogVerbose("Reading keymap %s...\n",cfg->GetSectionName(section));
		mbKeymap new_keymap(cfg->GetSectionName(section)+KEYBOARD_SECTION_NAME_LEN+1);
		
		std::vector<std::string> key_names;
		std::string key_name;
		
		for(int j=cfg->GetFirstItem(section);j>=0;j=cfg->GetNextItem(section,j)) {
			const char *key_name=cfg->GetItemName(section,j);

			int bbc_code=mbCodeFromKeyName(key_name);
			if(bbc_code==bbcKEY_NONE) {
				wxLogWarning("Invalid key %s",key_name);
			} else {
				std::vector<std::string> keys;
				mbTokenizeString(cfg->GetItemValue(section,j),",",keys,true);

//					wxLogDebug(__FUNCTION__ ": Keys for %s:",key_names[j].c_str());
				for(unsigned k=0;k<keys.size();++k) {
					size_t n=keys[k].find_first_of(":");
					mbKeymap::Key mapping;
					if(n==std::string::npos) {
						mapping.key=keys[k];
//							wxLogDebug(__FUNCTION__ ":     %s",mapping.key.c_str());
					} else {
						mapping.device=keys[k].substr(0,n);
						mapping.key=keys[k].substr(n+1);
//							wxLogDebug(__FUNCTION__ ":     %s:%s",mapping.device.c_str(),mapping.key.c_str());
					}
					new_keymap.keymap[bbc_code].push_back(mapping);
				}
			}
			//new_keymap.SetKey(key_names[j],host_keys);
		}
		this->keymaps.push_back(new_keymap);
	}

	if(this->keymaps.empty()) {
		//this->keymaps.push_back(mbKeymap::DefaultKeymap());
		wxLogWarning("No keymaps found.");// -- adding default UK QWERTY\n");
	}

	cfg->Read(KEYBOARD_SECTION_NAME,"SelectedKeymap",&this->selected_keymap);
	//this->selected_keymap=cfg->Read("/Keyboard/SelectedKeymap");
	mbLoadRect(cfg,KEYBOARD_SECTION_NAME "/" DIALOG_RECT_NAME,&this->dlg_rect);
}

void mbKeyboardConfig::Save(mbConfigFile *cfg) const {
	std::string binlinks;
	for(unsigned i=0;i<8;++i) {
		binlinks+=this->keylinks[i]?"1":"0";
	}
	cfg->Write(KEYBOARD_SECTION_NAME,"Keylinks",binlinks);

	mbKeymap::C::const_iterator src;
	for(src=this->keymaps.begin();src!=this->keymaps.end();++src) {
		std::string section_name=KEYBOARD_SECTION_NAME+std::string("/")+src->name;
		for(int i=0;i<256;++i) {
			const mbKeymap::HostKeys *srckeys=&src->keymap[i];
			mbKeymap::HostKeys::const_iterator srckey;

			std::string mb_key=mbKeyNameFromCode(i).c_str();
			if(!srckeys->empty()&&!mb_key.empty()) {
				std::string all;

				for(srckey=srckeys->begin();srckey!=srckeys->end();++srckey) {
					if(!all.empty()) {
						all+=",";
					}

					if(!srckey->device.empty()) {
						all+=srckey->device+":";
					}

					all+=srckey->key;
				}

				if(!all.empty()) {
//					wxLogDebug("/Keyboard/"+src->name+"/"+mb_key+"="+all);
					//cfg->Write("/Keyboard/"+src->name+"/"+mb_key,all);
					cfg->Write(section_name.c_str(),mb_key.c_str(),all);
				}
			}
		}
	}

	cfg->Write(KEYBOARD_SECTION_NAME,"SelectedKeymap",selected_keymap);
	mbSaveRect(cfg,KEYBOARD_SECTION_NAME "/" DIALOG_RECT_NAME,this->dlg_rect);
}

//	for(mbKeymap::C::const_iterator keymap=this->keymaps.begin();keymap!=this->keymaps.end();
//		++keymap)
//	{
//		for(int i=0;i<256;++i) {
//			const mbKeymap::HostKeys *host_keys=&keymap->keymap[i];
//
//			wxString mb_key=mbKeyNameFromCode(i);
//			if(host_keys->empty()||mb_key.empty()) {
//				continue;
//			}
//
//			wxString host_keys_names;
//			for(mbKeymap::HostKeys::const_iterator host_key=host_keys->begin();
//				host_key!=host_keys->end();++host_key)
//			{
//				if(!host_keys_names.empty()) {
//					host_keys_names+=",";
//				}
//
//				if(!host_key->device.empty()) {
//					host_keys_names+=host_key->device+":";
//				}
//
//				host_keys_names+=host_key->key;
//			}
//			if(!host_keys_names.empty()) {
//				wxLogDebug("/Keyboard/"+keymap->name+"/"+mb_key+"="+host_keys_names);
//				cfg->Write("/Keyboard/"+keymap->name+"/"+mb_key,host_keys_names);
//			}
//		}
//	}
//	cfg->Write("/Keyboard/SelectedKeymap",selected_keymap);
//	mbSaveRect(cfg,"/Keyboard/DialogRect",this->dlg_rect);
//}

//////////////////////////////////////////////////////////////////////////
//

const int mbVideoConfig::max_frame_skip=50;

static mbScanlinesMode ScanlinesModeFromName(const std::string &str) {
	if(stricmp(str.c_str(),"single")==0) {
		return mbSCANLINES_SINGLE;
	} else if(stricmp(str.c_str(),"interlace")==0) {
		return mbSCANLINES_INTERLACE;
	} else if(stricmp(str.c_str(),"double")==0) {
		return mbSCANLINES_DOUBLE;
	}
	wxLogWarning("Unknown scanlines mode \"%s\" -- using single",str.c_str());
	return mbSCANLINES_SINGLE;
}

static const char *NameFromScanlinesMode(mbScanlinesMode mode) {
	switch(mode) {
	default:
	case mbSCANLINES_SINGLE:
		return "single";
	case mbSCANLINES_INTERLACE:
		return "interlace";
	case mbSCANLINES_DOUBLE:
		return "double";
	}
}

mbVideoConfig::mbVideoConfig():
width(640),
height(272),
frame_skip(1),
cleanup_frequency(100),
full_screen(false),
full_screen_refresh_rate(0),
full_screen_vsync(false),
full_screen_scanlines(mbSCANLINES_SINGLE),
windowed_scanlines(mbSCANLINES_SINGLE),
monitor_type(mbMONITOR_COLOUR)
{
}

void mbVideoConfig::Load(mbConfigFile *cfg) {
	std::string scanlines_mode_name;
	int monitor;

	cfg->Read(VIDEO_SECTION_NAME,"Width",&this->width);
	cfg->Read(VIDEO_SECTION_NAME,"Height",&this->height);
	cfg->Read(VIDEO_SECTION_NAME,"FrameSkip",&this->frame_skip);
	cfg->Read(VIDEO_SECTION_NAME,"CleanupFrequency",&this->cleanup_frequency);
	//cfg->Read(VIDEO_SECTION_NAME,"Vertical2x",&this->vertical_2x);
	cfg->Read(VIDEO_SECTION_NAME,"FullScreen",&this->full_screen);
	cfg->Read(VIDEO_SECTION_NAME,"FullScreenRefreshRate",&this->full_screen_refresh_rate);
	cfg->Read(VIDEO_SECTION_NAME,"FullScreenVsync",&this->full_screen_vsync);
	cfg->Read(VIDEO_SECTION_NAME,"MonitorType",&monitor);

	scanlines_mode_name="";
	cfg->Read(VIDEO_SECTION_NAME,"FullScreenScanlinesMode",&scanlines_mode_name);
	this->full_screen_scanlines=ScanlinesModeFromName(scanlines_mode_name);
	
	scanlines_mode_name="";
	cfg->Read(VIDEO_SECTION_NAME,"WindowedScanlinesMode",&scanlines_mode_name);
	this->windowed_scanlines=ScanlinesModeFromName(scanlines_mode_name);
	
	ClampValue("Video/Width",&this->width,bbcVideo::min_buffer_width,bbcVideo::max_buffer_width);
	ClampValue("Video/Height",&this->height,bbcVideo::min_buffer_height,bbcVideo::max_buffer_height);
	ClampValue("Video/FrameSkip",&this->frame_skip,1,max_frame_skip);
	ClampValue("Video/CleanupFrequency",&this->cleanup_frequency,1,-1);
	ClampValue("Video/MonitorType",&monitor,0,mbMONITOR_NUM_TYPES-1);

	mbLoadRect(cfg,VIDEO_SECTION_NAME "/" DIALOG_RECT_NAME,&this->dlg_rect);
	this->monitor_type=mbMonitorType(monitor);

	cfg->Read(VIDEO_SECTION_NAME,"FullScreenAdapter",&this->full_screen_adapter);
}

void mbVideoConfig::Save(mbConfigFile *cfg) const {
	cfg->Write(VIDEO_SECTION_NAME,"Width",this->width);
	cfg->Write(VIDEO_SECTION_NAME,"Height",this->height);
	cfg->Write(VIDEO_SECTION_NAME,"FrameSkip",this->frame_skip);
	cfg->Write(VIDEO_SECTION_NAME,"CleanupFrequency",this->cleanup_frequency);
	//cfg->Write(VIDEO_SECTION_NAME,"Vertical2x",this->vertical_2x);
	cfg->Write(VIDEO_SECTION_NAME,"FullScreen",this->full_screen);
	cfg->Write(VIDEO_SECTION_NAME,"FullScreenScanlinesMode",NameFromScanlinesMode(this->full_screen_scanlines));
	cfg->Write(VIDEO_SECTION_NAME,"WindowedScanlinesMode",NameFromScanlinesMode(this->windowed_scanlines));
	cfg->Write(VIDEO_SECTION_NAME,"FullScreenRefreshRate",this->full_screen_refresh_rate);
	cfg->Write(VIDEO_SECTION_NAME,"FullScreenVsync",this->full_screen_vsync);
	cfg->Write(VIDEO_SECTION_NAME,"MonitorType",this->monitor_type);
	mbSaveRect(cfg,VIDEO_SECTION_NAME "/" DIALOG_RECT_NAME,this->dlg_rect);
	cfg->Write(VIDEO_SECTION_NAME,"FullScreenAdapter",this->full_screen_adapter);
}
	
//////////////////////////////////////////////////////////////////////////
//

mbMiscConfig::mbMiscConfig():
limit_speed(false),
show_status_bar(true),
show_menu(true),
fast_forward_disc_access(false),
model("b"),
window_rect(-1,-1,-1,-1),
window_is_maximized(false)
{
}

void mbMiscConfig::AddRecentDisc(const std::string &filename) {
	wxFileName f(filename.c_str());
	if(f.IsOk()) {
		for(unsigned i=0;i<this->recent_discs.size();++i) {
			wxFileName r(this->recent_discs[i].c_str());
			if(r.IsOk()&&f.SameAs(r)) {
				return;
			}
		}

		if(this->recent_discs.size()==max_recent_discs) {
			this->recent_discs.erase(this->recent_discs.begin());
		}
		this->recent_discs.push_back(filename);
		wxLogDebug("%s: added recent disc \"%s\"",__FUNCTION__,filename.c_str());
	}
}

void mbMiscConfig::Load(mbConfigFile *cfg) {
	cfg->Read(MISC_SECTION_NAME,"LimitSpeed",&this->limit_speed);
	//cfg->Read(MISC_SECTION_NAME,"DiscInterface",&this->disc_interface);
	cfg->Read(MISC_SECTION_NAME,"ShowStatusBar",&this->show_status_bar);
	cfg->Read(MISC_SECTION_NAME,"ShowMenu",&this->show_menu);
	cfg->Read(MISC_SECTION_NAME,"FastForwardDiscAccess",&this->fast_forward_disc_access);
	cfg->Read(MISC_SECTION_NAME,"Model",&this->model);
	mbLoadRect(cfg,MISC_SECTION_NAME "/WindowRect",&this->window_rect);
	cfg->Read(MISC_SECTION_NAME,"WindowMaximized",&this->window_is_maximized);
	mbLoadRect(cfg,MISC_SECTION_NAME "/HardwareDialogRect",&this->hardware_dlg_rect);

	this->recent_discs.clear();
	for(unsigned i=0;i<max_recent_discs;++i) {
		std::string str;
		std::string item_name=mbStrPrintf("%u",i);
		if(cfg->Read(MISC_SECTION_NAME "/RecentDiscs",item_name.c_str(),&str)) {
			wxFileName fn(str.c_str());
			if(fn.IsOk()) {
				this->recent_discs.push_back(str);
			}
		}
	}
}

void mbMiscConfig::Save(mbConfigFile *cfg) const {
	cfg->Write(MISC_SECTION_NAME,"LimitSpeed",this->limit_speed);
	cfg->DeleteItemByName(MISC_SECTION_NAME,"DiscInterface");
	cfg->Write(MISC_SECTION_NAME,"ShowStatusBar",this->show_status_bar);
	cfg->Write(MISC_SECTION_NAME,"ShowMenu",this->show_menu);
	cfg->Write(MISC_SECTION_NAME,"FastForwardDiscAccess",this->fast_forward_disc_access);
	cfg->Write(MISC_SECTION_NAME,"Model",this->model);
	mbSaveRect(cfg,MISC_SECTION_NAME "/WindowRect",this->window_rect);
	cfg->Write(MISC_SECTION_NAME,"WindowMaximized",this->window_is_maximized);
	mbSaveRect(cfg,MISC_SECTION_NAME "/HardwareDialogRect",this->hardware_dlg_rect);

	for(unsigned i=0;i<max_recent_discs;++i) {
		std::string str;
		std::string item_name=mbStrPrintf("%u",i);

		if(i<this->recent_discs.size()) {
			str=this->recent_discs[i];
		}

		cfg->Write(MISC_SECTION_NAME "/RecentDiscs",item_name.c_str(),str);
	}
}

//////////////////////////////////////////////////////////////////////////
//
mbQuickstartConfig::mbQuickstartConfig():
dialog_pos(-1,-1,-1,-1)
{
}

void mbQuickstartConfig::Load(mbConfigFile *cfg) {
	unsigned i=0;
	bool done=false;
	do {
		std::string key,value;
		key=mbStrPrintf("%u",i++);
		if(!cfg->Read(QUICKSTART_SECTION_NAME "/Dirs",key.c_str(),&value)) {
			done=true;
		} else {
			this->dirs.push_back(value);
			if(!wxFileName(this->dirs.back().c_str()).IsOk()) {
				wxLogWarning("Ignoring invalid quickstart dir\n%s",value.c_str());
				this->dirs.pop_back();
			}
		}
	} while(!done);

	mbLoadRect(cfg,QUICKSTART_SECTION_NAME "/" DIALOG_RECT_NAME,&this->dialog_pos);
}

void mbQuickstartConfig::Save(mbConfigFile *cfg)  const{
	cfg->DeleteSectionByName(QUICKSTART_SECTION_NAME "/Dirs");
	for(unsigned i=0;i<this->dirs.size();++i) {
		wxString key;
		key.Printf("%u",i);
		cfg->Write(QUICKSTART_SECTION_NAME "/Dirs",key,this->dirs[i]);
	}

	mbSaveRect(cfg,QUICKSTART_SECTION_NAME "/" DIALOG_RECT_NAME,this->dialog_pos);
}

//////////////////////////////////////////////////////////////////////////
//

mbJoysticksConfig::BeebStick::BeebStick():
enabled(false),
x_axis(X),
y_axis(Y),
button(-1)
{
}

mbJoysticksConfig::mbJoysticksConfig() {
}

void mbJoysticksConfig::Load(mbConfigFile *cfg) {
	for(unsigned i=0;i<2;++i) {
		std::string section=mbStrPrintf("Joysticks/%u",i);
		BeebStick *dest=&this->joysticks[i];
		int x_axis,y_axis;

		dest->enabled=false;
		cfg->Read(section.c_str(),"Enabled",&dest->enabled);
		dest->device_name.clear();
		cfg->Read(section.c_str(),"Device",&dest->device_name);
		dest->x_axis=INVALID_AXIS;
		cfg->Read(section.c_str(),"XAxis",&x_axis);
		dest->y_axis=INVALID_AXIS;
		cfg->Read(section.c_str(),"YAxis",&y_axis);
		dest->button=0;
		cfg->Read(section.c_str(),"ButtonIndex",&dest->button);
		
		ClampValue(section+"/ButtonIndex",&dest->button,-1,HostInp_num_joystick_buttons-1);
		ClampValue(section+"/XAxis",&x_axis,0,int(INVALID_AXIS));
		ClampValue(section+"/YAxis",&y_axis,0,int(INVALID_AXIS));

		dest->x_axis=Axis(x_axis);
		dest->y_axis=Axis(y_axis);
	}
}

void mbJoysticksConfig::Save(mbConfigFile *cfg) const {
	for(unsigned i=0;i<2;++i) {
		wxString section=wxString::Format("Joysticks/%u",i);
		const BeebStick *src=&this->joysticks[i];

		cfg->Write(section,"Enabled",src->enabled);
		cfg->Write(section,"Device",src->device_name);
		cfg->Write(section,"XAxis",src->x_axis);
		cfg->Write(section,"YAxis",src->y_axis);
		cfg->Write(section,"ButtonIndex",src->button);
	}
}

//////////////////////////////////////////////////////////////////////////
//
mbCmosConfig::mbCmosConfig() {
//	static char check_cmos_config_contents_size[sizeof this->contents==bbcComputer::cmos_size];
	memset(this->contents,0,sizeof this->contents);
}

static t65::byte unhex(char ch) {
	if(isdigit(ch)) {
		return ch-'0';
	} else {
		return 10+tolower(ch)-'a';
	}
}

void mbCmosConfig::Load(mbConfigFile *cfg) {
	std::string value;
	cfg->Read("Cmos","Contents",&value);

	if(value.find_first_not_of("0123456789ABCDEFabcdef")==std::string::npos&&value.length()==sizeof this->contents*2) {
		for(unsigned i=0;i<sizeof this->contents;++i) {
			this->contents[i]=unhex(value[i*2+0])*16+unhex(value[i*2+1]);
		}
	} else {
		memset(this->contents,0,sizeof this->contents);
	}
}

void mbCmosConfig::Save(mbConfigFile *cfg) const {
	wxString value;
	for(unsigned i=0;i<sizeof this->contents;++i) {
		value+=wxString::Format("%02X",this->contents[i]);
	}
	cfg->Write("Cmos","Contents",value);
}

//////////////////////////////////////////////////////////////////////////
//
mbDebugConfig::mbDebugConfig():
show_visual_profile(false)
{
}

void mbDebugConfig::Load(mbConfigFile *cfg) {
	cfg->Read(DEBUG_SECTION_NAME,"ShowVisualProfile",&this->show_visual_profile);
}

void mbDebugConfig::Save(mbConfigFile *cfg) const {
	cfg->Write(DEBUG_SECTION_NAME,"ShowVisualProfile",this->show_visual_profile);
}

//////////////////////////////////////////////////////////////////////////

mbFileDlgConfig::mbFileDlgConfig():
filter_idx(0)
{
}

mbFileDlgConfig::mbFileDlgConfig(const char *default_folder_in,int filter_idx_in):
default_folder(default_folder_in),
filter_idx(filter_idx_in)
{
}

//////////////////////////////////////////////////////////////////////////

//static const char *default_filename="./beeb.ini";

void mbConfig::GetConfig(mbConfigFile *cfg) const {
	this->roms.Save(cfg);
	this->sound.Save(cfg);
	this->keyboard.Save(cfg);
	this->video.Save(cfg);
	this->misc.Save(cfg);
	this->quickstart.Save(cfg);
	this->joysticks.Save(cfg);
	this->cmos.Save(cfg);
	this->profiles.Save(cfg);
	this->debug.Save(cfg);

	for(std::map<std::string,mbFileDlgConfig>::const_iterator it=this->file_dlgs.begin();it!=this->file_dlgs.end();++it)
	{
		std::string section=FILE_DLG_PREFIX;
		section+=it->first;
		cfg->Write(section.c_str(),"DefaultFolder",it->second.default_folder);
		cfg->Write(section.c_str(),"FilterIdx",it->second.filter_idx);
	}
}

void mbConfig::Load(const std::string &config_file_name) {
	//This is to get around the possibility that a file specified without a
	//path will end up coming from some kind of systemwide config type place,
	//as is the case on Windows.
	mbIniFile conf;
	if(conf.Load(config_file_name.c_str())) {
		this->roms.Load(&conf);
		this->sound.Load(&conf);
		this->keyboard.Load(&conf);
		this->video.Load(&conf);
		this->misc.Load(&conf);
		this->quickstart.Load(&conf);
		this->joysticks.Load(&conf);
		this->cmos.Load(&conf);
		this->profiles.Load(&conf);
		this->debug.Load(&conf);

		// What a hack!
		// DiscInterface entry is deleted in mbMiscConfig::Save
		std::string disc_interface;
		if(conf.Read(MISC_SECTION_NAME,"DiscInterface",&disc_interface)&&!disc_interface.empty()) {
			for(mbRomsConfig::RomSets::iterator it=this->roms.rom_sets.begin();it!=this->roms.rom_sets.end();++it) {
				it->second.disc_interface=disc_interface;
			}
		}

		for(int i=conf.GetFirstSection();i>=0;i=conf.GetNextSection(i)) {
			const char *section_name=conf.GetSectionName(i);
			if(strstartswith(section_name,FILE_DLG_PREFIX)) {
				if(strlen(section_name)>FILE_DLG_PREFIX_LEN) {
					mbFileDlgConfig fdcfg;
					if(conf.Read(section_name,"DefaultFolder",&fdcfg.default_folder)&&
						conf.Read(section_name,"FilterIdx",&fdcfg.filter_idx))
					{
						this->file_dlgs[section_name+FILE_DLG_PREFIX_LEN]=fdcfg;
					}
				}
			}
		}
	}
}

void mbConfig::SetFilename(const std::string &config_file_name,bool is_read_only) {
	this->read_only=is_read_only;
	this->file_name=wxFileName::FileName(config_file_name.c_str()).GetFullPath().c_str();
}

void mbConfig::Save() const {
	if(this->read_only) {
		return;
	}
	mbIniFile conf;
	conf.Load(this->file_name.c_str());
	this->GetConfig(&conf);
	conf.Save(this->file_name.c_str());
}

mbConfig::mbConfig():
read_only(false)
{
}
