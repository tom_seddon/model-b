struct Keycap {
	bbcKey beeb_key;
	const char *unshifted,*shifted;
	int width_in_halves;
	int colour;
	
	int WidthInHalves() const;
	wxString Caption() const;
	wxColour Colour() const;
};

wxColour Keycap::Colour() const {
	switch(this->colour) {
	case 0:
		return *wxBLACK;
	case 1:
		return *wxRED;
	case 2:
		return *wxLIGHT_GREY;
	}
	wxASSERT(false);
	return wxColour();
}

int Keycap::WidthInHalves() const {
//	return 1;
	if(this->width_in_halves==0) {
		return 2;
	}
	return this->width_in_halves;
}

wxString Keycap::Caption() const {
	wxString r;
	if(this->shifted) {
		r+=this->shifted;
	}
	if(this->unshifted) {
		if(!r.empty()) {
			r+="\n";
		}
		r+=this->unshifted;
	}
	return r;
}

#define FKEY(N) {bbcKEY_F##N,"f" #N,0,2,1}
#define LETTER(L) {bbcKEY_##L,#L}
#define EDITKEY(N) N,0,2,2,
//////////////////////////////////////////////////////////////////////////
// BBC keyboard caps
static const Keycap line1[]={
	{bbcKEY_NONE,0,0,5},//spacer
	FKEY(0),FKEY(1),FKEY(2),FKEY(3),FKEY(4),
	FKEY(5),FKEY(6),FKEY(7),FKEY(8),FKEY(9),
	{bbcKEY_BREAK,		"BRK"},
#ifdef bbcENABLE_M128
	{bbcKEY_NONE,		0,0,7},
	{bbcKEY_KP_PLUS,	"+"},
	{bbcKEY_KP_MINUS,	"-"},
	{bbcKEY_KP_SLASH,	"/"},
	{bbcKEY_KP_STAR,	"*"},
#endif
};

static const Keycap line2[]={
	{bbcKEY_ESCAPE,	"ESC"},
	{bbcKEY_1,			"1",	"!"},
	{bbcKEY_2,			"2",	"\""},
	{bbcKEY_3,			"3",	"#"},
	{bbcKEY_4,			"4",	"$"},
	{bbcKEY_5,			"5",	"%"},
	{bbcKEY_6,			"6",	"&&"},
	{bbcKEY_7,			"7",	"'"},
	{bbcKEY_8,			"8",	"("},
	{bbcKEY_9,			"9",	")"},
	{bbcKEY_0,			"0",	""},
	{bbcKEY_MINUS,		"-",	"="},
	{bbcKEY_TILDE,		"^",	"~"},
	{bbcKEY_BACKSLASH,	"\\",	"|"},
	{bbcKEY_LEFT,		EDITKEY("Lf")},
	{bbcKEY_RIGHT,		EDITKEY("Rt")},
#ifdef bbcENABLE_M128
	{bbcKEY_NONE,		0,0,2},
	{bbcKEY_KP_7,		"7"},
	{bbcKEY_KP_8,		"8"},
	{bbcKEY_KP_9,		"9"},
	{bbcKEY_KP_HASH,	"#"},
#endif
};

static const Keycap line3[]={
	{bbcKEY_TAB,		"TAB",	0,	3},
	LETTER(Q),LETTER(W),LETTER(E),LETTER(R),LETTER(T),LETTER(Y),
	LETTER(U),LETTER(I),LETTER(O),LETTER(P),
	{bbcKEY_AT,		"@"},
	{bbcKEY_LSQBRACKET,"[",	"{"},
	{bbcKEY_UNDERLINE,	"_",	"�"},
	{bbcKEY_UP,		EDITKEY("Up")},
	{bbcKEY_DOWN,		EDITKEY("Dn")},
#ifdef bbcENABLE_M128
	{bbcKEY_NONE,		0,0,1},
	{bbcKEY_KP_4,		"4"},
	{bbcKEY_KP_5,		"5"},
	{bbcKEY_KP_6,		"6"},
	{bbcKEY_KP_DEL,		"DEL"},
#endif
};

static const Keycap line4[]={
	{bbcKEY_CAPSLOCK,	"LK",	"CAP"},
	{bbcKEY_CTRL,		"CTL"},
	LETTER(A),LETTER(S),LETTER(D),LETTER(F),LETTER(G),LETTER(H),LETTER(J),
	LETTER(K),LETTER(L),
	{bbcKEY_SEMICOLON,	";",	"+"},
	{bbcKEY_COLON,		":",	"*"},
	{bbcKEY_RSQBRACKET,"]",	"}"},
	{bbcKEY_RETURN,	"RETURN",0,4},
#ifdef bbcENABLE_M128
	{bbcKEY_NONE,		0,0,2},
	{bbcKEY_KP_1,		"1"},
	{bbcKEY_KP_2,		"2"},
	{bbcKEY_KP_3,		"3"},
	{bbcKEY_KP_STOP,	"."},
#endif
};

static const Keycap line5[]={
	{bbcKEY_SHIFTLOCK,	"LK",	"SHF"},
	{bbcKEY_SHIFT,		"SHIFT",0,3},
	LETTER(Z),LETTER(X),LETTER(C),LETTER(V),LETTER(B),LETTER(N),LETTER(M),
	{bbcKEY_COMMA,		",",	"<"},
	{bbcKEY_STOP,		".",	">"},
	{bbcKEY_SLASH,		"/",	"?"},
	{bbcKEY_SHIFT,		"SHIFT",0,3},
	{bbcKEY_DELETE,	"DEL"},
	{bbcKEY_COPY,		EDITKEY("CPY")},
#ifdef bbcENABLE_M128
	{bbcKEY_NONE,		0,0,2},
	{bbcKEY_KP_0,		"0"},
	{bbcKEY_KP_COMMA,	","},
	{bbcKEY_KP_RETURN,	"RETURN",0,4},
#endif
};

static const Keycap line6[]={
	{bbcKEY_NONE,0,0,8},//spacer
	{bbcKEY_SPACE,"",0,16},
};

struct KeyboardRow {
	const Keycap *keys;
	unsigned num_keys;
	int TotalWidthInHalves() const;
};

int KeyboardRow::TotalWidthInHalves() const {
	int r=0;
	for(unsigned i=0;i<this->num_keys;++i) {
		r+=this->keys[i].WidthInHalves();
	}
	return r;
}

#define KBROW(X) {(X),sizeof (X)/sizeof (X)[0]}

const KeyboardRow keyboard_rows[]={
	KBROW(line1),
	KBROW(line2),
	KBROW(line3),
	KBROW(line4),
	KBROW(line5),
	KBROW(line6),
};

const unsigned num_keyboard_rows=sizeof keyboard_rows/sizeof keyboard_rows[0];
