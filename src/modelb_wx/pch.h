// (disable C++ exceptions in STL)
#define _HAS_EXCEPTIONS 1
//#define _STATIC_CPPLIB

#include "../port.h"
#include <base/base.h>

//#pragma warning(disable:4786)

#include <wx/wx.h>
//#include <wx/config.h>
//#include <wx/fileconf.h>
//#include <wx/frame.h>
//#include <wx/fs_inet.h>
//#include <wx/fs_zip.h>
//#include <wx/image.h>
//#include <wx/listbox.h>
#include <wx/listctrl.h>
//#include <wx/valgen.h>
//#include <wx/wfstream.h>
//#include <wx/event.h>
//#include <wx/cmdline.h>
//#include <wx/slider.h>
//#include <wx/dnd.h>
//#include <wx/progdlg.h>
#include <wx/notebook.h>
//#include <wx/zstream.h>
//#include <wx/mstream.h>

#include "twDebug.h"
#include <bbcCommon.h>

#include <vector>
#include <string>
//#include <algorithm>
//#include <set>

static const char mb_version[]=__DATE__;

//////////////////////////////////////////////////////////////////////////
//Debug thingies. todo rename to match mbXXX scheme...
#ifdef bbcDEBUG_ENABLE

//Save Challenger RAM disk (more for testing, you can't load it back in)
//#define BEEB_DEBUG_SAVE_CHALLENGER_RAM

#endif

#ifdef bbcENABLE_PROFILER
#define mbVISUAL_PROFILER
#endif

//////////////////////////////////////////////////////////////////////////

// http://wiki.wxwidgets.org/Converting_everything_to_and_from_wxString
//
// I'm sure this wasn't always like this? Anyway...
#define MB_STR(X) ((const char *)(X).mb_str())
