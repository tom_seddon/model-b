#ifndef mbKEYS_H_
#define mbKEYS_H_

#include <bbcKeys.h>
#include <Host.h>

enum mbKeys {
//	mbKEY_QUICKQUIT=bbcKEY_HOST_KEYS_START,
};

int mbCodeFromKeyName(const char *name);

wxString mbKeyNameFromCode(int code);

#endif

