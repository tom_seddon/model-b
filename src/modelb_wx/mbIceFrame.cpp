#include "pch.h"
#ifdef bbcENABLE_BEEB_ICE
#include "mbIceFrame.h"
#include "mbConfigFile.h"

//////////////////////////////////////////////////////////////////////////

// A nano factory creates default-constructible objects derived from type t.
template<class T>
class mbFactory
{
public:
	mbFactory();
	~mbFactory();

	T *Create(const wxString &type_name)
	{
		return this->GetCreator(type_name)->Create();
	}

	template<class A0>
	T *Create(const wxString &type_name,A0 arg0)
	{
		return this->GetCreator(type_name)->Create(arg0);
	}
protected:
private:
};

//////////////////////////////////////////////////////////////////////////

mbIcePanel::mbIcePanel(wxWindow *parent,wxWindowID id,const wxString &title):
wxPanel(parent,id),
title_(title)
{
}

const wxString &mbIcePanel::GetIceTitle() const
{
	return title_;
}

void mbIcePanel::SetIceTitle(const wxString &new_title)
{
	title_=new_title;
}

//////////////////////////////////////////////////////////////////////////

mbIceConfig::mbIceConfig():
enabled(false)
{
}

bool mbIceConfig::Load(const std::string &file_name_in)
{
	this->file_name=file_name_in;

	this->enabled=true;

	mbIniFile ini;
	if(!ini.Load(this->file_name.c_str()))
		return false;

	mbLoadWindowPlacement(&ini,"IceWindowPlacement",&this->placement);
	return true;
}

void mbIceConfig::Save() const
{
	if(!this->file_name.empty())
	{
		mbIniFile ini;

		mbSaveWindowPlacement(&ini,"IceWindowPlacement",this->placement);

		ini.Save(this->file_name.c_str());
	}
}

//////////////////////////////////////////////////////////////////////////
	
mbIceFrame::mbIceFrame(wxWindow *parent,const mbIceConfig &cfg):
wxFrame(parent,-1,"BeebIce",wxDefaultPosition,wxDefaultSize,wxFRAME_FLOAT_ON_PARENT|wxCAPTION|
	wxMAXIMIZE_BOX|wxRESIZE_BORDER|wxSYSTEM_MENU),
cfg_(cfg)
{
	mbSetWindowPlacement(this,cfg_.placement);
}

void mbIceFrame::GetConfig(mbIceConfig *cfg) const
{
	mbGetWindowPlacement(this,&cfg_.placement);

	*cfg=cfg_;
}

void mbIceFrame::OnClose(wxCloseEvent &) {
}

BEGIN_EVENT_TABLE(mbIceFrame,wxFrame)
	EVT_CLOSE(mbIceFrame::OnClose)
END_EVENT_TABLE()

#endif//bbcENABLE_BEEB_ICE
