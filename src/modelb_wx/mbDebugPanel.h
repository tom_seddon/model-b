#ifndef mbDEBUGPANEL_H_
#define mbDEBUGPANEL_H_

#ifdef bbcDEBUG_PANELS

#include "bbcDebugPanel.h"

class mbDebugPanel:
public wxFrame
{
public:
	mbDebugPanel(wxWindow *parent,wxWindowID id,const wxString &title);

	bbcDebugPanel *Panel();

	void UpdateFromPanel();
protected:
private:
	int width_,height_;

	wxFont font_;
	wxSizer *all_sz_;
	bbcDebugPanel panel_;
	std::vector<wxStaticText *> lines_;

	DECLARE_EVENT_TABLE()
};

#endif//bbcDEBUG_PANELS

#endif
