#ifndef MBDISCINTERFACE_H_
#define MBDISCINTERFACE_H_

#include <bbc1770Interface.h>

static const int mb_disc_interface_short_name_max_size=16;

struct mbDiscInterface {
	char short_name[mb_disc_interface_short_name_max_size];
	const char *long_name;
	const bbc1770Interface *iface;
};

const mbDiscInterface *mbDiscInterfaceFromShortName(const char *short_name);
void mbDiscInterfaceGetAll(std::vector<const mbDiscInterface *> *all_interfaces);

#endif
