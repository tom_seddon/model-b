#include "pch.h"
#include "mbKeyboardConfigDialog.h"
#include "Host.h"
#include <bbcKeys.h>
#include "mbKeys.h"
#include "mbMisc.h"
#include "mbJoyKeys.h"
#include <set>
#include <algorithm>

static const int border_size=6;

//////////////////////////////////////////////////////////////////////////
//
#include "mbKeyboardLayout.inl"

//static unsigned StickCodeFromDirection(unsigned dir) {
//	static unsigned table[16];
//	static bool table_init=false;
//
//	if(!table_init) {
//		for(unsigned i=0;i<16;++i) {
//			table[i]=HostInp_bad_key;
//		}
//		table[NORTH]=KEY_STICK_N;
//		table[NORTH|EAST]=KEY_STICK_NE;
//		table[EAST]=KEY_STICK_E;
//		table[SOUTH|EAST]=KEY_STICK_SE;
//		table[SOUTH]=KEY_STICK_S;
//		table[SOUTH|WEST]=KEY_STICK_SW;
//		table[WEST]=KEY_STICK_W;
//		table[NORTH|WEST]=KEY_STICK_NW;
//		table_init=true;
//	}
//
//	if(table[dir]==HostInp_bad_key) {
//		return HostInp_bad_key;
//	} else {
//		return table[dir]|KEY_IS_JOYSTICK;
//	}
//}

//////////////////////////////////////////////////////////////////////////
//
class SaveStringDialog:
public wxDialog
{
public:
	SaveStringDialog(wxWindow *parent,const wxArrayString &strings,
		const wxString &title,const wxString &caption,const wxString &def="");

	const wxString &Result() const;
protected:
private:
//	enum {
//		id_list=1,
//		id_input,
//	};
	wxArrayString strings_;
	wxListBox *list_;
	wxTextCtrl *text_;
	wxString result_;

	void RefreshDialog();
	void OnOk(wxCommandEvent &);
	void OnList(wxCommandEvent &);

	DECLARE_EVENT_TABLE()
};

const wxWindowID id_list=mbNewId();
const wxWindowID id_input=mbNewId();

SaveStringDialog::SaveStringDialog(wxWindow *parent,const wxArrayString &strings,
	const wxString &title,const wxString &caption,const wxString &def):
wxDialog(parent,-1,title,wxDefaultPosition,wxDefaultSize,
	wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER),
strings_(strings)
{
	//Controls
	wxStaticText *caption_st=0;
	if(!caption.empty()) {
		caption_st=new wxStaticText(this,-1,caption);
	}
	list_=new wxListBox(this,id_list,wxDefaultPosition,wxDefaultSize,0,0,
		wxLB_SINGLE|wxLB_SORT);
	list_->Append(strings_);
	wxButton *ok=new wxButton(this,wxID_OK,"OK");
	ok->SetDefault();
	wxButton *cancel=new wxButton(this,wxID_CANCEL,"Cancel");
	text_=new wxTextCtrl(this,id_input);//,wxEmptyString,wxDefaultPosition,wxDefaultSize,)

	//Layout
	wxBoxSizer *btns_sz=new wxBoxSizer(wxHORIZONTAL);
	btns_sz->Add(ok,0,wxALL|wxALIGN_RIGHT,border_size);
	btns_sz->Add(cancel,0,wxALL|wxALIGN_RIGHT,border_size);
	wxBoxSizer *all_sz=new wxBoxSizer(wxVERTICAL);
	if(caption_st) {
		all_sz->Add(caption_st,0,wxALL,border_size);
	}
	all_sz->Add(text_,0,wxALL|wxGROW,border_size);
	all_sz->Add(list_,1,wxALL|wxGROW,border_size);
	all_sz->Add(btns_sz,0,0);

	this->SetSizer(all_sz);
	all_sz->SetSizeHints(this);

	text_->Clear();
	text_->AppendText(def);
	this->RefreshDialog();
}

void SaveStringDialog::RefreshDialog() {
	wxString cur=text_->GetLineText(0);
	int idx=list_->FindString(cur);
	if(idx>=0) {
		list_->SetSelection(idx);
	}
}

void SaveStringDialog::OnList(wxCommandEvent &event) {
	bool double_click=false;

	WXTYPE event_type=event.GetEventType();
	if(event_type==wxEVT_COMMAND_LISTBOX_DOUBLECLICKED) {
		double_click=true;
	} else if(event_type!=wxEVT_COMMAND_LISTBOX_SELECTED) {
		//Not interested in anything else!
		return;
	}

	int idx=list_->GetSelection();
	wxString selection=list_->GetString(idx);
	text_->Clear();
	text_->AppendText(selection);
	if(double_click) {
		wxCommandEvent tmp_event;
		this->OnOk(tmp_event);
	}
}

void SaveStringDialog::OnOk(wxCommandEvent &) {
	wxString cur=text_->GetLineText(0);
	bool do_save=true;
	if(list_->FindString(cur)>=0) {
		wxString msg=wxString::Format("Are you sure you want to overwrite\n\"%s\"?",
			cur.c_str());
		do_save=wxMessageBox(msg,"Overwrite?",wxYES_NO,this)==wxYES;
	}
	if(do_save) {
		result_=cur;
	}
	this->EndModal(wxID_OK);
}

const wxString &SaveStringDialog::Result() const {
	return result_;
}

BEGIN_EVENT_TABLE(SaveStringDialog,wxDialog)
	EVT_BUTTON(wxID_OK,SaveStringDialog::OnOk)
	EVT_LISTBOX(id_list,SaveStringDialog::OnList)
	EVT_LISTBOX_DCLICK(id_list,SaveStringDialog::OnList)
END_EVENT_TABLE()

//////////////////////////////////////////////////////////////////////////
//
class SelectHostKeysDialog:
public wxDialog
{
public:
	SelectHostKeysDialog(wxWindow *parent,const mbKeymap::HostKeys *host_keys,const Keycap *keycap);
	~SelectHostKeysDialog();
	void GetHostKeys(mbKeymap::HostKeys *host_keys);
protected:
private:
	struct Key {
		bool is_joystick;
		int key_code;
		int joy_index;
		mbJoyKey joy_key;

		bool operator==(const Key &rhs);
		bool operator!=(const Key &rhs);
	};

	HostInpState *hostinp_;
	wxListBox *host_keys_lb_;
	std::vector<HostInp_JoyState> sticks_;
	std::vector<Key> keys_;

	void ToggleKeyState(const Key &key);
	void OnIdle(wxIdleEvent &event);
	void FillListBoxWithHostKeys();
	void OnOk(wxCommandEvent &event);
#ifdef PORT_USE_WX_KEYBOARD
	void OnKeyDown(wxKeyEvent &event);
	void OnKeyUp(wxKeyEvent &event);
	std::set<long> wxkeys_;
	std::set<long> last_idle_wxkeys_;
#endif

	DECLARE_EVENT_TABLE()
};

bool SelectHostKeysDialog::Key::operator==(const Key &rhs) {
	if(this->is_joystick!=rhs.is_joystick) {
		return false;
	}

	if(this->is_joystick) {
		return this->joy_index==rhs.joy_index&&this->joy_key==rhs.joy_key;
	} else {
		return this->key_code==rhs.key_code;
	}
}

bool SelectHostKeysDialog::Key::operator!=(const Key &rhs) {
	return !(*this==rhs);
}

SelectHostKeysDialog::SelectHostKeysDialog(wxWindow *parent,const mbKeymap::HostKeys *host_keys,const Keycap *keycap):
wxDialog(parent,-1,"Select keys ("+mbKeyNameFromCode(keycap->beeb_key)+")",wxDefaultPosition,wxDefaultSize,
		wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER),
hostinp_(0),
host_keys_lb_(0)
{
	hostinp_=HostInp_Start(this->GetHandle());
	wxBoxSizer *btns_sz=new wxBoxSizer(wxVERTICAL);
	wxButton *ok=new wxButton(this,wxID_OK,"OK");
	wxButton *cancel=new wxButton(this,wxID_CANCEL,"Cancel");
	btns_sz->Add(ok,1,wxALL,border_size);
	btns_sz->Add(cancel,1,wxALL,border_size);
	host_keys_lb_=new wxListBox(this,-1,wxDefaultPosition,wxDefaultSize,0,0,
		wxLB_SORT|wxLB_SINGLE);
	wxBoxSizer *all_sz=new wxBoxSizer(wxHORIZONTAL);
	all_sz->Add(host_keys_lb_,1,wxGROW|wxALL,border_size);
	all_sz->Add(btns_sz,0,wxALL|wxALIGN_RIGHT,border_size);
	this->SetSizer(all_sz);
	all_sz->SetSizeHints(this);

	sticks_.resize(HostInp_NumJoysticks(hostinp_));
	for(unsigned i=0;i<sticks_.size();++i) {
		memset(&sticks_[i],0,sizeof sticks_[i]);
	}

	//TODO merge this with very similar code in mbMainFrame
	mbKeymap::HostKeys::const_iterator srckey;
	for(srckey=host_keys->begin();srckey!=host_keys->end();++srckey) {
		if(srckey->device.empty()) {
			Key destkey;
#ifdef PORT_USE_WX_KEYBOARD
			destkey.key_code=mbGetWxKeyCodeFromName(srckey->key);
#else
			destkey.key_code=HostInp_CodeFromKeyName(srckey->key.c_str());
#endif
			if(destkey.key_code!=HostInp_bad_key) {
				destkey.is_joystick=false;
				keys_.push_back(destkey);
			}
		} else {
			Key destkey;

			mbJoyKeyGetIndexAndKey(hostinp_,*srckey,&destkey.joy_index,&destkey.joy_key);
			if(destkey.joy_index>=0&&destkey.joy_key!=mbJOYKEY_INVALID) {
				destkey.is_joystick=true;
				keys_.push_back(destkey);
			}
		}
	}

	this->FillListBoxWithHostKeys();
}

SelectHostKeysDialog::~SelectHostKeysDialog() {
	HostInp_Stop(hostinp_);
}

void SelectHostKeysDialog::ToggleKeyState(const Key &key) {
	std::vector<Key>::iterator it=std::find(keys_.begin(),keys_.end(),key);

	if(it==keys_.end()) {
		keys_.push_back(key);
	} else {
		keys_.erase(it);
	}
}

void SelectHostKeysDialog::OnIdle(wxIdleEvent &event) {
	HostInp_Poll(hostinp_);
	bool changed=false;
#ifdef PORT_USE_WX_KEYBOARD
	// TODO not very neat.
	for(std::set<long>::const_iterator it=wxkeys_.begin();it!=wxkeys_.end();++it) {
		if(last_idle_wxkeys_.find(*it)==last_idle_wxkeys_.end()) {
			Key k;

			k.is_joystick=false;
			k.key_code=*it;

			this->ToggleKeyState(k);
			changed=true;
		}
	}
	last_idle_wxkeys_.swap(wxkeys_);
	wxkeys_.clear();
#else
	unsigned char flags[256];
	HostInp_GetKeyboardDownFlags(hostinp_,flags);

	for(unsigned i=0;i<256;++i) {
		if(flags[i]) {
			Key k;

			k.is_joystick=false;
			k.key_code=i;

			this->ToggleKeyState(k);
			changed=true;
		}
	}
#endif

	for(int i=0;i<HostInp_NumJoysticks(hostinp_);++i) {
		HostInp_JoyState state;
		HostInp_JoyState *old_state=&sticks_[i];
		mbJoyKeyState new_jk,old_jk;

		HostInp_GetJoystickState(hostinp_,i,&state);

		mbJoyKeyGetKeyStates(&state,&new_jk);
		mbJoyKeyGetKeyStates(old_state,&old_jk);

		for(unsigned j=0;j<mbJOYKEY_NUM;++j) {
			if(new_jk.down_count[j]==1) {
				Key k;

				k.is_joystick=true;
				k.joy_index=i;
				k.joy_key=mbJoyKey(j);

				this->ToggleKeyState(k);
				changed=true;
			}
		}

		*old_state=state;
	}

	if(changed) {
		this->FillListBoxWithHostKeys();
	}
	event.RequestMore();
	event.Skip();
}

void SelectHostKeysDialog::FillListBoxWithHostKeys() {
	if(!host_keys_lb_) {
		return;
	}

	host_keys_lb_->Clear();
	mbKeymap::HostKeys host_keys;
	this->GetHostKeys(&host_keys);
	for(unsigned i=0;i<host_keys.size();++i) {
		std::string name;

		name=host_keys[i].key;
		if(!host_keys[i].device.empty()) {
			name+=" on "+host_keys[i].device;
		}

		host_keys_lb_->Append(name.c_str());
	}
}

void SelectHostKeysDialog::GetHostKeys(mbKeymap::HostKeys *host_keys) {
	wxASSERT(host_keys);
	host_keys->clear();
	for(unsigned i=0;i<keys_.size();++i) {
		std::string key,device;

		if(!keys_[i].is_joystick) {
			device="";
#ifdef PORT_USE_WX_KEYBOARD
			key=mbGetKeyNameFromWxKeyCode(keys_[i].key_code);
#else
			key=HostInp_KeyNameFromCode(keys_[i].key_code);
#endif
		} else {
			device=HostInp_JoystickName(hostinp_,keys_[i].joy_index);
			key=mbJoyKeyNameFromKey(keys_[i].joy_key);
		}
		if(!key.empty()) {
			host_keys->push_back(mbKeymap::Key(device,key));
		}
	}
}

void SelectHostKeysDialog::OnOk(wxCommandEvent &event) {
	event.Skip();
}

#ifdef PORT_USE_WX_KEYBOARD
void SelectHostKeysDialog::OnKeyDown(wxKeyEvent &event) {
	wxkeys_.insert(event.GetKeyCode());
}

void SelectHostKeysDialog::OnKeyUp(wxKeyEvent &event) {
	wxkeys_.erase(event.GetKeyCode());
}

#endif

BEGIN_EVENT_TABLE(SelectHostKeysDialog,wxDialog)
	EVT_IDLE(SelectHostKeysDialog::OnIdle)
	EVT_BUTTON(wxID_OK,SelectHostKeysDialog::OnOk)
#ifdef PORT_USE_WX_KEYBOARD
	EVT_KEY_DOWN(SelectHostKeysDialog::OnKeyDown)
	EVT_KEY_UP(SelectHostKeysDialog::OnKeyUp)
#endif
END_EVENT_TABLE()

//////////////////////////////////////////////////////////////////////////
//

const wxWindowID id_base=mbNewId();
const wxWindowID id_beebkey=mbNewId();
const wxWindowID id_load=mbNewId();
const wxWindowID id_save=mbNewId();
const wxWindowID id_close=mbNewId();

mbKeyboardConfigDialog::Key::Key():
keycap(0),
button(0),
row(-1),
x(-1)
{
}

static const wxString default_title="Configure Keymap";

mbKeyboardConfigDialog::mbKeyboardConfigDialog(wxWindow *parent,
	const mbKeyboardConfig &cfg):
wxDialog(parent,-1,"Configure Keymap",wxDefaultPosition,wxDefaultSize,
	wxDEFAULT_DIALOG_STYLE),//|wxRESIZE_BORDER),
cfg_(cfg),
row_width_(0)
{
	this->SetExtraStyle(this->GetExtraStyle()|wxWS_EX_VALIDATE_RECURSIVELY);

	wxBoxSizer *all_sz=new wxBoxSizer(wxVERTICAL);

	//////////////////////////////////////////////////////////////////////////
	//Beeb kb box
	wxStaticBox *kb_box=new wxStaticBox(this,-1,"Beeb Keys");
	wxStaticBoxSizer *kb_box_sz=new wxStaticBoxSizer(kb_box,wxVERTICAL);
	wxScrolledWindow *scrollw=new wxScrolledWindow(this,-1);
	wxBoxSizer *kb_rows_sz=new wxBoxSizer(wxVERTICAL);

	unsigned row_idx;
	row_width_=0;
	for(row_idx=0;row_idx<num_keyboard_rows;++row_idx) {
		row_width_=wxMax(keyboard_rows[row_idx].TotalWidthInHalves(),row_width_);
	}
	for(row_idx=0;row_idx<num_keyboard_rows;++row_idx) {
		wxBoxSizer *row_sz=new wxBoxSizer(wxHORIZONTAL);
		const KeyboardRow *row=&keyboard_rows[row_idx];
		int x=0;
		for(unsigned key_idx=0;key_idx<row->num_keys;++key_idx) {
			const Keycap *keycap=&row->keys[key_idx];
			int w=keycap->WidthInHalves()*16;
			int h=32;
			if(!keycap->shifted&&!keycap->unshifted) {
				row_sz->Add(w,h,0);
			} else {
				Key *k=&*keys_.insert(keys_.end(),Key());
				k->keycap=keycap;
				k->row=row_idx;
				k->x=x;
				k->button=new wxButton(scrollw,id_beebkey,k->keycap->Caption(),wxDefaultPosition,wxDefaultSize);
				x+=k->keycap->WidthInHalves();
				k->button->SetSize(wxSize(w,h));
				row_sz->Add(k->button,0,wxFIXED_MINSIZE);
			}
//			Key *k=&*keys_.insert(keys_.end(),Key());
//			k->keycap=&row->keys[key_idx];
//			k->row=row_idx;
//			k->x=x;
//			k->button=new wxButton(scrollw,id_beebkey,k->keycap->Caption(),
//				wxDefaultPosition,wxDefaultSize);//,wxBU_EXACTFIT);
//			if(!k->keycap->shifted&&!k->keycap->unshifted) {
//				k->button->Show(false);
//			}
//			x+=k->keycap->WidthInHalves();
//			k->button->SetSize(wxSize(k->keycap->WidthInHalves()*16,32));
//			row_sz->Add(k->button,0,wxFIXED_MINSIZE);
		}
		kb_rows_sz->Add(row_sz);//,1,wxGROW);
	}

	//scrollw->SetScrollbars(1,1,50,50);
	scrollw->SetSizer(kb_rows_sz);
	kb_rows_sz->SetSizeHints(scrollw);
	kb_box_sz->Add(scrollw,1,wxALL|wxGROW);

	//////////////////////////////////////////////////////////////////////////
	// Buttons area
	wxBoxSizer *btns_sz=new wxBoxSizer(wxHORIZONTAL);
	wxButton *load_bt=new wxButton(this,id_load,"&Load...");
	wxButton *save_bt=new wxButton(this,id_save,"&Save...");
	close_btn_=new wxButton(this,id_close,"&Close");
	close_btn_->SetDefault();

	btns_sz->Add(load_bt,1,wxALL|wxALIGN_RIGHT|wxALIGN_BOTTOM,border_size);
	btns_sz->Add(save_bt,1,wxALL|wxALIGN_RIGHT|wxALIGN_BOTTOM,border_size);
	btns_sz->Add(close_btn_,1,wxALL|wxALIGN_RIGHT|wxALIGN_BOTTOM,border_size);
	
	//////////////////////////////////////////////////////////////////////////
	// Join them all together!
	all_sz->Add(kb_box_sz,1,wxGROW);
	all_sz->Add(btns_sz,0,wxALIGN_RIGHT);//wxALIGN_BOTTOM);

	this->SetSizer(all_sz);
	all_sz->SetSizeHints(this);

	//
	this->LoadKeymap(cfg_.selected_keymap);

	mbSetWindowPos(this,cfg_.dlg_rect.GetTopLeft());//this->Move(cfg_.dlg_rect.GetLeft(),cfg_.dlg_rect.GetTop());
}

// fixed so OK to pass 'current_keymap_.name' as keymap_name [11/5/2003]
bool mbKeyboardConfigDialog::LoadKeymap(const std::string &keymap_name) {
	mbKeymap::C::iterator km;

	current_keymap_=mbKeymap();
	for(km=cfg_.keymaps.begin();km!=cfg_.keymaps.end();++km) {
		if(km->name==keymap_name) {
			current_keymap_=*km;
			break;
		}
	}
	current_keymap_changed_=false;
	this->SetTitle(default_title+" "+current_keymap_.name.c_str());
	return true;
}

void mbKeyboardConfigDialog::Changed() {
	current_keymap_changed_=true;
}

void mbKeyboardConfigDialog::OnBeebKey(wxCommandEvent &event) {
	wxObject *src=event.GetEventObject();
	unsigned i;
	for(i=0;i<keys_.size();++i) {
		if(keys_[i].button==src) {
			break;
		}
	}
	if(i<keys_.size()) {
		const Key *key=&keys_[i];
		mbKeymap::HostKeys *oldkeys=&current_keymap_.keymap[key->keycap->beeb_key];
		SelectHostKeysDialog dlg(this,oldkeys,key->keycap);

		//TODO disabling close prevents any problems when pressing keys
		//with the list box in focus -- probably a direct input problem
		//this?
		close_btn_->Enable(false);
		if(dlg.ShowModal()==wxID_OK) {
			mbKeymap::HostKeys newkeys;
			dlg.GetHostKeys(&newkeys);
			this->Changed();
			*oldkeys=newkeys;
		}
		close_btn_->Enable(true);
	}
}

bool mbKeyboardConfigDialog::DestructiveActionConfirmed() {
	static const char *caption=
		"You will lose your changes if you do this.\n"
		"\n"
		"Are you sure?";
	int r=wxYES;
	if(current_keymap_changed_) {
		r=wxMessageBox(caption,"Are you sure?",wxYES_NO,this);
	}
	return r==wxYES;
}

void mbKeyboardConfigDialog::OnLoad(wxCommandEvent &) {
	if(this->DestructiveActionConfirmed()) {
		wxArrayString names;
		for(mbKeymap::C::iterator km=cfg_.keymaps.begin();km!=cfg_.keymaps.end();++km) {
			names.Add(km->name.c_str());
		}
		wxString selected=wxGetSingleChoice("Select keymap to load","Load keymap",
			names,this);
		if(!selected.empty()) {
			this->LoadKeymap(selected.ToStdString());
		}
	}
}

void mbKeyboardConfigDialog::OnSave(wxCommandEvent &) {
	wxArrayString names;
	mbKeymap::C::iterator km;
	for(km=cfg_.keymaps.begin();km!=cfg_.keymaps.end();++km) {
		names.Add(km->name.c_str());
	}
	SaveStringDialog dlg(this,names,"Save keymap","",current_keymap_.name.c_str());
	int r=dlg.ShowModal();
	if(r==wxID_OK) {
		std::string name=dlg.Result().c_str();
		if(!name.empty()) {
			//If there's one with the same name, get rid of it
			for(km=cfg_.keymaps.begin();km!=cfg_.keymaps.end();++km) {
				if(km->name==name) {
					break;
				}
			}
			current_keymap_.name=name;
			if(km==cfg_.keymaps.end()) {
				wxLogDebug("%s: new keymap \"%s\".",__FUNCTION__,current_keymap_.name.c_str());
				cfg_.keymaps.insert(cfg_.keymaps.end(),current_keymap_);
			} else {
				wxLogDebug("%s: overwriting existing \"%s\".",__FUNCTION__,current_keymap_.name.c_str());
				*km=current_keymap_;
			}
			this->LoadKeymap(current_keymap_.name);
		}
	}
}

void mbKeyboardConfigDialog::OnCloseButton(wxCommandEvent &) {
	this->Close();
}

void mbKeyboardConfigDialog::OnClose(wxCloseEvent &event) {
	if(event.CanVeto()&&!this->DestructiveActionConfirmed()) {
		event.Veto();
	} else {
		cfg_.dlg_rect=this->GetRect();
		this->EndModal(wxID_OK);
	}
}

void mbKeyboardConfigDialog::GetResult(mbKeyboardConfig *cfg) {
	wxASSERT(cfg);
	*cfg=cfg_;
}

BEGIN_EVENT_TABLE(mbKeyboardConfigDialog,wxDialog)
	EVT_BUTTON(id_beebkey,mbKeyboardConfigDialog::OnBeebKey)
	EVT_BUTTON(id_load,mbKeyboardConfigDialog::OnLoad)
	EVT_BUTTON(id_save,mbKeyboardConfigDialog::OnSave)
	EVT_BUTTON(id_close,mbKeyboardConfigDialog::OnCloseButton)
	EVT_CLOSE(mbKeyboardConfigDialog::OnClose)
END_EVENT_TABLE()
