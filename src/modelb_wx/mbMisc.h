#ifndef mbMISC_H_
#define mbMISC_H_

#include <wx/filename.h>

class mbConfigFile;
struct mbFileDlgConfig;

wxWindowID mbNewId();
wxWindowID mbNewIds(int n);

typedef bool (*mbSaveFileFn)(const char *filename,const std::vector<wxByte> &contents);
bool mbSaveFile(const char *filename,const std::vector<wxByte> &contents);
bool mbSaveCompressedFile(const char *filename,const std::vector<wxByte> &contents);

typedef bool (*mbLoadFileFn)(const char *filename,std::vector<wxByte> &contents);
bool mbLoadFile(const char *filename,std::vector<wxByte> &contents);
bool mbLoadCompressedFile(const char *filename,std::vector<wxByte> &contents);

void mbGetMatchingFileNames(const wxFileName &root,const wxString &name_spec,
	std::vector<wxFileName> *file_names);
void mbGetDirectoryNames(const wxFileName &root,std::vector<wxFileName> *dir_names);


//C is probably container of T
//returns true if user clicked ok, false if clicked cancel.
//
//parent -- parent window
//items -- strings representing items in cont
//cont -- the items
//title -- title for the dialog
//caption -- caption in the dialog

static inline int mbIntCompare(int *a,int *b) {
	return *b-*a;
}

template<class C>
static bool mbRemoveItemsUi(wxWindow *parent,const wxArrayString &items,C *cont,
	const wxString &title,const wxString &caption)
{
	wxASSERT(cont);
	wxASSERT(items.Count()==cont->size());
	wxArrayInt indices;
	wxGetMultipleChoices(indices,caption,title,items,parent);
	
	//Remove the indices starting at the greatest, for obvious reasons,
	//regrettably std::vector will delete only by iterator hence the
	//palaver.
	indices.Sort(&mbIntCompare);
	t65TYPENAME C::iterator end;
	end=cont->end();
	for(unsigned i=indices.GetCount();i;--i) {
		int idx=indices[i-1];
		std::swap(*--end,cont->at(idx));//oops, C must be a vector or deque for now!
	}
	cont->erase(end,cont->end());
	return indices.Count()>0;
}

wxString mbNormalizedFileName(const wxString &file_name);
wxString mbRelativeFileName(const wxString &file_name,const wxString &base_dir);

void mbTokenizeString(const char *source,const char *seperators,std::vector<std::string> &tokensOutput,bool handleQuotes);
void mbTokenizeString(const char *source,const char *seperators,std::vector<wxString> &tokensOutput,bool handleQuotes);

std::string mbStrPrintf(const char *fmt,...);

class mbSTLStringValidator:
public wxTextValidator
{
public:
	mbSTLStringValidator(long style,std::string *val);
	mbSTLStringValidator(const mbSTLStringValidator &);
	wxObject *Clone() const;
	bool TransferToWindow();
	bool TransferFromWindow();
protected:
private:
	std::string *str_;
	wxString tmp_;
};

// these will set the window in a default position if the coordinates/rect is outside the screen
// bounds. this is required in case the monitor configuration changed since the window positions
// were saved.
void mbSetWindowPos(wxWindow *w,const wxPoint &new_pos);
void mbSetWindowRect(wxWindow *w,const wxRect &new_rect);

bool mbLoadRect(mbConfigFile *cfg,const char *section,wxRect *rect);
void mbSaveRect(mbConfigFile *cfg,const char *section,const wxRect &rect);

struct mbWindowPlacement {
	bool valid;
	int show_cmd;
	wxPoint min_pos;
	wxPoint max_pos;
	wxRect rect;

	mbWindowPlacement();
};

bool mbLoadWindowPlacement(mbConfigFile *cfg,const char *section,mbWindowPlacement *wp);
void mbSaveWindowPlacement(mbConfigFile *cfg,const char *section,const mbWindowPlacement &wp);

void mbGetWindowPlacement(const wxFrame *f,mbWindowPlacement *wp);
void mbSetWindowPlacement(wxFrame *f,const mbWindowPlacement &wp);

class mbFileDlg:
public wxFileDialog
{
public:
	mbFileDlg(mbFileDlgConfig &dlg_cfg,wxWindow *parent,const wxString &msg,const wxString &wildcard,long style);
	int ShowModal();
protected:
private:
	mbFileDlgConfig *const dlg_cfg_;
};

#ifdef PORT_USE_WX_KEYBOARD
long mbGetWxKeyCodeFromName(const wxString &name);
wxString mbGetKeyNameFromWxKeyCode(long code);
unsigned mbGetNumWxKeyCodes();
#endif

#endif
