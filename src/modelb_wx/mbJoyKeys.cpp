#include "pch.h"
#include "mbJoyKeys.h"
#include "mbMisc.h"

//mbJOYKEY_TX_NEG,
//mbJOYKEY_TX_POS,
//mbJOYKEY_TY_NEG,
//mbJOYKEY_TY_POS,
//mbJOYKEY_TZ_NEG,
//mbJOYKEY_TZ_POS,
//mbJOYKEY_RX_NEG,
//mbJOYKEY_RX_POS,
//mbJOYKEY_RY_NEG,
//mbJOYKEY_RY_POS,
//mbJOYKEY_RZ_NEG,
//mbJOYKEY_RZ_POS,
//mbJOYKEY_BUTTON_0,
//mbJOYKEY_BUTTON_LAST=mbJOYKEY_BUTTON_0+HostInp_num_joystick_buttons,

static const std::vector<std::string> &JoyKeyNames() {
	static std::vector<std::string> names;

	if(names.empty()) {
		names.resize(mbJOYKEY_NUM);
		names[mbJOYKEY_TX_NEG]="Translate -X";
		names[mbJOYKEY_TX_POS]="Translate +X";
		names[mbJOYKEY_TY_NEG]="Translate -Y";
		names[mbJOYKEY_TY_POS]="Translate +Y";
		names[mbJOYKEY_TZ_NEG]="Translate -Z";
		names[mbJOYKEY_TZ_POS]="Translate +Z";
		names[mbJOYKEY_RX_NEG]="Rotate -X";
		names[mbJOYKEY_RX_POS]="Rotate +X";
		names[mbJOYKEY_RY_NEG]="Rotate -Y";
		names[mbJOYKEY_RY_POS]="Rotate +Y";
		names[mbJOYKEY_RZ_NEG]="Rotate -Z";
		names[mbJOYKEY_RZ_POS]="Rotate +Z";
		for(unsigned i=0;i<HostInp_num_joystick_buttons;++i) {
			names[mbJOYKEY_BUTTON_0+i]=mbStrPrintf("button %u",i);
		}
	}

	return names;
}

const std::string &mbJoyKeyNameFromKey(mbJoyKey key) {
	return JoyKeyNames()[key];
}

mbJoyKey mbJoyKeyFromKeyName(const std::string &name) {
	const std::vector<std::string> &names=JoyKeyNames();
	for(unsigned i=0;i<names.size();++i) {
		if(stricmp(name.c_str(),names[i].c_str())==0) {
			return mbJoyKey(i);
		}
	}
	return mbJOYKEY_INVALID;
}

void mbJoyKeyGetIndexAndKey(HostInpState *hostinp,const mbKeymap::Key &key,int *joy_index,
	mbJoyKey *joy_key)
{
	*joy_index=-1;
	*joy_key=mbJoyKeyFromKeyName(key.key);

	for(int index=0;index<HostInp_NumJoysticks(hostinp);++index) {
		if(strcmp(key.device.c_str(),HostInp_JoystickName(hostinp,index))==0) {
			*joy_index=index;
			break;
		}
	}
}

static inline void GetState(float v,mbJoyKey neg,mbJoyKey pos,int *down_count) {
	if(v<-.5f) {
		++down_count[neg];
	} else {
		down_count[neg]=0;
	}

	if(v>.5f) {
		++down_count[pos];
	} else {
		down_count[pos]=0;
	}
}

void mbJoyKeyGetKeyStates(const HostInp_JoyState *state,mbJoyKeyState *key_states) {
	GetState(state->x,mbJOYKEY_TX_NEG,mbJOYKEY_TX_POS,key_states->down_count);
	GetState(state->y,mbJOYKEY_TY_NEG,mbJOYKEY_TY_POS,key_states->down_count);
	GetState(state->z,mbJOYKEY_TZ_NEG,mbJOYKEY_TZ_POS,key_states->down_count);
	GetState(state->rx,mbJOYKEY_RX_NEG,mbJOYKEY_RX_POS,key_states->down_count);
	GetState(state->ry,mbJOYKEY_RY_NEG,mbJOYKEY_RY_POS,key_states->down_count);
	GetState(state->rz,mbJOYKEY_RZ_NEG,mbJOYKEY_RZ_POS,key_states->down_count);
	for(unsigned i=0;i<HostInp_num_joystick_buttons;++i) {
		if(state->buttons[i]) {
			++key_states->down_count[mbJOYKEY_BUTTON_0+i];
		} else {
			key_states->down_count[mbJOYKEY_BUTTON_0+i]=0;
		}
	}
}
