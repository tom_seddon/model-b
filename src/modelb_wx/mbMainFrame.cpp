#include "pch.h"

#include "mbMainFrame.h"
#include "twArgs.h"
#include "mbModel.h"
#include "mbMisc.h"
#include "mbQuickstart.h"
#include "mbRomsConfigDialog.h"
#include "mbSoundConfigDialog.h"
#include "mbKeyboardConfigDialog.h"
#include "mbJoysticksConfigDialog.h"
#include "mbDiscFormat.h"
#include "mbHardwareDialog.h"
#include "mbVideoConfigDialog.h"
#include "mbKeys.h"
#include "mbProfilesDialog.h"
#include "mbStatusBar.h"

#include <bbcAdc.h>
#include <bbcVideo.h>
#include <bbcUef.h>
#include <bbcProfiler.h>
#ifdef BEEB_DEBUG_SAVE_CHALLENGER_RAM
#include <bbc1770OpusChallenger.h>
#endif
#ifdef mbVISUAL_PROFILER
#include <bbcTeletextFont.h>
#endif
#include <bbcComputer.h>
#include <bbcKeyboardMatrix.h>
#include <bbcSystemVIA.h>
#include <bbcSound.h>
#include <bbcFdd.h>
#include <bbcUserVIA.h>
#include <bbcSystemVIA.h>
#include <bbc1770.h>
#ifdef bbcENABLE_BEEB_ICE
#include "mbIceFrame.h"
#endif
#include <wx/progdlg.h>
#ifdef __WXX11__
#include <wx/x11/privx.h>
#endif//__WXX11__

#ifdef __WXGTK__
#include <gdk/gdk.h>
#include <gdk/gdkx.h>
#include <gtk/gtk.h>
#endif//__WXGTK__

#include <base/log.h>

#include <algorithm>

//////////////////////////////////////////////////////////////////////////
//
//#define VERBOSITY

// if defined, joysticks not updated
//#define DISABLE_JOYSTICKS

//////////////////////////////////////////////////////////////////////////
//Frequences of various update things
//
//input -- how often HostInp is polled
//sys -- how often the message loop is run
//sound -- how often sound update is done
//
//These are given in frequency per _emulated_ second. This is what you want,
//but it may bear little relation to real time.
enum {
	update_input_freq=200,
#ifdef PORT_USE_WX_KEYBOARD
	update_sys_freq=100,
#else//PORT_USE_WX_KEYBOARD
#ifdef bbcDEBUG_ENABLE
	//Upping this value doesn't make a big difference until the debug panels are
	//activated...
	update_sys_freq=25,//100,
#else
	update_sys_freq=25,
#endif
#endif//PORT_USE_WX_KEYBOARD
	update_sound_freq=50,

	update_input_cycles=2000000/update_input_freq,
	update_sys_cycles=2000000/update_sys_freq,
	update_sound_cycles=2000000/update_sound_freq,
};

//////////////////////////////////////////////////////////////////////////
// profiling things.
BP(static bbcProfileRecord bpr_do_sys_tick("DoSysTick",0));
BP(static bbcProfileRecord bpr_do_beeb_tick("DoBeebTick",0));
BP(static bbcProfileRecord bpr_run_6502("Run6502",&bpr_do_beeb_tick));
BP(static bbcProfileRecord bpr_bbccomputer_update("bbcComputer::Update",&bpr_do_beeb_tick));
BP(static bbcProfileRecord bpr_do_beeb_tick_rem("DoBeebTick remainder",&bpr_do_beeb_tick));
BP(static bbcProfileRecord bpr_hostgfx_present("HostGfx Present",&bpr_do_beeb_tick_rem));
BP(static bbcProfileRecord bpr_hostgfx_present_convert("Convert",&bpr_hostgfx_present));
BP(static bbcProfileRecord bpr_hostgfx_present_present("Present",&bpr_hostgfx_present));
BP(static bbcProfileRecord bpr_clear_video_buffer("Clear video buffer",&bpr_do_beeb_tick_rem));
BP(static bbcProfileRecord bpr_update_sound("Update sound",&bpr_do_beeb_tick_rem));
BP(static bbcProfileRecord bpr_limit_speed("Limit speed",&bpr_do_beeb_tick_rem));
BP(static bbcProfileRecord bpr_update_input("Update input",&bpr_do_beeb_tick_rem));
BP(static bbcProfileRecord bpr_draw_visual_profile("Draw timings",&bpr_do_beeb_tick_rem));

//////////////////////////////////////////////////////////////////////////
//Crapadelic keyboard press playback!
//#define mbFAKE_KEYPRESSES

#ifdef mbFAKE_KEYPRESSES
enum FakeActionType {
	FAKE_KEY_DOWN,
	FAKE_KEY_UP,

	//these don't have a time_cycles.
	FAKE_START_DISASSEMBLY,
	FAKE_STOP_DISASSEMBLY,
};

struct FakeAction {
	FakeActionType type;
	int cycles;//when to do it
	t65::byte bbc_code;
};

static const int std_step=2000000/10;

#define FAKE_KEY_TAP(START,KEY)\
	{ FAKE_KEY_DOWN, (START), (KEY) },\
	{ FAKE_KEY_UP, (START)+std_step, (KEY) }

static const FakeAction fake_actions[]={
	//*.
	{ FAKE_KEY_DOWN,	2000000+std_step*0,		bbcKEY_SHIFT, },
	{ FAKE_KEY_DOWN,	2000000+std_step*1,		bbcKEY_COLON, },
	{ FAKE_KEY_UP,		2000000+std_step*2,		bbcKEY_COLON, },
	{ FAKE_KEY_UP,		2000000+std_step*3,		bbcKEY_SHIFT, },
	FAKE_KEY_TAP(2000000+std_step*4,bbcKEY_STOP),
	FAKE_KEY_TAP(2000000+std_step*6,bbcKEY_RETURN),

	//*COMPACT
	{ FAKE_KEY_DOWN,	5000000+std_step*0,		bbcKEY_SHIFT, },
	{ FAKE_KEY_DOWN,	5000000+std_step*1,		bbcKEY_COLON, },
	{ FAKE_KEY_UP,		5000000+std_step*2,		bbcKEY_COLON, },
	{ FAKE_KEY_UP,		5000000+std_step*3,		bbcKEY_SHIFT, },
	FAKE_KEY_TAP(5000000+std_step*4,bbcKEY_C),
	FAKE_KEY_TAP(5000000+std_step*6,bbcKEY_O),
	FAKE_KEY_TAP(5000000+std_step*8,bbcKEY_M),
	FAKE_KEY_TAP(5000000+std_step*10,bbcKEY_P),
	FAKE_KEY_TAP(5000000+std_step*12,bbcKEY_A),
	FAKE_KEY_TAP(5000000+std_step*14,bbcKEY_C),
	FAKE_KEY_TAP(5000000+std_step*16,bbcKEY_T),
	FAKE_KEY_TAP(5000000+std_step*18,bbcKEY_RETURN),
	//{ FAKE_START_DISASSEMBLY, 5000000+std_step*19, },//on release of RETURN
};
static const unsigned num_fake_actions=sizeof fake_actions/sizeof fake_actions[0];
#endif//mbFAKE_KEYPRESSES

//////////////////////////////////////////////////////////////////////////
//
static const char *window_title="model-b";

//////////////////////////////////////////////////////////////////////////
//TODO the drive handling sucks. UiUnloadDrive similarly.

#ifdef bbcDEBUG_ENABLE
//Enable the File|Test item
#define ENABLE_TEST
#endif

#define AUTO_PAINT() this->SetAutoPaint(true)

//////////////////////////////////////////////////////////////////////////
//Id offsets for each drive

enum {
	subid_drive=0,
	subid_drive_filename,
	subid_drive_load,
	subid_drive_save,
	subid_drive_save_as,
	subid_drive_unload,
	subid_drive_sep,
	subid_drive_recent_first,
	subid_drive_recent_last=subid_drive_recent_first+mbMiscConfig::max_recent_discs-1,
	subid_drive_num,//MUST BE LAST
};

//const mbMainFrame::DriveCommandMfn mbMainFrame::drive_subactions[subid_drive_num]={
//	0,
//	0,
//	&mbMainFrame::OnDriveLoad,
//	&mbMainFrame::OnDriveSave,
//	&mbMainFrame::OnDriveSaveAs,
//	&mbMainFrame::OnDriveUnload,
//};

//////////////////////////////////////////////////////////////////////////

static const int max_num_keymaps=100;

const wxWindowID id_status_bar=mbNewId();
const wxWindowID id_exit=mbNewId();
const wxWindowID id_quickstart=mbNewId();
const wxWindowID id_hard_reset=mbNewId();
#ifdef ENABLE_TEST
const wxWindowID id_test=mbNewId();
#endif//ENABLE_TEST
const wxWindowID id_toggle_sound=mbNewId();
const wxWindowID id_toggle_limit_speed=mbNewId();
const wxWindowID id_toggle_status_bar=mbNewId();
const wxWindowID id_toggle_fast_forward_disc_access=mbNewId();
const wxWindowID id_toggle_full_screen=mbNewId();
const wxWindowID id_show_about_box=mbNewId();

const wxWindowID id_roms_dialog=mbNewId();
const wxWindowID id_sound_dialog=mbNewId();
const wxWindowID id_keymap_dialog=mbNewId();
const wxWindowID id_joysticks_dialog=mbNewId();
const wxWindowID id_keymaps_submenu=mbNewId();
const wxWindowID id_resolution_submenu=mbNewId();

//const wxWindowID id_resolutions_begin=mbNewId();
const wxWindowID id_resolution_0=mbNewId();
const wxWindowID id_resolution_1=mbNewId();
const wxWindowID id_resolution_2=mbNewId();
const wxWindowID id_resolution_3=mbNewId();
//const wxWindowID id_resolutions_end=mbNewId();

#ifdef bbcDEBUG_TRACE_EVENTS
const wxWindowID id_start_disassembly=mbNewId();
const wxWindowID id_reboot_with_disassembly=mbNewId();
const wxWindowID id_stop_disassembly=mbNewId();
#endif//bbcDEBUG_TRACE_EVENTS

#ifdef BEEB_DEBUG_SAVE_CHALLENGER_RAM
const wxWindowID id_save_challenger_ram=mbNewId();
#endif//BEEB_DEBUG_SAVE_CHALLENGER_RAM
#ifdef bbcDEBUG_ENABLE
const wxWindowID id_disassemble_ram=mbNewId();
#endif//bbcDEBUG_ENABLE
const wxWindowID id_screenshot_save_as=mbNewId();

#ifdef bbcDEBUG_PANELS
const wxWindowID id_toggle_dbgpanel_sysvia=mbNewId();
const wxWindowID id_dbgpanel_sysvia=mbNewId();
const wxWindowID id_toggle_dbgpanel_usrvia=mbNewId();
const wxWindowID id_dbgpanel_usrvia=mbNewId();
const wxWindowID id_toggle_dbgpanel_video=mbNewId();
const wxWindowID id_dbgpanel_video=mbNewId();
const wxWindowID id_toggle_dbgpanel_adc=mbNewId();
const wxWindowID id_dbgpanel_adc=mbNewId();
#endif//bbcDEBUG_PANELS
#ifdef bbcDEBUG_VIDEO
const wxWindowID id_toggle_showall=mbNewId();
const wxWindowID id_log_video_frames=mbNewId();
//#ifdef bbcDEBUG_TRACE
//const wxWindowID id_log_video_frames_disasm=mbNewId();
//#endif//bbcDEBUG_TRACE
const wxWindowID id_toggle_show_t1_timeouts=mbNewId();

#ifdef t65_COUNT_INSTRUCTION_FREQUENCIES
const wxWindowID id_show_instruction_frequencies=mbNewId();
const wxWindowID id_show_instruction_frequencies_sorted=mbNewId();
#endif//t65_COUNT_INSTRUCTION_FREQUENCIES

#ifdef bbcDEBUG_COUNT_MMIO_ACCESSES
const wxWindowID id_show_sheila_access_counts_by_addr=mbNewId();
const wxWindowID id_show_sheila_access_counts_by_count=mbNewId();
#endif//bbcDEBUG_COUNT_MMIO_ACCESSES

const wxWindowID id_toggle_log_enabled=mbNewId();

#endif//bbcDEBUG_VIDEO

#ifdef mbVISUAL_PROFILER
const wxWindowID id_show_visual_profile=mbNewId();
#endif//mbVISUAL_PROFILER

const wxWindowID id_first_keymap=mbNewIds(max_num_keymaps);
//const wxWindowID id_last_keymap=id_first_keymap+1000=mbNewId();

static const int num_drive_ids=bbcFdd::num_drives*subid_drive_num;
const wxWindowID id_drives_begin=mbNewIds(num_drive_ids);
//const wxWindowID id_drives_end=id_drives_begin+bbcFdd::num_drives*subid_drive_num=mbNewId();

const wxWindowID id_sound_record_start=mbNewId();
const wxWindowID id_sound_record_stop=mbNewId();

const wxWindowID id_screen_type_submenu=mbNewId();
const wxWindowID id_screen_types_begin=mbNewId();
const wxWindowID id_screen_type_colour=mbNewId();
const wxWindowID id_screen_type_bnw=mbNewId();
const wxWindowID id_screen_type_amber=mbNewId();
const wxWindowID id_screen_type_green=mbNewId();
const wxWindowID id_screen_types_end=mbNewId();

const wxWindowID id_scanlines_begin=mbNewId();
const wxWindowID id_windowed_scanlines_submenu=mbNewId();
const wxWindowID id_windowed_scanlines_single=mbNewId();
const wxWindowID id_windowed_scanlines_interlace=mbNewId();
const wxWindowID id_windowed_scanlines_double=mbNewId();

const wxWindowID id_full_screen_scanlines_submenu=mbNewId();
const wxWindowID id_full_screen_scanlines_single=mbNewId();
const wxWindowID id_full_screen_scanlines_interlace=mbNewId();
const wxWindowID id_full_screen_scanlines_double=mbNewId();
const wxWindowID id_full_screen_scanlines_end=mbNewId();
const wxWindowID id_scanlines_end=mbNewId();

const wxWindowID id_hardware_dialog=mbNewId();
const wxWindowID id_video_dialog=mbNewId();
const wxWindowID id_file_menu=mbNewId();
const wxWindowID id_options_menu=mbNewId();
const wxWindowID id_special_menu=mbNewId();
#ifdef bbcDEBUG_ENABLE
const wxWindowID id_debug_menu=mbNewId();
#endif
#ifdef bbcENABLE_SAVE_STATE
const wxWindowID id_load_state=mbNewId();
const wxWindowID id_save_state=mbNewId();
#endif
const wxWindowID id_help_menu=mbNewId();
#ifdef bbcENABLE_PROFILER
const wxWindowID id_display_profiler_results=mbNewId();
#endif
#ifdef bbcENABLE_BEEB_ICE
const wxWindowID id_ice_window_toggle=mbNewId();
#endif
//////////////////////////////////////////////////////////////////////////
//
static wxString host_log_file;
static bool host_log_lost=false;
#ifdef bbcDEBUG_ENABLE
static bool host_no_outputdebugstring=false;
#endif

static void HostLog(const char *str) {
	if(!host_log_file.empty()) {
		FILE *h=fopen(host_log_file,"a+t");
		if(!h) {
			host_log_lost=true;
		} else {
			if(host_log_lost) {
				fputs("*** some output was lost\n",h);
				host_log_lost=false;
			}

			fputs(str,h);
			fclose(h);
		}
	}

#ifdef bbcDEBUG_ENABLE
	if(!host_no_outputdebugstring) {
#ifdef _WIN32
		OutputDebugStringA(str);
#else
		fputs(str,stderr);
#endif
	}
#endif
}

//////////////////////////////////////////////////////////////////////////
//
static void GeneratePalette(float palette[8][3],float rmul,float gmul,float bmul) {
	for(unsigned i=0;i<8;++i) {
		float r=i&1;
		float g=(i>>1)&1;
		float b=(i>>2)&1;
		float v=r*.3f+g*.59f+b*.11f;
		
		palette[i][0]=v*rmul;
		palette[i][1]=v*gmul;
		palette[i][2]=v*bmul;
	}
}

//////////////////////////////////////////////////////////////////////////
//a simple panel that ignores erase background and posts right click messages
//to its parent.
//the main emulator display is drawn on top of this.
class NullPanel:
public wxWindow
{
public:
	NullPanel(wxWindow *parent,wxWindowID id);
#ifdef PORT_USE_WX_KEYBOARD
	const unsigned char *GetKeyStates() const;
#endif
private:
	void OnEraseBackground(wxEraseEvent &event);
	void OnRightUp(wxMouseEvent &event);
#ifdef PORT_USE_WX_KEYBOARD
	void OnKeyDown(wxKeyEvent &event);
	void OnKeyUp(wxKeyEvent &event);
	void OnChar(wxKeyEvent &event);

	std::vector<unsigned char> wx_key_states_;
#endif

	void DoEraseBackground(wxDC *dc);

	DECLARE_EVENT_TABLE()
};

BEGIN_EVENT_TABLE(NullPanel,wxWindow)
	EVT_ERASE_BACKGROUND(NullPanel::OnEraseBackground)
	EVT_RIGHT_UP(NullPanel::OnRightUp)
#ifdef PORT_USE_WX_KEYBOARD
	EVT_KEY_UP(NullPanel::OnKeyUp)
	EVT_KEY_DOWN(NullPanel::OnKeyDown)
	//EVT_CHAR(mbMainFrame::OnChar)
#endif
END_EVENT_TABLE()

NullPanel::NullPanel(wxWindow *parent,wxWindowID id):
wxWindow(parent,id,wxDefaultPosition,wxDefaultSize,0)
{
	// Disable automatic background repaints on GTK
	this->SetBackgroundStyle(wxBG_STYLE_CUSTOM);

#ifdef PORT_USE_WX_KEYBOARD
	wx_key_states_.resize(mbGetNumWxKeyCodes(),0);
	wxLogDebug("%s: wx_key_states_.size()=%u\n",__FUNCTION__,wx_key_states_.size());
#endif
}

#ifdef PORT_USE_WX_KEYBOARD
const unsigned char *NullPanel::GetKeyStates() const {
	return &wx_key_states_[0];
}
#endif

void NullPanel::OnEraseBackground(wxEraseEvent &event) {
	if(wxDC *dc=event.GetDC()) {
		this->DoEraseBackground(dc);
	} else {
		wxClientDC client_dc(this);
		this->DoEraseBackground(&client_dc);
	}
}

void NullPanel::OnRightUp(wxMouseEvent &event) {
	wxWindow *parent=this->GetParent();
	if(parent) {
		wxEvtHandler *h=this->GetEventHandler();
		h->AddPendingEvent(event);
	}
}

#ifdef PORT_USE_WX_KEYBOARD
void NullPanel::OnKeyDown(wxKeyEvent &event) {
	long k=event.GetKeyCode();
	if(k>=0&&unsigned(k)<wx_key_states_.size()) {
		wx_key_states_[k]=1;
		//printf("%s: %d, %d.\n",__FUNCTION__,event.GetKeyCode(),wx_key_states_[k]);
	}
	event.Skip();
}

void NullPanel::OnKeyUp(wxKeyEvent &event) {
	long k=event.GetKeyCode();
	if(k>=0&&unsigned(k)<wx_key_states_.size()) {
		wx_key_states_[k]=0;
		//printf("%s: %d, %d.\n",__FUNCTION__,event.GetKeyCode(),wx_key_states_[k]);
	}
	event.Skip();
}
#endif

void NullPanel::DoEraseBackground(wxDC *dc)
{
	(void)dc;

// 	static const wxBrush *const test_brushes[]={
// 		wxBLUE_BRUSH,
// 		wxGREEN_BRUSH,
// 		wxWHITE_BRUSH,
// 		wxBLACK_BRUSH,
// 		wxGREY_BRUSH,
// 		wxMEDIUM_GREY_BRUSH,
// 		wxLIGHT_GREY_BRUSH,
// 		wxCYAN_BRUSH,
// 		wxRED_BRUSH,
// 	};
// 	static const unsigned num_test_brushes=sizeof test_brushes/sizeof test_brushes[0];
// 
// 	dc->SetBackground(*test_brushes[rand()%num_test_brushes]);
// 	dc->Clear();
// 	wxLogDebug("%s.",__FUNCTION__);
}

//////////////////////////////////////////////////////////////////////////
//
mbMainFrame::SbState::SbState():
caps(false),
shift(false)
{
	for(unsigned i=0;i<bbcFdd::num_drives;++i) {
		this->drives[i].what=-1;//unloaded
	}
}

//////////////////////////////////////////////////////////////////////////
//
mbMainFrame::DriveUi::DriveUi():
//loaded(false),
last_filter_index(0)
{
}

//////////////////////////////////////////////////////////////////////////
//
bool mbMainFrame::DoCommandLine(const std::vector<wxString> &argv) {
	std::vector<twArg *> all_args;
	unsigned i;

	all_args.push_back(new twArgPresent("selectini",&cl_select_ini_));
	all_args.push_back(new twArgString("inifile",&cl_ini_file_));
	all_args.push_back(new twArgPresent("autoboot",&cl_autoboot_));
	all_args.push_back(new twArgMulti<twArgString>("inioverride",&cl_ini_override_files_));
	all_args.push_back(new twArgState("inisave",&cl_ini_save_));
	all_args.push_back(new twArgState("verbose",&cl_verbose_));
	for(i=0;i<4;++i) {
		all_args.push_back(new twArgString(wxString::Format("mount%u",i),&cl_drive_mounts_[i]));
	}
	all_args.push_back(new twArgString("hostlogfile",&host_log_file));
#ifdef bbcDEBUG_ENABLE
	all_args.push_back(new twArgPresent("hostnooutputdebugstring",&host_no_outputdebugstring));
#ifdef bbcDEBUG_TRACE_EVENTS
	all_args.push_back(new twArgPresent("withdisassembly",&cl_with_disassembly_));
#endif
#endif
#ifdef PORT_X
	all_args.push_back(new twArgPresent("noshm",&cl_no_shm_));
	all_args.push_back(new twArgPresent("noxv",&cl_no_xv_));
#endif//PORT_X

	bool ok=twArg::ProcessAll(argv,all_args);
	for(i=0;i<all_args.size();++i) {
		delete all_args[i];
	}
	return ok;
}

//////////////////////////////////////////////////////////////////////////
//
mbMainFrame::mbMainFrame(const std::vector<wxString> &argv):
wxFrame(0,-1,window_title,wxDefaultPosition,wxDefaultSize,wxDEFAULT_FRAME_STYLE),
auto_paint_(false),
ok_(false),
//cfg_(cfg),
hostgfx_(0),
hostinp_(0),
hostsnd_(0),
num_frames_(0),
next_update_input_(0),
next_update_sys_(0),
next_update_sound_(0),
last_update_sys_time_(0),
debouncing_break_(false),
last_update_sound_hf_time_(0),
beeb_paused_(true),
//keymaps_submenu_(0),
screenshot_countdown_(0),
beeb_screen_(0),
status_bar_(0),
beeb_fastforward_(false),
cl_select_ini_(false),
cl_autoboot_(false),
cl_ini_save_(true),
//cl_verbose_ set up below
#if (defined bbcDEBUG_ENABLE)&&(defined bbcDEBUG_TRACE_EVENTS)
cl_with_disassembly_(false),
#endif
#ifdef PORT_X
cl_no_shm_(false),
cl_no_xv_(false),
#endif
menu_bar_(0),
model_(0),
disc_interface_(0),
update_window_state_(false),
last_video_buffer_(0),
recent_discs_dirty_(true)
{
//	this->SetIcon(wxIcon("IDI_ICON1",wxBITMAP_TYPE_ICO_RESOURCE));
	int i;

	joystick_indexes_[0]=-1;
	joystick_indexes_[1]=-1;

	memset(key_down_counts_,0,sizeof key_down_counts_);

	for(i=0;i<bbcFdd::num_drives;++i) {
		drive_submenus_[i]=0;
	}

#ifdef bbcDEBUG_ENABLE
	cl_verbose_=true;
#else
	cl_verbose_=false;
#endif

	Host_SetLogOutputCallback(&HostLog);

	if(!this->DoCommandLine(argv)) {
		return;
	}

#ifdef bbcHACKING_URIDIUM
	cl_drive_mounts_[0]="z:\\beeb\\src\\run\\uridium-test-version.ssd";
	cl_autoboot_=true;
#endif

	wxLog::GetActiveTarget()->SetVerbose(cl_verbose_);
	
	if(cl_select_ini_) {
		wxArrayString ini_names_arr;
		std::vector<wxFileName> ini_names;
		mbGetMatchingFileNames(wxFileName::DirName("."),"*.ini",&ini_names);
		if(ini_names.empty()) {
			wxLogError("No files matching \"*.ini\" for -selectini");
			return;
		}
		for(unsigned i=0;i<ini_names.size();++i) {
			ini_names_arr.Add(ini_names[i].GetName());
		}
		int index=wxGetSingleChoiceIndex("Select INI file to use","-selectini",
			ini_names_arr);
		if(index<0) {
			return;
		} else {
			cl_ini_file_=mbNormalizedFileName(ini_names[index].GetFullPath());
		}
	}

	if(cl_ini_file_.empty()) {
		cl_ini_file_="./beeb.ini";
	}
	cfg_.Load(cl_ini_file_.ToStdString());
	for(i=0;i<int(cl_ini_override_files_.size());++i) {
		cfg_.Load(cl_ini_override_files_[i].ToStdString());
	}

	//By default, ini is readonly if overrides were specified. This stops override
	//settings leaking into the main file. However, this may be overridden with
	//-inisave.
	cfg_.SetFilename(cl_ini_file_.ToStdString(),!cl_ini_override_files_.empty()&&!cl_ini_save_);

	//Child windows
	beeb_screen_=new NullPanel(this,-1);//,wxDefaultPosition,wxDefaultSize,wxNO_BORDER);
	beeb_screen_->SetBackgroundColour(*wxBLACK);
	beeb_screen_->Show(true);

	wxBoxSizer *all_sz=new wxBoxSizer(wxVERTICAL);
	all_sz->Add(beeb_screen_,1,wxEXPAND);
	this->SetSizer(all_sz);

	status_bar_=new mbStatusBar(this,id_status_bar);//,wxST_SIZEGRIP);
//	wxSize tmp=status_bar_->GetBestFittingSize();

	//host
	ok_=true;
	auto_paint_=true;
#ifdef __WXMSW__
	//grr why does GetHandle() not return an HWND???
	hostgfx_=HostGfx_Start(HWND(this->GetHandle()),HWND(beeb_screen_->GetHandle()));
#elif defined __WXX11__
	Widget thisw=this->GetHandle(),beeb_screenw=this->GetHandle();
#error
	hostgfx_=HostGfx_Start(XID(this->GetHandle()),XID(beeb_screen_->GetHandle()));
#endif
	
	// disabled due to gtk (can let everything use the normal error checking)
	//if(!hostgfx_) {
		//wxMessageBox("Failed to initialise graphics.\n\nThe emulator will now exit.",
			//"Graphics failed",wxOK|wxICON_ERROR,this);
		//ok_=false;
		//return;
	//}

	hostinp_=HostInp_Start(this->GetHandle());
	if(!hostinp_) {
		wxMessageBox("Failed to initialise keyboard.\n\nThe emulator will now exit.",
			"Keyboard failed",wxOK|wxICON_ERROR,this);
		ok_=false;
		return;
	}
	HostTmr_Start();
	HostTmr_GetHfFrequency(&hf_freq_);

#ifdef bbcENABLE_BEEB_ICE
	ice_frame_=0;
	ice_cfg_.Load("./beebice.ini");
#endif

#if (defined bbcDEBUG_ENABLE)&&(defined bbcDEBUG_TRACE_EVENTS)
	if(cl_with_disassembly_) {
		wxCommandEvent tmp_event;
		this->OnStartDisassembly(tmp_event);
	}
#endif

	//////////////////////////////////////////////////////////////////////////
	// note to self: put nothing new below this line!
	this->InitMenuBar();

	if(!this->ApplyConfig()) {
		ok_=false;
	} else {
		//Mount startup drives
		for(i=0;i<bbcFdd::num_drives;++i) {
			if(!cl_drive_mounts_[i].empty()) {
				this->UiLoadDisc(i,cl_drive_mounts_[i]);
			}
		}

		if(cl_autoboot_) {
			bbcKeyboardMatrix::SetForceShiftPressed(true);
		}
	}

#ifdef mbVISUAL_PROFILER
	first_frame_drawn_yet_=false;
	this->MakeProfilerFontData();
#endif

#ifdef bbcTEST_SAVE_STATE
	tss_state=TSSS_NOT_STARTED;
#endif
}

//////////////////////////////////////////////////////////////////////////
bool mbMainFrame::Ok() const {
	return ok_;
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::SetKeymap(const std::string &keymap_name) {
	mbKeymap::C::iterator src;

	for(src=cfg_.keyboard.keymaps.begin();src!=cfg_.keyboard.keymaps.end();++src) {
		if(stricmp(keymap_name.c_str(),src->name.c_str())==0) {
			break;
		}
	}

	for(unsigned i=0;i<256;++i) {
		keymap_[i].clear();
	}

	if(src==cfg_.keyboard.keymaps.end()) {
		wxLogWarning("SetKeymap: \"%s\": not found",keymap_name.c_str());
	} else {
		for(unsigned i=0;i<256;++i) {
			mbKeymap::HostKeys::const_iterator srckey;

			for(srckey=src->keymap[i].begin();srckey!=src->keymap[i].end();++srckey) {
				if(srckey->device.empty()) {
#ifdef PORT_USE_WX_KEYBOARD
					int host_code=mbGetWxKeyCodeFromName(srckey->key);
#else
					int host_code=HostInp_CodeFromKeyName(srckey->key.c_str());
#endif
					if(host_code==HostInp_bad_key) {
						wxLogWarning("SetKeymap: Unknown key \"%s\"",srckey->key.c_str());
					} else if(host_code>0xFFFF) {
						wxLogWarning("SetKeymap: Key \"%s\" cannot be used",srckey->key.c_str());
					} else {
						Key destkey;

						destkey.flags=0;
						destkey.index=0;
						destkey.code=host_code;

// 						if(destkey.code==0x1c)//DIK_RETURN
// 							destkey.flags|=Key::F_AUTOFIRE;

						keymap_[i].push_back(destkey);

						//wxLogDebug(__FUNCTION__ ": %s -> code=%u",srckey->key.c_str(),destkey.code);
					}
				} else {
					int index;
					mbJoyKey code;

					mbJoyKeyGetIndexAndKey(hostinp_,*srckey,&index,&code);
					if(code==mbJOYKEY_INVALID) {
						wxLogWarning("SetKeymap: Unknown joystick key \"%s\"",srckey->key.c_str());
					} else if(index>=0) {
						Key destkey;

						destkey.flags=Key::F_IS_JOYSTICK;
						destkey.index=index;
						destkey.code=code;

						keymap_[i].push_back(destkey);

						//wxLogDebug(__FUNCTION__ ": %s:%s -> index=%u code=%u",srckey->device.c_str(),srckey->key.c_str(),destkey.index,destkey.code);
					}
				}
			}
		}
	}

	cfg_.keyboard.selected_keymap=keymap_name;
}

//////////////////////////////////////////////////////////////////////////
// Returns true if closing is OK.
//
// If there's no changes to be saved, return true; otherwise, pop up a
// dialog box asking for confirmation.
bool mbMainFrame::CloseConfirmed() {
	for(int i=0;i<bbcFdd::num_drives;++i) {
		if(bbcFdd::DriveStatus(i)!=bbcFdd::DRIVE_SIDE2&&
			!this->UiUnloadDrive(i))
		{
			return false;
		}
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::OnClose(wxCloseEvent &event) {
	if(event.CanVeto()) {
		if(!this->CloseConfirmed()) {
			event.Veto();
			return;
		}
	}

	cfg_.Save();
#ifdef bbcENABLE_BEEB_ICE
	if(ice_frame_) {
		ice_frame_->GetConfig(&ice_cfg_);
	}
	ice_cfg_.Save();
#endif

	//the main frame can receive more idle events if any dialogs are
	//popped up whilst processing this next bit.
	//This stops anything being done with them.
	beeb_paused_=true;

	for(int i=0;i<bbcFdd::num_drives;++i) {
		bbcFdd::UnloadDiscImage(i);
	}

	bbcComputer::Shutdown();

	HostSnd_Stop(hostsnd_);
	hostsnd_=0;
	HostInp_Stop(hostinp_);
	hostinp_=0;
	HostGfx_Stop(hostgfx_);
	hostgfx_=0;
	HostTmr_Stop();
//	this->Destroy();

#ifdef bbcENABLE_PROFILER
	{
		wxCommandEvent tmp_event;
		this->OnDisplayProfilerResults(tmp_event);
	}
#endif

	event.Skip();
}

/*
void mbMainFrame::UpdateStatusBar() {
	static const wxString caps_lock_on="CAPS LOCK";
	static const wxString shift_lock_on="SHIFT LOCK";
	wxStatusBar *sb=this->GetStatusBar();
	
	if(bbc_system_via.CapslockLedLit()) {
		sb->SetStatusText(caps_lock_on,0);
	} else {
		sb->SetStatusText("",0);
	}
	if(bbc_system_via.ShiftlockLedLit()) {

	}
}
*/

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::DoUpdateSys() {
	int this_time;
	HostTmr_ReadMilliseconds(&this_time);
//	wxLogDebug("this_time=%d",this_time);
	bbc1770::FdcAction action;
	int drive,side,track,sector;
	bbc1770::GetCurrentAction(&action,&drive,&side,&track,&sector);
	beeb_fastforward_=action!=bbc1770::ACTION_IDLE&&
		cfg_.misc.fast_forward_disc_access&&
		bbcKeyboardMatrix::NumKeysDown()==0;
//	if(this_time-last_update_sys_time_>100) 

#ifdef bbcDEBUG_PANELS
	mbDebugPanel *usr=static_cast<mbDebugPanel *>(this->FindWindow(id_dbgpanel_usrvia));
	mbDebugPanel *sys=static_cast<mbDebugPanel *>(this->FindWindow(id_dbgpanel_sysvia));
	mbDebugPanel *vid=static_cast<mbDebugPanel *>(this->FindWindow(id_dbgpanel_video));
	mbDebugPanel *adc=static_cast<mbDebugPanel *>(this->FindWindow(id_dbgpanel_adc));
	if(usr) {
		bbc_user_via.UpdateDebugPanel(usr->Panel());
		usr->UpdateFromPanel();
	}
	if(sys) {
		bbc_system_via.UpdateDebugPanel(sys->Panel());
		sys->UpdateFromPanel();
	}
	if(vid) {
		vid->UpdateFromPanel();
	}
	if(adc) {
		bbcAdc::UpdateDebugPanel(adc->Panel());
		adc->UpdateFromPanel();
	}
#endif


	BASSERT(this_time>=last_update_sys_time_);
	if(cfg_.misc.show_status_bar&&this_time-last_update_sys_time_>250) {
		//MHz
		static int last_time_cycles=0;
		int num_cycles=bbcComputer::cpucycles-last_time_cycles;
		float num_seconds=(this_time-last_update_sys_time_)/1000.f;
		float mhz=num_cycles/num_seconds/1000000.f;
		status_bar_->SetMhz(mhz);

		//wxLogDebug("%fMHz.",mhz);

#ifdef mbFAKE_KEYPRESSES
		this->SetTitle(wxString::Format("%s - %d",window_title,bbcComputer::cpucycles));
#endif

		//Screen
		int screen_w,screen_h;
		beeb_screen_->GetClientSize(&screen_w,&screen_h);
		status_bar_->SetScreenSize(screen_w,screen_h);

		//FDC status
		int real_drive=bbcFdd::DriveFromFdcAddress(drive,side);
		status_bar_->SetFdcStatus(action,real_drive,track,sector);

#ifdef bbcDEBUG_ENABLE
		//Program counter
		status_bar_->SetPc(bbcComputer::cpustate.pc);
#endif

		//
		last_time_cycles=bbcComputer::cpucycles;
		last_update_sys_time_=this_time;
	}
	next_update_sys_=bbcComputer::cpucycles+update_sys_cycles;
	//wxLogDebug("this_time=%u next_update_sys_=%d bbcComputer::cpucycles=%d\n",this_time,next_update_sys_,bbcComputer::cpucycles);
#ifdef __WXMSW__
	//This gives slightly smoother multitasking.
	//Windows will allegedly preemptively multitask GUI apps these days,
	//seems it does it very politely :)
	Sleep(0);
#endif
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnIdle(wxIdleEvent &event) {
	if(beeb_paused_) {
		event.Skip();
	} else {
		this->DoBeebTick();
		this->DoUpdateSys();
		event.RequestMore();//TODO shouldn't request more if we're being shut down?
	}
}

//////////////////////////////////////////////////////////////////////////
//// TODO what a mess!
void mbMainFrame::DoUpdateInput() {
#ifdef PORT_USE_WX_KEYBOARD
	const unsigned char *key_states=beeb_screen_->GetKeyStates();
#else
	unsigned char key_states[256];
#endif
	unsigned num_joysticks=HostInp_NumJoysticks(hostinp_);
    
	if(!hostinp_) {
#ifndef PORT_USE_WX_KEYBOARD
		memset(key_states,0,sizeof key_states);
#endif
	} else {
		HostInp_Poll(hostinp_);
#ifndef PORT_USE_WX_KEYBOARD
		HostInp_GetKeyboardState(hostinp_,key_states);
#endif

		if(num_joysticks!=joykey_states_.size()) {
			joykey_states_.resize(num_joysticks);
		}

		for(unsigned i=0;i<num_joysticks;++i) {
			HostInp_JoyState state;

			if(HostInp_GetJoystickState(hostinp_,i,&state)) {
				mbJoyKeyGetKeyStates(&state,&joykey_states_[i]);
			} else {
				memset(&joykey_states_[i],0,sizeof joykey_states_[i]);
			}
		}
	}

#ifdef bbcHACKING_URIDIUM
	bool user_space=false;//whether user has pressed space
#endif

#ifndef mbFAKE_KEYPRESSES
	// TODO fix up Host... so it works this way.
	for(unsigned i=0;i<256;++i) {
		if(!key_states[i]) {
			key_down_counts_[i]=0;
		} else {
			++key_down_counts_[i];
		}
	}

	for(unsigned i=0;i<256;++i) {
		if(!keymap_[i].empty()) {
			const Key *down_key=0;
			int down_count=0;

			unsigned n=keymap_[i].size();

			for(unsigned j=0;j<n;++j) {
				const Key *key=&keymap_[i][j];

				if(!(key->flags&Key::F_IS_JOYSTICK)) {
					int count=key_down_counts_[key->code];
					if(count>0) {
						down_key=key;
						down_count=count;
						break;
					}
				} else {
					if(key->index<num_joysticks) {
						int count=joykey_states_[key->index].down_count[key->code];
						if(count>0) {
							down_key=key;
							down_count=count;
							break;
						}
					}
				}
			}

			if(down_key) {
				if(down_key->flags&Key::F_AUTOFIRE) {
					if((down_count&1)==0) {
						down_count=0;
					}
				}
			}

			bbcKeyboardMatrix::SetKeyState(bbcKey(i),down_count>0);
#ifdef bbcHACKING_URIDIUM
			if(i==bbcKEY_SPACE&&key_state) {
				user_space=true;
			}
#endif
		}
	}
#endif

#ifdef mbFAKE_KEYPRESSES
	for(unsigned i=0;i<num_fake_actions;++i) {
		const FakeAction *fa=&fake_actions[i];
		bool active=fa->cycles>=bbcComputer::cpucycles&&fa->cycles<bbcComputer::cpucycles+update_input_cycles;

		if(active) {
			bbcPrintf("fa->type=%d fa->cycles=%d fa->bbc_code=%s\n",fa->type,fa->cycles,mbKeyNameFromCode(fa->bbc_code));
			switch(fa->type) {
			case FAKE_KEY_DOWN:
				bbcKeyboardMatrix::SetKeyState(fa->bbc_code,true);
				break;

			case FAKE_KEY_UP:
				bbcKeyboardMatrix::SetKeyState(fa->bbc_code,false);
				break;

			case FAKE_START_DISASSEMBLY:
				bbcPrintf("faking start disassembly");
				this->OnStartDisassembly(wxCommandEvent());
				break;

			case FAKE_STOP_DISASSEMBLY:
				bbcPrintf("faking stop disassembly");
				this->OnStopDisassembly(wxCommandEvent());
				break;
			}
		}
	}
#endif

#ifdef bbcHACKING_URIDIUM
	static const int space_needed=6300000;
//	static const int down_for=5*400000;//N frames (5 cocks it)
	static const int down_for=4*400000;//N frames
	bbcKeyboardMatrix::SetKeyState(bbcKEY_SPACE,!user_space&&bbcComputer::cpucycles>=space_needed&&
		bbcComputer::cpucycles<=space_needed+down_for);

#endif

	bbc_system_via.DoKeyboardIrq();
	if(bbcKeyboardMatrix::KeyState(bbcKEY_BREAK)&&!debouncing_break_) {	
		bbcComputer::ResetSoft();
	} else if(!bbcKeyboardMatrix::KeyState(bbcKEY_BREAK)) {
		debouncing_break_=false;
	}

	//And now, joysticks.
#ifndef DISABLE_JOYSTICKS
	for(int i=0;i<2;++i) {
		mbJoysticksConfig::BeebStick *stick=&cfg_.joysticks.joysticks[i];
		bool button=false;
		float x=0.f,y=0.f;

		if(hostinp_&&stick->enabled&&joystick_indexes_[i]>=0) {
			HostInp_JoyState state;

			if(HostInp_GetJoystickState(hostinp_,joystick_indexes_[i],&state)) {
				static const float HostInp_JoyState::*hostinp_axes[6]={
					&HostInp_JoyState::x,
					&HostInp_JoyState::y,
					&HostInp_JoyState::z,
					&HostInp_JoyState::rx,
					&HostInp_JoyState::ry,
					&HostInp_JoyState::rz,
				};

				if(stick->x_axis!=mbJoysticksConfig::INVALID_AXIS) {
					x=state.*hostinp_axes[stick->x_axis];
				}
				if(stick->y_axis!=mbJoysticksConfig::INVALID_AXIS) {
					y=state.*hostinp_axes[stick->y_axis];
				}

				if(stick->button>=0&&stick->button<HostInp_num_joystick_buttons&&
					state.buttons[stick->button])
				{
					button=true;
				}
			}
		}

		bbcAdc::SetJoystick(i,x,y);
		bbc_system_via.SetJoystickButtonState(i,button);
	}
#endif
}

//////////////////////////////////////////////////////////////////////////

wxMenu *CloneMenu(wxMenu *src) {
	wxMenu *dest=new wxMenu;
	wxMenuItemList &items=src->GetMenuItems();

	for(wxMenuItemList::Node *node=items.GetFirst();node;node=node->GetNext()) {
		wxMenuItem *item=node->GetData();
		wxMenu *submenu=item->GetSubMenu();
		int id=item->GetId();
		wxString text=item->GetItemLabel();
		wxString help=item->GetHelp();
		
		if(submenu) {
			dest->Append(id,text,CloneMenu(submenu),help);
		} else {
			switch(item->GetKind()) {
			case wxITEM_SEPARATOR:
				dest->AppendSeparator();
				break;

			case wxITEM_NORMAL:
				dest->Append(id,text,help);
				break;

			case wxITEM_CHECK:
				dest->AppendCheckItem(id,text,help);
				break;

			default:
				BASSERT(false);
				break;
			}
		}
	}

	return dest;
}

void mbMainFrame::InitMenuBar() {
	unsigned i;

	wxMenu *file=new wxMenu;
#ifdef ENABLE_TEST
	file->Append(id_test,"&Test...");
#endif
	file->Append(id_quickstart,"&Quickstart...","Select game to boot from menu");
	file->Append(id_hard_reset,"&Hard reset...","Reset BBC as if switched off then on");
#ifdef bbcENABLE_SAVE_STATE
	file->AppendSeparator();
	file->Append(id_load_state,"&Load state...");
	file->Append(id_save_state,"&Save state...");
#endif
	file->AppendSeparator();
	for(i=0;i<bbcFdd::num_drives;++i) {
		wxString drive_num=wxString::Format("%1.1d",i);
		wxString str;
		wxMenu *drive_menu;
		int base_id=id_drives_begin+i*subid_drive_num;

		drive_menu=new wxMenu;
		drive_menu->Append(base_id+subid_drive_filename,"*PLACEHOLDER*");
		drive_menu->Enable(base_id+subid_drive_filename,false);
		drive_menu->AppendSeparator();
		drive_menu->Append(base_id+subid_drive_load,"&Load...",
			"Insert disc into drive "+drive_num);
		drive_menu->Append(base_id+subid_drive_save,"&Save",
			"Save disc currently in "+drive_num);
		drive_menu->Append(base_id+subid_drive_save_as,"Save &as...",
			"Save disc currently in "+drive_num+" under a new name");
		drive_menu->Append(base_id+subid_drive_unload,"&Unload",
			"Remove disc from "+drive_num);
		file->Append(base_id+subid_drive,"Drive &"+drive_num,drive_menu,
			"Controls for drive "+drive_num);

		drive_submenus_[i]=drive_menu;
	}
	file->AppendSeparator();
	file->Append(id_exit,"E&xit","Exit the program");
	
	wxMenu *options=new wxMenu;
	options->AppendCheckItem(id_toggle_sound,"&Sound enabled",
		"Whether sound will be produced");
	options->AppendCheckItem(id_toggle_limit_speed,"&Limit speed",
		"Whether emulator is limited to max real BBC speed");
	options->AppendCheckItem(id_toggle_fast_forward_disc_access,
		"&Fast forward disc access",
		"Whether emulator should run flat out when acessing the disc");
//	options->AppendCheckItem(id_toggle_vertical_2x,"&Vertical doubling",
//		"Whether to double each scanline");
	options->AppendCheckItem(id_toggle_full_screen,"&Full screen");
	options->AppendCheckItem(id_toggle_status_bar,"&Show status bar",
		"Whether status bar is visible");
	wxMenu *keymaps_submenu=new wxMenu;
	options->Append(id_keymaps_submenu,"&Keymap",keymaps_submenu,
		"Select keyboard mapping");
	wxMenu *resolution_submenu=new wxMenu;
	for(i=0;i<4;++i) {
		resolution_submenu->Append(id_resolution_0+i,"Temp");
	}
	options->Append(id_resolution_submenu,"&Resolution",resolution_submenu,
		"Select window size");
	wxMenu *screen_type_submenu=new wxMenu;
	screen_type_submenu->AppendCheckItem(id_screen_type_colour,"&Colour",
		"Colour TV/monitor");
	screen_type_submenu->AppendCheckItem(id_screen_type_bnw,"&Black && White",
		"Black && white TV");
	screen_type_submenu->AppendCheckItem(id_screen_type_green,"&Green",
		"Green screen monitor (green only)");
	screen_type_submenu->AppendCheckItem(id_screen_type_amber,"&Amber",
		"Amber monitor");
	options->Append(id_screen_type_submenu,"S&creen type",screen_type_submenu,
		"Select screen type");

	{
		wxMenu *wsc=new wxMenu;
		wsc->AppendCheckItem(id_windowed_scanlines_single,"&Single");
		wsc->AppendCheckItem(id_windowed_scanlines_interlace,"&Interlace");
		wsc->AppendCheckItem(id_windowed_scanlines_double,"&Double");
		wxMenu *fssc=new wxMenu;
		fssc->AppendCheckItem(id_full_screen_scanlines_single,"&Single");
		fssc->AppendCheckItem(id_full_screen_scanlines_interlace,"&Interlace");
		fssc->AppendCheckItem(id_full_screen_scanlines_double,"&Double");
		options->Append(id_windowed_scanlines_submenu,"&Windowed scanlines",wsc);
		options->Append(id_full_screen_scanlines_submenu,"&Full screen scanlines",
			fssc);
	}
	options->AppendSeparator();
	options->Append(id_roms_dialog,"&ROMs...","Configure sideways ROMs and OS ROM");
	options->Append(id_sound_dialog,"S&ound...","Configure sound output");
	options->Append(id_keymap_dialog,"K&eymaps...","Define keyboard mappings");
	
	if(HostInp_SupportsJoysticks(hostinp_)) {
		options->Append(id_joysticks_dialog,"&Joysticks...","Configure joysticks");
	}

	options->Append(id_hardware_dialog,"&Hardware...");
	options->Append(id_video_dialog,"&Video...");
//	options->AppendCheckItem(id_toggle_toolbar,"&Toolbar","Toggle toolbar display");

#ifdef bbcDEBUG_ENABLE
	wxMenu *debug;
	{
		debug=new wxMenu;

#if (defined bbcDEBUG_TRACE_EVENTS)
		debug->Append(id_start_disassembly,"St&art disassembly",
			"Start saving a running disassembly");
		debug->Append(id_stop_disassembly,"St&op disassembly",
			"Stop the running disassembly and save results to disc");
		debug->Append(id_reboot_with_disassembly,"Reboot with disassembly",
			"Reboot computer with disassembly running");
#endif

#ifdef BEEB_DEBUG_SAVE_CHALLENGER_RAM
		debug->Append(id_save_challenger_ram,"Save &CHALLENGER RAM",
			"Save CHALLENGER RAM to a file (if CHALLENGER enabled)");
#endif

#ifdef bbcDEBUG_ENABLE
		debug->Append(id_disassemble_ram,"&Disassemble RAM",
			"Save a disassembly of all of RAM");
#endif

#ifdef bbcDEBUG_PANELS
		debug->AppendCheckItem(id_toggle_dbgpanel_sysvia,"S&ystem VIA debug panel");
		debug->AppendCheckItem(id_toggle_dbgpanel_usrvia,"&User VIA debug panel");
		debug->AppendCheckItem(id_toggle_dbgpanel_video,"&Video debug panel");
		debug->AppendCheckItem(id_toggle_dbgpanel_adc,"&ADC debug panel");
#endif

#ifdef bbcDEBUG_VIDEO
		debug->AppendCheckItem(id_toggle_showall,"Show all");
		debug->Append(id_log_video_frames,"Log a few video frames");
		//#ifdef bbcDEBUG_TRACE
		//	debug->Append(id_log_video_frames_disasm,"Log a few video frames w/ disasm");
		//#endif//bbcDEBUG_TRACE
		debug->AppendCheckItem(id_toggle_show_t1_timeouts,"Show sys (R)/usr (G) T1 timeouts");
#endif//bbcDEBUG_VIDEO

#ifdef t65_COUNT_INSTRUCTION_FREQUENCIES
		debug->Append(id_show_instruction_frequencies,"Show instruction frequencies (by opcode)");
		debug->Append(id_show_instruction_frequencies_sorted,"Show instruction frequencies (by freq)");
#endif//t65_COUNT_INSTRUCTION_FREQUENCIES

#ifdef bbcDEBUG_COUNT_MMIO_ACCESSES
		debug->Append(id_show_sheila_access_counts_by_addr,"Show SHEILA access counts (by addr)");
		debug->Append(id_show_sheila_access_counts_by_count,"Show SHEILA access counts (by count)");
#endif

// 		if(B_Log::GetFirstLog()) {
// 			wxMenu *logs_menu=new wxMenu;
// 
// 			for(B_Log *l=B_Log::GetFirstLog();l;l=l->GetNextLog()) {
// 				wxMenuItem *item=logs_menu->Append(id_toggle_log_enabled,l->GetName());
// 				item->SetClientData(l);
// 			}
// 		}
	}
#endif//bbcDEBUG_ENABLE

	wxMenu *special=new wxMenu;
	special->Append(id_sound_record_start,"&Start recording sound");
	special->Append(id_sound_record_stop,"Stop &recording sound...");
	special->AppendSeparator();
	special->Append(id_screenshot_save_as,"&Save screenshot...",
		"Save screen contents to disc");
#ifdef bbcENABLE_PROFILER
	special->AppendSeparator();
	special->Append(id_display_profiler_results,"&Display profiler results");
#endif
#ifdef mbVISUAL_PROFILER
	special->AppendCheckItem(id_show_visual_profile,"Show timings");
#endif
#ifdef bbcENABLE_BEEB_ICE
	special->AppendCheckItem(id_ice_window_toggle,"BeebIce");
#endif

	wxMenu *help=new wxMenu;
	help->Append(id_show_about_box,"&About...","Show help box");

#ifdef NEW_MENU_STUFF
	int brk=0;
#else
	menu_bar_=new wxMenuBar;

	menu_bar_->Append(file,"File");
	popup_menu_.Append(id_file_menu,"File",CloneMenu(file));

	menu_bar_->Append(options,"Options");
	popup_menu_.Append(id_options_menu,"Options",CloneMenu(options));

	menu_bar_->Append(special,"Special");
	popup_menu_.Append(id_special_menu,"Special",CloneMenu(special));

#ifdef bbcDEBUG_ENABLE
	menu_bar_->Append(debug,"Debug");
	popup_menu_.Append(id_debug_menu,"Debug",CloneMenu(debug));
#endif

	menu_bar_->Append(help,"Help");
	popup_menu_.Append(id_help_menu,"Help",CloneMenu(help));
#endif

	this->SetMenuBar(menu_bar_);

	//printf("id=%d\n",menu_bar_->FindItem(id_keymaps_submenu)->GetRealId());

//	this->RefreshWindow();
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::ModalBackground() {
	if(hostsnd_) {
		HostSnd_PlayStop(hostsnd_);
	}
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::GetConfig(mbConfig *cfg) const {
	wxASSERT(cfg);
	*cfg=cfg_;
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnMenuOpen(wxMenuEvent &) {
	wxLogDebug("OnMenuOpen: this->ModalBackground()");
	this->ModalBackground();
	//HostGfx_SetFullScreen(hostgfx_,false);
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnMenuClose(wxMenuEvent &) {
//	event.Skip();
	if(hostgfx_&&HostGfx_IsFullScreen(hostgfx_)) {
		//HostGfx_Cls(hostgfx_);
		wxLogDebug("OnMenuClose: clearing beeb screen.");
		//beeb_screen_->Clear();
	}
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnExit(wxCommandEvent &) {
	this->Close();
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnToggleSound(wxCommandEvent &) {
	this->SetSoundEnabled(!cfg_.sound.enabled);
	this->RefreshWindow();
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnToggleLimitSpeed(wxCommandEvent &) {
	cfg_.misc.limit_speed=!cfg_.misc.limit_speed;
	this->RefreshWindow();
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnShowAboutBox(wxCommandEvent &) {
	this->ModalBackground();
	wxString msg;
	
	msg+=wxString::Format("This is version %s\n",mb_version);
	msg+="\n";
	msg+="Tom Seddon <modelb@bbcmicro.com>\n";
	msg+="\n";
	msg+=wxString::Format("Compiled at %s on %s\n",__TIME__,__DATE__);
	wxMessageDialog about(this,msg,"Author",wxOK|wxCENTRE|wxICON_INFORMATION);
	about.ShowModal();
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnPaint(wxPaintEvent &) {
	wxPaintDC dc(this);
//	if(auto_paint_) {

//#ifdef VERBOSITY
	wxLogDebug("%s: painting.",__FUNCTION__);
//#endif

#ifdef __WXGTK__
	// quick hack -- looks like m_widget isn't set up until late.
	// TODO could do this for all platforms (but not all the time like this!)
	if(!hostgfx_) {
		unsigned flags=0;

		if(cl_no_shm_) {
			flags|=HGF_NO_SHM;

			if(cl_no_xv_) {
				flags|=HGF_NO_XV;
			}
		}

		//printf("m_widget->window=%p beeb_screen_->m_widget->window=%p\n",m_widget->window,beeb_screen_->m_widget->window);
		GdkWindow *gdkw=m_wxwindow->window;//beeb_screen_->m_wxwindow->window;
		Display *xdisplay=GDK_WINDOW_XDISPLAY(gdkw);
		Window xwindow=GDK_WINDOW_XWINDOW(gdkw);
		Window xtarget=GDK_WINDOW_XWINDOW(beeb_screen_->m_widget->window);
		hostgfx_=HostGfx_Start(xdisplay,xwindow,xtarget,flags);
		this->SetHostGfxAdapter();
	}
#endif//__WXGTK__
	
	if(!hostgfx_||!last_video_buffer_) {
		wxLogDebug("%s:     blanking; hostgfx_%p last_video_buffer_=%p",__FUNCTION__,hostgfx_,last_video_buffer_);
		dc.SetBrush(*wxBLACK_BRUSH);
		dc.Clear();
		dc.SetBrush(wxNullBrush);
	} else {
		//Interlace won't look very good, because we're not drawing very often. So pretend
		//interlaced mode is double mode (which it is, kind of). Pass dummy frame number to
		//PresentLastVideoBuffer as it's not required.
		mbScanlinesMode scanlines_mode=HostGfx_IsFullScreen(hostgfx_)?cfg_.video.full_screen_scanlines:cfg_.video.windowed_scanlines;
		if(scanlines_mode==mbSCANLINES_INTERLACE) {
			scanlines_mode=mbSCANLINES_DOUBLE;
		}

		this->PresentLastVideoBuffer(scanlines_mode,HostGfx_PRESENT_HINT_TIDY,0,0,0);
		//wxLogDebug(__FUNCTION__ ":     doing nowt, this bit not done yet.");
		//HostGfx_Present()
//		if(HostGfx_IsFullScreen(hostgfx_)) {
//			wxLogDebug(__FUNCTION__ ":     full screen, last_video_buffer_=%p",last_video_buffer_);
//			if(last_video_buffer_) {
//				HostGfx_Present(hostgfx_,last_video_buffer_->data,last_video_buffer_->width,last_video_buffer_->height,
//					last_video_buffer_->pitch);
//			}
//		} else {
//			wxLogDebug(__FUNCTION__ ":     windowed.");
//			HostGfx_ShowWindowed(hostgfx_);
//		}
	}
//	}
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnEraseBackground(wxEraseEvent &) {
	return;
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnSize(wxSizeEvent &event) {
	this->ModalBackground();
//	wxSize new_size=event.GetSize();
//	this->SetSize(new_size.GetWidth(),400);
	this->RefreshCfgWindowState();
	event.Skip();
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnMove(wxMoveEvent &event) {
	this->ModalBackground();
	this->RefreshCfgWindowState();
	event.Skip();
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::SetAutoPaint(bool new_auto_paint) {
	auto_paint_=new_auto_paint;
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::DoUpdateSound() {
	HostTmr_HfValue now_hf_time;

	HostTmr_ReadHf(&now_hf_time);
	
	if(cfg_.misc.limit_speed&&!beeb_fastforward_) {
		BPR(bpr_limit_speed);
//		HostTmr_HfValue sound_tick_length=hf_freq_/update_sound_freq;
//		if(now_hf_time-last_update_sound_hf_time_<sound_tick_length) {
//			HostTmr_HfValue wait_reqd=sound_tick_length-(now_hf_time-last_update_sound_hf_time_);
//			int wait_reqd_ms=wait_reqd/(hf_freq_/1000);
//			Sleep(wait_reqd_ms);
//		}

		HostTmr_HfValue sound_tick_length=hf_freq_/update_sound_freq;
		HostTmr_HfValue start_wait_time;
		HostTmr_ReadHf(&start_wait_time);

		HostTmr_HfValue time_left;
		while((time_left=now_hf_time-last_update_sound_hf_time_)<sound_tick_length) {
#ifdef __WXMSW__
			// more than 1ms or so? then wait. Sleep is inaccurate, so don't be too exact.
			if(time_left>hf_freq_/500) {
				Sleep(1);
			}
#endif
			HostTmr_ReadHf(&now_hf_time);
		}

//		{
//			static int ct=0;
//			static __int64 t=0;
//
//			t+=now_hf_time-start_wait_time;
//			++ct;
//			if(ct>250) {
//				static char arse[100];
//				_snprintf(arse,sizeof arse-1,"%I64d\n",t/ct);
//				OutputDebugStringA(arse);
//				ct=0;
//				t=0;
//			}
//		}
	}

	BPR(bpr_update_sound);
	if(!cfg_.sound.enabled||!hostsnd_) {
		BASSERT(!cfg_.sound.enabled||(cfg_.sound.enabled&&hostsnd_));
		//doesn't matter whether the 8 or 16 bit version is used here!
		bbcSound::Render16BitMono(0,0,0);
	} else {
		HostSnd_PlayStart(hostsnd_);
		void *ps[2];
		unsigned sizes[2];
		//sound_buffer_->GetCurrentPosition(&snd_play,&snd_write);
		int flag=HostSnd_GetWriteArea(hostsnd_,&ps[0],&sizes[0],&ps[1],&sizes[1]);
		if(flag<0) {
			bbcSound::Render16BitMono(0,0,0);
		} else if(flag>0) {
			static void (*(sound_renderers[]))(void **,unsigned *,unsigned)={
				&bbcSound::Render8BitMono,
				&bbcSound::Render8BitStereo,
				&bbcSound::Render16BitMono,
				&bbcSound::Render16BitStereo,
			};
			unsigned num_buffers=sizes[1]>0?2:1;
			unsigned idx=(cfg_.sound.stereo?1:0)|(cfg_.sound.bits==16?2:0);
			(*sound_renderers[idx])(ps,sizes,num_buffers);

			HostSnd_CommitWriteArea(hostsnd_);
		}
	}
	last_update_sound_hf_time_=now_hf_time;
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::SetSoundEnabled(bool new_sound_enabled) {
	if(new_sound_enabled) {
		if(!hostsnd_) {
			wxLogDebug("mbMainFrame::SetSoundEnabled(true): HostSnd_Start");
			hostsnd_=HostSnd_Start(this->GetHandle());
			if(hostsnd_) {
				wxLogDebug("mbMainFrame::SetSoundEnabled(true): HostSnd_* init stuff");
				int len_samples=cfg_.sound.hz/update_sound_freq*cfg_.sound.len_frames;
				wxLogDebug("Sound: %dHz %d bits %s %d samples",
					cfg_.sound.hz,cfg_.sound.bits,cfg_.sound.stereo?"stereo":"mono",
					len_samples);
				HostSnd_SetSoundParameters(hostsnd_,cfg_.sound.hz,cfg_.sound.bits,
					cfg_.sound.stereo?2:1,len_samples);
				bbcSound::SetFrequency(cfg_.sound.hz);
				for(unsigned i=0;i<4;++i) {
					bbcSound::SetChannelStereoLeftRight(i,
						cfg_.sound.is_stereo_left[i],cfg_.sound.is_stereo_right[i]);
				}
				HostSnd_PlayStart(hostsnd_);
//				Sleep(10);
//				HostSnd_SetVolume(hostsnd_,cfg_.sound.volume);
			}
		}
	} else {
		if(hostsnd_) {
			wxLogDebug("mbMainFrame::SetSoundEnabled(false): HostSnd_Stop");
			HostSnd_Stop(hostsnd_);
			hostsnd_=0;
		}
	}
	cfg_.sound.enabled=new_sound_enabled;
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::SetHostGfxAdapter() {
	const HostGfx_Adapter *const *adapters=HostGfx_GetAdapters(hostgfx_);
	
	int adapter_idx;
	for(adapter_idx=0;adapter_idx<HostGfx_NumAdapters(hostgfx_);++adapter_idx) {
		if(adapters[adapter_idx]->device_name==cfg_.video.full_screen_adapter) {
			break;
		}
	}

	HostGfx_SetAdapter(hostgfx_,adapter_idx);
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::RefreshWindow() {
	wxLogDebug("%s: called.",__FUNCTION__);
	//firstly, set up display


	if(hostgfx_) {
		this->SetHostGfxAdapter();

		wxLogDebug("RefreshWindow: full screen=%s",cfg_.video.full_screen?"yes":"no");
//		if(cfg_.video.full_screen) {
//			HostGfx_SetScanlinesMode(hostgfx_,cfg_.video.full_screen_scanlines);
//		} else {
//			HostGfx_SetScanlinesMode(hostgfx_,cfg_.video.windowed_scanlines);
//		}

		float palette[8][3];
		
		switch(cfg_.video.monitor_type) {
		default:
			BASSERT(false);
			cfg_.video.monitor_type=mbMONITOR_COLOUR;
		case mbMONITOR_COLOUR:
			for(unsigned i=0;i<8;++i) {
				palette[i][0]=i&1;
				palette[i][1]=(i>>1)&1;
				palette[i][2]=(i>>2)&1;
			}
			break;
		case mbMONITOR_BNW:
			GeneratePalette(palette,1.f,1.f,1.f);
			break;
		case mbMONITOR_AMBER:
			GeneratePalette(palette,1.f,.8f,.1f);
			break;
		case mbMONITOR_GREEN:
			GeneratePalette(palette,.2f,.9f,.1f);
			break;
		}
		HostGfx_SetPalette(hostgfx_,palette);
		
//		bbcVideo::SetPixelFixedBits(cfg_.video.full_screen?
//			HostGfx_FULL_SCREEN_PALETTE_BASE:0);
		//	status_bar_->SetResizeable()
		static const long fs_style=wxFULLSCREEN_NOBORDER|wxFULLSCREEN_NOCAPTION|
			wxFULLSCREEN_NOMENUBAR;

		//inhibit any updating of window states, as the window is mucked about with
		//when switching between windowed and full screen modes.
		bool old_update_window_state=update_window_state_;
		update_window_state_=false;
		bool full_screen_error=false;
		if(cfg_.video.full_screen) {
			//There's a bug in wxWindows; it saves the curent window rect as the unmaximized size
			//if the window is maximized.
			if(this->IsMaximized()) {
				this->Maximize(false);//but this won't update the current is_maximized setting
			}


			//int full_screen_adapter_index=0;
			//for(int i=0;i<HostGfx_NumAdapters(hostgfx_);++i) {
				//const HostGfx_Adapter *adapter=HostGfx_GetAdapters(hostgfx_)[i];

				//if(adapter->device_name==cfg_.video.full_screen_adapter) {
					//full_screen_adapter_index=i;
					//break;
				//} else if(adapter->default_device) {
					//full_screen_adapter_index=i;
				//}
			//}

			wxLogDebug("%s: setting full screen: ShowFullScreen then HostGfx_SetFullScreen.",__FUNCTION__);
			this->ShowFullScreen(true,fs_style);

			HostGfx_SetFullScreen(hostgfx_,true,cfg_.video.full_screen_refresh_rate);
			if(!HostGfx_IsFullScreen(hostgfx_)) {
				cfg_.video.full_screen=false;
				full_screen_error=true;
			}
		}
		
		if(!cfg_.video.full_screen) {
			wxLogDebug("%s: HostGfx_SetFullScreen then ShowFullScreen.",__FUNCTION__);
			HostGfx_SetFullScreen(hostgfx_,false,cfg_.video.full_screen_refresh_rate);
			this->ShowFullScreen(false,fs_style);
			
			//Coming out of full screen, we won't ever be maximized, because we don't go in
			//maximized. So if we were maximized before, do that again.
			if(cfg_.misc.window_is_maximized) {
				this->Maximize(true);
			}
		}

		if(full_screen_error) {
			wxLogError("Failed to set up full screen mode.\nThe emulator will continue to run windowed.");
		}
		update_window_state_=old_update_window_state;
	}
	
	if(hostsnd_) {
		HostSnd_SetVolume(hostsnd_,cfg_.sound.volume);
	}

	//tweak ui
	this->SetMenuItemCheck(id_toggle_sound,cfg_.sound.enabled);
	this->SetMenuItemCheck(id_toggle_limit_speed,cfg_.misc.limit_speed);
	this->SetMenuItemCheck(id_toggle_status_bar,cfg_.misc.show_status_bar);
	this->SetMenuItemCheck(id_toggle_fast_forward_disc_access,
		cfg_.misc.fast_forward_disc_access);
	//this->SetMenuItemCheck(id_toggle_vertical_2x,cfg_.video.vertical_2x);
	this->SetMenuItemCheck(id_toggle_full_screen,cfg_.video.full_screen);
	
#ifdef bbcDEBUG_PANELS
	this->SetMenuItemCheck(id_toggle_dbgpanel_sysvia,!!this->FindWindow(id_dbgpanel_sysvia));
	this->SetMenuItemCheck(id_toggle_dbgpanel_usrvia,!!this->FindWindow(id_dbgpanel_usrvia));
	this->SetMenuItemCheck(id_toggle_dbgpanel_video,!!this->FindWindow(id_dbgpanel_video));
#endif
#ifdef bbcDEBUG_VIDEO
	this->SetMenuItemCheck(id_toggle_showall,bbcVideo::show_all_);
	this->SetMenuItemCheck(id_toggle_show_t1_timeouts,bbcVideo::show_t1_timeouts_);
#endif

	for(unsigned i=0;i<4;++i) {
		int id=id_resolution_0+i;
		wxSize size=this->ResolutionFromId(id);
		wxString text=wxString::Format("%dx%d",size.GetWidth(),size.GetHeight());
		this->SetMenuItemText(id,text);
	}

//	HostGfx_BeebScreenType screen_type=HostGfx_COLOUR;
//	if(hostgfx_) {
//		screen_type=HostGfx_ScreenType(hostgfx_);
//	}
	this->SetMenuItemCheck(id_screen_type_colour,cfg_.video.monitor_type==mbMONITOR_COLOUR);
	this->SetMenuItemCheck(id_screen_type_bnw,cfg_.video.monitor_type==mbMONITOR_BNW);
	this->SetMenuItemCheck(id_screen_type_amber,cfg_.video.monitor_type==mbMONITOR_AMBER);
	this->SetMenuItemCheck(id_screen_type_green,cfg_.video.monitor_type==mbMONITOR_GREEN);

	this->SetMenuItemCheck(id_windowed_scanlines_single,cfg_.video.windowed_scanlines==mbSCANLINES_SINGLE);
	this->SetMenuItemCheck(id_windowed_scanlines_interlace,cfg_.video.windowed_scanlines==mbSCANLINES_INTERLACE);
	this->SetMenuItemCheck(id_windowed_scanlines_double,cfg_.video.windowed_scanlines==mbSCANLINES_DOUBLE);
	this->SetMenuItemCheck(id_full_screen_scanlines_single,cfg_.video.full_screen_scanlines==mbSCANLINES_SINGLE);
	this->SetMenuItemCheck(id_full_screen_scanlines_interlace,cfg_.video.full_screen_scanlines==mbSCANLINES_INTERLACE);
	this->SetMenuItemCheck(id_full_screen_scanlines_double,cfg_.video.full_screen_scanlines==mbSCANLINES_DOUBLE);

#ifdef mbVISUAL_PROFILER
	this->SetMenuItemCheck(id_show_visual_profile,cfg_.debug.show_visual_profile);
#endif
#ifdef bbcENABLE_BEEB_ICE
	this->SetMenuItemCheck(id_ice_window_toggle,!!ice_cfg_.enabled);
#endif

	bool is_fs=hostgfx_&&HostGfx_IsFullScreen(hostgfx_);
//	this->SetMenuItemEnabled(id_resolution_0,!is_fs);
//	this->SetMenuItemEnabled(id_resolution_1,!is_fs);
//	this->SetMenuItemEnabled(id_resolution_2,!is_fs);
//	this->SetMenuItemEnabled(id_resolution_3,!is_fs);
	if(popup_menu_.FindItem(id_resolution_submenu)) {//9/9/04 check added, fix for graphics init failure (assert in debug build)
		popup_menu_.Enable(id_resolution_submenu,!is_fs);//TODO doesn't work, don't know why.
	}
	this->SetMenuItemEnabled(id_resolution_submenu,!is_fs);

	// fix up status bar
	status_bar_->Show(false);

	wxASSERT(this->GetSizer());
	this->GetSizer()->Detach(status_bar_);
	this->GetSizer()->Layout();


	if(cfg_.misc.show_status_bar) {
		status_bar_->Show(true);
		status_bar_->SetVolumeEnabled(cfg_.sound.enabled);
		status_bar_->SetVolume(cfg_.sound.volume);
		this->GetSizer()->Add(status_bar_,0,wxEXPAND|wxFIXED_MINSIZE);
		this->GetSizer()->Layout();
	}

	//
	if(menu_bar_) {//9/9/04 fix for gfx init failure on startup
		for(int i=0;i<bbcFdd::num_drives;++i) {
			bbcFdd::DriveStatusType st=bbcFdd::DriveStatus(i);
			int id_base=id_drives_begin+i*subid_drive_num;

			if(st==bbcFdd::DRIVE_SIDE2) {
				this->SetMenuItemEnabled(id_base+subid_drive,false);
			} else {
				const DriveUi *ui=&drive_uis_[i];
				this->SetMenuItemEnabled(id_base+subid_drive,true);
				bool loaded=st!=bbcFdd::DRIVE_EMPTY;
				//this->SetMenuItemEnabled(id_base+subid_drive_load,!loaded);
				this->SetMenuItemEnabled(id_base+subid_drive_save,loaded&&!ui->no_save);
				//6/8/05 should always be able to save as!
				this->SetMenuItemEnabled(id_base+subid_drive_save_as,loaded);//&&!ui->no_save);
				this->SetMenuItemEnabled(id_base+subid_drive_unload,loaded);

				wxMenuItem *item=menu_bar_->FindItem(id_base+subid_drive_filename);
				wxASSERT(item);
				if(!loaded) {
					item->SetItemLabel("*PLACEHOLDER*");
				} else {
					item->SetItemLabel(ui->disc_image.GetFullPath());
				}
			}

			if(recent_discs_dirty_) {
				for(int j=subid_drive_sep;j<=subid_drive_recent_last;++j) {
					if(drive_submenus_[i]->FindItem(id_base+j)) {
						drive_submenus_[i]->Delete(id_base+j);
					}
				}

				if(!cfg_.misc.recent_discs.empty()) {
					wxMenuItem *last=drive_submenus_[i]->FindItemByPosition(drive_submenus_[i]->GetMenuItemCount()-1);
					if(!last->IsSeparator()) {
						drive_submenus_[i]->AppendSeparator();
					}

					for(unsigned j=0;j<cfg_.misc.recent_discs.size();++j) {
						wxFileName fn(cfg_.misc.recent_discs[j].c_str());
						if(fn.IsOk()) {
							drive_submenus_[i]->Append(id_base+subid_drive_recent_first+j,fn.GetFullPath());
						}
					}
				}

				recent_discs_dirty_=false;
			}
		}

		//Update keymaps
		this->RefreshKeymapsSubmenu(menu_bar_->FindItem(id_keymaps_submenu));
		this->RefreshKeymapsSubmenu(popup_menu_.FindItem(id_keymaps_submenu));
	}


	//Update joystick indexes
	if(hostinp_) {
		for(int i=0;i<2;++i) {
			int num_joysticks=HostInp_NumJoysticks(hostinp_);
			int j;
			for(j=0;j<num_joysticks;++j) {
				const char *device_name=HostInp_JoystickName(hostinp_,j);;
				if(stricmp(cfg_.joysticks.joysticks[j].device_name.c_str(),device_name)==0) {
					break;
				}
			}

			joystick_indexes_[i]=j<num_joysticks?j:-1;
		}
	}

#ifdef bbcENABLE_BEEB_ICE
	if(ice_frame_&&!ice_cfg_.enabled) {
		ice_frame_->GetConfig(&ice_cfg_);
		ice_frame_->Destroy();
		ice_frame_=0;
	} else if(!ice_frame_&&ice_cfg_.enabled) {
		// when the mbiceframe is created, this window gets some activate events, and would otherwise end up creating too many
		// windows. so, until it's all done, pretend the window is disabled.
		ice_cfg_.enabled=false;
		ice_frame_=new mbIceFrame(this,ice_cfg_);
		ice_frame_->Show();
		ice_cfg_.enabled=true;
	}
#endif

	//
	if(this->GetMenuBar()) {
		wxASSERT(this->GetMenuBar()==menu_bar_);
		menu_bar_->Refresh();
	}

	//Clear any rubbish that may have accumulated.
	//This is done too often. It doesn't matter.
	beeb_screen_->ClearBackground();
}

//////////////////////////////////////////////////////////////////////////

void mbMainFrame::RefreshKeymapsSubmenu(wxMenuItem *item) {
	BASSERT(item);
	wxMenu *keymaps_submenu=item->GetSubMenu();
	BASSERT(keymaps_submenu);

	//take them out
	wxMenuItemList &menu_items=keymaps_submenu->GetMenuItems();
	if(menu_items.GetCount()>0) {
		//i'm a bit concerned about the deletion changing menu_items...
		std::vector<wxMenuItem *> items;
		for(wxMenuItemList::Node *node=menu_items.GetFirst();node;node=node->GetNext()) {
			items.push_back(node->GetData());
		}
		for(unsigned j=0;j<items.size();++j) {
			keymaps_submenu->Delete(items[j]);
		}
	}

	//put them in
	int j=id_first_keymap;
	mbKeymap::C::const_iterator km;
	for(km=cfg_.keyboard.keymaps.begin();km!=cfg_.keyboard.keymaps.end();++km) {
		if(j>=id_first_keymap+max_num_keymaps) {
			//You don't get any more than that!!
			break;
		}
		keymaps_submenu->AppendCheckItem(j,km->name.c_str());
		keymaps_submenu->Check(j,stricmp(km->name.c_str(),cfg_.keyboard.selected_keymap.c_str())==0);
		++j;
	}

	item->Enable(keymaps_submenu->GetMenuItemCount()>0);
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::SetMenuItemEnabled(int menu_item_id,bool enabled) {
	if(menu_bar_) {
		wxMenuItem *menu_item;

		menu_item=menu_bar_->FindItem(menu_item_id);
		if(menu_item) {
			menu_item->Enable(enabled);
		}

		menu_item=popup_menu_.FindItem(menu_item_id);
		if(menu_item) {
			menu_item->Enable(enabled);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::SetMenuItemCheck(int menu_item_id,bool checked) {
	if(menu_bar_) {
		wxMenuItem *menu_item;
		
		menu_item=menu_bar_->FindItem(menu_item_id);
		if(menu_item) {
			menu_item->Check(checked);
		}

		menu_item=popup_menu_.FindItem(menu_item_id);
		if(menu_item) {
			menu_item->Check(checked);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::SetMenuItemText(int menu_item_id,const wxString &text) {
	if(menu_bar_) {
		wxMenuItem *menu_item;
		
		menu_item=menu_bar_->FindItem(menu_item_id);
		if(menu_item) {
			menu_item->SetItemLabel(text);
		}

		menu_item=popup_menu_.FindItem(menu_item_id);
		if(menu_item) {
			menu_item->SetItemLabel(text);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
static wxString WildcardsFromDiscFormat(const mbDiscFormat &fmt) {
	wxString wildcards;

	for(unsigned i=0;i<fmt.extensions.size();++i) {
		if(!wildcards.empty()) {
			wildcards+=";";
		}
		wildcards+="*."+fmt.extensions[i];
	}

	return wildcards;
}

void mbMainFrame::OnDriveLoadInternal(wxCommandEvent &event,int drive) {
	(void)event;

	wxString dialog_wildcards;
	wxString all_wildcards;
	std::vector<mbDiscFormat> fmts;

	//
	mbDiscFormatGetAll(&fmts);
	for(unsigned i=0;i<fmts.size();++i) {
		wxString wildcards=WildcardsFromDiscFormat(fmts[i]);
		if(!all_wildcards.empty()) {
			all_wildcards+=";";
		}
		all_wildcards+=wildcards;
		dialog_wildcards+=fmts[i].description+" ("+wildcards+")|"+wildcards+"|";
//		wxString wildcards;
//		for(unsigned j=0;j<fmts[i].extensions.size();++j) {
//			if(!wildcards.empty()) {
//				wildcards+=";";
//			}
//			wildcards+="*."+fmts[i].extensions[j];
//		}
//		if(!all_wildcards.empty()) {
//			all_wildcards+=";";
//		}
//		all_wildcards+=wildcards;
//		dialog_wildcards+=fmts[i].description+" ("+wildcards+")|"+wildcards+"|";
	}
	dialog_wildcards="Autodetect ("+all_wildcards+")|"+all_wildcards+"|"+dialog_wildcards;

	//wxLogDebug("%s: dialog_wildcards=\"%s\"",__FUNCTION__,dialog_wildcards.c_str());
	if(dialog_wildcards.Last()=='|') {
		dialog_wildcards.RemoveLast();
	}

	//
	DriveUi *ui=&drive_uis_[drive];
	wxFileDialog fd(this,"Select disc",ui->last_path,"",dialog_wildcards,wxFD_OPEN);
	fd.SetFilterIndex(ui->last_filter_index);
	int r=fd.ShowModal();
	if(r==wxID_CANCEL) {
		return;
	}

	//
	ui->last_filter_index=fd.GetFilterIndex();
	ui->last_path=fd.GetPath();
	this->UiLoadDisc(drive,fd.GetPath());
}

//////////////////////////////////////////////////////////////////////////
// UiUnloadDrive -- does user interface for unloading a drive, if that's
// necessary. Does not handle side 2 drives. Adds disc to recent discs
// list if returning true.
bool mbMainFrame::UiUnloadDrive(int drive) {
	BASSERT(drive>=0);

	//Early out if drive empty.
	bbcFdd::DriveStatusType stdrive=bbcFdd::DriveStatus(drive);
	if(stdrive==bbcFdd::DRIVE_EMPTY) {
		return true;
	}

	//'drive' should be the top side.
	BASSERT(stdrive!=bbcFdd::DRIVE_SIDE2);

	int other=bbcFdd::other_side_of[drive];
	bbcFdd::DriveStatusType stother=bbcFdd::DriveStatus(other);
	bool is_ds=stother==bbcFdd::DRIVE_SIDE2;

	if(stdrive==bbcFdd::DRIVE_CHANGED) {
		wxString msg;
		
		msg+=wxString::Format("Drive %d contains \"%s\".\n",
			drive,drive_uis_[drive].disc_image.GetFullPath().c_str());
		if(is_ds) {
			msg+=wxString::Format("(Unloading drive %d will also unload drive %d.)\n\n",
				drive,other);
		}
		msg+="This disc image has changed.\n";
		msg+="If you continue, these changes will be lost.\n\n";
		msg+="Do you want to continue?";
		int result=wxMessageBox(msg,wxString::Format("Unload drive %d",drive),
			wxYES_NO,this);
		return result==wxYES;
	}

//	cfg_.misc.AddRecentDisc(drive_uis_[drive].disc_image.GetFullPath().c_str());
	return true;
}

//////////////////////////////////////////////////////////////////////////
bool mbMainFrame::UiLoadDisc(int drive,wxFileName file_name) {
	wxString name=file_name.GetFullPath();

	//Load disc image
	std::vector<t65::byte> contents;
//	OutputDebugStringA(name.c_str());
//	OutputDebugStringA("\n");
	if(!file_name.IsOk()) {
		wxLogError("Invalid filename \"%s\"",name.c_str());
		return false;
	}
	bool loaded_ok=mbLoadFile(name,contents);
	if(!loaded_ok) {
		wxLogError("Failed to load \"%s\"",name.c_str());
		return false;
	}
	file_name.Normalize();

	//Determine the geometry of this disc image.
	mbDiscFormat fmt;
	if(!mbDiscFormatGetFromFile(file_name,contents.size(),&fmt)) {
		wxLogError("Cannot determine disc format of file \"%s\"",name.c_str());
		return false;
	}

	bool ok=false;
	switch(fmt.sides) {
	case 1:
		{
			if(!this->UiUnloadDrive(drive)) {
				return false;
			}
			bbcFdd::UnloadDiscImage(drive);
			ok=bbcFdd::LoadSsDiscImage(drive,fmt.sectors,fmt.density,contents);
			wxLogDebug("mbMainFrame::UiLoadDisc: load drive %d from \"%s\"",drive,name.c_str());
			if(!ok) {
				wxLogError("Failed to load drive %d from \"%s\"",drive,name.c_str());
			}
		}
		break;
	case 2:
		{
			int other=bbcFdd::other_side_of[drive];
			bool is_ds=bbcFdd::DriveStatus(other)==bbcFdd::DRIVE_SIDE2;
			if(!this->UiUnloadDrive(drive)||(!is_ds&&!this->UiUnloadDrive(other))) {
				return false;
			}
			//'drive' is top
			if(other<drive) {
				std::swap(other,drive);
			}
			bbcFdd::UnloadDiscImage(drive);
			bbcFdd::UnloadDiscImage(other);
			wxLogDebug("mbMainFrame::UiLoadDisc: load drive %d from \"%s\"",drive,name.c_str());
			ok=bbcFdd::LoadDsDiscImage(drive,fmt.sectors,fmt.density,contents);
			if(!ok) {
				wxLogError("Failed to load drives %d+%d from \"%s\"",drive,other,name.c_str());
			}
		}
		break;
	}
	if(ok) {
		//Assign to that drive
		drive_uis_[drive].disc_image=file_name;
		drive_uis_[drive].no_save=false;
		cfg_.misc.AddRecentDisc(drive_uis_[drive].disc_image.GetFullPath().ToStdString());
		recent_discs_dirty_=true;
	}
	this->RefreshWindow();
	return ok;
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnDriveLoad(wxCommandEvent &event,int drive) {
	this->ModalBackground();
//	wxASSERT(bbcFdd::DriveStatus(drive)==bbcFdd::DRIVE_EMPTY);
	this->OnDriveLoadInternal(event,drive);
	this->RefreshWindow();
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnDriveSave(wxCommandEvent &event,int drive) {
	(void)event;

	this->ModalBackground();
	wxASSERT(bbcFdd::DriveStatus(drive)!=bbcFdd::DRIVE_EMPTY);
	wxASSERT(bbcFdd::DriveStatus(drive)!=bbcFdd::DRIVE_SIDE2);
	bool ok;
	std::vector<t65::byte> contents;
	if(bbcFdd::IsDs(drive)) {
		int other=bbcFdd::other_side_of[drive];
		ok=bbcFdd::GetDsDiscImage(drive,other,&contents);
	} else {
		ok=bbcFdd::GetSsDiscImage(drive,&contents);
	}
	//bbcFdd::GetDiscImage(drive,contents);
	wxString filename=drive_uis_[drive].disc_image.GetFullPath();
	if(!ok||!mbSaveFile(filename,contents)) {
		wxLogWarning("Failed to save file\n%s",filename.c_str());
	} else {
		bbcFdd::SetDriveChangedStatus(drive,false);
	}
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnDriveSaveAs(wxCommandEvent &event,int drive) {
	(void)event;

	this->ModalBackground();
	
	wxASSERT(bbcFdd::DriveStatus(drive)!=bbcFdd::DRIVE_EMPTY);//neither of these should be...
	wxASSERT(bbcFdd::DriveStatus(drive)!=bbcFdd::DRIVE_SIDE2);//...possible to do from the ui

	wxString cur_ext=drive_uis_[drive].disc_image.GetExt().Lower();

	//disc must be saved in the same format. determine disc format
	//for the image in this drive.
	std::vector<mbDiscFormat> fmts;
	mbDiscFormatGetAll(&fmts);
	unsigned fmtidx;
	for(fmtidx=0;fmtidx<fmts.size();++fmtidx) {
		unsigned j;
		const mbDiscFormat *fmt=&fmts[fmtidx];
		for(j=0;j<fmt->extensions.size();++j) {
			if(cur_ext==fmt->extensions[j].Lower()) {
				break;
			}
		}
		if(j<fmt->extensions.size()) {
			break;
		}
	}
	
	if(fmtidx>=fmts.size()) {
		wxLogError("Drive %d somehow contains an unknown type of disc image.",drive);
	} else {
		const mbDiscFormat *fmt=&fmts[fmtidx];
		wxString wildcards=WildcardsFromDiscFormat(*fmt);
		wildcards=fmt->description+" ("+wildcards+")|"+wildcards;
		DriveUi *ui=&drive_uis_[drive];

		wxFileDialog fd(this,"Save disc",ui->last_path,"",wildcards,wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
		int r=fd.ShowModal();
		if(r==wxID_OK) {
			bool ok;
			std::vector<t65::byte> contents;
			if(bbcFdd::IsDs(drive)) {
				int other=bbcFdd::other_side_of[drive];
				ok=bbcFdd::GetDsDiscImage(drive,other,&contents);
			} else {
				ok=bbcFdd::GetSsDiscImage(drive,&contents);
			}
			wxASSERT(ok);

			if(!mbSaveFile(fd.GetPath(),contents)) {
				wxLogError("Failed to save \"%s\"",fd.GetPath().c_str());
			} else {
				ui->disc_image=wxFileName(fd.GetPath());
				bbcFdd::SetDriveChangedStatus(drive,false);
				this->RefreshWindow();
			}
		}
	}

	//

}


//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnDriveXCommand(wxCommandEvent &event) {
	wxASSERT(event.GetId()>=id_drives_begin&&event.GetId()<id_drives_begin+num_drive_ids);
	int id=event.GetId();
	id-=id_drives_begin;
	int subaction=id%subid_drive_num;
	int drive=id/subid_drive_num;

	if(subaction>=subid_drive_recent_first&&subaction<=subid_drive_recent_last) {
		unsigned index=subaction-subid_drive_recent_first;
		BASSERT(index>=0&&index<cfg_.misc.recent_discs.size());
		wxFileName file_name(cfg_.misc.recent_discs[index].c_str());
		this->UiLoadDisc(drive,file_name);
	} else {
		switch(subaction) {
		case subid_drive:
			break;

		case subid_drive_filename:
			break;

		case subid_drive_load:
			this->OnDriveLoad(event,drive);
			break;

		case subid_drive_save:
			this->OnDriveSave(event,drive);
			break;

		case subid_drive_save_as:
			this->OnDriveSaveAs(event,drive);
			break;

		case subid_drive_unload:
			this->OnDriveUnload(event,drive);
			break;

		default:
			BASSERT(false);
			break;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnDriveUnload(wxCommandEvent &event,int drive) {
	(void)event;

	bbcFdd::DriveStatusType status=bbcFdd::DriveStatus(drive);
	wxASSERT(status!=bbcFdd::DRIVE_EMPTY);
	if(status==bbcFdd::DRIVE_CHANGED) {
		static const wxString msg=
			"If this drive is unloaded, changes will be lost.\n"
			"\n"
			"Are you sure?";
		int r=wxMessageBox(msg,wxString::Format("Unload drive %d",drive),wxYES_NO);
		if(r!=wxYES) {
			return;
		}
	}
	bbcFdd::UnloadDiscImage(drive);
	//drive_uis_[drive].loaded=false;
	this->RefreshWindow();
	return;
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnQuickstart(wxCommandEvent &) {
	beeb_paused_=true;
	this->ModalBackground();
	mbQuickstartResult r;
	if(mbQuickstart(this,cfg_.quickstart,&r)) {
		if(this->UiLoadDisc(0,r.drive0)&&
			(!r.drive2.IsOk()||this->UiLoadDisc(2,r.drive2)))
		{
			bbcKeyboardMatrix::SetForceShiftPressed(true);
			this->PowerOnBeeb();// 7/2/04 fix for emulator losing input after quickstart (not resetting next_sys_update point)
		}
	}
	beeb_paused_=false;
}

//////////////////////////////////////////////////////////////////////////
#ifdef ENABLE_TEST
void mbMainFrame::OnTest(wxCommandEvent &) {
//	static const char *name="Z:\\beeb\\beebgames\\tapes\\Frak_B\\Frak_B.uef";
//	std::vector<t65::byte> contents;
//	if(mbLoadFile(name,contents)) {
//		bbcUef test(contents);
//	}
	mbProfilesDialog dlg(this,-1,cfg_.profiles);

	if(dlg.ShowModal()==wxID_OK) {
		//dlg.GetResult(&cfg_.profiles);
	}
}
#endif
	
//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnKeymapDialog(wxCommandEvent &) {
	beeb_paused_=true;
	this->ModalBackground();
	std::string old_selected_keymap=cfg_.keyboard.selected_keymap;
	mbKeyboardConfigDialog dlg(this,cfg_.keyboard);
	dlg.ShowModal();
	dlg.GetResult(&cfg_.keyboard);
	this->SetKeymap(old_selected_keymap);
	this->RefreshWindow();
	beeb_paused_=false;
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnSoundDialog(wxCommandEvent &) {
	beeb_paused_=true;
	this->ModalBackground();
	mbSoundConfigDialog snd_dlg(this,cfg_.sound);
	if(snd_dlg.ShowModal()==wxID_OK) {
		snd_dlg.GetResult(&cfg_.sound);
		bool old_enabled=cfg_.sound.enabled;
		this->SetSoundEnabled(false);
		this->SetSoundEnabled(old_enabled);
		this->RefreshWindow();
	}
	beeb_paused_=false;
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnRomsDialog(wxCommandEvent &) {
	BASSERT(model_);
	BASSERT(disc_interface_);
	beeb_paused_=true;
	this->ModalBackground();

	mbRomsConfigDialog roms_dlg(this,-1,cfg_.roms);
	
	int result=roms_dlg.ShowModal();
	if(result==wxID_OK||result==wxID_APPLY) {
		roms_dlg.GetResult(&cfg_.roms);
	}

	if(result==wxID_APPLY) {
		this->ApplyConfig();
	}

	beeb_paused_=false;
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnKeymap(wxCommandEvent &event) {
	int id=event.GetId();
	wxMenuItem *keymap=menu_bar_->FindItem(id);
	if(keymap) {
		this->SetKeymap(keymap->GetItemLabel().ToStdString());
		this->RefreshWindow();
	}
}

//////////////////////////////////////////////////////////////////////////
#if (defined bbcDEBUG_TRACE_EVENTS)
void mbMainFrame::ResetDisassembly() {
	bbcComputer::event_record.Clear();
	bbcComputer::trace_cpu_events=false;
}

void mbMainFrame::OnStartDisassembly(wxCommandEvent &) {
	this->ResetDisassembly();
	bbcComputer::trace_cpu_events=true;
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnRebootWithDisassembly(wxCommandEvent &event) {
	this->ResetDisassembly();
	this->OnStartDisassembly(event);
	this->PowerOnBeeb();
}

//////////////////////////////////////////////////////////////////////////
static bool ProgressIncProgressDialog(const char *msg,int percent_done,unsigned amount_written,void *context) {
	wxProgressDialog *dlg=static_cast<wxProgressDialog *>(context);

	int mb=int(100.f/percent_done*amount_written)/1048576;

	wxString caption;
	if(msg) {
		caption=msg;
		caption+="\n";
	}
	caption+=wxString::Format("Estimated output size: %dMB",mb);

	dlg->Update(percent_done,caption);
	return true;
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnStopDisassembly(wxCommandEvent &) {
	//anything to save?
	if(!bbcComputer::trace_cpu_events) {
		return;
	}

	wxString name=wxFileSelector("Save disassembly to file",wxEmptyString,wxEmptyString,wxEmptyString,
		wxFileSelectorDefaultWildcardStr,wxFD_SAVE|wxFD_OVERWRITE_PROMPT,this);
	if(!name.empty()) {
		wxProgressDialog progress("Please wait...","Saving disassembly.",100,this,
			wxPD_AUTO_HIDE|wxPD_APP_MODAL|wxPD_ESTIMATED_TIME|wxPD_ELAPSED_TIME);
		bool b=bbcComputer::event_record.SaveEventsText(name.c_str(),&ProgressIncProgressDialog,&progress);
		if(!b) {
			wxLogMessage("Save was cancelled.");
		}
	}

	wxProgressDialog progress("Please wait...","Freeing memory...",-1,this);
	this->ResetDisassembly();
}
#endif//bbcDEBUG_TRACE_EVENTS

////////////////////////////////////////////////////////////////////////
//
#ifdef BEEB_DEBUG_SAVE_CHALLENGER_RAM
//8/12/2004 -- currently broken as bbc1770OpusChallenger::ram_ isn't std::vector any more
void mbMainFrame::OnSaveChallengerRam(wxCommandEvent &) {
	wxString name=wxFileSelector("Save Challenger RAM to file",0,0,0,
		wxFileSelectorDefaultWildcardStr,wxFD_SAVE|wxFD_OVERWRITE_PROMPT,this);
	if(!name.empty()) {
		if(!mbSaveFile(name,bbc1770OpusChallenger::ram_)) {
			wxLogError("Failed to save \"%s\"",name.c_str());
		}
	}
}
#endif

//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::OnResolution(wxCommandEvent &event) {
	this->SetBeebScreenSize(this->ResolutionFromId(event.GetId()));
}

//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::SetBeebScreenSize(const wxSize &screen_size) {
	if(hostgfx_&&HostGfx_IsFullScreen(hostgfx_)) {
		return;
	}
	wxSize deltas=this->GetSize()-beeb_screen_->GetSize();
	wxASSERT(deltas.GetWidth()>=0&&deltas.GetHeight()>=0);
	wxSize size=deltas+screen_size;
	wxLogDebug("mbMainFrame::OnResolution: beeb screen=%dx%d",
		screen_size.GetWidth(),screen_size.GetHeight());
	wxLogDebug("mbMainFrame::OnResolution: new window=%dx%d",
		size.GetWidth(),size.GetHeight());
	this->SetSize(size);
}

//////////////////////////////////////////////////////////////////////////
wxSize mbMainFrame::ResolutionFromId(int id) {
	wxASSERT(id>=id_resolution_0&&id<=id_resolution_3);
	id-=id_resolution_0;
	wxSize s(cfg_.video.width,cfg_.video.height);
	if(!(id&2)) {
		s.x/=2;
	}
	if(id&1) {
		s.y*=2;
	}
	return s;
}

//////////////////////////////////////////////////////////////////////////
//
static wxString WithImageExtension(const wxString &str) {
	wxFileName fname(str);

	if(!fname.IsOk()) {
		return str;//hmm.
	} else {
		const wxList &image_handlers_list=wxImage::GetHandlers();
		wxList::Node *node;
		for(node=image_handlers_list.GetFirst();node;node=node->GetNext()) {
			wxImageHandler *image_handler=static_cast<wxImageHandler *>(node->GetData());
			if(fname.GetExt().CmpNoCase(image_handler->GetExtension())==0) {
				break;
			}
		}
		if(node) {
			// already has a valid extension.
			return str;
		} else {
			// extension is missing/invalid.
			return str+".bmp";
		}
	}
}

void mbMainFrame::OnScreenshotSaveAs(wxCommandEvent &) {
	screenshot_countdown_=0;
	wxString wildcards;
	wildcards+="Auto (*.*)|*.*";
	wxFileDialog fd(this,"Select file","","",wildcards,wxFD_SAVE);
	int r=fd.ShowModal();
	if(r==wxID_CANCEL) {
		return;
	}
	screenshot_countdown_=2;

	screenshot_name_=WithImageExtension(fd.GetPath());
}

//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::DoScreenshot() {
	bool error=true;

	if(last_video_buffer_) {
		wxImage *image=new wxImage(last_video_buffer_->w,last_video_buffer_->h);
		//const t65::byte *src=last_video_buffer_->data;
		for(int y=0;y<last_video_buffer_->h;++y) {
			for(int x=0;x<last_video_buffer_->w;++x) {
				unsigned char r,g,b;

				HostGfxBuffer_GetRgb(last_video_buffer_,x,y,&r,&g,&b);
//				unsigned char r=*src&1?255:0;
//				unsigned char g=*src&2?255:0;
//				unsigned char b=*src&4?255:0;
//				++src;
				image->SetRGB(x,y,r,g,b);
			}
		}
		error=!image->SaveFile(screenshot_name_);
		delete image;
	}

	if(error) {
		wxLogError("Failed to save image\n\"%s\"",screenshot_name_.c_str());
	}
}
//	wxImage *image=new wxImage(bbcVideo::buffer_width,bbcVideo::buffer_height);
//	const t65::byte *src=bbcVideo::buffer;
//	for(int y=0;y<bbcVideo::buffer_height;++y) {
//		for(int x=0;x<bbcVideo::buffer_width;++x) {
//			unsigned char r=*src&1?255:0;
//			unsigned char g=*src&2?255:0;
//			unsigned char b=*src&4?255:0;
//			++src;
//			image->SetRGB(x,y,r,g,b);
//		}
//	}
//	if(!image->SaveFile(screenshot_name_)) {
//		wxLogError("Failed to save image\n\"%s\"\n",screenshot_name_.c_str());
//	}
//	delete image;
//}

//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::OnToggleStatusBar(wxCommandEvent &) {
	cfg_.misc.show_status_bar=!cfg_.misc.show_status_bar;
	this->RefreshWindow();
}

//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::OnToggleFastForwardDiscAccess(wxCommandEvent &) {
	cfg_.misc.fast_forward_disc_access=!cfg_.misc.fast_forward_disc_access;
	this->RefreshWindow();
}

//////////////////////////////////////////////////////////////////////////
//
#ifdef bbcDEBUG_PANELS
static mbDebugPanel *ViaPanelInit(wxFrame *frame,long id,const char *t,bbcVIA *via) {
	mbDebugPanel *panel=static_cast<mbDebugPanel *>(frame->FindWindow(id));
	if(panel) {
		panel->Destroy();
		panel=0;
	} else {
		panel=new mbDebugPanel(frame,id,t);
		via->InitDebugPanel(panel->Panel());
	}
	return panel;
}

void mbMainFrame::OnToggleDbgpanelSysvia(wxCommandEvent &) {
	ViaPanelInit(this,id_dbgpanel_sysvia,"System VIA",&bbc_system_via);
}

void mbMainFrame::OnToggleDbgpanelUsrvia(wxCommandEvent &) {
	ViaPanelInit(this,id_dbgpanel_usrvia,"User VIA",&bbc_user_via);
}

void mbMainFrame::OnToggleDbgpanelVideo(wxCommandEvent &) {
	mbDebugPanel *panel=
		static_cast<mbDebugPanel *>(this->FindWindow(id_dbgpanel_video));

	if(panel) {
		panel->Destroy();
		panel=0;
	} else {
		panel=new mbDebugPanel(this,id_dbgpanel_video,"Video System");
		panel->Panel()->SetSize(52,16);
	}
}
#endif

#ifdef bbcDEBUG_VIDEO
void mbMainFrame::OnToggleShowAll(wxCommandEvent &) {
	bbcVideo::SetShowAll(!bbcVideo::show_all_);
	this->RefreshWindow();
}

void mbMainFrame::OnLogVideoFrames(wxCommandEvent &) {
//#ifdef bbcDEBUG_TRACE
//	bool with_disassembly=event.GetId()==id_log_video_frames_disasm;
//#else
	bool with_disassembly=false;
	wxString caption="Log frames";
	if(with_disassembly) {
		caption+=" w/ disasm";
	}
	wxString name=wxFileSelector(caption,wxEmptyString,wxEmptyString,wxEmptyString,"Text file (*.txt)|*.txt|",wxFD_SAVE|wxFD_OVERWRITE_PROMPT,
		this);
	if(!name.empty()) {
		bbcVideo::LogFrames(name.c_str(),5,with_disassembly);
	}
}

void mbMainFrame::OnToggleShowT1Timeouts(wxCommandEvent &) {
	bbcVideo::show_t1_timeouts_=!bbcVideo::show_t1_timeouts_;
}
#endif

void mbMainFrame::OnSoundRecordStart(wxCommandEvent &) {
	bbcSound::StartRecording();
	this->RefreshWindow();
}

void mbMainFrame::OnSoundRecordStop(wxCommandEvent &) {
	if(!bbcSound::IsRecording()) {
		return;
	}
	bbcSound::StopRecording();
	wxString name=wxFileSelector("Save VGM file",wxEmptyString,wxEmptyString,wxEmptyString,"Video Game Music file (*.vgm)|*.vgm|",
		wxFD_SAVE|wxFD_OVERWRITE_PROMPT,this);
	if(!name.empty()) {
		std::vector<t65::byte> binary;
		bbcSound::GetRecordedVgmData(&binary);
		if(!mbSaveFile(name,binary)) {
			wxLogError("Failed to save sound file \"%s\".",name.c_str());
		}
	}
	bbcSound::ClearRecording();
}

//////////////////////////////////////////////////////////////////////////
// Hard reset -- power on type thing.
void mbMainFrame::OnHardReset(wxCommandEvent &) {
	int result=wxMessageBox("This will totally reset the computer\n\nAre you sure?",
		"Hard reset",wxYES_NO|wxICON_QUESTION,this);
	if(result==wxYES) {
		this->PowerOnBeeb();
#ifdef bbcHACKING_URIDIUM
		if(cl_autoboot_) {
			bbcKeyboardMatrix::SetForceShiftPressed(true);
		}
#endif
	}
}

//////////////////////////////////////////////////////////////////////////
void mbMainFrame::OnSetScreenType(wxCommandEvent &event) {
	wxWindowID id=event.GetId();

	if(id==id_screen_type_colour) {
		cfg_.video.monitor_type=mbMONITOR_COLOUR;
	} else if(id==id_screen_type_bnw) {
		cfg_.video.monitor_type=mbMONITOR_BNW;
	} else if(id==id_screen_type_amber) {
		cfg_.video.monitor_type=mbMONITOR_AMBER;
	} else if(id==id_screen_type_green) {
		cfg_.video.monitor_type=mbMONITOR_GREEN;
	}

	this->RefreshWindow();
}

//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::OnVolume(wxCommandEvent &event) {
	//printf("mbMainFrame::OnVolume: volume=%ld\n",event.m_extraLong);
	wxASSERT(event.GetExtraLong()>=0&&event.GetExtraLong()<=100);
	cfg_.sound.volume=event.GetExtraLong();
	HostSnd_SetVolume(hostsnd_,cfg_.sound.volume);
}

//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::OnDropFile(wxCommandEvent &event) {
	wxString command_string=event.GetString();
	this->UiLoadDisc(event.GetInt(),wxFileName(command_string));
}

//////////////////////////////////////////////////////////////////////////
//
/*
void mbMainFrame::OnToggleVertical2x(wxCommandEvent &) {
	cfg_.video.vertical_2x=!cfg_.video.vertical_2x;
	this->RefreshWindow();
}
*/
//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::OnActivate(wxActivateEvent &event) {
#ifdef __WXMSW__
	if(!event.GetActive()) {
		HWND cur=GetForegroundWindow();
		HWND window=0;

		while(cur) {
//			char text[2000];
//
//			GetWindowText(cur,text,sizeof text);
//			wxLogDebug("cur=%08X text=\"%s\"\n",cur,text);
			window=cur;
			cur=::GetParent(cur);
		}

		if(window!=HWND(this->GetHandle())) {
			cfg_.video.full_screen=false;
		}
//		wxWindow *cur=wxGetActiveWindow();
//		wxWindow *window=0;
//
//		while(cur) {
//			wxLogDebug(__FUNCTION__ ": cur title=%s\n",cur->GetT)
//			window=cur;
//			cur=cur->GetParent();
//		}
//
//		if(window!=this) {
//			cfg_.video.full_screen=false;
//		}
	}

	wxLogDebug("%s: event.GetActive()=%d; cfg_.video.full_screen=%d",__FUNCTION__,event.GetActive(),cfg_.video.full_screen);

#else
	//TODO for now this bit of code is a bit dumb.
	cfg_.video.full_screen=event.GetActive()&&cfg_.video.full_screen;
#endif
	this->RefreshWindow();
}

//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::OnToggleFullScreen(wxCommandEvent &) {
	cfg_.video.full_screen=!cfg_.video.full_screen;
	this->RefreshWindow();
}

//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::OnScanlines(wxCommandEvent &event) {
	wxWindowID id=event.GetId();

	if(id==id_windowed_scanlines_single) {
		cfg_.video.windowed_scanlines=mbSCANLINES_SINGLE;
	} else if(id==id_windowed_scanlines_interlace) {
		cfg_.video.windowed_scanlines=mbSCANLINES_INTERLACE;
	} else if(id==id_windowed_scanlines_double) {
		cfg_.video.windowed_scanlines=mbSCANLINES_DOUBLE;
	} else if(id==id_full_screen_scanlines_single) {
		cfg_.video.full_screen_scanlines=mbSCANLINES_SINGLE;
	} else if(id==id_full_screen_scanlines_interlace) {
		cfg_.video.full_screen_scanlines=mbSCANLINES_INTERLACE;
	} else if(id==id_full_screen_scanlines_double) {
		cfg_.video.full_screen_scanlines=mbSCANLINES_DOUBLE;
	}
	this->RefreshWindow();
}

//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::OnJoysticksDialog(wxCommandEvent &) {
	beeb_paused_=true;
	this->ModalBackground();
//	wxString old_selected_keymap=cfg_.keyboard.selected_keymap;
//	mbKeyboardConfigDialog dlg(this,cfg_.keyboard);
//	dlg.ShowModal();
//	dlg.GetResult(&cfg_.keyboard);
//	this->SetKeymap(old_selected_keymap);

	mbJoysticksConfigDialog dlg(this,cfg_.joysticks);
	if(!dlg.IsOk()) {
		wxLogError("Failed to initialise joystick configuration dialog");
	} else {
		if(dlg.ShowModal()==wxID_OK) {
			dlg.GetResult(&cfg_.joysticks);
		}
	}

	this->RefreshWindow();
	beeb_paused_=false;
}

//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::OnRightUp(wxMouseEvent &event) {
//	if(hostgfx_&&HostGfx_IsFullScreen(hostgfx_)) {
	this->PopupMenu(&popup_menu_,event.GetPosition());
//	}
}

//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::OnHardwareDialog(wxCommandEvent &) {
	beeb_paused_=true;
	this->ModalBackground();
	
	mbHardwareDialog dlg(this,cfg_.misc,cfg_.roms);
	if(dlg.ShowModal()==wxID_OK) {
		dlg.GetResult(&cfg_.misc,&cfg_.roms);
		this->ApplyConfig();
	}
	
	this->RefreshWindow();
	beeb_paused_=false;
}

#ifdef bbcDEBUG_PANELS
//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::OnToggleDbgpanelAdc(wxCommandEvent &) {
	mbDebugPanel *panel=static_cast<mbDebugPanel *>(this->FindWindow(id_dbgpanel_adc));
	if(panel) {
		panel->Destroy();
		panel=0;
	} else {
		panel=new mbDebugPanel(this,id_dbgpanel_adc,"ADC");
		bbcAdc::InitDebugPanel(panel->Panel());
	}
}
#endif

//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::PresentLastVideoBuffer(mbScanlinesMode scanlines_mode,int present_hints,int frame_number,
	uint64_t *convert_time,uint64_t *present_time)
{
	int line_height=1;
	int gap_height=0;
	int y_first=0;

	if(hostgfx_&&last_video_buffer_) {
//		HostGfxBufferFormat fmt;
//
//		fmt.flags.is_packed=0;
//		fmt.px[0].fixed_bits=cfg_.video.full_screen?HostGfx_FULL_SCREEN_PALETTE_BASE:0;
//		for(int i=0;i<3;++i) {
//			fmt.px[0].bits[i]=1<<i;
//		}
//
		bbcVideo::SetBufferDetails(cfg_.video.width,cfg_.video.height,HostGfx_BestBufferFormat(hostgfx_));

		switch(scanlines_mode) {
		case mbSCANLINES_SINGLE:
			gap_height=HostGfx_IsFullScreen(hostgfx_)?1:0;
			break;

		case mbSCANLINES_INTERLACE:
			y_first=frame_number&1;
			gap_height=1;
			break;

		case mbSCANLINES_DOUBLE:
			line_height=2;
			break;
		}

		HostGfx_Present(hostgfx_,last_video_buffer_,y_first,line_height,gap_height,present_hints,convert_time,present_time);
#ifdef VERBOSITY
		wxLogDebug("HostGfx_Present called.");
#endif

//		HostGfx_Present(hostgfx_,cfg_.video.video_effect,last_video_buffer_->data,last_video_buffer_->width,last_video_buffer_->height,
//			last_video_buffer_->pitch,y_first,line_height,gap_height,present_hints);
	}
}

//////////////////////////////////////////////////////////////////////////
//
#ifdef mbVISUAL_PROFILER

static t65::dword GetColourValue(const HostGfxBuffer *buffer,int colour) {
	t65::dword value=0;
	if(colour&1) {
		value|=buffer->fmt.elems[0].mask;
	}
	if(colour&2) {
		value|=buffer->fmt.elems[1].mask;
	}
	if(colour&4) {
		value|=buffer->fmt.elems[2].mask;
	}
	value|=buffer->fmt.fixed_bits;

	return value;
}

int mbMainFrame::DrawProfilerText(const HostGfxBuffer *buffer,int strx,int stry,int colour,const char *str) {
	if(buffer->fmt.bypp!=1) {
		// erm... to do.
		return strx;
	}

	int x=strx;
	t65::dword value=GetColourValue(buffer,colour);
	t65::dword colour_mask=value|(value<<8)|(value<<16)|(value<<24);

	for(const char *c=str;*c;++c) {
		if(*c<32||*c>126) {
			continue;
		}

		if(x+6>=buffer->w) {
			break;
		}

		//const t65::byte *cbmp=&bbcTeletextFont::normal_chars_src[(*c-32)*60];
		const t65::qword *cbmp=profiler_font_data_[*c-32];
		t65::byte *dest=buffer->bitmap+x+stry*buffer->pitch;
		for(int cy=0;cy<10;++cy) {
			if(stry+cy>=buffer->h) {
				break;
			}

			*reinterpret_cast<t65::dword *>(dest)=*reinterpret_cast<const t65::dword *>(cbmp)&colour_mask;
			*reinterpret_cast<t65::word *>(dest+4)=*(reinterpret_cast<const t65::word *>(cbmp)+2)&colour_mask;
			++cbmp;

//			for(int cx=0;cx<6;++cx) {
//				if(*cbmp++) {
//					dest[cx]=7;
//				} else {
//					dest[cx]=4;
//				}
//			}

			dest+=buffer->pitch;
		}

		x+=6;
	}

	return x;
}

bbcProfileRecord *mbMainFrame::DrawProfileRecord(const HostGfxBuffer *buffer,int x,int *y,bbcProfileRecord *pr,
	float container_tsc,int colour,const wxPoint *pt)
{
	char tmp[100];
	_snprintf(tmp,sizeof tmp,"%s: %05.1f%% %d",pr->name,pr->total/container_tsc*100.f,pr->count);
	int end_x=this->DrawProfilerText(buffer,x,*y,colour,tmp);

	bbcProfileRecord *result;
	if(pt&&pt->x>=x&&pt->x<end_x&&pt->y>=*y&&pt->y<=*y+10) {
		result=pr;
	} else {
		result=0;
	}

	*y+=10;

	if(bbcProfileRecord *child=pr->FirstChild()) {
		uint64_t total=0;
		int colour=0;
		while(child) {
			if(bbcProfileRecord *p=this->DrawProfileRecord(buffer,x+10,y,child,float(pr->total),1+colour,pt)) {
				result=p;
			}
			total+=child->total;

			child=child->NextSibling();

			++colour;
			colour%=6;
		}

		int64_t remainder=pr->total-total;
		if(remainder<0) {
			remainder=0;
		}
		_snprintf(tmp,sizeof tmp,"(remainder %05.1f%%)",remainder/container_tsc*100.f);
		this->DrawProfilerText(buffer,x+10,*y,7,tmp);
		*y+=10;
	}

	return result;
}

void mbMainFrame::MakeProfilerFontData() {
	for(int i=0;i<96;++i) {
		for(int j=0;j<10;++j) {
			profiler_font_data_[i][j]=0;
			t65::byte *cbmp=reinterpret_cast<t65::byte *>(&profiler_font_data_[i][j]);
			for(int k=0;k<6;++k) {
				t65::byte v;
				if(bbcTeletextFont::normal_chars_src[i*60+j*6+k]) {
					v=7;
				} else {
					v=0;
				}
				cbmp[k]=v;
			}
		}
	}
}

static const int vp_bar_x=320;
static const int vp_bar_y=10;
static const int vp_bar_height=10;
static const int vp_gap_height=10;
static const int vp_bar_width=300;
static const int vp_line_height=vp_bar_height+vp_gap_height;

// draw bar for the children of pr. (x,y) is top left of bar. buffer is target buffer.
// fill in *leftx and *rightx with left and right X coordinates of section of bar that corresponds to query, which must
// be one of the children of pr.
void mbMainFrame::DrawVisualProfileBar(const HostGfxBuffer *buffer,int y,const bbcProfileRecord *pr,const bbcProfileRecord *query,
	int *leftx,int *rightx)
{
	wxASSERT(!query||(leftx&&rightx));

	uint64_t total=0;
	for(bbcProfileRecord *child=pr->FirstChild();child;child=child->NextSibling()) {
		total+=child->total;
	}

	if(total>0) {
		int colour=0;
		int x=vp_bar_x;
		for(bbcProfileRecord *child=pr->FirstChild();child;child=child->NextSibling()) {
			int w=int(child->total/float(total)*vp_bar_width);
			unsigned char *dest=buffer->bitmap+x+y*buffer->pitch;

			if(buffer->fmt.bypp!=1) {
				// erm...
			} else {
				for(int line=0;line<vp_bar_height;++line) {
					memset(dest,GetColourValue(buffer,1+colour),w);
					dest+=buffer->pitch;
				}
			}

			x+=w;

			++colour;
			colour%=6;
		}
	}
}

void mbMainFrame::DrawVisualProfile(const HostGfxBuffer *buffer,bbcProfileRecord *profile_record) {
	this->DrawVisualProfileBar(buffer,vp_bar_y,profile_record,0,0,0);
//
//	int depth=1;
//	bbcProfileRecord *root;
//	for(bbcProfileRecord *cur=profile_record->parent;cur;cur=cur->parent) {
//		++depth;
//	}
//
//	int bottom_y=area.GetTop()+depth*vp_line_height;
//	if(bottom_y>area.GetBottom()) {
//		bottom_y=area.GetBottom();
//	}
//
//	bbcProfileRecord *prev=0;
//	int y=bottom_y;
//	for(bbcProfileRecord *cur=profile_record;cur;cur=cur->parent) {
//		int leftx,rightx;
//		this->DrawVisualProfileBar(buffer,area.GetLeft(),y-vp_line_height,cur,prev,&leftx,&rightx);
//	}
}

#endif//mbVISUAL_PROFILER

void mbMainFrame::DoBeebTick() {
	this->SetAutoPaint(false);
	BPR(bpr_do_beeb_tick);
	for(;;) {
#ifdef bbcTEST_SAVE_STATE
		switch(tss_state) {
		case TSSS_NOT_STARTED:
			{
				tss_start_cycles_=bbcComputer::cpucycles;

				bbcSaveState tmp;
				bbcSaveState::SaveStatus status=this->SaveSaveState(&tmp);
				BASSERT(status==bbcSaveState::SS_OK);
				tmp.Save(&tss_start_data_);

				tss_state=TSSS_STARTED;
			}
			break;

		case TSSS_STARTED:
			if(bbcComputer::cpucycles-tss_start_cycles_>tss_freq_cycles_) {
				tss_end_cycles_=bbcComputer::cpucycles;

				bbcSaveState tmp;

				bbcSaveState::SaveStatus sstatus=this->SaveSaveState(&tmp);
				BASSERT(sstatus==bbcSaveState::SS_OK);
				tmp.Save(&tss_end_data_);

				// reload, and try again.
				bbcSaveState::LoadStatus lstatus=this->LoadSaveState(&tmp);
				BASSERT(lstatus==bbcSaveState::LS_OK);

				tss_state=TSSS_RUNNING_AGAIN;
			}
			break;

		case TSSS_RUNNING_AGAIN:
			if(bbcComputer::cpucycles-tss_start_cycles_>tss_freq_cycles_) {
				BASSERT(bbcComputer::cpucycles==tss_end_cycles_);

				bbcSaveState tmp;
				bbcSaveState::SaveStatus status=this->SaveSaveState(&tmp);
				BASSERT(status==bbcSaveState::SS_OK);

				std::vector<t65::byte> again_end_data;
				tmp.Save(&again_end_data);

				BASSERT(tss_end_data_.size()==again_end_data.size());
				BASSERT(memcmp(&tss_end_data_[0],&again_end_data[0],tss_end_data_.size())==0);

				tss_state=TSSS_NOT_STARTED;
			}
			break;
		}
#endif

		{
			BPR(bpr_run_6502);
			bbcComputer::Run6502();
		}
		
		//This is to cater for overflow, which takes about 15 mins to happen at 2MHz.
		//No update will cause the cycles counter to jump by 2,000,000.
		bbcComputer::next_stop+=2000000;//=INT_MAX;
		
		{
			BPR(bpr_bbccomputer_update);
			bbcComputer::Update();
		}
		
		BPR(bpr_do_beeb_tick_rem);

		//Do this first, so it can change the screen buffer to indicate stuff
		if(int(bbcComputer::cpucycles-next_update_sound_)>0) {//bbcComputer::cpucycles>next_update_sound_) {
			this->DoUpdateSound();
			next_update_sound_=bbcComputer::cpucycles+update_sound_cycles;
		}

		const HostGfxBuffer *new_video_buffer=bbcVideo::DisplayBuffer();
		if(new_video_buffer!=last_video_buffer_) {
			last_video_buffer_=new_video_buffer;

			if(screenshot_countdown_==1) {
				this->DoScreenshot();
			}
			
			int freq=beeb_fastforward_?50:cfg_.video.frame_skip;

#ifdef mbVISUAL_PROFILER
			uint64_t now_tsc;
			bbcProfilerReadTsc(&now_tsc);
			if(cfg_.debug.show_visual_profile&&first_frame_drawn_yet_) {
				BPR(bpr_draw_visual_profile);
				wxPoint mouse;
				bool mouse_valid;
				{
					mouse=wxGetMousePosition();
					wxPoint topleft(0,0);
					beeb_screen_->ClientToScreen(&topleft.x,&topleft.y);

					mouse.x-=topleft.x;
					mouse.y-=topleft.y;

					wxSize client_size;
					beeb_screen_->GetClientSize(&client_size.x,&client_size.y);

					if(mouse.x<0||mouse.x>=client_size.x||mouse.y<0||mouse.y>=client_size.y) {
						mouse_valid=false;
					} else {
						mouse_valid=true;
						mouse.x=int(mouse.x/float(client_size.x)*new_video_buffer->w);
						mouse.y=int(mouse.y/float(client_size.y)*new_video_buffer->h);
					}
				}

				bbcProfileRecord **prs;
				int num_prs;
				bbcProfilerGetAllProfileRecords(&prs,&num_prs);

				int x=10;
				int y=10;
				float frame_time_tsc=now_tsc-last_frame_tsc_;
				uint64_t total_tsc=0;
				bbcProfileRecord *mouse_pr=0;
				int colour=0;
				for(bbcProfileRecord *pr=bbc_profile_records_root.FirstChild();pr;pr=pr->NextSibling()) {
					bbcProfileRecord *tmp=this->DrawProfileRecord(new_video_buffer,x,&y,pr,frame_time_tsc,1+colour,
						mouse_valid?&mouse:0);

					if(tmp) {
						mouse_pr=tmp;
					}

					total_tsc+=pr->total;

					++colour;
					colour%=6;
				}

				int64_t remainder_tsc=now_tsc-last_frame_tsc_-total_tsc;
				if(remainder_tsc<0) {
					remainder_tsc=0;
				}

//				_snprintf(tmp,sizeof tmp,"remainder %03.1f%%%",remainder_tsc/frame_time_tsc*100.f);
//				if(mouse_valid) {
//					_snprintf(tmp,sizeof tmp,"mx=%d my=%d",mouse.x,mouse.y);
//				} else {
//					strcpy(tmp,"mouse pos invalid");
//				}
#ifdef _DEBUG
				{
					char tmp[100];
					_snprintf(tmp,sizeof tmp,"%d",twGetTotalNumAllocations());
					this->DrawProfilerText(new_video_buffer,x,y,7,tmp);
					y+=10;
				}
#endif//_DEBUG
				if(mouse_pr) {
					this->DrawProfilerText(new_video_buffer,x,y,7,mouse_pr->name);
					this->DrawVisualProfile(new_video_buffer,mouse_pr);
				}

				//for(int i=0;i<num_prs;++i) {
					//prs[i]->count=0;
					//prs[i]->total=0;
				//}
			}
			first_frame_drawn_yet_=true;
			last_frame_tsc_=now_tsc;
#endif

			if(num_frames_%freq==0) {
				int present_hints=0;
				bool is_full_screen=!!HostGfx_IsFullScreen(hostgfx_);

				if(is_full_screen&&cfg_.video.full_screen_vsync) {
					present_hints|=HostGfx_PRESENT_HINT_SYNC;
				}

				mbScanlinesMode scanlines_mode=is_full_screen?cfg_.video.full_screen_scanlines:cfg_.video.windowed_scanlines;
				{
					BPR(bpr_hostgfx_present);
					wxClientDC dc(beeb_screen_);
					uint64_t present_time,convert_time;
					this->PresentLastVideoBuffer(scanlines_mode,present_hints,num_frames_/freq,&convert_time,&present_time);

#ifdef bbcENABLE_PROFILER
					//wxLogDebug("convert_time=%llu present_time=%llu",convert_time,present_time);
					++bpr_hostgfx_present_convert.count;
					++bpr_hostgfx_present_present.count;
					bpr_hostgfx_present_present.total+=present_time;
					bpr_hostgfx_present_convert.total+=convert_time;
#endif//bbcENABLE_PROFILER
				}

//				if(is_full_screen) {
//					bbcVideo::SetPixelFixedBits(HostGfx_FULL_SCREEN_PALETTE_BASE);
//				} else {
//					bbcVideo::SetPixelFixedBits(0);
//				}
//
//				bbcVideo::SetBufferDimensions(cfg_.video.width,cfg_.video.height);
			}
			//bbcVideo::buffer_dirty=false;

			if(screenshot_countdown_>0||num_frames_%cfg_.video.cleanup_frequency==0) {
				//memset(bbcVideo::buffer,bbcVideo::blank_pixel,bbcVideo::buffer_size);
				BPR(bpr_clear_video_buffer);
				bbcVideo::ClearBuffer();
			}
			if(screenshot_countdown_>0) {
				--screenshot_countdown_;
			}

			++num_frames_;
		}
		if(int(bbcComputer::cpucycles-next_update_input_)>0) {//bbcComputer::cpucycles>next_update_keys_) {
			BPR(bpr_update_input);
			this->DoUpdateInput();
			next_update_input_=bbcComputer::cpucycles+update_input_cycles;
			//			if(bbcKeyboardMatrix::KeyState(mbKEY_QUICKQUIT)) {
			//				break;
			//			}
		}
		if(int(bbcComputer::cpucycles-next_update_sys_)>0) {//bbcComputer::cpucycles>next_update_sys_) {
			break;
		}
#ifdef bbcHACKING_URIDIUM
		if(bbcComputer::trace&&bbcComputer::trace->size()>=5000000) {
			this->OnStopDisassembly(wxCommandEvent());
		}
#endif
	}
	this->SetAutoPaint(true);
}

//////////////////////////////////////////////////////////////////////////
//
bool mbMainFrame::ApplyConfig() {
	std::vector<wxByte> rom;
	
	wxLogDebug("ApplyConfig...");

	this->SetSoundEnabled(cfg_.sound.enabled);

	if(hostsnd_) {
		HostSnd_SetVolume(hostsnd_,cfg_.sound.volume);
	}
	
	wxLogDebug("ApplyConfig: Keymap: keymap \"%s\"",cfg_.keyboard.selected_keymap.c_str());
	this->SetKeymap(cfg_.keyboard.selected_keymap);
	
	//If there is no window size stored in the INI file, resize the BBC screen
	//to the size specified in the INI file.
	if(cfg_.misc.window_rect.GetWidth()<0) {
		wxLogDebug("ApplyConfig: no saved window rect: BBC screen is %dx%d",
			cfg_.video.width,cfg_.video.height);
		beeb_screen_->SetSize(cfg_.video.width,cfg_.video.height);
		this->GetSizer()->Layout();
	} else {
		wxLogDebug("ApplyConfig: saved window rect is @(%d,%d) +(%d,%d)",
			cfg_.misc.window_rect.GetLeft(),cfg_.misc.window_rect.GetTop(),
			cfg_.misc.window_rect.GetWidth(),cfg_.misc.window_rect.GetHeight());
		this->SetSize(cfg_.misc.window_rect);
	}
	this->Show();
	
	if(cfg_.misc.window_is_maximized) {
		wxLogDebug("ApplyConfig: Maximizing window");
		this->Maximize();
	}
	
	this->RefreshWindow();
	
	this->PowerOnBeeb();
	
	beeb_paused_=false;
	update_window_state_=true;
	
	wxLogDebug("ApplyConfig: done.");
	
	return true;
}

//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::PowerOnBeeb() {
	HostTmr_GetHfFrequency(&hf_freq_);
	
#if (defined bbcHACKING_URIDIUM)&&((defined bbcDEBUG_TRACE_EVENTS))
	this->OnStartDisassembly(wxCommandEvent());
#endif
	//Beeb setup
	{
		model_=mbModelFromShortName(cfg_.misc.model);
		if(!model_) {
			wxLogWarning("Unknown BBC model '%s'\nBBC model set to B",cfg_.misc.model.c_str());
			cfg_.misc.model="b";
			model_=mbModelFromShortName(cfg_.misc.model);
			wxASSERT(model_);
		}

		mbRomsConfig::RomSets::const_iterator it=cfg_.roms.rom_sets.find(model_->base_model->short_name);
		if(it==cfg_.roms.rom_sets.end()) {
			wxLogWarning("No ROMs set for %s.\nBBC may fail to start properly until they are configured!",
				model_->base_model->long_name);
		} else {
			const mbRomsConfig::BaseModelRomSet *rom_set=&it->second;

			for(int i=0;i<16;++i) {
				const mbRomsConfig::Rom *rom=&rom_set->roms[i];
				if(!rom->filename.empty()) {
					std::vector<t65::byte> file_contents;
					if(!mbLoadFile(rom->filename.c_str(),file_contents)) {
						wxLogWarning("Failed to load \"%s\" into slot %d.",rom->filename.c_str(),i);
					} else {
						bbcComputer::SetRomContents(i,&file_contents[0],file_contents.size());
					}
				}
				bool is_ram=(rom->ram||(model_->force_swram_mask&(1<<i)))&&(model_->base_model->b_rom_mask&(1<<i));
				bbcComputer::SetRomWriteable(i,is_ram);
			}

			if(rom_set->os.empty()) {
				wxLogWarning("No OS ROM set for %s.\nBBC may fail to start properly until it is configured!",
					model_->base_model->long_name);
			} else {
				std::vector<t65::byte> file_contents;
				if(!mbLoadFile(rom_set->os.c_str(),file_contents)) {
					wxLogWarning("Failed to load OS from \"%s\".",rom_set->os.c_str());
				} else {
					bbcComputer::SetOsRomContents(&file_contents[0],file_contents.size());
				}
			}

			disc_interface_=mbDiscInterfaceFromShortName(rom_set->disc_interface.c_str());
		}
//		if(!model_->LoadRoms(cfg_.roms)) {
//			wxLogWarning("Failed to load ROMs\nBBC will fail to start properly until they are configured!");
//		}
		
		bbcComputer::SetModel(model_->model);

		if(!disc_interface_) {
			disc_interface_=mbDiscInterfaceFromShortName(0);
		}
		
		bbcComputer::SetDiscInterface(disc_interface_->iface);
//		bbcVideo::SetBufferDimensions(cfg_.video.width,cfg_.video.height);
	}

	this->PowerOnBeebPostSetup();
}

// call this once model, roms and disc interface are set up -- it does any other req'd stuff
void mbMainFrame::PowerOnBeebPostSetup() {
	bbcSound::SetFrequency(cfg_.sound.hz);
	
	if(!(model_->model->flags&bbcMODEL_NO_KEYBOARD_LINKS)) {
		for(unsigned i=0;i<8;++i) {
			bbcKeyboardMatrix::ForceSetKeyState(bbcKEY_KEYLINKS+i,cfg_.keyboard.keylinks[i]);
		}
	}

	bbcComputer::ResetHard();

	num_frames_=0;

	HostTmr_ReadMilliseconds(&last_update_sys_time_);
	HostTmr_ReadHf(&last_update_sound_hf_time_);
	next_update_input_=bbcComputer::cpucycles;
	next_update_sys_=bbcComputer::cpucycles;
	next_update_sound_=bbcComputer::cpucycles;
}

//////////////////////////////////////////////////////////////////////////
//
#ifdef bbcDEBUG_ENABLE
void mbMainFrame::OnDisassembleRam(wxCommandEvent &) {
	wxString name=wxFileSelector("Save RAM disassembly",wxEmptyString,wxEmptyString,wxEmptyString,wxFileSelectorDefaultWildcardStr,
		wxFD_SAVE|wxFD_OVERWRITE_PROMPT,this);
	if(!name.empty()) {
		FILE *h=fopen(name.c_str(),"wt");
		if(!h) {
			wxLogError("Failed to save to \"%s\"",name.c_str());
		} else {
			bbcComputer::WriteRamDisassembly(h);
			fclose(h);
		}
	}
}
#endif//bbcDEBUG_ENABLE

void mbMainFrame::OnVideoDialog(wxCommandEvent &) {
	beeb_paused_=true;
	this->ModalBackground();

	mbVideoConfigDialog dlg(this,cfg_.video,hostgfx_);
	if(dlg.ShowModal()==wxID_OK) {
		dlg.GetResult(&cfg_.video);
	}

	this->RefreshWindow();
	beeb_paused_=false;
}

//////////////////////////////////////////////////////////////////////////
//
void mbMainFrame::RefreshCfgWindowState() {
	//If running full screen, don't save window size.
	//Also, don't update it if we haven't done an ApplyConfig yet -- otherwise,
	//the window size gets stuck at the arbitrary default as a size/move msg is
	//sent on window creation.
	if(!update_window_state_) {
		return;
	}

	//must check a few different things, because full screen doesn't happen straight away.
	//*sigh* i'm not sure whether all of these are actually required...
	//
	//************************************************************
	//* Note to self: Don't EVER do things that way again. EVER. *
	//************************************************************
	//
	if(cfg_.video.full_screen||(hostgfx_&&HostGfx_IsFullScreen(hostgfx_))||this->IsFullScreen()) {
		wxLogDebug("%s: Full screen.",__FUNCTION__);
	} else {
		if(!cfg_.video.full_screen) {
			if(this->IsIconized()) {
				//If the window is iconized, don't save its size.
				wxLogDebug("%s: Iconized.",__FUNCTION__);
			} else if(this->IsMaximized()) {
				wxLogDebug("%s: Maximized.",__FUNCTION__);
				//it is maximized, don't save its window rect
				cfg_.misc.window_is_maximized=true;
			} else {
				//not maximized, save window rect.
				cfg_.misc.window_is_maximized=false;
				cfg_.misc.window_rect=this->GetRect();

#ifdef VERBOSITY
				wxLogDebug("%s: Window @(%d,%d) +(%d,%d).",__FUNCTION__,
					cfg_.misc.window_rect.GetLeft(),cfg_.misc.window_rect.GetTop(),
					cfg_.misc.window_rect.GetWidth(),cfg_.misc.window_rect.GetHeight());
#endif
			}
		}
	}
}

#ifdef t65_COUNT_INSTRUCTION_FREQUENCIES
//////////////////////////////////////////////////////////////////////////
//
struct InstrFreq {
	t65::byte op;
	bbcInstrInfo instr_info;
};

//note > -- want highest first
static bool InstrFreqLessThanByCount(const InstrFreq &a,const InstrFreq &b) {
	return a.instr_info.execution_count>b.instr_info.execution_count;
}

void mbMainFrame::OnShowInstructionFrequencies(wxCommandEvent &event) {
	InstrFreq instrs[256];

	for(unsigned i=0;i<256;++i) {
		InstrFreq *instr=&instrs[i];
		instr->op=i;
		bbcComputer::Model()->GetInstrInfo(instr->op,&instr->instr_info);
	}

	if(event.GetId()==id_show_instruction_frequencies_sorted) {
		std::stable_sort(instrs,instrs+256,&InstrFreqLessThanByCount);
	}

	printf("Instruction frequencies:\n");

	//4 cols, 64 rows
	for(unsigned i=0;i<64;++i) {
		for(unsigned j=0;j<4;++j) {
			const InstrFreq *instr=&instrs[i+j*64];

			//0123456789012345678901234
			//XX: YYY? ZZZ: 2147483648
			printf("%02X: %-4s %s: %-10u ",instr->op,instr->instr_info.dii->mnemonic,
				instr->instr_info.dii->addr_mode_name,instr->instr_info.execution_count);
		}
		printf("\n");
	}
}
#endif

#ifdef bbcDEBUG_COUNT_MMIO_ACCESSES
//////////////////////////////////////////////////////////////////////////
//
struct MmioAccessCount {
	t65::byte offset;
	unsigned count;
};

//note > -- want highest first
static bool MmioAccessCountLessThanByCount(const MmioAccessCount &a,const MmioAccessCount &b) {
	return a.count>b.count;
}

static void ShowMmioPageCounts(const unsigned *counts_in,bool sorted,int num_columns) {
	MmioAccessCount counts[256];

	for(unsigned i=0;i<256;++i) {
		counts[i].offset=i;
		counts[i].count=counts_in[i];
	}

	if(sorted) {
		std::stable_sort(counts,counts+256,&MmioAccessCountLessThanByCount);
	}

	int num_rows=256/num_columns;
	for(int r=0;r<num_rows;++r) {
		printf(" ");
		for(int c=0;c<num_columns;++c) {
			const MmioAccessCount *count=&counts[r+c*num_rows];

			printf("%02X: %-10u ",count->offset,count->count);
		}
		printf("\n");
	}
}

void mbMainFrame::OnShowSheilaAccessCounts(wxCommandEvent &event) {
	printf("Reads:\n");
	ShowMmioPageCounts(&bbcComputer::mmio_reads_count_[0x200],event.GetId()==id_show_sheila_access_counts_by_count,8);
	printf("Writes:\n");
	ShowMmioPageCounts(&bbcComputer::mmio_writes_count_[0x200],event.GetId()==id_show_sheila_access_counts_by_count,8);
}
#endif

#ifdef bbcENABLE_SAVE_STATE

//////////////////////////////////////////////////////////////////////////

static const bbcSaveStateItemId mb_save_state_item_id("mb_state");
static const bbcSaveStateItemId mb_disc_file_name_save_state_item_id("discname");

struct mbSaveState1 {
	char model_short_name[mb_model_max_short_name_size];
	char disc_interface_short_name[mb_disc_interface_short_name_max_size];
};

bbcSaveState::LoadStatus mbMainFrame::LoadSaveState(const bbcSaveState *save_state) {
	bbcSaveState::LoadStatus status;

	if(const mbSaveState1 *data=save_state->FindItem<mbSaveState1>(mb_save_state_item_id,0,1,&status)) {
		model_=mbModelFromShortName(data->model_short_name);
		disc_interface_=mbDiscInterfaceFromShortName(data->disc_interface_short_name);

		if(!model_||!disc_interface_) {
			wxLogDebug("%s: model_=%p disc_interface_=%p. LS_BAD_DATA.",__FUNCTION__,model_,disc_interface_);
			return bbcSaveState::LS_BAD_DATA;
		}
	} else {
		return status;
	}

	const bbcSaveStateHandler *handlers=model_->model->GetSaveStateHandlers();
	if(!handlers) {
		wxLogDebug("%s: !handlers for %s.",__FUNCTION__,model_->short_name);
		return bbcSaveState::LS_BAD_DATA;
	}

	status=bbcSaveState::LS_NOT_IMPLEMENTED;
	for(const bbcSaveStateHandler *handler=handlers;handler->save_fn||handler->load_fn;++handler) {
		if(!handler->load_fn) {
			status=bbcSaveState::LS_NOT_IMPLEMENTED;
		} else {
			status=(*handler->load_fn)(save_state);
		}

		if(status!=bbcSaveState::LS_OK) {
			break;
		}
	}

//	// don't load the drive names until last of all.
//	// this is to prevent any cocokups!
//		for(int i=0;i<bbcFdd::num_drives;++i) {
//		const bbcSaveStateItem *item=save_state->GetItem(mb_disc_file_name_save_state_item_id,i);
//		if(item) {
//			if(item->version!=1) {
//				return bbcSaveState::LS_BAD_VERSION;
//			}
//
//			const char *disc_name=reinterpret_cast<const char *>(&item->data[0]);
//
//		}
//		bbcFdd::DriveStatusType status=bbcFdd::DriveStatus(i);
//		if(status==bbcFdd::DRIVE_LOADED||status==bbcFdd::DRIVE_CHANGED) {
//			wxString disc_name=drive_uis_[i].disc_image.GetFullPath();
//			save_state->SetItem(mb_disc_file_name_save_state_item_id,i,1,disc_name.c_str(),disc_name.length()+1);//incl. '\x0'
//		}
//	}
//
	// tmp hack.
	for(int i=0;i<bbcFdd::num_drives;++i) {
		drive_uis_[i].no_save=true;
	}

	this->PowerOnBeebPostSetup();
	return status;
}

bbcSaveState::SaveStatus mbMainFrame::SaveSaveState(bbcSaveState *save_state) {
	bbcSaveState::SaveStatus status;

	// mbMainFrame bits and bats

	// disc interface and model
	{
		mbSaveState1 data;
		strncpy(data.model_short_name,model_->short_name,mb_model_max_short_name_size);
		strncpy(data.disc_interface_short_name,disc_interface_->short_name,mb_disc_interface_short_name_max_size);
		save_state->SetItem(mb_save_state_item_id,0,1,&data);
	}

	// names of discs
	for(int i=0;i<bbcFdd::num_drives;++i) {
		bbcFdd::DriveStatusType status=bbcFdd::DriveStatus(i);
		if(status==bbcFdd::DRIVE_LOADED||status==bbcFdd::DRIVE_CHANGED) {
			wxString disc_name=drive_uis_[i].disc_image.GetFullPath();
			save_state->SetItem(mb_disc_file_name_save_state_item_id,i,1,disc_name.c_str(),disc_name.length()+1);//incl. '\x0'
		}
	}

	// Model-specific
	const bbcSaveStateHandler *handlers=bbcComputer::Model()->GetSaveStateHandlers();
	if(!handlers) {
		status=bbcSaveState::SS_NOT_IMPLEMENTED;
	} else {
		status=bbcSaveState::SS_OK;

		for(const bbcSaveStateHandler *handler=handlers;handler->save_fn||handler->load_fn;++handler) {
			if(!handler->save_fn) {
				status=bbcSaveState::SS_NOT_IMPLEMENTED;
			} else {
				status=(*handler->save_fn)(save_state);
			}

			if(status!=bbcSaveState::SS_OK) {
				break;
			}
		}
	}

	return status;
}

static const char *load_state_files_specs="State files (*.mbss;*.mbss.gz)|*.mbss;*.mbss.gz";
static const char *save_state_files_specs=
	"Compressed state files (*.mbss.gz)|*.mbss.gz|"//<--NB
	"Uncompressed state files (*.mbss)|*.mbss";

void mbMainFrame::OnLoadState(wxCommandEvent &) {
	mbFileDlg fd(cfg_.file_dlgs["LoadState"],this,"Load state",load_state_files_specs,wxFD_OPEN);
	int r=fd.ShowModal();
	if(r==wxID_CANCEL) {
		return;
	}

	mbLoadFileFn load_fn;
	if(wxFileName(fd.GetPath()).GetExt().Lower()=="gz")
		load_fn=&mbLoadCompressedFile;
	else
		load_fn=&mbLoadFile;

	std::vector<wxByte> save_state_contents;
	if(!(*load_fn)(fd.GetPath(),save_state_contents)) {
		wxLogError("Failed to load save state file \"%s\"",fd.GetPath().c_str());
		return;
	}

	bbcSaveState save_state;
	if(!save_state.Load(save_state_contents)) {
		wxLogError("Bad save state file \"%s\"",fd.GetPath().c_str());
		return;
	}

	bbcSaveState::LoadStatus status=this->LoadSaveState(&save_state);
	if(status!=bbcSaveState::LS_OK) {
		wxLogError("Error loading from \"%s\": %s",fd.GetPath().c_str(),bbcSaveState::GetLoadStatusText(status));

		//can't do anything useful here.
		//TODO: should save state, then reload on error -- but must set the guarantee that 
		//loading will always load a save state saved in the same session, or something.
		this->PowerOnBeeb();
	}
}

void mbMainFrame::OnSaveState(wxCommandEvent &) {
	mbFileDlg fd(cfg_.file_dlgs["SaveState"],this,"Save state",save_state_files_specs,
		wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
	int r=fd.ShowModal();
	if(r==wxID_CANCEL) {
		return;
	}

	//save packed/unpacked?
	mbSaveFileFn save_fn;
	if(fd.GetFilterIndex()==0)
		save_fn=&mbSaveCompressedFile;
	else
		save_fn=&mbSaveFile;

	// make data
	bbcSaveState save_state;
	bbcSaveState::SaveStatus status=this->SaveSaveState(&save_state);
	if(status!=bbcSaveState::SS_OK) {
		wxLogError("Error saving state: %s",bbcSaveState::GetSaveStatusText(status));
		return;
	}

	// save it
	std::vector<t65::byte> save_state_contents;
	save_state.Save(&save_state_contents);
	if(!(*save_fn)(fd.GetPath(),save_state_contents)) {
		wxLogError("Failed to save state to \"%s\"",fd.GetPath().c_str());
	}
}
#endif

//////////////////////////////////////////////////////////////////////////
//
#ifdef bbcENABLE_PROFILER
static void wxLogDebugProfilerPrint(void *,const char *str) {
	printf("%s",str);
#ifdef _WIN32
	OutputDebugStringA(str);
#endif
}

void mbMainFrame::OnDisplayProfilerResults(wxCommandEvent &) {
	bbcProfilerPrintRecords(&wxLogDebugProfilerPrint,0);
}
#endif

#ifdef mbVISUAL_PROFILER
void mbMainFrame::OnShowVisualProfile(wxCommandEvent &) {
	cfg_.debug.show_visual_profile=!cfg_.debug.show_visual_profile;
	this->RefreshWindow();
}
#endif

//////////////////////////////////////////////////////////////////////////
//
#ifdef bbcENABLE_BEEB_ICE
void mbMainFrame::OnIceWindowToggle(wxCommandEvent &) {
	ice_cfg_.enabled=!ice_cfg_.enabled;
	wxLogDebug("ice_cfg_.enabled=%d ice_frame_=%p",ice_cfg_.enabled,ice_frame_);
	this->RefreshWindow();
}
#endif//bbcENABLE_BEEB_ICE

//////////////////////////////////////////////////////////////////////////
//
BEGIN_EVENT_TABLE(mbMainFrame,wxFrame)
	EVT_IDLE(mbMainFrame::OnIdle)
	EVT_SIZE(mbMainFrame::OnSize)
	EVT_MOVE(mbMainFrame::OnMove)
	EVT_CLOSE(mbMainFrame::OnClose)
	EVT_ERASE_BACKGROUND(mbMainFrame::OnEraseBackground)
	EVT_PAINT(mbMainFrame::OnPaint)
	EVT_MENU_OPEN(mbMainFrame::OnMenuOpen)
	EVT_MENU_CLOSE(mbMainFrame::OnMenuClose)
	EVT_MENU(id_exit,mbMainFrame::OnExit)
	EVT_MENU(id_quickstart,mbMainFrame::OnQuickstart)
#ifdef ENABLE_TEST
	EVT_MENU(id_test,mbMainFrame::OnTest)
#endif
	EVT_MENU(id_toggle_sound,mbMainFrame::OnToggleSound)
	EVT_MENU(id_toggle_limit_speed,mbMainFrame::OnToggleLimitSpeed)
	EVT_MENU(id_toggle_status_bar,mbMainFrame::OnToggleStatusBar)
	EVT_MENU(id_toggle_fast_forward_disc_access,mbMainFrame::OnToggleFastForwardDiscAccess)
	EVT_MENU(id_show_about_box,mbMainFrame::OnShowAboutBox)
	EVT_MENU(id_roms_dialog,mbMainFrame::OnRomsDialog)
	EVT_MENU(id_sound_dialog,mbMainFrame::OnSoundDialog)
	EVT_MENU(id_keymap_dialog,mbMainFrame::OnKeymapDialog)

#if (defined bbcDEBUG_TRACE_EVENTS)
	EVT_MENU(id_start_disassembly,mbMainFrame::OnStartDisassembly)
	EVT_MENU(id_stop_disassembly,mbMainFrame::OnStopDisassembly)
	EVT_MENU(id_reboot_with_disassembly,mbMainFrame::OnRebootWithDisassembly)
#endif

#ifdef BEEB_DEBUG_SAVE_CHALLENGER_RAM
	EVT_MENU(id_save_challenger_ram,mbMainFrame::OnSaveChallengerRam)
#endif
#ifdef bbcDEBUG_ENABLE
	EVT_MENU(id_disassemble_ram,mbMainFrame::OnDisassembleRam)
#endif
#ifdef bbcDEBUG_PANELS
	EVT_MENU(id_toggle_dbgpanel_sysvia,mbMainFrame::OnToggleDbgpanelSysvia)
	EVT_MENU(id_toggle_dbgpanel_usrvia,mbMainFrame::OnToggleDbgpanelUsrvia)
	EVT_MENU(id_toggle_dbgpanel_video,mbMainFrame::OnToggleDbgpanelVideo)
	EVT_MENU(id_toggle_dbgpanel_adc,mbMainFrame::OnToggleDbgpanelAdc)
#endif
#ifdef bbcDEBUG_VIDEO
	EVT_MENU(id_toggle_showall,mbMainFrame::OnToggleShowAll)
	EVT_MENU(id_log_video_frames,mbMainFrame::OnLogVideoFrames)
//#ifdef bbcDEBUG_TRACE
//	EVT_MENU(id_log_video_frames_disasm,mbMainFrame::OnLogVideoFrames)
//#endif
	EVT_MENU(id_toggle_show_t1_timeouts,mbMainFrame::OnToggleShowT1Timeouts)
#endif
	EVT_MENU(id_screenshot_save_as,mbMainFrame::OnScreenshotSaveAs)
	EVT_MENU_RANGE(id_first_keymap,id_first_keymap+max_num_keymaps,mbMainFrame::OnKeymap)

	EVT_MENU_RANGE(id_resolution_0,id_resolution_3,mbMainFrame::OnResolution)

	EVT_MENU_RANGE(id_drives_begin,id_drives_begin+num_drive_ids-1,mbMainFrame::OnDriveXCommand)

	EVT_MENU(id_sound_record_start,mbMainFrame::OnSoundRecordStart)
	EVT_MENU(id_sound_record_stop,mbMainFrame::OnSoundRecordStop)
	EVT_MENU(id_hard_reset,mbMainFrame::OnHardReset)
	EVT_MENU_RANGE(id_screen_types_begin,id_screen_types_end,mbMainFrame::OnSetScreenType)
	EVT_COMMAND(id_status_bar,mbEVT_VOLUME,mbMainFrame::OnVolume)
	EVT_COMMAND(id_status_bar,mbEVT_DROPFILE,mbMainFrame::OnDropFile)
//	EVT_MENU(id_toggle_vertical_2x,mbMainFrame::OnToggleVertical2x)
	EVT_MENU(id_toggle_full_screen,mbMainFrame::OnToggleFullScreen)
	EVT_ACTIVATE(mbMainFrame::OnActivate)
	EVT_MENU_RANGE(id_scanlines_begin,id_scanlines_end,mbMainFrame::OnScanlines)
	EVT_MENU(id_joysticks_dialog,mbMainFrame::OnJoysticksDialog)
	EVT_RIGHT_UP(mbMainFrame::OnRightUp)
	EVT_MENU(id_hardware_dialog,mbMainFrame::OnHardwareDialog)
	EVT_MENU(id_video_dialog,mbMainFrame::OnVideoDialog)
#ifdef t65_COUNT_INSTRUCTION_FREQUENCIES
	EVT_MENU(id_show_instruction_frequencies,mbMainFrame::OnShowInstructionFrequencies)
	EVT_MENU(id_show_instruction_frequencies_sorted,mbMainFrame::OnShowInstructionFrequencies)
#endif
#ifdef bbcDEBUG_COUNT_MMIO_ACCESSES
	EVT_MENU(id_show_sheila_access_counts_by_addr,mbMainFrame::OnShowSheilaAccessCounts)
	EVT_MENU(id_show_sheila_access_counts_by_count,mbMainFrame::OnShowSheilaAccessCounts)
#endif
#ifdef bbcENABLE_SAVE_STATE
	EVT_MENU(id_load_state,mbMainFrame::OnLoadState)
	EVT_MENU(id_save_state,mbMainFrame::OnSaveState)
#endif
#ifdef bbcENABLE_PROFILER
	EVT_MENU(id_display_profiler_results,mbMainFrame::OnDisplayProfilerResults)
#endif
#ifdef mbVISUAL_PROFILER
	EVT_MENU(id_show_visual_profile,mbMainFrame::OnShowVisualProfile)
#endif
#ifdef bbcENABLE_BEEB_ICE
	EVT_MENU(id_ice_window_toggle,mbMainFrame::OnIceWindowToggle)
#endif
END_EVENT_TABLE()
