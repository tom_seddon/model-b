#include "pch.h"
#include "mbModel.h"
#include "mbMisc.h"
#include "bbcComputer.h"
#include <bbcComputer.h>
#include <bbcSY6502A.h>
#include <bbcModelB.h>
#include <bbcModelBPlus.h>
#include <bbcModelMaster128.h>
#include <bbcModelBWatford.h>

static const char *all_disc_interfaces[]={
	"none",
	"acorn1770",
	"watford1770",
	"opus1770",
	"chal256",
	"chal512",
	0,
};

static const char *m128_disc_interface[]={
	"master1770",
	0,
};

static const mbBaseModel b_base_model={
	"BBC B",
	"b",
	all_disc_interfaces,
	0xFFFF,
	0,
};

static const mbBaseModel bplus_base_model={
	"BBC B+",
	"bplus",
	all_disc_interfaces,
	0xFFFF,
	0,
};

static const mbBaseModel m128_base_model={
	"Master 128",
	"m128",
	m128_disc_interface,
	0x01FF,
	mbBaseModel::M_NO_FSSLOT,
};

static const mbModel mb_models[]={
	//long_name									short		model,base_model						force_swram_mask
	// B
	{"BBC Model B",								"b",		&bbcModelB::model,&b_base_model,			0,},
	{"BBC Model B with Watford ROM/RAM board",	"bwatford",	&bbcModelBWatford::model,&b_base_model,		0,},
	{"BBC Model B with 65C12",					"b65C12",	&bbcModelB65C12::model,&b_base_model,		0,},

	// B+ and all its variants
	{"BBC Model B+",							"bplus",	&bbcModelBPlus::model,&bplus_base_model,	0,},
	{"BBC Model B+96",							"bplus96",	&bbcModelBPlus::model,&bplus_base_model,	(1<<12)|(1<<13),},
	{"BBC Model B+128",							"bplus128",	&bbcModelBPlus::model,&bplus_base_model,	(1<<12)|(1<<13)|(1<<0)|(1<<1),},
#ifdef bbcENABLE_M128
	// M128
	{"BBC Master 128",							"m128",		&bbcModelMaster128::model,&m128_base_model,	(1<<7)|(1<<6)|(1<<5)|(1<<4),},
#endif
};
static const unsigned mb_num_models=sizeof mb_models/sizeof mb_models[0];

const mbModel *mbModelFromShortName(const std::string &short_name) {
	for(unsigned i=0;i<mb_num_models;++i) {
		if(strcmp(short_name.c_str(),mb_models[i].short_name)==0) {
			return &mb_models[i];
		}
	}
	
	return 0;
}

void mbModelGetAll(std::vector<const mbModel *> *all_models) {
	BASSERT(all_models);
	for(unsigned i=0;i<mb_num_models;++i) {
		all_models->push_back(&mb_models[i]);
	}
}

void mbModelGetAllBaseModels(std::vector<const mbBaseModel *> *all_base_models) {
	BASSERT(all_base_models);
	all_base_models->push_back(&b_base_model);
	all_base_models->push_back(&bplus_base_model);
	all_base_models->push_back(&m128_base_model);
}

bool mbBaseModel::IsValidDiscInterface(const char *disc_interface) const {
	for(int i=0;this->valid_disc_interfaces[i];++i) {
		if(strcmp(this->valid_disc_interfaces[i],disc_interface)==0) {
			return true;
		}
	}
	return false;
}
