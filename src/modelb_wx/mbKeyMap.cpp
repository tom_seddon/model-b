#include "pch.h"
#include "mbKeyMap.h"
#include "Host.h"
#include "mbKeys.h"

mbKeymap::Key::Key(const std::string &device_in,const std::string &key_in):
device(device_in),
key(key_in)
{
}

mbKeymap::Key::Key() {
}

mbKeymap::mbKeymap() {
}

mbKeymap::mbKeymap(const std::string &name_in):
name(name_in)
{
}

//////////////////////////////////////////////////////////////////////////
//

//typedef std::pair<const std::string,std::string> T;
//
//const T default_key_map[]={
//	T("bbc_0","pc_0"),
//	T("bbc_1","pc_1"),
//	T("bbc_2","pc_2"),
//	T("bbc_3","pc_3"),
//	T("bbc_4","pc_4"),
//	T("bbc_5","pc_5"),
//	T("bbc_6","pc_6"),
//	T("bbc_7","pc_7"),
//	T("bbc_8","pc_8"),
//	T("bbc_9","pc_9"),
//	T("bbc_a","pc_a"),
//	T("bbc_at","pc_hash"),
//	T("bbc_b","pc_b"),
//	T("bbc_backslash","pc_backslash,pc_altgr"),
//	T("bbc_break","pc_f11"),
//	T("bbc_c","pc_c"),
//	T("bbc_capslock","pc_capslock,pc_left95"),
//	T("bbc_colon","pc_quote"),
//	T("bbc_comma","pc_comma"),
//	T("bbc_copy","pc_insert"),
//	T("bbc_ctrl","pc_leftctrl,pc_rightctrl,pc_alt"),
//	T("bbc_d","pc_d"),
//	T("bbc_delete","pc_backspace,pc_delete"),
//	T("bbc_down","pc_down"),
//	T("bbc_e","pc_e"),
//	T("bbc_escape","pc_esc"),
//	T("bbc_f","pc_f"),
//	T("bbc_f0","pc_f10,pc_kp_0"),
//	T("bbc_f1","pc_f1"),
//	T("bbc_f2","pc_f2"),
//	T("bbc_f3","pc_f3"),
//	T("bbc_f4","pc_f4"),
//	T("bbc_f5","pc_f5"),
//	T("bbc_f6","pc_f6"),
//	T("bbc_f7","pc_f7"),
//	T("bbc_f8","pc_f8"),
//	T("bbc_f9","pc_f9"),
//	T("bbc_g","pc_g"),
//	T("bbc_h","pc_h"),
//	T("bbc_i","pc_i"),
//	T("bbc_j","pc_j"),
//	T("bbc_k","pc_k"),
//	T("bbc_l","pc_l"),
//	T("bbc_left","pc_left"),
//	T("bbc_lsqbracket","pc_openbrace"),
//	T("bbc_m","pc_m"),
//	T("bbc_minus","pc_minus"),
//	T("bbc_n","pc_n"),
//	T("bbc_o","pc_o"),
//	T("bbc_p","pc_p"),
//	T("bbc_q","pc_q"),
//	T("bbc_quickquit","pc_end"),
//	T("bbc_r","pc_r"),
//	T("bbc_return","pc_enter,pc_kp_enter"),
//	T("bbc_right","pc_right"),
//	T("bbc_rsqbracket","pc_closebrace"),
//	T("bbc_s","pc_s"),
//	T("bbc_semicolon","pc_semicolon"),
//	T("bbc_shift","pc_rightshift,pc_leftshift"),
//	T("bbc_shiftlock","pc_numlock"),
//	T("bbc_slash","pc_slash"),
//	T("bbc_space","pc_space"),
//	T("bbc_stop","pc_stop"),
//	T("bbc_t","pc_t"),
//	T("bbc_tab","pc_tab"),
//	T("bbc_tilde","pc_backquote"),
//	T("bbc_u","pc_u"),
//	T("bbc_underline","pc_equals"),
//	T("bbc_up","pc_up"),
//	T("bbc_v","pc_v"),
//	T("bbc_w","pc_w"),
//	T("bbc_x","pc_x"),
//	T("bbc_y","pc_y"),
//	T("bbc_z","pc_z"),
//};
//const unsigned default_key_map_size=sizeof default_key_map/sizeof default_key_map[0];
//
//mbKeymap &mbKeymap::DefaultKeymap() {
//	static mbKeymap key_map("default");
//	static bool initialised=false;
//
//	if(!initialised) {
//		for(unsigned i=0;i<default_key_map_size;++i) {
//			int bbc_code=mbCodeFromKeyName(default_key_map[i].first);
//			if(bbc_code!=bbcKEY_NONE) {
//				key_map.keymap[bbc_code].push_back()
//			}
//			key_map.SetKey(default_key_map[i].first,default_key_map[i].second);
//		}
//		initialised=true;
//	}
//	return key_map;
//}
//
