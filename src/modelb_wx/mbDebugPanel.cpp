#include "pch.h"
#ifdef bbcDEBUG_PANELS
#include "mbDebugPanel.h"

static const long style=wxDEFAULT_FRAME_STYLE&~(wxRESIZE_BORDER|wxMAXIMIZE_BOX);

mbDebugPanel::mbDebugPanel(wxWindow *parent,wxWindowID id,const wxString &title):
wxFrame(parent,id,title,wxDefaultPosition,wxSize(0,0),style),
width_(-1),
height_(-1),
font_(9,wxMODERN,wxNORMAL,wxNORMAL)
{
	wxLogDebug("mbDebugPanel::mbDebugPanel: font is %s\n",
		font_.GetFaceName().c_str());
	this->Show();
}

bbcDebugPanel *mbDebugPanel::Panel() {
	return &panel_;
}

void mbDebugPanel::UpdateFromPanel() {
	if(width_!=panel_.Width()||height_!=panel_.Height()) {
		width_=panel_.Width();
		height_=panel_.Height();
		for(unsigned i=0;i<lines_.size();++i) {
			lines_[i]->Destroy();
		}
		lines_.clear();
		wxPoint current_pos(0,0);
		wxString default_text;
		default_text.resize(width_,' ');
		int w=0;
		for(int i=0;i<height_;++i) {
			wxStaticText *st=new wxStaticText(this,-1,default_text,current_pos,
				wxDefaultSize,wxALIGN_LEFT);
			st->Show();
			st->SetFont(font_);
			wxSize size=st->GetSize();
			current_pos.y+=size.GetHeight();
			w=size.GetWidth();
			lines_.push_back(st);
		}
		this->SetClientSize(w,current_pos.y);
	}
	if(height_>0) {
		const char *const *lines=panel_.LinesReadOnly();
		for(unsigned i=0;i<lines_.size();++i) {
			lines_[i]->SetLabel(lines[i]);
		}
	}
}

BEGIN_EVENT_TABLE(mbDebugPanel,wxFrame)
END_EVENT_TABLE()
#endif//bbcDEBUG_PANELS
