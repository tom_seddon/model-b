#ifndef mbQUICKSTART_H_
#define mbQUICKSTART_H_

#include <wx/filename.h>

struct mbQuickstartConfig;

struct mbQuickstartResult {
	mbQuickstartResult();

	wxFileName drive0;
	wxFileName drive2;
	bool allow_save;
};

//Pops up a quick start dialog.
//Returns true if user clicked OK -- *result filled in as appropriate.
//Returns false if user clicked OK -- *result untouched.
bool mbQuickstart(wxWindow *parent,mbQuickstartConfig &cfg,
	mbQuickstartResult *result);

#endif
