#include "pch.h"

#ifdef _MSC_VER
#include <crtdbg.h>

//#define twDEBUG_USE_TW_HEAP
#define twDEBUG_USE_CRTDBG
#endif

#if (defined twDEBUG_USE_TW_HEAP)&&(defined twIMAGEHLP_OK)&&(defined _DEBUG)



#include <imagehlp.h>
#include <dia2.h>

//inserts no-mans-land before/after each allocation, then cehcks
//on deallocation to ensure lack of overwrites.
//#define twDEBUG_CHECK_GAPS

//get full stack trace rather than just line and file.
//#define twDEBUG_STACK_TRACE

//use the debug heap
//#define twDEBUG_USE_TW_HEAP

//log heap activity
//#define twDEBUG_ALLOC_LOG

//correct (but probably slower) GetContext calling
//#define twDEBUG_CORRECT_GETCONTEXT

#if (defined UNICODE)||(defined _UNICODE)
#error "Ascii only, sorry, I can't be arsed"
//excessive bracketing due to removal of all _T() macros.
#endif

static long tw_alloc_count=0;
#ifdef twDEBUG_USE_CRTDBG
//static long last_crtdbg_request=-1;
#endif

#ifdef twDEBUG_ALLOC_LOG
static FILE *alloc_log=0;

static void twLog(const char *fmt,...) {
	if(alloc_log) {
		va_list v;
		va_start(v,fmt);
		vfprintf(alloc_log,fmt,v);
		va_end(v);
		fflush(alloc_log);
	}
}
#endif

//a sane strncpy
static char *StringCopy(char *dest,const char *src,unsigned dest_size) {
	unsigned i=0;
	while(src[i]&&i<dest_size-1) {
		dest[i]=src[i];
	}
	dest[i]=0;
	return dest;
}

//////////////////////////////////////////////////////////////////////////
static const unsigned BUFFERSIZE=512;
static bool tw_debug_initialised=false;
static IDiaDataSource *dia_data_source=0;
static IDiaSession *dia_session=0;

template<class T>
static void SafeRelease(T *&p) {
	if(p) {
		p->Release();
		p=0;
	}
}

int dprintf(const char *fmt,...) {
	int n=0;

	char tmpbuf[1024];
	va_list v;

	va_start(v,fmt);
	n=_vsnprintf(tmpbuf,sizeof tmpbuf,fmt,v);
	va_end(v);

	OutputDebugStringA(tmpbuf);

	return n;
}


void twDebugStart(const char *szUserSymbolPath) {
	SymSetOptions(SYMOPT_UNDNAME|SYMOPT_DEFERRED_LOADS);
	if(!SymInitialize(GetCurrentProcess(),0,TRUE)) {
		OutputDebugString(__FUNCTION__ ": SymInitialize failed\n");
	}
}

void twDebugStop() {
	SymCleanup(GetCurrentProcess());
}

void twDebugEnsureInitialised() {
	if(!tw_debug_initialised) {
		wxLog::GetActiveTarget();
		twDebugStart(0);
	}
}

#ifdef twDEBUG_CORRECT_GETCONTEXT
//This is how I get a valid CONTEXT for this thread. I create another thread that suspends the
//main one, gets its context, then finishes.

struct GetContextThreadProcData {
	HANDLE main_thread;
	CONTEXT *context;
};

static DWORD WINAPI GetContextThreadProc(void *param) {
	GetContextThreadProcData *data=static_cast<GetContextThreadProcData *>(param);

	SuspendThread(data->main_thread);
	GetThreadContext(data->main_thread,data->context);
	ResumeThread(data->main_thread);
	ExitThread(0);
}
#endif

//bool twDebugGetSymbolNameFromAddress(DWORD64 addr,char *funcname_buf,unsigned funcname_buf_size,
//	DWORD *displacement)
//{
//	char si_buf[500];//just picked a random value.
//	SYMBOL_INFO *si=reinterpret_cast<SYMBOL_INFO *>(si_buf);
//	DWORD64 disp;
//	HANDLE process=GetCurrentProcess();
//
//	memset(si_buf,0,sizeof si_buf);
//	si->SizeOfStruct=sizeof *si;
//	si->MaxNameLen=sizeof si_buf-sizeof *si;
//
//	if(SymFromAddr(process,addr,&disp,si)) {
//		StringCopy(funcname_buf,si->Name,funcname_buf_size);
//		*displacement=disp;
//		return true;
//	} else {
//		StringCopy(funcname_buf,"",funcname_buf_size);
//		*displacement=0;
//		return false;
//	}
//}

//file_buf must be of size MAX_PATH
bool twDebugGetLineAndFileFromAddress(DWORD addr,char *file_buf,int *line)
{
	IMAGEHLP_LINE ihl;
	DWORD disp;

	ihl.SizeOfStruct=sizeof ihl;
	if(!SymGetLineFromAddr(GetCurrentProcess(),addr,&disp,&ihl)) {
		StringCopy(file_buf,"?",MAX_PATH);
		*line=-1;
		return false;
	} else {
		StringCopy(file_buf,ihl.FileName,MAX_PATH);
		*line=ihl.LineNumber;
		return true;
	}
}

void twDebugGetStackTrace(char *buf,unsigned buf_size) {
//	HANDLE process=GetCurrentProcess();
//	HANDLE thread=GetCurrentThread();
//	DWORD machine=IMAGE_FILE_MACHINE_I386;
//	STACKFRAME stackframe;
//	CONTEXT context;
//
//	context.ContextFlags=CONTEXT_FULL;
//#ifdef twDEBUG_CORRECT_GETCONTEXT
//	{
//		GetContextThreadProcData dta;
//		DWORD thrid;
//
//		dta.main_thread=thread;
//		dta.context=&context;
//		HANDLE thr=CreateThread(0,0,&GetContextThreadProc,&dta,0,&thrid);
//		WaitForSingleObject(thr,INFINITE);
//		CloseHandle(thr);
//	}
//#else
//	GetThreadContext(thread,&context);
//#endif
//
//	memset(&stackframe,0,sizeof stackframe);
//
//	int depth=0;
//	for(;;) {
//		BOOL b=StackWalk(machine,process,thread,&stackframe,&context,0,&SymFunctionTableAccess,
//			&SymGetModuleBase,0);
//		if(depth==0) {
//			continue;
//		}
//		if(!b||stackframe.AddrFrame.Offset==0) {
//			break;
//		}
//
//		char funcname[256];
//		DWORD disp;
//		char disp_str[33];
//		twDebugGetSymbolNameFromAddress(stackframe.AddrPC,funcname,sizeof funcname,&disp);
//		_ultoa(disp,disp_str,16);
//
//		char *const buf_end=&buf[buf_size];
//		char *dest=StringCopy(buf,funcname,buf_end-dest);
//		StringCopy(dest,"(",buf_end-dest);
//	}
}

//////////////////////////////////////////////////////////////////////////
// Allocation functions

struct twAllocation {
	//How much the user requested
	unsigned char *user_base;
	unsigned user_size;

	//front gap is &user_base[-gap_size]
	//back gap is &user_base[user_size]
	int gap_size;

	//base + size of the allocation as the RTL sees it
	unsigned char *rtl_base;
	unsigned rtl_size;

	//where it was allocated
#ifdef twDEBUG_STACK_TRACE
	char stack_trace[2048];
#else
	char file[MAX_PATH];
	int line;
#endif
//	const char *file;
//	int line;

	void PrintName() const;
	int CheckGap(unsigned char *start) const;
	bool AreGapsValid() const;
	unsigned char *FrontGap() const;
	unsigned char *BackGap() const;
};

void twAllocation::PrintName() const {
#ifdef twDEBUG_STACK_TRACE
	OutputDebugStringA(this->stack_trace);
#else
	static char numbuf[20];
	_snprintf(numbuf,sizeof numbuf,"(%d): ",this->line);
	OutputDebugStringA(this->file);
	OutputDebugStringA(numbuf);
#endif
}

unsigned char *twAllocation::FrontGap() const {
	return this->user_base-this->gap_size;
}

unsigned char *twAllocation::BackGap() const {
	return this->user_base+this->user_size;
}

int twAllocation::CheckGap(unsigned char *start) const {
	for(int i=0;i<gap_size;++i) {
		if(start[i]!=i) {
			return i;
		}
	}
	return -1;
}

bool twAllocation::AreGapsValid() const {
	bool ret=true;
	int fg=this->CheckGap(this->user_base-gap_size);
	static char buf[100];
	if(fg>=0) {
		this->PrintName();
		_snprintf(buf,sizeof buf,"front gap: start-%d: ==0x%02X, !=0x%02X\n",
			this->gap_size-fg,this->FrontGap()[fg],fg);
		OutputDebugStringA(buf);
		ret=false;
	}
	int bg=this->CheckGap(this->user_base+this->user_size);
	if(bg>=0) {
		this->PrintName();
		_snprintf(buf,sizeof buf,"front gap: end+%d: ==0x%02X, !=0x%02X\n",
			bg,this->BackGap()[bg],bg);
		OutputDebugStringA(buf);
		ret=false;
	}
	return ret;
}

static const unsigned max_num_allocations=10000;
static twAllocation allocations[max_num_allocations];
static unsigned num_allocations=0;

void twVerifyAllocations() {
#ifdef twDEBUG_CHECK_GAPS
	for(unsigned i=0;i<num_allocations;++i) {
		wxASSERT(allocations[i].AreGapsValid());
	}
#endif
}

long twGetTotalNumAllocations() {
#ifdef twDEBUG_USE_CRTDBG
	return (&_crtBreakAlloc)[-1];// this is dead ugly.
#else
	return tw_alloc_count;
#endif
}

#ifdef twDEBUG_USE_CRTDBG

static void *twAllocate(unsigned size,unsigned caller) {//,const char *file,int line) {
	twDebugEnsureInitialised();
	IMAGEHLP_LINE ihl;
	DWORD disp;
	ihl.SizeOfStruct=sizeof ihl;
	const char *file="?";
	int line=-1;
	if(SymGetLineFromAddr(GetCurrentProcess(),caller,&disp,&ihl)) {
		file=ihl.FileName;
		line=ihl.LineNumber;
	}

	void *p=_malloc_dbg(size,_NORMAL_BLOCK,file,line);

//	_CrtIsMemoryBlock(p,size,&last_crtdbg_request,0,0);
//
	return p;
}

static void twDeallocate(void *ptr,unsigned caller) {
	_free_dbg(ptr,_NORMAL_BLOCK);
}

#else//twDEBUG_USE_CRTDBG

static void *twAllocate(unsigned size,unsigned caller) {//,const char *file,int line) {
	twDebugEnsureInitialised();
	twVerifyAllocations();
	wxASSERT(num_allocations<max_num_allocations);//too many
	twAllocation *a=&allocations[num_allocations++];

	//fill in debug info.
	if(!tw_track_heap_allocations) {
#ifdef twDEBUG_STACK_TRACE
		_snprintf(a->stack_trace,sizeof a->stack_trace,"%u",tw_alloc_count);
#else
		strcpy(a->file,"");
		a->line=int(tw_alloc_count);
#endif
	} else {
#ifdef twDEBUG_STACK_TRACE
		twDebugGetStackTrace(a->stack_trace,sizeof a->stack_trace);
#else
		twDebugGetLineAndFileFromAddress(caller,a->file,&a->line);
#endif
	}
	++tw_alloc_count;

	//Decide on size
	//This is a PC, therefore infinite memory, so super size gaps.
#ifdef twDEBUG_CHECK_GAPS
	a->gap_size=128;
#else
	a->gap_size=0;
#endif
	a->rtl_size=size+a->gap_size*2;
#ifdef _DEBUG
# ifdef twDEBUG_STACK_TRACE
	a->rtl_base=static_cast<unsigned char *>(
		_malloc_dbg(a->rtl_size,_NORMAL_BLOCK,a->stack_trace,tw_alloc_count));
# else
	a->rtl_base=static_cast<unsigned char *>(
		_malloc_dbg(a->rtl_size,_NORMAL_BLOCK,a->file,a->line));
# endif
#else//_DEBUG
	a->rtl_base=static_cast<unsigned char *>(malloc(a->rtl_size));
#endif//_DEBUG
	wxASSERT(a->rtl_base);//out of memory

	a->user_base=a->rtl_base+a->gap_size;
	a->user_size=size;

	//	a->file=file;
//	a->line=line;

	//sort out the gaps
	for(int i=0;i<a->gap_size;++i) {
		a->FrontGap()[i]=i;
		a->BackGap()[i]=i;
	}

#ifdef twDEBUG_ALLOC_LOG
	twLog("alloc: usr: %u bytes @ 0x%p rtl: %u bytes @ 0x%p\n",
		a->user_size,a->user_base,a->rtl_size,a->rtl_base);
#ifdef twDEBUG_STACK_TRACE
	twLog("from %s\n",a->stack_trace);
#else//twDEBUG_STACK_TRACE
	twLog("%s(%d) <- from\n",a->file,a->line);
#endif//twDEBUG_STACK_TRACE
#endif//twDEBUG_ALLOC_LOG
	return a->user_base;
}

static void twDeallocate(void *ptr,unsigned caller) {
	twDebugEnsureInitialised();
	twVerifyAllocations();
	if(ptr) {
		unsigned i;
		for(i=0;i<num_allocations;++i) {
			if(allocations[i].user_base==ptr) {
				break;
			}
		}
		wxASSERT(i<num_allocations);//that pointer isn't valid
		twAllocation *a=&allocations[i];
		wxASSERT(a->AreGapsValid());//oops, overran start/end of allocation!
#ifdef twDEBUG_ALLOC_LOG
		twLog("free : usr: %u bytes @ 0x%p rtl: %u bytes @ 0x%p\n",
			a->user_size,a->user_base,a->rtl_size,a->rtl_base);
#ifdef twDEBUG_STACK_TRACE
		static char stack_trace[2048];
		twDebugGetStackTrace(stack_trace,sizeof stack_trace);
		twLog("from %s\n",stack_trace);
#else//twDEBUG_STACK_TRACE
		char file[MAX_PATH];
		int line;
		twDebugGetLineAndFileFromAddress(caller,file,&line);
		twLog("%s(%d) <- from\n",file,line);
#endif//twDEBUG_STACK_TRACE
#endif//twDEBUG_ALLOC_LOG
		//Seems ok, get rid of that one.
		//_free_dbg(a->rtl_base,_NORMAL_BLOCK);
		_free_dbg(a->rtl_base,_NORMAL_BLOCK);
		--num_allocations;
		*a=allocations[num_allocations];
	}
}

#endif//twDEBUG_USE_CRTDBG

void __cdecl DumpClientDumpMemoryLeaks(void *, size_t) {
	const unsigned max_n=4096;
	char buf[max_n];

	for(unsigned i=0;i<num_allocations;++i) {
		twAllocation *a=&allocations[i];
		int n=0;

#ifndef twDEBUG_STACK_TRACE
		n+=_snprintf(&buf[n],max_n-n,"%s(%u): ",a->file,a->line);
#endif
		n+=_snprintf(&buf[n],max_n-n,"user: %u @ 0x%p rtl: %u @ 0x%p\n",
			a->user_size,a->user_base,a->rtl_base,a->rtl_size);
#ifdef twDEBUG_STACK_TRACE
		n+=_snprintf(&buf[n],max_n-n,"%s\n",a->stack_trace);
#endif
		OutputDebugString(buf);
	}
}

//////////////////////////////////////////////////////////////////////////
// Allocators

void *__cdecl operator new(unsigned sz) {
	unsigned caller;
	__asm {
		mov eax,[ebp+4];
		mov caller,eax;
	}
	void *p=twAllocate(sz,caller);//,file,line);//_malloc_dbg(sz,_NORMAL_BLOCK,file,line);
	return p;
}

void __cdecl operator delete(void *ptr) {
	unsigned caller;
	__asm {
		mov eax,[ebp+4];
		mov caller,eax;
	}
	twDeallocate(ptr,caller);
}

#if _MSC_VER>0x1200
//VC++6 (0x1200) doesn't seem to distinguish between the 2 forms.
void *__cdecl operator new[](unsigned sz) {
	unsigned caller;
	__asm {
		mov eax,[ebp+4];
		mov caller,eax;
	}
	void *p=twAllocate(sz,caller);//,file,line);//_malloc_dbg(sz,_NORMAL_BLOCK,file,line);
	return p;
}

void *__cdecl operator delete[](unsigned sz) {
	unsigned caller;
	__asm {
		mov eax,[ebp+4];
		mov caller,eax;
	}
	twDeallocate(ptr,caller);
}

#endif//_MSC_VER>0x1200

#else//twDEBUG_USE_TW_HEAP

long twGetTotalNumAllocations() {
#if (defined _DEBUG)&&(defined twDEBUG_USE_CRTDBG)
	return (&_crtBreakAlloc)[-1];// this is dead ugly.
#else
	return -1;
#endif
}

#endif//twDEBUG_USE_TW_HEAP
