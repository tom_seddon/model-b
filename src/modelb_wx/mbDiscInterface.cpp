#include "pch.h"
#include "mbDiscInterface.h"
#include <bbc1770Acorn.h>
#include <bbc1770Opus.h>
#include <bbc1770Watford.h>
#include <bbc1770OpusChallenger.h>
#include <bbc1770Master128.h>

static const mbDiscInterface null_interface={"none","None",0};

static const mbDiscInterface disc_interfaces[]={
	{"acorn1770",bbc1770Acorn::disc_interface.name,&bbc1770Acorn::disc_interface},//Acorn 1770
	{"opus1770",bbc1770Opus::disc_interface.name,&bbc1770Opus::disc_interface},
	{"watford1770",bbc1770Watford::disc_interface.name,&bbc1770Watford::disc_interface},
	{"chal256",bbc1770OpusChallenger::disc_interface_256k.name,&bbc1770OpusChallenger::disc_interface_256k},
	{"chal512",bbc1770OpusChallenger::disc_interface_512k.name,&bbc1770OpusChallenger::disc_interface_512k},
#ifdef bbcENABLE_M128
	{"master1770",bbc1770Master128::disc_interface.name,&bbc1770Master128::disc_interface},
#endif
};
static const unsigned num_disc_interfaces=sizeof disc_interfaces/sizeof disc_interfaces[0];

const mbDiscInterface *mbDiscInterfaceFromShortName(const char *short_name) {
	if(short_name) {
		for(unsigned i=0;i<num_disc_interfaces;++i) {
			if(strcmp(disc_interfaces[i].short_name,short_name)==0) {
				return &disc_interfaces[i];
			}
		}
	}

	return &null_interface;
}

void mbDiscInterfaceGetAll(std::vector<const mbDiscInterface *> *all_interfaces) {
	all_interfaces->clear();
	all_interfaces->push_back(&null_interface);
	for(unsigned i=0;i<num_disc_interfaces;++i) {
		all_interfaces->push_back(&disc_interfaces[i]);
	}
}
