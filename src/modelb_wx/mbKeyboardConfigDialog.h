#ifndef mbKeyboardCONFIGDIALOG_H_
#define mbKeyboardCONFIGDIALOG_H_

#include "mbConfig.h"

struct Keycap;
class wxListCtrl;

class mbKeyboardConfigDialog:
public wxDialog
{
public:
	mbKeyboardConfigDialog(wxWindow *parent,const mbKeyboardConfig &cfg);
	void GetResult(mbKeyboardConfig *cfg);
protected:
private:
	void OnSize(wxSizeEvent &event);
	void OnAdd(wxCommandEvent &event);
	void OnRemove(wxCommandEvent &event);
	void OnBeebKey(wxCommandEvent &event);
	void OnLoad(wxCommandEvent &event);
	void OnSave(wxCommandEvent &event);
	void OnCloseButton(wxCommandEvent &event);
	void OnClose(wxCloseEvent &event);
		
	//the widget for a key
	struct Key {
		const Keycap *keycap;
		wxButton *button;
		int row,x;
		
		Key();
	};
	
	mbKeyboardConfig cfg_;//cfg being edited
	mbKeymap current_keymap_;//keymap being edited
	bool current_keymap_changed_;//whether current_keymap_ is changed since set up
	std::vector<Key> keys_;//all key widgets
	int row_width_;//width (in halves) of the longest row
	wxButton *close_btn_;

	void Changed();
	bool DestructiveActionConfirmed();
	bool LoadKeymap(const std::string &keymap_name);

	DECLARE_EVENT_TABLE()
};

#endif
