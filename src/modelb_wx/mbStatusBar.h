#ifndef mbSTATUSBAR_H_
#define mbSTATUSBAR_H_

#include <bbc1770.h>
#include <bbcFdd.h>
//#include <bbcDebugTrace.h>

#if wxUSE_DRAG_AND_DROP
#include <wx/dnd.h>
#endif

class mbStatusBar:
public wxStatusBar
{
public:
	mbStatusBar(wxWindow *parent,int id);

	void SetVolumeEnabled(bool enabled);
	void SetVolume(int volume);
	void SetScreenSize(int width,int height);
	void SetMhz(float mhz);
	void SetFdcStatus(bbc1770::FdcAction action,int drive,int track,int sector);
#ifdef bbcDEBUG_ENABLE
	void SetPc(t65::Word pc);
#endif
protected:
private:
	struct DriveState {
		bbc1770::FdcAction action;
		int track,sector;
		wxRect rect;

		DriveState();
	};
	DriveState drive_states_[bbcFdd::num_drives];

	void OnSize(wxSizeEvent &event);
	void OnVolume(wxScrollEvent &event);

	wxSlider *volume_;

#if wxUSE_DRAG_AND_DROP
	class mbFileDropTarget:
	public wxFileDropTarget
	{
	public:
		mbFileDropTarget(mbStatusBar *owner);
		bool OnDropFiles(wxCoord x,wxCoord y,const wxArrayString &filenames);
		wxDragResult OnDragOver(wxCoord x,wxCoord y,wxDragResult def);
	private:
		mbStatusBar *owner_;
		int DriveFromPoint(wxCoord x,wxCoord y);
	};

	friend class mbFileDropTarget;
#endif//wxUSE_DRAG_AND_DROP

	DECLARE_EVENT_TABLE()
};

//m_extraLong=volume
DECLARE_LOCAL_EVENT_TYPE(mbEVT_VOLUME,_)
//m_commandInt=drive, m_commandString=filename
DECLARE_LOCAL_EVENT_TYPE(mbEVT_DROPFILE,_)

#endif
