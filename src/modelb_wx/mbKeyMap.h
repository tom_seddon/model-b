#ifndef mbKEYMAP_H_
#define mbKEYMAP_H_

struct mbKeymap {
	struct Key {
		std::string device;
		std::string key;

		Key();
		Key(const std::string &device,const std::string &key);
	};
	//static mbKeymap &DefaultKeymap();

	mbKeymap();
	mbKeymap(const std::string &name_in);

	std::string name;
	
	typedef std::vector<Key> HostKeys;//PC Key List
	//This maps BBC key to the list of PC keys that affect it.
	//This is done so each BBC key is touched once, regardless of how many
	//PC keys affect it.
	HostKeys keymap[256];
	
	typedef std::vector<mbKeymap> C;
};

#endif
