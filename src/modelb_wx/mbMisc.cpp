#include "pch.h"
#include "mbMisc.h"
#include "mbConfigFile.h"
#include "mbConfig.h"
#include <HostTmr.h>
#ifdef PORT_USE_WX_KEYBOARD
#include <HostInpShared.h>
#endif
#include <wx/filesys.h>
#include <wx/wfstream.h>
#include <wx/zstream.h>
#include <wx/mstream.h>

//This is the largest file mbLoadFile will load.
//I put this in because the validity of a disc is determined after its
//contents have loaded. So if the user clicks on a massive file by accident,
//it will be loaded first... this should prevent anything nasty happening.
static const off_t max_load_file_size=16777216;

bool mbSaveFile(const char *filename,const std::vector<wxByte> &contents) {
	wxFileOutputStream out(filename);
	if(!out.Ok()) {
		return false;
	}
	out.Write(&contents[0],contents.size());
	if(!out.Ok()) {
		return false;
	}
	return true;
}

bool mbLoadFile(const char *filename,std::vector<wxByte> &contents) {
#ifdef _WIN32
	//For some reason, the wx system had trouble loading the " start" file from the 6502 test
	//suite. So on Win32 I'm using CreateFile and friends, which seem to do the trick.
	HANDLE h;
	bool ok=false;

	h=CreateFileA(filename,GENERIC_READ,0,0,OPEN_EXISTING,0,0);
	if(h!=INVALID_HANDLE_VALUE) {
		DWORD size;

		size=GetFileSize(h,0);
		if(size!=INVALID_FILE_SIZE&&size<=max_load_file_size) {
			DWORD num_read;

			contents.resize(size);
			if(ReadFile(h,&contents[0],size,&num_read,0)&&num_read==size) {
				ok=true;
			}
		}
		CloseHandle(h);
	}
	return ok;
#else
	contents.clear();
	wxFileSystem fs;
	wxFSFile *f=fs.OpenFile(filename);
	if(!f) {
		return false;
	}
	wxInputStream *stream=f->GetStream();
	if(!stream) {
		delete f;
		return false;
	}
	stream->SeekI(0,wxFromEnd);
	off_t length=stream->TellI();
	if(length>=max_load_file_size) {
		delete f;
		return false;
	}
	stream->SeekI(0,wxFromStart);
	contents.resize(length);
	if(!stream->Read(&contents[0],length).IsOk()) {
		delete f;
		return false;
	}
	delete f;
	return true;
#endif
}

bool mbLoadCompressedFile(const char *filename,std::vector<wxByte> &contents) {
	std::vector<wxByte> packed;
	if(!mbLoadFile(filename,packed)) {
		return false;
	}

	wxMemoryInputStream mem_input(&packed[0],packed.size());
	wxZlibInputStream zi(mem_input);

	contents.clear();
	contents.reserve(packed.size()*2);//very very vague guess!
	do {
		contents.push_back(zi.GetC());
	} while(zi.LastRead()==1);

	return zi.GetLastError()==wxSTREAM_NO_ERROR;
}

void mbGetMatchingFileNames(const wxFileName &root,const wxString &name_spec,
	std::vector<wxFileName> *file_names)
{
	wxASSERT(file_names);
	wxFileName spec(root);
	spec.SetName(name_spec);

	//wxLogDebug("mbGetMatchingFileNames: files: \"%s\"...",spec.GetFullPath().c_str());
	wxFileSystem fs;
	wxString name=fs.FindFirst(spec.GetFullPath(),wxFILE);

	while(!name.empty()) {
		file_names->push_back(wxFileSystem::URLToFileName(name));
		name=fs.FindNext();
	}
	//wxLogDebug("mbGetMatchingFileNames: files: %u found",file_names->size());
}

void mbGetDirectoryNames(const wxFileName &root,std::vector<wxFileName> *dir_names) {
	wxASSERT(dir_names);
	wxFileName spec=root;
	spec.SetName("*");
//	wxLogDebug("mbGetDirectoryNames: \"%s\"...",spec.GetFullPath().c_str());
	wxFileSystem fs;
	wxString name=fs.FindFirst(spec.GetFullPath(),wxDIR);
	while(!name.empty()) {
		if(name!="."&&name!="..") {
			wxFileName dir_name=wxFileSystem::URLToFileName(name+"/");
			dir_names->push_back(dir_name);
		}
		name=fs.FindNext();
	}
//	wxLogDebug("mbGetDirectoryNames: %u found",dir_names->size());
}

wxString mbNormalizedFileName(const wxString &file_name) {
	wxFileName f=wxFileName::FileName(file_name);
	f.Normalize();
	return f.GetFullPath();
}

wxString mbRelativeFileName(const wxString &file_name,const wxString &base_dir) {
	wxFileName f=wxFileName::FileName(file_name);
	f.MakeRelativeTo(base_dir);
	return f.GetFullPath();
}

//A bit dumb, but there you go -- it has been tested a lot, though.
template<class T>
static void mbTokenizeStringInternal(const char *source,const char *seperators,T &tokensOutput,bool handleQuotes) {
	static const char QUOTE_CHAR='"';
	typename T::iterator currentToken;
	const char *currentChar;
	bool inQuotes=false;

	if(handleQuotes&&strchr(seperators,QUOTE_CHAR)) {
		handleQuotes=false;
	}
	tokensOutput.clear();
	currentToken=tokensOutput.end();
	for(currentChar=source;*currentChar!=0;++currentChar) {
		if(handleQuotes&&*currentChar==QUOTE_CHAR) {
			inQuotes=!inQuotes;
			continue;
		}
		if(!inQuotes&&strchr(seperators,*currentChar)) {
			// this is a seperator. 
			currentToken=tokensOutput.end();
			continue;
		}
		// this is not a seperator.
		if(currentToken==tokensOutput.end()) {
			currentToken=tokensOutput.insert(tokensOutput.end(),typename T::value_type());
		}
		currentToken->append(1,*currentChar);
	}
}

void mbTokenizeString(const char *source,const char *seperators,std::vector<std::string> &tokensOutput,bool handleQuotes) {
	return mbTokenizeStringInternal(source,seperators,tokensOutput,handleQuotes);
}

void mbTokenizeString(const char *source,const char *seperators,std::vector<wxString> &tokensOutput,bool handleQuotes) {
	return mbTokenizeStringInternal(source,seperators,tokensOutput,handleQuotes);
}

wxWindowID mbNewId() {
	static wxWindowID next=wxID_HIGHEST+1;

	return next++;
}

wxWindowID mbNewIds(int n) {
	wxWindowID ret=mbNewId();
	for(int i=1;i<n;++i) {
		mbNewId();
	}
	return ret;
}

std::string mbStrPrintf(const char *fmt,...) {
	char tmp[16384];
	va_list v;
	va_start(v,fmt);
	_vsnprintf(tmp,sizeof tmp,fmt,v);
	va_end(v);
	return tmp;
}

mbSTLStringValidator::mbSTLStringValidator(long style,std::string *val):
wxTextValidator(style,&tmp_),
str_(val),
tmp_(val->c_str())
{
}

mbSTLStringValidator::mbSTLStringValidator(const mbSTLStringValidator &src):
wxTextValidator(src),
str_(src.str_),
tmp_(src.tmp_)
{
	m_stringValue=&tmp_;
}

wxObject *mbSTLStringValidator::Clone() const {
	return new mbSTLStringValidator(*this);
}

bool mbSTLStringValidator::TransferToWindow() {
	tmp_=str_->c_str();
	bool r=this->wxTextValidator::TransferToWindow();
	return r;
}

bool mbSTLStringValidator::TransferFromWindow() {
	bool r=this->wxTextValidator::TransferFromWindow();
	*str_=tmp_.c_str();
	return r;
}

//////////////////////////////////////////////////////////////////////////

void mbSetWindowPos(wxWindow *w,const wxPoint &new_pos) {
	wxRect new_rect(new_pos,w->GetSize());
	mbSetWindowRect(w,new_rect);
}

void mbSetWindowRect(wxWindow *w,const wxRect &new_rect) {
	wxRect final_rect=new_rect;

#ifdef _WIN32

	RECT r;
	r.left=final_rect.GetLeft();
	r.top=final_rect.GetTop();
	r.right=final_rect.GetRight();
	r.bottom=final_rect.GetBottom();
	HMONITOR hm=MonitorFromRect(&r,MONITOR_DEFAULTTONULL);
	if(!hm) {
		// must reposition
		hm=MonitorFromRect(&r,MONITOR_DEFAULTTONEAREST);

		MONITORINFOEX mi;
		mi.cbSize=sizeof mi;
		GetMonitorInfo(hm,&mi);

		final_rect.SetTopLeft(wxPoint(mi.rcWork.left,mi.rcWork.top));

		wxLogDebug("%s: positioned window @ (%d,%d), device \"%s\".",__FUNCTION__,final_rect.GetLeft(),final_rect.GetTop(),
			mi.szDevice);
	}

#endif//_WIN32

	w->SetSize(final_rect);
}

//////////////////////////////////////////////////////////////////////////

bool mbLoadRect(mbConfigFile *cfg,const char *section,wxRect *rect) {
	wxASSERT(rect);

	if(!cfg->Read(section,"x",&rect->x)||!cfg->Read(section,"y",&rect->y)||
		!cfg->Read(section,"width",&rect->width)||!cfg->Read(section,"height",&rect->height))
	{
		*rect=wxRect(-1,-1,-1,-1);
		return false;
	} else {
		return true;
	}
}

//bool mbLoadRect(mbConfigFile *cfg,const char *section,RECT *rect) {
	//wxRect tmp;
	//if(!mbLoadRect(cfg,section,&tmp)) {
		//return false;
	//} else {
		//rect->left=tmp.GetLeft();
		//rect->top=tmp.GetTop();
		//rect->right=tmp.GetRight();
		//rect->bottom=tmp.GetBottom();

		//return true;
	//}
//}

void mbSaveRect(mbConfigFile *cfg,const char *section,const wxRect &rect) {
	cfg->Write(section,"x",rect.x);
	cfg->Write(section,"y",rect.y);
	cfg->Write(section,"width",rect.width);
	cfg->Write(section,"height",rect.height);
}

//void mbSaveRect(mbConfigFile *cfg,const char *section,const RECT &rect) {
	//wxRect r(wxPoint(rect.left,rect.top),wxPoint(rect.bottom,rect.right));
	//mbSaveRect(cfg,section,r);
//}

bool mbLoadPoint(mbConfigFile *cfg,const char *section,wxPoint *pt) {
	if(cfg->Read(section,"x",&pt->x)&&cfg->Read(section,"y",&pt->y)) {
		return true;
	} else {
		pt->x=0;
		pt->y=0;

		return false;
	}
}

void mbSavePoint(mbConfigFile *cfg,const char *section,const wxPoint &pt) {
	cfg->Write(section,"x",pt.x);
	cfg->Write(section,"y",pt.y);
}

bool mbLoadWindowPlacement(mbConfigFile *cfg,const char *section_in,mbWindowPlacement *wp) {
	std::string section(section_in);

	wp->valid=cfg->Read(section.c_str(),"ShowCmd",&wp->show_cmd)&&
		wp->show_cmd>=0&&
		mbLoadPoint(cfg,(section+"/MinPosition").c_str(),&wp->min_pos)&&
		mbLoadPoint(cfg,(section+"/MaxPosition").c_str(),&wp->max_pos)&&
		mbLoadRect(cfg,(section+"/NormalPosition").c_str(),&wp->rect);

	return wp->valid;
}

mbWindowPlacement::mbWindowPlacement():
valid(false)
{
}

void mbSaveWindowPlacement(mbConfigFile *cfg,const char *section_in,const mbWindowPlacement &wp) {
	std::string section(section_in);

	if(!wp.valid) {
		cfg->Write(section.c_str(),"ShowCmd",-1);
	} else {
		cfg->Write(section.c_str(),"ShowCmd",wp.show_cmd);
		mbSavePoint(cfg,(section+"/MinPosition").c_str(),wp.min_pos);
		mbSavePoint(cfg,(section+"/MaxPosition").c_str(),wp.max_pos);
		mbSaveRect(cfg,(section+"/NormalPosition").c_str(),wp.rect);
	}
}

void mbGetWindowPlacement(const wxFrame *f,mbWindowPlacement *wp) {
#ifdef _WIN32
	HWND h=HWND(f->GetHandle());

	WINDOWPLACEMENT tmp;
	tmp.length=sizeof tmp;

	if(!GetWindowPlacement(h,&tmp)) {
		wp->valid=false;
	} else {
		wp->valid=true;
		wp->show_cmd=tmp.showCmd;
		wp->min_pos=wxPoint(tmp.ptMinPosition.x,tmp.ptMinPosition.y);
		wp->max_pos=wxPoint(tmp.ptMaxPosition.x,tmp.ptMaxPosition.y);
		wp->rect=wxRect(wxPoint(tmp.rcNormalPosition.left,tmp.rcNormalPosition.top),
			wxPoint(tmp.rcNormalPosition.right,tmp.rcNormalPosition.bottom));
	}
#else//_WIN32
	wp->valid=false;
#endif//_WIN32
}

void mbSetWindowPlacement(wxFrame *f,const mbWindowPlacement &mbwp) {
#ifdef _WIN32
	if(mbwp.valid) {
		WINDOWPLACEMENT wp;
		wp.length=sizeof wp;
		wp.flags=0;
		wp.showCmd=mbwp.show_cmd;
		
		wp.ptMinPosition.x=mbwp.min_pos.x;
		wp.ptMinPosition.y=mbwp.min_pos.y;

		wp.ptMaxPosition.x=mbwp.max_pos.x;
		wp.ptMaxPosition.y=mbwp.max_pos.y;

		wp.rcNormalPosition.left=mbwp.rect.GetLeft();
		wp.rcNormalPosition.top=mbwp.rect.GetTop();
		wp.rcNormalPosition.right=mbwp.rect.GetRight();
		wp.rcNormalPosition.bottom=mbwp.rect.GetBottom();

		f->Show();
		HWND h=HWND(f->GetHandle());
		SetWindowPlacement(h,&wp);
		f->SendSizeEvent();
	}
#endif//_WIN32
}

bool mbSaveCompressedFile(const char *filename,const std::vector<wxByte> &contents) {
	wxFileOutputStream out_file(filename);
	if(!out_file.Ok()) {
		return false;
	}

	wxLogDebug("%s: saving to \"%s\"...",__FUNCTION__,filename);
	int start_ms;
	HostTmr_ReadMilliseconds(&start_ms);
	wxZlibOutputStream out_zlib(out_file,wxZ_BEST_COMPRESSION,wxZLIB_GZIP);
	out_zlib.Write(&contents[0],contents.size());
	int end_ms;
	HostTmr_ReadMilliseconds(&end_ms);
	wxLogDebug("    took %dms.",end_ms-start_ms);

	return true;
}

//////////////////////////////////////////////////////////////////////////

mbFileDlg::mbFileDlg(mbFileDlgConfig &dlg_cfg,wxWindow *parent,const wxString &msg,const wxString &wildcard,long style):
wxFileDialog(parent,msg,"","",wildcard,style),
dlg_cfg_(&dlg_cfg)
{
	if(!dlg_cfg_->default_folder.empty()) {
		this->SetDirectory(dlg_cfg_->default_folder.c_str());
	}

	this->SetFilterIndex(dlg_cfg_->filter_idx);
}

int mbFileDlg::ShowModal() {
	int r=this->wxFileDialog::ShowModal();
	if(r==wxID_OK) {
		dlg_cfg_->default_folder=this->GetDirectory();
		dlg_cfg_->filter_idx=this->GetFilterIndex();
	}

	return r;
}

//////////////////////////////////////////////////////////////////////////
#ifdef PORT_USE_WX_KEYBOARD
typedef std::pair<long,wxString> Key;
typedef std::vector<Key> Keys;

static void AddKey(Keys *keys,long code,const char *fmt,...) {
	// quick sanity check, to ensure the table doesn't become very lange
	// if this ever happens, will have to rethink.
	wxASSERT(code>=0&&code<512);

	if(unsigned(code)>=keys->size()) {
		keys->resize(1+code,Key(-1,""));
	}

	va_list v;
	va_start(v,fmt);
	(*keys)[code]=Key(code,"KEY_"+wxString::FormatV(fmt,v));
	va_end(v);
}

static const Keys &GetKeys() {
	static bool made_keys=false;
	static Keys keys;

	if(!made_keys) {
		for(int i=0;i<256;++i) {
			AddKey(&keys,i,"#%d",i);
		}

		for(int i=0;i<10;++i) {
			AddKey(&keys,'0'+i,"%c",'0'+i);
		}

		for(int i=0;i<26;++i) {
			AddKey(&keys,'A'+i,"%c",'A'+i);
		}

		AddKey(&keys,WXK_BACK,"BACK");
		AddKey(&keys,WXK_TAB,"TAB");
		AddKey(&keys,WXK_RETURN,"RETURN");
		AddKey(&keys,WXK_ESCAPE,"ESCAPE");
		AddKey(&keys,WXK_SPACE,"SPACE");
		AddKey(&keys,WXK_DELETE,"DELETE");
		AddKey(&keys,WXK_START,"START");
		AddKey(&keys,WXK_LBUTTON,"LBUTTON");
		AddKey(&keys,WXK_RBUTTON,"RBUTTON");
		AddKey(&keys,WXK_CANCEL,"CANCEL");
		AddKey(&keys,WXK_MBUTTON,"MBUTTON");
		AddKey(&keys,WXK_CLEAR,"CLEAR");
		AddKey(&keys,WXK_SHIFT,"SHIFT");
		AddKey(&keys,WXK_ALT,"ALT");
		AddKey(&keys,WXK_CONTROL,"CONTROL");
		AddKey(&keys,WXK_MENU,"MENU");
		AddKey(&keys,WXK_PAUSE,"PAUSE");
		AddKey(&keys,WXK_CAPITAL,"CAPITAL");
		AddKey(&keys,WXK_PRIOR,"PRIOR");
		AddKey(&keys,WXK_NEXT,"NEXT");
		AddKey(&keys,WXK_END,"END");
		AddKey(&keys,WXK_HOME,"HOME");
		AddKey(&keys,WXK_LEFT,"LEFT");
		AddKey(&keys,WXK_UP,"UP");
		AddKey(&keys,WXK_RIGHT,"RIGHT");
		AddKey(&keys,WXK_DOWN,"DOWN");
		AddKey(&keys,WXK_SELECT,"SELECT");
		AddKey(&keys,WXK_PRINT,"PRINT");
		AddKey(&keys,WXK_EXECUTE,"EXECUTE");
		AddKey(&keys,WXK_SNAPSHOT,"SNAPSHOT");
		AddKey(&keys,WXK_INSERT,"INSERT");
		AddKey(&keys,WXK_HELP,"HELP");
		AddKey(&keys,WXK_NUMPAD0,"NUMPAD0");
		AddKey(&keys,WXK_NUMPAD1,"NUMPAD1");
		AddKey(&keys,WXK_NUMPAD2,"NUMPAD2");
		AddKey(&keys,WXK_NUMPAD3,"NUMPAD3");
		AddKey(&keys,WXK_NUMPAD4,"NUMPAD4");
		AddKey(&keys,WXK_NUMPAD5,"NUMPAD5");
		AddKey(&keys,WXK_NUMPAD6,"NUMPAD6");
		AddKey(&keys,WXK_NUMPAD7,"NUMPAD7");
		AddKey(&keys,WXK_NUMPAD8,"NUMPAD8");
		AddKey(&keys,WXK_NUMPAD9,"NUMPAD9");
		AddKey(&keys,WXK_MULTIPLY,"MULTIPLY");
		AddKey(&keys,WXK_ADD,"ADD");
		AddKey(&keys,WXK_SEPARATOR,"SEPARATOR");
		AddKey(&keys,WXK_SUBTRACT,"SUBTRACT");
		AddKey(&keys,WXK_DECIMAL,"DECIMAL");
		AddKey(&keys,WXK_DIVIDE,"DIVIDE");
		AddKey(&keys,WXK_F1,"F1");
		AddKey(&keys,WXK_F2,"F2");
		AddKey(&keys,WXK_F3,"F3");
		AddKey(&keys,WXK_F4,"F4");
		AddKey(&keys,WXK_F5,"F5");
		AddKey(&keys,WXK_F6,"F6");
		AddKey(&keys,WXK_F7,"F7");
		AddKey(&keys,WXK_F8,"F8");
		AddKey(&keys,WXK_F9,"F9");
		AddKey(&keys,WXK_F10,"F10");
		AddKey(&keys,WXK_F11,"F11");
		AddKey(&keys,WXK_F12,"F12");
		AddKey(&keys,WXK_F13,"F13");
		AddKey(&keys,WXK_F14,"F14");
		AddKey(&keys,WXK_F15,"F15");
		AddKey(&keys,WXK_F16,"F16");
		AddKey(&keys,WXK_F17,"F17");
		AddKey(&keys,WXK_F18,"F18");
		AddKey(&keys,WXK_F19,"F19");
		AddKey(&keys,WXK_F20,"F20");
		AddKey(&keys,WXK_F21,"F21");
		AddKey(&keys,WXK_F22,"F22");
		AddKey(&keys,WXK_F23,"F23");
		AddKey(&keys,WXK_F24,"F24");
		AddKey(&keys,WXK_NUMLOCK,"NUMLOCK");
		AddKey(&keys,WXK_SCROLL,"SCROLL");
		AddKey(&keys,WXK_PAGEUP,"PAGEUP");
		AddKey(&keys,WXK_PAGEDOWN,"PAGEDOWN");
		AddKey(&keys,WXK_NUMPAD_SPACE,"NUMPAD_SPACE");
		AddKey(&keys,WXK_NUMPAD_TAB,"NUMPAD_TAB");
		AddKey(&keys,WXK_NUMPAD_ENTER,"NUMPAD_ENTER");
		AddKey(&keys,WXK_NUMPAD_F1,"NUMPAD_F1");
		AddKey(&keys,WXK_NUMPAD_F2,"NUMPAD_F2");
		AddKey(&keys,WXK_NUMPAD_F3,"NUMPAD_F3");
		AddKey(&keys,WXK_NUMPAD_F4,"NUMPAD_F4");
		AddKey(&keys,WXK_NUMPAD_HOME,"NUMPAD_HOME");
		AddKey(&keys,WXK_NUMPAD_LEFT,"NUMPAD_LEFT");
		AddKey(&keys,WXK_NUMPAD_UP,"NUMPAD_UP");
		AddKey(&keys,WXK_NUMPAD_RIGHT,"NUMPAD_RIGHT");
		AddKey(&keys,WXK_NUMPAD_DOWN,"NUMPAD_DOWN");
		AddKey(&keys,WXK_NUMPAD_PRIOR,"NUMPAD_PRIOR");
		AddKey(&keys,WXK_NUMPAD_PAGEUP,"NUMPAD_PAGEUP");
		AddKey(&keys,WXK_NUMPAD_NEXT,"NUMPAD_NEXT");
		AddKey(&keys,WXK_NUMPAD_PAGEDOWN,"NUMPAD_PAGEDOWN");
		AddKey(&keys,WXK_NUMPAD_END,"NUMPAD_END");
		AddKey(&keys,WXK_NUMPAD_BEGIN,"NUMPAD_BEGIN");
		AddKey(&keys,WXK_NUMPAD_INSERT,"NUMPAD_INSERT");
		AddKey(&keys,WXK_NUMPAD_DELETE,"NUMPAD_DELETE");
		AddKey(&keys,WXK_NUMPAD_EQUAL,"NUMPAD_EQUAL");
		AddKey(&keys,WXK_NUMPAD_MULTIPLY,"NUMPAD_MULTIPLY");
		AddKey(&keys,WXK_NUMPAD_ADD,"NUMPAD_ADD");
		AddKey(&keys,WXK_NUMPAD_SEPARATOR,"NUMPAD_SEPARATOR");
		AddKey(&keys,WXK_NUMPAD_SUBTRACT,"NUMPAD_SUBTRACT");
		AddKey(&keys,WXK_NUMPAD_DECIMAL,"NUMPAD_DECIMAL");
		AddKey(&keys,WXK_NUMPAD_DIVIDE,"NUMPAD_DIVIDE");
		AddKey(&keys,WXK_WINDOWS_LEFT,"WINDOWS_LEFT");
		AddKey(&keys,WXK_WINDOWS_RIGHT,"WINDOWS_RIGHT");
		AddKey(&keys,WXK_WINDOWS_MENU ,"WINDOWS_MENU ");
		AddKey(&keys,WXK_COMMAND,"COMMAND");
		AddKey(&keys,WXK_SPECIAL1,"SPECIAL1");
		AddKey(&keys,WXK_SPECIAL2,"SPECIAL2");
		AddKey(&keys,WXK_SPECIAL3,"SPECIAL3");
		AddKey(&keys,WXK_SPECIAL4,"SPECIAL4");
		AddKey(&keys,WXK_SPECIAL5,"SPECIAL5");
		AddKey(&keys,WXK_SPECIAL6,"SPECIAL6");
		AddKey(&keys,WXK_SPECIAL7,"SPECIAL7");
		AddKey(&keys,WXK_SPECIAL8,"SPECIAL8");
		AddKey(&keys,WXK_SPECIAL9,"SPECIAL9");
		AddKey(&keys,WXK_SPECIAL10,"SPECIAL10");
		AddKey(&keys,WXK_SPECIAL11,"SPECIAL11");
		AddKey(&keys,WXK_SPECIAL12,"SPECIAL12");
		AddKey(&keys,WXK_SPECIAL13,"SPECIAL13");
		AddKey(&keys,WXK_SPECIAL14,"SPECIAL14");
		AddKey(&keys,WXK_SPECIAL15,"SPECIAL15");
		AddKey(&keys,WXK_SPECIAL16,"SPECIAL16");
		AddKey(&keys,WXK_SPECIAL17,"SPECIAL17");
		AddKey(&keys,WXK_SPECIAL18,"SPECIAL18");
		AddKey(&keys,WXK_SPECIAL19,"SPECIAL19");
		AddKey(&keys,WXK_SPECIAL20,"SPECIAL20");

		made_keys=true;
	}

	return keys;
}


long mbGetWxKeyCodeFromName(const wxString &name) {
	const Keys &keys=GetKeys();
	for(unsigned i=0;i<keys.size();++i) {
		if(keys[i].second==name) {
			return long(i);
		}
	}

	return HostInp_bad_key;//compatibility fudge
}

wxString mbGetKeyNameFromWxKeyCode(long code) {
	const Keys &keys=GetKeys();
	for(unsigned i=0;i<keys.size();++i) {
		if(keys[i].first==code) {
			return keys[i].second;
		}
	}

	return "";
}

unsigned mbGetNumWxKeyCodes() {
	return unsigned(GetKeys().size());
}

#endif

