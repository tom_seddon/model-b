#include "pch.h"
#include "mbJoysticksConfigDialog.h"

static const wxString axis_names[]={
	"X Axis",
	"Y Axis",
	"Z Axis",
	"X Rotation",
	"Y Rotation",
	"Z Rotation",
	"None",
};
static const int num_axis_names=sizeof axis_names/sizeof axis_names[0];

static wxComboBox *NewAxesDropdown(wxWindow *parent,wxWindowID id) {
	wxComboBox *combo=new wxComboBox(parent,id,"",wxDefaultPosition,wxDefaultSize,num_axis_names,axis_names,
		wxCB_DROPDOWN|wxCB_READONLY);
	return combo;
}

static wxComboBox *NewButtonsDropdown(wxWindow *parent,wxWindowID id) {
	wxString button_names[33];

	button_names[0]="None";
	for(int i=0;i<HostInp_num_joystick_buttons;++i) {
		button_names[1+i]=wxString::Format("Button %d",i);
	}
	wxComboBox *combo=new wxComboBox(parent,id,"",wxDefaultPosition,wxDefaultSize,33,button_names,
		wxCB_DROPDOWN|wxCB_READONLY);
	return combo;
}

mbJoysticksConfigDialog::StickUi::StickUi():
enabled(0),
joystick(0),
x(0),
y(0),
fire(0)
{
}

mbJoysticksConfigDialog::mbJoysticksConfigDialog(wxWindow *parent,const mbJoysticksConfig &cfg):
wxDialog(parent,-1,"Joysticks",wxDefaultPosition,wxDefaultSize,wxDEFAULT_DIALOG_STYLE),//|wxRESIZE_BORDER),
cfg_(cfg),
ok_(false)
{
	hostinp_=HostInp_Start(this->GetHandle());

	if(hostinp_) {
		int i;

//		names_.push_back("(No joystick)");
		for(i=0;i<HostInp_NumJoysticks(hostinp_);++i) {
			names_.push_back(HostInp_JoystickName(hostinp_,i));
			wxLogDebug("Joystick %d: %s\n",i,HostInp_JoystickName(hostinp_,i));
		}

		wxBoxSizer *joy_box_szs[2];

		for(i=0;i<2;++i) {
			StickUi *ui=&stick_uis_[i];

			wxStaticBox *joy_box=new wxStaticBox(this,-1,wxString::Format("Joystick %u",i));

			ui->enabled=new wxCheckBox(this,-1,"Enabled");

			wxStaticText *joy_joy_text=new wxStaticText(this,-1,"Joystick");
			ui->joystick=new wxComboBox(this,-1,"",wxDefaultPosition,wxDefaultSize,0,0,
				wxCB_DROPDOWN|wxCB_READONLY);

			wxStaticText *joy_x_text=new wxStaticText(this,-1,"Joystick X");
			ui->x=NewAxesDropdown(this,-1);

			wxStaticText *joy_y_text=new wxStaticText(this,-1,"Joystick Y");
			ui->y=NewAxesDropdown(this,-1);

			wxStaticText *joy_button_text=new wxStaticText(this,-1,"Joystick fire");
			ui->fire=NewButtonsDropdown(this,-1);

			joy_box_szs[i]=new wxStaticBoxSizer(joy_box,wxVERTICAL);

			wxFlexGridSizer *joy_flex_sz=new wxFlexGridSizer(2,2,2);
			joy_flex_sz->AddGrowableCol(1);
			joy_flex_sz->Add(joy_joy_text,0,wxALL|wxALIGN_CENTRE_VERTICAL,2);
			joy_flex_sz->Add(ui->joystick,1,wxGROW|wxALL,2);
			joy_flex_sz->Add(joy_x_text,0,wxALL|wxALIGN_CENTRE_VERTICAL,2);
			joy_flex_sz->Add(ui->x,1,wxGROW|wxALL,2);
			joy_flex_sz->Add(joy_y_text,0,wxALL|wxALIGN_CENTRE_VERTICAL,2);
			joy_flex_sz->Add(ui->y,1,wxGROW|wxALL,2);
			joy_flex_sz->Add(joy_button_text,0,wxALL|wxALIGN_CENTRE_VERTICAL,2);
			joy_flex_sz->Add(ui->fire,1,wxGROW|wxALL,2);
			//joy_box_szs[i]->Layout();

			joy_box_szs[i]->Add(ui->enabled,0,wxGROW|wxALL,2);
			joy_box_szs[i]->Add(joy_flex_sz,1,wxGROW|wxALL,2);
			joy_box_szs[i]->Layout();
		}

		wxSizer *btns_sz=this->CreateButtonSizer(wxOK|wxCANCEL);

		wxBoxSizer *all_sz=new wxBoxSizer(wxVERTICAL);
		all_sz->Add(joy_box_szs[0],0,wxGROW|wxALL,2);
		all_sz->Add(joy_box_szs[1],0,wxGROW|wxALL,2);
		all_sz->Add(btns_sz,0,wxALL|wxALIGN_RIGHT,2);

		this->SetSizer(all_sz);
		all_sz->SetSizeHints(this);

		this->RefreshDialog();

		ok_=true;
	}
}

mbJoysticksConfigDialog::~mbJoysticksConfigDialog() {
	if(hostinp_) {
		HostInp_Stop(hostinp_);
	}
}

void mbJoysticksConfigDialog::GetResult(mbJoysticksConfig *cfg) {
	*cfg=cfg_;
}

void mbJoysticksConfigDialog::RefreshDialog() {
	static const char *const no_joystick="(No joystick)";

	for(unsigned i=0;i<2;++i) {
		const StickUi *ui=&stick_uis_[i];
		const mbJoysticksConfig::BeebStick *stick=&cfg_.joysticks[i];

		ui->enabled->SetValue(stick->enabled);

		ui->joystick->Clear();
		ui->joystick->Append(no_joystick);//index 0 -- it's special
		for(unsigned j=0;j<names_.size();++j) {
			ui->joystick->Append(names_[j]);
			//yuck... but what can you do?
			ui->joystick->SetClientData(ui->joystick->GetCount()-1,this);//just any pointer that's not NULL
		}

		int index=0;
		if(!stick->device_name.empty()) {
			unsigned j;
			for(j=0;j<names_.size();++j) {
				if(names_[j].CmpNoCase(stick->device_name.c_str())==0) {
					break;
				}
			}
			if(j<names_.size()) {
				index=int(j)+1;
			} else {
				//We must add another entry, with "(Not found) <name>", and set the selection
				//to that.
				ui->joystick->Append(wxString("(Not found) ")+stick->device_name.c_str());
				ui->joystick->SetClientData(ui->joystick->GetCount()-1,0);//gcc won't have "reinterpret_cast<void *>(false));"
				index=ui->joystick->GetCount()-1;
			}
		}

		ui->joystick->SetSelection(index);
		wxLogDebug("stick %d: Selected %s\n",i,ui->joystick->GetString(ui->joystick->GetSelection()).c_str());

//		if(stick->joystick_index>=0&&stick->joystick_index<HostInp_NumJoysticks(hostinp_)) {
//			ui->joystick->SetSelection(1+stick->joystick_index);
//		} else {
//			ui->joystick->SetSelection(0);
//		}
		ui->x->SetSelection(stick->x_axis);
		ui->y->SetSelection(stick->y_axis);
		if(stick->button>=0&&stick->button<HostInp_num_joystick_buttons) {
			ui->fire->SetSelection(1+stick->button);
		} else {
			ui->fire->SetSelection(0);
		}
	}
}

void mbJoysticksConfigDialog::OnOk(wxCommandEvent &event) {
	for(unsigned i=0;i<2;++i) {
		const StickUi *ui=&stick_uis_[i];
		mbJoysticksConfig::BeebStick *stick=&cfg_.joysticks[i];

		stick->enabled=ui->enabled->GetValue();
		//stick->joystick_index=ui->joystick->GetSelection()-1;
		int index=ui->joystick->GetSelection();
		if(index==0) {
			stick->device_name="";
		} else {
			void *data=ui->joystick->GetClientData(index);
			bool present=!!data;

			if(!present) {
				//Leave the name as it was.
			} else {
				stick->device_name=ui->joystick->GetString(index);
			}
		}

		stick->x_axis=mbJoysticksConfig::Axis(ui->x->GetSelection());
		stick->y_axis=mbJoysticksConfig::Axis(ui->y->GetSelection());
		stick->button=ui->fire->GetSelection()-1;
	}

	event.Skip();
}

//Joystick X
//	X Axis:		[PC Joystick Axis] <Set>
//	Y Axis:		[PC Joystick Axis] <Set>
//	

BEGIN_EVENT_TABLE(mbJoysticksConfigDialog,wxDialog)
	EVT_BUTTON(wxID_OK,mbJoysticksConfigDialog::OnOk)
END_EVENT_TABLE()
