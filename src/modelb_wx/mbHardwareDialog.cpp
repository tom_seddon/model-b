#include "pch.h"
#include "mbHardwareDialog.h"
#include "mbMisc.h"

const wxWindowID id_fsel=mbNewId();
const wxWindowID id_interface_radio=mbNewId();
const wxWindowID id_model_radio=mbNewId();

mbHardwareDialog::BaseModel::BaseModel():
base_model(0),
rom_set(0),
os_file_name(0),
select_file(0)
{
}

mbHardwareDialog::Model::Model():
base_model(0),
model(0),
radio(0)
{
}

mbHardwareDialog::BaseModelRom::BaseModelRom():
file_name(0),
select_file(0),
slot(-1)
{
}

mbHardwareDialog::Interface::Interface():
iface(0),
radio(0),
file_name(0),
select_file(0)
{
}

void mbHardwareDialog::Interface::Enable(wxControl *w,bool enabled) {
	if(w) {
		w->Enable(enabled);
	}
}

void mbHardwareDialog::Interface::SetEnabled(bool enabled) {
	this->Enable(this->radio,enabled);
	this->Enable(this->file_name,enabled);
	this->Enable(this->select_file,enabled);
}

//wxBoxSizer *mbHardwareDialog::CreateTextCtrlFsel(const wxString &caption,wxTextCtrl **text,wxButton **button) {
//	wxBoxSizer *sz=new wxBoxSizer(wxHORIZONTAL);
//
//	*text=new wxTextCtrl(this,-1,"");
//	*button=new wxButton(this,id_fsel,"...",wxDefaultPosition,wxDefaultSize,wxBU_EXACTFIT);
//
//	sz->Add(new wxStaticText(this,-1,caption),0,wxLEFT|wxRIGHT|wxALIGN_CENTRE_VERTICAL,2);
//	sz->Add(*text,1,wxLEFT|wxRIGHT,2);
//	sz->Add(*button,0,wxLEFT|wxRIGHT,2);
//
//	return sz;
//}

void mbHardwareDialog::AddTextCtrlFsel(wxFlexGridSizer *sz,const wxString &caption,wxTextCtrl **text,wxButton **button) {
	sz->Add(new wxStaticText(this,-1,caption),0,wxLEFT|wxRIGHT|wxALIGN_CENTRE_VERTICAL,2);

	*text=new wxTextCtrl(this,-1,"");
	sz->Add(*text,1,wxLEFT|wxRIGHT|wxEXPAND,2);

	*button=new wxButton(this,id_fsel,"...",wxDefaultPosition,wxDefaultSize,wxBU_EXACTFIT);
	sz->Add(*button,0,wxLEFT|wxRIGHT,2);
}

mbHardwareDialog::mbHardwareDialog(wxWindow *parent,const mbMiscConfig &cfg,const mbRomsConfig &roms_cfg):
wxDialog(parent,-1,"Hardware",wxDefaultPosition,wxDefaultSize,wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER),
cfg_(cfg),
roms_cfg_(roms_cfg),
interfaces_box_(0)
{
	static const int border_size=6;

	//models
	std::vector<const mbBaseModel *> bms;
	mbModelGetAllBaseModels(&bms);

	std::vector<const mbModel *> models;
	mbModelGetAll(&models);

	wxStaticBox *models_box=new wxStaticBox(this,-1,"Model");
	wxStaticBoxSizer *models_box_sz=new wxStaticBoxSizer(models_box,wxVERTICAL);

	for(unsigned bm_idx=0;bm_idx<bms.size();++bm_idx) {
		int first_model_idx=-1;
		for(unsigned model_idx=0;model_idx<models.size();++model_idx) {
			if(models[model_idx]->base_model==bms[bm_idx]) {
				Model *new_model=&*models_.insert(models_.end(),Model());
				BaseModel *new_bm=0;
				if(first_model_idx<0) {
					new_bm=&*base_models_.insert(base_models_.end(),BaseModel());
					new_bm->base_model=bms[bm_idx];
					new_bm->rom_set=&roms_cfg_.rom_sets[new_bm->base_model->short_name];
				}

				new_model->base_model=&base_models_.back();
				new_model->model=models[model_idx];
				wxString caption=wxString(new_model->model->long_name)+" ("+new_model->base_model->base_model->long_name+")";
				new_model->radio=new wxRadioButton(this,id_model_radio,caption,wxDefaultPosition,wxDefaultSize,wxRB_SINGLE);
				
				models_box_sz->Add(new_model->radio,0,wxALL,4);

				if(first_model_idx>=0) {
					wxString caption=wxString::Format("(OS ROM(s) as %s)",models_[first_model_idx].model->long_name);
					models_box_sz->Add(new wxStaticText(this,-1,caption),0,wxGROW|wxALL,4);
				} else {
					wxFlexGridSizer *roms_sz=new wxFlexGridSizer(3);
					roms_sz->AddGrowableCol(1);

					this->AddTextCtrlFsel(roms_sz,"OS:",&new_bm->os_file_name,&new_bm->select_file);

//					wxBoxSizer *os_sz=this->CreateTextCtrlFsel(
//					models_box_sz->Add(os_sz,0,wxGROW|wxALL,4);

					for(int slot=0;slot<16;++slot) {
						if(!(new_model->model->base_model->b_rom_mask&(1<<slot))) {
							BaseModelRom *bmr=&*new_bm->model_roms.insert(new_bm->model_roms.end(),BaseModelRom());
							bmr->slot=slot;
							this->AddTextCtrlFsel(roms_sz,wxString::Format("%X",slot),&bmr->file_name,&bmr->select_file);
//							wxBoxSizer *sz=this->CreateTextCtrlFsel(wxString::Format("%X",slot),&bmr->file_name,
//								&bmr->select_file);
//							models_box_sz->Add(sz,0,wxGROW|wxALL,4);
						}
					}

					models_box_sz->Add(roms_sz,0,wxGROW|wxALL,4);
					first_model_idx=int(models_.size()-1);
				}
			}
		}
	}

	//disc interfaces
	std::vector<const mbDiscInterface *> ifs;
	mbDiscInterfaceGetAll(&ifs);
	interfaces_box_=new wxStaticBox(this,-1,"Disc interface");
	wxStaticBoxSizer *interfaces_box_sz=new wxStaticBoxSizer(interfaces_box_,wxVERTICAL);
	for(unsigned if_idx=0;if_idx<ifs.size();++if_idx) {
		Interface *iface=&*interfaces_.insert(interfaces_.end(),Interface());

		iface->iface=ifs[if_idx];
		
		iface->radio=new wxRadioButton(this,id_interface_radio,iface->iface->long_name,wxDefaultPosition,wxDefaultSize,
			wxRB_SINGLE);
		interfaces_box_sz->Add(iface->radio,0,wxALL,4);

		if(iface->iface->iface) {//hmmm
			iface->select_file=new wxButton(this,id_fsel,"...",wxDefaultPosition,wxDefaultSize,wxBU_EXACTFIT);
			iface->file_name=new wxTextCtrl(this,-1,"");

			wxBoxSizer *sz=new wxBoxSizer(wxHORIZONTAL);
			sz->Add(new wxStaticText(this,-1,"Filing system ROM:"),0,wxLEFT|wxRIGHT|wxALIGN_CENTRE_VERTICAL,2);
			sz->Add(iface->file_name,1,wxLEFT|wxRIGHT,2);
			sz->Add(iface->select_file,0,wxLEFT|wxRIGHT,2);
			
			interfaces_box_sz->Add(sz,0,wxGROW|wxALL,4);
		}
	}

	//bloody thing won't lay itself out properly.
//	wxStaticBox *models_box=new wxStaticBox(this,-1,"Model");
//	wxStaticBoxSizer *models_box_sz=new wxStaticBoxSizer(models_box,wxVERTICAL);
//	b_=new wxRadioButton(models_box,-1,"Model B");
//	bplus_=new wxRadioButton(models_box,-1,"Model B+");
//	models_box_sz->Add(b_,0,wxALL,4);
//	models_box_sz->Add(bplus_,0,wxALL,4);
	

	wxBoxSizer *btns_sz;
	wxButton *ok=new wxButton(this,wxID_OK,"Apply and hard reset");
	wxButton *cancel=new wxButton(this,wxID_CANCEL,"Cancel");
	ok->SetDefault();
	btns_sz=new wxBoxSizer(wxHORIZONTAL);
	btns_sz->Add(ok,0,wxALL|wxALIGN_RIGHT,border_size);
	btns_sz->Add(cancel,0,wxALL|wxALIGN_RIGHT,border_size);

	wxBoxSizer *top_sz=new wxBoxSizer(wxHORIZONTAL);
	top_sz->Add(models_box_sz,1,wxALL,border_size);
	top_sz->Add(interfaces_box_sz,1,wxALL,border_size);

	wxBoxSizer *all_sz=new wxBoxSizer(wxVERTICAL);
	all_sz->Add(top_sz,1,wxGROW|wxALL,border_size);
//	all_sz->Add(models_box_sz,0,wxGROW|wxALL,border_size);
	all_sz->Add(btns_sz,0,wxALIGN_RIGHT);

	this->SetSizer(all_sz);
	all_sz->SetSizeHints(this);

	this->RefreshDialog();

	mbSetWindowRect(this,cfg_.hardware_dlg_rect);
}

void mbHardwareDialog::GetResult(mbMiscConfig *cfg,mbRomsConfig *roms_cfg) const {
	*cfg=cfg_;
	*roms_cfg=roms_cfg_;
}

void mbHardwareDialog::GetFselDetails(wxObject *src,wxTextCtrl **ctrl,std::string **string,wxString *caption) {
	BASSERT(src);
	BASSERT(ctrl);
	BASSERT(string);
	BASSERT(caption);

	unsigned i;
	
	for(i=0;i<interfaces_.size();++i) {
		Interface *f=&interfaces_[i];
		
		if(src==f->select_file) {
			*ctrl=f->file_name;
			*string=&roms_cfg_.fs_roms[f->iface->short_name];
			*caption=wxString::Format("Select ROM for %s",f->iface->long_name);
			return;
		}
	}

	for(std::list<BaseModel>::iterator bm=base_models_.begin();bm!=base_models_.end();++bm) {
		if(src==bm->select_file) {
			*ctrl=bm->os_file_name;
			*string=&bm->rom_set->os;
			*caption=wxString::Format("Select OS for %s",bm->base_model->long_name);
			return;
		}

		for(unsigned j=0;j<bm->model_roms.size();++j) {
			BaseModelRom *mr=&bm->model_roms[j];

			if(src==mr->select_file) {
				*ctrl=mr->file_name;
				*string=&bm->rom_set->roms[mr->slot].filename;
				*caption=wxString::Format("Select ROM %d for %s",mr->slot,bm->base_model->long_name);
				return;
			}
		}
	}
	BASSERT(false);
}

void mbHardwareDialog::OnFsel(wxCommandEvent &event) {
	wxTextCtrl *ctrl;
	std::string *string;
	wxString caption;

	this->GetFselDetails(event.GetEventObject(),&ctrl,&string,&caption);

	wxString old_dir,old_name;
	wxFileName old=wxFileName::FileName(string->c_str());
	if(old.IsOk()) {
		old_name=old.GetFullName();
		old_dir=old.GetPath();
	}

	wxFileDialog fd(this,caption,old_dir,old_name,"All files|*.*",wxFD_OPEN);
	if(fd.ShowModal()==wxID_OK) {
		*string=fd.GetPath();
		this->RefreshDialog();
	}
}

void mbHardwareDialog::RefreshDialog() {
	wxLogDebug("%s starts:",__FUNCTION__);
	const Model *selected=0;
	for(unsigned i=0;i<models_.size();++i) {
		const Model *m=&models_[i];

		if(strcmp(cfg_.model.c_str(),m->model->short_name)==0) {
			m->radio->SetValue(true);
			selected=m;
		} else {
			m->radio->SetValue(false);
		}
	}
	wxASSERT(selected);

	for(std::list<BaseModel>::const_iterator bm=base_models_.begin();bm!=base_models_.end();++bm) {
		if(bm->os_file_name) {
			bm->os_file_name->SetValue(bm->rom_set->os.c_str());
		}
		for(unsigned i=0;i<bm->model_roms.size();++i) {
			const BaseModelRom *bmr=&bm->model_roms[i];

			if(bmr->file_name) {
				bmr->file_name->SetValue(bm->rom_set->roms[bmr->slot].filename.c_str());
			}
		}
	}

	bool radio_set=false;
	int first_valid=-1;
	for(unsigned i=0;i<interfaces_.size();++i) {
		Interface *f=&interfaces_[i];

		if(f->file_name) {
			f->file_name->SetValue(roms_cfg_.fs_roms[f->iface->short_name].c_str());
		}

		f->radio->SetValue(false);
		wxLogDebug("    %s:",f->iface->short_name);
		if(selected->base_model->base_model->IsValidDiscInterface(f->iface->short_name)) {
			wxLogDebug("        valid.");
			if(first_valid<0) {
				first_valid=int(i);
			}
			f->SetEnabled(true);
			if(selected->base_model->rom_set->disc_interface==f->iface->short_name) {
				f->radio->SetValue(true);
				radio_set=true;
				wxLogDebug("        selected.");
			}
			if(selected->base_model->base_model->flags&mbBaseModel::M_NO_FSSLOT) {
				if(f->file_name) {
					f->file_name->Enable(false);
				}

				if(f->select_file) {
					f->select_file->Enable(false);
				}
			}
		} else {
			wxLogDebug("        selected.");
			f->SetEnabled(false);
			f->radio->SetValue(false);
		}
	}
	if(!radio_set&&first_valid>=0) {
		interfaces_[first_valid].radio->SetValue(true);
		wxLogDebug("        had to set to %s.",interfaces_[first_valid].iface->short_name);
	}

	interfaces_box_->SetLabel(wxString("Disc interface (")+selected->base_model->base_model->long_name+")");

	wxLogDebug("%s ended.",__FUNCTION__);
}

void mbHardwareDialog::OnInterfaceRadio(wxCommandEvent &event) {
	for(unsigned i=0;i<interfaces_.size();++i) {
		const Interface *f=&interfaces_[i];

		if(event.GetEventObject()==f->radio) {
			for(unsigned j=0;j<models_.size();++j) {
				if(models_[j].model->short_name==cfg_.model) {
					mbRomsConfig::BaseModelRomSet *bmrs=&roms_cfg_.rom_sets[models_[j].model->base_model->short_name];
					bmrs->disc_interface=f->iface->short_name;
					bmrs->roms[bmrs->fs_slot].filename=roms_cfg_.fs_roms[f->iface->short_name];
					this->RefreshDialog();
					return;
				}
			}
		}
	}
}

void mbHardwareDialog::OnModelRadio(wxCommandEvent &event) {
	for(unsigned i=0;i<models_.size();++i) {
		const Model *m=&models_[i];

		if(event.GetEventObject()==m->radio) {

			cfg_.model=m->model->short_name;
			this->RefreshDialog();
			return;
		}
	}
}

void mbHardwareDialog::OnOk(wxCommandEvent &event) {
//	if(roms_cfg_.fs_slot>=0) {
//		const std::string &rom=roms_cfg_.fs_roms[cfg_.disc_interface];
//
//		roms_cfg_.roms[roms_cfg_.fs_slot].filename=rom;
//	}
	cfg_.hardware_dlg_rect=this->GetRect();
	event.Skip();
}

BEGIN_EVENT_TABLE(mbHardwareDialog,wxDialog)
	EVT_BUTTON(id_fsel,mbHardwareDialog::OnFsel)
	EVT_RADIOBUTTON(id_interface_radio,mbHardwareDialog::OnInterfaceRadio)
	EVT_RADIOBUTTON(id_model_radio,mbHardwareDialog::OnModelRadio)
	EVT_BUTTON(wxID_OK,mbHardwareDialog::OnOk)
END_EVENT_TABLE()
