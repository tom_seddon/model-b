#include "pch.h"
#include "mbKeys.h"

//TODO This is suspiciously to both bbcKeys.cpp and HostWin32.cpp...!!

struct KeyAndName {
	int code;
	const char *name;
};

static const KeyAndName keys[]={
	{0,0},
};

int mbCodeFromKeyName(const char *name) {
	int code=bbcCodeFromKeyName(name);
	if(code==bbcKEY_NONE) {
		for(unsigned i=0;keys[i].name;++i) {
			if(stricmp(name,keys[i].name)==0) {
				code=keys[i].code;
				break;
			}
		}
	}
	return code;
}

wxString mbKeyNameFromCode(int code) {
	wxString name=bbcKeyNameFromCode(bbcKey(code));

	if(name.empty()) {
		for(unsigned i=0;keys[i].name;++i) {
			if(keys[i].code==code) {
				name=keys[i].name;
				break;
			}
		}
	}
	return name;
}
