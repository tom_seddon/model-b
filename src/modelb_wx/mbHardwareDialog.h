#ifndef MBHARDWAREDIALOG_H_
#define	MBHARDWAREDIALOG_H_

#include "mbConfig.h"
#include "mbDiscInterface.h"
#include "mbModel.h"
#include <list>

class mbHardwareDialog:
public wxDialog
{
public:
	mbHardwareDialog(wxWindow *parent,const mbMiscConfig &cfg,const mbRomsConfig &roms_cfg);

	void GetResult(mbMiscConfig *cfg,mbRomsConfig *roms_cfg) const;
protected:
private:
	mbMiscConfig cfg_;
	mbRomsConfig roms_cfg_;
	
	struct BaseModelRom {
		wxTextCtrl *file_name;
		wxButton *select_file;
		int slot;

		BaseModelRom();
	};

	struct BaseModel {
		const mbBaseModel *base_model;
		std::vector<BaseModelRom> model_roms;
		mbRomsConfig::BaseModelRomSet *rom_set;
		wxTextCtrl *os_file_name;
		wxButton *select_file;

		BaseModel();
	};
	std::list<BaseModel> base_models_;

	struct Model {
		const BaseModel *base_model;
		const mbModel *model;
		wxRadioButton *radio;

		Model();
	};

	std::vector<Model> models_;

	struct Interface {
		const mbDiscInterface *iface;

		wxRadioButton *radio;
		wxTextCtrl *file_name;
		wxButton *select_file;

		Interface();
		void SetEnabled(bool enabled);
	private:
		void Enable(wxControl *w,bool enabled);
	};

	std::vector<Interface> interfaces_;
	wxStaticBox *interfaces_box_;

	void OnFsel(wxCommandEvent &event);
	void OnInterfaceRadio(wxCommandEvent &event);
	void OnModelRadio(wxCommandEvent &event);
	void OnOk(wxCommandEvent &event);

	void RefreshDialog();
//	wxBoxSizer *CreateTextCtrlFsel(const wxString &caption,wxTextCtrl **text,wxButton **button);
	void AddTextCtrlFsel(wxFlexGridSizer *sz,const wxString &caption,wxTextCtrl **text,wxButton **button);
	void GetFselDetails(wxObject *src,wxTextCtrl **ctrl,std::string **string,wxString *caption);

	DECLARE_EVENT_TABLE()
};

#endif
