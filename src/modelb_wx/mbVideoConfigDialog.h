#ifndef MBVIDEOCONFIGDIALOG_H_
#define MBVIDEOCONFIGDIALOG_H_

#include "mbConfig.h"
#include <HostGfx.h>

class mbVideoConfigDialog:
public wxDialog
{
public:
	mbVideoConfigDialog(wxWindow *parent,const mbVideoConfig &cfg,HostGfxState *state);

	void GetResult(mbVideoConfig *cfg);
protected:
private:
	mbVideoConfig cfg_;
	
	wxComboBox *adapters_cb_;
	wxComboBox *hz_cb_;
	const HostGfx_Adapter *const *adapters_;
	int num_adapters_;
	
	//std::vector<int> freqs_;

	wxString width_,height_;
	wxString cleanup_freq_;
	wxString display_freq_;

	wxBoxSizer *NumberEntryBox(const wxString &caption,wxString *var);
	wxStaticBoxSizer *StaticBox(const wxString &caption,const std::vector<wxObject *> &contents);
	void OnOk(wxCommandEvent &event);
	void OnAdaptersComboBox(wxCommandEvent &event);
	void RefreshDialog();

	DECLARE_EVENT_TABLE()
};

#endif
