#ifndef mbJoysticksConfigDialog_h_
#define mbJoysticksConfigDialog_h_

#include "Host.h"
#include "mbConfig.h"

class mbJoysticksConfigDialog:
public wxDialog
{
public:
	mbJoysticksConfigDialog(wxWindow *parent,const mbJoysticksConfig &cfg);
	~mbJoysticksConfigDialog();

	bool IsOk() const;
	void GetResult(mbJoysticksConfig *cfg);
protected:
private:
	mbJoysticksConfig cfg_;
	HostInpState *hostinp_;
	bool ok_;
	std::vector<wxString> names_;

	struct StickUi {
		wxCheckBox *enabled;
		wxComboBox *joystick;
		wxComboBox *x,*y,*fire;

		StickUi();
	};
	StickUi stick_uis_[2];

	void RefreshDialog();

	void OnOk(wxCommandEvent &event);

	DECLARE_EVENT_TABLE()
};

inline bool mbJoysticksConfigDialog::IsOk() const {
	return ok_;
}


#endif
