#include "pch.h"
#include "mbTestMode.h"
#ifdef TEST_MODE
#include <t65.h>
#include <bbcSY6502A.h>
#include "mbMisc.h"

// http://www.ffd2.com/fridge/misc/petcom.c
static const wxByte ascii_from_petscii[256] = {
	0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x14,0x09,0x0d,0x11,0x93,0x0a,0x0e,0x0f,
	0x10,0x0b,0x12,0x13,0x08,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,
	0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f,
	0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,0x3c,0x3d,0x3e,0x3f,
	0x40,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x6b,0x6c,0x6d,0x6e,0x6f,
	0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7a,0x5b,0x5c,0x5d,0x5e,0x5f,
	0xc0,0xc1,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xcb,0xcc,0xcd,0xce,0xcf,
	0xd0,0xd1,0xd2,0xd3,0xd4,0xd5,0xd6,0xd7,0xd8,0xd9,0xda,0xdb,0xdc,0xdd,0xde,0xdf,
	0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8a,0x8b,0x8c,0x8d,0x8e,0x8f,
	0x90,0x91,0x92,0x0c,0x94,0x95,0x96,0x97,0x98,0x99,0x9a,0x9b,0x9c,0x9d,0x9e,0x9f,
	0xa0,0xa1,0xa2,0xa3,0xa4,0xa5,0xa6,0xa7,0xa8,0xa9,0xaa,0xab,0xac,0xad,0xae,0xaf,
	0xb0,0xb1,0xb2,0xb3,0xb4,0xb5,0xb6,0xb7,0xb8,0xb9,0xba,0xbb,0xbc,0xbd,0xbe,0xbf,
	0x60,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,0x4c,0x4d,0x4e,0x4f,
	0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5a,0x7b,0x7c,0x7d,0x7e,0x7f,
	0xa0,0xa1,0xa2,0xa3,0xa4,0xa5,0xa6,0xa7,0xa8,0xa9,0xaa,0xab,0xac,0xad,0xae,0xaf,
	0xb0,0xb1,0xb2,0xb3,0xb4,0xb5,0xb6,0xb7,0xb8,0xb9,0xba,0xbb,0xbc,0xbd,0xbe,0xbf
};

//////////////////////////////////////////////////////////////////////////

static void dprintf(const char *fmt,...) {
	va_list v;

	va_start(v,fmt);
	vprintf(fmt,v);
	char tmpbuf[1024];
	_vsnprintf(tmpbuf,sizeof tmpbuf,fmt,v);
	OutputDebugString(tmpbuf);
	va_end(v);
}

//////////////////////////////////////////////////////////////////////////

static const wxString test_suite_path="Z:\\beeb\\6502TestSuite\\testsuite-2.15";

//////////////////////////////////////////////////////////////////////////

struct mbTestModeMachine {
	struct ReadZP {};
	struct WriteZP {};
	struct ReadStack {};
	struct WriteStack {};
	struct FetchInstr {};
	struct FetchAddress {};
	struct ReadOperand {};
	struct WriteOperand {};
	struct ReadDebug {};
	struct WriteDebug {};

	template<typename Policy>
	static FINLINE t65::byte ReadByte(t65::Word addr,const Policy *const=0) {
		return ram[addr.w];
	}

	template<typename Policy>
	static FINLINE void WriteByte(t65::Word addr,t65::byte v,const Policy *const) {
		ram[addr.w]=v;
	}

	typedef t65::State65xx StateType;
	
	static StateType cpustate;
	static int cpucycles;
	static t65::byte ram[65536];

	static void Trap_ffd2();
	static void Trap_e16f();
	static void Trap_ffe4();
	static void Trap_8000();
	static void Trap_a474();

	static void LoadTestSuiteFileAndReset(const wxString &filename);

	static void IrqsReenabled() {
		//don't care.
	}
};

mbTestModeMachine::StateType mbTestModeMachine::cpustate;
int mbTestModeMachine::cpucycles;
t65::byte mbTestModeMachine::ram[65536];

struct mbTestModeConfig {
	typedef mbTestModeMachine MachineType;

	//////////////////////////////////////////////////////////////////////////
	// Memory access -- which form of memory access should be used for each
	// access category?

	// ReadZP: how the processor reads bytes from zero page
	typedef MachineType::ReadZP ReadZP;

	// WriteZP: how the processor writes bytes to zero page
	typedef MachineType::WriteZP WriteZP;

	// ReadStack: how the processor reads bytes from the stack
	typedef MachineType::ReadStack ReadStack;

	// WriteStack: how the processor write bytes to the stack
	typedef MachineType::WriteStack WriteStack;

	// FetchInstr: how the processor reads bytes whilst fetching the bytes
	// that make up an instruction
	typedef MachineType::FetchInstr FetchInstr;

	// FetchAddress: how the processor reads an indirect address from
	// main memory, e.g. during IRQ processing or when doing the indirect
	// memory mode.
	typedef MachineType::FetchAddress FetchAddress;

	// ReadOperand: how the processor reads bytes when reading an
	// instruction's operand
	typedef MachineType::ReadOperand ReadOperand;

	// WriteOperand: how the processor writes bytes when writing an
	// instruction's operand
	typedef MachineType::WriteOperand WriteOperand;

	// ReadDebug: how to read a byte in debug mode (no update hardware
	// or signal interrupts etc.)
	typedef MachineType::ReadDebug ReadDebug;

	// WriteDebug: how to write a byte in debug mode (no update hardware
	// or signal interrupts etc.)
	typedef MachineType::WriteDebug WriteDebug;
};

typedef bbcSY6502A<mbTestModeConfig> mbTestModeCpu;
typedef bbcSY6502AInstructionSet<mbTestModeCpu> mbTestModeInstructionSet;
typedef t65::Sim6502<mbTestModeCpu,mbTestModeInstructionSet> mbTestModeSim;

//////////////////////////////////////////////////////////////////////////

void mbTestModeMachine::Trap_ffd2() {
	ram[0x30c]=0;
	dprintf("%c",ascii_from_petscii[cpustate.a]);

	++cpustate.s.l;
	cpustate.pc.l=ram[cpustate.s.w];
	++cpustate.s.l;
	cpustate.pc.h=ram[cpustate.s.w];
	++cpustate.pc.w;
}

void mbTestModeMachine::Trap_e16f() {
	wxString filename;
	t65::Word src;

	// Get filename
	src.l=ram[0xbb];
	src.h=ram[0xbc];
	for(t65::byte i=0;i<ram[0xb7];++i) {
		filename.append(1,wxChar(ascii_from_petscii[ram[src.w+i]]));
	}

	if(filename.Lower()=="trap17") {
		__asm int 3;
	}

	LoadTestSuiteFileAndReset(filename);
}

void mbTestModeMachine::Trap_ffe4() {
	cpustate.a=3;//that's what it says to do...!

	++cpustate.s.l;
	cpustate.pc.l=ram[0x100+cpustate.s.l];
	++cpustate.s.l;
	cpustate.pc.h=ram[0x100+cpustate.s.l];
}

void mbTestModeMachine::Trap_8000() {
	__asm int 3;
}

void mbTestModeMachine::Trap_a474() {
	__asm int 3;
}

void mbTestModeMachine::LoadTestSuiteFileAndReset(const wxString &name) {
	wxString filename=test_suite_path+"\\bin\\"+name;

	dprintf("[[Loading \"%s\"]]",filename.c_str());

	// Read file contents
	std::vector<wxByte> contents;
	bool ok=mbLoadFile(filename,contents);
	BASSERT(ok);

	// Copy into RAM
	t65::Word dest;
	dest.l=contents[0];
	dest.h=contents[1];
	BASSERT(dest.w+contents.size()-2<65536);
	memcpy(&ram[dest.w],&contents[2],contents.size()-2);

	// Initialize some memory locations:
	//	$0002 = $00
	//	$A002 = $00
	//	$A003 = $80
	//	$FFFE = $48
	//	$FFFF = $FF
	//	$01FE = $FF
	//	$01FF = $7F
	ram[2]=0;
	ram[0xa002]=0;
	ram[0xa003]=0x80;

	ram[0xfffe]=0x48;
	ram[0xffff]=0xff;

	ram[0x1fe]=0xff;
	ram[0x1ff]=0x7f;

	t65::word a=0xff48;

	// Set up the KERNAL "IRQ handler" at $FF48:
	//
	//	FF48  48        PHA
	ram[a++]=0x48;
	//	FF49  8A        TXA
	ram[a++]=0x8a;
	//	FF4A  48        PHA
	ram[a++]=0x48;
	//	FF4B  98        TYA
	ram[a++]=0x98;
	//	FF4C  48        PHA
	ram[a++]=0x48;
	//	FF4D  BA        TSX
	ram[a++]=0xba;
	//	FF4E  BD 04 01  LDA    $0104,X
	ram[a++]=0xbd;
	ram[a++]=0x04;
	ram[a++]=0x01;
	//	FF51  29 10     AND    #$10
	ram[a++]=0x29;
	ram[a++]=0x10;
	//	FF53  F0 03     BEQ    $FF58
	ram[a++]=0xf0;
	ram[a++]=0x03;
	//	FF55  6C 16 03  JMP    ($0316)
	ram[a++]=0x6c;
	ram[a++]=0x16;
	ram[a++]=0x03;
	//	FF58  6C 14 03  JMP    ($0314)
	ram[a++]=0x6c;
	ram[a++]=0x14;
	ram[a++]=0x03;

	BASSERT(a==0xff5b);

	// And carry on.
	cpustate.s.l=0xfd;
	cpustate.p=0x04;
	cpustate.pc.w=0x816;
}

int mbTestModeRun() {
	static mbTestModeMachine machine;

	mbTestModeSim::Init();
	//5/12/2004 -- removing InitialiseCpuState (suspect real 6502 starts up with gibberish)
//	mbTestModeSim::InitialiseCpuState(machine);

	mbTestModeMachine::LoadTestSuiteFileAndReset(" start");

	// Now start running.

	for(;;) {
		switch(mbTestModeMachine::cpustate.pc.w) {
		case 0xffd2:
			mbTestModeMachine::Trap_ffd2();
			break;
		
		case 0xe16f:
			mbTestModeMachine::Trap_e16f();
			break;
		
		case 0xffe4:
			mbTestModeMachine::Trap_ffe4();
			break;
		
		case 0x8000:
			mbTestModeMachine::Trap_8000();
			break;
		
		case 0xa474:
			mbTestModeMachine::Trap_a474();
			break;

		default:
			mbTestModeSim::RunSingleInstruction(machine);
		}
	}
}

#endif//TEST_MODE

