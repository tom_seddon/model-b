#ifndef mbROMSCONFIGDIALOG_H_
#define mbROMSCONFIGDIALOG_H_

#include "mbConfig.h"
#include "mbModel.h"

class wxGenericValidator;

class mbRomsConfigDialog:
public wxDialog
{
public:
	mbRomsConfigDialog(wxWindow *parent,wxWindowID id,mbRomsConfig &cfg);
	~mbRomsConfigDialog();

	void GetResult(mbRomsConfig *cfg) const;
protected:
private:
//	mbRomsConfig::Rom roms_[16];
	mbRomsConfig cfg_;

	struct RomControls {
		int slot;
		wxStaticText *slot_number;
		wxTextCtrl *file_name;
		wxButton *select_file;
		wxCheckBox *is_ram;

//		wxTextValidator *file_name_vd;
//		wxGenericValidator *is_ram_vd;

		RomControls();
#ifdef __WXDEBUG__
		bool AllOk() const;
#endif
	};

	struct BaseModelTab {
		wxWindow *page;
		RomControls rom_controls[16];
		wxRadioButton *rom_is_fs[16];
		const mbBaseModel *base_model;
		mbRomsConfig::BaseModelRomSet *rom_set;

		BaseModelTab();
	};

	std::vector<BaseModelTab> tabs_;

	void CreateRomControls();
	void DeleteRomControls();
	void RefreshDialog();

	//cmds
	void OnFileSelect(wxCommandEvent &event);
	void OnOk(wxCommandEvent &event);
	void OnCancel(wxCommandEvent &event);
	void OnApplyReset(wxCommandEvent &event);
	void OnIsFs(wxCommandEvent &event);
	void DataOk(int flags);

	DECLARE_EVENT_TABLE()
};

#endif
