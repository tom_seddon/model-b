#include "pch.h"
#include "mbConfigFile.h"
#include "mbMisc.h"
#include <limits>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <assert.h>

#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

#define DP (void)
//#define DP printf

mbConfigFile::~mbConfigFile() {
}

template<class T>
bool mbConfigFile::ReadNumberHelper(const char *value,T *dest) {
	if(value) {
		char *ep;
		int64_t tmp=_strtoi64(value,&ep,0);
		if(!ep||*ep==0||isspace(*ep)) {
			if(tmp>=std::numeric_limits<T>::min()&&tmp<=std::numeric_limits<T>::max()) {
				*dest=T(tmp);
				return true;
			}
		}
	}
	return false;
}
const char *mbConfigFile::GetValueByName(const char *section,const char *name) {
	for(int i=this->GetFirstSection();i>=0;i=this->GetNextSection(i)) {
		if(strcmp(this->GetSectionName(i),section)==0) {
			for(int j=this->GetFirstItem(i);j>=0;j=this->GetNextItem(i,j)) {
				if(strcmp(this->GetItemName(i,j),name)==0) {
					return this->GetItemValue(i,j);
				}
			}
		}
	}
	return 0;
}

void mbConfigFile::GetItemLocationByName(const char *section_name,const char *item_name,int *section,int *item,bool insert) {
	*section=-1;
	*item=-1;

	for(int i=this->GetFirstSection();i>=0;i=this->GetNextSection(i)) {
		if(strcmp(this->GetSectionName(i),section_name)==0) {
			*section=i;
			for(int j=this->GetFirstItem(i);j>=0;j=this->GetNextItem(i,j)) {
				if(strcmp(this->GetItemName(i,j),item_name)==0) {
					*item=j;
					return;
				}
			}
		}
	}

	if(insert) {
		// not found
		if(*section<0) {
			// section does not exist.
			*section=this->AppendSection(section_name);
		}

		// item does not exist in *section.
		*item=this->AppendItem(*section,item_name);
	}
}

template<class T>
void mbConfigFile::WriteNumberHelper(const char *section_name,const char *item_name,const char *fmt,
	const T &value)
{
	int section,item;
	this->GetItemLocationByName(section_name,item_name,&section,&item,true);

	char tmp[200];
	_snprintf(tmp,sizeof tmp,fmt,value);
	this->SetItem(section,item,0,tmp);
}

bool mbConfigFile::Read(const char *section,const char *name,bool *dest) {
	const char *value=this->GetValueByName(section,name);
	if(!value) {
		return false;
	} else {
		if(strcmp(value,"1")==0) {
			*dest=true;
		} else {
			*dest=false;
		}
		return true;
	}
}

bool mbConfigFile::Read(const char *section,const char *name,long *dest) {
	return ReadNumberHelper(this->GetValueByName(section,name),dest);
}

bool mbConfigFile::Read(const char *section,const char *name,int *dest) {
	return ReadNumberHelper(this->GetValueByName(section,name),dest);
}

bool mbConfigFile::Read(const char *section,const char *name,std::string *dest) {
	const char *value=this->GetValueByName(section,name);
	if(!value) {
		return false;
	} else {
		*dest=value;
		return true;
	}
}

void mbConfigFile::Write(const char *section_name,const char *item_name,const bool &value) {
	int section,item;
	this->GetItemLocationByName(section_name,item_name,&section,&item,true);
	this->SetItem(section,item,0,value?"1":"0");
}

void mbConfigFile::Write(const char *section,const char *name,const long &value) {
	WriteNumberHelper(section,name,"%ld",value);
}

void mbConfigFile::Write(const char *section,const char *name,const int &value) {
	WriteNumberHelper(section,name,"%d",value);
}

void mbConfigFile::Write(const char *section_name,const char *name,const char *value) {
	int section,item;
	this->GetItemLocationByName(section_name,name,&section,&item,true);
	this->SetItem(section,item,0,value);
}

void mbConfigFile::Write(const char *section,const char *name,const std::string &value) {
	this->Write(section,name,value.c_str());
}

int mbConfigFile::GetSectionByName(const char *section_name) {
	for(int i=this->GetFirstSection();i>=0;i=this->GetNextSection(i)) {
		if(strcmp(this->GetSectionName(i),section_name)==0) {
			return i;
		}
	}
	return -1;
}

void mbConfigFile::DeleteSectionByName(const char *section_name) {
	int i=this->GetSectionByName(section_name);
	if(i>=0) {
		this->DeleteSection(i);
	}
}

void mbConfigFile::DeleteItemByName(const char *section_name,const char *item_name) {
	int section,item;
	this->GetItemLocationByName(section_name,item_name,&section,&item,false);
	if(section>=0&&item>=0) {
		this->DeleteItem(section,item);
	}
}

//////////////////////////////////////////////////////////////////////////
//

static std::string GetEscaped(const std::string &s) {
	std::string r;
	for(unsigned i=0;i<s.length();++i) {
		switch(s[i]) {
		case '\t':
			r+="\\t";
			break;

		case '\n':
			r+="\\n";
			break;

		case '\\':
			r+="\\\\";
			break;

		case '\r':
			r+="\\r";
			break;

		default:
			if(isprint(s[i])) {
				r.append(1,s[i]);
			} else {
				r+=mbStrPrintf("\\x%02X",unsigned(s[i]));
			}
			break;
		}
	}

	return r;
}

static unsigned unhex(char c) {
	if(isdigit(c)) {
		return c-'0';
	} else if(isxdigit(c)) {
		return tolower(c)-'a'+10;
	} else {
		return 0;
	}
}

static std::string GetUnescaped(const std::string &s) {
	unsigned i=0;
	std::string r;
	while(i<s.length()) {
		if(s[i]!='\\') {
			r.append(1,s[i]);
			++i;
		} else {
			++i;
			if(i==r.length()) {
				r+="\\";//erm...
			} else {
				switch(s[i]) {
				case '\\':
					r+="\\";
					++i;
					break;

				case 't':
					r+="\t";
					++i;
					break;

				case 'n':
					r+="\n";
					++i;
					break;

				case 'r':
					r+="\r";
					++i;
					break;

				case 'x':
					// next 2 chars always ignored. invalid? don't care!
					++i;
					if(i+2<=s.length()) {
						unsigned v;
						v=unhex(s[i])<<4;
						v|=unhex(s[i+1]);
						r.append(1,char(v));
						i+=2;
					}
					break;

				default:
					r+="\\";// erm...
					break;
				}
			}
		}
	}

	return r;
}

#ifdef bbcDEBUG_ENABLE
static void EscapedChk(const std::string &s) {
	std::string esc=GetEscaped(s);
	std::string unesc=GetUnescaped(esc);
	BASSERT(unesc==s);
}
#endif

mbIniFile::mbIniFile() {
#ifdef bbcDEBUG_ENABLE
	EscapedChk("xyz\\\t\n\r\x1f");
#endif
}

static void CopyTrimmed(std::string *dest,const std::string &src,std::string::size_type begin,
	std::string::size_type end)
{
	dest->clear();
	while(isspace(src[begin])&&begin<end) {
		++begin;
	}
	if(begin<end) {
		std::string::size_type non_ws_end=begin;
		while(non_ws_end<end&&!isspace(src[non_ws_end])) {
			++non_ws_end;
		}
		*dest=src.substr(begin,non_ws_end-begin);
	}
}

bool mbIniFile::Load(const char *filename) {
	bool ok=this->LoadInternal(filename);
	if(!ok) {
		lines_.clear();
		sections_.clear();
	}
	return ok;
}

bool mbIniFile::LoadInternal(const char *filename) {
	std::ifstream input(filename);
	if(!input) {
		return false;
	}

	while(!!input) {
		std::string text;
		Line line;

		std::getline(input,text);
		if(input.rdstate()&std::ios::badbit) {
			return false;
		}

		// Type?
		std::string::size_type i=0;
		while(i<text.length()&&isspace(text[i])) {
		}
		if(i==text.length()||text[i]==';') {
			line.type=Line::COMMENT;
			line.name=text;
			DP("%s(%d): COMMENT: %s\n",filename,lines_.size()+1,line.name.c_str());
		} else if(text[i]=='[') {
			std::string::size_type rsq=text.find_last_of(']');
			line.type=Line::SECTION;
			line.name=text;
			CopyTrimmed(&line.value,text,i+1,rsq);
			sections_.push_back(int(lines_.size()));
			DP("%s(%d): SECTION: [%s]\n",filename,lines_.size()+1,line.value.c_str());
		} else {
			line.type=Line::ITEM;
			std::string::size_type eq=text.find_first_of('=',i);
			if(eq==std::string::npos) {
				CopyTrimmed(&line.name,text,i,text.length());
			} else {
				CopyTrimmed(&line.name,text,i,eq);
				line.value=GetUnescaped(text.substr(eq+1));
			}
			DP("%s(%d): ITEM: %s=%s\n",filename,lines_.size()+1,line.name.c_str(),line.value.c_str());
		}
		lines_.push_back(line);
//		printf("'%s'\n",text.c_str());
	}

	return true;
}

int mbIniFile::GetFirstSection() {
	if(sections_.empty()) {
		return -1;
	} else {
		return 0;
	}
}

int mbIniFile::GetNextSection(int section) {
	++section;
	if(section>=int(sections_.size())) {
		return -1;
	} else {
		return section;
	}
}

const char *mbIniFile::GetSectionName(int section) {
	assert(this->IsValidSection(section));
	return lines_[sections_[section]].value.c_str();
}

int mbIniFile::GetFirstItem(int section) {
	assert(this->IsValidSection(section));
	return this->GetNextItem(section,sections_[section]);
}

int mbIniFile::GetNextItem(int section,int item) {
	(void)section;

	do {
		++item;
	} while(item<int(lines_.size())&&lines_[item].type==Line::COMMENT);

	if(item>=int(lines_.size())) {
		return -1;
	} else if(lines_[item].type!=Line::ITEM) {
		return -1;
	} else {
		return item;
	}
}

const char *mbIniFile::GetItemName(int section,int item) {
	(void)section;
	assert(this->IsValidItem(section,item));

	return lines_[item].name.c_str();
}

const char *mbIniFile::GetItemValue(int section,int item) {
	(void)section;
	assert(this->IsValidItem(section,item));

	return lines_[item].value.c_str();
}

void mbIniFile::SetItem(int section,int item,const char *new_name,const char *new_value) {
	(void)section;
	assert(this->IsValidItem(section,item));

	if(new_name) {
		lines_[item].name=new_name;
	}
	if(new_value) {
		lines_[item].value=new_value;
	}
}

#ifndef NDEBUG
bool mbIniFile::IsValidSection(int section) {
	assert(section<int(sections_.size()));
	assert(lines_[sections_[section]].type==Line::SECTION);
	return true;
}

bool mbIniFile::IsValidItem(int section,int item) {
	assert(this->IsValidSection(section));
	assert(lines_[item].type==Line::ITEM);
	return true;
}
#endif

int mbIniFile::AppendSection(const char *section_name) {
	assert(section_name);
	Line new_line;

	new_line.type=Line::SECTION;
	new_line.name=std::string("[")+section_name+"]";
	new_line.value=section_name;

	sections_.push_back(int(lines_.size()));
	lines_.push_back(new_line);

	return int(sections_.size()-1);
}

void mbIniFile::DeleteSection(int section) {
	assert(this->IsValidSection(section));

	int i=sections_[section];
	do {
		++i;
	} while(i<int(lines_.size())&&lines_[i].type!=Line::SECTION);

	// delete from sections_[section] to i.
	int num_removed=i-sections_[section];
	int dest=sections_[section];
	for(unsigned j=i;j<lines_.size();++j) {
		lines_[dest++]=lines_[j];
	}

	// dest to end is junk.
	lines_.resize(dest);

	// fix up sections.
	for(int j=section;j<int(sections_.size()-1);++j) {
		sections_[j]=sections_[j+1]-num_removed;
		assert(lines_[sections_[j]].type==Line::SECTION);
	}
	sections_.pop_back();
}

void mbIniFile::DeleteItem(int section,int item) {
	(void)section;
	assert(this->IsValidItem(section,item));

	lines_[item].type=Line::EMPTY;
}

int mbIniFile::AppendItem(int section,const char *item_name) {
	assert(this->IsValidSection(section));
	int i=sections_[section];
	do {
		++i;
	} while(i<int(lines_.size())&&lines_[i].type!=Line::SECTION);
	
	if(i>=int(lines_.size())) {
		// append at end.
		Line new_line;

		new_line.type=Line::ITEM;
		new_line.name=item_name;
		lines_.push_back(new_line);
		return int(lines_.size()-1);
	} else {
		// insert in the middle.
		// back up past comments, which are thought of as attached to
		// the section name.
		bool insert_after=false;
		while(i>=sections_[section]&&lines_[i].type==Line::COMMENT) {
			--i;
			insert_after=true;
		}

		if(insert_after) {
			++i;
		}
		lines_.resize(lines_.size()+1);
		Line *new_line=&lines_[i];
		for(int j=int(lines_.size()-1);j>i;--j) {
			lines_[j]=lines_[j-1];
		}
		new_line->type=Line::ITEM;
		new_line->name=item_name;
		new_line->value="";

		// update sections
		for(unsigned j=0;j<sections_.size();++j) {
			if(sections_[j]>=i) {
				++sections_[j];
			}
			assert(lines_[sections_[j]].type==Line::SECTION);
		}

		return i;
	}
}

//void mbIniFile::DeleteItem(int section,int item) {
//	assert(this->IsValidItem(section,item));
//
//	// easy
//	for(unsigned i=item+1;i<lines_.size();++i) {
//		lines_[i-1]=lines_[i];
//	}
//	lines_.pop_back();
//
//	for(unsigned i=0;i<lines_.size();++i) {
//		if(sections_[i]>item) {
//			--sections_[i];
//			assert(lines_[sections_[i]].type==Line::SECTION);
//		}
//	}
//}

bool mbIniFile::Save(const char *filename) {
	FILE *h=fopen(filename,"wt");
	if(!h) {
		return false;
	}
	for(unsigned i=0;i<lines_.size();++i) {
		const Line *l=&lines_[i];

		switch(l->type) {
		case Line::COMMENT:
			fprintf(h,"%s\n",l->name.c_str());
			break;

		case Line::SECTION:
			fprintf(h,"%s\n",l->name.c_str());
			break;

		case Line::ITEM:
			fprintf(h,"%s=%s\n",l->name.c_str(),GetEscaped(l->value).c_str());
			break;

		case Line::EMPTY:
			break;

		default:
			assert(false);
			break;
		}
	}
	fclose(h);
	return true;
}

void mbIniFile::Check() {
#ifdef _DEBUG
	for(unsigned i=0;i<sections_.size();++i) {
		assert(lines_[sections_[i]].type==Line::SECTION);
	}
#endif
}

void mbIniFile::DebugDump() {
#ifdef _WIN32
	for(unsigned i=0;i<lines_.size();++i) {
		const Line *l=&lines_[i];
		char linenum[13];
		_snprintf(linenum,sizeof linenum,"%-10d: ",i);
		OutputDebugStringA(linenum);

		switch(l->type) {
		case Line::COMMENT:
			OutputDebugStringA("COMMENT: ");
			OutputDebugStringA(l->name.c_str());
			break;

		case Line::SECTION:
			OutputDebugStringA("SECTION: ");
			OutputDebugStringA(l->name.c_str());
			break;

		case Line::ITEM:
			OutputDebugStringA("ITEM   : ");
			OutputDebugStringA(l->name.c_str());
			OutputDebugStringA("=");
			OutputDebugStringA(l->value.c_str());
			break;

		case Line::EMPTY:
			OutputDebugStringA("EMPTY.");

		default:
			assert(false);
			break;
		}

		OutputDebugStringA("\n");
	}
#endif//_WIN32
}

