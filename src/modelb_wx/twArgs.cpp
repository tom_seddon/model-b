#include "pch.h"
#include "twArgs.h"
#include "mbMisc.h"
#include <iostream>
#include <fstream>

///////////////////////////////////////////////////////////////////////////
// misc crap
//static bool StringToFloat(const wxString &str,float &f) {
	//char *ep;
	//double r=strtod(str.c_str(),&ep);
	//if(*ep&&!isspace(*ep)) {
		//return false;
	//}
	//f=float(r);
	//return true;
//}

static bool ReadResponseFile(const wxString &name,std::vector<wxString> &lines) {
	const char *name_str=name.c_str();
	std::ifstream inf(name_str);
	while(inf) {
		std::vector<wxString> toks;
		std::string line;
		std::getline(inf,line);
		mbTokenizeString(line.c_str()," \t\n",toks,true);
		lines.insert(lines.end(),toks.begin(),toks.end());
	}
	return inf.rdstate()==std::ios::goodbit||(inf.rdstate()&std::ios::eofbit);
}

///////////////////////////////////////////////////////////////////////////
// this should really be somewhere else... but it isn't...
bool twArg::ProcessAll(const std::vector<wxString> &args,const std::vector<twArg *> &optList) {
	unsigned j;
	std::vector<wxString>::const_iterator arg=args.begin();
	while(arg!=args.end()) {
		//What is this?
		if((*arg)[0]=='@') {
			std::vector<wxString> resp;
			// it's a response file
			const wxString respFile=arg->substr(1);
			if(!ReadResponseFile(respFile,resp)) {
				wxLogError("failed to read response file:\n\"%s\"\n",respFile.c_str());
				return false;
			}
			++arg;//skip the @
			ProcessAll(resp,optList);
		} else if((*arg)[0]!='-') {
			wxLogWarning("Ignoring unexpected atom \"%s\"\n",arg->c_str());
			++arg;
		} else {
			const char *szArg=arg->c_str()+1;
			for(j=0;j<optList.size();++j) {
				if(stricmp(szArg,optList[j]->Str().c_str())==0) {
					break;
				}
			}
			if(j>=optList.size()) {
				wxLogWarning("Ignoring unrecognised flag \"%s\"\n",arg->c_str());
				++arg;
			} else {
				int num=optList[j]->NumArgs(),argIdx;
				assert(num>=0);
				std::vector<wxString> tmpArgs;
				tmpArgs.reserve(num);
				wxString argname=*arg;
				++arg;//skip option name
				for(argIdx=0;argIdx<num&&arg!=args.end();++argIdx) {
					tmpArgs.push_back(*arg);
					++arg;
				}
				if(argIdx<num) {
					// ran out early
					wxLogError("flag \"%s\" requires %d arguments\n",argname.c_str(),num);
					return false;
				}
				if(!optList[j]->Perform(tmpArgs)) {
					return false;
				}
			}
		}
	}
	return true;
}

twArg::twArg(const wxString &inStr,int numArgs):
str(inStr),
numArgs(numArgs)
{
}

twArg::twArg(const char *inStr,int inNumArgs):
str(inStr),
numArgs(inNumArgs)
{
}

twArg::~twArg() {
}

///////////////////////////////////////////////////////////////////////////
twArgPresent::twArgPresent(const char *inStr,bool *inFlag,bool inSetting):
twArg(inStr,0),
flag(inFlag),
setting(inSetting)
{
	wxASSERT(this->flag);
}

bool twArgPresent::Perform(const std::vector<wxString> &) {
	*this->flag=this->setting;
	return true;
}

///////////////////////////////////////////////////////////////////////////
twArgString::twArgString(const char *inStr,wxString *inSetting,const wxString &inDefault):
twArg(inStr,1),
setting(inSetting)
{
	wxASSERT(this->setting);
	*this->setting=inDefault;
}

bool twArgString::Perform(const std::vector<wxString> &args) {
	*this->setting=args[0];
	return true;
}

///////////////////////////////////////////////////////////////////////////

twArgState::twArgState(const char *inStr,bool *inFlag,bool inDefault):
twArg(inStr,1),
flag(inFlag)
{
	wxASSERT(this->flag);
	*this->flag=inDefault;
}

bool twArgState::Perform(const std::vector<wxString> &args) {
	if(args[0]=="yes"||args[0]=="on"||args[0]=="true") {
		*this->flag=true;
		return true;
	} else if(args[0]=="no"||args[0]=="off"||args[0]=="false") {
		*this->flag=false;
		return true;
	}
	wxLogError("STATE must be \"yes\"/\"no\", \"on\"/\"off\" or \"true\"/\"false\".\n");
	return false;
}

///////////////////////////////////////////////////////////////////////////
twArgUint::twArgUint(const char *inStr,unsigned *inVarRef,unsigned inMin,unsigned inMax):
twArg(inStr,1),
minimum(inMin),
maximum(inMax),
hasMinMax(true),
varRef(inVarRef)
{
}

twArgUint::twArgUint(const char *inStr,unsigned *inVarRef):
twArg(inStr,1),
hasMinMax(false),
varRef(inVarRef)
{
}

bool twArgUint::Perform(const std::vector<wxString> &args) {
	unsigned tmp;
	char *ep;

	tmp=strtoul(args[0].c_str(),&ep,0);
	if(*ep&&!isspace(*ep)) {
		wxLogError("could not understand number \"%s\"\n",args[0].c_str());
		return false;
	}
	*this->varRef=tmp;
	return true;
}

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
twArgFloat::twArgFloat(const char *inStr,float *inVarRef):
twArg(inStr,1),
varRef(inVarRef)
{
	wxASSERT(this->varRef);
}

bool twArgFloat::Perform(const std::vector<wxString> &args) {
	float tmp;
	char *ep;
	
	tmp=strtod(args[0].c_str(),&ep);
	if(*ep&&!isspace(*ep)) {
		wxLogError("could not understand number \"%s\"\n",args[0].c_str());
		return false;
	}
	*this->varRef=tmp;
	return true;
}

///////////////////////////////////////////////////////////////////////////
twArgFnCall::twArgFnCall(const wxString &inStr,FuncType inFunc,void *inContext/* =0 */):
twArg(inStr,0),
func(inFunc),
context(inContext)
{
}

bool twArgFnCall::Perform(const std::vector<wxString> &args) {
	(void)args;

	return (*this->func)(this->context);
}
///////////////////////////////////////////////////////////////////////////
twArgStrings::twArgStrings(const char *inStr,std::vector<wxString> *inVar,unsigned inNumArgs):
twArg(inStr,inNumArgs),
var(inVar)
{
	wxASSERT(this->var);
}

bool twArgStrings::Perform(const std::vector<wxString> &args) {
	*this->var=args;
	return true;
}

