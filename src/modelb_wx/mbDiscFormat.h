#ifndef mbDISCFORMAT_H_
#define mbDISCFORMAT_H_

#include <bbcFdd.h>
#include <wx/filename.h>

struct mbDiscFormat {
	int sides;
	int tracks;
	int sectors;
	bbcFdd::DiscDensity density;
	wxString description;
	std::vector<wxString> extensions;
};

bool mbDiscFormatGetFromFileName(const wxFileName &file_name,mbDiscFormat *fmt);
bool mbDiscFormatGetFromFile(const wxFileName &file_name,unsigned file_size,mbDiscFormat *fmt);
void mbDiscFormatGetAll(std::vector<mbDiscFormat> *all_fmts);
void mbDiscFormatGetAllExtensions(std::vector<wxString> *all_exts);

#endif
