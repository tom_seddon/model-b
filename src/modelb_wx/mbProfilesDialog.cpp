#include "pch.h"
#include "mbProfilesDialog.h"
#include "mbMisc.h"

const wxWindowID id_lst_profiles=mbNewId();
const wxWindowID id_parent_cb=mbNewId();
const wxWindowID id_slot_override_chk=mbNewId();
const wxWindowID id_slot_ram_chk=mbNewId();
const wxWindowID id_slot_select_file_bt=mbNewId();
const wxWindowID id_models_rd=mbNewId();
const wxWindowID id_difs_rd=mbNewId();
const wxWindowID id_os_override_chk=mbNewId();
const wxWindowID id_os_select_file_bt=mbNewId();
const wxWindowID id_models_chk=mbNewId();
const wxWindowID id_dif_chk=mbNewId();

mbProfilesDialog::mbProfilesDialog(wxWindow *parent,wxWindowID id,const mbProfilesConfig &cfg):
wxDialog(parent,id,"Profiles",wxDefaultPosition,wxDefaultSize,wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER),
cfg_(cfg)
{
	mbModelGetAll(&models_);
	mbDiscInterfaceGetAll(&difs_);

	this->CreateWidgets();

	this->SetCurrent(cfg_.current);
	this->Refresh();
}

void mbProfilesDialog::SetCurrent(const std::string &new_current) {
	cur_name_=new_current;

	mbProfile *cur_pro=cfg_.ProfileByName(cur_name_);
	wxASSERT(cur_pro);
	if(cur_pro) {
		cur_pro_=*cur_pro;
	}
	
	par_fpro_.ResetToDefaults();
	if(!cur_pro_.parent.empty()) {
		cfg_.GetFullProfileByName(cur_pro_.parent,&par_fpro_);
	}

//	cfg_.GetFullProfileByName(cur_name_,&cur_fpro_);

	cur_changed_=false;

	this->Refresh(true);
}

static const int border_size=2;

wxSizer *mbProfilesDialog::CreateRomsArea() {
	wxStaticBox *roms_bx=new wxStaticBox(this,-1,"Paged ROMs");
	wxStaticBoxSizer *sz_roms_bx=new wxStaticBoxSizer(roms_bx,wxVERTICAL);

	//override,slot,filename,...,ram
	wxFlexGridSizer *sz=new wxFlexGridSizer(16,5,2,2);

	for(int i=0;i<16;++i) {
		int slot=15-i;
		RomCtrls *rc=&roms_[slot];

		rc->ovr_chk=new wxCheckBox(this,id_slot_override_chk,"");
		rc->slot_sta=new wxStaticText(this,-1,wxString::Format("%d",slot));
		rc->fname_txt=new wxTextCtrl(this,-1,"");
		rc->fname_bt=new wxButton(this,id_slot_select_file_bt,"...",wxDefaultPosition,wxDefaultSize,wxBU_EXACTFIT);
		rc->ram_chk=new wxCheckBox(this,id_slot_ram_chk,"RAM");

		sz->Add(rc->ovr_chk,0,wxALIGN_CENTER_VERTICAL);
		sz->Add(rc->slot_sta,0,wxALIGN_CENTER_VERTICAL);
		sz->Add(rc->fname_txt,0,wxGROW|wxALIGN_CENTER_VERTICAL);
		sz->Add(rc->fname_bt,0,wxALIGN_CENTER_VERTICAL);
		sz->Add(rc->ram_chk,0,wxALIGN_CENTER_VERTICAL);
	}

	sz->AddGrowableCol(2);//fname_txt column

	sz_roms_bx->Add(sz,1,wxGROW);

	return sz_roms_bx;
}

void mbProfilesDialog::CreateWidgets() {
	//profiles box
	wxStaticBox *bx_profiles=new wxStaticBox(this,-1,"Profiles");
	wxStaticBoxSizer *sz_bx_profiles=new wxStaticBoxSizer(bx_profiles,wxHORIZONTAL);
	profiles_lst_=new wxListBox(this,id_lst_profiles,wxDefaultPosition,wxDefaultSize);
	sz_bx_profiles->Add(profiles_lst_,1,wxEXPAND|wxALL,border_size);
	
	//settings area
	wxStaticBox *bx_settings=new wxStaticBox(this,-1,"Profile configuration");
	wxStaticBoxSizer *sz_bx_settings=new wxStaticBoxSizer(bx_settings,wxHORIZONTAL);

	//parent for this config
	wxSizer *sz_parent=new wxBoxSizer(wxHORIZONTAL);
	wxStaticText *parent_sta=new wxStaticText(this,-1,"Parent");
	parent_cb_=new wxComboBox(this,id_parent_cb,wxEmptyString,wxDefaultPosition,wxDefaultSize,0,0,wxCB_DROPDOWN|wxCB_READONLY);
	sz_parent->Add(parent_sta,0,wxALL,border_size);
	sz_parent->Add(parent_cb_,1,wxGROW|wxALL,border_size);

	//model types
	wxBoxSizer *sz_models=new wxBoxSizer(wxHORIZONTAL);
	std::vector<wxString> model_names;
	for(unsigned i=0;i<models_.size();++i) {
		model_names.push_back(models_[i]->long_name);
	}
	models_chk_=new wxCheckBox(this,id_models_chk,"");
	models_rdb_=new wxRadioBox(this,id_models_rd,"Model",wxDefaultPosition,wxDefaultSize,model_names.size(),&model_names[0],
		0,wxRA_SPECIFY_ROWS);

	sz_models->Add(models_chk_,0,0);
	sz_models->Add(models_rdb_,1,wxGROW);

	//disc interfaces
	wxBoxSizer *sz_difs=new wxBoxSizer(wxHORIZONTAL);
	std::vector<wxString> dif_names;
	for(unsigned i=0;i<difs_.size();++i) {
		dif_names.push_back(difs_[i]->long_name);
	}
	dif_chk_=new wxCheckBox(this,id_dif_chk,"");
	difs_rdb_=new wxRadioBox(this,id_difs_rd,"Disc interface",wxDefaultPosition,wxDefaultSize,dif_names.size(),&dif_names[0],
		0,wxRA_SPECIFY_ROWS);

	sz_difs->Add(dif_chk_,0,0);
	sz_difs->Add(difs_rdb_,1,wxGROW);

	//os rom
	wxBoxSizer *sz_os=new wxBoxSizer(wxHORIZONTAL);
	wxStaticBox *os_bx=new wxStaticBox(this,-1,"OS ROM");
	wxStaticBoxSizer *sz_os_bx=new wxStaticBoxSizer(os_bx,wxHORIZONTAL);
	os_chk_=new wxCheckBox(this,id_os_override_chk,"");
	os_txt_=new wxTextCtrl(this,-1,"");
	os_bt_=new wxButton(this,id_os_select_file_bt,"...",wxDefaultPosition,wxDefaultSize,wxBU_EXACTFIT);

	sz_os_bx->Add(os_txt_,1,0);
	sz_os_bx->Add(os_bt_,0,0);
	sz_os->Add(os_chk_,0,0);
	sz_os->Add(sz_os_bx,1,0);

	//settings area left col
	wxBoxSizer *sz_lsettings=new wxBoxSizer(wxVERTICAL);
	sz_lsettings->Add(sz_parent,0,wxGROW);
	sz_lsettings->Add(sz_models,0,wxGROW);
	sz_lsettings->Add(sz_difs,0,wxGROW);
	sz_lsettings->Add(sz_os,0,wxGROW);

	//right col
	wxSizer *sz_rsettings=this->CreateRomsArea();

	//settings area columns
	wxBoxSizer *sz_settings=new wxBoxSizer(wxHORIZONTAL);
	sz_settings->Add(sz_lsettings,0,0);
	sz_settings->Add(sz_rsettings,1,wxGROW);

	//final settings setup
	sz_bx_settings->Add(sz_settings,1,wxGROW);

	//top area of dlg
	wxBoxSizer *sz_boxes=new wxBoxSizer(wxHORIZONTAL);
	sz_boxes->Add(sz_bx_profiles,1,wxEXPAND|wxALL,border_size);
	sz_boxes->Add(sz_bx_settings,3,wxEXPAND|wxALL,border_size);

	//buttons
	wxBoxSizer *sz_buttons=new wxBoxSizer(wxHORIZONTAL);
	wxButton *bt_ok=new wxButton(this,wxID_OK,"Close");
	sz_buttons->Add(bt_ok,0,wxALL|wxALIGN_RIGHT,border_size);

	//everything.
	wxBoxSizer *sz_all=new wxBoxSizer(wxVERTICAL);
	sz_all->Add(sz_boxes,1,wxEXPAND|wxALL,border_size);
	sz_all->Add(sz_buttons,0,wxALL,border_size);

	this->SetSizer(sz_all);
	sz_all->SetSizeHints(this);

//	this->Refresh();
}

//mbProfile *mbProfilesDialog::CurrentProfile() {
//	mbProfile *current=cfg_.ProfileByName(current_);
//	wxASSERT(current);
//	return current;
//}

//NOTE returns 0 on error (so something does get selected)
template<class T>
static int FindIndex(const T &cont,const wxString &short_name) {
	for(unsigned i=0;i<cont.size();++i) {
		if(short_name.CmpNoCase(cont[i]->short_name)==0) {
			return int(i);
		}
	}
	return 0;
}

void mbProfilesDialog::Refresh(bool first_time) {
	//wxLogDebug(__FUNCTION__ ": FindFocus=%p\n",FindFocus());
	wxWindow *old_focus=FindFocus();

	//update full current

	//
	profiles_lst_->Clear();
	for(unsigned i=0;i<cfg_.profiles.size();++i) {
		profiles_lst_->Append(cfg_.profiles[i].name.c_str(),reinterpret_cast<void *>(i));
	}
	profiles_lst_->SetStringSelection(cur_name_.c_str());

	// parent list
	parent_cb_->Clear();
	parent_cb_->Append("(No parent)",static_cast<void *>(0));
	for(unsigned i=0;i<cfg_.profiles.size();++i) {
		if(cfg_.profiles[i].name!=cur_name_) {
			parent_cb_->Append(cfg_.profiles[i].name.c_str(),reinterpret_cast<void *>(1));
		}
	}
	if(cur_pro_.parent.empty()) {
		parent_cb_->SetSelection(0);
	} else {
		parent_cb_->SetStringSelection(cur_pro_.parent.c_str());
	}

	// model and disc interface
	models_chk_->SetValue(!cur_pro_.model.empty());
	models_rdb_->SetSelection(FindIndex(models_,(cur_pro_.model.empty()?par_fpro_.model:cur_pro_.model).c_str()));
	models_rdb_->Enable(models_chk_->GetValue());

	dif_chk_->SetValue(!cur_pro_.fdc.empty());
	difs_rdb_->SetSelection(FindIndex(difs_,(cur_pro_.fdc.empty()?par_fpro_.fdc:cur_pro_.fdc).c_str()));
	difs_rdb_->Enable(dif_chk_->GetValue());

	//roms
	for(unsigned i=0;i<16;++i) {
		this->RefreshSlot(i);
	}

	//only do this first time, in case os is "" all the way up the list.
	if(first_time) {
		os_chk_->SetValue(!cur_pro_.os.empty());
	}
	os_txt_->SetValue((cur_pro_.os.empty()?par_fpro_.os:cur_pro_.os).c_str());
	os_txt_->Enable(os_chk_->GetValue());
	os_bt_->Enable(os_chk_->GetValue());

	old_focus->SetFocus();
}

void mbProfilesDialog::GetResult(mbProfilesConfig *cfg) const {
	*cfg=cfg_;
}

void mbProfilesDialog::RefreshSlot(int slot) {
//	roms_[slot].Refresh(&cur_fpro_.roms[slot]);
	RomCtrls *rc=&roms_[slot];
	mbProfileRom *currom=&cur_pro_.roms[slot];
	const mbProfileRom *parrom=&par_fpro_.roms[slot];

	rc->ovr_chk->SetValue(currom->override);
	rc->fname_txt->SetValue((currom->override?currom->filename:parrom->filename).c_str());
	rc->ram_chk->SetValue(currom->override?currom->ram:parrom->ram);

	rc->slot_sta->Enable(currom->override);
	rc->fname_txt->Enable(currom->override);
	rc->fname_bt->Enable(currom->override);
	rc->ram_chk->Enable(currom->override);

	if(currom->override) {
		rc->fname_txt->SetValidator(mbSTLStringValidator(wxFILTER_NONE,&currom->filename));
	} else {
		rc->fname_txt->SetValidator(wxDefaultValidator);
	}
}

void mbProfilesDialog::OnModelsRadioBox(wxCommandEvent &) {
	wxASSERT(!cur_pro_.model.empty());
	cur_pro_.model=models_[models_rdb_->GetSelection()]->short_name;
	wxLogDebug("%s: model set to \"%s\"\n",__FUNCTION__,cur_pro_.model.c_str());
	this->Refresh();
	cur_changed_=true;
}

void mbProfilesDialog::OnDiscInterfaceRadioBox(wxCommandEvent &) {
	wxASSERT(!cur_pro_.fdc.empty());
	cur_pro_.fdc=difs_[difs_rdb_->GetSelection()]->short_name;
	wxLogDebug("%s: fdc set to \"%s\"\n",__FUNCTION__,cur_pro_.fdc.c_str());
	this->Refresh();
	cur_changed_=true;
}

void mbProfilesDialog::OnRomOverride(wxCommandEvent &event) {
	int slot=this->SlotFromCtrl(event.GetEventObject());
	if(slot>=0) {
		cur_pro_.roms[slot].override=roms_[slot].ovr_chk->GetValue();
		this->RefreshSlot(slot);
		cur_changed_=true;
}
}

void mbProfilesDialog::OnRomRam(wxCommandEvent &event) {
	int slot=this->SlotFromCtrl(event.GetEventObject());
	if(slot>=0) {
		cur_pro_.roms[slot].ram=roms_[slot].ram_chk->GetValue();
		this->RefreshSlot(slot);
		cur_changed_=true;
	}
}

void mbProfilesDialog::OnRomSelectFile(wxCommandEvent &event) {
	int slot=this->SlotFromCtrl(event.GetEventObject());
	if(slot>=0) {
		this->SelectRomFile(&cur_pro_.roms[slot].filename,wxString::Format("Select ROM for slot %d",slot),false);
		cur_changed_=true;
	}
}

void mbProfilesDialog::SelectRomFile(std::string *str,const wxString &title,bool warn_if_not_16k) {
	(void)warn_if_not_16k;// <-- what happened here??

	wxString old_dir,old_name;
	wxFileName old=wxFileName::FileName(str->c_str());
	if(old.IsOk()) {
		old_name=old.GetFullName();
		old_dir=old.GetPath();
	}

	wxFileDialog fd(this,title,old_dir,old_name,"All files|*.*",wxFD_OPEN);
	if(fd.ShowModal()==wxID_OK) {
		*str=fd.GetPath();
		this->Refresh();
	}
}

void mbProfilesDialog::OnOsOverride(wxCommandEvent &) {
	bool ov=os_chk_->GetValue();

	cur_pro_.os=ov?par_fpro_.os:std::string("");
	this->Refresh();
	cur_changed_=true;
}

void mbProfilesDialog::OnProfilesList(wxCommandEvent &) {
	bool change=true;

	//wxLogDebug(__FUNCTION__ ": selection=%s\n",profiles_lst_->GetStringSelection().c_str());
	if(cur_changed_) {
		wxString caption="Save changes?";
		wxString msg=
			"You have made changes to \""+wxString(cur_name_.c_str())+"\".\n"
			"Do you want to save them first?";

		int r=wxMessageBox(msg,caption,wxYES_NO|wxCANCEL|wxICON_QUESTION,this);
		switch(r) {
		case wxYES:
			{
				mbProfile *cur=cfg_.ProfileByName(cur_name_);

				wxASSERT(cur);
				*cur=cur_pro_;
			}
			break;

		case wxNO:
			break;

		case wxCANCEL:
			change=false;
			break;

		}
	}

	if(change) {
		this->SetCurrent(profiles_lst_->GetStringSelection().ToStdString());
	} else {
		this->Refresh();
	}
}

void mbProfilesDialog::OnOsSelectFile(wxCommandEvent &) {
	this->SelectRomFile(&cur_pro_.os,"Select OS ROM",true);
	this->Refresh();
	cur_changed_=true;
}

void mbProfilesDialog::OnModelsCheckBox(wxCommandEvent &) {
	bool ov=models_chk_->GetValue();

	cur_pro_.model=ov?par_fpro_.model:std::string("");
	this->Refresh();
	cur_changed_=true;
}

void mbProfilesDialog::OnDiscInterfaceCheckBox(wxCommandEvent &) {
	bool ov=dif_chk_->GetValue();

	cur_pro_.fdc=ov?par_fpro_.fdc:std::string("");
	this->Refresh();
	cur_changed_=true;
}

int mbProfilesDialog::SlotFromCtrl(wxObject *ctrl) {
	for(int i=0;i<16;++i) {
		const RomCtrls *rc=&this->roms_[i];

		if(rc->fname_bt==ctrl||rc->ovr_chk==ctrl||rc->ram_chk==ctrl) {
			return i;
		}
	}
	return -1;

}

BEGIN_EVENT_TABLE(mbProfilesDialog,wxDialog)
	EVT_LISTBOX(id_lst_profiles,mbProfilesDialog::OnProfilesList)

	EVT_RADIOBOX(id_difs_rd,mbProfilesDialog::OnDiscInterfaceRadioBox)
	EVT_CHECKBOX(id_dif_chk,mbProfilesDialog::OnDiscInterfaceCheckBox)

	EVT_RADIOBOX(id_models_rd,mbProfilesDialog::OnModelsRadioBox)
	EVT_CHECKBOX(id_models_chk,mbProfilesDialog::OnModelsCheckBox)

	EVT_CHECKBOX(id_slot_override_chk,mbProfilesDialog::OnRomOverride)
	EVT_CHECKBOX(id_slot_ram_chk,mbProfilesDialog::OnRomRam)
	EVT_BUTTON(id_slot_select_file_bt,mbProfilesDialog::OnRomSelectFile)

	EVT_CHECKBOX(id_os_override_chk,mbProfilesDialog::OnOsOverride)
	EVT_BUTTON(id_os_select_file_bt,mbProfilesDialog::OnOsSelectFile)
END_EVENT_TABLE()
