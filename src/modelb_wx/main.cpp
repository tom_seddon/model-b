#include "pch.h"
#include "mbMainFrame.h"
#include "mbMisc.h"
#include "mbApp.h"
#ifdef _MSC_VER
#include <crtdbg.h>
#endif
#include <base/log.h>

#if wxUSE_THREADS
//#error "wxWindows has threads enabled. Please decide what you're going to do!"
#endif

extern const bool tw_track_heap_allocations=true;
extern const int tw_unknown_allocation_break=-1;

//////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER

//#define ALLOC_LOG_FILENAME "Z:\\heap\\mblog.dat"

#ifdef ALLOC_LOG_FILENAME

#define AL(X) AllocLog X

static void WriteAllocLog(bool alloc,size_t size,void *p) {
	static FILE *h=0;

	if(!h) {
		h=fopen(ALLOC_LOG_FILENAME,"wb");
	}

	int alloc_int=alloc;

	//assumes VC++!
	fwrite(&alloc_int,1,4,h);
	fwrite(&size,1,4,h);
	fwrite(&p,1,4,h);

	fflush(h);
}

void *operator new(size_t size) {
	void *p=malloc(size);
	WriteAllocLog(true,size,p);
	return p;
}

void *operator new[](size_t size) {
	void *p=malloc(size);
	WriteAllocLog(true,size,p);
	return p;
}

void operator delete(void *p) {
	WriteAllocLog(false,0,p);
	free(p);
}

void operator delete[](void *p) {
	WriteAllocLog(false,0,p);
	free(p);
}

#endif//_ALLOC_LOG_FILENAME

#endif//_MSC_VER

//////////////////////////////////////////////////////////////////////////

//#if !_HAS_EXCEPTIONS
//
//void std::_Throw(const std::exception &e) {
//	static char tmp[1000];
//
//	_snprintf(tmp,sizeof tmp-1,"Fatal error.\n%s\n",e.what());
//	MessageBox(0,tmp,"modelb Fatal error",MB_OK|MB_ICONERROR);
//	abort();
//}
//
//void (*std::_Raise_handler)(class std::exception const &e)=0;
//
//#endif

//////////////////////////////////////////////////////////////////////////
//
#ifdef _CONSOLE
static BOOL WINAPI ConsoleCtrlHandler(DWORD dwCtrlType) {
	switch(dwCtrlType) {
	case CTRL_C_EVENT:
	case CTRL_BREAK_EVENT:
	case CTRL_CLOSE_EVENT:
		{
			mbMainFrame *frame=wxGetApp().MainFrame();
			if(frame) {
				frame->Close(true);//true=force close (no veto)
				return TRUE;
			}
		}
		break;
	}
	return FALSE;
}
#endif

//////////////////////////////////////////////////////////////////////////
//grr borland C++! can't seem to work out the linker options for generating something
//that starts at WinMain.
#if (defined _CONSOLE)||(defined __BORLANDC__)
int main(int argc,char *argv[]) {
#ifdef _CONSOLE
	{
		static const int console_width=128;
		HANDLE handle=GetStdHandle(STD_OUTPUT_HANDLE);
		COORD size;
		size.X=console_width;
		size.Y=2000;
		SetConsoleScreenBufferSize(handle,size);
		SMALL_RECT r;
		r.Left=0;
		r.Top=0;
		r.Right=console_width-1;
		r.Bottom=50;
		SetConsoleWindowInfo(handle,TRUE,&r);

		SetConsoleCtrlHandler(&ConsoleCtrlHandler,TRUE);//TRUE=add
	}
#endif

//	_CrtSetDbgFlag(_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG)|_CRTDBG_CHECK_ALWAYS_DF);

	B_LOG_ENABLE(QStart,true);

	wxString args;
	for(int i=1;i<argc;++i) {
		if(!args.empty()) {
			args+=" ";
		}
		args+="\"";
		args+=argv[i];
		args+="\"";
	}

	char *cmdline=strdup(args.c_str());
	int r=WinMain(GetModuleHandle(0),0,cmdline,SW_SHOW);
	free(cmdline);
	return r;
}
#endif//_CONSOLE

