#ifndef mbICEFRAME_H_
#define mbICEFRAME_H_

#ifdef bbcENABLE_BEEB_ICE

#include <bbcBeebIce.h>
#include "mbMisc.h"

class mbConfigFile;

class mbIcePanel:
public wxPanel
{
public:
	const wxString &GetIceTitle() const;
	void SetIceTitle(const wxString &new_title);
protected:
	mbIcePanel(wxWindow *parent,wxWindowID id,const wxString &title);
private:
	wxString title_;
};

struct mbIceConfig {
	bool enabled;
	mbWindowPlacement placement;

	std::string file_name;

	mbIceConfig();
	bool Load(const std::string &file_name);
	void Save() const;
};

class mbIceFrame:
public wxFrame
{
public:
	mbIceFrame(wxWindow *parent,const mbIceConfig &cfg);

	void GetConfig(mbIceConfig *cfg) const;
protected:
private:
	mutable mbIceConfig cfg_;

	void OnClose(wxCloseEvent &event);

	DECLARE_EVENT_TABLE()
};

#endif//bbcENABLE_BEEB_ICE

#endif
