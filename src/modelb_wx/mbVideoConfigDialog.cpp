#include "pch.h"
#include "mbVideoConfigDialog.h"
#include "mbMisc.h"
#include <bbcVideo.h>
#include <wx/valgen.h>

//Width=640
//Height=272
//FrameSkip=1
//CleanupFrequency=1
//FullScreen=0
//FullScreenScanlinesMode=interlace
//WindowedScanlinesMode=single
//FullScreenRefreshRate=75
//MonitorType=0

static const int border_size=5;
static const int border_flags=wxTOP|wxRIGHT|wxLEFT;
static const int left_proportion=1;
static const int right_proportion=1;

const wxWindowID id_adapters_cb=mbNewId();

wxBoxSizer *mbVideoConfigDialog::NumberEntryBox(const wxString &caption,wxString *var) {
	wxTextCtrl *textentry=new wxTextCtrl(this,-1,*var);
	textentry->SetValidator(wxTextValidator(wxFILTER_NUMERIC,var));
	
	wxBoxSizer *sz=new wxBoxSizer(wxHORIZONTAL);
	sz->Add(new wxStaticText(this,-1,caption),left_proportion,border_flags,border_size);
	sz->Add(textentry,right_proportion,border_flags,border_size);

	return sz;
}

wxStaticBoxSizer *mbVideoConfigDialog::StaticBox(const wxString &caption,
	const std::vector<wxObject *> &contents)
{
	wxStaticBox *box=new wxStaticBox(this,-1,caption);
	wxStaticBoxSizer *sz=new wxStaticBoxSizer(box,wxVERTICAL);
	for(unsigned i=0;i<contents.size();++i) {
		wxObject *obj=contents[i];

		if(obj->IsKindOf(CLASSINFO(wxSizer))) {
			sz->Add(static_cast<wxSizer *>(obj),1,wxALL|wxGROW,border_size);
		} else if(obj->IsKindOf(CLASSINFO(wxWindow))) {
			sz->Add(static_cast<wxWindow *>(obj),1,wxALL|wxGROW,border_size);
		} else {
			BASSERT(false);//ooops...
		}
	}

	return sz;
}

mbVideoConfigDialog::mbVideoConfigDialog(wxWindow *parent,const mbVideoConfig &cfg,HostGfxState *state):
wxDialog(parent,-1,"Configure video",wxDefaultPosition,wxDefaultSize,wxDEFAULT_DIALOG_STYLE),
cfg_(cfg)
{
	width_=wxString::Format("%d",cfg_.width);
	height_=wxString::Format("%d",cfg_.height);
	cleanup_freq_=wxString::Format("%d",cfg_.cleanup_frequency);
	display_freq_=wxString::Format("%d",cfg_.frame_skip);

	adapters_=HostGfx_GetAdapters(state);
	num_adapters_=HostGfx_NumAdapters(state);

	std::vector<wxObject *> windowed,full_screen,settings;

	//Display box
	windowed.push_back(this->NumberEntryBox("&Width",&width_));
	windowed.push_back(this->NumberEntryBox("&Height",&height_));

	wxBoxSizer *adapters_sz=new wxBoxSizer(wxHORIZONTAL);
	{
		adapters_cb_=new wxComboBox(this,id_adapters_cb,"",wxDefaultPosition,wxDefaultSize,0,0,wxCB_DROPDOWN|wxCB_SORT|wxCB_READONLY);
		adapters_sz->Add(new wxStaticText(this,-1,"&Adapter"),left_proportion,border_flags,border_size);
		adapters_sz->Add(adapters_cb_,right_proportion,border_flags,border_size);

		for(int i=0;i<num_adapters_;++i) {
			adapters_cb_->Append(adapters_[i]->device_name,reinterpret_cast<void *>(i));
			printf("%s: adapters_[%u]=%p: device_name=\"%s\".\n",__FUNCTION__,i,adapters_[i],adapters_[i]->device_name);
		}

		//select the right one.
		for(int i=0;(unsigned)i<adapters_cb_->GetCount();++i) {
			const HostGfx_Adapter *adapter=adapters_[int(adapters_cb_->GetClientData(i))];

			if(adapter->device_name==cfg_.full_screen_adapter) {
				adapters_cb_->SetSelection(i);
				break;
			} else if(adapter->default_device) {
				adapters_cb_->SetSelection(i);
			}
		}
	}

	wxBoxSizer *hz_sz=new wxBoxSizer(wxHORIZONTAL);
	{
		hz_cb_=new wxComboBox(this,-1,"",wxDefaultPosition,wxDefaultSize,0,0,
			wxCB_DROPDOWN|wxCB_SORT|wxCB_READONLY);
		hz_sz->Add(new wxStaticText(this,-1,"&Refresh rate"),left_proportion,border_flags,border_size);
		hz_sz->Add(hz_cb_,right_proportion,border_flags,border_size);
	}
//	full_screen.push_back(adapters_sz);
	settings.push_back(adapters_sz);
	full_screen.push_back(hz_sz);

	wxCheckBox *vsync=new wxCheckBox(this,-1,"Sync to refresh rate");
	vsync->SetValidator(wxGenericValidator(&cfg_.full_screen_vsync));
	full_screen.push_back(vsync);

	//Settings box
	settings.push_back(this->NumberEntryBox("Cleanup frequency",&cleanup_freq_));
	settings.push_back(this->NumberEntryBox("Display frequency",&display_freq_));

	//Buttons
	wxBoxSizer *btns_sz=new wxBoxSizer(wxHORIZONTAL);
	wxButton *ok=new wxButton(this,wxID_OK,"OK");
	wxButton *cancel=new wxButton(this,wxID_CANCEL,"Cancel");
	ok->SetDefault();
	btns_sz->Add(ok,0,wxALL|wxALIGN_RIGHT,border_size);
	btns_sz->Add(cancel,0,wxALL|wxALIGN_RIGHT,border_size);

	//Altogether now...
	wxBoxSizer *all_sz=new wxBoxSizer(wxVERTICAL);
	all_sz->Add(this->StaticBox("Windowed",windowed),0,wxGROW|border_flags,border_size);
	all_sz->Add(this->StaticBox("Full screen",full_screen),0,wxGROW|border_flags,border_size);
	all_sz->Add(this->StaticBox("Options",settings),0,wxGROW|border_flags,border_size);
	all_sz->Add(btns_sz,1,wxALIGN_RIGHT,border_size);

	//
	this->SetSizer(all_sz);
	all_sz->SetSizeHints(this);
	this->Layout();

	mbSetWindowPos(this,cfg_.dlg_rect.GetTopLeft());//this->Move(cfg_.dlg_rect.GetLeft(),cfg_.dlg_rect.GetTop());

	this->RefreshDialog();
}

void mbVideoConfigDialog::GetResult(mbVideoConfig *cfg) {
	BASSERT(cfg);
	*cfg=cfg_;
}

static void GetInt(const char *name,const wxString &str,int *dest,int mini,int maxi) {
	long value;
	bool bad=false;

	BASSERT(mini>=0);
	if(!str.ToLong(&value,0)) {
		bad=true;
		value=mini;
	} else if(mini>=0&&value<mini) {
		bad=true;
		value=mini;
	} else if(maxi>=0&&value>maxi) {
		bad=true;
		value=maxi;
	}

	if(bad) {
		wxLogWarning("Invalid %s \"%s\" -- reset to %ld\n",name,str.c_str(),value);
	}

	*dest=value;
}

void mbVideoConfigDialog::OnOk(wxCommandEvent &) {
	int rate=reinterpret_cast<int>(hz_cb_->GetClientData(hz_cb_->GetSelection()));

	this->TransferDataFromWindow();
	
	cfg_.full_screen_refresh_rate=rate;
	cfg_.full_screen_adapter=adapters_cb_->GetStringSelection();
	GetInt("width",width_,&cfg_.width,bbcVideo::min_buffer_width,bbcVideo::max_buffer_width);
	GetInt("height",height_,&cfg_.height,bbcVideo::min_buffer_height,bbcVideo::max_buffer_height);
	GetInt("cleanup frequency",cleanup_freq_,&cfg_.cleanup_frequency,1,-1);
	GetInt("display frequency",display_freq_,&cfg_.frame_skip,1,mbVideoConfig::max_frame_skip);

	cfg_.dlg_rect=this->GetRect();

	this->EndModal(wxID_OK);
}

void mbVideoConfigDialog::OnAdaptersComboBox(wxCommandEvent &) {
	this->RefreshDialog();
}

void mbVideoConfigDialog::RefreshDialog() {
	int old_hz=0;
	wxLogDebug("%s: hz_cb_: selection=%d count=%d\n",__FUNCTION__,hz_cb_->GetSelection(),hz_cb_->GetCount());
	int old_selection=hz_cb_->GetSelection();
	// on gtk, sometimes the selection is out of range (wx bug? me?)
	if(old_selection>=0&&(unsigned)old_selection<hz_cb_->GetCount()) {
		void *client_data=hz_cb_->GetClientData(old_selection);
		if(client_data) {
			old_hz=int(client_data);
		}
	}

	int adapter_index=int(adapters_cb_->GetClientData(adapters_cb_->GetSelection()));
	const HostGfx_Adapter *adapter=adapters_[adapter_index];
	hz_cb_->Clear();
	int new_selection=-1;
	for(int i=0;i<adapter->num_refresh_rates;++i) {
		int hz=adapter->refresh_rates[i];
		hz_cb_->Append(wxString::Format("%dHz",hz),reinterpret_cast<void *>(hz));
	}
	wxLogDebug("%s: hz_cb_: selection=%d count=%d\n",__FUNCTION__,hz_cb_->GetSelection(),hz_cb_->GetCount());
	hz_cb_->Append("Default",static_cast<void *>(0));
	wxLogDebug("%s: hz_cb_: selection=%d count=%d\n",__FUNCTION__,hz_cb_->GetSelection(),hz_cb_->GetCount());
	//They were sorted after adding, so get index from their new order.
	for(int i=0;(unsigned)i<hz_cb_->GetCount();++i) {
		if(int(hz_cb_->GetClientData(i))==old_hz) {
			new_selection=i;
			break;
		}
	}
	if(new_selection<0) {
		new_selection=hz_cb_->GetCount()-1;
	}
	if(hz_cb_->GetCount()!=0) {
		hz_cb_->SetSelection(new_selection);
	}

	wxLogDebug("%s: finally: hz_cb_: selection=%d count=%d\n",__FUNCTION__,hz_cb_->GetSelection(),hz_cb_->GetCount());
}

BEGIN_EVENT_TABLE(mbVideoConfigDialog,wxDialog)
	EVT_COMBOBOX(id_adapters_cb,mbVideoConfigDialog::OnAdaptersComboBox)
	EVT_BUTTON(wxID_OK,mbVideoConfigDialog::OnOk)
END_EVENT_TABLE()
