#ifndef MBTESTMODE_H_
#define MBTESTMODE_H_

//Define for a special mode in which the CPU test suite is run.
//#define TEST_MODE

#ifdef TEST_MODE
int mbTestModeRun();
#endif

#endif
