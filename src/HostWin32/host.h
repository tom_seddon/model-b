#ifndef HOSTWIN32_H_
#define HOSTWIN32_H_

#include <windows.h>

//TODO split into HostGeneric and HostXXX, some stuff will be constant.

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////////////////////////////
// global host flags.
enum Host_GlobalFlags {
	//disable use of SSE stuff.
	Host_DISABLE_SSE=1,
};

void Host_SetGlobalFlags(unsigned new_global_flags);

//////////////////////////////////////////////////////////////////////////
// configurable logging

typedef void (*Host_LogOutputCallbackFn)(const char *str);

void Host_SetLogOutputCallback(Host_LogOutputCallbackFn callback);
#ifdef __cplusplus
}
#endif


#include "HostTmr.h"
#include "HostGfx.h"
#include "HostSnd.h"
#include "HostInp.h"

#endif

