#include <port.h>
#include "host_dx_versions.h"

// suxx, but otherwise main program must know about DX3_ONLY...
// umm... which it does already. i think there was a better reason originally.
#ifdef _MSC_VER
# ifdef DX3_ONLY
#  pragma comment(lib,"dinput.lib")
# else
#  pragma comment(lib,"dinput8.lib")
# endif
#endif

#include "host_internal.h"
#include <windows.h>
#include <dinput.h>
#include <stddef.h>
#include "HostInp.h"
#include "host_internal.h"

//If defined, use emulated keyboard.
//DirectX3 does not support emulated keyboards it seems.
#if (defined HOST_DEBUG)&&!(defined DX3_ONLY)
# define EMULATED_KEYBOARD
#endif

//////////////////////////////////////////////////////////////////////////
//
#ifdef DX3_ONLY
typedef IDirectInput2A IDirectInputType;
typedef IDirectInputDeviceA IDirectInputDeviceType;
#else
typedef IDirectInput8A IDirectInputType;
typedef IDirectInputDevice8A IDirectInputDeviceType;
#endif

enum {
	max_num_joysticks=16,
};

#ifndef DX3_ONLY
struct Joy {
	IDirectInputDeviceType *dev;
	DIJOYSTATE state;
	DIJOYSTATE mini,maxi;
	char name[MAX_PATH];
};
typedef struct Joy Joy;
#endif

struct HostInpState {
	HWND owner;
	IDirectInputType *dinput;
	IDirectInputDeviceType *kb;

	BYTE kb_state[256];
	BYTE last_kb_state[256];

#ifndef DX3_ONLY
	Joy joysticks[max_num_joysticks];
	int num_joysticks;
#endif//DX3_ONLY
};

#ifndef DX3_ONLY
static const unsigned joystate_axis_offsets[]={
	offsetof(DIJOYSTATE,lX),
		offsetof(DIJOYSTATE,lY),
		offsetof(DIJOYSTATE,lZ),
		offsetof(DIJOYSTATE,lRx),
		offsetof(DIJOYSTATE,lRy),
		offsetof(DIJOYSTATE,lRz),
		offsetof(DIJOYSTATE,rglSlider[0]),
		offsetof(DIJOYSTATE,rglSlider[1]),
};
static const unsigned num_joystate_axes=
sizeof joystate_axis_offsets/sizeof joystate_axis_offsets[0];

static BOOL CALLBACK EnumDevicesCallback(LPCDIDEVICEINSTANCE lpddi,LPVOID pvRef) {
	HostInpState *state=pvRef;
	Joy *joy;
	HRESULT hr;
	DIPROPDWORD pdw;
	unsigned i;

	if(state->num_joysticks==max_num_joysticks) {
		return DIENUM_CONTINUE;
	}

	//	state->joysticks.push_back(HostInpState::Joy());
	//	joy=&state->joysticks.back();
	joy=&state->joysticks[state->num_joysticks++];
	dprintf("HostInp_Start:     Instance: %s\n",lpddi->tszInstanceName);
	dprintf("HostInp_Start:     Product: %s\n",lpddi->tszProductName);
	strcpy(joy->name,lpddi->tszInstanceName);

	for(i=0;joy->name[i];++i) {
		char c=joy->name[i];

		if(!isalnum(c)&&c!=' '&&c!='_') {
			joy->name[i]='_';
		}
	}

	joy->dev=0;
	hr=IDirectInput_CreateDevice(state->dinput,&lpddi->guidInstance,&joy->dev,0);
	if(FAILED(hr)) {
		goto stop_error;
	}
	hr=IDirectInputDevice_SetCooperativeLevel(joy->dev,state->owner,
		DISCL_FOREGROUND|DISCL_NONEXCLUSIVE);
	if(FAILED(hr)) {
		goto stop_error;
	}
	hr=IDirectInputDevice_SetDataFormat(joy->dev,&c_dfDIJoystick);
	if(FAILED(hr)) {
		goto stop_error;
	}

	//Set absolute axis mode
	pdw.diph.dwSize=sizeof pdw;
	pdw.diph.dwHeaderSize=sizeof pdw.diph;
	pdw.diph.dwObj=0;
	pdw.diph.dwHow=DIPH_DEVICE;
	pdw.dwData=DIPROPAXISMODE_ABS;
	hr=IDirectInputDevice_SetProperty(joy->dev,DIPROP_AXISMODE,&pdw.diph);
	if(FAILED(hr)) {
		goto stop_error;
	}

	//Axes deadzone and saturation
	pdw.diph.dwSize=sizeof pdw;
	pdw.diph.dwHeaderSize=sizeof pdw.diph;
	pdw.diph.dwObj=0;
	pdw.diph.dwHow=DIPH_DEVICE;
	pdw.dwData=2500;
	hr=IDirectInputDevice_SetProperty(joy->dev,DIPROP_DEADZONE,&pdw.diph);
	if(FAILED(hr)) {
		goto stop_error;
	}
	pdw.dwData=9500;
	hr=IDirectInputDevice_SetProperty(joy->dev,DIPROP_SATURATION,&pdw.diph);
	if(FAILED(hr)) {
		goto stop_error;
	}

	//Determine ranges
	for(i=0;i<num_joystate_axes;++i) {
		DIPROPRANGE prop;
		HRESULT hr2;
		LONG *mini,*maxi;
		unsigned offset;

		prop.diph.dwSize=sizeof prop;
		prop.diph.dwHeaderSize=sizeof prop.diph;
		prop.diph.dwObj=joystate_axis_offsets[i];
		prop.diph.dwHow=DIPH_BYOFFSET;
		prop.lMin=0;
		prop.lMax=0;
		hr2=IDirectInputDevice_GetProperty(joy->dev,DIPROP_RANGE,&prop.diph);
		offset=joystate_axis_offsets[i];
		mini=offsetwith(&joy->mini,offset);
		maxi=offsetwith(&joy->maxi,offset);
		//twprintf("JnbInput:\taxis %u: ",i);
		if(FAILED(hr2)) {
			//twprintf("%s",DXGetErrorString8A(hr2));
			*mini=*maxi=0;
		} else {
			*mini=prop.lMin;
			*maxi=prop.lMax;
			//twprintf("min=%u max=%u",*mini,*maxi);
		}
		//twprintf("\n");
	}			
	return DIENUM_CONTINUE;

stop_error:
	dprintf("HostInp_Start:     Failed: 0x%08X: %s\n",hr,DXGetErrorString(hr));
	SAFE_RELEASE(joy->dev);
	HOST_ASSERT(state->num_joysticks>0);
	--state->num_joysticks;
	//state->joysticks.pop_back();
	return DIENUM_CONTINUE;
}
#endif//DX3_ONLY

// Fix for BC++.
// See http://groups.google.com/groups?hl=en&lr=&ie=UTF-8&selm=bdq64b%24qhc%241%40news.contactel.cz
static HRESULT SetKeyboardDataFormat(IDirectInputDeviceType *keyboard) {
#ifndef __BORLANDC__

	return IDirectInputDevice_SetDataFormat(keyboard,&c_dfDIKeyboard);

#else
	DIDATAFORMAT dfkey;
	DIOBJECTDATAFORMAT odf[256];
	unsigned i;

	for(i=0;i<256;i++){
		odf[i].pguid=&GUID_Key;
		odf[i].dwOfs=i;
		odf[i].dwType=DIDFT_BUTTON|DIDFT_MAKEINSTANCE(i)|0x80000000;
		odf[i].dwFlags=0;
	}

	dfkey.dwSize=sizeof(DIDATAFORMAT);
	dfkey.dwObjSize=sizeof(DIOBJECTDATAFORMAT);
	dfkey.dwFlags=0;
	dfkey.dwDataSize=256;
	dfkey.dwNumObjs=256;
	dfkey.rgodf=odf;

	return IDirectInputDevice_SetDataFormat(keyboard,&dfkey);
#endif
}

HostInpState *HostInp_Start(void *window) {
	HostInpState *state=malloc(sizeof *state);
	HRESULT hr;
#ifdef DX3_ONLY
	IDirectInputA *dinput;
#endif
	unsigned coop;

	state->owner=window;
	state->dinput=0;
	state->kb=0;
	memset(state->kb_state,0,sizeof state->kb_state);
	memset(state->last_kb_state,0,sizeof state->last_kb_state);
#ifdef DX3_ONLY
	hr=DirectInputCreate(GetModuleHandle(0),DIRECTINPUT_VERSION,&dinput,0);
#else
	hr=DirectInput8Create(GetModuleHandle(0),DIRECTINPUT_VERSION,&IID_IDirectInput8A,&state->dinput,0);
#endif
	if(FAILED(hr)) {
		goto stop_error;
	}
#ifdef DX3_ONLY
	hr=IDirectInput_QueryInterface(dinput,&IID_IDirectInput2A,&state->dinput);
	IDirectInput_Release(dinput);
	if(FAILED(hr)) {
		goto stop_error;
	}
#endif
#if !(defined DX3_ONLY)&&(defined EMULATED_KEYBOARD)
	dprintf("HostInp_Start: trying GUID_SysKeyboardEm2.\n");
	hr=IDirectInput_CreateDevice(state->dinput,&GUID_SysKeyboardEm2,&state->kb,0);
	if(FAILED(hr)) {
		dprintf("HostInp_Start: trying GUID_SysKeyboardEm.\n");
		hr=IDirectInput_CreateDevice(state->dinput,&GUID_SysKeyboardEm,&state->kb,0);
		if(FAILED(hr)) {
			dprintf("HostInp_Start: trying GUID_SysKeyboard.\n");
			hr=IDirectInput_CreateDevice(state->dinput,&GUID_SysKeyboard,&state->kb,0);
		}
	}
#else
	dprintf("HostInp_Start: trying GUID_SysKeyboard.\n");
	hr=IDirectInput_CreateDevice(state->dinput,&GUID_SysKeyboard,&state->kb,0);
#endif
	if(FAILED(hr)) {
		goto stop_error;
	}
	coop=DISCL_FOREGROUND|DISCL_EXCLUSIVE;
#ifndef DX3_ONLY
	coop|=DISCL_NOWINKEY;
#endif
	hr=IDirectInputDevice_SetCooperativeLevel(state->kb,state->owner,coop);
	if(FAILED(hr)) {
		dprintf("HostInp_Start: failed to set exclusive keyboard mode, using nonexclusive.\n");
		coop&=~DISCL_EXCLUSIVE;
		coop|=DISCL_NONEXCLUSIVE;
		hr=IDirectInputDevice_SetCooperativeLevel(state->kb,state->owner,coop);
	}
	if(FAILED(hr)) {
		goto stop_error;
	}
	//hr=IDirectInputDevice_SetDataFormat(state->kb,&c_dfDIKeyboard);
	hr=SetKeyboardDataFormat(state->kb);
	if(FAILED(hr)) {
		goto stop_error;
	}
#ifndef DX3_ONLY
	dprintf("HostInp_Start: Enumerating joysticks...\n");
	state->num_joysticks=0;
	hr=IDirectInput_EnumDevices(state->dinput,DI8DEVCLASS_GAMECTRL,
		&EnumDevicesCallback,state,DIEDFL_ATTACHEDONLY);
	if(FAILED(hr)) {
		goto stop_error;
	}
	dprintf("HostInp_Start:     joysticks done.\n");
#endif
	dprintf("HostInp_Start: ok.\n");
	return state;

stop_error:
	HostInp_Stop(state);
	return 0;
}

void HostInp_Stop(HostInpState *state) {
	if(state) {
#ifndef DX3_ONLY
		int i;
#endif

		if(state->kb) {
			int num_refs;

			IDirectInputDevice_Unacquire(state->kb);
			num_refs=IDirectInputDevice_Release(state->kb);
			dprintf("HostInp_Stop: kb refs: %d\n",num_refs);
			state->kb=0;
		}
#ifndef DX3_ONLY
		for(i=0;i<state->num_joysticks;++i) {
			Joy *joy=&state->joysticks[i];
			if(joy->dev) {
				int num_refs;

				IDirectInputDevice_Unacquire(joy->dev);
				num_refs=IDirectInputDevice_Release(joy->dev);
				dprintf("HostInp_Stop: joy %d refs: %d\n",i,num_refs);
			}
		}
		state->num_joysticks=0;
#endif//DX3_ONLY
		if(state->dinput) {
			int num_refs;

			num_refs=IDirectInput_Release(state->dinput);
			state->dinput=0;
			dprintf("HostInp_Stop: dinput refs: %d\n",num_refs);
		}
	}
	free(state);
}

static void HostInp_PollInternal(IDirectInputDeviceType *dev,void *state,
								 unsigned state_size)
{
	HRESULT hr;

	hr=IDirectInputDevice_Acquire(dev);
	if(SUCCEEDED(hr)) {
#ifndef DX3_ONLY
		hr=IDirectInputDevice2_Poll(dev);
		if(SUCCEEDED(hr))
#endif
		{
			hr=IDirectInputDevice_GetDeviceState(dev,state_size,state);
			if(SUCCEEDED(hr)) {
				return;
			}
		}
	}
	memset(state,state_size,0);
}

void HostInp_Poll(HostInpState *state) {
#ifndef DX3_ONLY
	int i;
#endif

	memcpy(state->last_kb_state,state->kb_state,sizeof state->kb_state);
	HostInp_PollInternal(state->kb,state->kb_state,sizeof state->kb_state);
#ifndef DX3_ONLY
	for(i=0;i<state->num_joysticks;++i) {
		Joy *joy=&state->joysticks[i];
		if(joy->dev) {
			HostInp_PollInternal(joy->dev,&joy->state,sizeof joy->state);
		}
	}
#endif//DX3_ONLY
}

void HostInp_GetKeyboardState(HostInpState *state,unsigned char *keyflags) {
	memcpy(keyflags,state->kb_state,sizeof state->kb_state);
}

void HostInp_GetKeyboardDownFlags(HostInpState *state,unsigned char *downflags) {
	int i;

	for(i=0;i<256;++i) {
		downflags[i]=state->kb_state[i]&&!state->last_kb_state[i];
	}
}

#ifdef DX3_ONLY
int HostInp_SupportsJoysticks(HostInpState *state) {
	return 0;
}

int HostInp_NumJoysticks(HostInpState *state) {
	return 0;
}

int HostInp_GetJoystickState(HostInpState *state,int index,HostInp_JoyState *joy_state) {
	joy_state->x=0.f;
	joy_state->y=0.f;
	joy_state->z=0.f;
	joy_state->rx=0.f;
	joy_state->ry=0.f;
	joy_state->rz=0.f;
	joy_state->sliders[0]=0.f;
	joy_state->sliders[1]=0.f;
	memset(joy_state->buttons,0,sizeof joy_state->buttons);
	return 0;
}

const char *HostInp_JoystickName(HostInpState *state,int joy_index) {
	return 0;
}

#else

int HostInp_SupportsJoysticks(HostInpState *state) {
	(void)state;

	return 1;
}

int HostInp_NumJoysticks(HostInpState *state) {
	return state->num_joysticks;
}

static void DoRangeElement(Joy *joy,unsigned offset,float *f) {
	LONG v=*(LONG *)offsetwith(&joy->state,offset);
	LONG mini=*(LONG *)offsetwith(&joy->mini,offset);
	LONG maxi=*(LONG *)offsetwith(&joy->maxi,offset);

	if(v<mini) {
		v=mini;
	} else if(v>maxi) {
		v=maxi;
	}
	if(maxi==mini) {
		*f=0.f;
	} else {
		*f=(float)(v-mini);
		*f/=maxi-mini;
		*f*=2.f;
		*f-=1.f;
	}
	HOST_ASSERT(*f>=-1.f&&*f<=1.f);
}

//Based on POV hat reading, set joystick directions so that the POV
//can be used as a dpad.
static void EmulateDpad(DWORD hat,HostInp_JoyState *state) {
	float *vars[2];
	float vals[4];
	int i;

	if(LOWORD(hat)==0xFFFF) {
		//No reading.
		return;
	}

	vars[0]=&state->y;
	vars[1]=&state->x;

	vals[0]=-1;
	vals[1]=1;
	vals[2]=1;
	vals[3]=-1;

	for(i=0;i<8;++i) {
		int angle=i*45*DI_DEGREES;
		int d=(hat-angle)%(360*DI_DEGREES);

		if(d>=-23&&d<22) {
			*vars[(i>>1)&1]=vals[i>>1];
			if(i&1) {
				int j=(i+1)&7;

				*vars[(j>>1)&1]=vals[j>>1];
			}
		}
	}
}

int HostInp_GetJoystickState(HostInpState *state,int index,HostInp_JoyState *joy_state) {
	if(index<0||index>=state->num_joysticks) {
		joy_state->x=0.f;
		joy_state->y=0.f;
		joy_state->z=0.f;
		joy_state->rx=0.f;
		joy_state->ry=0.f;
		joy_state->rz=0.f;
		joy_state->sliders[0]=0.f;
		joy_state->sliders[1]=0.f;
		memset(joy_state->buttons,0,sizeof joy_state->buttons);
		return 0;
	} else {
		unsigned i;
		Joy *joy=&state->joysticks[index];

		DoRangeElement(joy,offsetof(DIJOYSTATE,lX),&joy_state->x);
		DoRangeElement(joy,offsetof(DIJOYSTATE,lY),&joy_state->y);
		DoRangeElement(joy,offsetof(DIJOYSTATE,lZ),&joy_state->z);
		DoRangeElement(joy,offsetof(DIJOYSTATE,lRx),&joy_state->rx);
		DoRangeElement(joy,offsetof(DIJOYSTATE,lRy),&joy_state->ry);
		DoRangeElement(joy,offsetof(DIJOYSTATE,lRz),&joy_state->rz);
		DoRangeElement(joy,offsetof(DIJOYSTATE,rglSlider[0]),&joy_state->sliders[0]);
		DoRangeElement(joy,offsetof(DIJOYSTATE,rglSlider[1]),&joy_state->sliders[1]);
		memcpy(joy_state->buttons,&joy->state.rgbButtons,HostInp_num_joystick_buttons);
		for(i=0;i<4;++i) {
			EmulateDpad(joy->state.rgdwPOV[i],joy_state);
		}
		return 1;
	}
}

const char *HostInp_JoystickName(HostInpState *state,int joy_index) {
	if(joy_index<0||joy_index>state->num_joysticks) {
		return 0;
	} else {
		return state->joysticks[joy_index].name;
	}
}

#endif//DX3_ONLY

struct KeyAndName {
	unsigned key_code;
	const char *key_name;
};

//The rationale behind all this is to keep the key names positional rather
//than based on keycap.
static const struct KeyAndName pc_key_details[]={
	{DIK_ESCAPE,"PC_ESC"},
	{DIK_F1,"PC_F1"},
	{DIK_F2,"PC_F2"},
	{DIK_F3,"PC_F3"},
	{DIK_F4,"PC_F4"},
	{DIK_F5,"PC_F5"},
	{DIK_F6,"PC_F6"},
	{DIK_F7,"PC_F7"},
	{DIK_F8,"PC_F8"},
	{DIK_F9,"PC_F9"},
	{DIK_F10,"PC_F10"},
	{DIK_F11,"PC_F11"},
	{DIK_F12,"PC_F12"},
	{DIK_SYSRQ,"PC_PRTSCR"},
	{DIK_SCROLL,"PC_SCROLLLOCK"},
	{DIK_PAUSE,"PC_PAUSE"},
	{DIK_GRAVE,"PC_BACKQUOTE"},
	{DIK_1,"PC_1"},
	{DIK_2,"PC_2"},
	{DIK_3,"PC_3"},
	{DIK_4,"PC_4"},
	{DIK_5,"PC_5"},
	{DIK_6,"PC_6"},
	{DIK_7,"PC_7"},
	{DIK_8,"PC_8"},
	{DIK_9,"PC_9"},
	{DIK_0,"PC_0"},
	{DIK_MINUS,"PC_MINUS"},
	{DIK_EQUALS,"PC_EQUALS"},
	{DIK_BACK,"PC_BACKSPACE"},
	{DIK_INSERT,"PC_INSERT"},
	{DIK_HOME,"PC_HOME"},
	{DIK_PRIOR,"PC_PGUP"},
	{DIK_TAB,"PC_TAB"},
	{DIK_Q,"PC_Q"},
	{DIK_W,"PC_W"},
	{DIK_E,"PC_E"},
	{DIK_R,"PC_R"},
	{DIK_T,"PC_T"},
	{DIK_Y,"PC_Y"},
	{DIK_U,"PC_U"},
	{DIK_I,"PC_I"},
	{DIK_O,"PC_O"},
	{DIK_P,"PC_P"},
	{DIK_LBRACKET,"PC_OPENBRACE"},
	{DIK_RBRACKET,"PC_CLOSEBRACE"},
	{DIK_RETURN,"PC_ENTER"},
	{DIK_DELETE,"PC_DELETE"},
	{DIK_END,"PC_END"},
	{DIK_NEXT,"PC_PGDN"},
	{DIK_CAPITAL,"PC_CAPSLOCK"},
	{DIK_A,"PC_A"},
	{DIK_S,"PC_S"},
	{DIK_D,"PC_D"},
	{DIK_F,"PC_F"},
	{DIK_G,"PC_G"},
	{DIK_H,"PC_H"},
	{DIK_J,"PC_J"},
	{DIK_K,"PC_K"},
	{DIK_L,"PC_L"},
	{DIK_SEMICOLON,"PC_SEMICOLON"},
	{DIK_APOSTROPHE,"PC_QUOTE"},
	{DIK_BACKSLASH,"PC_HASH"},
	{DIK_LSHIFT,"PC_LEFTSHIFT"},
	{DIK_OEM_102,"PC_BACKSLASH"},
	{DIK_Z,"PC_Z"},
	{DIK_X,"PC_X"},
	{DIK_C,"PC_C"},
	{DIK_V,"PC_V"},
	{DIK_B,"PC_B"},
	{DIK_N,"PC_N"},
	{DIK_M,"PC_M"},
	{DIK_COMMA,"PC_COMMA"},
	{DIK_PERIOD,"PC_STOP"},
	{DIK_SLASH,"PC_SLASH"},
	{DIK_RSHIFT,"PC_RIGHTSHIFT"},
	{DIK_LCONTROL,"PC_LEFTCTRL"},
	{DIK_LWIN,"PC_LEFT95"},
	{DIK_LMENU,"PC_ALT"},
	{DIK_SPACE,"PC_SPACE"},
	{DIK_RMENU,"PC_ALTGR"},
	{DIK_RWIN,"PC_RIGHT95"},
	{DIK_APPS,"PC_MENU95"},
	{DIK_RCONTROL,"PC_RIGHTCTRL"},
	{DIK_UP,"PC_UP"},
	{DIK_DOWN,"PC_DOWN"},
	{DIK_LEFT,"PC_LEFT"},
	{DIK_RIGHT,"PC_RIGHT"},
	{DIK_NUMLOCK,"PC_NUMLOCK"},
	{DIK_DIVIDE,"PC_KP_SLASH"},
	{DIK_MULTIPLY,"PC_KP_STAR"},
	{DIK_SUBTRACT,"PC_KP_MINUS"},
	{DIK_ADD,"PC_KP_PLUS"},
	{DIK_NUMPADENTER,"PC_KP_ENTER"},
	{DIK_NUMPAD0,"PC_KP_0"},
	{DIK_NUMPAD1,"PC_KP_1"},
	{DIK_NUMPAD2,"PC_KP_2"},
	{DIK_NUMPAD3,"PC_KP_3"},
	{DIK_NUMPAD4,"PC_KP_4"},
	{DIK_NUMPAD5,"PC_KP_5"},
	{DIK_NUMPAD6,"PC_KP_6"},
	{DIK_NUMPAD7,"PC_KP_7"},
	{DIK_NUMPAD8,"PC_KP_8"},
	{DIK_NUMPAD9,"PC_KP_9"},
	{DIK_DECIMAL,"PC_KP_STOP"},
};
static const unsigned num_pc_key_details=sizeof pc_key_details/sizeof pc_key_details[0];

const char *HostInp_KeyNameFromCode(unsigned key_code) {
	unsigned i;

	for(i=0;i<num_pc_key_details;++i) {
		if(pc_key_details[i].key_code==key_code) {
			return pc_key_details[i].key_name;
		}
	}
	return "";
}

int HostInp_CodeFromKeyName(const char *key_name) {
	unsigned i;

	for(i=0;i<num_pc_key_details;++i) {
		if(stricmp(pc_key_details[i].key_name,key_name)==0) {
			return pc_key_details[i].key_code;
		}
	}
	return HostInp_bad_key;
}

