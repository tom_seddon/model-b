#include "host_dx_versions.h"
#include <windows.h>
#include <dsound.h>
#include <math.h>
#include "HostSnd.h"
#include "host_internal.h"

//////////////////////////////////////////////////////////////////////////
struct HostSndState {
	HWND owner;
#ifdef DX3_ONLY
	IDirectSound *dsound;
	IDirectSoundBuffer *buffer;
#else
	IDirectSound8 *dsound;
	IDirectSoundBuffer8 *buffer;
#endif
	DSBUFFERDESC bd;
	WAVEFORMATEX fmt;
	DWORD last_play_cursor;
	int playing;
	int locked;
	void *lp0,*lp1;
	DWORD lnum0,lnum1;
	int volume;//0-100
	LONG dsound_volume;//dsound logarithmic
};

//////////////////////////////////////////////////////////////////////////
// Updates volume for given HostSndState. state->volume is the desired volume
// (linear scale, 0-100). Updates state->dsound_volume and, if the buffer is
// created, sets the buffer's volume too.
static void HostSnd_UpdateVolume(HostSndState *state) {
	HOST_ASSERT(DSBVOLUME_MIN==-10000&&DSBVOLUME_MAX==0);//Yes, I _am_ paranoid.
	state->dsound_volume=DSBVOLUME_MIN;
	if(state->volume>0) {
		state->dsound_volume=(LONG)(-10000/log(1/100.)*log(state->volume/100.));
		//printf("volume=%d; ds volume=%ld\n",state->volume,state->dsound_volume);
	}
	//IDirectSoundBuffer_SetVolume(state->buffer,ds_volume);
	//	dprintf("HostSnd_UpdateVolume: volume=%d state->dsound_volume=%ld\n",state->volume,state->dsound_volume);
	if(state->buffer) {
		//LONG set_volume;
		HRESULT hr;

		IDirectSoundBuffer_SetVolume(state->buffer,DSBVOLUME_MIN);
		//		state->buffer->SetVolume(DSBVOLUME_MIN);
		hr=IDirectSoundBuffer_SetVolume(state->buffer,state->dsound_volume);
		//		IDirectSoundBuffer_GetVolume(state->buffer,&set_volume);
		//		state->buffer->GetVolume(&set_volume);
		//		dprintf("HostSnd_UpdateVolume:     volume set, hr=0x%08X, GetVolume=%ld\n",hr,set_volume);
	}
}

//////////////////////////////////////////////////////////////////////////
// Resets the given state. Stops playing, frees DirectSound and buffers.
static void HostSnd_StopInternal(HostSndState *state) {
	HOST_ASSERT(state);
	HostSnd_PlayStop(state);
	SAFE_RELEASE(state->buffer);
	SAFE_RELEASE(state->dsound);
}

//////////////////////////////////////////////////////////////////////////
// Restarts the given state.
static int HostSnd_StartInternal(HostSndState *state) {
	HRESULT hr;

	HOST_ASSERT(!state->dsound);
#ifdef DX3_ONLY
	hr=DXCHK(DirectSoundCreate(0,&state->dsound,0));
#else
	hr=DXCHK(DirectSoundCreate8(0,&state->dsound,0));
#endif
	if(FAILED(hr)) {
		goto stop_error;
	}
	hr=DXCHK(IDirectSound_SetCooperativeLevel(state->dsound,state->owner,DSSCL_NORMAL));
	if(FAILED(hr)) {
		goto stop_error;
	}
	return 1;
stop_error:
	HostSnd_StopInternal(state);
	return 0;
}
/*
HWND owner;
IDirectSound *dsound;
IDirectSoundBuffer *buffer;
DSBUFFERDESC bd;
WAVEFORMATEX fmt;
DWORD last_play_cursor;
int playing;
int locked;
void *lp0,*lp1;
DWORD lnum0,lnum1;
*/
HostSndState *HostSnd_Start(void *window) {
	HostSndState *state=malloc(sizeof *state);

	dprintf("HostSnd_Start: setting up.\n");
	state->owner=window;
	state->dsound=0;
	state->buffer=0;
	state->playing=0;
	state->locked=0;
	if(!HostSnd_StartInternal(state)) {
		dprintf("HostSnd_Start: HostSnd_StartInternal failed.\n");
		free(state);
		return 0;
	}
	HostSnd_SetVolume(state,0);
	dprintf("HostSnd_Start: ok, state=0x%p.\n",state);
	return state;
}

void HostSnd_Stop(HostSndState *state) {
	dprintf("HostSnd_Stop: state=0x%p\n",state);
	if(state) {
		HostSnd_StopInternal(state);
		free(state);
	}
}

void HostSnd_SetSoundParameters(HostSndState *state,int hz,int bits,int channels,
								int len_samples)
{
	HRESULT hr;
	void *p[2];
	DWORD num[2];
	int silence;
	IDirectSoundBuffer *sound_buffer=0;

	dprintf("HostSnd_SetSoundParameters: hz=%d bits=%d channels=%d len_samples=%d\n",
		hz,bits,channels,len_samples);

	HOST_ASSERT(state->dsound);//HostSnd_Start must have been called first.
	HostSnd_PlayStop(state);
	SAFE_RELEASE(state->buffer);

	state->fmt.wFormatTag=WAVE_FORMAT_PCM;
	state->fmt.nChannels=(WORD)channels;
	state->fmt.nSamplesPerSec=hz;
	state->fmt.wBitsPerSample=(WORD)bits;

	state->fmt.nBlockAlign=state->fmt.nChannels*state->fmt.wBitsPerSample/8;
	state->fmt.nAvgBytesPerSec=state->fmt.nSamplesPerSec*state->fmt.nBlockAlign;
	state->fmt.cbSize=0;

	state->bd.dwBufferBytes=len_samples*state->fmt.nBlockAlign;
	state->bd.dwFlags=DSBCAPS_GETCURRENTPOSITION2|DSBCAPS_CTRLVOLUME|
		DSBCAPS_LOCSOFTWARE;
	state->bd.dwReserved=0;
	state->bd.dwSize=sizeof state->bd;
	state->bd.lpwfxFormat=&state->fmt;
#ifndef DX3_ONLY
	memset(&state->bd.guid3DAlgorithm,0,sizeof state->bd.guid3DAlgorithm);
#endif

	dprintf("HostSnd_SetSoundParameters: IDirectSound_CreateSoundBuffer...\n");
	hr=IDirectSound_CreateSoundBuffer(state->dsound,&state->bd,&sound_buffer,0);
	if(FAILED(hr)) {
		dprintf("HostSnd_SetSoundParameters: failed %s\n",DXGetErrorString(hr));
		return;
	}
#ifdef DX3_ONLY
	state->buffer=sound_buffer;
#else
	dprintf("HostSnd_SetSoundParameters: IDirectSound_QueryInterface...\n");
	hr=IDirectSound_QueryInterface(sound_buffer,&IID_IDirectSoundBuffer8,&state->buffer);
	SAFE_RELEASE(sound_buffer);// must always release [14/11/2003]
	if(FAILED(hr)) {
		return;
	}
#endif

	dprintf("HostSnd_SetSoundParameters: fill buffer with silence...\n");
	hr=IDirectSoundBuffer_Lock(state->buffer,0,0,&p[0],&num[0],&p[1],&num[1],
		DSBLOCK_ENTIREBUFFER);
	if(FAILED(hr)) {
		dprintf("HostSnd_SetSoundParameters:     IDirectSoundBuffer_Lock failed: %s\n",
			DXGetErrorString(hr));
		SAFE_RELEASE(state->buffer);
		return;
	}

	silence=bits==8?0x80:0;
	memset(p[0],silence,num[0]);
	memset(p[1],silence,num[1]);

	DXCHK(IDirectSoundBuffer_Unlock(state->buffer,p[0],num[0],p[1],num[1]));

	HostSnd_UpdateVolume(state);

	state->last_play_cursor=0;
	dprintf("HostSnd_SetSoundParameters: ok.\n");
}

void HostSnd_PlayStart(HostSndState *state) {
	HOST_ASSERT(state);
	/*	
	LONG tmp=-1;

	if(state->buffer) {
	state->buffer->GetVolume(&tmp);
	}
	dprintf("HostSnd_PlayStart: state->buffer=0x%p state->playing=%d curvol=%ld\n",
	state->buffer,state->playing,tmp);
	*/
	HostSnd_UpdateVolume(state);
	if(state->buffer&&!state->playing) {
		IDirectSoundBuffer_Play(state->buffer,0,0,DSBPLAY_LOOPING);
		state->playing=1;
	}
}

void HostSnd_PlayStop(HostSndState *state) {
	HOST_ASSERT(state);
	//	dprintf("HostSnd_PlayStop: state->buffer=0x%p state->playing=%d\n",
	//		state->buffer,state->playing);
	if(state->buffer&&state->playing) {
		IDirectSoundBuffer_Stop(state->buffer);
		state->playing=0;
	}
}

int HostSnd_GetWriteArea(HostSndState *state,void **p1,unsigned *len1,
						 void **p2,unsigned *len2)
{
	unsigned num_bytes;
	DWORD this_play;
	HRESULT hr;

	HOST_ASSERT(state);
	HOST_ASSERT(state->buffer);

	if(!state->playing) {
		return 0;
	}

	hr=DXCHK(IDirectSoundBuffer_GetCurrentPosition(state->buffer,&this_play,0));
	if(FAILED(hr)) {
		return -1;
	}
	if(this_play>=state->last_play_cursor) {
		num_bytes=this_play-state->last_play_cursor;
	} else {
		num_bytes=(this_play+state->bd.dwBufferBytes)-state->last_play_cursor;
	}
	if(num_bytes==0) {
		return 0;
	}
	hr=DXCHK(IDirectSoundBuffer_Lock(state->buffer,state->last_play_cursor,num_bytes,
		&state->lp0,&state->lnum0,&state->lp1,&state->lnum1,0));
	if(FAILED(hr)) {
		return -1;
	}
	state->locked=1;
	if(p1) {
		*p1=state->lp0;
	}
	if(len1) {
		*len1=state->lnum0;
	}
	if(p2) {
		*p2=state->lp1;
	}
	if(len2) {
		*len2=state->lnum1;
	}
	state->last_play_cursor=this_play;
	return 1;
}

void HostSnd_CommitWriteArea(HostSndState *state) {
	if(state->locked) {
		DXCHK(IDirectSoundBuffer_Unlock(state->buffer,state->lp0,state->lnum0,
			state->lp1,state->lnum1));
		state->locked=0;
	}
}

void HostSnd_SetVolume(HostSndState *state,int volume) {

	HOST_ASSERT(volume>=0&&volume<=100);
	HOST_ASSERT(!state->locked);
	//http://download.insomniavisions.com/sources/uncompressed/c/out_ivdsound/plugin.c

	state->volume=volume;
	dprintf("HostSnd_SetVolume: state->volume=%d now\n",state->volume);
	HostSnd_UpdateVolume(state);

	return;
}

