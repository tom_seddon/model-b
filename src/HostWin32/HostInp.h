#ifndef HOSTINP_H_
#define HOSTINP_H_

#include <HostInpShared.h>

#ifdef __cplusplus
extern "C" {
#endif

//Start input system attached to given window.
HostInpState *HostInp_Start(void *window);

#ifdef __cplusplus
}
#endif

#endif
