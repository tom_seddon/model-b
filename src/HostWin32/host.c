#include <port.h>
#include <windows.h>
#include <stdio.h>
#include "Host.h"
#include "host_internal.h"

//////////////////////////////////////////////////////////////////////////
//
unsigned host_global_flags=0;

void Host_SetGlobalFlags(unsigned new_global_flags) {
	host_global_flags=new_global_flags;
}

//////////////////////////////////////////////////////////////////////////
//
static Host_LogOutputCallbackFn log_output_fn=0;

void Host_SetLogOutputCallback(Host_LogOutputCallbackFn callback) {
	log_output_fn=callback;
}

int dprintf(const char *fmt,...) {
	int n=0;

	if(log_output_fn) {
		char tmpbuf[1024];
		va_list v;

		va_start(v,fmt);
		n=_vsnprintf(tmpbuf,sizeof tmpbuf,fmt,v);
		va_end(v);

		(*log_output_fn)(tmpbuf);
	}

	return n;
}

//////////////////////////////////////////////////////////////////////////

HRESULT RealDXCHK(HRESULT hr,const char *what,const char *file,unsigned line,
	const char *function)
{
	if(FAILED(hr)) {
		int indent;

		indent=dprintf("%s(%u): %s: ",file,line,function);
		dprintf("FAILED(%s)\n",what);
		dprintf("%-*s0x%08X: %s\n",indent,"",hr,DXGetErrorString(hr));
	}
	return hr;
}
