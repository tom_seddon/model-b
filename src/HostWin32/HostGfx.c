#include <port.h>
#include "host_dx_versions.h"
#include "host_internal.h"
#include <windows.h>
#include <windowsx.h>
#include <crtdbg.h>
#include <ddraw.h>
#include <stddef.h>
//#include <xmmintrin.h>
#include "HostGfx.h"
#include "HostGfxConvert.h"

//Right now, this only affects this file...
#define mbENABLE_FULL_SCREEN

//This is too slow for general use.
//#define CHECK_PIXEL_VALUES

// check some timings
//#define CHECK_TIMINGS

//If defined, align HostGfxState to the given number of bits.
#define HOSTGFX_STATE_ALIGNMENT_BITS (5)

#ifdef HOSTGFX_STATE_ALIGNMENT_BITS
# define HOSTGFX_STATE_ALIGNMENT_BYTES (1<<(HOSTGFX_STATE_ALIGNMENT_BITS))
#endif

// experimental assembly language optimisations

// "opt" should be in quotes...
//#define ASM_OPT

// "opt" should be in quotes...
//#define MMX_OPT


//////////////////////////////////////////////////////////////////////////

//the 'list' returned if there are no refresh rates.
static const int empty_refresh_rate_list=0;

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

enum {
	FULL_SCREEN_WIDTH=800,
	FULL_SCREEN_HEIGHT=600,
	MAX_FAILED_STARTS=2,
};

//////////////////////////////////////////////////////////////////////////

//Bits 2-0 of this value should be clear.
//It is the base of the 8 palette entries the emulator will use.
//This way the system palette can be left as-is, allowing the display
//of system widgets in full screen mode.
enum HostGfx_Config {
	HostGfx_FULL_SCREEN_PALETTE_BASE=128,
};
typedef enum HostGfx_Config HostGfx_Config;

//////////////////////////////////////////////////////////////////////////
//
#ifdef DX3_ONLY
typedef IDirectDraw2 IDirectDrawN;
typedef IDirectDrawSurface2 IDirectDrawSurfaceN;
typedef DDSURFACEDESC DDSURFACEDESCN;
#else
typedef IDirectDraw7 IDirectDrawN;
typedef IDirectDrawSurface7 IDirectDrawSurfaceN;
typedef DDSURFACEDESC2 DDSURFACEDESCN;
#endif

//////////////////////////////////////////////////////////////////////////

// http://blogs.msdn.com/b/chuckw/archive/2010/06/16/wither-directdraw.aspx
// - seems you're expected to load ddraw.dll by hand these days and extract all
//   the functions yourself.
//
// progress!

typedef HRESULT (WINAPI *LPDIRECTDRAWCREATE)(GUID FAR *lpGUID,LPDIRECTDRAW FAR *lplpDD,IUnknown FAR *pUnkOuter );
typedef HRESULT (WINAPI *LPDIRECTDRAWCREATEEX)(GUID FAR * lpGuid,LPVOID  *lplpDD,REFIID  iid,IUnknown FAR *pUnkOuter );
typedef HRESULT (WINAPI *LPDIRECTDRAWCREATECLIPPER)(DWORD dwFlags,LPDIRECTDRAWCLIPPER FAR *lplpDDClipper,IUnknown FAR *pUnkOuter );
typedef HRESULT (WINAPI *LPDIRECTDRAWENUMERATEW)(LPDDENUMCALLBACKW lpCallback,LPVOID lpContext );
typedef HRESULT (WINAPI *LPDIRECTDRAWENUMERATEA)(LPDDENUMCALLBACKA lpCallback,LPVOID lpContext );
typedef HRESULT (WINAPI *LPDIRECTDRAWENUMERATEEXW)(LPDDENUMCALLBACKEXW lpCallback,LPVOID lpContext,DWORD dwFlags);
typedef HRESULT (WINAPI *LPDIRECTDRAWENUMERATEEXA)(LPDDENUMCALLBACKEXA lpCallback,LPVOID lpContext,DWORD dwFlags);

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// HostGfx
//
enum {
	max_num_back_buffers=3
};

#ifndef DX3_ONLY
struct AdapterPrivate {
	int has_guid;
	GUID guid_data;
	HMONITOR hm;
};
typedef struct AdapterPrivate AdapterPrivate;
#endif

#ifdef CHECK_TIMINGS
struct TimingData {
	qword total;
	int count;
};
typedef struct TimingData TimingData;

static __inline void TimingData_Reset(TimingData *td) {
	td->total=0;
	td->count=0;
}

static __inline void TimingData_Update(TimingData *td,qword num_cycles) {
	td->total+=num_cycles;
	++td->count;
}

static void TimingData_Print(const TimingData *td,const char *name) {
	if(td->count%200==0&&td->count!=0) {
		dprintf("timing: %s: total=%I64u count=%d, avg=%I64u.\n",name,td->total,td->count,td->total/td->count);
	}
}

#define TD(X) X

#else

#define TD(X)

#endif

struct HostGfxState {
	//palettes
	byte convert_table[256+7];
	float palette[8][3];

	HostGfxBufferFormat best_fmt;
	HostGfx_ConvertFn convert_fn;
	HostGfx_RefreshConvertTableFn refresh_convert_table_fn;

	//Owner. Used by DirectDraw. Must be toplevel window.
	HWND owner;
	//Target. Display is shown in this window, so could have
	//child window here.
	HWND target;
	IDirectDrawN *ddraw;
	IDirectDrawSurfaceN *primary;
	DDSURFACEDESCN primary_desc;
	IDirectDrawSurfaceN *back_buffers[max_num_back_buffers];
	int num_back_buffers;
	DDSURFACEDESCN back_buffer_desc;
	IDirectDrawClipper *primary_clipper;
	IDirectDrawPalette *primary_palette;
	int cpu_buffer,gpu_buffer;
	int buffers_ok;
	//HostGfx_BeebScreenType beeb_screen_type;
	//HostGfx_ScanlinesMode scanlines_mode;//bool vertical_2x;

	int full_screen;
	int full_screen_refresh_rate;
	int full_screen_adapter_index;

	unsigned present_count;
#ifdef HOST_DEBUG
	qword present_ticks_total;
#endif

	DDCAPS halcaps;

	//ends with 0.
	//int *refresh_rates;

	//this is bumped each time a start fails for a particular buffer size.
	//each time the Start fails, this value is bumped.
	//if failures happen too often, the system will stop trying to start itself until
	//the screen size changes or we go into full screen.
	//
	//we accept a few failures to cope with transient problems. in no way has this
	//anything to do with my having rather lost track of exactly what gets called
	//from where and how many times the init may get run.
	//
	//if we go into full screen, but that fails, the system tries again in windowed
	//mode, so the process restarts. 
	//
	//in the failure situation, 'present' will ignore the buffer and instead draw
	//a message on the target window using standard Win32 functions.
	int num_failed_starts;

	int last_present_w,last_present_h;

	//pointer to real start of the heap block this structure lives in, in
	//case it's different from the start of the structure.
	void *real_address;

	//clip list from the clipper.
	RGNDATA *clip_list;
	DWORD clip_list_size;

	//whether SSE is available
	int has_sse;

	//devices and stuff.
#ifdef DX3_ONLY
	HostGfx_Adapter primary_adapter;
#else
	HostGfx_Adapter **adapters;
	AdapterPrivate *adapters_private;
	int num_adapters;
#endif

	// timings
	TD(TimingData blit_td;)
	TD(TimingData wait_vb_td;)
	TD(TimingData convert_td;)
	TD(TimingData lock_td;)

	HMODULE ddraw_dll_h;

	LPDIRECTDRAWCREATE dd_create_fn;
	LPDIRECTDRAWCREATEEX dd_createex_fn;
	LPDIRECTDRAWCREATECLIPPER dd_createclipper_fn;
	LPDIRECTDRAWENUMERATEW dd_enumeratea_fn;
	LPDIRECTDRAWENUMERATEA dd_enumeratew_fn;
	LPDIRECTDRAWENUMERATEEXW dd_enumaretexw_fn;
	LPDIRECTDRAWENUMERATEEXA dd_enumaretexa_fn;
};

//think this should be offsetof(HostGfxState,convert_table[0]), but compiler didn't accept it
extern const int check_convert_table[offsetof(HostGfxState,convert_table)==0];

//////////////////////////////////////////////////////////////////////////

static void GetBitfield(DWORD value,int *shift,int *count) {
	*shift=0;
	*count=0;
	if(value!=0) {
		while(!(value&1)) {
			++*shift;
			value>>=1;
		}
		while(value&1) {
			++*count;
			value>>=1;
		}
	}
}

#define RECT_WIDTH(R) ((R)->right-(R)->left)
#define RECT_HEIGHT(R) ((R)->bottom-(R)->top)

static void GetRectIntersection(RECT *dest,const RECT *r1,const RECT *r2) {
	//IntersectRect(dest,r1,r2);
	int x_disjoint,y_disjoint;

	dest->left=0;
	dest->right=0;
	dest->top=0;
	dest->bottom=0;

	//early outs
	x_disjoint=r1->left>=r2->right||r2->left>=r1->right;
	y_disjoint=r1->top>=r2->bottom||r2->top>=r1->bottom;
	if(x_disjoint||y_disjoint) {
		return;
	}

	//plonk
	dest->left=max(r1->left,r2->left);
	dest->right=min(r1->right,r2->right);
	dest->top=max(r1->top,r2->top);
	dest->bottom=min(r1->bottom,r2->bottom);
}

static void DrawRect(byte *dest_bitmap,unsigned dest_pitch,const RECT *dest_rect,byte outline,int interior) {
	int y;
	byte *dest;

	if(RECT_WIDTH(dest_rect)>0&&RECT_HEIGHT(dest_rect)>0) {
		dest=dest_bitmap+dest_rect->top*dest_pitch+dest_rect->left;

		//draw the box
		for(y=0;y<RECT_HEIGHT(dest_rect);++y) {
			if(y==0||y==RECT_HEIGHT(dest_rect)-1) {
				memset(dest,outline,RECT_WIDTH(dest_rect));
			} else {
				dest[0]=outline;
				if(RECT_WIDTH(dest_rect)>2&&interior>=0) {
					memset(dest+1,interior,RECT_WIDTH(dest_rect)-2);
				}
				dest[RECT_WIDTH(dest_rect)-1]=outline;
			}
			dest+=dest_pitch;
		}
	}
}

//clears the intersection of r1 and r2. dest_bitmap is 8pp.
static void HostGfx_ClearRectIntersection(byte *dest_bitmap,unsigned dest_pitch,const RECT *r1,const RECT *r2) {
	RECT dest_rect;

	GetRectIntersection(&dest_rect,r1,r2);

	if(RECT_WIDTH(&dest_rect)>0&&RECT_HEIGHT(&dest_rect)>0) {
		int y;
		byte *dest;

		dest=dest_bitmap+dest_rect.top*dest_pitch+dest_rect.left;
		for(y=0;y<RECT_HEIGHT(&dest_rect);++y) {
			memset(dest,0,RECT_WIDTH(&dest_rect));
			dest+=dest_pitch;
		}
	}
}

//HostGfx_CopyRect8(state,dest,dest_pitch,src,RECT_WIDTH(dest_rect),num_whole_lines,gap_height,src_pitch);
static void HostGfx_CopyRect8(HostGfxState *state,byte *dest,unsigned dest_pitch,const byte *src,int srcy,int width,int num_whole_lines,
	int line_height,int gap_height,unsigned src_pitch)
{
	int y;

	(void)srcy;//???
	(void)state;

	for(y=0;y<num_whole_lines;++y) {
		int subline;

		for(subline=0;subline<line_height;++subline) {
			memcpy(dest,src,width);
			dest+=dest_pitch;
		}

		//this might pop us over the end of the buffer, but we won't actually write to that,
		//so it's ok.
		dest+=gap_height*dest_pitch;

		src+=src_pitch;
	}
}

//for full screen only. draws directly to front buffer, so must clip manually against the clipper's clip list.
//as an additional complication, the source bitmap is automatically centred horizontally.
//
//TODO take into account present_hints, by clearing areas that are in the clip list but outside the src bitmap.
static void HostGfx_Copy8(HostGfxState *state,byte *dest_bitmap,unsigned dest_pitch,
	const byte *src_bitmap,int src_width,unsigned src_pitch,int src_height,
	int y_first,int line_height,int gap_height,unsigned present_hints)
{
	const RGNDATAHEADER *clip_list_hdr;
	DWORD clip_list_size;
	HRESULT hr;
	int src_line_dest_height=line_height+gap_height;
	RECT entire_dest_rect;
	int entire_dest_dx,entire_dest_dy;
	RECT tidy_top,tidy_bottom,tidy_left,tidy_right;

	HOST_ASSERT(y_first<=gap_height);

	entire_dest_dx=(FULL_SCREEN_WIDTH-src_width)/2;
	entire_dest_dy=y_first;

	entire_dest_rect.left=entire_dest_dx;
	entire_dest_rect.top=entire_dest_dy;
	entire_dest_rect.right=entire_dest_rect.left+src_width;
	entire_dest_rect.bottom=entire_dest_rect.top+src_height*src_line_dest_height;

	hr=IDirectDrawClipper_GetClipList(state->primary_clipper,0,0,&clip_list_size);
	if(FAILED(hr)) {
		dprintf("%s: IDirectDrawClipper_GetClipList (get clip list size) failed.\n",__FUNCTION__);
		return;
	}

	if(clip_list_size>state->clip_list_size) {
		RGNDATA *new_clip_list=realloc(state->clip_list,clip_list_size);
		if(new_clip_list) {
			dprintf("%s: clip list now %u bytes, was %u\n",__FUNCTION__,clip_list_size,state->clip_list_size);
			state->clip_list=new_clip_list;
			state->clip_list_size=clip_list_size;
		}
	}

	//docs are unclear, will this update *lpdwSize? don't want it too (might be larger than current
	//clip list)
	hr=IDirectDrawClipper_GetClipList(state->primary_clipper,0,state->clip_list,&clip_list_size);
	if(FAILED(hr)) {
		dprintf("%s: IDirectDrawClipper_GetClipList (get clip list itself) failed.\n",__FUNCTION__);
		return;
	}

	clip_list_hdr=&state->clip_list->rdh;
	if(clip_list_hdr->iType==RDH_RECTANGLES) {
		DWORD i;
		const RECT *rects=(RECT *)state->clip_list->Buffer;

		if(present_hints&HostGfx_PRESENT_HINT_TIDY) {
			//Top (and top left/top right corners)
			tidy_top.left=0;
			tidy_top.right=FULL_SCREEN_WIDTH;
			tidy_top.top=0;
			tidy_top.bottom=entire_dest_rect.top;

			//Bottom (and bottom left/bottom right corners)
			tidy_bottom.left=0;
			tidy_bottom.right=FULL_SCREEN_WIDTH;
			tidy_bottom.top=entire_dest_rect.bottom;
			tidy_bottom.bottom=FULL_SCREEN_HEIGHT;

			//Left
			tidy_left.left=0;
			tidy_left.right=entire_dest_rect.left;
			tidy_left.top=entire_dest_rect.top;
			tidy_left.bottom=entire_dest_rect.bottom;

			//Right
			tidy_right.left=entire_dest_rect.right;
			tidy_right.right=FULL_SCREEN_WIDTH;
			tidy_right.top=entire_dest_rect.top;
			tidy_right.bottom=entire_dest_rect.bottom;
		}

		//		dprintf(__FUNCTION__ ": Gtc=%u nCount=%u\n",GetTickCount(),clip_list_hdr->nCount);
		for(i=0;i<clip_list_hdr->nCount;++i) {
			RECT dest_rect;
			static byte value=0;

			//			DrawRect(dest_bitmap,dest_pitch,&rects[i],2,4);
			GetRectIntersection(&dest_rect,&rects[i],&entire_dest_rect);
			if(dest_rect.right-dest_rect.left>0&&dest_rect.bottom-dest_rect.top>0) {
				int num_whole_lines;
				const byte *src;
				byte *dest;
				int src_dx,src_dy;
				int dest_dx,dest_dy;
				int y;

				//Fill this rect in then.
				//This was going to be more complicated (correct handling of partial lines, etc.),
				//but it ended up dead simple instead.

				//Determine source
				src_dx=dest_rect.left-entire_dest_dx;
				src_dy=(dest_rect.top-entire_dest_dy)/src_line_dest_height;

				src=src_bitmap;
				src+=src_dx;
				src+=src_dy*src_pitch;

				//And dest
				dest_dx=dest_rect.left;
				dest_dy=dest_rect.top;

				dest=dest_bitmap;
				dest+=dest_dx;
				dest+=dest_dy*dest_pitch;

				//this isn't _quite_ right :) I think this is what
				//DirectDraw does anyway, though, judging by the weird
				//artefacts when moving dialogs over a stretched bitmap.
                //However, I'm potentially leaving a few lines unwritten at
				//the bottom of the destination rectangle, which DirectDraw
				//doesn't. Fortunately, only a cosmetic thing.
				num_whole_lines=(dest_rect.bottom-dest_rect.top)/src_line_dest_height;

				HostGfx_CopyRect8(state,dest,dest_pitch,src,src_dy,RECT_WIDTH(&dest_rect),num_whole_lines,line_height,gap_height,src_pitch);

				//and tidy the gaps, if there are any.
				if((present_hints&HostGfx_PRESENT_HINT_TIDY)&&gap_height>0) {
					dest=dest_bitmap;
					dest+=dest_dx;
					dest+=dest_dy*dest_pitch;

					for(y=0;y<num_whole_lines;++y) {
						int gapline;

						dest+=line_height*dest_pitch;

						for(gapline=0;gapline<gap_height;++gapline) {
							memset(dest,0,RECT_WIDTH(&dest_rect));
							dest+=dest_pitch;
						}
					}
				}
			}

			//tidy up the edges, if the present hints say so.
			if(present_hints&HostGfx_PRESENT_HINT_TIDY) {
				HostGfx_ClearRectIntersection(dest_bitmap,dest_pitch,&tidy_top,&rects[i]);
				HostGfx_ClearRectIntersection(dest_bitmap,dest_pitch,&tidy_bottom,&rects[i]);
				HostGfx_ClearRectIntersection(dest_bitmap,dest_pitch,&tidy_left,&rects[i]);
				HostGfx_ClearRectIntersection(dest_bitmap,dest_pitch,&tidy_right,&rects[i]);
			}
		}
	}
}

static void HostGfx_Copy8Windowed(HostGfxState *state,byte *dest_bitmap,unsigned dest_pitch,const byte *src_bitmap,int src_width,unsigned src_pitch,int src_height,int y_first,int line_height,int gap_height,unsigned present_hints)
{
	int num_lines;
	int y,i;
	const unsigned char *src;
	char *dest;

	(void)present_hints,(void)state;

	dest=(char *)dest_bitmap+y_first*dest_pitch;
	src=src_bitmap;

	num_lines=src_height*(line_height+gap_height);
	for(y=0;y<src_height;++y) {
		for(i=0;i<line_height;++i) {
			memcpy(dest,src,src_width);
			dest+=dest_pitch;
		}
		dest+=gap_height*dest_pitch;
		src+=src_pitch;
	}
}


//////////////////////////////////////////////////////////////////////////

#ifdef ASM_OPT

// 2 tables (low dword and high dword)
// 8 bytes per entry, 8 entries, 64 bytes/table

static void HostGfx_RefreshConvertTableTo32MMX(HostGfxState *state) {
	DWORD *masks[3];
	int i,j,table;
	unsigned *entry;

	masks[0]=&state->primary_desc.ddpfPixelFormat.dwRBitMask;
	masks[1]=&state->primary_desc.ddpfPixelFormat.dwGBitMask;
	masks[2]=&state->primary_desc.ddpfPixelFormat.dwBBitMask;

	memset(state->convert_table,0,sizeof state->convert_table);

	for(table=0;table<2;++table) {
		for(i=0;i<8;++i) {
			int index;

			index=0;
			for(j=0;j<3;++j) {
				if(i&(1<<j)) {
					index|=state->best_fmt.elems[j].mask;
						//px[0].bits[j];
				}
			}

			index*=sizeof(unsigned)*2;

			entry=(unsigned *)((char *)state->convert_table+index+table*64);

			for(j=0;j<3;++j) {
				if(i&(1<<j)) {
					entry[table]|=*masks[j];
				}
				entry[table^1]=0;
			}
		}
	}
}

void HostGfx_ConvertTo32MMX(HostGfxState *state,byte *dest_bitmap,unsigned dest_pitch,const byte *src_bitmap,int src_width,
	unsigned src_pitch,int src_height,int y_first,int line_height,int gap_height,unsigned present_hints)
{
	if(src_pitch&31) {
		HostGfx_ConvertTo32(state,dest_bitmap,dest_pitch,src_bitmap,src_width,src_pitch,src_height,y_first,line_height,
			gap_height,present_hints);
	} else {
		int num_lines;
		int y,i;//,x;
		const unsigned char *src;
		char *dest;

		dest=(char *)dest_bitmap+y_first*dest_pitch;
		src=src_bitmap;
		num_lines=src_height*(line_height+gap_height);
		for(y=0;y<src_height;++y) {
			for(i=0;i<line_height;++i) {
				//			00046	0f b6 2c 30	 movzx	 ebp, BYTE PTR [eax+esi]
				//			0004a	8b 2c af	 mov	 ebp, DWORD PTR [edi+ebp*4]
				//			0004d	89 2c 81	 mov	 DWORD PTR [ecx+eax*4], ebp
				//				00050	40		 inc	 eax
				//				00051	3b c2		 cmp	 eax, edx
				//				00053	7c f1		 jl	 SHORT $L77000
				//			void *convtbl=state->convert_table;
				__asm {
					mov ecx,src_width

					mov ebx,state

					mov esi,src
					add esi,ecx

					mov edi,dest
					lea edi,[edi+ecx*4]

					shr ecx,1
					neg ecx

					//sub eax,eax
					lea eax,[ebx+64]
					sub edx,edx

					align 16
ConvertTo32Loop:
					mov dl,[esi+ecx*2];
					movd mm0,[ebx+edx*8]
					mov dl,[esi+ecx*2+1];
					por mm0,[eax+edx*8]

					mov dl,[esi+ecx*2+2];
					movd mm1,[ebx+edx*8]
					mov dl,[esi+ecx*2+3];
					por mm1,[eax+edx*8]

					mov dl,[esi+ecx*2+4];
					movd mm2,[ebx+edx*8]
					mov dl,[esi+ecx*2+5];
					por mm2,[eax+edx*8]

					mov dl,[esi+ecx*2+6];
					movd mm3,[ebx+edx*8]
					mov dl,[esi+ecx*2+7];
					por mm3,[eax+edx*8]

					//inc ecx
					add ecx,4

					movntq [edi+ecx*8+0],mm0
					movntq [edi+ecx*8+8],mm1
					movntq [edi+ecx*8+16],mm2
					movntq [edi+ecx*8+24],mm3

					jnz ConvertTo32Loop

					emms
				}
				dest+=dest_pitch;
			}
			dest+=gap_height*dest_pitch;
			src+=src_pitch;
		}
	}
}

static void HostGfx_RefreshConvertTableTo32Asm(HostGfxState *state) {
	DWORD *masks[3];
	int i,j;
	unsigned *entry;

	masks[0]=&state->primary_desc.ddpfPixelFormat.dwRBitMask;
	masks[1]=&state->primary_desc.ddpfPixelFormat.dwGBitMask;
	masks[2]=&state->primary_desc.ddpfPixelFormat.dwBBitMask;

	memset(state->convert_table,0,sizeof state->convert_table);

	for(i=0;i<8;++i) {
		int index;

		index=0;
		for(j=0;j<3;++j) {
			if(i&(1<<j)) {
				index|=state->best_fmt.elems[j].mask;
			}
		}

		index*=sizeof(unsigned);

		entry=(unsigned *)((char *)state->convert_table+index);

		for(j=0;j<3;++j) {
			if(i&(1<<j)) {
				entry[0]|=*masks[j];
			}
		}
	}
}

//128 bytes, 32 pixels, 16 byte aligned
static dword wc_bufs_buf[32+3];

void HostGfx_ConvertTo32Asm(HostGfxState *state,byte *dest_bitmap,unsigned dest_pitch,const byte *src_bitmap,int src_width,
							unsigned src_pitch,int src_height,int y_first,int line_height,int gap_height,unsigned present_hints)
{
	if((src_pitch&31)||(((unsigned)dest_bitmap)&15)||(dest_pitch&15)) {
		HostGfx_ConvertTo32(state,dest_bitmap,dest_pitch,src_bitmap,src_width,src_pitch,src_height,y_first,line_height,
			gap_height,present_hints);
	} else {
		int num_lines;
		int y,i;//,x;
		const unsigned char *src;
		char *dest;
		dword *wc_bufs;
		
		wc_bufs=(dword *)(((unsigned)wc_bufs_buf+15)&~15);
		dest=(char *)dest_bitmap+y_first*dest_pitch;
		src=src_bitmap;

		num_lines=src_height*(line_height+gap_height);
		for(y=0;y<src_height;++y) {
			for(i=0;i<line_height;++i) {
				int x,j;

				for(x=0;x<src_width;x+=32) {
					for(j=0;j<32;++j) {
						wc_bufs[j]=*(unsigned *)((unsigned *)state->convert_table+src[j+x]);
					}
					__asm {
						mov esi,wc_bufs
						mov edi,dest

						movaps xmm0,[esi+0]
						movaps xmm1,[esi+16]
						movaps xmm2,[esi+32]
						movaps xmm3,[esi+48]
						movaps xmm4,[esi+64]
						movaps xmm5,[esi+80]
						movaps xmm6,[esi+96]
						movaps xmm7,[esi+112]

						movntps [edi+0],xmm0
						movntps [edi+16],xmm1
						movntps [edi+32],xmm2
						movntps [edi+48],xmm3
						movntps [edi+64],xmm4
						movntps [edi+80],xmm5
						movntps [edi+96],xmm6
						movntps [edi+112],xmm7
					}
				}
				dest+=dest_pitch;
/*
				//			00046	0f b6 2c 30	 movzx	 ebp, BYTE PTR [eax+esi]
				//			0004a	8b 2c af	 mov	 ebp, DWORD PTR [edi+ebp*4]
				//			0004d	89 2c 81	 mov	 DWORD PTR [ecx+eax*4], ebp
				//				00050	40		 inc	 eax
				//				00051	3b c2		 cmp	 eax, edx
				//				00053	7c f1		 jl	 SHORT $L77000
				//			void *convtbl=state->convert_table;
				__asm {
					mov ecx,src_width

					mov ebx,state

					mov esi,src
					add esi,ecx

					mov edi,dest
					lea edi,[edi+ecx*4]

					shr ecx,1
					neg ecx

					sub eax,eax
					sub edx,edx

					align 16
//ConvertTo32Loop:
//					mov al,[esi+ecx]
//					mov edx,[ebx+eax*4]
//					mov [edi+ecx*4],edx
//					inc ecx
//					jnz ConvertTo32Loop

ConvertTo32Loop:
					//prefetchnta [esi+ecx*2+32]
					mov ax,[esi+ecx*2];

					movzx edx,al
					mov edx,[ebx+edx*4]
					mov [edi+ecx*8],edx

					movzx edx,ah
					mov edx,[ebx+edx*4]
					mov [edi+ecx*8+4],edx

					inc ecx
					jnz ConvertTo32Loop
				}
				//			for(x=0;x<src_width;++x) {
				//				((unsigned *)dest)[x]=*(unsigned *)((unsigned *)state->convert_table+src[x]);
				//			}
				dest+=dest_pitch;
				*/
			}
			dest+=gap_height*dest_pitch;
			src+=src_pitch;
		}
	}
}

#endif//ASM_OPT

void HostGfx_ConvertTo32_2(HostGfxState *state,unsigned char *dest_bitmap,unsigned dest_pitch,const unsigned char *src_bitmap,int src_width,unsigned src_pitch,int src_height,int y_first,int line_height,int gap_height,unsigned present_hints) {
	int num_lines;
	int y,i,x;
	const unsigned char *src;
	char *dest;
	unsigned *tmp_buffer,tmp_buffer_size;

	(void)present_hints;

	dest=(char *)dest_bitmap+y_first*dest_pitch;
	src=src_bitmap;

	// TODO could overflow (but it won't in practice I hope!)
	tmp_buffer_size=src_width*sizeof *tmp_buffer;
	tmp_buffer=_alloca(tmp_buffer_size);

	num_lines=src_height*(line_height+gap_height);
	for(y=0;y<src_height;++y) {
		for(x=0;x<src_width;++x) {
			tmp_buffer[x]=*(unsigned *)((unsigned *)state->convert_table+src[x]);
		}

		for(i=0;i<line_height;++i) {
			memcpy(dest,tmp_buffer,tmp_buffer_size);
// 			// 			for(x=0;x<src_width;x+=8) {
// 			// 				((unsigned *)dest)[x+0]=*(unsigned *)((unsigned *)state->convert_table+src[x+0]);
// 			// 				((unsigned *)dest)[x+1]=*(unsigned *)((unsigned *)state->convert_table+src[x+1]);
// 			// 				((unsigned *)dest)[x+2]=*(unsigned *)((unsigned *)state->convert_table+src[x+2]);
// 			// 				((unsigned *)dest)[x+3]=*(unsigned *)((unsigned *)state->convert_table+src[x+3]);
// 			// 				((unsigned *)dest)[x+4]=*(unsigned *)((unsigned *)state->convert_table+src[x+4]);
// 			// 				((unsigned *)dest)[x+5]=*(unsigned *)((unsigned *)state->convert_table+src[x+5]);
// 			// 				((unsigned *)dest)[x+6]=*(unsigned *)((unsigned *)state->convert_table+src[x+6]);
// 			// 				((unsigned *)dest)[x+7]=*(unsigned *)((unsigned *)state->convert_table+src[x+7]);
// 			// 			}
// 
// 			for(;x<src_width;++x) {
// 				((unsigned *)dest)[x]=*(unsigned *)((unsigned *)state->convert_table+src[x]);
// 			}
// 
			dest+=dest_pitch;
		}
		dest+=gap_height*dest_pitch;
		src+=src_pitch;
	}
}

//////////////////////////////////////////////////////////////////////////


static IDirectDrawSurfaceN *NewSurface(HostGfxState *state,DDSURFACEDESCN *desc) {
	HRESULT hr;
	DDBLTFX fx;
	IDirectDrawSurfaceN *surface;
#ifdef DX3_ONLY
	IDirectDrawSurface *tmp_surface;
#endif

#ifdef DX3_ONLY
	hr=DXCHK(IDirectDraw_CreateSurface(state->ddraw,desc,&tmp_surface,0));
	if(FAILED(hr)) {
		return 0;
	}

	hr=DXCHK(IDirectDrawSurface_QueryInterface(tmp_surface,&IID_IDirectDrawSurface2,&surface));
	IDirectDrawSurface_Release(tmp_surface);
	if(FAILED(hr)) {
		return 0;
	}
#else
	hr=DXCHK(IDirectDraw_CreateSurface(state->ddraw,desc,&surface,0));
	if(FAILED(hr)) {
		return 0;
	}
#endif

	if(!(desc->ddsCaps.dwCaps&DDSCAPS_PRIMARYSURFACE)) {
		memset(&fx,0,sizeof fx);
		fx.dwSize=sizeof fx;
		fx.dwFillColor=0;
		DXCHK(IDirectDrawSurface_Blt(surface,0,0,0,DDBLT_COLORFILL|DDBLT_WAIT,&fx));
	}

	return surface;
}

static void HostGfx_DeleteBuffers(HostGfxState *state) {
	unsigned i;

	dprintf("HostGfx_DeleteBuffers: Release: primary_clipper=0x%p\n",
		state->primary_clipper);
	SAFE_RELEASE(state->primary_clipper);

	dprintf("HostGfx_DeleteBuffers: Release: primary_palette=0x%p\n",
		state->primary_palette);
	SAFE_RELEASE(state->primary_palette);

	for(i=0;i<max_num_back_buffers;++i) {
		dprintf("HostGfx_DeleteBuffers: Release: back_buffers[%u]=0x%p\n",
			i,state->back_buffers[i]);
		SAFE_RELEASE(state->back_buffers[i]);
	}

	dprintf("HostGfx_DeleteBuffers: Release: primary=0x%p\n",state->primary);
	SAFE_RELEASE(state->primary);
}

static void HostGfx_StopInternal(HostGfxState *state) {
	HOST_ASSERT(state);

#ifdef HOST_DEBUG
	dprintf("HostGfx_StopInternal:avg %.3f cycles/present\n",state->present_ticks_total/(double)state->present_count);
#endif

	if(state->ddraw) {
		if(state->full_screen) {
			IDirectDraw_RestoreDisplayMode(state->ddraw);
		}
		IDirectDraw_SetCooperativeLevel(state->ddraw,0/*state->owner*/,DDSCL_NORMAL);
	}

	HostGfx_DeleteBuffers(state);

	dprintf("HostGfx_StopInternal: Release: ddraw=0x%p\n",state->ddraw);
	SAFE_RELEASE(state->ddraw);

	if(state->ddraw_dll_h) {
		dprintf("HostGfx_StopInternal: unload DLL.\n");

		state->dd_create_fn=0;
		state->dd_createex_fn=0;
		state->dd_createclipper_fn=0;
		state->dd_enumeratea_fn=0;
		state->dd_enumeratew_fn=0;
		state->dd_enumaretexw_fn=0;
		state->dd_enumaretexa_fn=0;

		FreeLibrary(state->ddraw_dll_h);
		state->ddraw_dll_h=0;
	}

	state->num_back_buffers=0;
	state->buffers_ok=0;
}

static int HostGfx_CreateClipper(HostGfxState *state) {//Clipper
	HRESULT hr;

	dprintf("HostGfx_CreateClipper: CreateClipper.\n");
	hr=IDirectDraw_CreateClipper(state->ddraw,0,&state->primary_clipper,0);
	if(FAILED(hr)) {
		return 0;
	}

	dprintf("HostGfx_CreateClipper: Set clipper window.\n");
	hr=IDirectDrawClipper_SetHWnd(state->primary_clipper,0,state->target);
	if(FAILED(hr)) {
		return 0;
	}

	dprintf("HostGfx_CreateClipper: Set clipper on primary surface.\n");
	hr=IDirectDrawSurface_SetClipper(state->primary,state->primary_clipper);
	if(FAILED(hr)) {
		return 0;
	}

	return 1;
}

static void HostGfx_RefreshConvertTableWindowsPalettized(HostGfxState *state) {
	(void)state;
}

static HRESULT HostGfx_StartWindowedInternal(HostGfxState *state,int w,int h) {
	DDSURFACEDESCN sd;
	HRESULT hr;
	unsigned i;

	dprintf("HostGfx_StartWindowedInternal: SetCooperativeLevel.\n");
	hr=IDirectDraw_SetCooperativeLevel(state->ddraw,state->owner,DDSCL_NORMAL);
	if(FAILED(hr)) {
		return hr;
	}

	//Primary surface and its description
	memset(&sd,0,sizeof sd);
	sd.dwSize=sizeof sd;
	sd.dwFlags=DDSD_CAPS;
	sd.ddsCaps.dwCaps=DDSCAPS_PRIMARYSURFACE;
	dprintf("HostGfx_StartInternal: Create primary surface.\n");
	state->primary=NewSurface(state,&sd);
	if(!state->primary) {
		return E_FAIL;
	}
	memset(&state->primary_desc,0,sizeof state->primary_desc);
	state->primary_desc.dwSize=sizeof state->primary_desc;
	DXCHK(IDirectDrawSurface_GetSurfaceDesc(state->primary,&state->primary_desc));

	//Back surfaces, if wanted.
	state->num_back_buffers=0;
	if(w<0||h<0) {
		dprintf("HostGfx_StartWindowedInternal: invalid dimensions, not creating back buffers.\n");
	} else {
		for(i=0;i<max_num_back_buffers;++i) {
			memset(&sd,0,sizeof sd);
			sd.dwSize=sizeof sd;
			sd.dwFlags=DDSD_CAPS|DDSD_WIDTH|DDSD_HEIGHT;
			sd.ddsCaps.dwCaps=DDSCAPS_OFFSCREENPLAIN;

			dprintf("HostGfx_StartWindowedInternal: DDCAPS_BLT=%s, DDCAPS_CANBLTSYSMEM=%s.\n",
				state->halcaps.dwCaps&DDCAPS_BLT?"yes":"no",state->halcaps.dwCaps&DDCAPS_CANBLTSYSMEM?"yes":"no");

			// blits from system memory seem to work poorly on my nvidia
            // gs7600 at least, even though the caps have
            // DDCAPS_CANBLTSYSMEM set. it's 4-6x faster to have the back
            // buffers in video memory.
			switch(state->halcaps.dwCaps&(DDCAPS_BLT|DDCAPS_CANBLTSYSMEM)) {
			case DDCAPS_CANBLTSYSMEM:
				// ????
			case 0:
				sd.ddsCaps.dwCaps|=DDSCAPS_SYSTEMMEMORY;
				dprintf("HostGfx_StartWindowedInternal: back buffers in system memory.\n");
				break;

			default:
			//case DDCAPS_BLT:
				sd.ddsCaps.dwCaps|=DDSCAPS_VIDEOMEMORY;
				dprintf("HostGfx_StartWindowedInternal: back buffers in video memory.\n");
				break;
			}

			sd.dwWidth=w;
			sd.dwHeight=h;//<<HostGfx_Shift(state);
			dprintf("HostGfx_StartWindowedInternal: Create %ux%u back buffer %u/%u.\n",
				sd.dwWidth,sd.dwHeight,1+i,max_num_back_buffers);
			state->back_buffers[i]=NewSurface(state,&sd);
			if(!state->back_buffers[i]) {
				dprintf("HostGfx_StartWindowedInternal:\tthis one failed\n");
				if(state->num_back_buffers==0) {
					dprintf("HostGfx_StartWindowedInternal:\tno back buffers created, failed overall.\n");
					hr=E_FAIL;
				}
				break;
			}
			++state->num_back_buffers;
		}
		dprintf("HostGfx_StartWindowedInternal: created %d back buffers\n",
			state->num_back_buffers);
	}

	if(!HostGfx_CreateClipper(state)) {
		goto stop_error;
	}

	state->convert_fn=0;
	state->refresh_convert_table_fn=0;

	HostGfxBufferFormat_SetPixelElement(&state->best_fmt,0,1);
	HostGfxBufferFormat_SetPixelElement(&state->best_fmt,1,2);
	HostGfxBufferFormat_SetPixelElement(&state->best_fmt,2,4);
	state->best_fmt.fixed_bits=0;
	state->best_fmt.bypp=1;

	switch(state->primary_desc.ddpfPixelFormat.dwRGBBitCount) {
	case 8:
		state->convert_fn=&HostGfx_Copy8Windowed;
		//no convert table
		for(i=0;i<3;++i) {
			HostGfxBufferFormat_SetPixelElement(&state->best_fmt,i,state->best_fmt.elems[i].mask);
		}
		break;

	case 16:
		state->convert_fn=&HostGfx_ConvertTo16;
		state->refresh_convert_table_fn=&HostGfx_RefreshConvertTableTo16;
		break;

	case 24:
		state->convert_fn=&HostGfx_ConvertTo24;
		state->refresh_convert_table_fn=&HostGfx_RefreshConvertTableTo24;
		break;

	case 32:
#ifdef ASM_OPT
#ifdef MMX_OPT
		state->convert_fn=&HostGfx_ConvertTo32MMX;
		state->refresh_convert_table_fn=&HostGfx_RefreshConvertTableTo32MMX;
#else//MMX_OPT
		state->convert_fn=&HostGfx_ConvertTo32Asm;
		state->refresh_convert_table_fn=&HostGfx_RefreshConvertTableTo32Asm;
#endif//MMX_OPT
#else//ASM_OPT
		state->convert_fn=&HostGfx_ConvertTo32;
		state->refresh_convert_table_fn=&HostGfx_RefreshConvertTableTo32;
#endif//ASM_OPT
		break;
	}

	dprintf("HostGfx_StartWindowedInternal: ok.\n");

	return S_OK;

stop_error:
	dprintf("HostGfx_StartWindowedInternal: failed: %s.\n",DXGetErrorString(hr));
	SAFE_RELEASE(state->primary_clipper);
	for(i=0;i<max_num_back_buffers;++i) {
		SAFE_RELEASE(state->back_buffers[i]);
	}
	SAFE_RELEASE(state->primary);
	return E_FAIL;
}

static void HostGfx_RefreshConvertTableFullScreen(HostGfxState *state) {
	PALETTEENTRY rgb_palette[8];
	int i;

	for(i=0;i<8;++i) {
		rgb_palette[i].peRed=(byte)(state->palette[i][0]*255.f);
		rgb_palette[i].peGreen=(byte)(state->palette[i][1]*255.f);
		rgb_palette[i].peBlue=(byte)(state->palette[i][2]*255.f);
		rgb_palette[i].peFlags=PC_NOCOLLAPSE;

		dprintf("Palette entry %d: (%d,%d,%d)\n",i,rgb_palette[i].peRed,rgb_palette[i].peGreen,rgb_palette[i].peBlue);
	}

	if(state->primary_palette) {
		HRESULT hr;
		
		hr=IDirectDrawPalette_SetEntries(state->primary_palette,0,HostGfx_FULL_SCREEN_PALETTE_BASE,8,rgb_palette);
		dprintf("%s: IDirectDrawPalette_SetEntries result: %s\n",__FUNCTION__,DXGetErrorString(hr));
	}
}

static HRESULT HostGfx_StartFullScreenInternal(HostGfxState *state,int w,int h) {
	DDSURFACEDESCN sd;
	HRESULT hr;
	PALETTEENTRY palette[256];
	//	unsigned i;
	//DDSCAPS dscaps;
	DDBLTFX bltfx;
	HDC screen_dc;
	DWORD coop;

	(void)w,(void)h;

	//Set video mode
	dprintf("HostGfx_StartFullScreenInternal: SetCooperativeLevel.\n");
	coop=DDSCL_EXCLUSIVE|DDSCL_FULLSCREEN|DDSCL_ALLOWREBOOT;
	hr=IDirectDraw_SetCooperativeLevel(state->ddraw,state->owner,coop);
	if(FAILED(hr)) {
		goto stop_error;
	}
	dprintf("HostGfx_StartFullScreenInternal: SetDisplayMode, %dHz.\n",
		state->full_screen_refresh_rate);
	hr=IDirectDraw2_SetDisplayMode(state->ddraw,FULL_SCREEN_WIDTH,FULL_SCREEN_HEIGHT,8,
		state->full_screen_refresh_rate,0);
	if(FAILED(hr)) {
		goto stop_error;
	}

	//Primary surface and its description.
	//I want to avoid catering for flipping etc., so I don't bother
	memset(&sd,0,sizeof sd);
	sd.dwSize=sizeof sd;
	sd.dwFlags=DDSD_CAPS|DDSD_BACKBUFFERCOUNT;
	sd.dwBackBufferCount=1;
	sd.ddsCaps.dwCaps=DDSCAPS_PRIMARYSURFACE|DDSCAPS_COMPLEX|DDSCAPS_FLIP;
	dprintf("HostGfx_StartFullScreenInternal: Create primary surface.\n");
	state->primary=NewSurface(state,&sd);
	if(!state->primary) {
		hr=E_FAIL;
		goto stop_error;
	}

	//Set that as the front buffer.
	state->back_buffers[0]=state->primary;
	IDirectDrawSurface_AddRef(state->back_buffers[0]);

	//Clear the buffer that's going to be used.
	memset(&bltfx,0,sizeof bltfx);
	bltfx.dwSize=sizeof bltfx;
	bltfx.dwFillColor=0;
	DXCHK(IDirectDrawSurface_Blt(state->back_buffers[0],0,0,0,
		DDBLT_COLORFILL|DDBLT_WAIT,&bltfx));

	//Here the palette is retrieved, and 8 entries reset for use by the emulator,
	//then set as the current palette. The hope is that the GDI can draw onto the
	//primary surface, allowing use of normal Windows menu bar and status bar if
	//the user wants this.

	//This seems to be the best way to get the system palette.
	//http://groups.google.com/groups?q=default+palette+directdraw&hl=en&lr=&ie=UTF-8&oe=UTF-8&selm=38F675F48B36D4118E4D00508B5EBA37BE6BBB%40cpmsftmsgv21.microsoft.com&rnum=10
	//(was the first useful one from google)
	dprintf("HostGfx_StartFullScreenInternal: determining default palette\n");
	dprintf("HostGfx_StartFullScreenInternal:     IDirectDrawSurface_GetDC...\n");
	hr=IDirectDrawSurface_GetDC(state->primary,&screen_dc);
	if(FAILED(hr)) {
		goto stop_error;
	}
	dprintf("HostGfx_StartFullScreenInternal:     GetSystemPaletteEntries...\n");
	if(GetSystemPaletteEntries(screen_dc,0,256,palette)!=256) {
		hr=E_FAIL;
		//but the DC should still be released.
	}
	IDirectDrawSurface_ReleaseDC(state->primary,screen_dc);
	//ShowPalette(stdout,"system palette",palette);
	if(FAILED(hr)) {
		goto stop_error;
	}

	hr=IDirectDraw_CreatePalette(state->ddraw,DDPCAPS_8BIT|DDPCAPS_ALLOW256,palette,
		&state->primary_palette,0);
	if(FAILED(hr)) {
		goto stop_error;
	}

	//It's not clear what to do if this should fail. So hope it doesn't.
	IDirectDrawSurface_SetPalette(state->primary,state->primary_palette);

	if(!HostGfx_CreateClipper(state)) {
		goto stop_error;
	}

	//set up pixel format.
	HostGfxBufferFormat_SetPixelElement(&state->best_fmt,0,1);
	HostGfxBufferFormat_SetPixelElement(&state->best_fmt,1,2);
	HostGfxBufferFormat_SetPixelElement(&state->best_fmt,2,4);
	state->best_fmt.fixed_bits=(unsigned)HostGfx_FULL_SCREEN_PALETTE_BASE|
		((unsigned)HostGfx_FULL_SCREEN_PALETTE_BASE<<8)|((unsigned)HostGfx_FULL_SCREEN_PALETTE_BASE<<16)|
		((unsigned)HostGfx_FULL_SCREEN_PALETTE_BASE<<24);
	state->best_fmt.bypp=1;

	state->convert_fn=&HostGfx_Copy8;
	state->refresh_convert_table_fn=&HostGfx_RefreshConvertTableFullScreen;

	//HostGfx_RefreshPalette(state);

	dprintf("HostGfx_StartFullScreenInternal: ok.\n");
	return S_OK;

stop_error:
	dprintf("HostGfx_StartFullScreenInternal: fail: %s\n",DXGetErrorString(hr));
	IDirectDraw_RestoreDisplayMode(state->ddraw);
	IDirectDraw_SetCooperativeLevel(state->ddraw,state->owner,DDSCL_NORMAL);
	SAFE_RELEASE(state->primary_palette);
	SAFE_RELEASE(state->back_buffers[0]);
	SAFE_RELEASE(state->primary);
	return hr;
}

//static HRESULT WINAPI StoreRefreshRatesEnumModesCallback(DDSURFACEDESCN *lpDDSurfaceDesc,LPVOID lpContext) {
//	HostGfxState *state=lpContext;
//	DWORD mask=DDSD_WIDTH|DDSD_HEIGHT|DDSD_REFRESHRATE;
//
//	if((lpDDSurfaceDesc->dwFlags&mask)==mask) {
//		if(lpDDSurfaceDesc->dwWidth==FULL_SCREEN_WIDTH&&lpDDSurfaceDesc->dwHeight==FULL_SCREEN_HEIGHT) {
//			int rate=lpDDSurfaceDesc->dwRefreshRate;
//			if(rate!=0) {
//				int num=0;
//				int add=1;
//
//				if(state->refresh_rates) {
//					for(num=0;state->refresh_rates[num]!=0;++num) {
//						if(state->refresh_rates[num]==rate) {
//							add=0;
//							break;
//						}
//					}
//				}
//
//				if(add) {
//					int *p=realloc(state->refresh_rates,(num+2)*sizeof *state->refresh_rates);
//					if(p) {
//						state->refresh_rates=p;
//						state->refresh_rates[num]=rate;
//						state->refresh_rates[num+1]=0;
//					}
//				}
//			}
//		}
//	}
//
//	return DDENUMRET_OK;
//}

static HRESULT HostGfx_StartInternal2(HostGfxState *state,int w,int h) {
	HRESULT hr=S_OK;

	if(state->full_screen) {
		hr=HostGfx_StartFullScreenInternal(state,w,h);
		if(FAILED(hr)) {
			//HostGfx_StopInternal(state);
			state->full_screen=0;
		}
	}

	if(!state->full_screen) {
		hr=HostGfx_StartWindowedInternal(state,w,h);
	}
	if(FAILED(hr)) {
		return hr;
	}

	if(state->back_buffers[0]) {
		memset(&state->back_buffer_desc,0,sizeof state->back_buffer_desc);
		state->back_buffer_desc.dwSize=sizeof state->back_buffer_desc;
		IDirectDrawSurface_GetSurfaceDesc(state->back_buffers[0],
			&state->back_buffer_desc);
		state->buffers_ok=1;
		state->cpu_buffer=0;
		state->gpu_buffer=state->num_back_buffers-1;//num_back_buffers-1;
	}

	return S_OK;
}

static int HostGfx_StartInternal(HostGfxState *state,int w,int h) {
	HRESULT hr=S_OK;

	state->present_count=0;
#ifdef HOST_DEBUG
	state->present_ticks_total=0;
#endif

	dprintf("HostGfx_StartInternal: w=%d h=%d\n",w,h);

	dprintf("HostGfx_StartInternal: loading ddraw.dll.\n");
	state->ddraw_dll_h=LoadLibraryA("ddraw.dll");
	if(!state->ddraw_dll_h) {
		goto stop_error;
	}

	state->dd_create_fn=(LPDIRECTDRAWCREATE)GetProcAddress(state->ddraw_dll_h,"DirectDrawCreate");
	state->dd_createex_fn=(LPDIRECTDRAWCREATEEX)GetProcAddress(state->ddraw_dll_h,"DirectDrawCreateEx");
	state->dd_createclipper_fn=(LPDIRECTDRAWCREATECLIPPER)GetProcAddress(state->ddraw_dll_h,"DirectDrawCreateClipper");
	state->dd_enumeratea_fn=(LPDIRECTDRAWENUMERATEW)GetProcAddress(state->ddraw_dll_h,"DirectDrawEnumerateW");
	state->dd_enumeratew_fn=(LPDIRECTDRAWENUMERATEA)GetProcAddress(state->ddraw_dll_h,"DirectDrawEnumerateA");
	state->dd_enumaretexw_fn=(LPDIRECTDRAWENUMERATEEXW)GetProcAddress(state->ddraw_dll_h,"DirectDrawEnumerateExW");
	state->dd_enumaretexa_fn=(LPDIRECTDRAWENUMERATEEXA)GetProcAddress(state->ddraw_dll_h,"DirectDrawEnumerateExA");

	if(!state->dd_create_fn||!state->dd_createex_fn||!state->dd_createclipper_fn||!state->dd_enumeratea_fn||!state->dd_enumeratew_fn||!state->dd_enumaretexw_fn||!state->dd_enumaretexa_fn) {
		dprintf("HostGfx_StartInternal: ddraw.dll is missing a function.\n");
		goto stop_error;
	}

#ifdef DX3_ONLY
	{
		//DirectDraw object
		IDirectDraw *ddraw;

		dprintf("HostGfx_StartInternal: starting DirectDraw.\n");
		hr=(*state->dd_create_fn)(0,&ddraw,0);
		if(FAILED(hr)) {
			IDirectDraw_Release(ddraw);
			goto stop_error;
		}
		dprintf("HostGfx_StartInternal: QueryInterface for IDirectDraw2.\n");
		hr=IDirectDraw_QueryInterface(ddraw,&IID_IDirectDraw2,&state->ddraw);
		IDirectDraw_Release(ddraw);
		if(FAILED(hr)) {
			goto stop_error;
		}
	}
#else
	dprintf("HostGfx_StartInternal: starting DirectDraw.\n");
	// TODO hack -- should be part of start full screen/start windowed difference?
	{
		GUID *adapter_guid;

		if(!state->full_screen) {
			adapter_guid=0;
			
			dprintf("HostGfx_StartInternal: windowed, not specifying any adapter.\n");
		} else {
			adapter_guid=&state->adapters_private[state->full_screen_adapter_index].guid_data;

			dprintf("HostGfx_StartInternal: adapter is %s ",
				state->adapters[state->full_screen_adapter_index]->device_name);
		}

		if(!adapter_guid) {
			dprintf("(no GUID)");
		} else {
			dprintf("{%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02}",
				adapter_guid->Data1,
				adapter_guid->Data2,
				adapter_guid->Data3,
				adapter_guid->Data4[0],adapter_guid->Data4[1],
				adapter_guid->Data4[2],adapter_guid->Data4[3],adapter_guid->Data4[4],adapter_guid->Data4[5],
				adapter_guid->Data4[6],adapter_guid->Data4[7]);
		}
		dprintf("\n");

		hr=(*state->dd_createex_fn)(adapter_guid,&state->ddraw,&IID_IDirectDraw7,0);
		if(FAILED(hr)) {
			goto stop_error;
		}
	}
#endif

	dprintf("HostGfx_StartInternal: GetCaps.\n");
	memset(&state->halcaps,0,sizeof state->halcaps);
	state->halcaps.dwSize=sizeof state->halcaps;
	hr=IDirectDraw_GetCaps(state->ddraw,&state->halcaps,0);
	if(FAILED(hr)) {
		goto stop_error;
	}

	hr=HostGfx_StartInternal2(state,w,h);
	if(FAILED(hr)) {
		goto stop_error;
	}

//	HostGfx_RefreshPalette(state);

//	hr=IDirectDraw_EnumDisplayModes(state->ddraw,DDEDM_REFRESHRATES,0,state,
//		&StoreRefreshRatesEnumModesCallback);
//	if(FAILED(hr)) {
//		free(state->refresh_rates);
//		state->refresh_rates=0;
//	}

#ifdef DX3_ONLY
	// TODO set up refresh rates for primary device.
#else
	// TODO
#endif

	if(state->refresh_convert_table_fn) {
		(*state->refresh_convert_table_fn)(state);
	}

	//	HostGfx_SetScreenType(state,HostGfx_COLOUR);
	//	HostGfx_SetScanlinesMode(state,HostGfx_SINGLE);

	return 1;

stop_error:
	dprintf("HostGfx_StartInternal: fail: %s\n",DXGetErrorString(hr));
	HostGfx_StopInternal(state);
	return 0;
}

#ifndef DX3_ONLY
static HRESULT WINAPI StoreAdapterDisplayModesEnumCallback(DDSURFACEDESC2 *sd,void *adapter_ptr) {
	static const DWORD mask=DDSD_WIDTH|DDSD_HEIGHT|DDSD_REFRESHRATE;
	HostGfx_Adapter *adapter;

	adapter=adapter_ptr;
	if((sd->dwFlags&mask)==mask) {
		if(sd->dwWidth==FULL_SCREEN_WIDTH&&sd->dwHeight==FULL_SCREEN_HEIGHT) {
			if(sd->dwRefreshRate!=0) {
				int i;

				// New?
				for(i=0;i<adapter->num_refresh_rates;++i) {
					if(adapter->refresh_rates[i]==(int)sd->dwRefreshRate) {
						break;
					}
				}

				if(i>=adapter->num_refresh_rates) {
					//Add it in.
					adapter->refresh_rates=realloc(adapter->refresh_rates,
						(adapter->num_refresh_rates+1)*sizeof *adapter->refresh_rates);
					adapter->refresh_rates[adapter->num_refresh_rates]=sd->dwRefreshRate;
					++adapter->num_refresh_rates;
				}
			}
		}
	}

	return DDENUMRET_OK;
}

static BOOL WINAPI StoreAdaptersEnumCallback(GUID *guid,LPSTR driver_description,LPSTR driver_name,
	void *state_ptr,HMONITOR hm)
{
	HostGfxState *state;
	HostGfx_Adapter *adapter;
	AdapterPrivate *adapter_private;
	IDirectDraw7 *ddraw;
	HRESULT hr;

	state=state_ptr;

	dprintf("%s: found adapter %d:\n",__FUNCTION__,state->num_adapters);
	dprintf("    description: %s\n",driver_description);
	dprintf("    name: %s\n",driver_name);

	// Try to get refresh rates list.
	hr=(*state->dd_createex_fn)(guid,&ddraw,&IID_IDirectDraw7,0);
	if(FAILED(hr)) {
		dprintf("    Failed to create its IDirectDraw7: %s\n",DXGetErrorString(hr));
		return 1;
	} else {
		HostGfx_Adapter adapter_tmp;
		// Looks good so far.

		// try to get display modes
		adapter_tmp.num_refresh_rates=0;
		adapter_tmp.refresh_rates=0;
		hr=IDirectDraw7_EnumDisplayModes(ddraw,DDEDM_REFRESHRATES,0,&adapter_tmp,&StoreAdapterDisplayModesEnumCallback);
		if(FAILED(hr)) {
			dprintf("    Failed to enumerate display modes: %s\n",DXGetErrorString(hr));
		} else {
			//success
			int i;

			//new objects
			state->adapters=realloc(state->adapters,(state->num_adapters+1)*sizeof *state->adapters);
			state->adapters[state->num_adapters]=malloc(sizeof(HostGfx_Adapter));
			state->adapters_private=realloc(state->adapters_private,
				(state->num_adapters+1)*sizeof *state->adapters_private);

			adapter=state->adapters[state->num_adapters];
			adapter_private=&state->adapters_private[state->num_adapters];
			++state->num_adapters;

			//finish off adapter object
			*adapter=adapter_tmp;
			memset(adapter->device_name,0,sizeof adapter->device_name);
			strncpy(adapter->device_name,driver_description,sizeof adapter->device_name-1);

			//adapter_private
			adapter_private->hm=hm;
			if(!guid) {
				adapter_private->has_guid=0;
				
				adapter->default_device=1;
			} else {
				adapter_private->guid_data=*guid;
				adapter_private->has_guid=1;

				adapter->default_device=0;
			}

			dprintf("    GUID: ");
			if(!adapter_private->has_guid) {
				dprintf("null\n");
			} else {
				dprintf("{%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X}\n",adapter_private->guid_data.Data1,
					adapter_private->guid_data.Data2,adapter_private->guid_data.Data3,adapter_private->guid_data.Data4[0],
					adapter_private->guid_data.Data4[1],adapter_private->guid_data.Data4[2],adapter_private->guid_data.Data4[3],
					adapter_private->guid_data.Data4[4],adapter_private->guid_data.Data4[5],adapter_private->guid_data.Data4[6],
					adapter_private->guid_data.Data4[7]);
			}
			dprintf("    Default device: %s\n",adapter->default_device?"yes":"no");
			dprintf("    Available refresh rates:");
			for(i=0;i<adapter->num_refresh_rates;++i) {
				dprintf(" %dHz",adapter->refresh_rates[i]);
			}
			dprintf("\n");
		}

		IDirectDraw7_Release(ddraw);
	}

	return 1;//continue enumeration
}
#endif

HostGfxState *HostGfx_Start(HWND owner,HWND target) {
	HostGfxState *new_state;
	unsigned i;

#ifdef HOSTGFX_STATE_ALIGNMENT_BITS
	{
		unsigned aligned;
		void *real_address;

		real_address=malloc(sizeof *new_state+HOSTGFX_STATE_ALIGNMENT_BYTES-1);
		aligned=(unsigned)real_address;
		aligned+=HOSTGFX_STATE_ALIGNMENT_BYTES-1;
		aligned&=~(HOSTGFX_STATE_ALIGNMENT_BYTES-1);
		new_state=(HostGfxState *)aligned;
		new_state->real_address=real_address;
	}
#else
	new_state=malloc(sizeof *new_state);//HostGfxState;
	new_state->real_address=new_state;
#endif

	//	dprintf("HostGfx_Start: owner window \"%s\"",owner->GetTitle().c_str());
	new_state->owner=owner;
	new_state->target=target;
	new_state->ddraw=0;
	new_state->primary=0;
	for(i=0;i<max_num_back_buffers;++i) {
		new_state->back_buffers[i]=0;
	}
	new_state->num_back_buffers=0;
	new_state->primary_clipper=0;
	new_state->primary_palette=0;
	new_state->buffers_ok=0;
	new_state->full_screen=0;
	new_state->full_screen_refresh_rate=0;
	new_state->full_screen_adapter_index=0;

	for(i=0;i<8;++i) {
		new_state->palette[i][0]=(float)(i&1);
		new_state->palette[i][1]=(float)((i>>1)&1);
		new_state->palette[i][2]=(float)((i>>2)&1);
	}

//	new_state->refresh_rates=0;

	new_state->num_failed_starts=0;

	new_state->clip_list=0;
	new_state->clip_list_size=0;

#ifdef CHECK_TIMINGS
	TimingData_Reset(&new_state->blit_td);
	TimingData_Reset(&new_state->wait_vb_td);
	TimingData_Reset(&new_state->convert_td);
	TimingData_Reset(&new_state->lock_td);
#endif

	if(!HostGfx_StartInternal(new_state,-1,-1)) {
		dprintf("%s: HostGfx_Stop due to failed HostGfx_StartInternal.\n",__FUNCTION__);
		HostGfx_Stop(new_state);
		return 0;
	}

	new_state->has_sse=HasSSE();

#ifdef DX3_ONLY
	strcpy(new_state->primary_adapter.device_name,"Primary adapter");
	new_state->primary_adapter.num_refresh_rates=0;
	new_state->primary_adapter.refresh_rates=0;
#else
	new_state->adapters=0;
	new_state->adapters_private=0;
	new_state->num_adapters=0;

	{
		HRESULT hr;

		hr=(*new_state->dd_enumaretexa_fn)(&StoreAdaptersEnumCallback,new_state,DDENUM_ATTACHEDSECONDARYDEVICES);
		if(FAILED(hr)) {
			dprintf("%s: DirectDrawEnumerateEx fail: %s\n",DXGetErrorString(hr));
			HostGfx_Stop(new_state);
			return 0;
		}
	}
#endif

	dprintf("HostGfx_Start: ok, has SSE=%d.\n",new_state->has_sse);
	return new_state;
}

void HostGfx_Stop(HostGfxState *state) {
	if(state) {
		dprintf("%s: HostGfx_StopInternal due to stop.\n",__FUNCTION__);
		HostGfx_StopInternal(state);

#ifndef DX3_ONLY
		{
			int i;

			for(i=0;i<state->num_adapters;++i) {
				free(state->adapters[i]->refresh_rates);
				free(state->adapters[i]);
			}
			free(state->adapters);
			free(state->adapters_private);
		}
#endif

		free(state->clip_list);
//		free(state->refresh_rates);

#ifdef PALETTE_ALIGNMENT_BITS
		free(state->palette_data);
#endif

		//must be last.
		free(state->real_address);
	}
}

static void HostGfx_DrawStartFailedMsg(HostGfxState *state) {
	static const char *msg="Unable to start graphics.";
	HDC dc;

	dc=GetDC(state->target);
	if(dc) {
		int index;
		RECT client;

		index=SaveDC(dc);

		GetClientRect(state->target,&client);

		FillRect(dc,&client,GetStockBrush(BLACK_BRUSH));

		SetBkMode(dc,TRANSPARENT);
		SetTextColor(dc,RGB(255,255,255));
		SetTextAlign(dc,TA_CENTER|TA_BASELINE);
		TextOut(dc,client.right/2,client.bottom/2,msg,strlen(msg));

		RestoreDC(dc,index);
		ReleaseDC(state->target,dc);
	}
}

//TODO this logic is becoming twisted, all this asynchronous setting up business is simply
//far too much!
void HostGfx_Present(HostGfxState *state,const HostGfxBuffer *buffer,int y_first,int line_height,
	int gap_height,unsigned present_hints,uint64_t *convert_time,uint64_t *present_time)
{
	DDSURFACEDESCN sd;
	HRESULT hr;
	int right_width,right_height;
	int h,w;
	DWORD lock_flags;
	TD(qword start_tsc);

	lock_flags=DDLOCK_WRITEONLY|DDLOCK_WAIT|DDLOCK_SURFACEMEMORYPTR;
#if (defined HOST_DEBUG)&&!(defined DX3_ONLY)
	lock_flags|=DDLOCK_NOSYSLOCK;
#endif

	if(convert_time)
		*convert_time=0;

	if(present_time)
		*present_time=0;

	HOST_ASSERT(y_first<=gap_height);
	w=buffer->w;
	h=buffer->h*(line_height+gap_height);
	//if we're confronted with something different from last time, that resets
	//our failed start count.
	if(w!=state->last_present_w||h!=state->last_present_h) {
		state->last_present_w=w;
		state->last_present_h=h;
		state->num_failed_starts=0;
	}

	//if we failed to start too many times, draw a message, and don't
	//bother trying again, yet.
	if(!state->buffers_ok&&state->num_failed_starts>=MAX_FAILED_STARTS) {
		HostGfx_DrawStartFailedMsg(state);
		return;
	}

	//we may need to recreate the buffers because we're being asked to display
	//a differently-sized buffer in windowed mode.
	right_width=1;
	right_height=1;
	if(state->buffers_ok) {
		if(!state->full_screen) {
			right_width=w==(int)state->back_buffer_desc.dwWidth;
			right_height=h==(int)state->back_buffer_desc.dwHeight;
		}
	}

	if(!state->ddraw) {
		dprintf("%s: HostGfx_Stop (and restart) due to settings change. (buffers_ok=%d request: w=%d h=%d buffer: w=%d lasth=%d)\n",
			__FUNCTION__,state->buffers_ok,w,h,state->back_buffer_desc.dwWidth,state->back_buffer_desc.dwHeight);
		HostGfx_StopInternal(state);
		HostGfx_StartInternal(state,w,h);

		if(!state->ddraw) {
			dprintf("%s: !state->ddraw; restart must have failed.\n",__FUNCTION__);
			return;
		}
	}

	if(!state->buffers_ok||!right_width||!right_height) {
		dprintf("%s: DeleteBuffers and StartInternal2 due to settings change. (buffers_ok=%d request: w=%d h=%d buffer: w=%d lasth=%d)\n",
			__FUNCTION__,state->buffers_ok,w,h,state->back_buffer_desc.dwWidth,state->back_buffer_desc.dwHeight);
		//HostGfx_StopInternal(state);
		//HostGfx_StartInternal(state,w,h);

		HostGfx_DeleteBuffers(state);
//		if(state->full_screen) {
//			HostGfx_StartFullScreenInternal(state,w,h);
//		} else {
//			HostGfx_StartWindowedInternal(state,w,h);
//		}
		HostGfx_StartInternal2(state,w,h);

		//Anything might have happened, so drop out without doing anything so
		//that next time Present is called the emulator can check if full screen
		//succeeded. Also bump the number of failed starts if that didn't work.
		if(!state->buffers_ok) {
			++state->num_failed_starts;
		}
		return;
	}

	if(present_hints&HostGfx_PRESENT_HINT_SYNC) {
		TD(start_tsc=ReadTsc());
		IDirectDraw_WaitForVerticalBlank(state->ddraw,DDWAITVB_BLOCKBEGIN,0);
		TD(TimingData_Update(&state->wait_vb_td,ReadTsc()-start_tsc));
	}

	state->num_failed_starts=0;

	memset(&sd,0,sizeof sd);
	sd.dwSize=sizeof sd;
	TD(start_tsc=ReadTsc());
	hr=IDirectDrawSurface_Lock(state->back_buffers[state->cpu_buffer],0,&sd,lock_flags,0);
	TD(TimingData_Update(&state->lock_td,ReadTsc()-start_tsc));
	if(FAILED(hr)) {
		state->buffers_ok=0;
		return;
	}

	if(state->convert_fn) {
		TD(start_tsc=ReadTsc());
		(*state->convert_fn)(state,sd.lpSurface,sd.lPitch,buffer->bitmap,buffer->w,buffer->pitch,buffer->h,y_first,line_height,
			gap_height,present_hints);
		TD(TimingData_Update(&state->convert_td,ReadTsc()-start_tsc));
	}

	IDirectDrawSurface_Unlock(state->back_buffers[state->cpu_buffer],0);

	++state->present_count;

	if(state->full_screen) {
		//IDirectDrawSurface_Flip(state->primary,0,DDFLIP_WAIT);
		//		ValidateRect(state->owner,0);
	} else {
		POINT top_left={0,0};
		RECT dest;

		state->gpu_buffer=(state->gpu_buffer+1)%state->num_back_buffers;
		state->cpu_buffer=(state->cpu_buffer+1)%state->num_back_buffers;
		GetClientRect(state->target,&dest);
		if(dest.right>0&&dest.bottom>0) {
			ClientToScreen(state->target,&top_left);
			OffsetRect(&dest,top_left.x,top_left.y);

			TD(start_tsc=ReadTsc());
			IDirectDrawSurface_Blt(state->primary,&dest,state->back_buffers[state->gpu_buffer],0,DDBLT_WAIT,0);
			TD(TimingData_Update(&state->blit_td,ReadTsc()-start_tsc));
		}
	}
	ValidateRect(state->target,0);

	TD(TimingData_Print(&state->blit_td,"blt");)
	TD(TimingData_Print(&state->lock_td,"lock");)
	TD(TimingData_Print(&state->wait_vb_td,"wait_vb");)
	TD(TimingData_Print(&state->convert_td,"convert");)
}

void HostGfx_SetPalette(HostGfxState *state,const float new_palette[8][3]) {
	memmove(&state->palette,new_palette,sizeof state->palette);
	if(state->refresh_convert_table_fn) {
		(*state->refresh_convert_table_fn)(state);
	}
}

void HostGfx_SetFullScreen(HostGfxState *state,int full_screen,int full_screen_refresh_rate) {
#ifdef mbENABLE_FULL_SCREEN
	int restart=0;
	int full_screen_adapter_index=0;//TODO

	if(full_screen!=state->full_screen||
		(state->full_screen&&
		full_screen_refresh_rate!=state->full_screen_refresh_rate))
	{
		dprintf("%s: HostGfx_Stop due to changing full screen state. (new: fs=%d hz=%d old: fs=%d hz=%d)\n",
			__FUNCTION__,full_screen,full_screen_refresh_rate,state->full_screen,state->full_screen_refresh_rate);
		HostGfx_StopInternal(state);
		restart=1;
	}
	state->full_screen=full_screen;
	if(state->full_screen) {
		state->full_screen_refresh_rate=full_screen_refresh_rate;
		state->full_screen_adapter_index=full_screen_adapter_index;
	}
	if(restart) {
		HostGfx_StartInternal(state,-1,-1);
	}
#else
	state->full_screen=0;
#endif
}

int HostGfx_IsFullScreen(HostGfxState *state) {
	return /*state->buffers_ok&&*/state->full_screen;
}

void HostGfx_Cls(HostGfxState *state) {
	if(state->full_screen) {
		DDBLTFX fx;

		memset(&fx,0,sizeof fx);
		fx.dwSize=sizeof fx;
		fx.dwFillColor=0;
		IDirectDrawSurface_Blt(state->primary,0,0,0,DDBLT_COLORFILL|DDBLT_WAIT,&fx);
	}
}

//const int *HostGfx_FullScreenRefreshRates(HostGfxState *state) {
//	if(state->refresh_rates) {
//		return state->refresh_rates;
//	} else {
//		return &empty_refresh_rate_list;
//	}
//}

const HostGfxBufferFormat *HostGfx_BestBufferFormat(HostGfxState *state) {
	return &state->best_fmt;
}

#ifdef DX3_ONLY

const HostGfx_Adapter *HostGfx_GetAdapters(HostGfxState *state) {
	return &state->primary_adapter;//TODO
}

int HostGfx_NumAdapters(HostGfxState *state) {
	return 1;
}

#else

const HostGfx_Adapter *const *HostGfx_GetAdapters(HostGfxState *state) {
	return state->adapters;
}

int HostGfx_NumAdapters(HostGfxState *state) {
	return state->num_adapters;
}

#endif

void HostGfx_SetAdapter(HostGfxState *state,int adapter_index)
{
	// TODO Doesn't do anything yet.
	(void)state,(void)adapter_index;
}

#include "HostGfxConvert.inl"
