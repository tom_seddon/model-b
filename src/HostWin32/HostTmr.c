#include <windows.h>
#include "HostTmr.h"

void HostTmr_Start() {
}

void HostTmr_Stop() {
}

void HostTmr_ReadMilliseconds(int *value) {
	*value=GetTickCount();
}

void HostTmr_ReadHf(HostTmr_HfValue *value) {
	QueryPerformanceCounter((LARGE_INTEGER *)value);
}

int HostTmr_GetHfFrequency(HostTmr_HfValue *frequency) {
	LARGE_INTEGER qpf;
	if(!QueryPerformanceFrequency(&qpf)) {
		return 0;
	}
	*frequency=qpf.QuadPart;
	return 1;
}

#ifdef _MSC_VER

void HostTmr_ReadUhf(HostTmr_UhfValue *value) {
	__asm cpuid
	__asm mov edi,value
	__asm mov [edi+0],eax
	__asm mov [edi+4],edx
}

int HostTmr_GetUhfFrequency(HostTmr_UhfValue *frequency) {
	(void)frequency;

	return 650*1000000;//this is correct for my PC :)
}

#endif
