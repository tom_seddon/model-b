#ifndef HOSTTMR_H_
#define HOSTTMR_H_

#include <HostTmrShared.h>

#ifdef __cplusplus
extern "C" {
#endif

//High frequency timer -- read current reading, or query its frequency.
//(This needs to be accurate to at least a millisecond or so, which the
//Windows millisecond timers aren't.)

typedef LONGLONG HostTmr_HfValue;

void HostTmr_ReadHf(HostTmr_HfValue *value);

int HostTmr_GetHfFrequency(HostTmr_HfValue *frequency);


//Ultra high frequency timer (RDTSC on PCs)
//Solely for debugging/profiling type use
#ifdef _MSC_VER

typedef ULONGLONG HostTmr_UhfValue;

void HostTmr_ReadUhf(HostTmr_UhfValue *value);
int HostTmr_GetUhfFrequency(HostTmr_UhfValue *frequency);

#else

// non-VC++ just uses HF timer.
typedef HostTmr_HfValue HostTmr_UhfValue;

#define HostTmr_ReadUhf(X) HostTmr_ReadHf(X)
#define HostTmr_GetUhfFrequency(X) HostTmr_GetHfFrequency(X)

#endif


#ifdef __cplusplus
}
#endif

#endif
