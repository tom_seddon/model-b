#include <port.h>
#include "host_dx_versions.h"
#define INITGUID
#include "host.h"
#include "host_internal.h"
#include <ddraw.h>
#include <dinput.h>
#include <dsound.h>

#ifdef _MSC_VER

int HasSSE() {
	unsigned features;

	if(host_global_flags&Host_DISABLE_SSE) {
		return 0;
	}

	__asm mov eax,1
	__asm cpuid
	__asm mov features,edx

	return !!(features&(1<<25));
}

#else

int HasSSE() {
	return 0;
}

#endif
