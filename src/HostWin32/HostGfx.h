#ifndef HOSTGFX_H_
#define HOSTGFX_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <HostGfxShared.h>

//////////////////////////////////////////////////////////////////////////
//functions

//Create a HostGfxState that can be used to draw into the client area of
//the specified window.
HostGfxState *HostGfx_Start(HWND owner,HWND target);

#ifdef __cplusplus
}
#endif

#endif
