#ifndef HOST_INTERNAL_H_
#define HOST_INTERNAL_H_

#ifdef __cplusplus
#error "c only"
#endif

#ifdef _MSC_VER
#pragma warning(disable:4201)//nonstandard extension used : nameless struct/union
#endif//_MSC_VER

#define CINTERFACE
#include <unknwn.h>

//Whether to use the dxerr lib to get text descriptions of errors.
//(lib only works with MSVC it seems)
#ifdef _MSC_VER
#define USE_DXERR
#endif

#ifdef USE_DXERR
# include <dxerr.h>
# ifdef _MSC_VER
#  pragma comment(lib,"dxerr.lib")
# endif
#endif

#ifdef HOST_DEBUG

# ifdef _MSC_VER

#  define HOST_ASSERT(X)\
	((X)?(void)0:__debugbreak())

# elif defined(__BORLANDC__)

#  define HOST_ASSERT(X)\
	do {\
		if(!(X)) {\
			abort();\
		}\
	} while(0)

# elif defined(__GNUC__)

#  define HOST_ASSERT(X)\
	do {\
		if(!(X)) {\
			asm("int $3");\
		}\
	} while(0)

# endif

#else

#define HOST_ASSERT(X)

#endif

#define SAFE_RELEASE(V)\
	((V)?(V)->lpVtbl->Release(V),(V)=0,(void)0:(void)0)

#define DXCHK(X) RealDXCHK(X,#X,__FILE__,__LINE__,__FUNCTION__)

//static void *offsetwith(void *base,int offset) {
//	return (char *)base+offset;
//}
#define offsetwith(base,offset) ((void *)(((char *)(base))+(offset)))

typedef signed char int8;

// one of mingw's bloody headers has its own 'byte'.
#if !((defined __GNUC__)&&(defined __RPCNDR_H__))//yugh
typedef unsigned char byte;
#endif

typedef signed short int16;
typedef unsigned short word;

typedef signed long int32;
typedef unsigned long dword;

typedef unsigned __int64 qword;

HRESULT RealDXCHK(HRESULT hr,const char *what,const char *file,unsigned line,
	const char *function);

int dprintf(const char *fmt,...);

extern unsigned host_global_flags;

int HasSSE();

static __inline qword ReadTsc() {
#ifdef _MSC_VER
	qword val;

	__asm cpuid
	__asm rdtsc
	__asm mov dword ptr [val],eax
	__asm mov dword ptr [val+4],edx

	return val;
#else
	return 0;//err... todo.
#endif
}

#endif
