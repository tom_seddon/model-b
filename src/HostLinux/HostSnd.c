#include "HostSnd.h"
#include <malloc.h>

struct HostSndState
{
};

HostSndState *HostSnd_Start(void *window)
{
	HostSndState *state;

	state=malloc(sizeof *state);

	return state;
}


void HostSnd_Stop(HostSndState *state)
{
	free(state);
}

void HostSnd_SetSoundParameters(HostSndState *state,int hz,int bits,int channels,int len_samples)
{
	return;
}

void HostSnd_PlayStart(HostSndState *state)
{
	return;
}

void HostSnd_PlayStop(HostSndState *state)
{
	return;
}

void HostSnd_CommitWriteArea(HostSndState *state)
{
	return;
}

int HostSnd_GetWriteArea(HostSndState *state,void **p1,unsigned *len1,void **p2,unsigned *len2)
{
	return 0;
}

void HostSnd_SetVolume(HostSndState *state,int volume)
{
	return;
}

