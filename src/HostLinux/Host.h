#ifndef HOST_H_
#define HOST_H_

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////////////////////////////
// configurable logging

typedef void (*Host_LogOutputCallbackFn)(const char *str);

void Host_SetLogOutputCallback(Host_LogOutputCallbackFn callback);

#ifdef __cplusplus
}
#endif


#include "HostTmr.h"
#include "HostGfx.h"
#include "HostSnd.h"
#include "HostInp.h"

#endif

