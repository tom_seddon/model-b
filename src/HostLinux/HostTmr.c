#include "../port.h"
#include "HostTmr.h"
#include <time.h>
#include <stdio.h>

void HostTmr_Start()
{
	printf("%s: CLOCKS_PER_SEC=%ld\n",__FUNCTION__,CLOCKS_PER_SEC);
}

void HostTmr_Stop()
{
}

void HostTmr_ReadMilliseconds(int *value)
{
	clock_t c;

	c=clock();
	*value=(int)(c/(CLOCKS_PER_SEC/1000.));
}

void HostTmr_ReadHf(HostTmr_HfValue *value)
{
	HostTmr_ReadMilliseconds(value);
}

int HostTmr_GetHfFrequency(HostTmr_HfValue *frequency)
{
	*frequency=1000;
	return 1;
}

void HostTmr_GetUhfFrequency(HostTmr_UhfValue *freq)
{
	// correct for my PC (TODO not aware of good way of determining frequency)
	*freq=650000000ULL;
}

