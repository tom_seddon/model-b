#include "HostInp.h"
#include <malloc.h>

struct HostInpState
{
};

HostInpState *HostInp_Start(void *window)
{
	HostInpState *state;

	state=malloc(sizeof *state);

	return state;
}

void HostInp_Stop(HostInpState *state)
{
	free(state);
}

void HostInp_Poll(HostInpState *state)
{
	return;
}

void HostInp_GetKeyboardState(HostInpState *state,unsigned char *keyflags)
{
	return;
}

void HostInp_GetKeyboardDownFlags(HostInpState *state,unsigned char *downflags)
{
	return;
}

const char *HostInp_KeyNameFromCode(unsigned key_code)
{
	return "?";
}

int HostInp_CodeFromKeyName(const char *key_name)
{
	return 0;
}

int HostInp_SupportsJoysticks(HostInpState *state)
{
	return 0;
}

int HostInp_NumJoysticks(HostInpState *state)
{
	return 0;
}

int HostInp_GetJoystickState(HostInpState *state,int joy_index,HostInp_JoyState *joy_state)
{
	return 0;
}

const char *HostInp_JoystickName(HostInpState *state,int joy_index)
{
	return 0;
}

