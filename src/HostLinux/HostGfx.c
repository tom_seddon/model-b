// vim:foldmethod=marker:
// includes{{{
#include "../port.h"
#include "HostGfx.h"
#include "HostTmr.h"

#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/param.h>//only place I could find a min() macro!
#include <assert.h>
#include <ctype.h>

#include <sys/ipc.h>
#include <sys/shm.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/XShm.h>
#include <X11/extensions/Xv.h>
#include <X11/extensions/Xvlib.h>
//}}}

#include "../HostWin32/HostGfxConvert.h"//TODO

// Configuration defines {{{

// If defined, verbosity on stdout.
#define VERBOSE

// If defined, dump Xv image formats per port ID on stdout.
//#define XV_FORMATS_VERBOSITY

// If defined, progress on stdout.
#define SHOW_PROGRESS
//}}}

// Macros{{{

#ifdef VERBOSE

#define VERBOSITY(X)\
	do\
	{\
		printf X;\
	}\
	while(0)

#else//VERBOSE

#define VERBOSITY(...) ((void)0) 

#endif//VERBOSE

#ifdef SHOW_PROGRESS

#define PROGRESS(X)\
	do\
	{\
		printf X;\
	}\
	while(0)

#else//SHOW_PROGRESS

#define PROGRESS(...) ((void)0)

#endif//SHOW_PROGRESS
//}}}

struct Adapter
//{{{
{
	int (*start_fn)(HostGfxState *);
	int (*get_convert_buffer_fn)(HostGfxState *,int w,int h,void **buffer,unsigned *stride);
	void (*present_fn)(HostGfxState *);
	void (*stop_fn)(HostGfxState *);
	size_t data_size;
	void *static_data;
	HostGfx_Adapter hgfx_adapter;
};
typedef struct Adapter Adapter;
//}}}

struct HostGfxState
{//{{{
	unsigned char convert_table[256+7];
	float palette[8][3];

	Bool ok;//set if all OK for drawing

	// Managed by HostGfx_(Start|Stop)
	unsigned flags;
	Display *xdisplay;
	Window xwindow,xtarget;
	GC xgc;
	Visual *xvisual;

	//horrid hack!
	//TODO fix HostGfxConvertTemplate.inl...
	struct
	{
		struct
		{
			unsigned dwRBitMask,dwGBitMask,dwBBitMask;
		}
		ddpfPixelFormat;
	}
	primary_desc;

	HostGfxBufferFormat best_fmt;
	HostGfx_ConvertFn convert_fn;
	HostGfx_RefreshConvertTableFn refresh_convert_table_fn;

	const HostGfx_Adapter **hgfx_adapters;
	Adapter *adapters;
	int num_adapters;
	const Adapter *adapter;
	void *adapter_data;
};
//}}}

static int SetPixmapConvertFn(HostGfxState *state)
{{{
	int bpp;

	bpp=DefaultDepth(state->xdisplay,DefaultScreen(state->xdisplay));
	switch(bpp)
	{
	default:
		fprintf(stderr,"%s: %dbpp not supported.\n",__FUNCTION__,bpp);
		return 0;

	case 24:
	case 32:
		state->convert_fn=&HostGfx_ConvertTo32;
		state->refresh_convert_table_fn=&HostGfx_RefreshConvertTableTo32;
		break;
	}

	state->primary_desc.ddpfPixelFormat.dwRBitMask=0x00FF0000;
	state->primary_desc.ddpfPixelFormat.dwGBitMask=0x0000FF00;
	state->primary_desc.ddpfPixelFormat.dwBBitMask=0x000000FF;

	return 1;
}}}

// XPixmap adapter {{{
struct XPixmapAdapterData
//{{{
{
	XImage *ximage;
};
typedef struct XPixmapAdapterData XPixmapAdapterData;
//}}}

static int XPixmapAdapter_Start(HostGfxState *state)
{{{
	XPixmapAdapterData *const data=state->adapter_data;

	data->ximage=0;

	if(!SetPixmapConvertFn(state))
		return 0;
	
	return 1;
}}}

static void XPixmapAdapter_Stop(HostGfxState *state)
{{{
	XPixmapAdapterData *const data=state->adapter_data;

	if(data->ximage)
	{
		free(data->ximage->data);
		data->ximage->data=0;

		XDestroyImage(data->ximage);
		data->ximage=0;
	}

	return;
}}}

static int XPixmapAdapter_GetConvertBuffer(HostGfxState *state,int w,int h,void **buffer,unsigned *stride)
{{{
	XPixmapAdapterData *const data=state->adapter_data;

	if(!data->ximage||w!=data->ximage->width||h!=data->ximage->height)
	{
		char *buf;

		XPixmapAdapter_Stop(state);

		buf=malloc(w*h*4);
		data->ximage=XCreateImage(state->xdisplay,state->xvisual,24,ZPixmap,0,buf,w,h,32,w*4);
		if(!data->ximage)
		{
			fprintf(stderr,"%s: XCreateImage failed.\n",__FUNCTION__);
			free(buf);
			data->ximage=0;
		}
	}

	if(!data->ximage)
		return 0;

	*buffer=data->ximage->data;
	*stride=w*4;

	return 1;
}}}

static void XPixmapAdapter_Present(HostGfxState *state)
{{{
	XPixmapAdapterData *const data=state->adapter_data;

	XSync(state->xdisplay,False); 
	XPutImage(state->xdisplay,state->xwindow,state->xgc,data->ximage,0,0,0,0,data->ximage->width,data->ximage->height);
}}}

static const Adapter g_x_pixmap_adapter=
//{{{
{
	&XPixmapAdapter_Start,
	&XPixmapAdapter_GetConvertBuffer,
	&XPixmapAdapter_Present,
	&XPixmapAdapter_Stop,
	sizeof(XPixmapAdapterData),
	0,
	{
		1,
		0,
		0,0,
		"X Pixmap",
		1,
	},
};
//}}}
//}}}

// XShm adapter {{{
struct XShmAdapterData
//{{{
{
	XShmSegmentInfo *xssi;
	Bool shm_attached;
	XImage *ximage;
};
typedef struct XShmAdapterData XShmAdapterData;
//}}}

static int XShmAdapter_Start(HostGfxState *state)
{{{
	XShmAdapterData *const data=state->adapter_data;

	data->xssi=0;
	data->shm_attached=False;
	data->ximage=0;

	if(!SetPixmapConvertFn(state))
		return 0;


	return 1;
}}}

static void XShmAdapter_Stop(HostGfxState *state)
{{{
	XShmAdapterData *const data=state->adapter_data;

	if(data->shm_attached)
	{
		XShmDetach(state->xdisplay,data->xssi);
		data->shm_attached=False;
	}

	if(data->ximage)
	{
		XDestroyImage(data->ximage);
		data->ximage=0;
	}

	if(data->xssi&&data->xssi->shmaddr)
		shmdt(data->xssi->shmaddr);

	free(data->xssi);
	data->xssi=0;
}}}

static int XShmAdapter_GetConvertBuffer(HostGfxState *state,int w,int h,void **buffer,unsigned *stride)
{{{
	XShmAdapterData *const data=state->adapter_data;

	if(!data->ximage||w!=data->ximage->width||h!=data->ximage->height)
	{
		XShmAdapter_Stop(state);

		data->xssi=malloc(sizeof *data->xssi);
		memset(data->xssi,0,sizeof *data->xssi);
		data->xssi->shmaddr=0;

		data->ximage=XShmCreateImage(state->xdisplay,state->xvisual,24,ZPixmap,0,data->xssi,w,h);
		if(!data->ximage)
		{
			fprintf(stderr,"%s: XShmCreateImage failed.\n",__FUNCTION__);
			return 0;
		}

		//
		data->xssi->shmid=shmget(IPC_PRIVATE,data->ximage->bytes_per_line*data->ximage->height,IPC_CREAT|0777);
		if(data->xssi->shmid<0)
		{
			fprintf(stderr,"%s: shmget failed.\n",__FUNCTION__);
			goto err_no_shmid;
		}

		//
		data->xssi->shmaddr=shmat(data->xssi->shmid,0,0);
		if(!data->xssi->shmaddr)
		{
			fprintf(stderr,"%s: shmat failed.\n",__FUNCTION__);
			goto err;
		}

		data->ximage->data=data->xssi->shmaddr;

		//
		data->xssi->readOnly=False;

		//
		if(!XShmAttach(state->xdisplay,data->xssi))
		{
			fprintf(stderr,"%s: XShmAttach failed.\n",__FUNCTION__);
			goto err;
		}
		data->shm_attached=True;

err:
		//http://www.mail-archive.com/xfree86@xfree86.org/msg11364.html
		shmctl(data->xssi->shmid,IPC_RMID,0);
err_no_shmid:
		;
	}

	if(!data->ximage||!data->shm_attached)
	{
		return 0;
	}

	*buffer=data->ximage->data;
	*stride=w*4;

	return 1;
}}}

static void XShmAdapter_Present(HostGfxState *state)
{{{
	XShmAdapterData *const data=state->adapter_data;

	XSync(state->xdisplay,False);

	XShmPutImage(state->xdisplay,state->xwindow,state->xgc,data->ximage,0,0,0,0,data->ximage->width,data->ximage->height,False);
}}}

static const Adapter g_x_shm_adapter=
//{{{
{
	&XShmAdapter_Start,
	&XShmAdapter_GetConvertBuffer,
	&XShmAdapter_Present,
	&XShmAdapter_Stop,
	sizeof(XShmAdapterData),
	0,
	{
		1,
		0,
		0,0,
		"XShm",
		1,
	},
};
//}}}
//}}}

// Xv adapter {{{
struct XvAdapterPotentialPort
//{{{
{
	XvPortID id;
	XvImageFormatValues best_fmt;
};
typedef struct XvAdapterPotentialPort XvAdapterPotentialPort;
//}}}

struct XvAdapterStaticData
//{{{
{
	int num_ports;
	XvAdapterPotentialPort ports[0];
};
typedef struct XvAdapterStaticData XvAdapterStaticData;
//}}}

struct XvAdapterData
//{{{
{
	XShmSegmentInfo *xssi;
	Bool shm_attached;
	XvImage *xvimage;
	const XvAdapterPotentialPort *port;
};
typedef struct XvAdapterData XvAdapterData;
//}}}

static int XvAdapter_Start(HostGfxState *state)
{{{
	XvAdapterData *const data=state->adapter_data;

	data->xssi=0;
	data->shm_attached=False;
	data->xvimage=0;
	data->port=0;

	return 1;
}}}

static void XvAdapter_Stop(HostGfxState *state)
{{{
	XvAdapterData *const data=state->adapter_data;

	if(data->port)
	{
		XvUngrabPort(state->xdisplay,data->port->id,CurrentTime);
		data->port=0;
	}

	if(data->shm_attached)
	{
		XShmDetach(state->xdisplay,data->xssi);
		data->shm_attached=False;
	}

	if(data->xvimage)
	{
		XFree(data->xvimage);
		data->xvimage=0;
	}

	if(data->xssi&&data->xssi->shmaddr)
		shmdt(data->xssi->shmaddr);

	free(data->xssi);
	data->xssi=0;
}}}

static int XvAdapter_GetConvertBuffer(HostGfxState *state,int w,int h,void **buffer,unsigned *stride)
{{{
	const XvAdapterStaticData *const static_data=state->adapter->static_data;
	XvAdapterData *const data=state->adapter_data;

	if(!data->xvimage||data->xvimage->width!=w||data->xvimage->height!=h)
	{
		int i;

		XvAdapter_Stop(state);

		// I guess this is the way this is supposed to be done?!
		for(i=0;i<static_data->num_ports;++i)
		{
			VERBOSITY(("%s: Trying port %lu; ",__FUNCTION__,static_data->ports[i].id));
			if(XvGrabPort(state->xdisplay,static_data->ports[i].id,CurrentTime)==Success)
			{
				VERBOSITY(("success.\n"));
				data->port=&static_data->ports[i];
				break;
			}
			else
				VERBOSITY(("failed.\n"));
		}

		if(!data->port)
			return 0;

		data->xssi=malloc(sizeof *data->xssi);
		memset(data->xssi,0,sizeof *data->xssi);
		data->xssi->shmaddr=0;

		data->xvimage=XvShmCreateImage(state->xdisplay,data->port->id,data->port->best_fmt.id,0,w,h,data->xssi);
		if(!data->xvimage)
		{
			fprintf(stderr,"%s: XvShmCreateImage failed.\n",__FUNCTION__);
			return 0;
		}

		data->xssi->shmid=shmget(IPC_PRIVATE,data->xvimage->pitches[0]*data->xvimage->height,IPC_CREAT|0777);
		if(data->xssi->shmid<0)
		{
			fprintf(stderr,"%s: shmget failed.\n",__FUNCTION__);
			goto err_no_shmid;
		}

		data->xssi->shmaddr=shmat(data->xssi->shmid,0,0);
		if(!data->xssi->shmaddr)
		{
			fprintf(stderr,"%s: shmat failed.\n",__FUNCTION__);
			goto err;
		}

		data->xvimage->data=data->xssi->shmaddr;
		data->xssi->readOnly=False;

		if(!XShmAttach(state->xdisplay,data->xssi))
		{
			fprintf(stderr,"%s: XShmAttach failed.\n",__FUNCTION__);
			goto err;
		}
		data->shm_attached=True;

err:
		shmctl(data->xssi->shmid,IPC_RMID,0);
err_no_shmid:
		;
	}

	if(!data->xvimage||!data->shm_attached)
		return 0;

	*buffer=data->xvimage->data+data->xvimage->offsets[0];
	*stride=data->xvimage->pitches[0];

	return 1;
}}}

static void XvAdapter_Present(HostGfxState *state)
{{{
	XvAdapterData *const data=state->adapter_data;
	Window root_xwindow;
	int dest_x,dest_y;
	unsigned dest_w,dest_h,dest_border_width,dest_depth;
	int r;
	
	XSync(state->xdisplay,False);

	// I had XGetGeometry returning something other than Success, but the values looked ok...
	r=XGetGeometry(state->xdisplay,state->xtarget,&root_xwindow,&dest_x,&dest_y,&dest_w,&dest_h,&dest_border_width,&dest_depth);
	/*if(r!=Success)*/
		/*fprintf(stderr,"%s: XGetGeometry failed: %d.\n",__FUNCTION__,r);*/
	/*else*/
	{
		//VERBOSITY(("%s: dest_x=%d dest_y=%d dest_w=%d dest_h=%d dest_border_width=%d dest_depth=%d\n",__FUNCTION__,dest_x,dest_y,dest_w,dest_h,dest_border_width,dest_depth));

		dest_y=dest_x=0;
		
		r=XvShmPutImage(state->xdisplay,data->port->id,state->xtarget,state->xgc,data->xvimage,0,0,data->xvimage->width,data->xvimage->height,dest_x,dest_y,dest_w,dest_h,False);
		if(r!=Success)
			fprintf(stderr,"%s: XvShmPutImage failed: %d.\n",__FUNCTION__,r);
	}
}}}

static const Adapter g_xv_adapter=
//{{{
{
	&XvAdapter_Start,
	&XvAdapter_GetConvertBuffer,
	&XvAdapter_Present,
	&XvAdapter_Stop,
	sizeof(XvAdapterData),
	0,
	{
		1,
		0,
		0,0,
		"Xv",
		1,
	},
};
//}}}
//}}}

// Xv info dump {{{
#define FLAG_STR(F,M)\
	((F)&(M)?" "#M:"")

#define FOURCC_CHAR2(X,N)\
	((char)((((uint32_t)(X))>>(24-N*8))&0xFF))

#define FOURCC_CHAR(X,N)\
	(isprint(FOURCC_CHAR2(X,N))?FOURCC_CHAR2(X,N):'-')

static void DumpXvInfo(Display *display,Drawable drawable)
{{{
	int num_adapters,i;
	XvAdaptorInfo *adapters;

	if(XvQueryAdaptors(display,drawable,&num_adapters,&adapters)!=Success)
	{
		fprintf(stderr,"%s: XvQueryAdapters failed.\n",__FUNCTION__);
		return;
	}

	VERBOSITY(("%d Xv adapters:\n",num_adapters));
	for(i=0;i<num_adapters;++i)
	{
		XvAdaptorInfo *a;
		unsigned long j;
		
		a=&adapters[i];
		VERBOSITY(("%d: \"%s\":\n",i,a->name));
		VERBOSITY(("    base_id=%lu num_ports=%lu num_adaptors=%lu\n",a->base_id,a->num_ports,a->num_adaptors));
		VERBOSITY(("    type:%s%s%s%s%s\n",FLAG_STR(a->type,XvInputMask),FLAG_STR(a->type,XvOutputMask),FLAG_STR(a->type,XvImageMask),FLAG_STR(a->type,XvVideoMask),FLAG_STR(a->type,XvStillMask)));
		VERBOSITY(("    %lu formats:\n",a->num_formats));
		for(j=0;j<a->num_formats;++j)
			VERBOSITY(("    %lu: depth=%d visual_id=%lu\n",j,a->formats[j].depth,a->formats[j].visual_id));

		VERBOSITY(("    Port attributes:\n"));
		for(j=0;j<a->num_ports;++j)
		{
			XvPortID id;
			XvAttribute *attrs;
			int num_attrs;
#ifdef XV_FORMATS_VERBOSITY
			XvImageFormatValues *fmts;
			int num_fmts;
#endif//XV_FORMATS_VERBOSITY

			id=a->base_id+j;

			VERBOSITY(("    %lu: id=%lu, ",j,id));
			if(!(attrs=XvQueryPortAttributes(display,id,&num_attrs)))
				VERBOSITY(("no attributes.\n"));
			else
			{
				int k;

				VERBOSITY(("%d attributes:\n",num_attrs));
				for(k=0;k<num_attrs;++k)
				{
					const XvAttribute *attr;

					attr=&attrs[k];
					VERBOSITY(("        %d: \"%s\": min=%d max=%d flags:%s%s value=",k,attr->name,attr->min_value,attr->max_value,FLAG_STR(attr->flags,XvGettable),FLAG_STR(attrs->flags,XvSettable)));
					if(!(attr->flags&XvGettable))
						VERBOSITY(("?"));
					else
					{
						int value;

						if(XvGetPortAttribute(display,id,XInternAtom(display,attr->name,False),&value)!=Success)
							VERBOSITY(("(XGetPortAttribute failed)"));
						else
							VERBOSITY(("%d",value));
					}
					VERBOSITY(("\n"));
				}
				XFree(attrs);
			}

#ifdef XV_FORMATS_VERBOSITY
			if(!(fmts=XvListImageFormats(display,id,&num_fmts)))
				VERBOSITY(("        no formats.\n"));
			else
			{
				int k;

				VERBOSITY(("        %d fmts:\n",num_fmts));
				for(k=0;k<num_fmts;++k)
				{
					const XvImageFormatValues *fmt;

					fmt=&fmts[k];

					VERBOSITY(("        %d: id=%d (%c%c%c%c) type=%s order=%s bpp=%d format=%s num_planes=%d\n",k,fmt->id,FOURCC_CHAR(fmt->id,0),FOURCC_CHAR(fmt->id,1),FOURCC_CHAR(fmt->id,2),FOURCC_CHAR(fmt->id,3),fmt->type==XvRGB?"XvRGB":fmt->type==XvYUV?"XvYUV":"?",fmt->byte_order==LSBFirst?"LSBFirst":fmt->byte_order==MSBFirst?"MSBFirst":"?",fmt->bits_per_pixel,fmt->format==XvPacked?"XvPacked":fmt->format==XvPlanar?"XvPlanar":"?",fmt->num_planes));
					if(fmt->type==XvYUV)
					{
						VERBOSITY(("            %d%d%d YUV component_order=\"%.32s\" scanline_order=%s\n",fmt->y_sample_bits,fmt->u_sample_bits,fmt->v_sample_bits,fmt->component_order,fmt->scanline_order==XvTopToBottom?"XvTopToBottom":fmt->scanline_order==XvBottomToTop?"XvBottomToTop":"?"));
						VERBOSITY(("            horz: y=%d u=%d v=%d; vert: y=%d u=%d v=%d\n",fmt->horz_y_period,fmt->horz_u_period,fmt->horz_v_period,fmt->vert_y_period,fmt->vert_u_period,fmt->vert_v_period));
					}
					else
						VERBOSITY(("            depth=%d R=0x%08X G=0x%08X B=0x%08X\n",fmt->depth,fmt->red_mask,fmt->green_mask,fmt->blue_mask));
				}
			}
#endif//XV_FORMATS_VERBOSITY
		}
	}


	XvFreeAdaptorInfo(adapters);
}}}
//}}}

static int AddAdapter(HostGfxState *state,const Adapter *adapter,void *static_data,const char *device_sub_name)
{{{
	int i;
	Adapter *new_adapter;

	state->adapters=realloc(state->adapters,(state->num_adapters+1)*sizeof *state->adapters);
	state->adapters[state->num_adapters]=*adapter;
	new_adapter=&state->adapters[state->num_adapters];

	new_adapter->static_data=static_data;
	if(device_sub_name)
		snprintf(new_adapter->hgfx_adapter.device_name,sizeof new_adapter->hgfx_adapter.device_name,"%s (%s)",adapter->hgfx_adapter.device_name,device_sub_name);

	state->hgfx_adapters=realloc(state->hgfx_adapters,(state->num_adapters+1)*sizeof *state->hgfx_adapters);
	++state->num_adapters;

	for(i=0;i<state->num_adapters;++i)
		state->hgfx_adapters[i]=&state->adapters[i].hgfx_adapter;

//	VERBOSITY(("%s: added adapter %d, \"%s\" (%p).\n",__FUNCTION__,state->num_adapters,state->hgfx_adapters[state->num_adapters]->device_name,state->hgfx_adapters[state->num_adapters]));

	return state->num_adapters-1;
}}}

static int AddXvAdapters(HostGfxState *state,int best_adapter_so_far)
{{{
	int num_adapters,i;
	XvAdaptorInfo *adapters;

	if(XvQueryAdaptors(state->xdisplay,state->xwindow,&num_adapters,&adapters)!=Success)
	{
		fprintf(stderr,"%s: XvQueryAdapters failed.\n",__FUNCTION__);
		return best_adapter_so_far;
	}

	for(i=0;i<num_adapters;++i)
	{
		XvAdapterStaticData *static_data;
		XvAdaptorInfo *a;
		unsigned long j;
		
		a=&adapters[i];
		if(!(a->type&XvImageMask))
			continue;

		static_data=malloc(sizeof *static_data);
		static_data->num_ports=0;

		for(j=0;j<a->num_ports;++j)
		{
			XvPortID port_id;
			XvImageFormatValues *fmts;
			int num_fmts,k;

			port_id=a->base_id+j;

			if(!(fmts=XvListImageFormats(state->xdisplay,port_id,&num_fmts)))
				continue;

			// Just pick first chunky RGB format for now.
			for(k=0;k<num_fmts;++k)
				if(fmts[k].type==XvRGB&&fmts[k].num_planes==1)
					break;

			if(k<num_fmts)
			{
				XvAdapterPotentialPort *pp;

				static_data=realloc(static_data,sizeof *static_data+(static_data->num_ports+1)*sizeof *static_data->ports);
				pp=&static_data->ports[static_data->num_ports++];

				pp->id=port_id;
				pp->best_fmt=fmts[k];

				VERBOSITY(("%s: fmt %d is best.\n",a->name,pp->best_fmt.id));
			}

			XFree(fmts);
		}

		if(static_data->num_ports==0)
		{
			VERBOSITY(("%s: Xv adapter \"%s\" will not be used, as it does not support any RGB pixel formats.\n",__FUNCTION__,a->name));
			free(static_data);
		}
		else
		{
			VERBOSITY(("%s: Adding Xv adapter \"%s\", with %d potential ports.\n",__FUNCTION__,a->name,static_data->num_ports));
			AddAdapter(state,&g_xv_adapter,static_data,a->name);
		}
	}		
	
	return best_adapter_so_far;
}}}

HostGfxState *HostGfx_Start(void *display,unsigned long window,unsigned long target,unsigned flags)
{{{
	HostGfxState *state;
	int i,shm_maj,shm_min;
	Bool shm_pixmaps;
	int xv_maj,xv_min,xv_request_base,xv_event_base,xv_error_base;
	int best_adapter;

	VERBOSITY(("%s: X display depth=%d.\n",__FUNCTION__,DefaultDepth(display,DefaultScreen(display))));

	if(!(flags&HGF_NO_SHM))
	{
		if(!XShmQueryVersion(display,&shm_maj,&shm_min,&shm_pixmaps))
		{
			fprintf(stderr,"%s: Warning: XShm unavailable.\n",__FUNCTION__);
			flags&=~HGF_NO_SHM;
		}
		else
		{
			if(XvQueryExtension(display,&xv_maj,&xv_min,&xv_request_base,&xv_event_base,&xv_error_base)==Success)
				DumpXvInfo(display,window);
			else
			{
				fprintf(stderr,"%s: Warning: Xv unavailable.\n",__FUNCTION__);
				flags&=~HGF_NO_XV;
			}
		}
	}

	if(flags&HGF_NO_SHM)
		VERBOSITY(("%s: XShm will not be used.\n",__FUNCTION__));
	else
	{
		VERBOSITY(("%s: XShm version is %d.%d; shm_pixmaps=%d.\n",__FUNCTION__,shm_maj,shm_min,shm_pixmaps));
		if(flags&HGF_NO_XV)
			VERBOSITY(("%s: Xv will not be used.\n",__FUNCTION__));
		else
		{
			VERBOSITY(("%s: Xv version is %d.%d.\n",__FUNCTION__,xv_maj,xv_min));
		}
	}

	//
	state=malloc(sizeof *state);
	memset(state,0,sizeof *state);

	//
	state->flags=flags;
	state->xdisplay=display;
	state->xwindow=window;
	state->xtarget=target;
	state->xvisual=DefaultVisualOfScreen(DefaultScreenOfDisplay(display));
	
	//
	state->xgc=XCreateGC(state->xdisplay,state->xwindow,0,0);

	{
		XGCValues v;
		unsigned long xgcmask;

		xgcmask=GCFunction|GCSubwindowMode|GCForeground|GCBackground;

		XGetGCValues(state->xdisplay,state->xgc,xgcmask,&v);

		if(v.function!=GXcopy)
		{
			VERBOSITY(("%s: XGC: Setting function to GXcopy.\n",__FUNCTION__));
			v.function=GXcopy;
		}

		if(v.subwindow_mode!=IncludeInferiors)
		{
			VERBOSITY(("%s: XGC: Setting subwindow mode to IncludeInferiors.\n",__FUNCTION__));
			v.subwindow_mode=IncludeInferiors;
		}

		VERBOSITY(("%s: XGC: colours were fg=0x%08lX bg=0x%08lX.\n",__FUNCTION__,v.foreground,v.background));
		v.foreground=WhitePixel(state->xdisplay,DefaultScreen(state->xdisplay));
		v.background=BlackPixel(state->xdisplay,DefaultScreen(state->xdisplay));

		VERBOSITY(("%s: XGC: colours now fg=0x%08lX bg=0x%08lX.\n",__FUNCTION__,v.foreground,v.background));

		XChangeGC(state->xdisplay,state->xgc,xgcmask,&v);
	}
	
	//
	for(i=0;i<8;++i)
	{
		state->palette[i][0]=i&1?1.f:0.f;
		state->palette[i][1]=i&2?1.f:0.f;
		state->palette[i][2]=i&4?1.f:0.f;
	}

	//
	VERBOSITY(("%s: display=%p window=0x%lX\n",__FUNCTION__,display,window));
	
	//
	HostGfxBufferFormat_SetPixelElement(&state->best_fmt,0,1);
	HostGfxBufferFormat_SetPixelElement(&state->best_fmt,1,2);
	HostGfxBufferFormat_SetPixelElement(&state->best_fmt,2,4);
	state->best_fmt.fixed_bits=0;
	state->best_fmt.bypp=1;

	state->convert_fn=0;
	state->refresh_convert_table_fn=0;

	state->adapters=0;
	state->num_adapters=0;

	state->adapter=0;

	best_adapter=AddAdapter(state,&g_x_pixmap_adapter,0,0);

	if(!(state->flags&HGF_NO_SHM))
	{
		best_adapter=AddAdapter(state,&g_x_shm_adapter,0,0);

		if(!(state->flags&HGF_NO_XV))
			best_adapter=AddXvAdapters(state,best_adapter);
	}

	HostGfx_SetAdapter(state,best_adapter);

	return state;
}}}

void HostGfx_Stop(HostGfxState *state)
{{{
	int i;

	if(!state)
		return;

	HostGfx_SetAdapter(state,-1);
	
	XFreeGC(state->xdisplay,state->xgc);

	for(i=0;i<state->num_adapters;++i)
		free(state->adapters[i].static_data);

	free(state->adapters);
	free(state->hgfx_adapters);

	free(state);
}}}

void HostGfx_Present(HostGfxState *state,const HostGfxBuffer *buffer,int y_first,int line_height,int gap_height,unsigned present_hints,uint64_t *convert_time,uint64_t *present_time)
{{{
	int reqd_width,reqd_height;
	void *convert_buffer;
	unsigned convert_buffer_stride;

	reqd_width=buffer->w;
	reqd_height=buffer->h*(line_height+gap_height);

	if(convert_time)
		*convert_time=0;

	if(present_time)
		*present_time=0;

	/*if(!state->ok||state->ximage->width!=reqd_width||state->ximage->height!=reqd_height)*/
	/*{*/
		/*if(!state->ok)*/
			/*PROGRESS(("%s: Creating new XImage.\n",__FUNCTION__));*/
		/*else*/
			/*PROGRESS(("%s: Requires %dx%d, XImage %dx%d -- recreating XImage.\n",__FUNCTION__,reqd_width,reqd_height,state->ximage->width,state->ximage->height));*/
		
		/*DeleteXImage(state);*/
		/*CreateXImage(state,reqd_width,reqd_height);*/
	/*}*/

	if(!state->adapter)
		return;

	if(!(*state->adapter->get_convert_buffer_fn)(state,reqd_width,reqd_height,&convert_buffer,&convert_buffer_stride))
	{
		PROGRESS(("%s: get_convert_buffer_fn failed.\n",__FUNCTION__));
		HostGfx_SetAdapter(state,-1);
	}
	else
	{
		HostTmr_UhfValue a,b;

		HostTmr_ReadUhf(&a);
		(*state->convert_fn)(state,convert_buffer,convert_buffer_stride,buffer->bitmap,buffer->w,buffer->pitch,buffer->h,y_first,line_height,gap_height,present_hints);
		HostTmr_ReadUhf(&b);
		if(convert_time)
		{
			*convert_time+=b-a;
			//printf("a=%llu b=%llu convert_time=%llu\n",a,b,*convert_time);
		}

		HostTmr_ReadUhf(&a);

		(*state->adapter->present_fn)(state);

		/*XSync(state->xdisplay,False);*/

		/*if(state->flags&HGF_NO_SHM)*/
		/*{*/
			// Not clear what the error return is (or even if there is one)
			/*XPutImage(state->xdisplay,state->xwindow,state->xgc,state->ximage,0,0,0,0,state->ximage->width,state->ximage->height);*/
		/*}*/
		/*else*/
		/*{*/
			/*if(XShmPutImage(state->xdisplay,state->xwindow,state->xgc,state->ximage,0,0,0,0,state->ximage->width,state->ximage->height,False)==0)*/
				/*fprintf(stderr,"%s: XShmPutImage failed.\n",__FUNCTION__);*/
		/*}*/

		// this seems to take up a fair bit of time -- possibly serializing cpu & gpu? (not sure how it works)
		// TODO i'm not using the send_event arg of XShmPutImage, so i guess the new data could then overwrite the old, or cause
		// visual glitches (etc.)?
		//XSync(state->xdisplay,False);
		
		HostTmr_ReadUhf(&b);
		if(present_time)
			*present_time+=b-a;
	}
}}}

void HostGfx_SetPalette(HostGfxState *state,const float new_palette[8][3])
{{{
	memcpy(state->palette,new_palette,8*3*sizeof(float));

}}}

void HostGfx_SetFullScreen(HostGfxState *state,int full_screen,int full_screen_refresh_rate)
{{{
	return;
}}}

int HostGfx_IsFullScreen(HostGfxState *state)
{{{
	return 0;
}}}

void HostGfx_Cls(HostGfxState *state)
{{{
	return;
}}}

const HostGfxBufferFormat *HostGfx_BestBufferFormat(HostGfxState *state)
{{{
	return &state->best_fmt;
}}}

const HostGfx_Adapter *const *HostGfx_GetAdapters(HostGfxState *state)
{{{
	return state->hgfx_adapters;
}}}

int HostGfx_NumAdapters(HostGfxState *state)
{{{
	return state->num_adapters;
}}}

void HostGfx_SetAdapter(HostGfxState *state,int adapter_index)
{{{
	const Adapter *new_adapter;

	if(adapter_index<0||adapter_index>=state->num_adapters)
		new_adapter=0;
	else
		new_adapter=&state->adapters[adapter_index];

	if(state->adapter==new_adapter)
		return;

	if(state->adapter)
	{
		VERBOSITY(("%s: Stopping \"%s\".\n",__FUNCTION__,state->adapter->hgfx_adapter.device_name));
		(*state->adapter->stop_fn)(state);
	}
	free(state->adapter_data);

	state->adapter=new_adapter;

	if(!state->adapter)
		VERBOSITY(("%s: no adapter set.\n",__FUNCTION__));
	else
	{
		VERBOSITY(("%s: starting \"%s\"; ",__FUNCTION__,state->adapter->hgfx_adapter.device_name));
		if(state->adapter->data_size==0)
			state->adapter_data=0;
		else
			state->adapter_data=malloc(state->adapter->data_size);

		if(!(*state->adapter->start_fn)(state))
		{
			(*state->adapter->stop_fn)(state);
			free(state->adapter_data);
			state->adapter_data=0;
			state->adapter=0;

			VERBOSITY(("failed to start."));
		}
		else
		{
			if(state->refresh_convert_table_fn)
				(*state->refresh_convert_table_fn)(state);

			VERBOSITY(("started ok."));
		}

		VERBOSITY(("\n"));
	}
}}}

#include "../HostWin32/HostGfxConvert.inl"//TODO

