#include "../port.h"
#include "Host.h"
#include <stdio.h>
#include <stdarg.h>

//////////////////////////////////////////////////////////////////////////
//
static Host_LogOutputCallbackFn log_output_fn=0;

void Host_SetLogOutputCallback(Host_LogOutputCallbackFn callback) {
	log_output_fn=callback;
}

int dprintf(const char *fmt,...) {
	int n=0;

	if(log_output_fn) {
		char tmpbuf[1024];
		va_list v;

		va_start(v,fmt);
		n=vsnprintf(tmpbuf,sizeof tmpbuf,fmt,v);
		va_end(v);

		(*log_output_fn)(tmpbuf);
	}

	return n;
}

