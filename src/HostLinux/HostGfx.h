#ifndef HOSTGFX_H_
#define HOSTGFX_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <HostGfxShared.h>

//////////////////////////////////////////////////////////////////////////
//functions
//
enum HostGfxFlags
{
	// If set, don't use XShm, even if available.
	HGF_NO_SHM=1,

	// If set, don't use Xv, even if available.
	// Ignored if HGF_NO_SHM.
	HGF_NO_XV=2,
};

//Create a HostGfxState that can be used to draw into the client area of
//the specified window.
//
//(unsigned long = XID... but without the headers)
HostGfxState *HostGfx_Start(void *display,unsigned long window,unsigned long target,unsigned flags);

#ifdef __cplusplus
}
#endif

#endif

