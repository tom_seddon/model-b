#ifndef HOSTTMR_H_
#define HOSTTMR_H_

#include <HostTmrShared.h>

#ifdef __cplusplus
extern "C" {
#endif

//High frequency timer -- read current reading, or query its frequency.
//(This needs to be accurate to at least a millisecond or so, which the
//Windows millisecond timers aren't.)

typedef int HostTmr_HfValue;

void HostTmr_ReadHf(HostTmr_HfValue *value);

int HostTmr_GetHfFrequency(HostTmr_HfValue *frequency);

// UHF timer (RDTSC on x86)

#ifndef PORT_X86

typedef HostTmr_HfValue HostTmr_UhfValue;
#define HostTmr_ReadUhf(X) HostTmr_ReadHf(X)
#define HostTmr_GetUhfFrequency(X) HostTmr_GetHfFrequency(X)

#else

typedef uint64_t HostTmr_UhfValue;

static __inline__ void HostTmr_ReadUhf(HostTmr_UhfValue *v)
{
	asm(
		"cpuid;\n"
		"rdtsc;\n"
		:"=A"(*v)
		:"a"(0)
		:"%ebx","%ecx");
}

void HostTmr_GetUhfFrequency(HostTmr_UhfValue *freq);

#endif

#ifdef __cplusplus
}
#endif

#endif

