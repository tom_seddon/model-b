#ifndef HOSTSND_H_
#define HOSTSND_H_

#include <HostSndShared.h>

#ifdef __cplusplus
extern "C" {
#endif

//Create a HostSndState that is tagged to the given window.
HostSndState *HostSnd_Start(void *window);

#ifdef __cplusplus
}
#endif

#endif

