#include "pch.h"
#include "bbcModelB.h"
//#include "bbcComputer.h"
#include "bbcCommonHardware.h"
#include "bbcKeyboardMatrix.h"

static const bbcSaveStateHandler model_b_handlers[]={
	{&bbcComputer::LoadRomSelBSaveState,&bbcComputer::SaveRomSelBSaveState},
	{&bbcComputer::LoadBbcComputerSaveState,&bbcComputer::SaveBbcComputerSaveState},
	{&bbcFdd::LoadSaveState,&bbcFdd::SaveSaveState},
	{&bbcLoadSystemVIASaveState,&bbcSaveSystemVIASaveState},
	{&bbcLoadUserVIASaveState,&bbcSaveUserVIASaveState},
	{&bbc1770::LoadSaveState,&bbc1770::SaveSaveState},
	{&bbcAdc::LoadSaveState,&bbcAdc::SaveSaveState},
	{&bbcVideo::LoadSaveState,&bbcVideo::SaveSaveState},
	{&bbcKeyboardMatrix::LoadSaveState,&bbcKeyboardMatrix::SaveSaveState},
	{0,0,},
};

const bbcModelB bbcModelB::model;

const bbcMmioFnInit *bbcModelB::ExtraMmioFns() const {
	static const bbcMmioFnInit mmio_fns[]={
		{0xfe30,16,&bbcComputer::ReadRomSelB,&bbcComputer::WriteRomSelB},
		{0,0},
	};

	return mmio_fns;
}

#ifdef bbcUPDATE_IN_BBCMODEL
void bbcModelB::Update() const {
	bbcVideo::Update();
	bbc_system_via.Update();
	bbc_user_via.Update();
	bbc1770::Update();
	bbcAdc::Update();
#ifdef bbcSERIAL_ENABLE
	bbcAciaUpdate();
#endif
}
#endif//bbcUPDATE_IN_BBCMODEL

const bbcSaveStateHandler *bbcModelB::GetSaveStateHandlers() const {
	return model_b_handlers;
}

//////////////////////////////////////////////////////////////////////////

const bbcModelB65C12 bbcModelB65C12::model;

const bbcMmioFnInit *bbcModelB65C12::ExtraMmioFns() const {
	static const bbcMmioFnInit mmio_fns[]={
		{0xfe30,16,&bbcComputer::ReadRomSelB,&bbcComputer::WriteRomSelB},
		{0,0},
	};

	return mmio_fns;
}

#ifdef bbcUPDATE_IN_BBCMODEL
void bbcModelB65C12::Update() const {
	bbcVideo::Update();
	bbc_system_via.Update();
	bbc_user_via.Update();
	bbc1770::Update();
	bbcAdc::Update();
#ifdef bbcSERIAL_ENABLE
	bbcAciaUpdate();
#endif
}
#endif//bbcUPDATE_IN_BBCMODEL
