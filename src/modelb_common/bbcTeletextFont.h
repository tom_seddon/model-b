#ifndef bbcTELETEXTFONT_H_
#define bbcTELETEXTFONT_H_

struct bbcTeletextFont {
	static void Init();

	static t65::word normal_chars[96][10];
	static t65::word graphics_chars[96][10];
	static t65::word sep_graphics_chars[96][10];
	static const t65::byte normal_chars_src[96*60];
	static const t65::byte graphics_chars_src[96*60];
	static const t65::byte sep_graphics_chars_src[96*60];
private:
	static void Convert(const t65::byte src[96*60],t65::word dest[96][10]);
};

#endif
