#include "pch.h"
#include "bbcSound.h"
#include "bbcComputer.h"

//http://web.inter.nl.net/users/J.Kortink/home/articles/sn76489/index.htm
//http://www.smspower.org/maxim/smschecker/SN76489.txt for the details

#define bbcFIXED_OF(N) (fixedpoint((N)*256))
#define bbcFLOAT_OF(N) ((N)/256.f)

t65::byte bbcSound::latch_;
unsigned bbcSound::write_idx_;
#ifdef bbcSOUND_WORD_CYCLES
bbcSound::SoundWriteEntryWord bbcSound::writes_[max_num_writes+1];
#else
bbcSound::SoundWriteEntry bbcSound::writes_[max_num_writes+1];
#endif
bbcSound::SoundRegister bbcSound::cur_regs_[4];//012 3=noise
int bbcSound::last_render_at_;
//bbcSound::sample bbcSound::volumes_16bit_[16];
//bbcSound::fixedpoint bbcSound::freq_mul_;
bbcSound::fixedpoint bbcSound::samples_per_switch_[1024];
int bbcSound::hz_from_tone_[1024];
t65::word bbcSound::noise_seed_;
int bbcSound::noise_bit_;
bool bbcSound::is_stereo_left_[4];
bool bbcSound::is_stereo_right_[4];

struct Render8BitTraits {
	typedef t65::byte sample_type;
	enum {
		silence=0x80,
		sample_min=0,
		sample_max=255,
	};
	static sample_type volumes[16];
};

struct Render16BitTraits {
	typedef t65::int16 sample_type;
	enum {
		silence=0x0000,
		sample_min=-32768,
		sample_max=32767,
	};
	static sample_type volumes[16];
};

Render8BitTraits::sample_type Render8BitTraits::volumes[16];
Render16BitTraits::sample_type Render16BitTraits::volumes[16];

bool bbcSound::is_recording_;
bbcSound::RecordedWrites bbcSound::recorded_writes_;

// See mail from Rich Talbot-Watkins

//0 (loudest)     1.00000               1.00000
//1               0.93333               0.79433
//2               0.86667               0.63096
//3               0.80000               0.50119
//4               0.73333               0.39811
//5               0.66667               0.31623
//6               0.60000               0.25119
//7               0.53333               0.19953
//8               0.46667               0.15849
//9               0.40000               0.12589
//10               0.33333               0.10000
//11               0.26667               0.07943
//12               0.20000               0.06310
//13               0.13333               0.05012
//14               0.66667               0.03981
//15 (silent)      0.00000               0.00000

static const float volumes_logarithmic[16]={
	1.00000f,
	0.79433f,
	0.63096f,
	0.50119f,
	0.39811f,
	0.31623f,
	0.25119f,
	0.19953f,
	0.15849f,
	0.12589f,
	0.10000f,
	0.07943f,
	0.06310f,
	0.05012f,
	0.03981f,
	0.00000f,
};

void bbcSound::Init() {
	unsigned i;

	write_idx_=0;
	latch_=0;
	last_render_at_=bbcSOUND_CYCLES();

	//Reset registers
	//
	//Volumes initially set to %1111 (silent)
	//Tone/noise set to 0
	for(i=0;i<4;++i) {
		SoundRegister *reg=&cur_regs_[i];
		reg->counter=bbcFIXED_OF(0);
		reg->pitch=1023;
		reg->vol=0;//silent
		reg->mul=1;
	}

	//44100Hz is the frequency at the mo'
	//freq_mul_=bbcFIXED_OF(250000/44100.f);

	for(i=1;i<1024;++i) {
		hz_from_tone_[i]=4000000/(32*i);
	}
	hz_from_tone_[0]=hz_from_tone_[1];
	
	SetFrequency(44100);

	for(i=0;i<16;++i) {
//		int v=i^15;
//		Render8BitTraits::volumes[i]=Render8BitTraits::sample_type(v/15.f*127);
//		Render16BitTraits::volumes[i]=Render16BitTraits::sample_type(v/15.f*32767);

		Render8BitTraits::volumes[i]=Render8BitTraits::sample_type(volumes_logarithmic[i]*127);
		Render16BitTraits::volumes[i]=Render16BitTraits::sample_type(volumes_logarithmic[i]*32767);
	}

	noise_seed_=0x4000;
	noise_bit_=0;//I don't think it really matters.

	is_recording_=false;
}

void bbcSound::SetFrequency(int hz) {
	for(unsigned i=0;i<1024;++i) {
		samples_per_switch_[i]=bbcFIXED_OF(float(hz)/hz_from_tone_[i]/2.f);
		if(samples_per_switch_[i]<bbcFIXED_OF(1)) {
			//This fixed the sound pretty much entirely...
			samples_per_switch_[i]=bbcFIXED_OF(0);
		}
	}
	samples_per_switch_[0]=bbcFIXED_OF(0);//Special.
}

void bbcSound::Write(t65::byte v) {
	int sound_cycles=bbcSOUND_CYCLES();
	if(write_idx_==0||writes_[write_idx_-1].cycles!=sound_cycles) {
		if(write_idx_==max_num_writes) {
			printf("bbcSound::Write: overflow. Resetting. Some sound will be lost.\n");
			write_idx_=0;
		} else {
			SoundWriteEntry *swe=&writes_[write_idx_];
			swe->cycles=sound_cycles;
			swe->val=v;
			++write_idx_;
		}
		if(is_recording_) {
			recorded_writes_.push_back(SoundWriteEntry(v,sound_cycles));
//			SoundWriteEntry *swe=&*recorded_writes_.insert(recorded_writes_.end(),
//				SoundWriteEntry());
//			swe->cycles=bbcSOUND_CYCLES();
//			swe->val=v;
		}
	}
}

//Updates current settings as if byte 'v' were just written
//register value is CCV
//CC bits dictate channel (0,1,2,3=noise)
//V dictates channel data -- 1=Volume, 0=Tone (for ch3, aka noise)

//#define REG_P(R) (((R)&1)?&cur_regs_[(R)>>1].vol:&cur_regs_[(R)>>1].pitch)

//THingies for incoming values
#define VAL_IS_LATCH(V) (!!((V)&0x80))

#define VAL_LATCH_REG(V) (((V)>>4)&7)
#define VAL_LATCH_DATA(V) ((V)&15)
#define VAL_DATA_DATA(V) ((V)&63)

//Internal register number thingies
#define REG_IS_VOLUME(R) (!!((R)&1))
#define REG_CHANNEL(R) (((R)>>1)&3)
//4bit registers used for volume and noise settings
#define REG_IS_4BIT(R) (REG_IS_VOLUME(R)||(REG_CHANNEL(R)==3))


void bbcSound::DoByte(t65::byte v) {
	t65::byte data;
	int shift;
	t65::word mask;
	if(VAL_IS_LATCH(v)) {
		latch_=VAL_LATCH_REG(v);
		data=VAL_LATCH_DATA(v);
		shift=0;
		mask=0x3f0;//%1111110000
	} else {
		data=VAL_DATA_DATA(v);
		if(REG_IS_4BIT(latch_)) {
			mask=0x3f0;
			shift=0;//4 bit registers get the lower 4 bits of the data
		} else {
			shift=4;
			mask=0x00f;
		}
	}
	SoundRegister *sreg=&cur_regs_[latch_>>1];
	t65::word *reg;
	if(latch_&1) {
		reg=&sreg->vol;
	} else {
		reg=&sreg->pitch;
//		sreg->counter=bbcFIXED_OF(0);
//		sreg->mul=1;
	}
	*reg&=mask;
	*reg|=(data<<shift)&~mask;
	if(latch_==6) {
		//Noise register write -- this resets the noise
		//seed
		noise_seed_=0x4000;
	}
}

int bbcSound::NextNoiseBit() {
	bool new_bit=!((noise_seed_^(noise_seed_>>1))&1);
	noise_seed_=(noise_seed_>>1)|(new_bit?(1<<14):0);
	return noise_seed_&1;
}

void bbcSound::SetChannelStereoLeftRight(int channel,bool is_left,bool is_right) {
	is_stereo_left_[channel]=is_left;
	is_stereo_right_[channel]=is_right;
}

bool bbcSound::IsRecording() {
	return is_recording_;
}

void bbcSound::StartRecording() {
	is_recording_=true;
}

void bbcSound::StopRecording() {
	is_recording_=false;
}

void bbcSound::ClearRecording() {
	recorded_writes_.clear();
}

void bbcSound::GetRecordedVgmData(std::vector<t65::byte> *vgm_data) {
	struct VgmHeader {
		char id[4];
		size_t end_offset;
		unsigned version;
		unsigned psg_speed;
		unsigned fm_speed;
		unsigned gd3_offset;
		unsigned num_samples;
		unsigned loop_offset;
		unsigned samples_per_loop;
		unsigned frame_rate;
	};

	BASSERT(vgm_data);
	vgm_data->clear();
	//INit header
	VgmHeader hdr;
	hdr.end_offset=0;//vgm_data_size-4;
	hdr.fm_speed=0;
	hdr.frame_rate=50;
	hdr.gd3_offset=0;
	hdr.id[0]='V';
	hdr.id[1]='g';
	hdr.id[2]='m';
	hdr.id[3]=' ';
	hdr.loop_offset=0;
	hdr.num_samples=0;//TBD
	hdr.psg_speed=4000000;
	hdr.samples_per_loop=0;
	hdr.version=0x101;
	
	//Will probably need 5 bytes per write -- 2 bytes for the write, and 3
	//bytes for the following pause.
	//Also 0x40 for the header.
	//vgm_data->resize(0x40+recorded_writes_.size()*5,0);
	vgm_data->resize(0x40,0);//64-byte header
	RecordedWrites::const_iterator write;
	bool first_write=true;
	int last_cycles=0;//inhibit spurious gcc warning (last_cycles only used if !first_write; and, if !first_write, last_cycles initialized)
	for(write=recorded_writes_.begin();write!=recorded_writes_.end();++write) {
		//If there was a previous one, we'll need a pause.
		if(!first_write) {
			//0x61 0xCD 0xAB (pause for 0xABCD samples [44100Hz])
			BASSERT(write->cycles-last_cycles>0);
			int wait_samples=int(((write->cycles-last_cycles)/250000.f)*44100.f);
			hdr.num_samples+=wait_samples;
			while(wait_samples>0) {
				vgm_data->push_back(0x61);
				vgm_data->push_back(wait_samples&0xff);
				vgm_data->push_back((t65::byte)(wait_samples>>8));
				wait_samples-=65536;
			}
		}
		first_write=false;
		last_cycles=write->cycles;

		//Save this register write
		vgm_data->push_back(0x50);
		vgm_data->push_back(write->val);
	}
	vgm_data->push_back(0x66);
	hdr.end_offset=vgm_data->size()-4;
	memcpy(&vgm_data->at(0),&hdr,sizeof hdr);
}

template<class T>
void bbcSound::RenderSound(void **buffers,unsigned *buffer_sizes_bytes,unsigned num_buffers,
	bool stereo)
{
	typedef t65TYPENAME T::sample_type sample_type;
	const unsigned bytes_per_sample=sizeof(sample_type);
	unsigned i;
	unsigned widx=0;
	unsigned cur_sample=0;
	unsigned cur_buffer=0;
	unsigned total_size_samples=0;
	bool do_left[4];
	int soundcycles=bbcSOUND_CYCLES()-last_render_at_;
	if(soundcycles==0) {
		return;
	}

	for(i=0;i<4;++i) {
		do_left[i]=is_stereo_left_[i]||(!stereo&&(is_stereo_left_[i]||is_stereo_right_[i]));
	}

	//Get total number of samples needed
	for(i=0;i<num_buffers;++i) {
		BASSERT(buffer_sizes_bytes[i]%bytes_per_sample==0);
		total_size_samples+=buffer_sizes_bytes[i]/bytes_per_sample;
	}
	if(stereo) {
		total_size_samples/=2;
	}

	float soundcycles_per_sample=soundcycles/float(total_size_samples);
	float this_c=float(last_render_at_);
	for(i=0;i<total_size_samples;++i) {
		while(widx<write_idx_&&writes_[widx].cycles<int(this_c)) {
			DoByte(writes_[widx].val);
			++widx;
		}

		//Mix tone channels
		//int samp=0;
		int left=0,right=0;
		for(unsigned j=0;j<3;++j) {
			SoundRegister *reg=&cur_regs_[j];
			fixedpoint sps=samples_per_switch_[reg->pitch&1023];
			int samp;

			if(sps==bbcFIXED_OF(0)) {
				samp=T::volumes[reg->vol&0xf];
			} else {
				samp=T::silence;
				if(is_stereo_left_[j]||is_stereo_right_[j]) {
					samp+=reg->mul*T::volumes[reg->vol&0xf];
				}
				reg->counter+=bbcFIXED_OF(1);
				if(reg->counter>=sps) {
					reg->mul=-reg->mul;
					reg->counter-=sps;
				}
			}

			if(do_left[j]) {
				left+=samp;
			}

			if(is_stereo_right_[j]) {
				right+=samp;
			}
		}

		//Mix noise channel
		{
			SoundRegister *reg=&cur_regs_[3];
			fixedpoint stop=0;//inhibit spurious gcc warning (all values of reg->pitch&3 are covered)
			switch(reg->pitch&3) {
			case 0:
				//High freq
				stop=0x10;
				break;
			case 1:
				//Mid freq
				stop=0x20;
				break;
			case 2:
				//Low freq
				stop=0x40;
				break;
			case 3:
				//Tone 2 freq
				stop=cur_regs_[2].pitch&1023;
				break;
			}
			stop=samples_per_switch_[stop];
			
			reg->counter+=bbcFIXED_OF(1);
			if(reg->counter>=stop) {
				reg->mul=-reg->mul;
				if(reg->mul>0) {
					//A new bit is output just once for every 2 transitions.
					if(reg->pitch&4) {
						//White noise
						noise_bit_=NextNoiseBit();
					} else {
						//Periodic noise -- outputs bottom seed bit, and
						//the seed is rotated. (15bit according to Kortink, 16bit according
						//to Maxim, I'm using 15bit here.)
						//
						noise_bit_=noise_seed_&1;
						noise_seed_=((noise_seed_>>1)|(noise_seed_<<14))&0x7FFF;
					}
				}
				reg->counter-=stop;//bbcFIXED_OF(0);
			}
			
			//Apparently, the noise output is either 0 or +1 rather than -1/+1.
			int samp=T::silence+noise_bit_*T::volumes[reg->vol&0xf];

			if(do_left[3]) {
				left+=samp;
			}

			if(is_stereo_right_[3]) {
				right+=samp;
			}
		}

		//Put in buffer
		sample_type *buffer=static_cast<sample_type *>(buffers[cur_buffer]);
		
		BASSERT(left/4>=T::sample_min&&left/4<=T::sample_max);
		buffer[cur_sample++]=sample_type(left/4);
		if(stereo) {
			BASSERT(right/4>=T::sample_min&&right/4<=T::sample_max);
			buffer[cur_sample++]=sample_type(right/4);
		}

		this_c+=soundcycles_per_sample;
		if(cur_sample*bytes_per_sample>=buffer_sizes_bytes[cur_buffer]) {
			BASSERT(cur_buffer<num_buffers);
			++cur_buffer;
			cur_sample=0;
		}
	}

	//Flush remaining writes.
	//There shouldn't be any, but one or two might hang around due to inaccuracies
	//or something.
	while(widx<write_idx_) {
		DoByte(writes_[widx].val);
		++widx;
	}
	last_render_at_=bbcSOUND_CYCLES();
	write_idx_=0;
}

void bbcSound::Render8BitMono(void **buffers,unsigned *buffer_sizes,unsigned num_buffers) {
	RenderSound<Render8BitTraits>(buffers,buffer_sizes,num_buffers,false);
}

void bbcSound::Render8BitStereo(void **buffers,unsigned *buffer_sizes,unsigned num_buffers) {
	RenderSound<Render8BitTraits>(buffers,buffer_sizes,num_buffers,true);
}

void bbcSound::Render16BitMono(void **buffers,unsigned *buffer_sizes,unsigned num_buffers) {
	RenderSound<Render16BitTraits>(buffers,buffer_sizes,num_buffers,false);
}

void bbcSound::Render16BitStereo(void **buffers,unsigned *buffer_sizes,unsigned num_buffers) {
	RenderSound<Render16BitTraits>(buffers,buffer_sizes,num_buffers,true);
}

