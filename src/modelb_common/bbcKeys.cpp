#include "pch.h"
#include "bbcKeys.h"
#include <string.h>

struct KeyAndName {
	bbcKey key_code;
	const char *key_name;
};

static const KeyAndName bbc_key_details[]={
	{bbcKEY_SPACE,"bbc_space"},
	{bbcKEY_COMMA,"bbc_comma"},
	{bbcKEY_MINUS,"bbc_minus"},
	{bbcKEY_STOP,"bbc_stop"},
	{bbcKEY_SLASH,"bbc_slash"},
	{bbcKEY_0,"bbc_0"},
	{bbcKEY_1,"bbc_1"},
	{bbcKEY_2,"bbc_2"},
	{bbcKEY_3,"bbc_3"},
	{bbcKEY_4,"bbc_4"},
	{bbcKEY_5,"bbc_5"},
	{bbcKEY_6,"bbc_6"},
	{bbcKEY_7,"bbc_7"},
	{bbcKEY_8,"bbc_8"},
	{bbcKEY_9,"bbc_9"},
	{bbcKEY_COLON,"bbc_colon"},
	{bbcKEY_SEMICOLON,"bbc_semicolon"},
	{bbcKEY_AT,"bbc_at"},
	{bbcKEY_A,"bbc_a"},
	{bbcKEY_B,"bbc_b"},
	{bbcKEY_C,"bbc_c"},
	{bbcKEY_D,"bbc_d"},
	{bbcKEY_E,"bbc_e"},
	{bbcKEY_F,"bbc_f"},
	{bbcKEY_G,"bbc_g"},
	{bbcKEY_H,"bbc_h"},
	{bbcKEY_I,"bbc_i"},
	{bbcKEY_J,"bbc_j"},
	{bbcKEY_K,"bbc_k"},
	{bbcKEY_L,"bbc_l"},
	{bbcKEY_M,"bbc_m"},
	{bbcKEY_N,"bbc_n"},
	{bbcKEY_O,"bbc_o"},
	{bbcKEY_P,"bbc_p"},
	{bbcKEY_Q,"bbc_q"},
	{bbcKEY_R,"bbc_r"},
	{bbcKEY_S,"bbc_s"},
	{bbcKEY_T,"bbc_t"},
	{bbcKEY_U,"bbc_u"},
	{bbcKEY_V,"bbc_v"},
	{bbcKEY_W,"bbc_w"},
	{bbcKEY_X,"bbc_x"},
	{bbcKEY_Y,"bbc_y"},
	{bbcKEY_Z,"bbc_z"},
	{bbcKEY_LSQBRACKET,"bbc_lsqbracket"},
	{bbcKEY_BACKSLASH,"bbc_backslash"},
	{bbcKEY_RSQBRACKET,"bbc_rsqbracket"},
	{bbcKEY_TILDE,"bbc_tilde"},
	{bbcKEY_UNDERLINE,"bbc_underline"},
	{bbcKEY_ESCAPE,"bbc_escape"},
	{bbcKEY_TAB,"bbc_tab"},
	{bbcKEY_CAPSLOCK,"bbc_capslock"},
	{bbcKEY_CTRL,"bbc_ctrl"},
	{bbcKEY_SHIFTLOCK,"bbc_shiftlock"},
	{bbcKEY_SHIFT,"bbc_shift"},
	{bbcKEY_DELETE,"bbc_delete"},
	{bbcKEY_COPY,"bbc_copy"},
	{bbcKEY_RETURN,"bbc_return"},
	{bbcKEY_UP,"bbc_up"},
	{bbcKEY_DOWN,"bbc_down"},
	{bbcKEY_LEFT,"bbc_left"},
	{bbcKEY_RIGHT,"bbc_right"},
	{bbcKEY_F0,"bbc_f0"},
	{bbcKEY_F1,"bbc_f1"},
	{bbcKEY_F2,"bbc_f2"},
	{bbcKEY_F3,"bbc_f3"},
	{bbcKEY_F4,"bbc_f4"},
	{bbcKEY_F5,"bbc_f5"},
	{bbcKEY_F6,"bbc_f6"},
	{bbcKEY_F7,"bbc_f7"},
	{bbcKEY_F8,"bbc_f8"},
	{bbcKEY_F9,"bbc_f9"},

	//Master keypad
	{bbcKEY_KP_4,"bbc_kp_4"},
	{bbcKEY_KP_5,"bbc_kp_5"},
	{bbcKEY_KP_6,"bbc_kp_6"},
	{bbcKEY_KP_0,"bbc_kp_0"},
	{bbcKEY_KP_1,"bbc_kp_1"},
	{bbcKEY_KP_2,"bbc_kp_2"},
	{bbcKEY_KP_HASH,"bbc_kp_hash"},
	{bbcKEY_KP_STAR,"bbc_kp_star"},
	{bbcKEY_KP_COMMA,"bbc_kp_comma"},
	{bbcKEY_KP_SLASH,"bbc_kp_slash"},
	{bbcKEY_KP_DEL,"bbc_kp_del"},
	{bbcKEY_KP_STOP,"bbc_kp_stop"},
	{bbcKEY_KP_PLUS,"bbc_kp_plus"},
	{bbcKEY_KP_MINUS,"bbc_kp_minus"},
	{bbcKEY_KP_RETURN,"bbc_kp_return"},
	{bbcKEY_KP_8,"bbc_kp_8"},
	{bbcKEY_KP_9,"bbc_kp_9"},
	{bbcKEY_KP_6,"bbc_kp_6"},
	{bbcKEY_KP_7,"bbc_kp_7"},

//	{bbcKEY_QUICKQUIT,"bbc_quickquit"},
//	{bbcKEY_REDRAW,"bbc_redraw"},
//	{bbcKEY_CONTROL,"bbc_control"},
	{bbcKEY_BREAK,"bbc_break"},
};
static const unsigned num_bbc_key_details=sizeof bbc_key_details/sizeof bbc_key_details[0];
//static int key_codes_table[num_bbc_key_details];
//static bool made_key_codes_table=false;

//////////////////////////////////////////////////////////////////////////
bbcKey bbcCodeFromKeyName(const char *name) {
	for(unsigned i=0;i<num_bbc_key_details;++i) {
		if(stricmp(bbc_key_details[i].key_name,name)==0) {
			return bbc_key_details[i].key_code;
		}
	}
	return bbcKEY_NONE;
}

const char *bbcKeyNameFromCode(bbcKey code) {
	for(unsigned i=0;i<num_bbc_key_details;++i) {
		if(bbc_key_details[i].key_code==code) {
			return bbc_key_details[i].key_name;
		}
	}
	return "";
}

//void bbcGetAllKeyCodes(int **key_codes,unsigned *num_key_codes) {
//	if(!made_key_codes_table) {
//		for(unsigned i=0;i<num_bbc_key_details;++i) {
//			key_codes_table[i]=bbc_key_details[i];
//		}
//		made_key_codes_table=true;
//	}
//
//	*key_codes=key_codes_table;
//	*num_key_codes=num_bbc_key_details;
//}
