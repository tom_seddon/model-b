#include "pch.h"
#include "bbc1770.h"
#include "bbc1770Interface.h"

bbcMmioFnInit MkMmioFnInit(t65::word start,unsigned count,
	bbcReadMmioFn read_fn,bbcWriteMmioFn write_fn)
{
	bbcMmioFnInit fn_init;

	fn_init.start=start;
	fn_init.count=count;
	fn_init.read_fn=read_fn;
	fn_init.write_fn=write_fn;
	return fn_init;
}
