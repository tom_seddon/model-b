#include "pch.h"
#include "bbc1770Watford.h"

// contrary to what beebem says:
//
//	Bit       Meaning
//	-----------------
//	7,6      Not used.
//	5 32     0
//	4 16     0
//	3 8      density select (0=double 1=single)
//	2 4      side select (0=0 1=1)
//	1 2      select drive 2
//	0 1      select drive 1

const bbc1770Interface bbc1770Watford::disc_interface={
	"Watford 1770",
	0xfe84,
	0xfe80,
	&bbc1770Watford::GetDriveControlFromByte,
	&bbc1770Watford::GetByteFromDriveControl,
	0,//init_fn
	bbc1770Interface::INVERT_TRACK0,//so says BeebEm!!
	0,//extra_mmio_fns
	{
		0,//save_state_handler.load_fn
		0,//save_state_handler.save_fn
	},
};

void bbc1770Watford::GetByteFromDriveControl(const bbc1770DriveControl &src,
	t65::byte *dest)
{
	*dest=0;
	*dest|=src.drive==0?1:2;
	if(src.side==1) {
		*dest|=4;
	}
	if(!src.is_double_density) {
		*dest|=8;
	}
}

void bbc1770Watford::GetDriveControlFromByte(t65::byte src,bbc1770DriveControl *dest)
{
	dest->drive=0;
	if(src&2) {
		dest->drive=1;
	}
	dest->side=src&4?1:0;
	dest->is_double_density=!(src&8);
	dest->disable_irq=false;
}
