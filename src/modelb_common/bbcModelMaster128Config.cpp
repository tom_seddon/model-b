#include "pch.h"
#include "bbcComputer.h"
//#include "bbcModelMaster128.h"
#include "bbcModelMaster128Config.h"

const bbcReadMmioFn bbcModelMaster128Config::romsel_read_fn=&bbcComputer::ReadRomSelMaster128;
const bbcWriteMmioFn bbcModelMaster128Config::romsel_write_fn=&bbcComputer::WriteRomSelMaster128;
