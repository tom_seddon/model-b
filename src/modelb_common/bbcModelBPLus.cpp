#include "pch.h"
#include "bbcModelBPlus.h"
#include "bbcCommonHardware.h"

const bbcModelBPlus bbcModelBPlus::model;

const bbcMmioFnInit *bbcModelBPlus::ExtraMmioFns() const {
	static const bbcMmioFnInit mmio_fns[]={
		{0xfe30,16,&bbcComputer::ReadRomSelBPlus,&bbcComputer::WriteRomSelBPlus},
		{0,0},
	};

	return mmio_fns;
}

#ifdef bbcUPDATE_IN_BBCMODEL
void bbcModelBPlus::Update() const {
	bbcVideo::Update();
	bbc_system_via.Update();
	bbc_user_via.Update();
	bbc1770::Update();
	bbcAdc::Update();
#ifdef bbcSERIAL_ENABLE
	bbcAciaUpdate();
#endif
}
#endif

