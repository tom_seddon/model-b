#include "pch.h"

#include "bbcSaveState.h"
//                                     0123456789abcdef
static const char save_state_hdr[]="modelb-savestate";
static const int check_save_state_hdr_size[sizeof(save_state_hdr)==17]={0};
static const int check_bbc_save_state_item_id_size[sizeof(bbcSaveStateItemId)==8]={0};

#pragma pack(push,1)

struct bbcSaveStateHdr {
	char hdr_text[16];
	bbcSaveStateDword total_size;
	bbcSaveStateDword first_item_offset;
};

static const int check_bbc_save_state_hdr_size[sizeof(bbcSaveStateHdr)==24]={0};

struct bbcSaveStateItemHdr {
	bbcSaveStateDword next_item_offset;//0
	bbcSaveStateItemId id;//4
	bbcSaveStateDword sub_type;//12
	bbcSaveStateDword version;//16
	bbcSaveStateDword data_size;//20
	bbcSaveStateDword data_offset;//24
};

static const int check_bbc_save_state_item_hdr_size[sizeof(bbcSaveStateItemHdr)==28]={0};

#pragma pack(pop)

//////////////////////////////////////////////////////////////////////////

bbcSaveStateItemId::bbcSaveStateItemId():
id(0)
{
}

bbcSaveStateItemId::bbcSaveStateItemId(const char *id_text_in) {
	BASSERT(strlen(id_text_in)==8);
	memcpy(this->id_text,id_text_in,8);
}
																				 
bbcSaveStateItemId::bbcSaveStateItemId(t65::qword id_in):
id(id_in)
{
	BASSERT(this->id_text[0]!=0&&this->id_text[1]!=0&&this->id_text[2]!=0&&this->id_text[3]!=0&&this->id_text[4]!=0&&
		this->id_text[5]!=0&&this->id_text[6]!=0&&this->id_text[7]!=0);
}

bool bbcSaveStateItemId::operator==(const bbcSaveStateItemId &rhs) const {
	return this->id==rhs.id;
}

//////////////////////////////////////////////////////////////////////////

const char *const bbcSaveState::load_status_texts_[LS_NUM]={
	"OK",//LS_OK=0,
	"Invalid save state version.",//LS_BAD_VERSION,
	"Missing save state data.",//LS_MISSING,
	"Bad save state data.",//LS_BAD_DATA,
	"Not implemented.",//LS_NOT_IMPLEMENTED,
};

const char *const bbcSaveState::save_status_texts_[SS_NUM]={
	"OK",//SS_OK=0,
	"Not implemented.",//SS_NOT_IMPLEMENTED,
};

bbcSaveState::bbcSaveState() {
}

bbcSaveState::~bbcSaveState() {
	this->Reset();
}

const char *bbcSaveState::GetLoadStatusText(LoadStatus s) {
	return load_status_texts_[s];
}

const char *bbcSaveState::GetSaveStatusText(SaveStatus s) {
	return save_status_texts_[s];
}

void bbcSaveState::Reset() {
	for(unsigned i=0;i<items_.size();++i) {
		delete items_[i];
	}
	items_.clear();
}

bool bbcSaveState::Load(const std::vector<t65::byte> &contents) {
	if(contents.size()<sizeof(bbcSaveStateHdr)) {
		return false;
	}

	const bbcSaveStateHdr *hdr=reinterpret_cast<const bbcSaveStateHdr *>(&contents[0]);
	if(memcmp(hdr->hdr_text,save_state_hdr,16)!=0) {
		return false;
	} else if(contents.size()!=hdr->total_size) {
		return false;
	}

	this->Reset();
	const bbcSaveStateItemHdr *item=0;
	for(t65::dword offs=hdr->first_item_offset;offs!=0;offs=item->next_item_offset) {
		item=reinterpret_cast<const bbcSaveStateItemHdr *>(&contents[offs]);
		if(item->next_item_offset>=contents.size()||item->data_offset>=contents.size()) {
			this->Reset();
			return false;
		}

		bbcSaveStateItem *new_item=new bbcSaveStateItem;
		new_item->id=item->id;
		new_item->sub_type=item->sub_type;
		new_item->version=item->version;
		new_item->data.resize(item->data_size);
		memcpy(&new_item->data[0],&contents[item->data_offset],item->data_size);

		items_.push_back(new_item);
	}

	return true;
}

void bbcSaveState::Save(std::vector<t65::byte> *contents) const {
	contents->clear();

	unsigned total_size=sizeof(bbcSaveStateHdr);

	std::vector<int> offsets;
	for(int i=0;i<this->GetNumItems();++i) {
		offsets.push_back(total_size);
		total_size+=sizeof(bbcSaveStateItemHdr);
		total_size+=this->GetItem(i)->data.size();
	}

	contents->resize(0);
	contents->resize(total_size,0);

	bbcSaveStateHdr *hdr=reinterpret_cast<bbcSaveStateHdr *>(&contents->at(0));
	memcpy(hdr->hdr_text,save_state_hdr,16);
	hdr->total_size=total_size;
	hdr->first_item_offset=0;

	t65::dword offset=sizeof(bbcSaveStateHdr);
	t65::dword *last_offset_ptr=&hdr->first_item_offset;
	for(int i=0;i<this->GetNumItems();++i) {
		const bbcSaveStateItem *item=this->GetItem(i);
		bbcSaveStateItemHdr *new_item_hdr=reinterpret_cast<bbcSaveStateItemHdr *>(&contents->at(offset));

		*last_offset_ptr=offset;

		new_item_hdr->next_item_offset=0;
		new_item_hdr->id=item->id;
		new_item_hdr->sub_type=item->sub_type;
		new_item_hdr->version=item->version;
		new_item_hdr->data_size=item->data.size();
		new_item_hdr->data_offset=offset+sizeof(bbcSaveStateItemHdr);

		BASSERT(offset+item->data.size()<=total_size);
		memcpy(new_item_hdr+1,&item->data[0],item->data.size());

		last_offset_ptr=&new_item_hdr->next_item_offset;
		offset+=sizeof(bbcSaveStateItemHdr)+item->data.size();
	}
}

int bbcSaveState::GetNumItems() const {
	return int(items_.size());
}

bbcSaveStateItem *bbcSaveState::GetItem(int idx) {
	if(idx<0||idx>=int(items_.size())) {
		return 0;
	} else {
		return items_[idx];
	}
}

const bbcSaveStateItem *bbcSaveState::GetItem(int idx) const {
	if(idx<0||idx>=int(items_.size())) {
		return 0;
	} else {
		return items_[idx];
	}
}

int bbcSaveState::GetItemIdx(const bbcSaveStateItemId &id,t65::dword sub_type) const {
	for(int i=0;i<int(items_.size());++i) {
		bbcSaveStateItem *item=items_[i];

		if(item->id==id&&item->sub_type==sub_type) {
			return i;
		}
	}

	return -1;
}

void bbcSaveState::SetItem(const bbcSaveStateItemId &id,t65::dword sub_type,t65::dword version,const void *data,
	unsigned data_size)
{
	void *p=this->PrepareItem(id,sub_type,version,data_size);
	memcpy(p,data,data_size);
}

void *bbcSaveState::PrepareItem(const bbcSaveStateItemId &id,t65::dword sub_type,t65::dword version,unsigned data_size) {
	int idx=this->GetItemIdx(id,sub_type);
	bbcSaveStateItem *item;
	if(idx>=0) {
		item=items_[idx];
	} else {
		item=new bbcSaveStateItem;
		items_.push_back(item);
	}

	item->id=id;
	item->sub_type=sub_type;
	item->version=version;
	item->data.clear();
	item->data.resize(data_size,0);

	return &item->data[0];
}

const void *bbcSaveState::FindItemInternal(const bbcSaveStateItemId &id,t65::dword sub_type,t65::dword version,size_t size,
	LoadStatus *status) const
{
	const bbcSaveStateItem *item=this->GetItem(id,sub_type);
	if(!item) {
		*status=LS_MISSING;
		return 0;
	}

	if(item->version!=version) {
		*status=LS_BAD_VERSION;
		return 0;
	}

	if(item->data.size()!=size) {
		*status=LS_BAD_DATA;
		return 0;
	}

	return &item->data[0];
}
