#include "pch.h"
#include "bbc1770Acorn.h"

// References:
//
// [a]	"WD1770/2/3 data (disk controller chip)"
//		http://www.cloud9.co.uk/james/BBCMicro/Documentation/wd1770.html
// [b]	BBC Mess driver
//		http://mamerominfo.retrogames.com/messbbc/
//		Source from www.mess.org/
//

//	Bit       Meaning
//	-----------------
//	7,6      Not used.
//	5        Reset drive controller chip.
//	4        Interrupt ? [a]
//	         Enable 1770 interrupts (0=enable, 1=disable) [b]
//	3        Double density select (0 = double, 1 = single).
//	2        Side select (0 = side 0, 1 = side 1).
//	1        Drive select 1.
//	0        Drive select 0.
//
// TODO bit 4 is not emulated at all yet...

const bbc1770Interface bbc1770Acorn::disc_interface={
	"Acorn 1770",
	0xfe84,
	0xfe80,
	&bbc1770Acorn::GetDriveControlFromByte,
	&bbc1770Acorn::GetByteFromDriveControl,
	0,//init_fn
	0,//disable_restore_iq
	0,//extra_mmio_fns
};

void bbc1770Acorn::GetByteFromDriveControl(const bbc1770DriveControl &src,
	t65::byte *dest)
{
	*dest=0;
	if(src.drive==0) {
		*dest|=1;
	} else if(src.drive==1) {
		*dest|=2;
	}
	if(src.side==1) {
		*dest|=4;
	}
	if(!src.is_double_density) {
		*dest|=8;
	}
	if(src.disable_irq) {
		*dest|=16;
	}
}

void bbc1770Acorn::GetDriveControlFromByte(t65::byte src,bbc1770DriveControl *dest) {
	dest->drive=0;
	if(src&1) {
		dest->drive=0;
	} else if(src&2) {
		dest->drive=1;
	}
	dest->side=src&4?1:0;
	dest->is_double_density=!(src&8);
	dest->disable_irq=!!(src&16);
}
