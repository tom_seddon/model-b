#include "pch.h"
#include "bbc1770Opus.h"

const bbc1770Interface bbc1770Opus::disc_interface={
	"Opus 1770",//name
	0xfe80,//fdc base
	0xfe84,//ctrl reg
	&bbc1770Opus::GetDriveControlFromByte,
	&bbc1770Opus::GetByteFromDriveControl,
	0,//init_fn
	0,//disable_restore_irq
	0,//extra_mmio_fns
	{
		0,//save_state_handler.load_fn
		0,//save_state_handler.save_fn
	},
};

//	Bit       Meaning
//	-----------------
//	7         Not used.
//	6         Density select (0 = single, 1 = double).
//	5-2       Not used.
//	1         Side select (0 = side 0, 1 = side 1).
//	0         Drive select (0 = drive 0, 1 = drive 1).
void bbc1770Opus::GetByteFromDriveControl(const bbc1770DriveControl &src,
	t65::byte *dest)
{
	*dest=0;
	if(src.drive==1) {
		*dest|=1;
	}
	if(src.side==1) {
		*dest|=2;
	}
	if(src.is_double_density) {
		*dest|=64;
	}
}

void bbc1770Opus::GetDriveControlFromByte(t65::byte src,bbc1770DriveControl *dest) {
	dest->disable_irq=false;
	dest->drive=src&1?1:0;
	dest->side=src&2?1:0;
	dest->is_double_density=!!(src&64);
}
/*
//////////////////////////////////////////////////////////////////////////
//
//	Opus double density board on BBC:
//
t65::byte Beeb1770::ReadDriveCtrlOpus(t65::byte offset) {
	t65::byte val=0;
	
	if(dd_mode_) {
		val|=0x40;
	}
	if(phys_side_==1) {
		val|=2;
	}
	if(phys_drive_==1) {
		val|=1;
	}
	
	return val;
}

void Beeb1770::WriteDriveCtrlOpus(t65::byte offset,t65::byte val) {
	LOG(fprintf(log_h,"1770: WriteDriveCtrlOpus: val=0x%02X,%d\n",val,val));
	phys_side_=(val&2)?1:0;
	phys_drive_=(val&1)?1:0;
	dd_mode_=!!(val&0x40);
	LOG(fprintf(log_h,"1770: WriteDriveCtrlOpus: physside=%d physdrive=%d dd_mode=%d\n",
		phys_side_,phys_drive_,dd_mode_));
}
*/
