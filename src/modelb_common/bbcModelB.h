#ifndef bbcMODELB_H_
#define bbcMODELB_H_

//
//bbcModelB is one of the Beeb models
//
//each model "X" has 4 classes:
//
//bbcXConfig
//bbcXCpu
//bbcXInstructionSet
//bbcXSim
//bbcX -- derived from bbcModel, just has functions.

//The ram_ memory map is as follows:
//
//	+0x00000	BBC RAM
//	+0x08000	Unused
//	+0x20000	END

#include "bbcSY6502A.h"
#include "bbc65C12.h"
#include "bbcModelGeneric.h"
#include "bbcComputer.h"
//#include "bbcModelBConfig.h"

struct bbcModelBConfig {
	typedef bbcComputer MachineType;

	//////////////////////////////////////////////////////////////////////////
	// Memory access -- which form of memory access should be used for each
	// access category?

	// ReadZP: how the processor reads bytes from zero page
	typedef MachineType::ReadNoHW ReadZP;

	// WriteZP: how the processor writes bytes to zero page
	typedef MachineType::WriteNoHW WriteZP;

	// ReadStack: how the processor reads bytes from the stack
	typedef MachineType::ReadNoHW ReadStack;

	// WriteStack: how the processor write bytes to the stack
	typedef MachineType::WriteNoHW WriteStack;

	// FetchInstr: how the processor reads bytes whilst fetching the bytes
	// that make up an instruction
	typedef MachineType::ReadExecute FetchInstr;

	// FetchAddress: how the processor reads an indirect address from
	// main memory, e.g. during IRQ processing or when doing the indirect
	// memory mode.
	typedef MachineType::ReadNoHW FetchAddress;

	// ReadOperand: how the processor reads bytes when reading an
	// instruction's operand
	typedef MachineType::ReadWithHW ReadOperand;

	// WriteOperand: how the processor writes bytes when writing an
	// instruction's operand
	typedef MachineType::WriteWithHW WriteOperand;

	// ReadDebug: how to read a byte in debug mode (no update hardware
	// or signal interrupts etc.)
	typedef MachineType::ReadNoHW ReadDebug;

	// WriteDebug: how to write a byte in debug mode (no update hardware
	// or signal interrupts etc.)
	typedef MachineType::WriteNoHW WriteDebug;

//	static const bbcReadMmioFn romsel_read_fn;
//	static const bbcWriteMmioFn romsel_write_fn;
};

typedef bbcSY6502A<bbcModelBConfig> bbcModelBCpu;
typedef bbcSY6502AInstructionSet<bbcModelBCpu> bbcModelBInstructionSet;
typedef t65::Sim6502<bbcModelBCpu,bbcModelBInstructionSet> bbcModelBSim;

typedef bbc65C12<bbcModelBConfig> bbcModelB65C12Cpu;
typedef bbc65C12InstructionSet<bbcModelB65C12Cpu> bbcModelB65C12InstructionSet;
typedef t65::Sim6502<bbcModelB65C12Cpu,bbcModelB65C12InstructionSet> bbcModelB65C12Sim;

//typedef bbcModelGeneric<bbcModelBSim,0> bbcModelB;

class bbcModelB:
public bbcModelGeneric<bbcModelBSim>
{
public:
#ifdef bbcUPDATE_IN_BBCMODEL
	void Update() const;
#endif
	const bbcMmioFnInit *ExtraMmioFns() const;
	const bbcSaveStateHandler *GetSaveStateHandlers() const;

	static const bbcModelB model;
};

class bbcModelB65C12:
public bbcModelGeneric<bbcModelB65C12Sim>
{
public:
#ifdef bbcUPDATE_IN_BBCMODEL
	void Update() const;
#endif
	const bbcMmioFnInit *ExtraMmioFns() const;

	static const bbcModelB65C12 model;
};

#endif
