#include "pch.h"
#include "bbcUef.h"
#include <stdarg.h>
#include <algorithm>
#include <functional>

static const char uef_id[]="UEF File!";//10 bytes

static t65::word WordAt(const std::vector<t65::byte> &data,unsigned index) {
	return data[index]|(data[index+1]<<8);
}

static t65::word DwordAt(const std::vector<t65::byte> &data,unsigned index) {
	return WordAt(data,index)|(WordAt(data,index+2)<<16);
}

void bbcUefChunk::AppendData(std::vector<t65::byte> &contents) const {
	contents.push_back(this->id&0xFF);
	contents.push_back(this->id>>8);

	for(unsigned i=0;i<4;++i) {
		contents.push_back((t65::byte)(this->data.size()>>(i*8)));
	}

	std::copy(this->data.begin(),this->data.end(),std::back_inserter(contents));
}


bbcUef::bbcUef(t65::byte vmajor,t65::byte vminor):
ok_(true),
vmajor_(vmajor),
vminor_(vminor)
{
}

bbcUef::bbcUef(const std::vector<t65::byte> &contents):
vmajor_(0xFF),
vminor_(0xFF)
{
	this->ReadData(contents);
}

bool bbcUef::ReadData(const std::vector<t65::byte> &contents) {
	if(contents.size()>=18) {// +12 header +2 id +4 len
		if(memcmp(&contents[0],uef_id,10)==0) {
			vminor_=contents[10];
			vmajor_=contents[11];

			ok_=true;
			t65::dword i=12;
			while(i<contents.size()) {
				bbcUefChunk *chunk=&*chunks_.insert(chunks_.end(),bbcUefChunk());

				chunk->id=WordAt(contents,i);

				t65::dword len=DwordAt(contents,i+2);

				i+=6;
				for(t65::dword j=0;j<len;++j) {
					if(i+j>=contents.size()) {
						//This will result in a fucked up uef object, but anything that doesn't
						//check this->Ok() shouldn't cock up too badly. viva std::vector!
						ok_=false;
						break;
					} else {
						chunk->data.push_back(contents[i+j]);
					}
				}

				i+=len;
			}
		}
	}

	bbcPrintf("%s: UEF file, %u chunks as follows:\n",__FUNCTION__,this->NumChunks());
	for(unsigned i=0;i<this->NumChunks();++i) {
		bbcPrintf("%s: chunk %u:\n",__FUNCTION__,i);
		bbcPrintf("%s:     id 0x%04X, %u bytes\n",__FUNCTION__,this->Chunks()[i].id,this->Chunks()[i].data.size());
	}

	return ok_;
}

void bbcUef::GetData(std::vector<t65::byte> &contents) const {
	contents.resize(12);
	memcpy(&contents[0],uef_id,10);
	contents[10]=vminor_;
	contents[11]=vmajor_;

	for(C::const_iterator chunk=chunks_.begin();chunk!=chunks_.end();++chunk) {
		chunk->AppendData(contents);
	}

}


unsigned bbcUef::NumChunks() const {
	return chunks_.size();
}

const bbcUefChunk *bbcUef::Chunks() const {
	BASSERT(this->NumChunks()>0);
	return &chunks_[0];
}

bool bbcUef::Ok() const {
	return ok_;
}
