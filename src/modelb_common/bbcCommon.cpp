#include "pch.h"
#include <stdarg.h>
#include <stdio.h>
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#define STRICT
#include <windows.h>
#endif
#include "bbcComputer.h"

#ifdef bbcDEBUG_PRINTF

//static bool first_time=true;

static void Out(const char *str) {
#ifdef _WIN32
	OutputDebugStringA(str);
#endif
	printf("%s",str);

//	FILE *h=fopen(".\\bbcCommon.txt",first_time?"wt":"a+t");
//	first_time=false;
//	if(h) {
//		fprintf(h,"%s",str);
//		fclose(h);
//	}
}

int bbcPrintf(const char *fmt,...) {
	va_list v;

	va_start(v,fmt);
	char tmpbuf[1024];
	int n=_vsnprintf(tmpbuf,sizeof tmpbuf,fmt,v);
	va_end(v);

	char *line=strtok(tmpbuf,"\n");
	bool first_line=true;
	char prefix[100];
	_snprintf(prefix,sizeof prefix-1,"@%04X #%-12d: ",bbcComputer::cpustate.pc.w,bbcComputer::Cycles2MHz());
	while(line) {
		Out(first_line?prefix:"                     ");//21 spaces (oh dear)
		Out(line);
		Out("\n");
		line=strtok(0,"\n");
	}

	return n;
}
#endif

void BAssertMsg(const char *file,int line,const char *what) {
	char msg[1024];

	_snprintf(msg,sizeof msg,"%s(%d): BASSERT failure:\n    %s\n",file,line,what);
	
#ifdef _WIN32
	// TODO MessageBox?
	OutputDebugStringA(msg);
#endif
	fputs(msg,stderr);
}

