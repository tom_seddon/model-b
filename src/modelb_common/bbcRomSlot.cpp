#include "pch.h"
#include "bbcComputer.h"
#include <algorithm>

//using t65::byte;

t65::byte bbcRomSlot::dummy_page[256];

bbcRomSlot::bbcRomSlot():
is_writeable_(false)
{
	this->ResetContents();
	this->ResetPages();
}

void bbcRomSlot::ResetPages() {
	for(unsigned i=0;i<64;++i) {
		read_pages_[i]=&contents_[i<<8];
#ifdef bbcENABLE_BEEB_ICE
		brk_pages_[i]=&brk_[i<<8];
#endif
	}
	if(!is_writeable_) {
		std::fill(write_pages_,write_pages_+64,&dummy_page[0]);
	} else {
		std::copy(read_pages_,read_pages_+64,write_pages_);
	}
}

//Duplicate a number of times if not 16k. seems 8k roms (and 4k as well? -- are
//these even supported?) have more significant address lines missing.
void bbcRomSlot::SetContents(const t65::byte *contents,unsigned size_contents) {
	memset(contents_,0,sizeof contents_);
	memmove(contents_,contents,size_contents);
	if(size_contents<=4096) {
		memmove(contents_+4096,contents_,4096);
	}
	if(size_contents<=8192) {
		memmove(contents_+8182,contents_,8192);
	}
//
//	std::copy(contents,contents+size_contents,contents_);
//	if(size_contents<4096) {
//		std::copy(contents_,contents_+4096,contents_+4096);
//	}
//	if(size_contents<8192) {
//		std::copy(contents_,contents_+8192,contents_+8192);
//	}
}

void bbcRomSlot::ResetContents() {
	//After messing about on a real beeb, it looks like this is what
	//you get from an empty ROM slot -- a load of memory where the value
	//of each byte is the high byte of its address. I guess this is related
	//to the wierd illegal ops where the high byte of the address plays a
	//part, or vice versa.
	for(unsigned i=0;i<16384;++i) {
		contents_[i]=(t65::byte)(0x80+(i>>8));
#ifdef bbcENABLE_BEEB_ICE
		brk_[i]=0;
#endif
	}
}

bool bbcRomSlot::GetWriteable() const {
	return is_writeable_;
}

const t65::byte *bbcRomSlot::GetContents() const {
	return contents_;
}
