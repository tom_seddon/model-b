#ifndef USERVIA_H_
#define USERVIA_H_

#include "bbcVIA.h"
#include "bbcSaveState.h"

//#define bbcUSER_VIA_PB_RANDOMNESS

// note: no extra save state for user via (yet!)

class bbcUserVIA:
public bbcVIA
{
public:
	bbcUserVIA();

#ifdef bbcUSER_VIA_PB_RANDOMNESS
	void ReadingPb();
#endif

	static t65::byte MmioRead(t65::word addr);
	static void MmioWrite(t65::word addr,t65::byte val);

protected:
private:
};

extern bbcUserVIA bbc_user_via;

bbcSaveState::LoadStatus bbcLoadUserVIASaveState(const bbcSaveState *save_state);
bbcSaveState::SaveStatus bbcSaveUserVIASaveState(bbcSaveState *save_state);

#endif
