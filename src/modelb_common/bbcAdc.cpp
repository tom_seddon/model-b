#include "pch.h"
#include "bbcAdc.h"
#include "bbcSystemVIA.h"
#include "bbcComputer.h"

int bbcAdc::next_stop_at_;
bool bbcAdc::busy_;
bool bbcAdc::is_8bit_;
int bbcAdc::input_channel_;
t65::word bbcAdc::channels_[4];
t65::Word bbcAdc::last_conversion_;
bool bbcAdc::conversion_complete_;

void bbcAdc::Init() {
	bbcAdc::Reset();
}

void bbcAdc::Reset() {
	unsigned i;
	
	busy_=true;
	is_8bit_=false;
	input_channel_=0;
	last_conversion_.w=0;
	conversion_complete_=false;
	for(i=0;i<4;++i) {
		channels_[i]=0x8000;
	}
}

void bbcAdc::Update() {
	if(busy_) {
		const int cycles=bbcComputer::Cycles2MHz();//11/12/2004 -- 
		if(cycles-next_stop_at_<0) {//bbcComputer::cycles<next_stop_at_) {
			bbcComputer::SetNextStop(next_stop_at_);
			return;
		} else {
			busy_=false;
			conversion_complete_=true;
			last_conversion_.w=channels_[input_channel_]&0xFFF0;
			bbc_system_via.CB1(false);
		}
	}
}

t65::byte bbcAdc::Read(t65::word addr) {
	t65::byte v=0;

	switch(addr&3) {
	case 0:
		//Status
		v=input_channel_&3;
		if(is_8bit_) {
			v|=8;
		}
		if(last_conversion_.w&0x8000) {
			v|=32;
		}
		if(last_conversion_.w&0x4000) {
			v|=16;
		}
		if(!busy_) {
			v|=64;
		}
		if(!conversion_complete_) {
			v|=128;
		}
		//busy_=false;//TODO is this correct?
		return v;
	case 1:
		//MSB of conversion
		//busy_=false;
		return last_conversion_.h;
	case 2:
		//LSB of conversion
		//busy_=false;
		return last_conversion_.l;
	}
	return 0;
}

void bbcAdc::Write(t65::word addr,t65::byte val) {
	if((addr&3)==0) {
		input_channel_=val&3;
		is_8bit_=!!(val&8);
		busy_=true;
		conversion_complete_=false;
		//AUG p429, 8bit takes 4ms (8000 cycles) and 12bit 10ms (20000 cycles)
		next_stop_at_=bbcComputer::Cycles2MHz()+(is_8bit_?8000:20000);
		bbcComputer::SetNextStop(next_stop_at_);
	}
}

#ifdef bbcDEBUG_PANELS
void bbcAdc::InitDebugPanel(bbcDebugPanel *panel) {
	panel->SetSize(20,6);
}

void bbcAdc::UpdateDebugPanel(bbcDebugPanel *panel) {
	for(int i=0;i<4;++i) {
		panel->Print(0,i,"ch%d=%f",i,channels_[i]);
	}
	panel->Print(0,4,"input=%d",input_channel_);
	panel->Print(0,5,"last_conversion=&%04X",last_conversion_);
}
#endif

//bbc joysticks are the wrong way round compared to PC ones...

static inline t65::word GetAdcWord(float adc_float) {
	float f=.5f-adc_float*.5f;

	if(f<0.f) {
		return 0;
	} else if(f>1.f) {
		return 65535;
	} else {
		return t65::word(f*65535.f);
	}
}

void bbcAdc::SetJoystick(int joystick,float x,float y) {
	BASSERT(joystick==0||joystick==1);
	
	channels_[joystick*2+0]=GetAdcWord(x);
	channels_[joystick*2+1]=GetAdcWord(y);
}


//	static int next_stop_at_;
//	static bool busy_;
//	static bool is_8bit_;
//	static int input_channel_;
//	static float channels_[4];
//	static t65::Word last_conversion_;
//	static bool conversion_complete_;

static const bbcSaveStateItemId bbc_adc_save_state_item_id("bbc_adc_");

#pragma pack(push,1)
struct bbcAdcSaveState1 {
	bbcSaveStateInt32 next_stop_at;
	bbcSaveStateByte is_busy;
	bbcSaveStateByte is_8bit;
	bbcSaveStateInt32 input_channel;
	bbcSaveStateWord channels[4];
	bbcSaveStateWord last_conversion;
	bbcSaveStateByte is_conversion_complete;
};
#pragma pack(pop)

bbcSaveState::LoadStatus bbcAdc::LoadSaveState(const bbcSaveState *save_state) {
	const bbcSaveStateItem *item=save_state->GetItem(bbc_adc_save_state_item_id,0);
	if(!item) {
		return bbcSaveState::LS_MISSING;
	}

	switch(item->version) {
	default:
		return bbcSaveState::LS_BAD_VERSION;

	case 1:
		if(item->data.size()!=sizeof(bbcAdcSaveState1)) {
			return bbcSaveState::LS_BAD_DATA;
		} else {
			const bbcAdcSaveState1 *data=reinterpret_cast<const bbcAdcSaveState1 *>(&item->data[0]);

			next_stop_at_=data->next_stop_at;
			busy_=!!data->is_busy;
			is_8bit_=!!data->is_8bit;
			input_channel_=data->input_channel;
			channels_[0]=data->channels[0];
			channels_[1]=data->channels[1];
			channels_[2]=data->channels[2];
			channels_[3]=data->channels[3];
			last_conversion_.w=data->last_conversion;
			conversion_complete_=!!data->is_conversion_complete;
		}
		break;
	}

	return bbcSaveState::LS_OK;
}

bbcSaveState::SaveStatus bbcAdc::SaveSaveState(bbcSaveState *save_state) {
	bbcAdcSaveState1 data;

	data.next_stop_at=next_stop_at_;
	data.is_busy=busy_;
	data.is_8bit=is_8bit_;
	data.input_channel=input_channel_;
	data.channels[0]=channels_[0];
	data.channels[1]=channels_[1];
	data.channels[2]=channels_[2];
	data.channels[3]=channels_[3];
	data.last_conversion=last_conversion_.w;
	data.is_conversion_complete=conversion_complete_;

	save_state->SetItem(bbc_adc_save_state_item_id,0,1,&data);

	return bbcSaveState::SS_OK;
}
