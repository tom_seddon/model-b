#ifndef BBCMODELMASTER128_H_
#define BBCMODELMASTER128_H_

//Master 128 support
//
//The ram_ memory map is as follows:
//
//	ram_ delta	Name		What				Where in CPU memory map
//												start	end (incl)
//	+0x00000	BBC RAM		normal memory		0000	7FFF
//	+0x08000	ANDY		exploded chars?		8000	8FFF
//	+0x09000	HAZEL		filing system RAM	C000	DFFF
//	+0x0B000	LYNNE		Shadow video RAM	3000	7FFF
//	+0x10000	END
//
//Some notes:
//
//1.	Master 128 still has f**king B+ style shadow RAM (aargh)
//2.	FE30 is ROMSEL:		A--- -RRR
//			RRR controls paged ROM
//			A if set pages in ANDY
//3.	FE34 is ACCCON
//			Low nybble is YXED:
//				D	CRT source
//					0	BBC RAM
//					1	LYNNE
//				E	B+-style shadow RAM
//					0	CPU accesses in 3000-7FFF go to BBC RAM
//					1	CPU accesses in 3000-7FFF go to LYNNE if opcode in C000-DFFF.
//				X	CPU accesses to LYNNE
//					0	CPU accesses in 3000-7FFF go to BBC RAM
//					1	CPU accesses in 3000-7FFF go to LYNNE
//				Y	page in HAZEL if set
//			High nybble is [IRR][TST][IFJ][ITU]
//				IRR	IRQ request -- ?
//					"When set, this bit causes an open drain output to
//					pull the CPU NlRQ pin down to Vs"... ?!
//				TST	?
//				IFJ cortridge access
//					0	fc00-fdff is cartridge
//					1	fc00-fdff is 1MHz bus
//				ITU copro control
//					0	internal
//					1	external
//

#include "bbc65C12.h"
#include "bbcModelGeneric.h"
//#include "bbcComputer.h"
#include "bbcModelMaster128Config.h"

typedef bbc65C12<bbcModelMaster128Config> bbcModelMaster128Cpu;
typedef bbc65C12InstructionSet<bbcModelMaster128Cpu> bbcModelMaster128InstructionSet;
typedef t65::Sim6502<bbcModelMaster128Cpu,bbcModelMaster128InstructionSet> bbcModelMaster128Sim;

//typedef bbcModelGeneric<bbcModelMaster128Sim,bbcMODEL_HAS_CMOS|bbcMODEL_NO_KEYBOARD_LINKS> bbcModelMaster128;

class bbcModelMaster128:
public bbcModelGeneric<bbcModelMaster128Sim>
{
public:
	bbcModelMaster128();
#ifdef bbcUPDATE_IN_BBCMODEL
	void Update() const;
#endif
	const bbcMmioFnInit *ExtraMmioFns() const;

	static const bbcModelMaster128 model;
};


#endif
