#ifndef bbcFDD_H_
#define bbcFDD_H_

//#include <vector>

#include "bbcSaveState.h"

//If set, stuff for beebm_wx (pre python) will be available.
#define bbcFDD_ENABLE_NOPYTHON

//////////////////////////////////////////////////////////////////////////
// Sector on a disc
struct bbcDiscSector {
	std::vector<t65::byte> data;
};

//////////////////////////////////////////////////////////////////////////
// Track on a disc
struct bbcDiscTrack {
	std::vector<bbcDiscSector> sectors;
};

//////////////////////////////////////////////////////////////////////////
// Side on a disc
struct bbcDiscSide {
#ifdef bbcFDD_ENABLE_NOPYTHON
	bool is_side2;
#endif
	bool is_dd;
	bool changed;
	std::vector<bbcDiscTrack> tracks;
	
	bbcDiscSide();
};

//////////////////////////////////////////////////////////////////////////
// floppy disc drive
//
//accepts 2 types of addressing
//
//beeb addressing -- *DRIVE format like so:
//
//	0 - fdc drive 0 side 0
//	1 - fdc drive 1 side 0
//	2 - fdc drive 0 side 1
//	3 - fdc drive 1 side 0
//
//	Beeb adressing used for UI and management of drives
//	Drive 6/7 too if system supports 3 drives. (drive 4/5 kept free for challenger.)
//
//FDC addressing -- drive,side
//
//	Used when FDC is reading individual bytes
//	drivefromfdcaddress.
struct bbcFdd {
	enum DriveStatusType {
		DRIVE_EMPTY,
		DRIVE_LOADED,
		DRIVE_CHANGED,
		DRIVE_SIDE2,
	};

	enum DiscDensity {
		DENSITY_SINGLE,
		DENSITY_DOUBLE,
		DENSITY_ANY,
	};
	
	//Init (one time)
	static void Init();
	
	static void Shutdown();

	//Load
	static bool LoadDsDiscImage(int drive,int sectors_per_track,DiscDensity density,
		const std::vector<t65::byte> &disc_image);
	static bool LoadSsDiscImage(int drive,int sectors_per_track,DiscDensity density,
		const std::vector<t65::byte> &disc_image);
	
	//Get images
	//This will do some twiddling of the data, but there's no UI for
	//this yet.
	static bool GetSsDiscImage(int drive,std::vector<t65::byte> *disc_image);
	static bool GetDsDiscImage(int top,int bot,std::vector<t65::byte> *disc_image);
	
	//Whether the given drive is dirty
	static DriveStatusType DriveStatus(int drive);
	
	//Unloads disc image from drive. Doesn't save it.
	static void UnloadDiscImage(int drive);
	
	//marks the given drive as altered/unaltered
	static void SetDriveChangedStatus(int drive,bool changed);
	
	//Number of drives available
	enum {
		num_drives=4,
	};
	
	//What's in each drive (each drive has 1 side)
	static bbcDiscSide *drives[num_drives];
	
	//Given fdc address return drive.
	static int DriveFromFdcAddress(int fdc_drive,int fdc_side);
	
	//Get FDC byte
	static bool GetByteFdc(int fdc_drive,int fdc_side,int fdc_track,int fdc_sector,
		int fdc_offset,DiscDensity density,t65::byte *result);
	static bool SetByteFdc(int fdc_drive,int fdc_side,int fdc_track,int fdc_sector,
		int fdc_offset,DiscDensity density,t65::byte value);
	static bool IsOkAddressFdc(int fdc_drive,int fdc_side,int fdc_track,
		int fdc_sector,DiscDensity density);
	
	//is that drive double density
	static bool IsDd(int drive);

	//is that drive double sided
	static bool IsDs(int drive);

	//Retrieve number of sectors per track
	//returns false if that track is not valid.
//	static bool GetSectorsPerTrack(int fdc_drive,int fdc_side,int fdc_track,int *num_sectors);

	//Set drive contents directly (python)
	static void SetDriveContents(int drive,const bbcDiscSide *contents);

	static bbcSaveState::LoadStatus LoadSaveState(const bbcSaveState *save_state);
	static bbcSaveState::SaveStatus SaveSaveState(bbcSaveState *save_state);
private:
	static bool LoadDiscSide(int drive,int sectors_per_track,unsigned start,
		unsigned track_size,DiscDensity density,const std::vector<t65::byte> &side_image);
	static t65::byte *FdcBytePtr(int fdc_drive,int fdc_side,int fdc_track,
		int fdc_sector,int fdc_offset,DiscDensity density);

#ifdef bbcFDD_ENABLE_NOPYTHON
public://must be public for pre python code
	//if i in range(num_drives) other_side_of[i] is drive for other side of
	//drive i. The smaller number is the top one.
	static const int other_side_of[num_drives];
#endif
};

#endif
