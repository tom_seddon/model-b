#include "pch.h"
#include "bbcModelBWatford.h"
#include "bbcCommonHardware.h"

const bbcReadMmioFn bbcModelBWatfordConfig::romsel_read_fn=&bbcComputer::ReadRomSelBWatford;//only present to satisfy the bbcModelGeneric template
const bbcWriteMmioFn bbcModelBWatfordConfig::romsel_write_fn=&bbcComputer::WriteRomSelBWatford;//only present to satisfy the bbcModelGeneric template

const bbcModelBWatford bbcModelBWatford::model;

const bbcMmioFnInit *bbcModelBWatford::ExtraMmioFns() const {
	static const bbcMmioFnInit mmio_fns_[]={
		{0xfe30,16,&bbcComputer::ReadRomSelBWatford,&bbcComputer::WriteRomSelBWatford},
		{0xff30,16,0,&bbcComputer::WriteWatfordRomBoard},
		{0,0},
	};

	return mmio_fns_;
}

#ifdef bbcUPDATE_IN_BBCMODEL
void bbcModelBWatford::Update() const {
	bbcVideo::Update();
	bbc_system_via.Update();
	bbc_user_via.Update();
	bbc1770::Update();
	bbcAdc::Update();
#ifdef bbcSERIAL_ENABLE
	bbcAciaUpdate();
#endif
}
#endif
