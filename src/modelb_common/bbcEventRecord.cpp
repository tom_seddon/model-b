#include "pch.h"
#include "bbcEventRecord.h"
#include "bbcComputer.h"
#include <list>
#include <time.h>

#ifdef _WIN32

#include <process.h>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>//yum

#endif//_WIN32

// define to write in binary mode, if USE_FWRITE
#define BINARY_WRITE

// define for verbosity
//#define VERBOSE

// notes to self
//
// USE_WIN32 is fastest. WriteFile blocks for a LOT longer when running in
// the secondary thread. Suspect it requires CPU work (shuffling data onto
// some kind of queue?), and having the main thread running at the same
// time is preventing this being done quickly. The difference is reduced
// when chunk_write_size is closer to 256K (which I vaguely remember
// reading is the unit of caching), but having one thread is still faster.
// And increasing chunk_write_size makes the single-threaded version
// slightly quicker at the cost of making the threaded one much slower...
//
// NOTE: UI for threading is currently broken, as thread_num_chunks_written
// counts text chunks of chunk_write_size but the calculation (doh! copy
// and paste!) assumes it counts input chunks. So the %age gets over 100,
// and the dialog box disappears. (The UI does work (by accident!) if
// chunk_write_size is approx 5*chunk_size, as the relative proportions are
// then about the same; see chunk_write_size comment. This is staying
// unfixed as thread use is not an optimization, and it's only a cosmetic
// thing anyway.)

// note: numeric! no ifdef!

#ifdef _WIN32

#define USE_FWRITE 0
#define USE_WIN32 1
#define USE_THREAD 0

#else//_WIN32

#define USE_FWRITE 1
#define USE_WIN32 0
#define USE_THREAD 0

#endif//_WIN32

#if USE_FWRITE+USE_WIN32+USE_THREAD!=1
#error only one configuration define at once please!
#endif

//////////////////////////////////////////////////////////////////////////
//
bbcEventHandler::bbcEventHandler(unsigned data_size_in,const char *id_in):
data_size(data_size_in),
id(real_id_)
{
	BASSERT(id_in[0]!=0&&id_in[1]!=0&&id_in[2]!=0&&id_in[3]!=0&&id_in[4]==0);
	strcpy(real_id_,id_in);
}

bbcEventHandler::~bbcEventHandler() {
}

//////////////////////////////////////////////////////////////////////////
//
static const unsigned chunk_size=1048576;

// text is generated and written out in chunks of about this size in bytes.
// writing occurs when buffer >= this amount, so there will be a bit extra
// at the end -- there's no rule about how much extra there will be, but it
// will be Not Much.
//
// (fyi: on avg., text appears to take about about 4.5x-5x the space of the
// event records)
static const unsigned chunk_write_size=5*chunk_size;

// if there are more pending writes than this, main thread spins until one is flushed.
// text is generated faster than it can be saved to disk!
static const unsigned max_pending_thread_writes=4;

struct bbcEventRecord::Impl {
	std::list<Chunk> chunks;
};

bbcEventRecord::bbcEventRecord():
i_(new Impl)
{
	this->AddChunk();
}

bbcEventRecord::~bbcEventRecord() {
	for(std::list<Chunk>::iterator chunk=i_->chunks.begin();chunk!=i_->chunks.end();++chunk) {
		delete[] chunk->data;
	}
	delete i_;
}

void *bbcEventRecord::AddEvent(const bbcEventHandler *handler) {
	//If no handler specified, just return null. this allows for disabling
	//events if you store handler pointers in variables. obviously return
	//value now has to be checked :(
	if(!handler) {
		return 0;
	}

	Chunk *chunk=&i_->chunks.back();

	//will it fit? if not, create a new one. if there's still space left in this
	//chunk, add terminator.
	unsigned event_size=sizeof(bbcEventInfo)+handler->data_size;
	if(chunk->end+event_size>chunk_size) {
		//add new chunk.
		chunk=this->AddChunk();
	}

	//add space for this event.
	void *p=&chunk->data[chunk->end];
	chunk->end+=event_size;

	//fill in the bbcEventInfo.
	bbcEventInfo *info=static_cast<bbcEventInfo *>(p);
	info->handler=handler;
	info->cycles=bbcComputer::cpucycles;
	info->next_stop=bbcComputer::next_stop;

	//call the handler to fill it in.
	void *data=info+1;
	info->handler->FillEventData(data);

	//and return pointer to the data part.
	return data;
}

bbcEventRecord::Chunk *bbcEventRecord::AddChunk() {
	Chunk chunk;

	chunk.data=new t65::byte[chunk_size];
	chunk.end=0;

	i_->chunks.push_back(chunk);

	return &i_->chunks.back();
}

void bbcEventRecord::Clear() {
	for(std::list<Chunk>::const_iterator chunk=i_->chunks.begin();chunk!=i_->chunks.end();++chunk) {
		delete[] chunk->data;
	}
	i_->chunks.clear();
	this->AddChunk();
}

static double GetMBPerSec(unsigned num_bytes,clock_t start_ms,clock_t end_ms) {
	if(start_ms==end_ms) {
		return 0.;
	} else {
		return (num_bytes/((end_ms-start_ms)/1000.))/(1024.*1024.);
	}
}

#ifdef VERBOSE
static unsigned long GetMS(clock_t n) {
	return static_cast<unsigned long>(n/(CLOCKS_PER_SEC/1000.));
}
#endif

// adds exactly 10 chars, number left justified...
static void AddNum(char *dest,unsigned n) {
	bool seen_non_zero=false;

	//2147483648
	//1000000000
	int i=0;
	for(unsigned digit=1000000000;digit!=0;digit/=10) {
		unsigned d=n/digit%10;
		seen_non_zero=seen_non_zero||d!=0||digit==1;
		if(seen_non_zero) {
			dest[i++]=(char)(d+'0');
		}
	}

	while(i<10) {
		dest[i++]=' ';
	}
}

#if USE_THREAD
static CRITICAL_SECTION thread_cs;
static HANDLE thread_event;
static HANDLE thread_stop_event;
static HANDLE thread_forced_stop_event;
static std::list<std::vector<char> *> thread_pending_saves;
static HANDLE thread_file_handle;
static volatile bool thread_running;
static volatile bool thread_error;
static volatile unsigned thread_amount_written;
static volatile unsigned thread_num_chunks_written;

static void SaveThread(void *) {
	thread_running=true;
	thread_error=false;
	thread_amount_written=0;
	thread_num_chunks_written=0;
	OutputDebugString("Thread: started.\n");

	for(;;) {
		EnterCriticalSection(&thread_cs);

		std::vector<char> *v=0;
		if(!thread_pending_saves.empty()) {
			v=thread_pending_saves.front();
			thread_pending_saves.pop_front();
		}

		LeaveCriticalSection(&thread_cs);

		if(!v) {
			HANDLE handles[3]={
				thread_event,
				thread_stop_event,
				thread_forced_stop_event,
			};
			DWORD r=WaitForMultipleObjects(3,handles,FALSE,INFINITE);

			if(r!=WAIT_TIMEOUT) {
				// Which ones fired?
				if(WaitForSingleObject(thread_event,0)!=WAIT_TIMEOUT) {
					ResetEvent(thread_event);
				}

				if(WaitForSingleObject(thread_forced_stop_event,0)!=WAIT_TIMEOUT) {
					// no need for synchronization. main thread won't touch thread_pending_saves after
					// thread_forced_stop_event is set.
					for(std::list<std::vector<char> *>::iterator it=thread_pending_saves.begin();
						it!=thread_pending_saves.end();++it)
					{
						delete *it;
					}
					thread_pending_saves.clear();
					break;
				}

				if(WaitForSingleObject(thread_stop_event,0)!=WAIT_TIMEOUT) {
					break;
				}
			}
		} else {
			if(!thread_error) {
				DWORD nw;
				DWORD sms=clock();
				if(!WriteFile(thread_file_handle,&(*v)[0],v->size(),&nw,0)||nw!=v->size()) {
					thread_error=true;
				} else {
					thread_amount_written+=nw;
					++thread_num_chunks_written;
				}

				char buf[200];
				sprintf(buf,"Thread: p=%p n=%u error=%d time=%dms\n",&(*v)[0],v->size(),thread_error,clock()-sms);
				OutputDebugString(buf);

			}

			delete v;
		}
	}

	OutputDebugString("Thread: finished.\n");
	thread_running=false;

	_endthread();
}
#endif//USE_THREAD

bool bbcEventRecord::SaveEventsText(const char *file_name,ProgressFn progress_fn,void *context) {
#if USE_FWRITE
	FILE *h=fopen(file_name,
#ifdef BINARY_WRITE
		"wb"
#else
		"wt"
#endif
		);
	if(!h) {
		return false;
	}
#elif USE_WIN32
	HANDLE h=CreateFile(file_name,GENERIC_WRITE,0,0,CREATE_ALWAYS,0,0);
	if(h==INVALID_HANDLE_VALUE) {
		return false;
	}
#elif USE_THREAD
	thread_file_handle=CreateFile(file_name,GENERIC_WRITE,0,0,CREATE_ALWAYS,0,0);
	if(thread_file_handle==INVALID_HANDLE_VALUE) {
		return false;
	}
#else
#error
#endif

#if USE_THREAD
	thread_pending_saves.clear();
	thread_running=false;
	InitializeCriticalSection(&thread_cs);
	thread_event=CreateEvent(0,TRUE,FALSE,0);
	thread_stop_event=CreateEvent(0,TRUE,FALSE,0);
	thread_forced_stop_event=CreateEvent(0,TRUE,FALSE,0);
	_beginthread(&SaveThread,0,0);
#endif
	unsigned chunk_index=0;
#if USE_THREAD
	std::vector<char> *output=new std::vector<char>;
#else
	std::vector<char> real_output;
	std::vector<char> *output=&real_output;
#endif
//	unsigned write_size=4*1048576;//minimum number of bytes to write in one go
	float chunkamt=1.f/i_->chunks.size();
	unsigned amount_written=0;
	bool ok=false;

	clock_t start_ms=clock();
	output->reserve(chunk_write_size);
	(*progress_fn)(0,0,0,context);
	std::list<Chunk>::const_iterator chunk;
	for(chunk=i_->chunks.begin();chunk!=i_->chunks.end();++chunk,++chunk_index) {
		unsigned i=0;
		clock_t make_start_ms=clock();
		while(i<chunk->end) {
			const bbcEventInfo *info=reinterpret_cast<const bbcEventInfo *>(&chunk->data[i]);
			BASSERT(info->handler);
			BASSERT(i+info->handler->data_size<=chunk_size);
			const void *event=info+1;
			char prefix[31];

			//each event starts with the event type id, cycle count and next stop.
			//int indent=sprintf(prefix,"%s %-10u(@%-10u): ",info->handler->id,info->cycles,info->next_stop);//30 chars

			int indent=0;
			{
				for(;info->handler->id[indent];++indent) {
					prefix[indent]=info->handler->id[indent];
				}

				prefix[indent++]=' ';
				AddNum(&prefix[indent],info->cycles);
				indent+=10;
				prefix[indent++]='(';
				prefix[indent++]='@';
				AddNum(&prefix[indent],info->next_stop);
				indent+=10;
				prefix[indent++]=')';
				prefix[indent++]=':';
				prefix[indent++]=' ';
			}

			output->insert(output->end(),prefix,prefix+indent);

			//turn that into a string
			info->handler->AppendEventStr(info,event,indent,output);

			//advance to next event.
			i+=sizeof(bbcEventInfo)+info->handler->data_size;

			//if we've got enough, or we've reached the end of a chunk, write the text.
			if(output->size()>=chunk_write_size||i>=chunk->end) {
#ifdef VERBOSE
				clock_t make_end_ms=clock();
#endif

#if USE_THREAD
				float percent=thread_num_chunks_written/float(i_->chunks.size())+i/float(chunk->end)*chunkamt;
#else
				float percent=chunk_index/float(i_->chunks.size())+i/float(chunk->end)*chunkamt;
#endif

#ifdef VERBOSE
				printf("Saving events output: ");
#endif

				if(!(*progress_fn)(0,int(percent*100.f),amount_written,context)) {
					break;
				}

#ifdef VERBOSE
				clock_t save_start_ms=clock();
#endif
				unsigned old_output_size=output->size();
#if USE_FWRITE
				if(fwrite(&(*output)[0],1,output->size(),h)!=output->size()) {
					goto bye;
				}
#elif USE_WIN32
				DWORD num_written;
				if(!WriteFile(h,&(*output)[0],output->size(),&num_written,0)||num_written!=output->size()) {
					goto bye;
				}
#elif USE_THREAD
				if(thread_error) {
					goto bye;
				}

				// don't get too far ahead...
				// TODO: if thread_error, is this really safe? (think it is...)
				bool added=false;
				bool warned=false;
				clock_t wait_start_ms=clock();
				while(!added&&!thread_error) {
					EnterCriticalSection(&thread_cs);
					if(thread_pending_saves.size()<max_pending_thread_writes) {
						thread_pending_saves.push_back(output);
						added=true;
					} else {
						if(!warned) {
#ifdef VERBOSE
							printf("too many pending writes, waiting... ");
#endif
							Sleep(0);
							warned=true;
						}
					}
					LeaveCriticalSection(&thread_cs);
				}

				if(added&&warned) {
#ifdef VERBOSE
					printf("%dms.\n",clock()-wait_start_ms);
#endif
				}

				SetEvent(thread_event);

				output=new std::vector<char>;
#else
#error
#endif
#ifdef VERBOSE
				clock_t save_end_ms=clock();
#endif
#if USE_THREAD
				amount_written=thread_amount_written;
#else
				amount_written+=old_output_size;
#endif

#ifdef VERBOSE
				printf("wrote %u bytes. %.2fMB/sec save (%dms), %.2fMB/sec make (%dms). chunk %u/%u",output->size(),
					GetMBPerSec(old_output_size,save_start_ms,save_end_ms),GetMS(save_end_ms-save_start_ms),
					GetMBPerSec(old_output_size,make_start_ms,make_end_ms),GetMS(make_end_ms-make_start_ms),
					chunk_index,i_->chunks.size());
#endif
				output->resize(0);
				output->reserve(chunk_write_size+16384);

				printf(".\n");

				make_start_ms=clock();
			}
		}

#ifdef VERBOSE
		printf("   clear... ");
#endif
		output->resize(0);
		output->reserve(chunk_write_size+16384);
#ifdef VERBOSE
		printf("done.\n");
#endif
	}

	ok=chunk==i_->chunks.end();

bye:
#if USE_FWRITE
	fclose(h);
#elif USE_WIN32
	CloseHandle(h);
#elif USE_THREAD
	BASSERT(output->empty());
	delete output;
#ifdef VERBOSE
	printf("Waiting for thread... ");
#endif
	SetEvent(thread_stop_event);
	clock_t wait_thread_start_ms=clock();
	while(thread_running) {
		float percent=thread_num_chunks_written/float(i_->chunks.size());
		if(!(*progress_fn)(0,int(percent*100.f),thread_amount_written,context)) {
#ifdef VERBOSE
			printf("forcibly stopping... ");
#endif
			SetEvent(thread_forced_stop_event);
			ok=false;
		} else {
			Sleep(250);
		}
	}
	CloseHandle(thread_forced_stop_event);
	CloseHandle(thread_stop_event);
	CloseHandle(thread_event);
	CloseHandle(thread_file_handle);
	printf("%dms.\n",clock()-wait_thread_start_ms);
	DeleteCriticalSection(&thread_cs);
#else
#error
#endif

	if(ok) {
		clock_t end_ms=clock();
		float seconds=(end_ms-start_ms)/1000.f;
		printf("%u chunks (%.2fMB) in %.2fs. %.2fchunks/s, %.2fMB/sec.\n",i_->chunks.size(),amount_written/1048576.,seconds,
			i_->chunks.size()/seconds,GetMBPerSec(amount_written,start_ms,end_ms));
	}

	this->Clear();
	return ok;
}
