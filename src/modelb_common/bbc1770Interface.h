#ifndef bbc1770INTERFACE_H_
#define bbc1770INTERFACE_H_

#include "bbcMmioFnInit.h"
#include "bbc1770DriveControl.h"
#include "bbcSaveState.h"

//TODO this is so silly, should have virtual functions instead!!!! Fool!
struct bbc1770Interface {
	const char *name;
	t65::word fdc_start;
	t65::word drive_ctrl;

	typedef void (*GetDriveControlFromByteFn)(t65::byte src,
		bbc1770DriveControl *dest);
	typedef void (*GetByteFromDriveControlFn)(const bbc1770DriveControl &src,
		t65::byte *dest);

	GetDriveControlFromByteFn get_drive_control_from_byte_fn;
	GetByteFromDriveControlFn get_byte_from_drive_control_fn;

	void (*init_fn)();

	//flags for hacks, tweaks and such.
	unsigned flags;

	const bbcMmioFnInit *extra_mmio_fns;

	enum InterfaceFlags {
		DISABLE_RESTORE_IRQ=1,
		INVERT_TRACK0=2,
	};

	bbcSaveStateHandler save_state_handler;
};

#endif
