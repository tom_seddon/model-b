#include "pch.h"
#include "bbcModelMaster128.h"
#include "bbcCommonHardware.h"

const bbcModelMaster128 bbcModelMaster128::model;

const bbcMmioFnInit *bbcModelMaster128::ExtraMmioFns() const {
	static const bbcMmioFnInit mmio_fns[]={
		{0xfe30,16,&bbcComputer::ReadRomSelMaster128,&bbcComputer::WriteRomSelMaster128},
		{0,0},
	};

	return mmio_fns;
}

bbcModelMaster128::bbcModelMaster128():
bbcModelGeneric<bbcModelMaster128Sim>(bbcMODEL_NO_KEYBOARD_LINKS)
{
}

#ifdef bbcUPDATE_IN_BBCMODEL
void bbcModelMaster128::Update() const {
	bbcVideo::Update();
	bbc_system_via.Update();
	bbc_user_via.Update();
	bbc1770::Update();
	bbcAdc::Update();
#ifdef bbcSERIAL_ENABLE
	bbcAciaUpdate();
#endif
}
#endif
