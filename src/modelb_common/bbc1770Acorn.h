#ifndef FDCACORN1770_H_
#define FDCACORN1770_H_

#include "bbc1770Interface.h"

struct bbc1770Acorn {
	static const bbc1770Interface disc_interface;

	static void GetByteFromDriveControl(const bbc1770DriveControl &src,
		t65::byte *dest);
	static void GetDriveControlFromByte(t65::byte src,bbc1770DriveControl *dest);
};

#endif
