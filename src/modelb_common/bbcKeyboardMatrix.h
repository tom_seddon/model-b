#ifndef bbcKEYBOARDMATRIX_H_
#define bbcKEYBOARDMATRIX_H_

#include "bbcSaveState.h"

//The bbcKeyboardMatrix maintains up to 256 key states. Keys >=128
//aren't accessible from the BBC, but the bbcKeyboardMatrix tracks
//their state anyway to keep the code cleaner.
class bbcKeyboardMatrix {
public:
	static void Init();
	static bool Write(t65::byte *);
	static bool KeyState(t65::byte code);
	static void SetKeyState(t65::byte code,bool state);
	static void ForceSetKeyState(t65::byte code,bool state);
	static int NumKeysDown();
	static bool NeedsIrq();
	static void SetWriteEnabled(bool w_en);

	static bool ManualScanFlag();

	static void SetInput(t65::byte input);

	static void SetForceShiftPressed(bool new_force_shift_pressed);

	static bbcSaveState::LoadStatus LoadSaveState(const bbcSaveState *save_state);
	static bbcSaveState::SaveStatus SaveSaveState(bbcSaveState *save_state);
protected:
private:
	static bool manual_scan_flag_;
	static bool write_enabled_;
	static t65::byte input_;

	static bool keyflags_[256];
	static bool force_shift_pressed_;
	static int num_keys_down_;

	bbcKeyboardMatrix();
	bbcKeyboardMatrix(const bbcKeyboardMatrix &);
	bbcKeyboardMatrix &operator=(const bbcKeyboardMatrix &);
};

inline bool bbcKeyboardMatrix::KeyState(t65::byte code) {
	return keyflags_[code];
}

inline void bbcKeyboardMatrix::ForceSetKeyState(t65::byte code,bool state) {
	BASSERT(!(code&0xF0));//it shouldn't be one that would generate an IRQ
	keyflags_[code]=state;
}

inline int bbcKeyboardMatrix::NumKeysDown() {
	return num_keys_down_;
}

inline bool bbcKeyboardMatrix::ManualScanFlag() {
	return write_enabled_&&manual_scan_flag_;
}

inline void bbcKeyboardMatrix::SetInput(t65::byte input) {
	input_=input;
}

//inline bool bbcKeyboardMatrix::NeedsIrq() {
//	return NumKeysDown()>0;
//}

inline void bbcKeyboardMatrix::SetWriteEnabled(bool w_en) {
	write_enabled_=w_en;
}

inline void bbcKeyboardMatrix::SetForceShiftPressed(bool new_force_shift_pressed) {
	force_shift_pressed_=new_force_shift_pressed;
}

#endif
