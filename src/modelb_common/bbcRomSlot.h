#ifndef bbcROMSLOT_H_
#define bbcROMSLOT_H_

#include <string.h>

struct bbcRomSlot {
	bbcRomSlot();
	void GetPages(t65::byte **read_pages,t65::byte **write_pages) const;
	void GetReadPages(t65::byte **read_pages) const;
	void GetWritePages(t65::byte **write_pages) const;
#ifdef bbcENABLE_BEEB_ICE
	void GetBrkPages(t65::byte **brk_pages) const;
#endif
	const t65::byte *GetContents() const;

	void ResetContents();
	void SetContents(const t65::byte *src,unsigned num_bytes);
	bool GetWriteable() const;
	void SetWriteable(bool is_writeable);
	void WriteByte(t65::word offset,t65::byte val);
private:
	void ResetPages();
	t65::byte *read_pages_[64];
	t65::byte *write_pages_[64];
	bool is_writeable_;
	t65::byte contents_[16384];
#ifdef bbcENABLE_BEEB_ICE
	t65::byte *brk_pages_[64];
	t65::byte brk_[16384];
#endif
	static t65::byte dummy_page[256];//where writes to non-writeable ROMs go.
};

inline void bbcRomSlot::GetReadPages(t65::byte **read_pages) const {
	memcpy(read_pages,read_pages_,64*sizeof(t65::byte *));
}

inline void bbcRomSlot::GetWritePages(t65::byte **write_pages) const {
	memcpy(write_pages,write_pages_,64*sizeof(t65::byte *));
}

#ifdef bbcENABLE_BEEB_ICE
inline void bbcRomSlot::GetBrkPages(t65::byte **brk_pages) const {
	memcpy(brk_pages,brk_pages_,64*sizeof(t65::byte *));
}
#endif

inline void bbcRomSlot::GetPages(t65::byte **read_pages,t65::byte **write_pages) const {
	this->GetReadPages(read_pages);
	this->GetWritePages(write_pages);
}

inline void bbcRomSlot::SetWriteable(bool is_writeable) {
	is_writeable_=is_writeable;
	this->ResetPages();
}

inline void bbcRomSlot::WriteByte(t65::word offset,t65::byte val) {
	BASSERT(offset<16384);
	contents_[offset]=val;
}

#endif
