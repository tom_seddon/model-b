#include "pch.h"
#include "bbcModel.h"
#include "bbcComputer.h"
#include "bbc1770.h"
#include "bbcSystemVIA.h"
#include "bbcUserVIA.h"
#include <stdarg.h>

#ifdef bbcDEBUG_TRACE_EVENTS
//////////////////////////////////////////////////////////////////////////
//
static const char hex_digits[]="0123456789ABCDEF";

const bbcModelCpuEventHandler bbcModelCpuEventHandler::handler;

bbcModelCpuEventHandler::bbcModelCpuEventHandler():
bbcEventHandler(sizeof(Data),"CPU ")
{
}

void bbcModelCpuEventHandler::FillEventData(void *event) const {
	(void)event;

	//Doesn't do anything
}

static int CopyText(char *dest,int dest_idx,const char *src,int width) {
	int src_idx;
	for(src_idx=0;src_idx<width&&src[src_idx];++src_idx) {
		dest[dest_idx++]=src[src_idx];
	}

	for(;src_idx<width;++src_idx) {
		dest[dest_idx++]=' ';
	}

	return dest_idx;
}

#define ADD_REG(C,R)\
	do {\
		linebuf[n++]=C;\
		linebuf[n++]=':';\
		linebuf[n++]=hex_digits[(ev->R)>>4];\
		linebuf[n++]=hex_digits[(ev->R)&0xf];\
		linebuf[n++]=' ';\
	} while((0,0))

void bbcModelCpuEventHandler::AppendEventStr(const bbcEventInfo *info,const void *event,int indent,void *voidOutput) const {
//	static const char *nullbytes="  ";
	(void)indent,(void)info;

	std::vector<char> *output=static_cast<std::vector<char> *>(voidOutput);
	const Data *ev=static_cast<const Data *>(event);
	char linebuf[80];
	char opcode_buf[10],operand_buf[15];
	int len=bbcComputer::DisassembleBytes(ev->bytes,ev->pc,opcode_buf,operand_buf);

//	char bytes[3][3];
//	for(int i=0;i<3;++i) {
//		bytes[i][0]=hex_digits[ev->bytes[i]>>4];
//		bytes[i][1]=hex_digits[ev->bytes[i]&0xf];
//		bytes[i][2]=0;
//	}
//
	//0123456789012345678901234567890123456789012345678901234567890123456789
	//1234: 12345 1234567890 xx xx xx -> A:xx X:xx Y:xx S:xx P:12345678
//	int n=_snprintf(linebuf,sizeof linebuf-1,"%04X: %-5s %-10s %s %s %s -> A:%02X X:%02X Y:%02X S:%02X P:%c%c%c%c%c%c%c%c\n",
//		ev->pc.w,opcode_buf,operand_buf,
//		len>0?bytes[0]:nullbytes,
//		len>1?bytes[1]:nullbytes,
//		len>2?bytes[2]:nullbytes,
//		ev->a,ev->x,ev->y,ev->s,
//		ev->p&t65::N_MASK?'N':'-',
//		ev->p&t65::V_MASK?'V':'-',
//		ev->p&t65::U_MASK?'-':'-',
//		ev->p&t65::B_MASK?'B':'-',
//		ev->p&t65::D_MASK?'D':'-',
//		ev->p&t65::I_MASK?'I':'-',
//		ev->p&t65::Z_MASK?'Z':'-',
//		ev->p&t65::C_MASK?'C':'-');

	int n=0;

	//"%04X"
	linebuf[n++]=hex_digits[ev->pc.w>>12];
	linebuf[n++]=hex_digits[(ev->pc.w>>8)&0xf];
	linebuf[n++]=hex_digits[(ev->pc.w>>4)&0xf];
	linebuf[n++]=hex_digits[ev->pc.w&0xf];

	//": "
	linebuf[n++]=':';
	linebuf[n++]=' ';

	//"%-5s"
	n=CopyText(linebuf,n,opcode_buf,5);

	linebuf[n++]=' ';

	//"%-10s"
	n=CopyText(linebuf,n,operand_buf,10);

	for(int i=0;i<3;++i) {
		if(i<len) {
			linebuf[n++]=hex_digits[ev->bytes[i]>>4];
			linebuf[n++]=hex_digits[ev->bytes[i]&0xf];
		} else {
			linebuf[n++]=' ';
			linebuf[n++]=' ';
		}
		linebuf[n++]=' ';
	}

	//" -> "
	linebuf[n++]=' ';
	linebuf[n++]='-';
	linebuf[n++]='>';
	linebuf[n++]=' ';

	ADD_REG('A',a);
	ADD_REG('X',x);
	ADD_REG('Y',y);
	ADD_REG('S',s);

	linebuf[n++]='P';
	linebuf[n++]=':';
	linebuf[n++]=ev->p&t65::N_MASK?'N':'-';
	linebuf[n++]=ev->p&t65::V_MASK?'V':'-';
	linebuf[n++]=ev->p&t65::U_MASK?'-':'-';
	linebuf[n++]=ev->p&t65::B_MASK?'B':'-';
	linebuf[n++]=ev->p&t65::D_MASK?'D':'-';
	linebuf[n++]=ev->p&t65::I_MASK?'I':'-';
	linebuf[n++]=ev->p&t65::Z_MASK?'Z':'-';
	linebuf[n++]=ev->p&t65::C_MASK?'C':'-';

	linebuf[n++]='\n';

	output->insert(output->end(),linebuf,linebuf+n);
}

//////////////////////////////////////////////////////////////////////////
//
const bbcModelIrqEventHandler bbcModelIrqEventHandler::handler;

bbcModelIrqEventHandler::bbcModelIrqEventHandler():
bbcEventHandler(sizeof(Data),"IRQ ")
{
}

//grab irq data from wherever.
//(this is all getting disturbingly cosy.)
void bbcModelIrqEventHandler::FillEventData(void *event) const {
	Data *ev=static_cast<Data *>(event);

	ev->fdc_status=bbc1770::Status();
	ev->irqflags=bbcComputer::irq_flags_;
	bbc_system_via.FillTraceState(&ev->sys);
	bbc_user_via.FillTraceState(&ev->usr);
}

//static const char *const via_irq_flag_names[]={
//	"T1","T2","CB1","CB2","SR","CA1","CA2",
//};
//
//static void WriteViaIrqFlags(FILE *out,int indent,const char *reg,t65::byte flags) {
//	fprintf(out,"%-*s: %s:",indent,"",reg);
//	for(unsigned i=0;i<7;++i) {
//		fprintf(out,"%-4s",(flags<<i)&0x40?via_irq_flag_names[i]:"");
//	}
//	fprintf(out,"\n");
//}
//
//void bbcDebugTrace::WriteViaState(FILE *out,int indent,const char *via_name,const Irq::Via *state) const {
//	fprintf(out,"%-*s%s: IER=&%02X IFR=&%02X ACR=&%02X T1L=&%02X%02X T1C=%d T2C=%d\n",
//		indent,"",via_name,state->ier,state->ifr,state->acr,state->t1l.h,state->t1l.l,state->t1c,state->t2c);
//	int irq_indent=indent+strlen(via_name);
//	WriteViaIrqFlags(out,irq_indent,"IER",state->ier);
//	WriteViaIrqFlags(out,irq_indent,"IFR",state->ifr);
//}
//

#define IRQ_FLAG(F) (ev->irqflags&(F)?" " #F:"")

//static void AppendFmt(std::vector<char> *output,const char *fmt,...) {
//	va_list v;
//	static const unsigned max_size=150;
//
//	unsigned old_size=output->size();
//	output->resize(old_size+max_size);//make space
//	va_start(v,fmt);
//	int n=_vsnprintf(&(*output)[old_size],max_size,fmt,v);
//	va_end(v);
//
//	//chop off the excess.
//	output->resize(old_size+n);
//}

void bbcModelIrqEventHandler::AppendViaFlagsState(t65::byte val,const char *name,int indent,void *voidOutput) const {
	static const char *const via_irq_flag_names[]={"T1","T2","CB1","CB2","SR","CA1","CA2",};
	static const char *blank="   ";//must be same length as longest flag name
	std::vector<char> *output=static_cast<std::vector<char> *>(voidOutput);

	output->insert(output->end(),indent,' ');
	output->insert(output->end(),name,name+3);
	for(unsigned i=0;i<7;++i) {
		t65::byte mask=0x40>>i;
		const char *src=val&mask?via_irq_flag_names[i]:blank;

		output->push_back(' ');
		for(unsigned j=0;via_irq_flag_names[i][j];++j) {
			output->push_back(src[j]);
		}
	}
	output->push_back('\n');
}

void bbcModelIrqEventHandler::AppendViaState(const bbcVIA::TraceState &state,const char *name,int indent,void *voidOutput) const
{
	std::vector<char> *output=static_cast<std::vector<char> *>(voidOutput);
	
	//line 1, basic state
	output->insert(output->end(),indent,' ');

	// basic info
	{
		int name_len=strlen(name);
		char buf[80];

		memcpy(buf,name,name_len);
		int i=name_len;
		buf[i++]=':';
		buf[i++]=' ';
		buf[i++]='I';
		buf[i++]='E';
		buf[i++]='R';
		buf[i++]='=';
		buf[i++]='&';
		buf[i++]=hex_digits[state.ier>>4];
		buf[i++]=hex_digits[state.ier&0xf];
		buf[i++]=' ';
		buf[i++]='I';
		buf[i++]='F';
		buf[i++]='R';
		buf[i++]='=';
		buf[i++]='&';
		buf[i++]=hex_digits[state.ifr>>4];
		buf[i++]=hex_digits[state.ifr&0xf];
		buf[i++]=' ';
		buf[i++]='A';
		buf[i++]='C';
		buf[i++]='R';
		buf[i++]='=';
		buf[i++]='&';
		buf[i++]=hex_digits[state.acr>>4];
		buf[i++]=hex_digits[state.acr&0xf];
		buf[i++]=' ';
		buf[i++]='T';
		buf[i++]='1';
		buf[i++]='L';
		buf[i++]='=';
		buf[i++]='&';
		buf[i++]=hex_digits[state.t1l.h>>4];
		buf[i++]=hex_digits[state.t1l.h&0xf];
		buf[i++]=hex_digits[state.t1l.l>>4];
		buf[i++]=hex_digits[state.t1l.l&0xf];
		buf[i++]=' ';
		buf[i++]='T';
		buf[i++]='1';
		buf[i++]='C';
		buf[i++]='=';
		buf[i++]='0'+state.t1c/10000%10;
		buf[i++]='0'+state.t1c/1000%10;
		buf[i++]='0'+state.t1c/100%10;
		buf[i++]='0'+state.t1c/10%10;
		buf[i++]='0'+state.t1c%10;
		buf[i++]=' ';
		buf[i++]='T';
		buf[i++]='2';
		buf[i++]='C';
		buf[i++]='=';
		buf[i++]='&';
		buf[i++]='0'+state.t2c/10000%10;
		buf[i++]='0'+state.t2c/1000%10;
		buf[i++]='0'+state.t2c/100%10;
		buf[i++]='0'+state.t2c/10%10;
		buf[i++]='0'+state.t2c%10;
		buf[i++]='\n';
		output->insert(output->end(),buf,buf+i);
	}

//	AppendFmt(output,"%s: IER=&%02X IFR=&%02X ACR=&%02X T1L=&%02X%02X T1C=%d T2C=%d\n",
//		name,state.ier,state.ifr,state.acr,state.t1l.h,state.t1l.l,state.t1c,state.t2c);

	//line 2, IER state
	indent+=strlen(name)+2;//+2 for ": "
	this->AppendViaFlagsState(state.ier,"IER",indent,output);

	//line 3, IFR state
	this->AppendViaFlagsState(state.ifr,"IFR",indent,output);
}

void bbcModelIrqEventHandler::AppendEventStr(const bbcEventInfo *info,const void *event,int indent,void *voidOutput) const {
	(void)info;

	std::vector<char> *output=static_cast<std::vector<char> *>(voidOutput);
	const Data *ev=static_cast<const Data *>(event);
	
//	AppendFmt(output,"flags 0x%X: %s%s%s%s\n",ev->irqflags,IRQ_FLAG(IRQ_SYSVIA),IRQ_FLAG(IRQ_USERVIA),IRQ_FLAG(IRQ_ACIA),IRQ_FLAG(IRQ_NMI));

	{
		char buf[80];
		int i=0;

		buf[i++]='f';
		buf[i++]='l';
		buf[i++]='a';
		buf[i++]='g';
		buf[i++]='s';
		buf[i++]=' ';
		buf[i++]='0';
		buf[i++]='x';
		buf[i++]=hex_digits[(ev->irqflags>>28)&0xf];
		buf[i++]=hex_digits[(ev->irqflags>>24)&0xf];
		buf[i++]=hex_digits[(ev->irqflags>>20)&0xf];
		buf[i++]=hex_digits[(ev->irqflags>>16)&0xf];
		buf[i++]=hex_digits[(ev->irqflags>>12)&0xf];
		buf[i++]=hex_digits[(ev->irqflags>>8)&0xf];
		buf[i++]=hex_digits[(ev->irqflags>>4)&0xf];
		buf[i++]=hex_digits[ev->irqflags&0xf];
		buf[i++]=':';
		buf[i++]=' ';
		if(ev->irqflags&IRQ_SYSVIA) {
			memcpy(buf+i,"IRQ_SYSVIA ",11);
			i+=11;
		}
		if(ev->irqflags&IRQ_USERVIA) {
			memcpy(buf+i,"IRQ_USERVIA ",12);
			i+=12;
		}
		if(ev->irqflags&IRQ_ACIA) {
			memcpy(buf+i,"IRQ_ACIA ",9);
			i+=9;
		}
		if(ev->irqflags&IRQ_NMI) {
			memcpy(buf+i,"IRQ_NMI ",8);
			i+=8;
		}
		buf[i++]='\n';
		output->insert(output->end(),buf,buf+i);
	}


	this->AppendViaState(ev->sys,"SysVIA",indent,output);
	this->AppendViaState(ev->usr,"UsrVIA",indent,output);
//	AppendFmt(output,"%-*s1770  : Status=&%02X\n",indent,"",ev->fdc_status);
	{
		char buf[80];
		int i=0;
		
		while(i<indent) {
			buf[i++]=' ';
		}
		buf[i++]='1';
		buf[i++]='7';
		buf[i++]='7';
		buf[i++]='0';
		buf[i++]=' ';
		buf[i++]=' ';
		buf[i++]=':';
		buf[i++]=' ';
		buf[i++]='S';
		buf[i++]='t';
		buf[i++]='a';
		buf[i++]='t';
		buf[i++]='u';
		buf[i++]='s';
		buf[i++]='=';
		buf[i++]='&';
		buf[i++]=hex_digits[ev->fdc_status>>4];
		buf[i++]=hex_digits[ev->fdc_status&0xf];
		buf[i++]='\n';
		output->insert(output->end(),buf,buf+i);
	}

//	fprintf(out,"flags 0x%X: %s %s %s %s\n",
//		irq->irqflags,IRQ_FLAG(IRQ_SYSVIA),IRQ_FLAG(IRQ_USERVIA),IRQ_FLAG(IRQ_ACIA),IRQ_FLAG(IRQ_NMI));
//	this->WriteViaState(out,left_margin,"SysVIA",&irq->sys);
//	this->WriteViaState(out,left_margin,"UsrVIA",&irq->usr);
//	fprintf(out,"%-*s1770  : Status=&%02X\n",
//		left_margin,"",irq->fdc_status);

}
#endif

//////////////////////////////////////////////////////////////////////////
//
bbcModel::bbcModel(unsigned flags_in):
flags(flags_in)
{
}

bbcModel::~bbcModel() {
}

//bbcSaveState::LoadStatus bbcModel::LoadSaveState(const bbcSaveState *save_state)
//{
//	return bbcSaveState::LS_NOT_IMPLEMENTED;
//}
//
//bbcSaveState::SaveStatus bbcModel::SaveSaveState(bbcSaveState *save_state)
//{
//	return bbcSaveState::SS_NOT_IMPLEMENTED;
//}

const bbcSaveStateHandler *bbcModel::GetSaveStateHandlers() const {
	return 0;
}
