#ifndef bbcBEEBICE_H_
#define bbcBEEBICE_H_

#ifdef bbcENABLE_BEEB_ICE

// due to the sheer number of flags, and how they have to be correctly maintained when paging,
// it is bbcComputer that holds the membrk flags. the beebice class just holds the regbrk
// ones. so this class is informed on every register change, and detects regbrks itself. (that
// is kind of its interface to t65.) is informed when bbcComputer detects a membrk, but doesn't
// detect those itself. (that is its interface to bbcComputer.) this also reduces the amount of
// code; there are only a few memory access functions, but tons of places in which the 6502 simulator
// changes the register values.
//
// this should really be neatened up. whatever happens, it should be this class that manages the
// getting and setting of the breakpoint flags.

class bbcBeebIce
{
public:
	// these flags fit in 1 byte
	enum MemBrkTypes {
		MBT_READ=1,
		MBT_WRITE=2,
		MBT_EXECUTE=4,
	};

	// these flags fit in 1 byte
	enum RegBrkTypes {
		RBT_A=1,
		RBT_X=2,
		RBT_Y=4,
		RBT_P=8,
		RBT_S=16,
	};

	struct BrkStatus {
		union Flags {
			t65::dword all;
			struct {
				t65::byte mem;
				t65::byte reg;
			};
		};
		Flags flags;
		t65::Word read_addr;
		t65::Word write_addr;
		t65::byte write_val;
		t65::Word execute_addr;

		BrkStatus();
	};

	static void Init();

	static void OnChangeA();
	static void OnChangeX();
	static void OnChangeY();
	static void OnChangeP();
	static void OnChangeS();

	static void OnReadBrk(t65::Word addr);
	static void OnWriteBrk(t65::Word addr,t65::byte val);
	static void OnExecuteBrk(t65::Word addr);

	static BrkStatus *GetBrkStatus();
protected:
private:
	static BrkStatus bs_;
	static t65::byte regbrks_[256];
};

inline bbcBeebIce::BrkStatus *bbcBeebIce::GetBrkStatus() {
	return &bs_;
}

#endif//bbcENABLE_BEEB_ICE

#endif
