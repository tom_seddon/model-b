#ifndef BBCUEF_H_
#define BBCUEF_H_

//#include <vector>

struct bbcUefChunk {
	t65::word id;
	std::vector<t65::byte> data;

	void AppendData(std::vector<t65::byte> &contents) const;
};

class bbcUef {
public:
	bbcUef(t65::byte vmajor,t65::byte vminor);
	bbcUef(const std::vector<t65::byte> &contents);

	bool ReadData(const std::vector<t65::byte> &contents);
	void GetData(std::vector<t65::byte> &contents) const;

	unsigned NumChunks() const;
	const bbcUefChunk *Chunks() const;
	bool Ok() const;
protected:
private:
	bool ok_;
	typedef std::vector<bbcUefChunk> C;
	C chunks_;
	t65::byte  vmajor_,vminor_;
};

#endif
