#ifndef bbc1770_H_
#define bbc1770_H_

#include "bbcFdd.h"
#include "bbc1770Interface.h"

//struct BeebDiscImage;

struct bbc1770 {
public:
	typedef void (*CmdUpdateFn)();
	
	//////////////////////////////////////////////////////////////////////////
	// Query functions

	enum FdcAction {
		ACTION_IDLE,
		ACTION_READ,
		ACTION_WRITE,
		ACTION_OTHER,
	};

	static bool GetCurrentAction(FdcAction *action,int *drive,int *side,int *track,
		int *sector);
	static t65::byte Status();

	//////////////////////////////////////////////////////////////////////////
	//Init (one time)
	static void Init(const bbc1770Interface &disc_interface);

	//Hard reset
	static void Reset();

	//Update
	static void Update();

	static t65::byte ReadFdc(t65::word addr);
	static void WriteFdc(t65::word addr,t65::byte val);
	static t65::byte ReadControl(t65::word addr);
	static void WriteControl(t65::word addr,t65::byte val);

	// STate
	static bbcSaveState::LoadStatus LoadSaveState(const bbcSaveState *save_state);
	static bbcSaveState::SaveStatus SaveSaveState(bbcSaveState *save_state);
protected:
private:
	enum StatusRegisterFlags {
		STATUS_MOTOR_ON=128,
		STATUS_TYPE23_NOTREADY=128,//data sheet says 1773 only??
		STATUS_WRITE_PROTECT=64,
		STATUS_TYPE1_SPINUPENDED=32,
		STATUS_TYPE3_DELETEDDATA=32,
		STATUS_RECORDNOTFOUND=16,
		STATUS_CRCERROR=8,
		STATUS_TYPE1_NOTTRACKZERO=4,
		STATUS_TYPE23_LOSTDATA=4,
		STATUS_TYPE1_INDEX=2,
		STATUS_TYPE23_DATAREQUEST=2,
		STATUS_BUSY=1,
	};
	
	static void WriteCommandRegister(t65::byte value);
	static t65::byte ReadStatusRegister();
	static t65::byte ReadTrackRegister();
	static void WriteTrackRegister(t65::byte value);
	static t65::byte ReadSectorRegister();
	static void WriteSectorRegister(t65::byte value);
	static t65::byte ReadDataRegister();
	static void WriteDataRegister(t65::byte value);

	static t65::byte status_;
	static t65::byte track_;
	static t65::byte sector_;
	static t65::byte data_;

	//Where the drive head actually is.
	static int phys_track_;
	static int phys_sector_;

	static bbc1770DriveControl drive_control_;

	//Direction of the last Step In/Step Out command.
	static int last_step_;

	//Current disc interface
	static bbc1770Interface disc_interface_;

	enum FdcCmdFlags {
		//
		CMD_SEEK_VERIFY=8,
			
		//bodge (this seek is actually a restore)
		CMD_SEEK_RESTORE=4,

		//Whether to update track register or not
		CMD_UPDATE_TRACK=1,//Seek/Reset

		//Don't IRQ at the end of this command -- a bodge for the Restore
		//command on the Challenger disc ysstem (which doesn't NMI for some reason)
		CMD_NO_IRQ=2,
	};

	static void CmdUpdateWinddown();
	static void CmdUpdateSeek();
	static void CmdUpdateReadSector();
	static void CmdUpdateWriteSectorSetDrq();
	static void CmdUpdateWriteSectorWriteData();
	static void CmdUpdateReadAddress();
	static bool CmdNextByte();

	//Current command
	
	//Update function to call.
	//static CmdUpdateFn cmd_update_fn_;
	enum CmdUpdateMode {
		UPDATE_IDLE,
		UPDATE_WINDDOWN,
		UPDATE_SEEK,
		UPDATE_READ_SECTOR,
		UPDATE_WRITE_SECTOR_SET_DRQ,
		UPDATE_WRITE_SECTOR_WRITE_DATA,
		UPDATE_READ_ADDRESS,
		UPDATE_MAX,
	};

	static const FdcAction fdc_action_from_update_mode_[UPDATE_MAX];

	static CmdUpdateMode cmd_update_mode_;
	static const CmdUpdateFn cmd_update_fns_[UPDATE_MAX];

	//Flags for this command. Combination of FdcCmdFlags. (All)
	static unsigned cmd_flags_;

	//Track the command is processing now (Read Sector/Write Sector/Seek)
	static int cmd_track_;

	//Sector the command is processing now (Read Sector/Write Sector)
	static int cmd_sector_;//Read/Write

	//Number of sectors left to go (Read Sector/Write Sector)
	//Processing stops when this many sectors have been read or the end of the track is encountered.
	static int cmd_num_sectors_left_;//Read/Write

	//Index of current byte in current sector (Read Sector/Write Sector)
	//Index of current byte in address data (Read Address)
	static int cmd_cur_offset_;

	//Cpu cycle point at which the FDC is next going to do something.
	static int cmd_next_byte_at_;//Any

	//Read Address data, data is fed out of this little buffer.
	static t65::byte cmd_address_[6];

	//Set up the parameters as appropriate.
	//Then call this. It does the SetNextStop & NMI stuff.
	static void StartCmd(CmdUpdateMode start_mode,int delay);

	//Sets the relevant bit of the status register to 0 or 1 depending
	//on 'setting'.
	static void StatusFlagIf(t65::byte mask,bool setting);

	//Whether current command address is valid
	static bool IsCurrentCmdAddressValid(bbcFdd::DiscDensity density);

	//NMI.
	static void SetNMI();
};

#endif
