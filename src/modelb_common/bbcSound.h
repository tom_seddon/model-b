#ifndef bbcSOUND_H_
#define bbcSOUND_H_

//#include <vector>

//#define bbcSOUND_SOUND_CYCLES() (BBCMicro::cycles&(~7))
#define bbcSOUND_CYCLES_DIVISOR() (8)
#define bbcSOUND_CYCLES() (bbcComputer::Cycles2MHz()/bbcSOUND_CYCLES_DIVISOR())

//TODO (abandoned for now... realised I can't remember how the Render functions work :)
//
//If defined, SoundWriteEntry will used 'word cycles' rather than 'int cycles'. 
//For this to work, it must not be possible for two writes in the queue to be further
//than 65,536 cycles apart.
//
//Can write to sound every 8th cycle at most. So max 5,000 writes per frame. So ensure
//max_len_frames is 13 (13*5000=65000) or less.
//#define bbcSOUND_WORD_CYCLES

#ifdef bbcSOUND_WORD_CYCLES
#error "bbcSOUND_WORD_CYCLES not supported yet."
#endif

struct bbcSound {
public:
	enum {
		max_len_frames=8,
	};
	typedef int fixedpoint;

	static void Init();
	
	static void Write(t65::byte v);

	static void SetFrequency(int hz);

	static void Render8BitMono(void **buffers,unsigned *buffer_sizes,unsigned num_buffers);
	static void Render16BitMono(void **buffers,unsigned *buffer_sizes,unsigned num_buffers);
	static void Render8BitStereo(void **buffers,unsigned *buffer_sizes,unsigned num_buffers);
	static void Render16BitStereo(void **buffers,unsigned *buffer_sizes,unsigned num_buffers);

	static void SetChannelStereoLeftRight(int channel,bool is_left,bool is_right);

	static bool IsRecording();
	static void StartRecording();
	static void StopRecording();
	static void GetRecordedVgmData(std::vector<t65::byte> *vgm_data);
	static void ClearRecording();
private:
	struct SoundWriteEntry {
		t65::byte val;
		int cycles;

		inline SoundWriteEntry() {
		}

		inline SoundWriteEntry(t65::byte val_in,int cycles_in):
		val(val_in),
		cycles(cycles_in)
		{
		}
	};

	struct SoundWriteEntryWord {
		t65::byte val;
		t65::word wcycles;
	};
	
	typedef std::vector<SoundWriteEntry> RecordedWrites;
	static bool is_recording_;
	static RecordedWrites recorded_writes_;

	//Sound queue -- built up while emulator runs, flushed when data is rendered.
	//
	enum {
		max_num_writes=2000000/bbcSOUND_CYCLES_DIVISOR()/50*max_len_frames,
	};

	static unsigned write_idx_;
#ifdef bbcSOUND_WORD_CYCLES
	static SoundWriteEntryWord writes_[max_num_writes+1];
#else
	static SoundWriteEntry writes_[max_num_writes+1];
#endif
	static int last_render_at_;

	//Current sound state
	//Not externally visible -- just used by the renderer

	//Latch
	static t65::byte latch_;
	struct SoundRegister {
		t65::word vol;
		t65::word pitch;
		fixedpoint counter;
		int mul;
	};
	static SoundRegister cur_regs_[4];

	//Whether this channel is stereo left
	static bool is_stereo_left_[4];

	//Whether this channel is stereo right
	static bool is_stereo_right_[4];
	
	//Noise generator
	static t65::word noise_seed_;
	static int noise_bit_;//The bit currently being output.

	//Updates current settings as if byte 'v' were just written
	static void DoByte(t65::byte v);

	//value in Hz given tone register setting
	static int hz_from_tone_[1024];

	//static fixedpoint freq_mul_;

	static fixedpoint samples_per_switch_[1024];

	//Gets next noise value, updates seed & all the rest.
	static int NextNoiseBit();

	//Current frequency
	static int frequency_;

	template<class T>
	static void RenderSound(void **buffers,unsigned *buffer_sizes_bytes,unsigned num_buffers,
		bool stereo);
};

#endif
