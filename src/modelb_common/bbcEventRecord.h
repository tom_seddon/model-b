#ifndef BBCEVENTRECORD_H_
#define BBCEVENTRECORD_H_

#include <stdio.h>
//#include <list>
//#include <vector>

//a system for recording events. an event consists of a small amount of event-agnostic information
//(bbcEventInfo), and a number of bytes of event-specific data. 

class bbcEventHandler;

//info about an event.
#ifdef _MSC_VER
#pragma pack(push,1)
#endif

struct bbcEventInfo {
	const bbcEventHandler *handler;
	int cycles;
	int next_stop;
};

#ifdef _MSC_VER
#pragma pack(pop)
#endif

//a bbcEventHandler-derived class represents a type of event. (it used to do something a bit different,
//hence the misnomer.) each event type stores data of a fixed size. each event type also has an id, a
//string exactly 4 characters in length.
//
//see bbcModelCpuEventHandler for an example.
class bbcEventHandler {
public:
	const unsigned data_size;
	const char *const id;

	virtual ~bbcEventHandler()=0;

	//a new event has been created -- fill in any details necessary.
	virtual void FillEventData(void *event) const=0;

	//append string representation of this event to the end of *output. use "\n" for line separator.
	//output is actually std::vector<char> * -- void * to cut down on includes.
	virtual void AppendEventStr(const bbcEventInfo *info,const void *event,int indent,void *output) const=0;
protected:
	bbcEventHandler(unsigned data_size,const char *id);
private:
	bbcEventHandler(const bbcEventHandler &);
	bbcEventHandler &operator=(const bbcEventHandler &);

	char real_id_[5];
};

class bbcEventRecord {
public:
	bbcEventRecord();
	~bbcEventRecord();

	//clear the event record.
	void Clear();

	//add an event of the specified type.
	void *AddEvent(const bbcEventHandler *handler);

	//save all events to a file. the file is (at the mo') written in chunks with fwrite.
	//calls the given progress function as the save progresses.
	typedef bool (*ProgressFn)(const char *msg,int percent_done,unsigned amount_written,void *context);
	bool SaveEventsText(const char *file_name,ProgressFn progress_fn,void *context);

private:
	// cut down on includes
	struct Impl;

	struct Chunk {
		t65::byte *data;
		unsigned end;
	};

//	typedef std::list<Chunk> ChunkList;
//	ChunkList chunks_;
	Impl *i_;

	Chunk *AddChunk();

	bbcEventRecord(const bbcEventRecord &);
	bbcEventRecord &operator=(const bbcEventRecord &);
};

#endif
