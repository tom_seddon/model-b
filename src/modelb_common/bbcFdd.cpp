#include "pch.h"
#include "bbcFdd.h"
#include <algorithm>
#include <stdio.h>

bbcDiscSide::bbcDiscSide():
#ifdef bbcFDD_ENABLE_NOPYTHON
is_side2(false),
#endif
is_dd(false),
changed(false)
{
}

//////////////////////////////////////////////////////////////////////////
//
const int bbcFdd::other_side_of[num_drives]={
	2,
	3,
	0,
	1,
};

//////////////////////////////////////////////////////////////////////////
//
bbcDiscSide *bbcFdd::drives[bbcFdd::num_drives]={0,};

//////////////////////////////////////////////////////////////////////////
// Initialises the disc drives stuff
void bbcFdd::Init() {
	for(unsigned i=0;i<num_drives;++i) {
		drives[i]=0;
	}
}

//////////////////////////////////////////////////////////////////////////
// Shuts down drive stuff
void bbcFdd::Shutdown() {
	for(unsigned i=0;i<num_drives;++i) {
		delete drives[i];
		drives[i]=0;
	}
}

//////////////////////////////////////////////////////////////////////////
// set drive contents directly
// used by mbpython
void bbcFdd::SetDriveContents(int drive,const bbcDiscSide *contents) {
	BASSERT(drive>=0&&drive<num_drives);
	if(drives[drive]) {
		delete drives[drive];
	}
	drives[drive]=0;
	if(contents) {
		drives[drive]=new bbcDiscSide(*contents);
	}
}

//////////////////////////////////////////////////////////////////////////
// Load disc image into the specified drives.
// TODO only SSD supported at the mo'.

bool bbcFdd::LoadDsDiscImage(int drive,int sectors_per_track,DiscDensity density,
	const std::vector<t65::byte> &disc_image)
{
	bool ok;
	int side2drive=other_side_of[drive];

	ok=LoadDiscSide(drive,sectors_per_track,0,sectors_per_track*256*2,density,disc_image);
	ok=ok&&LoadDiscSide(side2drive,sectors_per_track,sectors_per_track*256,
		sectors_per_track*256*2,density,disc_image);
	if(ok) {
		drives[side2drive]->is_side2=true;
	}
	return ok;
}

bool bbcFdd::LoadSsDiscImage(int drive,int sectors_per_track,DiscDensity density,
	const std::vector<t65::byte> &disc_image)
{
	bool ok=LoadDiscSide(drive,sectors_per_track,0,sectors_per_track*256,density,disc_image);
	return ok;
}

bool bbcFdd::LoadDiscSide(int drive,int sectors_per_track,
	unsigned start,unsigned track_size,DiscDensity density,const std::vector<t65::byte> &side_image)
{
	unsigned i,j;
	BASSERT(density!=DENSITY_ANY);
	BASSERT(DriveStatus(drive)==DRIVE_EMPTY);
	BASSERT(sectors_per_track==10||sectors_per_track==16||sectors_per_track==18);
	if(side_image.size()%256!=0) {
		fprintf(stderr,"image is not multiple of 256 bytes in length\n");
		return false;
	}

	//Basic disc geometry setup
	bbcDiscSide *side=new bbcDiscSide;
	side->tracks.resize(80);
	for(j=0;j<side->tracks.size();++j) {
		bbcDiscTrack *track=&side->tracks.at(j);
		track->sectors.resize(sectors_per_track);
		for(unsigned k=0;k<track->sectors.size();++k) {
			track->sectors.at(k).data.resize(256);
		}
	}
	
	//Read the data
	unsigned src=0;
	for(unsigned track=0;track<side->tracks.size()&&src<side_image.size();++track) {
		src=start+track*track_size;
		for(int sector=0;sector<sectors_per_track&&src<side_image.size();++sector) {
			printf("%s: drive %d track %d sector %d: starts at +%u (+0x%08X)\n",__FUNCTION__,drive,track,sector,src,src);
			for(i=0;i<256;++i) {
				side->tracks[track].sectors[sector].data[i]=side_image.at(src);
				++src;
			}
		}
	}

	//Set DD flag
	side->is_dd=density==DENSITY_DOUBLE;
	
	drives[drive]=side;

	return true;
}

//////////////////////////////////////////////////////////////////////////
//
bbcFdd::DriveStatusType bbcFdd::DriveStatus(int drive) {
	if(!drives[drive]) {
		return DRIVE_EMPTY;
	}
	if(drives[drive]->is_side2) {
		BASSERT(!drives[other_side_of[drive]]->is_side2);
		return DRIVE_SIDE2;//use other_side_of for true status!
	}
	if(drives[drive]->changed) {
		return DRIVE_CHANGED;
	}
	return DRIVE_LOADED;
}

void bbcFdd::UnloadDiscImage(int drive) {
	if(drives[drive]) {
		bbcDiscSide **other=&drives[other_side_of[drive]];
		if(*other&&(*other)->is_side2) {
			//joined
			delete *other;
			*other=0;
		}
		delete drives[drive];
		drives[drive]=0;
	}
}

void bbcFdd::SetDriveChangedStatus(int drive,bool changed) {
	BASSERT(drives[drive]);
	if(drives[drive]->is_side2) {
		BASSERT(drives[other_side_of[drive]]);
		drives[other_side_of[drive]]->changed=changed;
	} else {
		drives[drive]->changed=changed;
	}
}

int bbcFdd::DriveFromFdcAddress(int fdc_drive,int fdc_side) {
	BASSERT(fdc_drive==0||fdc_drive==1);
	BASSERT(fdc_side==0||fdc_side==1);
	
	int drive=fdc_drive;
	if(fdc_side==1) {
		drive=other_side_of[drive];
	}
	return drive;
}

bool bbcFdd::GetByteFdc(int fdc_drive,int fdc_side,int fdc_track,
	int fdc_sector,int fdc_offset,DiscDensity density,t65::byte *result)
{
	t65::byte *ptr=FdcBytePtr(fdc_drive,fdc_side,fdc_track,fdc_sector,fdc_offset,density);
	if(!ptr) {
		return false;
	}
	*result=*ptr;
	return true;
}

bool bbcFdd::SetByteFdc(int fdc_drive,int fdc_side,int fdc_track,
	int fdc_sector,int fdc_offset,DiscDensity density,t65::byte value)
{
	t65::byte *ptr=FdcBytePtr(fdc_drive,fdc_side,fdc_track,fdc_sector,fdc_offset,density);
	if(!ptr) {
		return false;
	}
	*ptr=value;
	SetDriveChangedStatus(DriveFromFdcAddress(fdc_drive,fdc_side),true);
	return true;
}

t65::byte *bbcFdd::FdcBytePtr(int fdc_drive,int fdc_side,int fdc_track,
	int fdc_sector,int fdc_offset,DiscDensity density)
{
	int drive=DriveFromFdcAddress(fdc_drive,fdc_side);
	bbcDiscSide *side=drives[drive];
	if(!side) {
		return 0;
	}
	if(density!=DENSITY_ANY&&(density==DENSITY_DOUBLE)!=side->is_dd) {
		return 0;
	}
	if(fdc_track<0||unsigned(fdc_track)>=side->tracks.size()) {
		return 0;
	}
	bbcDiscTrack *track=&side->tracks[fdc_track];
	if(fdc_sector<0||unsigned(fdc_sector)>=track->sectors.size()) {
		return 0;
	}
	bbcDiscSector *sector=&track->sectors[fdc_sector];
	if(fdc_offset<0||unsigned(fdc_offset)>=sector->data.size())
	{
		return 0;
	}
	return &sector->data[fdc_offset];
}

bool bbcFdd::IsOkAddressFdc(int fdc_drive,int fdc_side,int fdc_track,
	int fdc_sector,DiscDensity density)
{
	t65::byte *p=FdcBytePtr(fdc_drive,fdc_side,fdc_track,fdc_sector,0,density);
	return !!p;
}

bool bbcFdd::IsDd(int drive) {
	return drives[drive]&&drives[drive]->is_dd;
}

bool bbcFdd::IsDs(int drive) {
	int other=other_side_of[drive];
	return drives[other]&&drives[other]->is_side2;
}

bool bbcFdd::GetSsDiscImage(int drive,std::vector<t65::byte> *disc_image) {
	BASSERT(disc_image);
	BASSERT(drives[drive]);

	//disc_image->reserve(80*18*256);//max likely
	const bbcDiscSide *side=drives[drive];
	for(unsigned i=0;i<side->tracks.size();++i) {
		const bbcDiscTrack *track=&side->tracks[i];

		for(unsigned j=0;j<track->sectors.size();++j) {
			const bbcDiscSector *sector=&track->sectors[j];

			std::copy(sector->data.begin(),sector->data.end(),std::back_inserter(*disc_image));
		}
	}
	return true;
}

bool bbcFdd::GetDsDiscImage(int top,int bot,std::vector<t65::byte> *disc_image) {
//	std::vector<t65::byte> topraw;
//	std::vector<t65::byte> botraw;
//	bbcDiscSide *topside,*botside;

//	if(!GetSsDiscImage(top,&topraw)||!GetSsDiscImage(bot,&botraw)) {
//		return false;
//	}
//	if(topraw.size()!=botraw.size()) {
//		//can't happen now, but will remind me when I put DD stuff in.
//		BASSERT(false);
//		return false;
//	}
	//can assume 80 tracks 10 sectors 256 bytes
	disc_image->clear();

	for(unsigned i=0;i<80;++i) {
		for(unsigned dr=0;dr<2;++dr) {
			const bbcDiscSide *side=drives[dr?bot:top];
			const bbcDiscTrack *track=&side->tracks[i];
			
			for(unsigned j=0;j<track->sectors.size();++j) {
				const bbcDiscSector *sector=&track->sectors[j];
				
				std::copy(sector->data.begin(),sector->data.end(),std::back_inserter(*disc_image));
			}
		}
	}

//	for(int i=0;i<80;++i) {
//		//top side
//
//		std::copy(&topside[i*2560],&topside[(i+1)*2560],
//			std::back_inserter(*disc_image));
//		std::copy(&botside[i*2560],&botside[(i+1)*2560],
//			std::back_inserter(*disc_image));
//	}
	return true;
}

//Retrieve number of sectors per track
//returns false if that track is not valid.
//bool bbcFdd::GetSectorsPerTrack(int fdc_drive,int fdc_side,int fdc_track,int *num_sectors) {
//	if(!IsOkAddressFdc(fdc_drive,fdc_side,fdc_track,0,DENSITY_ANY)) {
//		return false;
//	} else {
//		int drive=DriveFromFdcAddress(fdc_drive,fdc_side);
//		*num_sectors=int(drives[drive]->tracks[fdc_track]->sectors.size());
//	}
//}

static const bbcSaveStateItemId disc_side_save_state_item_id("discside");

#pragma pack(push,1)

struct bbcDiscSideState1 {
	bbcSaveStateByte is_dd;
	bbcSaveStateByte is_side2;
	bbcSaveStateByte num_tracks;
	bbcSaveStateByte pad;
};
#pragma pack(pop)

static const char check_disc_side_state_size[sizeof(bbcDiscSideState1)==4]={0,};

template<class T>
static T *Add(std::vector<t65::byte> *vec) {
	unsigned old_size=vec->size();
	vec->resize(old_size+sizeof(T));
	return reinterpret_cast<T *>(&vec->at(old_size));
}

bbcSaveState::LoadStatus bbcFdd::LoadSaveState(const bbcSaveState *save_state) {
	for(int i=0;i<num_drives;++i) {
		delete drives[i];

		const bbcSaveStateItem *item=save_state->GetItem(disc_side_save_state_item_id,i);
		if(!item) {
			continue;
		}

		switch(item->version) {
		default:
			return bbcSaveState::LS_BAD_VERSION;

		case 1:
			{
				drives[i]=new bbcDiscSide;

				const bbcDiscSideState1 *side=reinterpret_cast<const bbcDiscSideState1 *>(&item->data[0]);
				const void *data_end=&item->data[item->data.size()];

				drives[i]->changed=false;
				drives[i]->is_dd=!!side->is_dd;
				drives[i]->is_side2=!!side->is_side2;

				drives[i]->tracks.resize(side->num_tracks);

				unsigned sectors_data_size=0;

				const t65::dword *size=reinterpret_cast<const t65::dword *>(side+1);
				for(unsigned track=0;track<side->num_tracks;++track) {
					if(size+1>data_end) {
						return bbcSaveState::LS_BAD_DATA;
					}

					drives[i]->tracks[track].sectors.resize(*size);
					++size;

					for(unsigned sector=0;sector<drives[i]->tracks[track].sectors.size();++sector) {
						if(size+1>data_end) {
							return bbcSaveState::LS_BAD_DATA;
						}

						drives[i]->tracks[track].sectors[sector].data.resize(*size);
						sectors_data_size+=*size;
						++size;
					}
				}

				const t65::byte *sector_data=reinterpret_cast<const t65::byte *>(size);
				if(sector_data+sectors_data_size!=data_end) {
					return bbcSaveState::LS_BAD_DATA;
				}

				for(unsigned track=0;track<drives[i]->tracks.size();++track) {
					for(unsigned sector=0;sector<drives[i]->tracks[track].sectors.size();++sector) {
						bbcDiscSector *dest=&drives[i]->tracks[track].sectors[sector];
						memcpy(&dest->data[0],sector_data,dest->data.size());
					}
				}
			}
			break;
		}
	}

	return bbcSaveState::LS_OK;
}

bbcSaveState::SaveStatus bbcFdd::SaveSaveState(bbcSaveState *save_state) {
	for(int i=0;i<num_drives;++i) {
		if(!drives[i]) {
			continue;
		}

		std::vector<t65::byte> data;

		bbcDiscSideState1 *side=Add<bbcDiscSideState1>(&data);
		side->is_dd=drives[i]->is_dd;
		side->is_side2=drives[i]->is_side2;
		side->num_tracks=(bbcSaveStateByte)(drives[i]->tracks.size());

		unsigned sectors_data_size=0;

		for(unsigned track=0;track<drives[i]->tracks.size();++track) {
			t65::dword *sectors_per_track=Add<t65::dword>(&data);
			*sectors_per_track=drives[i]->tracks[track].sectors.size();

			for(unsigned sector=0;sector<drives[i]->tracks[track].sectors.size();++track) {
				t65::dword *bytes_per_sector=Add<t65::dword>(&data);
				*bytes_per_sector=drives[i]->tracks[track].sectors[sector].data.size();
				sectors_data_size+=*bytes_per_sector;
			}
		}

		data.reserve(data.size()+sectors_data_size);
		for(unsigned track=0;track<drives[i]->tracks.size();++track) {
			for(unsigned sector=0;sector<drives[i]->tracks.size();++track) {
				const bbcDiscSector *s=&drives[i]->tracks[track].sectors[sector];
				std::copy(s->data.begin(),s->data.end(),std::back_inserter(data));
			}
		}

		save_state->SetItem(disc_side_save_state_item_id,i,1,&data[0],data.size());
	}

	return bbcSaveState::SS_OK;
}
