#ifndef COMMON_H_
#define COMMON_H_

#include <t65.h>

#ifdef bbcDEBUG_ENABLE

void BAssertMsg(const char *file,int line,const char *what);

#define BASSERT(X)\
	((X)?void(0):(BAssertMsg(__FILE__,__LINE__,#X),SoftBreak(),void(0)))

#else

#define BASSERT(X)

#endif

//using t65::byte;
//using t65::word;
//using t65::Word;

//////////////////////////////////////////////////////////////////////////

enum {
	IRQ_SYSVIA=1<<0,
	IRQ_USERVIA=1<<1,
	IRQ_ACIA=1<<2,

	IRQ_NMI=1<<31,
};

//////////////////////////////////////////////////////////////////////////
// Beeb debug features
#ifdef bbcDEBUG_ENABLE

//Enable the trace event facilities. Event recording is always activated (eventually
//will be used for keylogging etc.), but this activates tracing of cpu events for something
//a bit like the DEBUG_TRACE but better.
#define bbcDEBUG_TRACE_EVENTS

//Enable the debug panel stuff
#define bbcDEBUG_PANELS

//Misc video debugging features:
//	Show VIA T1 timeouts
//	Show All feature
//	Frame Logging
#define bbcDEBUG_VIDEO

//Define if trying to make Uridium work again :)
//#define bbcHACKING_URIDIUM

//define for voluminous logging from the 1770.
//#define bbc1770_LOG

//Enable bbcPrintf
#define bbcDEBUG_PRINTF

//Enable instruction counts
#define t65_COUNT_INSTRUCTION_FREQUENCIES

//Enable MMIO counts
#define bbcDEBUG_COUNT_MMIO_ACCESSES

//Enable save state
#define bbcENABLE_SAVE_STATE

//enable Master 128 emulation
#define bbcENABLE_M128

//Enable beeb ice debugger
#define bbcENABLE_BEEB_ICE

#endif

//////////////////////////////////////////////////////////////////////////
//
//
#ifdef bbcENABLE_BEEB_ICE
#define t65_DEBUGGER
#endif

//////////////////////////////////////////////////////////////////////////
// Enable/disable profiling
#ifdef bbcDEBUG_ENABLE
#define bbcENABLE_PROFILER
#endif

//////////////////////////////////////////////////////////////////////////
// automated save state testing
#ifdef bbcENABLE_SAVE_STATE
//#define bbcTEST_SAVE_STATE
#endif

//////////////////////////////////////////////////////////////////////////
// Half-done stuff

// Serial/tape system
#ifdef bbcDEBUG_ENABLE
//#define bbcSERIAL_ENABLE
#endif

#define bbcUPDATE_IN_BBCMODEL

//////////////////////////////////////////////////////////////////////////
#ifdef bbcDEBUG_PRINTF

int bbcPrintf(const char *fmt,...);

#else

static inline int bbcPrintf(...) {
	return 0;
}

#endif

#endif

