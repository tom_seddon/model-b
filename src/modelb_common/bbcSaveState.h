#ifndef BBCSAVESTATE_H_
#define BBCSAVESTATE_H_

struct bbcSaveStateHdr;

#pragma pack(push,1)

typedef t65::byte bbcSaveStateByte;
typedef t65::word bbcSaveStateWord;
typedef t65::dword bbcSaveStateDword;
typedef t65::qword bbcSaveStateQword;

typedef t65::int8 bbcSaveStateInt8;
typedef t65::int16 bbcSaveStateInt16;
typedef t65::int32 bbcSaveStateInt32;
typedef t65::int64 bbcSaveStateInt64;

// this is always available -- cuts down on #ifdef madness.
union bbcSaveStateItemId {
	char id_text[8];
	t65::qword id;

	bbcSaveStateItemId();
	bbcSaveStateItemId(const char *id_text);
	bbcSaveStateItemId(t65::qword id);

	bool operator==(const bbcSaveStateItemId &rhs) const;
};

#pragma pack(pop)

struct bbcSaveStateItem {
	bbcSaveStateItemId id;
	t65::dword sub_type;
	t65::dword version;
	std::vector<t65::byte> data;
};

class bbcSaveState
{
public:
	enum LoadStatus
	{
		LS_OK=0,
		LS_BAD_VERSION,
		LS_MISSING,
		LS_BAD_DATA,
		LS_NOT_IMPLEMENTED,

		LS_NUM,
	};

	enum SaveStatus
	{
		SS_OK=0,
		SS_NOT_IMPLEMENTED,

		SS_NUM,
	};

	bbcSaveState();
	~bbcSaveState();

	static const char *GetLoadStatusText(LoadStatus s);
	static const char *GetSaveStatusText(SaveStatus s);

	void Reset();
	bool Load(const std::vector<t65::byte> &contents);
	void Save(std::vector<t65::byte> *contents) const;

	int GetNumItems() const;
	bbcSaveStateItem *GetItem(int idx);
	const bbcSaveStateItem *GetItem(int idx) const;
	const bbcSaveStateItem *GetItem(const bbcSaveStateItemId &id,t65::dword sub_type) const;
	int GetItemIdx(const bbcSaveStateItemId &id,t65::dword sub_type) const;

	void SetItem(const bbcSaveStateItemId &id,t65::dword sub_type,t65::dword version,const void *data,unsigned data_size);
	void *PrepareItem(const bbcSaveStateItemId &id,t65::dword sub_type,t65::dword version,unsigned data_size);

	template<class T>
	void SetItem(const bbcSaveStateItemId &id,t65::dword sub_type,t65::dword version,const T *data) {
		this->SetItem(id,sub_type,version,data,sizeof(T));
	}

	template<class T>
	T *PrepareItem(const bbcSaveStateItemId &id,t65::dword sub_type,t65::dword version) {
		void *data=this->PrepareItem(id,sub_type,version,sizeof(T));
		return static_cast<T *>(data);
	}

	// helper for loading fixed size structs
	// not all calling code has been updated to use this!
	template<class T>
	const T *FindItem(const bbcSaveStateItemId &id,t65::dword sub_type,t65::dword version,LoadStatus *status) const {
		return static_cast<const T *>(this->FindItemInternal(id,sub_type,version,sizeof(T),status));
	}
protected:
private:
	std::vector<bbcSaveStateItem *> items_;

	static const char *const load_status_texts_[LS_NUM];
	static const char *const save_status_texts_[SS_NUM];

	const void *FindItemInternal(const bbcSaveStateItemId &id,t65::dword sub_type,t65::dword version,size_t size,
		LoadStatus *status) const;
	
	bbcSaveState(const bbcSaveState &);
	bbcSaveState &operator=(const bbcSaveState &);
};

inline const bbcSaveStateItem *bbcSaveState::GetItem(const bbcSaveStateItemId &id,t65::dword sub_type) const {
	return this->GetItem(this->GetItemIdx(id,sub_type));
}

struct bbcSaveStateHandler {
	bbcSaveState::LoadStatus (*load_fn)(const bbcSaveState *save_state);
	bbcSaveState::SaveStatus (*save_fn)(bbcSaveState *save_state);
};

#endif
