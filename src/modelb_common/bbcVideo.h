#ifndef bbcVIDEO_H_
#define bbcVIDEO_H_

#include <HostGfxShared.h>
#include "bbcSaveState.h"

class bbcVideo {
public:
	static const int min_buffer_width;
	static const int max_buffer_width;
	static const int min_buffer_height;
	static const int max_buffer_height;

	static t65::byte BlankPixel();

#ifdef bbcDEBUG_VIDEO
	static bool show_all_;//don't change this by hand
	static unsigned show_all_offset_;
	static void SetShowAll(bool new_show_all);
	static void LogFrames(const char *filename,int num_frames,bool with_disassembly);
	static int num_log_frames_left_;
	static FILE *log_frames_h_;
	static bool show_t1_timeouts_;
#endif

	static void SetBufferDetails(int width,int height,const HostGfxBufferFormat *fmt);
	static void ClearBuffer();

	enum Registers {
		ULA_BEGIN=0,

		//Palette -- 16 registers
		ULA_PALETTE=ULA_BEGIN,

		//ULA control register
		ULA_CTRL=ULA_PALETTE+16,

		ULA_END,

		//CRTC -- 18 registers
		CRTC_BEGIN=ULA_END,
		CRTC_BASE=CRTC_BEGIN,
		CRTC_END=CRTC_BEGIN+18,

		//Invalid (ignored)
		DUMMY,

		NUM_REGS,
	};
	
	static void Init();
	static void Shutdown();

	static void Reset();

	static void Write(t65::byte reg,t65::byte value);

	static void SetScreenWrapSize(t65::word new_wrap_size);

	//If entire screen was just finished, returns pointer to the new bbcVideoBuffer.
	//If needed, this can be referred to, until Update returns a new buffer.
	static const HostGfxBuffer *DisplayBuffer();
	static void Update();

	static t65::byte Read6845(t65::word addr);

	static void Write6845(t65::word addr,t65::byte val);
	static void WriteUla(t65::word addr,t65::byte val);
	static void WritePalette(t65::word addr,t65::byte val);

	//Sets pointer to where video RAM is.
	static void SetVideoRam(const t65::byte *video_ram);

	static void DebugDump(FILE *h);

	//
	static bbcSaveState::LoadStatus LoadSaveState(const bbcSaveState *save_state);
	static bbcSaveState::SaveStatus SaveSaveState(bbcSaveState *save_state);
protected:
private:
private:
	struct SaveState1;

	static int buffer_width_;
	static int buffer_height_;
	static int buffer_size_;
	static HostGfxBufferFormat buffer_format_;
	static t65::byte *buffer_;
	static HostGfxBuffer video_buffers_[];
	static bool video_buffer_cleared_[];//one for each of video_buffers_[]
	static int current_video_buffer_;

	static const HostGfxBuffer *display_video_buffer_;
	static const t65::byte *video_ram_;

	static char reg_names_[NUM_REGS][100];
	static t65::byte cpu_current_[NUM_REGS];//as the CPU sees them
	static t65::byte video_current_[NUM_REGS];//as the video system sees them
	static int last_flush_at_;

	//Display list entry
	struct DlEntry {
		int when;
		t65::byte reg;
		t65::byte val;
	};

	//This will always be sufficient.
	//Max throughput is STA, one write/4 cycles; max cycle count per scanline
	//is 512 (256 x 2 for slow 6845 clock) so 128 writes.
	enum {
		dlq_num_entries=128,
	};

	static DlEntry dlq_[dlq_num_entries];
	static int dlq_read_;
	static int dlq_write_;

	//runs from 'start_idx' until 'stop_cycles' is reached; returns new index.
	static int DlqRun(int stop_cycles,int start_idx);

	enum VideoRegFlags {
		READABLE=256,
		WRITABLE=512,
	};

	static t65::byte reg_6845_select_;
	static const int reg_6845_flags_[18];

	static t65::word read_address_;

	//dest -- where in buffer to draw
	//src -- where in beeb ram to draw from
	//subline -- which subline to draw
	static void RenderFastClockScanline(t65::byte *dest,t65::word src);
	static void RenderSlowClockScanline(t65::byte *dest,t65::word src);
	static void RenderTeletextScanline(t65::byte *dest,t65::word src);

	static void RebuildFastClockPaletteTables();
	static void RebuildSlowClockPaletteTables();
	
	//6845 state
	static int cur_line_,cur_subline_;
//	static int vsync_left_;
	static t65::word framebuffer_base_;


	//Index of scanline -- -ve after vsync, +ve before
	static int cur_scanline_;

	//Set according to R5 as appropriate -- number of blank scanlines to insert
	//right now.
	static int num_blank_scanlines_;

	static int frame_count_;

	static t65::word wrap_size_;
	static bool palette_dirty_3bit_;
	static bool palette_dirty_slow_;
	static bool palette_dirty_fast_;

	static int DlqNext(int v);
	static void SetupTables();

	static t65::qword expand_1bit_slow_[16];
	static t65::dword expand_1bit_fast_[16];
	static t65::byte rearrange_2bit_[256];
	static t65::byte rearrange_4bit_[256];

	static bool is_teletext_frame_;
	//Index of last line on which was displayed the top half of double height characters.
	static int last_double_height_line_;
	
	//hblank value (in fast-6845-clock units) at which the
	//beeb's display will be on the left of the virtal screen.
	//98 for bitmap modes
	//
	static int hblank_leftaligned_;

	bbcVideo();
	bbcVideo(const bbcVideo &);
	bbcVideo &operator=(const bbcVideo &);

	static void Rebuild3bitPalette();

	static t65::byte palette_3bit_[16];
	static t65::byte palette_colours_[8];

	static void DrawCursor();

	//static t65::byte pixel_fixed_bits_;
};

inline int bbcVideo::DlqNext(int v) {
	return (v+1)%dlq_num_entries;
}

inline void bbcVideo::SetScreenWrapSize(t65::word new_wrap_size) {
	wrap_size_=new_wrap_size;
}

inline void bbcVideo::SetVideoRam(const t65::byte *video_ram) {
	video_ram_=video_ram;
}

inline t65::byte bbcVideo::BlankPixel() {
#ifdef bbcDEBUG_ENABLE
	return t65::byte(buffer_format_.fixed_bits|4);
#else
	return t65::byte(buffer_format_.fixed_bits);
#endif
}

inline const HostGfxBuffer *bbcVideo::DisplayBuffer() {
	return display_video_buffer_;
}

#endif
