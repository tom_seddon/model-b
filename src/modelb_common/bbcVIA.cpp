#include "pch.h"
#include "bbcVIA.h"
#include "bbcComputer.h"
#include "bbcDebugPanel.h"
#include "bbcSystemVIA.h"
#ifdef bbcDEBUG_VIDEO
#include "bbcVideo.h"
#endif
//#include "UserVIA.h"
#include <limits.h>
#include <string>
#include "bbcProfiler.h"

#ifdef VIA_METRICS
#define VIA_METRIC(N) N
#else
#define VIA_METRIC(N)
#endif

#ifdef bbcDEBUG_VIDEO
//logging for sphere of destiny
//very, very slow.
//#define BASTARD_SPHERE_OF_DESTINY
#endif

#ifdef BASTARD_SPHERE_OF_DESTINY
//And don't think I don't mean it!!
static void LogSOD(const char *fmt,...) {
	va_list v;
	va_start(v,fmt);
	vfprintf(bbcVideo::log_frames_h_,fmt,v);
	va_end(v);
}
#define SODLOG(X)\
	if(bbcVideo::log_frames_h_&&this==&bbc_system_via) {\
		LogSOD X;\
	}
#else
#define SODLOG(X)
#endif

//New (enhanced, we hope) VIA stuff
#define VIA_NEW_STUFF

//const int via_timer_fire=-1;
//const int via_timer_nextstop_delta=2;

//Reset takes 2 1MHz cycles. So, don't fire the interrupt and do the reset until
//timer reaches -4. Actually, this isn't quite right, because the timing is (I
//guess) more like:
//
// -0:	irq next cycle
// -1:	irq, reset L
// -2:	reset H
// -3:	timer is now reset and counting down.
//
//So, the IRQ occurs 4 cycles too late. I don't consider this a major issue:
//
//1.Most timing-dependent stuff polls the timers rather than relying on exact
//IRQ timings. (And polling gives the correct results.)
//2.Timers used for screen fx when IRQs enabled (Elite, etc.) -- in this case,
//there'll be some smudge anyway
//3.Timers used for precise screen fx -- won't be quite right on the emulator
//anyway.
//4.The VIAs are kind of broken anyway :) [See Super Invaders, Sphere Of Destiny]
//
//TODO something out there will rely on this :)

// -4 -- war broken
// -3 --
// -2 -- war works, super invaders works (!), pedro flaky
// -1 --
// 0 -- war works
const int via_timer_fire=-4;//-4;
const int via_timer_nextstop_delta=1;

//Basically, the timer values were always one out. I think this is related
//to my use of the adjusted bbcComputer::cycles in the update function.
//
//TODO For obvious reasons, this breaks pulse counting mode -- after setting T2, reading the
//timer gives back the written value +1.
static const int via_timer_value_adjust=2;//added to timer before adjusting
static const int via_timer_result_adjust=0;//added to result of timer read

bbcVIA::bbcVIA(unsigned int_mask,const char *type_name):
//type_name_(type_name),
t1_count(0),
t2_count(0),
int_mask_(int_mask)
{
	BASSERT(strlen(type_name)==4);
	strcpy(type_name_,type_name);
}

/////////////////////////////////////////////////////////////////////////
// update: update timers
//
// "c" is the current cycle counter

//#define BEEBIT_UPDATE_STYLE

BP(static bbcProfileRecord bpr_via_update("VIA Update",0));

void bbcVIA::Update() {
	BPR(bpr_via_update);

	const int cycles=bbcComputer::Cycles2MHz();//11/12/2004 -- 
	int t1cycles=cycles-last_update_cycles_;
	int t2cycles=t1cycles;
	
	// Update Timer 1
	t1c_-=t1cycles;
	if(t1c_<via_timer_fire) {
		int adjust;
#ifdef BEEBIT_UPDATE_STYLE
		//this is more like the way beebit does it
		// attempt to fix skirmish and war and pedro. [14/7/2003]
		switch(acr_>>6) {
		case 0://0x00
			//op dis & 1-shot
			if(!t1_shot_) {
				this->T1();
				t1_shot_=true;
			}
			adjust=65536;
			break;
		case 1://0x40
			//op dis & free
			if(!t1_shot_) {
				this->T1();
			}
			adjust=t1l_.w;
			break;
		case 2://0x80
			//op en & 1-shot
			if(!t1_shot_) {
				this->T1();
				t1_shot_=true;
			}
			this->pb_|=0x80;
			this->WrittenPb();
			adjust=65536;
			break;
		case 3://0xC0
			this->pb_^=0x80;
			this->WrittenPb();
			if(!t1_shot_) {
				this->T1();
			}
			adjust=t1l_.w;
			break;
		}
#else
		if(acr_&0x40) {
			//free run
			SODLOG(("SystemVIA: bbcVIA::Update: this->T1()\n",cycles));
			if(!t1_shot_)
			{
				//this is what beebit does -- see mail from mike borcherds
				T1();               // indicate time out
				//t1_shot_=true;
			}

			if(acr_&ACR_T1_PB7) {
				pb_^=0x80;       // Toggle PB7 if ACR says so
				this->WrittenPb();
			}
			adjust=t1l_.w;
		} else {
			//1-shot
			SODLOG(("SystemVIA: bbcVIA::Update: this->T1(): t1_shot=%s",t1_shot_?"yes":"no"));
			if(!t1_shot_) {
				T1();
				t1_shot_=true;
				SODLOG((" (t1_shot now=true)"));
			}
			
			if(acr_&ACR_T1_PB7) {
				pb_|=0x80;       // Set PB7 if ACR says so
				this->WrittenPb();
			}
			
			SODLOG(("\n"));
			//adjust=65536;//<-- original
			//This is correct. Checked against a real Beeb 12/2/04. See also diagram in data sheet, page 8.
			adjust=t1l_.w;
		}
#endif
		adjust=adjust*2+4;
		while(t1c_<via_timer_fire) {
			t1c_+=adjust;
		}
	}

	// Timer 1 always decrements, even after it has timed out and interrupted
	// and stuff.
	
	// Update Timer 2, somewhat more simple
	if(acr_&32) {
		// PB6 pulse counting
	} else {
		t2c_-=t2cycles;
		// T2 one-shot
		if(!t2_shot_&&t2c_<via_timer_fire) {
			// Timed out
			T2();
			t2_shot_=true;
		}
		// The counter continues to decrement even after it has timed out.
	}
	//< is correct here.
	//(See Gnome Ranger & Psycastria.)
	//But where did the +4 go?! :)
	//
	//checked against real Beeb 12/2/04, same results with VIATEST on tests.ssd...
	if(t2c_<via_timer_fire) {
		t2c_+=131072;
	}
	
	//this->UpdateIfr();
	last_update_cycles_=cycles;
	ScheduleNextStop();
}

/////////////////////////////////////////////////////////////////////////
// Assert interrupts: CA1, CB1, CA2, CB2
//
// The parameter to each is the pulse being activated on that pin -- false
// for inactive, true for active. The peripheral control register determines
// which edge generates an interrupt.
//
// AUGp404 has more details, though it's a bit sketchy. I'm pretty sure I've
// got this bit wrong, as well, though it seems to work acceptably most of
// the time.
//
// removed clearing of interrupt flag, I believe the interrupt sticks around until
// acknowledged by reading/writing the relevant port [3/17/2003]

void bbcVIA::CA1(bool edge) {
	//    if((edge?1:0)==(PCR&1)) {
	if((edge && (pcr_&0x01)) || (!edge && !(pcr_&0x01))) {
		this->ReadingPa();
		ira_=pa_;
		ifr_|=CA1_INT;
	}
	UpdateIfr();
}


void bbcVIA::CB1(bool edge) {
	//    if((edge?0x10:0)==(PCR&0x10)) {
	if((edge && (pcr_&0x10)) || (!edge && !(pcr_&0x10))) {
		this->ReadingPb();
		irb_=pb_;
		ifr_|=CB1_INT;
	}
	UpdateIfr();
}


void bbcVIA::CA2(bool edge) {
	if(!(pcr_&0x08)) {
		// input mode. Bit 1 determines its behaviour in relation to [OI]RA.
		// Bit 2 is the point of interest.
		//        if((edge?4:0)==(PCR&4)) {
		if((edge && (pcr_&4)) || (!edge && !(pcr_&4))) {
			ifr_|=CA2_INT;
		}
		UpdateIfr();
	}
	// I believe that in output mode incoming pulses are ignored.
}


void bbcVIA::CB2(bool edge) {
	if(!(pcr_&0x80)) {
		// input mode. Bit 5 determines its behaviour in relation to [OI]RB.
		// Bit 6 is the point of interest.
		//        if((edge?64:0)==(PCR&64)) {
		if((edge && (pcr_&0x40)) || (!edge && !(pcr_&0x40))) {
			ifr_|=CB2_INT;
		}
		UpdateIfr();
	}
	// I believe that incoming pulses are ignored in output mode.
}

/////////////////////////////////////////////////////////////////////////
// Write: writes a VIA register
//
// 'a' is the address to read from -- only the bottom 4 bits are
// significant -- and v is the value to write to that address.

void bbcVIA::Write(t65::word addr,t65::byte v) {//,int *cycles_counter) {
	Update();
	switch(addr&0x0F) {
	case 0x00:      // ORB
		// Write: MPU writes output level for output bits. Input: MPU
		// writes into ORB but no effect on pin level 'til DDRB change
		if(pcr_&0x80) {
			// CB2 is an output and goes low on ORB write
			if((pcr_&(0x20+0x40))==0) {
//#ifndef VIA_NEW_STUFF
				ifr_&=~CB2_INT;      // handshake mode
//#endif
			} else if((pcr_&(0x20+0x40))==0x20) {
				// pulse output mode, not emulated
			}
		} else {
			// CB2 is an input, cleared in "normal" mode
			if(!(pcr_&0x40)) {
				ifr_&=~CB2_INT;      // normalmode
			}
		}
		ifr_&=~CB1_INT;          // always clear CB1...?
		UpdateIfr();
		orb_=v;
		pb_=(pb_&~ddrb_)|(orb_&ddrb_);
		this->WrittenPb();
		break;
	case 0x01:      // ORA
		if(pcr_&0x08) {
			// CA2 is an output; goes low on write of ORA
			if((pcr_&(0x02+0x04))==0) {
//#ifndef VIA_NEW_STUFF
				ifr_&=~CA2_INT;      // handshake mode
//#endif
			} else if((pcr_&(0x02+0x04))==0x02) {
				// pulse output mode, not emulated
			}
		} else {
			// CA2 is an input, cleared in "normal" mode
			if(!(pcr_&0x04)) {
				ifr_&=~CA2_INT;
			}
		}
		ifr_&=~CA1_INT;          // always clear CA1...?
		UpdateIfr();
		// Fall through to ORA (non-handshaking)
	case 0x0F:
		ora_=v;
		// Only pins configured as outputs get to PA
		pa_=(pa_&~ddra_)|(v&ddra_);
		this->WrittenPa();
		break;
	case 0x02:      // DDRB
		ddrb_=v;
		if(acr_&ACR_PB_LATCHING) {
			// input latching on
			pb_=(pb_&~ddrb_)|(orb_&ddrb_);
			this->WrittenPb();
		}
		break;
	case 0x03:
		ddra_=v;
		if(acr_&ACR_PA_LATCHING) {
			// intput latching on
			pa_=(pa_&~ddra_)|(ora_&ddra_);
			this->WrittenPa();
		}
		break;
	case 0x04:      // Timer 1 low-order counter
		// Fall through to Timer 1 low-order latches
	case 0x06:      // Timer 1 low-order latches
		t1l_.l=v;
		break;
	case 0x05:      // Timer 1 high-order counter
		t1l_.h=v;
		t1c_=t1l_.w;
		t1c_*=2;
		t1_shot_=false;
		SODLOG(("SystemVIA: Write $%02X to T1L-H (now $%04X), t1 counting & t1_shot=false now\n",v,t1l_.w));
		ifr_&=~T1_INT;
		UpdateIfr();
		if((acr_&(0x80+0x40))==0x80) {
			pb_&=~0x80;      // PB7 cleared if PB7 xoring on
		}
		ScheduleNextStop();
		break;
	case 0x07:      // Timer 1 high-order latches
		t1l_.h=v;
		
		//Clear T1 int flag. AUG doesn't tell you this :)
		//Fixed Skirmish. It loads values into latch, acknowledging T1 int that way. Without this,
		//it gets another T1 IRQ straight away, and its most excellent "don't sync to the vblank" 
		//style makes it all go rather wrong.
		if(acr_&0x40) {
			ifr_&=~T1_INT;
			this->UpdateIfr();
		}
		break;
	case 0x08:      // Timer 2 low-order counter
		t2l_l_=v;
		break;
	case 0x09:      // Timer 2 high-order counter
		t2c_=(v<<8)|t2l_l_;
		t2c_*=2;
		t2_shot_=false;
		ifr_&=~T2_INT;
		this->UpdateIfr();
		this->ScheduleNextStop();
		break;
	case 0x0A:      // Shift register
		sr_=v;
		break;
	case 0x0B:      // Auxiliary Control Register
		acr_=v;
		this->ScheduleNextStop();//might have changed timer modes.
		break;
	case 0x0C:      // Peripheral Control Register
		pcr_=v;
		// Check C[AB]2 values if in manual mode
		if((pcr_&(0x80+0x40))==0x80+0x40) {
			// CB2 is output and manual -- set CB2 line as per bit 5
			// don't think you can do an IRQ this way [18/3/2003]
#ifndef VIA_NEW_STUFF
			if(pcr_&0x20) {
				ifr_|=CB2_INT;
			} else {
				ifr_&=~CB2_INT;
			}
			UpdateIfr();
#endif
		}
		if((pcr_&(0x08+0x04))==0x08+0x04) {
			// don't think you can do an IRQ this way [18/3/2003]
#ifndef VIA_NEW_STUFF
			// CA2 is output and manual -- set CA2 line as per bit 2
			if(pcr_&0x02) {
				ifr_|=CA2_INT;
			} else {
				ifr_&=~CA2_INT;
			}
			UpdateIfr();
#endif
		}
		break;
	case 0x0D:      // Interrupt Flag Register
		ifr_&=~(v&0x7F);
//		if(!(ifr_&T1_INT)) {
//			t1_shot_=false;// 13/2/04 guess :( 14/2/03 checked on beeb, ain't so
//		}
		UpdateIfr();
		break;
	case 0x0E:      // Interrupt Enable Register
		if(v&0x80) {
			ier_|=v&0x7F;
		} else {
			ier_&=~(v&0x7F);
		}
		UpdateIfr();
		this->ScheduleNextStop();//it may have changed T[12] modes.
		break;
	}
}

/////////////////////////////////////////////////////////////////////////
// Read: read a VIA register
//
// Returns the value read. 'a' is the address to read from. Only the bottom
// 4 bits are used.

t65::byte bbcVIA::Read(t65::word addr) {//,int *cycles_counter) {
	t65::byte r;
//	int c=*cycles_counter-last_update_cycles;
	Update();
	switch(addr&0x0F) {
	case 0x00:
		// IRB
		if((pcr_&(0x80+0x40))==0) {
//#ifndef VIA_NEW_STUFF
			// don't think you can do an IRQ this way [18/3/2003]
			ifr_&=~CB2_INT;          // cb2 not independent
//#endif
		}
		ifr_&=~CB1_INT;              // always clear CB1
		UpdateIfr();
		this->ReadingPb();
		// Output bits always come from ORB
		r=orb_&ddrb_;
		if(acr_&ACR_PB_LATCHING) {
			r|=irb_&~ddrb_;           // latching on
		} else {
			r|=pb_&~ddrb_;            // latching off
		}
		return r;
	case 0x01:      
		// IRA
		if((pcr_&(0x08+0x02))==0) {
//#ifndef VIA_NEW_STUFF
//			// don't think you can do an IRQ this way [18/3/2003]
			ifr_&=~CA2_INT;          // CA2 not independent
//#endif
		}
		ifr_&=~CA1_INT;              // always clear CA1
		UpdateIfr();
		// Fall through to IRA (non-handshaking)
	case 0x0F:      
		// IRA (non-handshaking)
		this->ReadingPa();
		return (acr_&ACR_PA_LATCHING)?ira_:pa_;
	case 0x02:      
		// DDRB
		return ddrb_;
	case 0x03:      
		// DDRA
		return ddra_;
	case 0x04:      
		// Timer 1 low-order counter
		ifr_&=~T1_INT;
		UpdateIfr();  // T1 interrupt flag is reset
		if(t1c_<-via_timer_value_adjust) {
			return 0xFF;
		} else {
			return (((t1c_+via_timer_value_adjust)/2)&0xFF)+via_timer_result_adjust;
		}
		break;
	case 0x05:      
		// Timer 1 high-order counter
		if(t1c_<-via_timer_value_adjust) {
			return 0xFF;
		} else {
			return (t65::byte)((((t1c_+via_timer_value_adjust)/2)>>8)+via_timer_result_adjust);
		}
		break;
	case 0x06:      
		// Timer 1 low-order latches
		return t1l_.l;
	case 0x07:      
		// Timer 1 high-order latches
		return t1l_.h;
	case 0x08:
		// Timer 2 low-order counter
		ifr_&=~T2_INT;
		UpdateIfr();  // T2 interrupt flag is reset
		return (((t2c_+via_timer_value_adjust)/2)&0xFF)+via_timer_result_adjust;
	case 0x09:      
		// Timer 2 high-order counter
		return (t65::byte)((((t2c_+via_timer_value_adjust)/2)>>8)+via_timer_result_adjust);
	case 0x0A:      
		// Shift register
		return sr_;
	case 0x0B:      
		// Auxiliary Control Register
		return acr_;
	case 0x0C:      
		// Peripheral Control Register
		return pcr_;
	case 0x0D:      
		// Interrupt Flag Register
		return ifr_;
	case 0x0E:      
		// Interrupt Enable Register
		return ier_|0x80;        // bit 7 always set on a read
	}
	// This point will never be reached, but g++ still complains about it.
	return 0;
}

/////////////////////////////////////////////////////////////////////////
// Reset VIA to power-on defaults

void bbcVIA::Reset() {
	//TODO FF is a hack. This gives different readings from on a Beeb.
	ora_=0xFF;
	ira_=0xFF;
	orb_=0xFF;
	irb_=0xFF;
	pa_=0xFF;
	pb_=0xFF;
	ddra_=0;
	ddrb_=0;
	t1_shot_=false;
	t2_shot_=false;
	sr_=0xff;
	acr_=0;
	pcr_=0;
	ifr_=0;
	ier_=0;
	//t1l_.w=0;
	t1l_.l=250;
	t1l_.h=202;
	t2l_l_=0;
	t1c_=0xFFFF;//0x666;
	t2c_=0xFFFF;//0x666

	last_update_cycles_=bbcComputer::Cycles2MHz();
//	ca1_signal_=false;
//	ca2_signal_=false;
//	cb1_signal_=false;
//	cb2_signal_=false;

//	VIAType::ResetHandler();
}

/////////////////////////////////////////////////////////////////////////
// Private functions
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Assert T[12] interrupts. These are only activated by the timers timing
// out, so they are not accessible outside of the class.
// Furthermore, they always happen.

void bbcVIA::T1() {
	//t1_shot_=true;//guesswork :) 13/2/04
	ifr_|=T1_INT;
	UpdateIfr();
	VIA_METRIC(++t1_count);
//	if(this==&bbc_system_via&&!(bbcComputer::irq_flags_&int_mask_)) {
//		int brk=0;
//	}
}


void bbcVIA::T2() {
	ifr_|=T2_INT;
	UpdateIfr();
	VIA_METRIC(++t2_count);
}

/////////////////////////////////////////////////////////////////////////
// Update Interrupt Flag Register

#define CHK(X) ((en&(1<<(X)))&&!(last_ifr_&(1<<(X))))

void bbcVIA::UpdateIfr() {
	//if any outstanding interrupts, set bit7.
//	if(ifr_&0x7f) {
//		ifr_|=0x80;
//	} else {
//		ifr_&=0x7f;
//	}

//	//if any new enabled interrupts, do irq.
//	t65::byte en=ifr_&ier_&0x7f;
//	if(CHK(7)||CHK(6)||CHK(5)||CHK(4)||CHK(3)||CHK(2)||CHK(1)||CHK(0)) {
//		bbcComputer::irq_flags_|=int_mask_;
//	} else {
//		bbcComputer::irq_flags_&=~int_mask_;
//	}
//
//	last_ifr_=en;

	//if any outstanding enabled interrupts, do IRQ.
	if(ifr_&ier_&0x7f) {
		ifr_|=0x80;
		bbcComputer::irq_flags_|=int_mask_;
		//Stop next time the while() in the main loop is encountered, i.e. after
		//this instruction has run.
		// 2/2/04 bbcComputer::SetNextIrqStop(bbcComputer::Cycles2MHz());
		//it's pointless setting the next stop point, because if IRQs are enabled
		//then the CPU will stop straight away because of the interrupt mask.
		//(but consider reinstating this when unifying both bbcModel instruction loops)
	} else {
		ifr_&=0x7f;
		bbcComputer::irq_flags_&=~int_mask_;
	}
}

//////////////////////////////////////////////////////////////////////////
// This only uses the timers that might cause IRQs, because they are set
// up to do so in the ACR and the relevant IRQ is enabled.
//
// Improves 3D Pool in debug build.
inline void bbcVIA::ScheduleNextStop() {//TODO inline?! Don't think that will work when all its callers are defined first.
	int t1c_left=INT_MAX;
	int t2c_left=INT_MAX;
	bool stop_needed=false;

	if(((acr_&64)||!t1_shot_)&&(ier_&T1_INT)) {
		//T1 free run
		//T1 1 shot and not timed out yet.
		t1c_left=t1c_;
		stop_needed=true;
	}
	if((!(acr_&32)&&(!t2_shot_))&&(ier_&T2_INT)) {
		//T2 1 shot and not timed out yet.
		t2c_left=t2c_;
		stop_needed=true;
	}

	if(stop_needed) {
		int smaller_timer;
		smaller_timer=t1c_left<t2c_left?t1c_left:t2c_left;
		if(smaller_timer<via_timer_nextstop_delta) {
			smaller_timer=via_timer_nextstop_delta;
		} else {
			//smaller_timer+=via_timer_nextstop_delta;//TEST (war/pedro) 4/11/2003
		}
		BASSERT(smaller_timer>0);
		bbcComputer::SetNextIrqStopDelta(smaller_timer);
	}
//	if(smaller_timer==0) {
//		smaller_timer=1;
//	}

	//3/11/2003 test (fix adc?)
	this->UpdateIfr();
}

/*

inline void bbcVIA::Init() {
	VIAType::InitHandler();
}
*/

void bbcVIA::DebugDumpAllFormats(FILE *h,t65::byte b) {
	fprintf(h,"0x%02X (%d) %%",b,b);
	for(unsigned i=0;i<8;++i) {
		if(b&(1<<(7-i))) {
			fprintf(h,"1");
		} else {
			fprintf(h,"0");
		}
	}
	fprintf(h,"\n");
}


void bbcVIA::DebugDumpPort(FILE *h,char port,t65::byte outreg,t65::byte inreg,
	t65::byte p,t65::byte ddr)
{
	char caption[200];
	_snprintf(caption,sizeof caption,"Port %c status: ",port);
	size_t n=strlen(caption);
	std::string blank=std::string(n,' ');
	fprintf(h,"%s: %sOR%c : ",this->type_name_,caption,port);
	DebugDumpAllFormats(h,outreg);
	fprintf(h,"%s: %sIR%c : ",this->type_name_,blank.c_str(),port);
	DebugDumpAllFormats(h,inreg);
	fprintf(h,"%s: %sP%c  : ",this->type_name_,blank.c_str(),port);
	DebugDumpAllFormats(h,p);
	fprintf(h,"%s: %sDDR%c: ",this->type_name_,blank.c_str(),port);
	DebugDumpAllFormats(h,ddr);
}


void bbcVIA::DebugDump(FILE *h) {
	static const char *t1_control_txt[4]={
		"T1 1-shot",
		"T1 free run",
		"T1 1-shot, PB7 toggle",
		"T1 free run, PB7 toggle",
	};
	static const char *t2_control_txt[2]={
		"T2 1-shot",
		"T2 PB6 pulse counting",
	};
	static const char *sr_control_txt[8]={
		"Disabled",
		"Shift in under control of T2",
		"Shift in under control of O2 (?)",
		"Shift in under control of ext. clk",
		"Shift out free running at T2 rate",
		"Shift out under control of T2",
		"Shift out under control of O2 (?)",
		"Shift out under control of ext. clk",
	};
	static const char *cab_control_txt[8]={
		"input negative active edge",
		"independent interrupt input negative edge",
		"input positive active edge",
		"independent interrupt input positive edge",
		"handshake output",
		"pulse output",
		"low output",
		"high output",
	};
	static const char *cab_int_control_txt[2]={
		"negative active edge",
		"positive active edge",
	};

	DebugDumpPort(h,'A',ora_,ira_,pa_,ddra_);
	DebugDumpPort(h,'B',orb_,irb_,pb_,ddrb_);
	static const int t_div=2;
	/////////////////////////////////////////////////////////////////////
	//Timers
	fprintf(h,"%s: Timer 1: T1C=0x%04X (%u) T1L=0x%04X (%u)\n",
		this->type_name_,t1c_/t_div,t1c_/t_div,t1l_.w,t1l_.w);
	fprintf(h,"%s: Timer 2: T2C=0x%04X (%u) T2L_L=0x%02X (%u)\n",
		this->type_name_,t2c_/t_div,t2c_/t_div,t2l_l_,t2l_l_);
	fprintf(h,"%s: T1shot=%s T2shot=%s\n",
		this->type_name_,t1_shot_?"yes":"no",t2_shot_?"yes":"no");

	/////////////////////////////////////////////////////////////////////
	//ACR
	fprintf(h,"%s: ACR=",this->type_name_);
	DebugDumpAllFormats(h,acr_);
	fprintf(h,"%s:\tT1 control: %s\n",
		this->type_name_,t1_control_txt[acr_>>6]);
	fprintf(h,"%s:\tT2 control: %s\n",
		this->type_name_,t2_control_txt[(acr_>>5)&1]);
	fprintf(h,"%s:\tShift reg control: %s\n",
		this->type_name_,sr_control_txt[(acr_>>2)&7]);
	fprintf(h,"%s:\tLatch enable: %s\n",
		this->type_name_,acr_&1?"yes":"no");

	/////////////////////////////////////////////////////////////////////
	//PCR
	fprintf(h,"%s: PCR=",this->type_name_);
	DebugDumpAllFormats(h,pcr_);
	fprintf(h,"%s:\tCB2 control: %s\n",
		this->type_name_,cab_control_txt[(pcr_>>5)&7]);
	fprintf(h,"%s:\tCB1 interrupt control: %s\n",
		this->type_name_,cab_int_control_txt[(pcr_>>4)&1]);
	fprintf(h,"%s:\tCA2 control: %s\n",
		this->type_name_,cab_control_txt[(pcr_>>1)&7]);
	fprintf(h,"%s:\tCA1 interrupt control: %s\n",
		this->type_name_,cab_int_control_txt[pcr_&1]);
	
	/////////////////////////////////////////////////////////////////////
	//IER/EFR
	fprintf(h,"%s: IER=",this->type_name_);
	DebugDumpAllFormats(h,ier_);
	fprintf(h,"%s: IFR=",this->type_name_);
	DebugDumpAllFormats(h,ifr_);

	/////////////////////////////////////////////////////////////////////
	//Other info
	fprintf(h,"%s: last_update_cycles=%d\n",this->type_name_,last_update_cycles_);

	/////////////////////////////////////////////////////////////////////
	//Metrics
	VIA_METRIC(fprintf(h,"%s: t1_count=%u t2_count=%u\n",
		this->type_name_,this->t1_count,this->t2_count));
}

#ifdef VIA_METRICS

void bbcVIA::ResetMetrics() {
	this->t1_count=0;
	this->t2_count=0;
}
#endif

void bbcVIA::WrittenPa() {
}

void bbcVIA::WrittenPb() {
}

void bbcVIA::ReadingPa() {
}

void bbcVIA::ReadingPb() {
}

#ifdef bbcDEBUG_PANELS
//////////////////////////////////////////////////////////////////////////
//0000000000111111111122222222223333333333444444444455555555556666666666
//0123456789012345678901234567890123456789012345678901234567890123456789
//PCR=%111 1 111 1
//PA=&XX IRA=&XX ORA=&XX DDR=IIIIIIII
//PB=&XX IRA=&XX ORA=&XX DDR=OOOOOOOO
//ACR=%11 1 111 1 1
//T1=&XXXX YYYYY T1L=&XXXX YYYYY
//T2=&XXXX YYYYY T2LL=&XX YYY
//IFR=%11111111
//IER=%11111111
//

void bbcVIA::InitDebugPanel(bbcDebugPanel *panel) {
	panel->SetSize(40,8);
	panel->Print(0,0,"PCR=%111 1 111 1");
	//                0         1         2         3
	//                0123456789012345678901234567890123456789
	panel->Print(0,1,"PA=$XX IRA=$XX ORA=$XX DDR=IIIIIIII 0=in");
	panel->Print(0,2,"PB=$XX IRA=$XX ORA=$XX DDR=OOOOOOOO 1=ou");
	panel->Print(0,3,"ACR=b11 1 111 1 1");
	panel->Print(0,4,"T1=$XXXX YYYYY T1L=$XXXX YYYYY");
	panel->Print(0,5,"T2=$XXXX YYYYY T2LL=$XX YYY");
	panel->Print(0,6,"IFR=b11111111");
	panel->Print(0,7,"IER=b11111111");
	//                0123456789012345678901234567890123456789
	//                0         1         2         3
}

//TODO piss take. this is just stupid...
static const char hex_digits[]="0123456789ABCDEF";
static const int powers_of_10[]={1,10,100,1000,10000,};

#define NYB(V,N) (hex_digits[((V)>>((N)*4))&0xf])
#define BIT(V,N) ("01"[((V)>>N)&1])
#define DIG(V,N) ((V)/(powers_of_10[N])%10+'0')

#define HEX(V,X) p[X]=NYB((V),1);p[(X)+1]=NYB((V),0)
#define HEXW(V,X) HEX(((V)>>8),(X));HEX((V),(X)+2)
#define DEC(V,X) p[X]=DIG(V,2);p[(X)+1]=DIG(V,1);p[(X)+2]=DIG(V,0)
#define DECW(V,X) p[X]=DIG(V,4);p[(X)+1]=DIG(V,3);p[(X)+2]=DIG(V,2);p[(X)+3]=DIG(V,1);p[(X)+4]=DIG(V,0)
#define BIN(V,X)\
	p[(X)+0]=BIT((V),7);p[(X)+1]=BIT((V),6);p[(X)+2]=BIT((V),5);p[(X)+3]=BIT((V),4);\
	p[(X)+4]=BIT((V),3);p[(X)+5]=BIT((V),2);p[(X)+6]=BIT((V),1);p[(X)+7]=BIT((V),0)

void bbcVIA::UpdateDebugPanel(bbcDebugPanel *panel) {
	char *p,*const *lines=panel->Lines();

	p=lines[0];
	p[5]=BIT(pcr_,7);p[6]=BIT(pcr_,6);p[7]=BIT(pcr_,5);
	p[9]=BIT(pcr_,4);
	p[11]=BIT(pcr_,3);p[12]=BIT(pcr_,2);p[13]=BIT(pcr_,1);
	p[15]=BIT(pcr_,0);
	p=lines[1];
	HEX(pa_,4); HEX(ira_,12); HEX(ora_,20); BIN(ddra_,27);
	p=lines[2];
	HEX(pb_,4); HEX(irb_,12); HEX(orb_,20); BIN(ddrb_,27);
	p=lines[3];
	p[5]=BIT(acr_,7);p[6]=BIT(acr_,6);
	p[8]=BIT(acr_,5);
	p[10]=BIT(acr_,4);p[11]=BIT(acr_,3);p[12]=BIT(acr_,2);
	p[14]=BIT(acr_,1);
	p[16]=BIT(acr_,0);
	p=lines[4];
	HEXW(t1c_/2,4);DECW(t1c_/2,9);HEXW(t1l_.w,20);DECW(t1l_.w,25);
	p=lines[5];
	HEXW(t2c_/2,4);DECW(t2c_/2,9);HEX(t2l_l_,21);DEC(t2l_l_,24);
	p=lines[6];
	BIN(ifr_,5);
	p=lines[7];
	BIN(ier_,5);
}
#endif

#ifdef bbcDEBUG_TRACE_EVENTS
void bbcVIA::FillTraceState(TraceState *state) {
	state->acr=acr_;
	state->ier=ier_;
	state->ifr=ifr_;
	state->t1c=t1c_;
	state->t1l=t1l_;
	state->t2c=t2c_;
}
#endif

// this should be readable by eye in a hex dump!
t65::dword bbcVIA::GetSaveStateSubType() const {
#ifdef t65_WRONG_ENDIAN
#error
#else
	return t65::dword(type_name_[0])|(t65::dword(type_name_[1])<<8)|(t65::dword(type_name_[2])<<16)|
		(t65::dword(type_name_[3])<<24);
#endif
}

//////////////////////////////////////////////////////////////////////////

static const bbcSaveStateItemId via_save_state_item_id("6522_via");

#pragma pack(push,1)
struct bbcViaSaveState1 {
    t65::byte orb;
	t65::byte irb;
	t65::byte ora;
	t65::byte ira;
    t65::byte ddra;
	t65::byte ddrb;
	t65::byte t1_shot;
	t65::byte t2_shot;
	t65::int32 t1c;
	t65::int32 t2c;
	t65::Word t1l;
    t65::byte t2l_l;
    t65::byte sr;
    t65::byte acr;
    t65::byte pcr;
    t65::byte ier;
	t65::byte ifr;
	t65::byte pa;
	t65::byte pb;
	t65::int32 last_update_cycles;
};
#pragma pack(pop)

bbcSaveState::LoadStatus bbcVIA::LoadSaveState(const bbcSaveState *save_state) {
	// save common
	const bbcSaveStateItem *item=save_state->GetItem(via_save_state_item_id,this->GetSaveStateSubType());
	if(!item) {
		return bbcSaveState::LS_MISSING;
	}

	switch(item->version) {
	default:
		return bbcSaveState::LS_BAD_VERSION;

	case 1:
		if(item->data.size()!=sizeof(bbcViaSaveState1)) {
			return bbcSaveState::LS_BAD_DATA;
		} else {
			const bbcViaSaveState1 *data=reinterpret_cast<const bbcViaSaveState1 *>(&item->data[0]);

			orb_=data->orb;
			irb_=data->irb;
			ora_=data->ora;
			ira_=data->ira;
			ddra_=data->ddra;
			ddrb_=data->ddrb;
			t1_shot_=!!data->t1_shot;
			t2_shot_=!!data->t2_shot;
			t1c_=data->t1c;
			t2c_=data->t2c;
			t1l_=data->t1l;
			t2l_l_=data->t2l_l;
			sr_=data->sr;
			acr_=data->acr;
			pcr_=data->pcr;
			ier_=data->ier;
			ifr_=data->ifr;
			pa_=data->pa;
			pb_=data->pb;
			last_update_cycles_=data->last_update_cycles;
		}
		break;
	}

	// save extra
	bbcSaveState::LoadStatus status=this->LoadExtraSaveState(save_state);
	if(status!=bbcSaveState::LS_OK) {
		return status;
	}

	return bbcSaveState::LS_NOT_IMPLEMENTED;
}

bbcSaveState::SaveStatus bbcVIA::SaveSaveState(bbcSaveState *save_state) const {
	// save common
	bbcViaSaveState1 data;

	data.orb=orb_;
	data.irb=irb_;
	data.ora=ora_;
	data.ira=ira_;
	data.ddra=ddra_;
	data.ddrb=ddrb_;
	data.t1_shot=t1_shot_;
	data.t2_shot=t2_shot_;
	data.t1c=t1c_;
	data.t2c=t2c_;
	data.t1l=t1l_;
	data.t2l_l=t2l_l_;
	data.sr=sr_;
	data.acr=acr_;
	data.pcr=pcr_;
	data.ier=ier_;
	data.ifr=ifr_;
	data.pa=pa_;
	data.pb=pb_;
	data.last_update_cycles=last_update_cycles_;

	save_state->SetItem(via_save_state_item_id,this->GetSaveStateSubType(),1,&data);

	// save extra
	bbcSaveState::SaveStatus status=this->SaveExtraSaveState(save_state);
	if(status!=bbcSaveState::SS_OK) {
		return status;
	}

	return bbcSaveState::SS_NOT_IMPLEMENTED;
}

bbcSaveState::LoadStatus bbcVIA::LoadExtraSaveState(const bbcSaveState *save_state) {
	(void)save_state;

	return bbcSaveState::LS_OK;
}

bbcSaveState::SaveStatus bbcVIA::SaveExtraSaveState(bbcSaveState *save_state) const {
	(void)save_state;

	return bbcSaveState::SS_OK;
}
