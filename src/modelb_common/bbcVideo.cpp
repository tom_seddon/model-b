#include "pch.h"
#include "bbcVideo.h"
#include "bbcComputer.h"
#include "bbcTeletextFont.h"
#include "bbcSystemVIA.h"
#ifdef bbcDEBUG_VIDEO
#include <stdio.h>
#endif
#ifdef bbcDEBUG_VIDEO
#include "bbcUserVIA.h"
#endif
#include <stdarg.h>
#include <utility>

//Pointless logging.
//#define bbcVIDEO_LOG_DLQ

//if defined, do the WAR fudge.
#define bbcVARIABLE_FRAME_TIMINGS

//Whether to update registers BEFORE or AFTER the scanline...
//I was hoping this would fix Revs (mode change being 1 line out), but it didn't!
//To be investigated!
//#define bbcVIDEO_UPDATE_REGISTERS_BEFORE

//If defined, vsync is negative -- vsync start is -CA1, vsync end is +CA1
//Otherwise, +CA1 then -CA1.
//(Nothing seems to work properly if defined -- to be investigated!)
//#define VSYNC_NEGATIVE

//using t65::byte;

enum {
	video_buffer_alignment=16,//must be power of two! (16 at least for MOVAPS on P3+)
};

enum {
	num_video_buffers=3,
};

#ifdef bbcDEBUG_VIDEO
static void VlogDebug(const char *fmt,...) {
	va_list v;
	va_start(v,fmt);
	vfprintf(bbcVideo::log_frames_h_,fmt,v);
	va_end(v);
}
#define LOGFRAME(X)\
	if(bbcVideo::log_frames_h_) {\
		VlogDebug X;\
	}
#else
#define LOGFRAME(X)
#endif

const int bbcVideo::min_buffer_width=640;
const int bbcVideo::max_buffer_width=800;
const int bbcVideo::min_buffer_height=272;
const int bbcVideo::max_buffer_height=312;

//std::vector<t65::byte> bbcVideo::buffers_vector_;
t65::byte *bbcVideo::buffer_=0;
int bbcVideo::buffer_width_=0;
int bbcVideo::buffer_height_=0;
int bbcVideo::buffer_size_=0;//This is set up during Init.
HostGfxBuffer bbcVideo::video_buffers_[num_video_buffers];
bool bbcVideo::video_buffer_cleared_[num_video_buffers];
int bbcVideo::current_video_buffer_=0;
const HostGfxBuffer *bbcVideo::display_video_buffer_=0;
HostGfxBufferFormat bbcVideo::buffer_format_;

#define END_OF_BUFFER (&buffer_[bbcVideo::buffer_size_])


int bbcVideo::frame_count_;
//bool bbcVideo::buffer_dirty;
char bbcVideo::reg_names_[NUM_REGS][100];
t65::byte bbcVideo::cpu_current_[NUM_REGS];//used by CPU
t65::byte bbcVideo::video_current_[NUM_REGS];//used to render
int bbcVideo::last_flush_at_;
bbcVideo::DlEntry bbcVideo::dlq_[dlq_num_entries];
int bbcVideo::dlq_read_;
int bbcVideo::dlq_write_;
//void *bbcVideo::dest_;
t65::word bbcVideo::read_address_;
t65::qword bbcVideo::expand_1bit_slow_[16];
t65::dword bbcVideo::expand_1bit_fast_[16];
t65::byte bbcVideo::rearrange_2bit_[256];
t65::byte bbcVideo::rearrange_4bit_[256];

t65::byte bbcVideo::reg_6845_select_;
int bbcVideo::cur_line_;
int bbcVideo::cur_subline_;
//int bbcVideo::vsync_left_;
int bbcVideo::cur_scanline_;
int bbcVideo::num_blank_scanlines_;
t65::word bbcVideo::framebuffer_base_;
t65::word bbcVideo::wrap_size_;
bool bbcVideo::palette_dirty_slow_;
bool bbcVideo::palette_dirty_fast_;
bool bbcVideo::palette_dirty_3bit_;
bool bbcVideo::is_teletext_frame_;
t65::byte bbcVideo::palette_3bit_[16];
t65::byte bbcVideo::palette_colours_[8];
int bbcVideo::hblank_leftaligned_;
int bbcVideo::last_double_height_line_;
const t65::byte *bbcVideo::video_ram_;
#ifdef bbcDEBUG_VIDEO
bool bbcVideo::show_all_;
int bbcVideo::num_log_frames_left_;
FILE *bbcVideo::log_frames_h_;
bool bbcVideo::show_t1_timeouts_;
unsigned bbcVideo::show_all_offset_;
#endif
//t65::byte bbcVideo::pixel_fixed_bits_;

//////////////////////////////////////////////////////////////////////////
// Bits in the ULA control register
enum {
	ULA_MASTERCURSORSIZE=128,
	ULA_CURSORWIDTH=64|32,
	ULA_6845FASTCLOCK=16,
	ULA_CHARSPERLINE=8|4,
	ULA_TELETEXT=2,
	ULA_FLASHCOLOUR=1,

	ULA_CURSORWIDTH_SHIFT=5,
	ULA_CHARSPERLINE_SHIFT=2,	
};

//////////////////////////////////////////////////////////////////////////
// Accessors for the register set
#define CRTC(N) (video_current_[CRTC_BASE+(N)])
#define PALETTE(N) (video_current_[ULA_PALETTE+(N)])
#define ULACTRL() (video_current_[ULA_CTRL])

//////////////////////////////////////////////////////////////////////////
// Definitions for read/write/bitness of each CRTC register
#define MASK(N) ((1<<(N))-1)
#define RW(N) bbcVideo::WRITABLE|bbcVideo::READABLE|MASK(N)
#define WO(N) bbcVideo::WRITABLE|MASK(N)

const int bbcVideo::reg_6845_flags_[18]={
	WO(8),//R0
	WO(8),//R1
	WO(8),//R2
	WO(8),//R3
	WO(7),//R4
	WO(5),//R5
	WO(7),//R6
	WO(7),//R7
	WO(8),//R8
	WO(5),//R9
	WO(7),//R10
	WO(5),//R11
	RW(6),//R12
	RW(8),//R13
	RW(6),//R14
	RW(8),//R15
	WO(6),//R16
	WO(8),//R17
};

//////////////////////////////////////////////////////////////////////////
// bbcVideo::Init
//
// Initialises everything.
void bbcVideo::Init() {
	frame_count_=0;

	SetupTables();

	HostGfxBufferFormat_Reset(&buffer_format_);
	for(unsigned i=0;i<num_video_buffers;++i) {
		HostGfxBuffer_Init(&video_buffers_[i]);
		video_buffer_cleared_[i]=false;
	}

	for(unsigned i=0;i<NUM_REGS;++i) {
		if(i==DUMMY) {
			strcpy(reg_names_[i],"dummy");
		} else if(i==ULA_CTRL) {
			strcpy(reg_names_[i],"ula ctrl");
		} else if(i>=ULA_PALETTE&&i<ULA_PALETTE+16) {
			_snprintf(reg_names_[i],sizeof reg_names_[i],"ula pal #%d",i-ULA_PALETTE);
		} else if(i>=CRTC_BASE&&i<CRTC_BASE+18) {
			_snprintf(reg_names_[i],sizeof reg_names_[i],"crtc r%d",i-CRTC_BASE);
		}
	}

#ifdef bbcDEBUG_VIDEO
	show_all_=false;
	num_log_frames_left_=0;
	log_frames_h_=0;
	show_t1_timeouts_=false;
	show_all_offset_=0;
#endif

	//pixel_fixed_bits_=0;

	bbcVideo::Reset();
}

//////////////////////////////////////////////////////////////////////////
// Shutdown
//
// Frees any allocated memory.
void bbcVideo::Shutdown() {
	for(unsigned i=0;i<num_video_buffers;++i) {
		HostGfxBuffer_Shutdown(&video_buffers_[i]);
	}
}

//////////////////////////////////////////////////////////////////////////
// SetupTables
//
// Sets up some tables to swizzle mode 1/2/5/8 data about.
void bbcVideo::SetupTables() {
	//Swizzles memory bits so that with >> and & can get ULA palette indices.
	//ABABABAB ends up as AAAABBBB
	static const t65::byte tbl4bit[8]={0x80,0x08,0x40,0x04,0x20,0x02,0x10,0x01,};
	//ABCDABCD ends up as ABABCDCD
	static const t65::byte tbl2bit[8]={0x80,0x40,0x08,0x04,0x20,0x10,0x02,0x01,};
	unsigned i;
	for(i=0;i<256;++i) {
		t65::byte *b2=&rearrange_2bit_[i];
		t65::byte *b4=&rearrange_4bit_[i];

		*b2=*b4=0;
		for(unsigned j=0;j<8;++j) {
			bool set=!!(i&(1<<(7-j)));
			if(set) {
				*b2|=tbl2bit[j];
				*b4|=tbl4bit[j];
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Rebuild(Fast|Slow)ClockPaletteTables
//
// Rebuilds palette tables for decoding modes 0/3/4/6
//
// Each table is just for half a byte to increase coherency of memory accesses.

// #define RPT_FAST_DO(N) \
// 	do {\
// 		if(i&(1<<(N))) {\
// 			expand_1bit_fast_[i]|=palette_3bit_[8]<<((3-(N))*8);\
// 		} else {\
// 			expand_1bit_fast_[i]|=palette_3bit_[0]<<((3-(N))*8);\
// 		}\
// 	} while(false)

#define RPT_FAST_DO(N)\
	(expand_1bit_fast_[i]|=palette_3bit_[i&(1<<(N))?8:0]<<((3-(N))*8))

//TODO I suspect some of these qword casts aren't needed!!
// #define RPT_SLOW_DO(N) \
// 	do {\
// 		if(i&(1<<(N))) {\
// 			expand_1bit_slow_[i]|=t65::qword((palette_3bit_[8]<<8)|palette_3bit_[8])<<((t65::qword(3)-(N))*t65::qword(16));\
// 		} else {\
// 			expand_1bit_slow_[i]|=t65::qword((palette_3bit_[0]<<8)|palette_3bit_[0])<<((t65::qword(3)-(N))*t65::qword(16));\
// 		}\
// 	} while(false)

#define RPT_SLOW_DO(N)\
	(expand_1bit_slow_[i]|=t65::qword((palette_3bit_[i&(1<<(N))?8:0])|(palette_3bit_[i&(1<<(N))?8:0]<<8))<<((t65::qword(3)-(N))*t65::qword(16)))

void bbcVideo::RebuildFastClockPaletteTables() {
	unsigned i;
#ifdef bbcDEBUG_VIDEO
	if(show_all_) {
		for(i=0;i<16;++i) {
			t65::qword v=0;
			t65::qword bits=0x07;
			for(unsigned j=0;j<8;++j) {
				if(i&(1<<j)) {
					v|=bits<<t65::qword((7-j)*8);
				}
			}
			expand_1bit_fast_[i]=t65::dword(v);
		}
	} else
#endif
	{
		for(i=0;i<16;++i) {
			expand_1bit_fast_[i]=0;
			RPT_FAST_DO(3);
			RPT_FAST_DO(2);
			RPT_FAST_DO(1);
			RPT_FAST_DO(0);
		}
	}
	palette_dirty_fast_=false;
}

void bbcVideo::RebuildSlowClockPaletteTables() {
	unsigned i;
#ifdef bbcDEBUG_VIDEO
	if(show_all_) {
		for(i=0;i<16;++i) {
			t65::qword v=0;
			t65::qword bits=0x0707;
			for(unsigned j=0;j<4;++j) {
				if(i&(1<<j)) {
					v|=bits<<t65::qword((3-j)*16);
				}
			}
			BASSERT(i==0||v);
			expand_1bit_slow_[i]=v;
		}
	} else
#endif
	{
		for(i=0;i<16;++i) {
			expand_1bit_slow_[i]=0;
			RPT_SLOW_DO(3);
			RPT_SLOW_DO(2);
			RPT_SLOW_DO(1);
			RPT_SLOW_DO(0);
		}
	}
	palette_dirty_slow_=false;
}

//////////////////////////////////////////////////////////////////////////
// Reset
//
// Resets the video system. Fairly arbitrarily, it starts in mode 0.
//
void bbcVideo::Reset() {
	//See aug p462 -- this is mode 0
	CRTC(0)=0x7f;
	CRTC(1)=0x50;
	CRTC(2)=0x62;
	CRTC(3)=0x08|(0x02<<4);
	CRTC(4)=0x26;
	CRTC(5)=0x00;
	CRTC(6)=0x20;
	CRTC(7)=0x22;
	CRTC(8)=0x01;
	CRTC(9)=0x07;
	CRTC(10)=0x67;
	CRTC(11)=0x08;
	t65::word start=0x3000;
	start>>=3;
	CRTC(12)=t65::byte(start>>8);
	CRTC(13)=t65::byte(start);
	CRTC(14)=0;
	CRTC(15)=0;
	CRTC(16)=0;
	CRTC(17)=0;
	
	read_address_=0x3000;

	for(unsigned i=0;i<16;++i) {
		video_current_[ULA_PALETTE+i]=i&8?7:0;
	}

	video_current_[ULA_CTRL]=0x9C;

	hblank_leftaligned_=98;

	//stuff that was in bbcVideo::Init
	reg_6845_select_=0;
	framebuffer_base_=0;
	
	is_teletext_frame_=false;
	last_double_height_line_=-1;

	dlq_read_=0;
	dlq_write_=0;
	framebuffer_base_=0;
	//	vsync_left_=-1;
	wrap_size_=0x5000;//arbitrary
	palette_dirty_slow_=true;
	palette_dirty_fast_=true;
	palette_dirty_3bit_=true;
	cur_line_=0;
	cur_subline_=0;
	cur_scanline_=0;
	num_blank_scanlines_=0;

	last_flush_at_=bbcComputer::Cycles2MHz();//cycles;
	
//	CopyCurrentToStarting();
	memcpy(cpu_current_,video_current_,sizeof video_current_);
}

//////////////////////////////////////////////////////////////////////////
// Write6845
//
// Writes a value to the currently select 6845 reg
//
void bbcVideo::Write6845(t65::word addr,t65::byte val) {
	//	twprintf("bbcVideo::Write6845: offset=%d val=%d cycles=%d\n",offset,val,cycles);
	if(!(addr&1)) {
		reg_6845_select_=val&31;
	} else if(reg_6845_select_<18&&(reg_6845_flags_[reg_6845_select_]&WRITABLE)) {
		Write(CRTC_BASE+reg_6845_select_,val&reg_6845_flags_[reg_6845_select_]&0xFF);
	}
}

//////////////////////////////////////////////////////////////////////////
// WriteUla
//
// Writes a value to the ULA control register
//
void bbcVideo::WriteUla(t65::word,t65::byte val) {
	Write(ULA_CTRL,val);
}

//////////////////////////////////////////////////////////////////////////
// WritePalette
//
// Writes a value to the palette register
//
void bbcVideo::WritePalette(t65::word,t65::byte val) {
	Write(ULA_PALETTE+(val>>4),(val&0xf)^7);
}

//////////////////////////////////////////////////////////////////////////
// Read6845
//
// Reads a byte from the 6845
//
//TODO offset is ignored, it always returns the data register
t65::byte bbcVideo::Read6845(t65::word addr) {
	(void)addr;

	if(reg_6845_select_<18&&(reg_6845_flags_[reg_6845_select_]&READABLE)) {
		return cpu_current_[CRTC_BASE+reg_6845_select_];
	}
	return 0;
}

//////////////////////////////////////////////////////////////////////////
// Write
//
// Writes a byte to a video system register. The reg is one of the bbcVideo::Register
// enumerants. The write is added to the display list queue, if there's room,
// or more display is produced if there's no room left.
//
void bbcVideo::Write(t65::byte reg,t65::byte value) {
	int next_write=DlqNext(dlq_write_);
	if(next_write==dlq_read_) {
		Update();//Does not modify dlq_write_
	}
	BASSERT(reg<NUM_REGS);
	DlEntry *ent=&dlq_[dlq_write_];
	ent->when=bbcComputer::Cycles2MHz();
	ent->reg=reg;
	ent->val=value;
	cpu_current_[ent->reg]=value;
#ifdef bbcDEBUG_VIDEO
	if(log_frames_h_) {
		fprintf(log_frames_h_,"%d: Write: cycles=%d CPU wrote 0x%02X to %s, dlq index %d\n",
			bbcComputer::Cycles2MHz(),ent->when,ent->val,reg_names_[ent->reg],dlq_write_);
	}
#endif
	dlq_write_=next_write;
}

//////////////////////////////////////////////////////////////////////////
// Debugging crap
static int last_vsync_cycles=0;
static int vsync_low_cycles=0;

//////////////////////////////////////////////////////////////////////////
// Subline divisors
//
// Depending on the setting of R8, there might be .5x as many scanlines on
// screen as the CRTC thinks there are, or something. Who knows. This table
// is indexed using the bottom 2 bits of R8, and sorts it all out.
//
static const int subline_divisors[4]={
	1,//Normal sync mode
	1,//Normal sync mode
	1,//Interlace sync mode
	2,//Interlace sync and video
};

//////////////////////////////////////////////////////////////////////////
// Macro hackery

//Width in PC pixels of a CRTC column 
#define CRTC_COLUMN_WIDTH() (ULACTRL()&ULA_TELETEXT?12:(ULACTRL()&ULA_6845FASTCLOCK?8:16))

//Current subline divisor
#define SUBLINE_DIVISOR() (subline_divisors[CRTC(8)&3])

//Scanlines in one character row
#define SCANLINES_PER_ROW() (CRTC(9)/SUBLINE_DIVISOR()+1)

//true if 6845 fast clock in effect
#define IS_FASTCLOCK() (!!(video_current_[ULA_CTRL]&ULA_6845FASTCLOCK))

//How far to bob the screen across
#define HORIZ_OFFSET() (std::_cpp_max(((hblank_leftaligned_-CRTC(2)*(IS_FASTCLOCK()?1:2))*CRTC_COLUMN_WIDTH()/(IS_FASTCLOCK()?1:2)),0))

//////////////////////////////////////////////////////////////////////////
// DrawCursor
//
// Adds a cursor to the screen based on the current settings. This is added
// as a postprocess effect, and uses the current cursor settings in effect
// like RIGHT NOW. This is heinously incorrect, but looks fine.
//
//TODO AlienFromOuttaSpace looks wrong, it changes the registers too damn much
//and has a funny screen, so the cursor is in totally the wrong place.
void bbcVideo::DrawCursor() {
	//Bits 6 and 7 if both set disable cursor
	if((CRTC(8)&0xC0)==0xC0) {
		return;
	} else if(CRTC(1)==0) {
		//Screen is too small
		return;
	}

	//Cursor may be flashing or disabled
	switch((CRTC(10)>>5)&3) {
	case 0:
		//Non-blink
		break;
	case 1:
		//Non-display
		return;
	case 2:
		//1/16 field rate blink
		if(frame_count_&0x10) {
			return;
		}
		break;
	case 3:
		//1/32 field rate blink
		if(frame_count_&0x20) {
			return;
		}
		break;
	}
	
	//Rather dodgy calculation, should be OK for most stuff though.
	t65::word fb_base=framebuffer_base_;
	t65::Word cursor_addr;
	cursor_addr.l=CRTC(15);
	cursor_addr.h=CRTC(14);
	if(is_teletext_frame_) {
		cursor_addr.h^=0x20;
		cursor_addr.h+=0x74;
	} else {
		//cursor_addr.w<<=3;
		fb_base>>=3;
	}

	//Make it relative to screen start
	if(cursor_addr.w<fb_base) {
		cursor_addr.w-=fb_base;
		cursor_addr.w+=is_teletext_frame_?1024:wrap_size_>>3;
		return;
	} else {
		cursor_addr.w-=fb_base;
	}

	//Turn it back into character positions
	if(!is_teletext_frame_) {
//		cursor_addr.w>>=3;
	}
	int x=cursor_addr.w%CRTC(1)*CRTC_COLUMN_WIDTH()+HORIZ_OFFSET();
	int y=cursor_addr.w/CRTC(1)*SCANLINES_PER_ROW();
//	y+=last_start_scanline_;

	int start_line=(CRTC(10)&31)/SUBLINE_DIVISOR();
	int end_line=CRTC(11)/SUBLINE_DIVISOR();

	//Move cursor up and down according to R7 (which is how *TV works)
	int vsync_pos=CRTC(7)*SCANLINES_PER_ROW();
	y+=280-vsync_pos;

	if(y+start_line>=0&&y+end_line<buffer_height_) {
		//index using (ULACTRL()>>5)&3 -- see AUG p379
		static const int cursor_widths[4]={
			1,//0/3/4/6
			0,//undefined
			2,//1/5/7
			4,//2
		};
		int w=cursor_widths[(ULACTRL()&ULA_CURSORWIDTH)>>ULA_CURSORWIDTH_SHIFT];
		if(!(ULACTRL()&ULA_MASTERCURSORSIZE)) {
			w/=2;
		}
		w*=CRTC_COLUMN_WIDTH();
		if(w>0&&x>=0&&x<=buffer_width_-w) {
//			for(int i=CRTC(10)&31;i<=CRTC(11);i+=SUBLINE_DIVISOR()) {
//				t65::byte *dest=&buffer_[x+(y+i/SUBLINE_DIVISOR())*buffer_width];
//				for(int j=0;j<w;++j) {
//					dest[j]^=7;
//				}
//			}
			for(int i=start_line;i<=end_line;++i) {
				t65::byte *dest=&buffer_[x+(y+i)*buffer_width_];// OK with no buffer [1/8/2003]
				for(int j=0;j<w;++j) {
					dest[j]^=7;
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Update
//
// Does a video system update. 'cycles' is the current 2MHz cycle count.
// Renders more video, or doesn't, and does its thing.
//
void bbcVideo::Update() {
	const int cycles=bbcComputer::Cycles2MHz();//11/12/2004 -- 
	BASSERT(video_ram_);
	int ent=dlq_read_;
	int cycles_per_scanline;
	for(;;) {
		int c=last_flush_at_;

		//If less than one line to render, don't bother (yet)
		cycles_per_scanline=CRTC(0)+1;
		if(!(video_current_[ULA_CTRL]&ULA_6845FASTCLOCK)||(video_current_[ULA_CTRL]&ULA_TELETEXT)) {
			cycles_per_scanline*=2;
		}
		if(cycles-last_flush_at_<cycles_per_scanline) {
			break;
		}

		//Do this line
//		if(num_blank_scanlines_>0) {
//			--num_blank_scanlines_;
//			++cur_scanline_;
//			//and do nothing.
//		} else {

#ifdef VIDEO_UPDATE_REGISTERS_BEFORE
		ent=DlqRun(c+cycles_per_scanline,ent);
#endif
		LOGFRAME(("%-9d: %-3d: Render: ",bbcComputer::Cycles2MHz(),cur_scanline_));
#ifdef bbcDEBUG_VIDEO
		//Don't clip against ndisp if showall
		if(cur_scanline_>=0&&cur_scanline_<buffer_height_&&
			(show_all_||cur_line_<CRTC(6)))
#else
		if(cur_scanline_>=0&&cur_scanline_<buffer_height_&&cur_line_<CRTC(6))
#endif
		{
			//int pitch=cycles_per_scanline*5;
			//128 is the correct amount for one scanline
			//this value is therefore a 1:7 fixed point number
			//so (cycles_per_scanline*pitch)>>7
			//clamp at 128 to fix Labyrinth 30/10/2003
			int clamped_cycles_per_scanline=cycles_per_scanline;
			if(clamped_cycles_per_scanline>128) {
				clamped_cycles_per_scanline=128;
			}
			int pitch=(clamped_cycles_per_scanline*video_buffers_[current_video_buffer_].pitch)>>7;//*bbcVideo::buffer_width_)>>7;
			t65::byte *dest=&buffer_[cur_scanline_*pitch];//OK with no buffer

			dest+=HORIZ_OFFSET();//adjust based on hblank position

			if(dest>=buffer_&&dest+buffer_width_<END_OF_BUFFER) {
#ifdef bbcDEBUG_VIDEO
				//**** NOTE DANGLING ELSE ****
				int timer_min=cycles_per_scanline*2;
				if(show_t1_timeouts_&&
					(bbc_system_via.t1c_<timer_min||bbc_user_via.t1c_<timer_min))
				{
					int colour=0;
					if(bbc_system_via.t1c_<timer_min) {
						colour|=1;
						LOGFRAME(("[SysVIA-T1] "));
					}
					if(bbc_user_via.t1c_<timer_min) {
						colour|=2;
						LOGFRAME(("[UserVIA-T1]"));
					}
					memset(dest,colour,pitch);
				} else
#endif
#ifdef bbcDEBUG_VIDEO
				if(!show_all_&&num_blank_scanlines_>0)
#else
				if(num_blank_scanlines_>0)
#endif
				{
					//If R8 bits 4+5 both set, display is disabled.
#ifdef bbcDEBUG_VIDEO
					if(num_blank_scanlines_>0) {
						LOGFRAME(("Blank scanline (num_blank_scanlines=%d)",num_blank_scanlines_));
					} else {
						LOGFRAME(("Blank scanline (display disabled)"));
					}
#endif
					memset(dest,BlankPixel(),pitch);
				} else if(is_teletext_frame_) {
					if((CRTC(8)&0x30)==0x30) {
						LOGFRAME(("Blank scanline (display disabled)"));
						memset(dest,BlankPixel(),pitch);
					} else {
						LOGFRAME(("Teletext scanline @ 0x%04X",(t65::word)(framebuffer_base_+cur_line_*CRTC(1))));
						RenderTeletextScanline(dest,read_address_);
					}
				} else {
					//t65::word src=framebuffer_base_+cur_line_*CRTC(1)*8+cur_subline_;
					if(cur_subline_>=8) {
						LOGFRAME(("Blank scanline (subline %d)",cur_subline_));
						memset(dest,BlankPixel(),pitch);
					} else {
						if((CRTC(8)&0x30)==0x30) {
							LOGFRAME(("Blank scanline (display disabled)"));
							memset(dest,BlankPixel(),pitch);
						} else if(video_current_[ULA_CTRL]&ULA_6845FASTCLOCK) {
							LOGFRAME(("Fast 6845 scanline, ULA=&%02X (%d)",video_current_[ULA_CTRL],video_current_[ULA_CTRL]));
							RenderFastClockScanline(dest,(t65::word)(read_address_+cur_subline_));
						} else {
							LOGFRAME(("Slow 6845 scanline, ULA=&%02X (%d)",video_current_[ULA_CTRL],video_current_[ULA_CTRL]));
							RenderSlowClockScanline(dest,(t65::word)(read_address_+cur_subline_));
						}
					}
				}
			}
		}
#ifdef bbcDEBUG_VIDEO
		else {
			LOGFRAME(("Off screen, clipped"));
		}
#endif
		LOGFRAME(("\n           %-3d:",cur_scanline_));

		if(num_blank_scanlines_>0) {
			--num_blank_scanlines_;
		} else {
			cur_subline_+=SUBLINE_DIVISOR();
			if(cur_subline_>CRTC(9)) {
				cur_subline_=0;
				++cur_line_;
				if(is_teletext_frame_) {
					read_address_+=CRTC(1);
					if(read_address_&0x8000) {
						read_address_-=1024;
					}
				} else {
					read_address_+=CRTC(1)*8;
					if(read_address_&0x8000) {
						read_address_-=wrap_size_;
					}
				}
			}
		}

		//Update timing counts for that lot
		//TODO this was previously below the vsync bit and before the end-of-frame bit.
		c+=cycles_per_scanline;
		last_flush_at_=c;
		++cur_scanline_;

		//This won't be correct if R7==R4
		//TODO I'm not sure why though!
		if(cur_line_==CRTC(7)) {
			if(cur_subline_==0) {
				DrawCursor();
				cur_scanline_=-32;

#ifdef VSYNC_NEGATIVE
				bbc_system_via.CA1(false);
#else
				bbc_system_via.CA1(true);
#endif
				LOGFRAME((" [Vsync+]"));
				vsync_low_cycles=cycles;

				//buffer_dirty=true;
				//TODO Move this into a function!
//				ret=&video_buffers_[current_video_buffer_];
				display_video_buffer_=&video_buffers_[current_video_buffer_];
				++current_video_buffer_;
				current_video_buffer_%=num_video_buffers;
				buffer_=video_buffers_[current_video_buffer_].bitmap;

				if(!video_buffer_cleared_[current_video_buffer_]) {
					HostGfxBuffer_Clear(&video_buffers_[current_video_buffer_]); 
					video_buffer_cleared_[current_video_buffer_]=true;
				}

#ifdef bbcDEBUG_VIDEO
				if(log_frames_h_) {
					LOGFRAME((" [Frame %d done]",frame_count_));
					--num_log_frames_left_;
					if(num_log_frames_left_==0) {
//#ifdef bbcDEBUG_TRACE
//						if(bbcComputer::trace) {
//							bbcComputer::trace->WriteTrace(log_frames_h_,0,0);
//							delete bbcComputer::trace;
//							bbcComputer::trace=0;
//						}
//#endif
						fclose(log_frames_h_);
						log_frames_h_=0;
					}
				}
				if(show_all_&&(frame_count_%25==0)) {
					++show_all_offset_;
					palette_dirty_3bit_=true;
				}
#endif
				++frame_count_;
			} else {
				t65::byte vsynclen=(CRTC(3)>>4);//+1;
				t65::byte rowheight=CRTC(9);
				if(cur_subline_/SUBLINE_DIVISOR()==vsynclen||
					(vsynclen>rowheight&&cur_subline_==rowheight))
				{
#ifdef VSYNC_NEGATIVE
					bbc_system_via.CA1(true);
#else
					bbc_system_via.CA1(false);
#endif
					LOGFRAME((" [Vsync-]"));
					{
						//static char tmp[20];
						//printf("%d\n",cycles-last_vsync_cycles);
						last_vsync_cycles=cycles;
					}
				}
			}
		}

		//If past the bottom, go back to the top.
		if(cur_line_>CRTC(4)) {
			//Reset the scanline counter to 0 if the mode changes.
//			bool new_is_teletext_frame=!!(video_current_[ULA_CTRL]&ULA_TELETEXT);
//			if(new_is_teletext_frame!=is_teletext_frame_) {
//				cur_scanline_=0;
//			}
//			is_teletext_frame_=new_is_teletext_frame;
			LOGFRAME((" [line=%d, R4=%d]",cur_line_,CRTC(4)));
			is_teletext_frame_=!!(video_current_[ULA_CTRL]&ULA_TELETEXT);
			if(is_teletext_frame_) {
				t65::Word tmp;
				tmp.h=CRTC(12);
				tmp.h^=0x20;
				tmp.h+=0x74;
				tmp.l=CRTC(13);
				framebuffer_base_=tmp.w;

				//Default R2 setting in Mode 7 is 51. That's *2 for 102. 115-102 is 13, product
				//of which & column width (6) is 78, which is not far shy of where the normal
				//Mode 7 screen (480x250) needs to be (80) to be centred.
				hblank_leftaligned_=115;

				last_double_height_line_=-1;
			} else {
				framebuffer_base_=(CRTC(12)<<8|CRTC(13))<<3;
				hblank_leftaligned_=98;
			}
			cur_subline_=0;
			cur_line_=0;
			//In an interlaced PAL frame, there are 312.5 scanlines.
			//No interlace here, so there's just a timing fudge.
			if(CRTC(8)&1) {
				last_flush_at_+=cycles_per_scanline/2;
#ifdef bbcVARIABLE_FRAME_TIMINGS
				//This introduces some variation into the frame timings.
				//This is an attempt to fix WAR, which does this:

//				22189470  (@22189568 ) 0A36: LDA   &7F        A5 7F    -> A:D2 X:14 Y:01 S:F9 P:N------C
//				22189473  (@22189568 ) 0A38: CMP   &7F        C5 7F    -> A:D2 X:14 Y:01 S:F9 P:------ZC
//				22189476  (@22189568 ) 0A3A: BEQ   &0A36      F0 FA    -> A:D2 X:14 Y:01 S:F9 P:------ZC

				//with an interrupt routine that does DEC &7F. Unfortunately the IRQs never occur between
				//the LDA and the CMP, so the game appears to hang.
				//
				//Not any more.
				int extra=2-(frame_count_&3);
				if(extra<=0) {
					--extra;
				}
				last_flush_at_+=extra;
#endif
			}
			//There are extra blank scanlines according to R5.
			num_blank_scanlines_=CRTC(5);
			read_address_=framebuffer_base_;
		}

		LOGFRAME(("\n"));
#ifndef VIDEO_UPDATE_REGISTERS_BEFORE
		ent=DlqRun(last_flush_at_,ent);
#endif
		//the problem with the old check: we are checking against the old value of cycles_per_scanline, which
		//might have since been updated by the DlqRun call. hence change to for(;;). TODO tidy that up :)
	}// while(true);//cycles-last_flush_at_>=cycles_per_scanline);

	dlq_read_=ent;
//	CopyCurrentToStarting();
	bbcComputer::SetNextStop(last_flush_at_+cycles_per_scanline);
}

//////////////////////////////////////////////////////////////////////////
// DlqRun
//
// Runs the display list to the point where 'stop_cycles' cycles had executed,
// starting from entry 'ent'. Updates dirty flags as relevant.
//
int bbcVideo::DlqRun(int stop_cycles,int ent) {
	int startent=ent;
	(void)startent;

	//Copy and paste from non-rupture Update
	for(;ent!=dlq_write_;ent=DlqNext(ent)) {
		const DlEntry *dle=&dlq_[ent];
		if(stop_cycles<dle->when) {
			break;
		}
		video_current_[dle->reg]=dle->val;
		LOGFRAME(("%-9d: WWW: dle->when=%u 0x%02X (%d) written to %s\n",
			bbcComputer::Cycles2MHz(),dle->when,dle->val,dle->val,reg_names_[dle->reg]));
		//if(dle->reg>=ULA_BEGIN&&dle->reg<ULA_END) {
		if(dle->reg<ULA_END) {
//			palette_dirty_slow_=true;
//			palette_dirty_fast_=true;
			palette_dirty_3bit_=true;
		}
	}
	LOGFRAME(("         : WWW: queue ran, started at %d finished at %d\n",startent,ent));
	return ent;
}

//#define BITOF(M) ((bbcComputer::ram_[addr]&(M))?7:0)

//////////////////////////////////////////////////////////////////////////
// RenderTeletextScanline
//
// renders a teletext scanline.
//
// TODO I think double height is a bit wrong, see instructions screen for
// Frak!.
enum Mode7TextSettings {
	M7_GFX=1,
	M7_SEP=2,
	M7_GFXMASK=M7_GFX|M7_SEP,
	M7_DH=4,
};

//Actually, it isn't any smoother.
#define SMOOTH_MODE7_FONT

#ifdef SMOOTH_MODE7_FONT
#define FONT_PITCH (1)
#else
#define FONT_PITCH (6)
#endif


void bbcVideo::RenderTeletextScanline(t65::byte *dest,t65::word src) {
	if(cur_subline_&1) {
		return;
	}
//	int scanline=CUR_SCANLINE();
//	byte *dest=DEST_ADDRESS();
	int subline=cur_subline_/SUBLINE_DIVISOR();
	if(subline>=10) {
		memset(dest,BlankPixel(),buffer_width_);
	} else {
//		word src=framebuffer_base_+CRTC(1)*cur_line_;
		unsigned cpl=std::_cpp_min(t65::byte(buffer_width_/12),CRTC(1));//cap at 53, that's 636 pixels
		t65::byte colours[2],new_colours[2];

		colours[0]=palette_colours_[0];//0|buffer_format_.fixed_bits;//TODOpixelformat
		colours[1]=palette_colours_[7];//7|buffer_format_.fixed_bits;//TODOpixelformat

		new_colours[0]=palette_colours_[0];//0|buffer_format_.fixed_bits;//TODOpixelformat
		new_colours[1]=palette_colours_[7];//7|buffer_format_.fixed_bits;//TODOpixelformat

		bool hold_graphics=false;
		unsigned textsettings=0;
#ifdef SMOOTH_MODE7_FONT
		static const t65::word *textfonts[4]={
			&bbcTeletextFont::normal_chars[0][0],//normal
			&bbcTeletextFont::graphics_chars[0][0],//graphics
			&bbcTeletextFont::normal_chars[0][0],//normal separated
			&bbcTeletextFont::sep_graphics_chars[0][0],//graphics separated
		};
#else
		static const t65::byte *textfonts[4]={
			bbcTeletextFont::normal_chars_src,//normal
			bbcTeletextFont::graphics_chars_src,//graphics
			bbcTeletextFont::normal_chars_src,//normal separated
			bbcTeletextFont::sep_graphics_chars_src,//graphics separated
		};
#endif
		//Last byte seen. Used for hold graphics mode.
		t65::byte last_b=0;
		bool was_double_height=false;
		bool was_bottom_half=false;
		for(unsigned i=0;i<cpl;++i) {
			if(src&0x8000) {
				src-=1024;
			}
			t65::byte b=video_ram_[src]&0x7f;//bbcComputer::ram_[src]&0x7F;
			if(b<32) {
				switch(b) {
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
					//Text mode colour
					new_colours[1]=palette_colours_[b];//b|buffer_format_.fixed_bits;//pixel_fixed_bits_;
					textsettings&=~M7_GFX;
					break;
				case 140&0x7F:
					//Single height
					textsettings&=~M7_DH;
					break;
				case 141&0x7F:
					//Double height
					textsettings|=M7_DH;
					was_double_height=true;
					break;
				case 145&0x7F:
				case 146&0x7F:
				case 147&0x7F:
				case 148&0x7F:
				case 149&0x7F:
				case 150&0x7F:
				case 151&0x7F:
					//Graphics mode colour
					new_colours[1]=palette_colours_[b-(144&0x7f)];//(b-(144&0x7f))|buffer_format_.fixed_bits;//TODOpixelformat
					textsettings|=M7_GFX;
					break;
				case 157&0x7f:
					//Set bg colour
					new_colours[0]=new_colours[1];//|buffer_format_.fixed_bits;//TODOpixelformat
					break;
				case 156&0x7f:
					//Reset bg colour
					new_colours[0]=palette_colours_[0];//0|buffer_format_.fixed_bits;//TODOpixelformat
					break;
				case 154&0x7f:
					//Separated graphics
					textsettings|=M7_SEP;
					break;
				case 153&0x7f:
					//Normal graphics
					textsettings&=~M7_SEP;
					break;
				case 158&0x7f:
					//Hold graphics
					hold_graphics=true;
					break;
				case 159&0x7f:
					//Release graphics
					hold_graphics=false;
					break;
				}
			}
#ifdef SMOOTH_MODE7_FONT
			const t65::word *ch=&textfonts[textsettings&M7_GFXMASK][0];
#else
			const t65::byte *ch=&textfonts[textsettings&M7_GFXMASK][0];
#endif
			if(b>=32) {
				ch+=(b-32)*10*FONT_PITCH;
			} else {
				if(hold_graphics) {
					if(last_b>=32) {
						ch+=(last_b-32)*10*FONT_PITCH;
					}
				} else {
					colours[0]=new_colours[0];
					colours[1]=new_colours[1];
				}
			}
			last_b=b;
			if(!(textsettings&M7_DH)) {
				ch+=subline*FONT_PITCH;
			} else {
				//Double height
				ch+=subline/2*FONT_PITCH;
				if(last_double_height_line_>=0&&last_double_height_line_==cur_line_-1) {
					//Bottom half
					ch+=5*FONT_PITCH;
					was_bottom_half=true;
				}
			}
#ifdef SMOOTH_MODE7_FONT
			t65::word c=*ch;
			dest[0]=colours[(c>>11)&1];
			dest[1]=colours[(c>>10)&1];
			dest[2]=colours[(c>>9)&1];
			dest[3]=colours[(c>>8)&1];
			dest[4]=colours[(c>>7)&1];
			dest[5]=colours[(c>>6)&1];
			dest[6]=colours[(c>>5)&1];
			dest[7]=colours[(c>>4)&1];
			dest[8]=colours[(c>>3)&1];
			dest[9]=colours[(c>>2)&1];
			dest[10]=colours[(c>>1)&1];
			dest[11]=colours[c&1];
#else
			for(unsigned j=0;j<6;++j) {
				dest[j*2+1]=dest[j*2]=(ch[j]&colours[1])|(~ch[j]&colours[0]);
			}
#endif

//			fg=new_fg;
//			bg=new_bg;
			dest+=12;
			++src;
		}
		if(was_double_height&&!was_bottom_half) {
			last_double_height_line_=cur_line_;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Rebuild3bitPalette
//
// Rebuilds the current palette_3bit_ table. Indexed by ULA palette index,
// this contains a _3_ bit value which takes into account the current flash
// bit.
//
// Note to self: stop dicking about with this function! It works and
// you will only cock it up!
void bbcVideo::Rebuild3bitPalette() {
#ifdef bbcDEBUG_VIDEO
	if(show_all_) {
		palette_3bit_[0]=0;
		palette_3bit_[1]=1;
		palette_3bit_[2]=2;
		palette_3bit_[3]=3;
		palette_3bit_[4]=4;
		palette_3bit_[5]=5;
		palette_3bit_[6]=6;
		palette_3bit_[7]=7;
		palette_3bit_[8]=7;
		palette_3bit_[9]=6;
		palette_3bit_[10]=5;
		palette_3bit_[11]=4;
		palette_3bit_[12]=3;
		palette_3bit_[13]=2;
		palette_3bit_[14]=1;
		palette_3bit_[15]=0;
	} else
#endif
	{
		if(!(video_current_[ULA_CTRL]&1)) {
			for(unsigned i=0;i<16;++i) {
				palette_3bit_[i]=PALETTE(i)&7;
				//palette_3bit_[i]|=pixel_fixed_bits_;
			}
		} else {
			for(unsigned i=0;i<16;++i) {
				palette_3bit_[i]=PALETTE(i);
				if(palette_3bit_[i]&8) {
					//Flashing colour
					palette_3bit_[i]^=15;//remove bit 3 invert bits 210
				}
			}
		}

		//remap to buffer format
		for(unsigned i=0;i<16;++i) {
			t65::byte newval=0;

			if(palette_3bit_[i]&1) {
				newval|=buffer_format_.elems[0].mask;
			}
			if(palette_3bit_[i]&2) {
				newval|=buffer_format_.elems[1].mask;
			}
			if(palette_3bit_[i]&4) {
				newval|=buffer_format_.elems[2].mask;
			}
			newval|=buffer_format_.fixed_bits;

			palette_3bit_[i]=newval;
//			bbcPrintf(__FUNCTION__ ": palette_3bit_[%u]=%d 0x%02X\n",i,palette_3bit_[i],palette_3bit_[i]);
		}
	}
	palette_dirty_fast_=true;
	palette_dirty_slow_=true;
}

//////////////////////////////////////////////////////////////////////////
// Render(Fast|Slow)ClockScanline
//
// Renders a bitmap mode scanline for the appropriate 6845 clock speed.
void bbcVideo::RenderFastClockScanline(t65::byte *dest,t65::word src) {
	if(palette_dirty_3bit_) {
		Rebuild3bitPalette();
		palette_dirty_3bit_=false;
	}
	unsigned cpl=std::_cpp_min(CRTC(1),t65::byte(buffer_width_/8));//cap at 80, that's 640 pixels
#ifdef bbcDEBUG_VIDEO
	if(show_all_) {
		cpl=buffer_width_/8;
	}
#endif
	unsigned i;
	switch((video_current_[ULA_CTRL]&ULA_CHARSPERLINE)>>ULA_CHARSPERLINE_SHIFT) {
	case 0://10 (broken)
		for(i=0;i<cpl;++i) {
			if(src&0x8000) {
				src-=wrap_size_;
			}
		}
		break;
	case 1://20 (mode 2)
		for(i=0;i<cpl;++i) {
			if(src&0x8000) {
				src-=wrap_size_;
			}
			t65::byte knobbled=rearrange_4bit_[video_ram_[src]];
			t65::byte first=palette_3bit_[knobbled>>4];
			t65::byte second=palette_3bit_[knobbled&15];
			dest[3]=dest[2]=dest[1]=dest[0]=first;
			dest[7]=dest[6]=dest[5]=dest[4]=second;
			src+=8;
			dest+=8;
		}
		break;
	case 2://40 (mode 1)
		for(i=0;i<cpl;++i) {
			if(src&0x8000) {
				src-=wrap_size_;
			}
			t65::byte knobbled=rearrange_2bit_[video_ram_[src]];
			//Higher nybble: A-A----- and -B-B---- (1010 == 10)
			dest[1]=dest[0]=palette_3bit_[(knobbled>>4)&10];
			dest[3]=dest[2]=palette_3bit_[(knobbled>>3)&10];

			//Lower nybble: ----C-C- and -----D-D
			dest[5]=dest[4]=palette_3bit_[knobbled&10];
			dest[7]=dest[6]=palette_3bit_[(knobbled<<1)&10];
			
			src+=8;
			dest+=8;
		}
		break;
	case 3://80 (mode 0/3)
		if(palette_dirty_fast_) {
			RebuildFastClockPaletteTables();
		}
		for(i=0;i<cpl;++i) {
			if(src&0x8000) {
				src-=wrap_size_;
			}
			t65::byte b=video_ram_[src];
			*reinterpret_cast<t65::dword *>(dest)=expand_1bit_fast_[b>>4];
			*reinterpret_cast<t65::dword *>(dest+4)=expand_1bit_fast_[b&0xf];

			src+=8;
			dest+=8;
		}
		break;
	}
}

void bbcVideo::RenderSlowClockScanline(t65::byte *dest,t65::word src) {
	if(palette_dirty_3bit_) {
		Rebuild3bitPalette();
		palette_dirty_3bit_=false;
	}
	unsigned cpl=std::_cpp_min(CRTC(1),t65::byte(buffer_width_/16));//cap at 40, that's 640 pixels
#ifdef bbcDEBUG_VIDEO
	if(show_all_) {
		cpl=buffer_width_/16;
	}
#endif
	unsigned i;
	switch((video_current_[ULA_CTRL]&ULA_CHARSPERLINE)>>ULA_CHARSPERLINE_SHIFT) {
	case 0://10 (mode '8')
		for(i=0;i<cpl;++i) {
			if(src&0x8000) {
				src-=wrap_size_;
			}
			t65::byte knobbled=rearrange_4bit_[video_ram_[src]];
			t65::byte first=palette_3bit_[knobbled>>4];
			t65::byte second=palette_3bit_[knobbled&15];
			dest[7]=dest[6]=dest[5]=dest[4]=dest[3]=dest[2]=dest[1]=dest[0]=first;
			dest[15]=dest[14]=dest[13]=dest[12]=dest[11]=dest[10]=dest[9]=dest[8]=second;
			src+=8;
			dest+=16;
		}
		break;
	case 1://20 (mode 5)
		for(i=0;i<cpl;++i) {
			if(src&0x8000) {
				src-=wrap_size_;
			}
			t65::byte knobbled=rearrange_2bit_[video_ram_[src]];
			//Higher nybble: A-A----- and -B-B---- (1010 == 10)
			dest[3]=dest[2]=dest[1]=dest[0]=palette_3bit_[(knobbled>>4)&10];
			dest[7]=dest[6]=dest[5]=dest[4]=palette_3bit_[(knobbled>>3)&10];
			
			//Lower nybble: ----C-C- and -----D-D
			dest[11]=dest[10]=dest[9]=dest[8]=palette_3bit_[knobbled&10];
			dest[15]=dest[14]=dest[13]=dest[12]=palette_3bit_[(knobbled<<1)&10];
			
			src+=8;
			dest+=16;
		}
		break;
	case 2://40 (mode 4/6)
		if(palette_dirty_slow_) {
			RebuildSlowClockPaletteTables();
		}
		for(i=0;i<cpl;++i) {
			if(src&0x8000) {
				src-=wrap_size_;
			}

			t65::byte b=video_ram_[src];
			*reinterpret_cast<t65::qword *>(dest)=expand_1bit_slow_[b>>4];
			*reinterpret_cast<t65::qword *>(dest+8)=expand_1bit_slow_[b&0xf];
			
			src+=8;
			dest+=16;
		}
		break;
	case 3://80 (broken)
		for(i=0;i<cpl;++i) {
			if(src&0x8000) {
				src-=wrap_size_;
			}
			src+=8;
		}
		break;
	}
}

//////////////////////////////////////////////////////////////////////////
// DebugDump
//
// Hmm.
void bbcVideo::DebugDump(FILE *h) {
	fprintf(h,"Video system state:\n");
	fprintf(h,"===================\n");
	fprintf(h,"\n");
	unsigned i;
	for(i=0;i<NUM_REGS;++i) {
		fprintf(h,"%s: 0x%02X (%d)\n",reg_names_[i],video_current_[i],video_current_[i]);
	}
	fprintf(h,"?&355=%d\n",bbcComputer::ram_[0x355]);
}

//////////////////////////////////////////////////////////////////////////
//
void bbcVideo::SetBufferDetails(int width,int height,const HostGfxBufferFormat *fmt) {
	int changed;

	changed=0;
	if(width!=buffer_width_||height!=buffer_height_) {
		//nothing can be done; must recreate all buffers.
		for(unsigned i=0;i<num_video_buffers;++i) {
			HostGfxBuffer_SetDetails(&video_buffers_[i],width,height,fmt);
			video_buffer_cleared_[i]=false;
		}
		changed=1;
	} else {
		if(!HostGfxBufferFormat_Equal(&buffer_format_,fmt)) {
			//convert each one.
//			for(unsigned i=0;i<num_video_buffers;++i) {
//				HostGfxBuffer_Convert(&video_buffers_[i],fmt,&video_buffers_[i]);
//			}
			for(unsigned i=0;i<num_video_buffers;++i) {
				HostGfxBuffer_SetDetails(&video_buffers_[i],width,height,fmt);
			}
			changed=1;
		}
	}

	if(changed) {
		buffer_width_=width;
		buffer_height_=height;
		buffer_format_=*fmt;
		//pixel_fixed_bits_=fmt->px[0].fixed_bits;
		current_video_buffer_=0;
		buffer_=video_buffers_[current_video_buffer_].bitmap;
		
		//assumes all video buffers have the same pitch (which will be the case)
		buffer_size_=video_buffers_[0].pitch*buffer_height_;

		palette_dirty_slow_=true;
		palette_dirty_fast_=true;
		palette_dirty_3bit_=true;

		for(unsigned i=0;i<8;++i) {
			palette_colours_[i]=0;
			for(unsigned j=0;j<3;++j) {
				if(i&(1<<j)) {
					palette_colours_[i]|=buffer_format_.elems[j].mask;
				}
				palette_colours_[i]|=buffer_format_.fixed_bits;
			}
//			bbcPrintf("palette_colours_[i]=%d 0x%02X\n",palette_colours_[i],palette_colours_[i]);
		}

		bbcPrintf("%s: new buffer format: r=%d g=%d b=%d fb=%d",__FUNCTION__,
			buffer_format_.elems[0].mask,
			buffer_format_.elems[1].mask,
			buffer_format_.elems[2].mask,
			buffer_format_.fixed_bits);
	}
}

//////////////////////////////////////////////////////////////////////////
// Sets the dimensions of the screen buffer.
// Returns false if the dimensions are inappropriate.
//bool bbcVideo::SetBufferDimensions(int width,int height) {
//	if(width==buffer_width_&&height==buffer_height_) {
//		return true;
//	}
//	if(width<min_buffer_width||width>max_buffer_width) {
//		return false;
//	}
//	if(height<min_buffer_height||height>max_buffer_height) {
//		return false;
//	}
//
//	buffer_width_=width;
//	buffer_height_=height;
//	
//	buffer_size_=buffer_width_*buffer_height_;
//
//	buffers_vector_.resize(buffer_size_*num_video_buffers,pixel_fixed_bits_);
//
//	for(int i=0;i<num_video_buffers;++i) {
//		HostGfxBufferFormat deffmt;
//
//		HostGfxBufferFormat_Reset(&deffmt);
//
//		HostGfxBuffer_SetDetails(&video_buffers_[i],buffer_width_,buffer_height_.&deffmt);
////		HostGfxBuffer *b=&video_buffers_[i];
////		
////		b->w=buffer_width_;
////		b->h=buffer_height_;
////		b->pitch=buffer_width_;
////
////		b->bitmap_3bit=&buffers_vector_.at(i*buffer_size_);
////
////		HostGfxBufferFormat_Reset(b);
////		bbcVideoBuffer *buffer=&video_buffers_[i];
////
////		buffer->width=buffer_width_;
////		buffer->height=buffer_height_;
////		buffer->pitch=buffer_width_;
////
////		buffer->data=&buffers_vector_.at(i*buffer_size_);
//	}
//
//	current_video_buffer_=0;
//	buffer_=video_buffers_[current_video_buffer_].data;
//
//	return true;
//}
//	//there's one scanline of blank. i'm experimenting with asm routines that read too far
//	//forward on the last few pixels. this will prevent those causing access violations.
//	//note this also affects the fixed bits thing; that too must cater for this extra scanline,
//	//since the asm may use it for a table lookup expecting it to be 3 bits only. [5/11/2003]
//	buffer_size=buffer_width*(buffer_height+1);
//	buffer_vector_.clear();
//	if(buffer_size==0) {
//		buffer=0;
//	} else {
//		buffer_vector_.resize(buffer_size+video_buffer_alignment-1+256,pixel_fixed_bits_);
//		t65::dword aligned=t65::dword(&buffer_vector_[0]);
//		aligned+=video_buffer_alignment-1;
//		aligned&=~(video_buffer_alignment-1);
//		buffer=reinterpret_cast<t65::byte *>(aligned);
//	}
////	buffer_dirty=true;
//	return true;
//}

//////////////////////////////////////////////////////////////////////////
// Clears current BBC video buffer.
void bbcVideo::ClearBuffer() {
//	HostGfxBuffer_Clear(&video_buffers_[current_video_buffer_]);
	for(int i=0;i<num_video_buffers;++i) {
		video_buffer_cleared_[i]=false;
	}
}

#ifdef bbcDEBUG_VIDEO
void bbcVideo::SetShowAll(bool new_show_all) {
	show_all_=new_show_all;
	palette_dirty_3bit_=true;
	show_all_offset_=0;
}

void bbcVideo::LogFrames(const char *filename,int num_frames,bool with_disassembly) {
	(void)with_disassembly;

	fprintf(stderr,"bbcVideo::LogFrames(\"%s\",%d): ",filename,num_frames);
	if(log_frames_h_) {
		fprintf(stderr,"already logging frames, %d frames left.\n",num_log_frames_left_);
	} else {
		log_frames_h_=fopen(filename,"wt");
		if(log_frames_h_) {
			fprintf(stderr,"yes\n");
			num_log_frames_left_=num_frames;
//#ifdef bbcDEBUG_TRACE
//			if(with_disassembly&&!bbcComputer::trace) {
//				bbcComputer::trace=new bbcDebugTrace;
//			}
//#endif
		} else {
			fprintf(stderr,"no, open failed\n");
		}
	}
}
#endif

//////////////////////////////////////////////////////////////////////////
//  0         1         2         3         4         5         6
//  0123456789012345678901234567890123456789012345678901234567890123456789
//00 0 H TOTAL   XX YYY 11 C'R END   XX YYY PAL  00 = 00
// 1 1 CHRS/LINE XX YYY 12 START MSB XX YYY
// 2 2 HSYNC POS XX YYY 13 START LSB XX YYY
// 3 3           XX YYY 14 C'R MSB   XX YYY
// 4   HSYNC LEN XX YYY 15 C'R LSB   XX YYY
// 5   VSYNC LEN XX YYY  
// 6 4 VERT TOT  XX YYY SCREEN BASE   &XXXX
// 7 5 VERT ADJ  XX YYY WRAP SIZE     &XXXX
// 8 6 VERT DISP XX YYY 
// 9 7 VSYNC POS XX YYY 
//10 8           XX YYY 
// 1   INTERLACE XX YYY 
// 2   DISP DEL  XX YYY 
// 3   CURS DEL  XX YYY
// 4 9 LINES/CHR XX YYY 
// 510           XX YYY
// 6   C'R START XX YYY
// 7   C'R BLINK XX YYY
// 8   C'R TYPE  XX YYY

//////////////////////////////////////////////////////////////////////////

static const bbcSaveStateItemId bbc_video_save_state_item_id("bbcvideo");

#pragma pack(push,1)

struct bbcVideo::SaveState1 {
	struct DlEntry {
		bbcSaveStateInt32 when;
		bbcSaveStateByte reg;
		bbcSaveStateByte val;

		DlEntry();
	};

	struct Regs {
		bbcSaveStateByte palette[16];
		bbcSaveStateByte ula_ctrl;
		bbcSaveStateByte crtc[18];

		Regs();

		void Set(const t65::byte *regs);
		void Get(t65::byte *regs) const;
	};

	Regs cpu_regs;
	Regs video_regs;
	bbcSaveStateInt32 last_flush_at;
	bbcSaveStateInt32 num_dl_entries;
	DlEntry dl_entries[bbcVideo::dlq_num_entries];

	bbcSaveStateByte crtc_reg_select;
	bbcSaveStateWord crtc_read_address;
	bbcSaveStateInt32 crtc_cur_line;
	bbcSaveStateInt32 crtc_cur_subline;
	bbcSaveStateWord frame_buffer_base;
	bbcSaveStateInt32 crtc_cur_scanline;
	bbcSaveStateWord crtc_wrap_size;

	bbcSaveStateByte is_teletext_frame;
	bbcSaveStateInt32 last_double_height_line;
	bbcSaveStateInt32 hblank_leftaligned;

	SaveState1();
};
#pragma pack(pop)

bbcVideo::SaveState1::DlEntry::DlEntry():
when(0),
reg(0),
val(0)
{
}

bbcVideo::SaveState1::Regs::Regs():
ula_ctrl(0)
{
	memset(this->palette,0,sizeof this->palette);
	memset(this->crtc,0,sizeof this->crtc);
}

bbcVideo::SaveState1::SaveState1():
last_flush_at(0),
num_dl_entries(0),
crtc_reg_select(0),
crtc_read_address(0),
crtc_cur_line(0),
crtc_cur_subline(0),
frame_buffer_base(0),
crtc_cur_scanline(0),
crtc_wrap_size(0),
is_teletext_frame(0),
last_double_height_line(0),
hblank_leftaligned(0)
{
}

void bbcVideo::SaveState1::Regs::Set(const t65::byte *regs) {
	for(int i=0;i<16;++i) {
		this->palette[i]=regs[bbcVideo::ULA_PALETTE+i];
	}

	this->ula_ctrl=regs[bbcVideo::ULA_CTRL];

	for(int i=0;i<18;++i) {
		this->crtc[i]=regs[bbcVideo::CRTC_BASE+i];
	}
}

void bbcVideo::SaveState1::Regs::Get(t65::byte *regs) const {
	for(int i=0;i<16;++i) {
		regs[bbcVideo::ULA_PALETTE+i]=this->palette[i];
	}

	regs[bbcVideo::ULA_CTRL]=this->ula_ctrl;

	for(int i=0;i<18;++i) {
		regs[bbcVideo::CRTC_BASE+i]=this->crtc[i];
	}
}

bbcSaveState::LoadStatus bbcVideo::LoadSaveState(const bbcSaveState *save_state) {
	const bbcSaveStateItem *item=save_state->GetItem(bbc_video_save_state_item_id,0);
	if(!item) {
		return bbcSaveState::LS_MISSING;
	}

	switch(item->version) {
	default:
		return bbcSaveState::LS_BAD_VERSION;

	case 1:
		if(item->data.size()!=sizeof(SaveState1)) {
			return bbcSaveState::LS_BAD_DATA;
		} else {
			const SaveState1 *data=reinterpret_cast<const SaveState1 *>(&item->data[0]);

			if(data->num_dl_entries>dlq_num_entries) {
				return bbcSaveState::LS_BAD_DATA;
			}

			data->cpu_regs.Get(cpu_current_);
			data->video_regs.Get(video_current_);

			dlq_read_=0;
			dlq_write_=0;
			for(int i=0;i<data->num_dl_entries;++i) {
				const SaveState1::DlEntry *src=&data->dl_entries[i];
				DlEntry *dest=&dlq_[dlq_write_++];

				dest->when=src->when;
				dest->reg=src->reg;
				dest->val=src->val;
			}

			last_flush_at_=data->last_flush_at;
			reg_6845_select_=data->crtc_reg_select;
			read_address_=data->crtc_read_address;
			cur_line_=data->crtc_cur_line;
			cur_subline_=data->crtc_cur_subline;
			framebuffer_base_=data->frame_buffer_base;
			cur_scanline_=data->crtc_cur_scanline;
			wrap_size_=data->crtc_wrap_size;
			is_teletext_frame_=!!data->is_teletext_frame;
			last_double_height_line_=data->last_double_height_line;
			hblank_leftaligned_=data->hblank_leftaligned;

			palette_dirty_3bit_=true;
			palette_dirty_slow_=true;
			palette_dirty_fast_=true;
		}
	}

	return bbcSaveState::LS_OK;
}

bbcSaveState::SaveStatus bbcVideo::SaveSaveState(bbcSaveState *save_state) {
	SaveState1 data;

	data.cpu_regs.Set(cpu_current_);
	data.video_regs.Set(video_current_);

	data.num_dl_entries=0;
	for(int i=dlq_read_;i!=dlq_write_;i=(i+1)%dlq_num_entries) {
		const DlEntry *src=&dlq_[i];
		SaveState1::DlEntry *dest=&data.dl_entries[data.num_dl_entries++];

		dest->when=src->when;
		dest->reg=src->reg;
		dest->val=src->val;
	}

	data.last_flush_at=last_flush_at_;
	data.crtc_reg_select=reg_6845_select_;
	data.crtc_read_address=read_address_;
	data.crtc_cur_line=cur_line_;
	data.crtc_cur_subline=cur_subline_;
	data.frame_buffer_base=framebuffer_base_;
	data.crtc_cur_scanline=cur_scanline_;
	data.crtc_wrap_size=wrap_size_;
	data.is_teletext_frame=is_teletext_frame_;
	data.last_double_height_line=last_double_height_line_;
	data.hblank_leftaligned=hblank_leftaligned_;

	save_state->SetItem(bbc_video_save_state_item_id,0,1,&data);

	return bbcSaveState::SS_OK;
}
