#ifndef bbcPROFILER_H_
#define bbcPROFILER_H_

#ifndef bbcENABLE_PROFILER

#define BP(X)
#define BPR(X)

#else

//#include <vector>

//times code from this point til end of enclosing scope
//variable name generated automatically.
#define BPR(PARENT) bbcPROFILER_VAR(PARENT,__LINE__)
#define bbcPROFILER_VAR(PARENT,LINE) bbcProfiler bbc_tmp_profiler_##LINE##__(&(PARENT))

//x86 only at the mo'

typedef void (*bbcProfilerPrintFn)(void *context,const char *str);

//insert code only when profiling enabled
#define BP(X) X

// TODO should really be a rationalization of all this stuff
#ifdef _MSC_VER
static inline void bbcProfilerReadTsc(unsigned __int64 *p) {
	__asm cpuid
	__asm rdtsc
	__asm mov edi,p
	__asm mov [edi+0],eax
	__asm mov [edi+4],edx
}
#else
static inline void bbcProfilerReadTsc(uint64_t *p) {
	asm(
		"cpuid;\n"
		"rdtsc;\n"
		:"=A"(*p)
		:"a"(0)
		:"%ebx","%ecx");
}
#endif

class bbcProfileRecord {
public:
	unsigned count;
	uint64_t total;
	const char *const name;
	bbcProfileRecord *const parent;

	//name stored bf pointer.
	bbcProfileRecord(const char *name,bbcProfileRecord *parent);

	void RecursivePrint(bbcProfilerPrintFn pfn,int indent,void *context);
	void GetImmediateChildren(std::vector<bbcProfileRecord *> *prs);

	bbcProfileRecord *FirstChild() const;
	bbcProfileRecord *NextSibling() const;

	static void GetChildrenByParent(bbcProfileRecord *parent,std::vector<bbcProfileRecord *> *prs);
protected:
private:
	bbcProfileRecord *first_child_;
	bbcProfileRecord *next_sibling_;

	bbcProfileRecord(const bbcProfileRecord &);
	bbcProfileRecord &operator=(const bbcProfileRecord &);

	friend void bbcProfilerSetupTree();
}; 

inline bbcProfileRecord *bbcProfileRecord::FirstChild() const {
	return first_child_;
}

inline bbcProfileRecord *bbcProfileRecord::NextSibling() const {
	return next_sibling_;
}

class bbcProfiler {
public:
	bbcProfiler(bbcProfileRecord *bpr);
	~bbcProfiler();
protected:
private:
	uint64_t start_;
	bbcProfileRecord *bpr_;
	bbcProfiler(const bbcProfiler &);
	bbcProfiler &operator=(const bbcProfiler &);
};

inline bbcProfiler::bbcProfiler(bbcProfileRecord *bpr):
bpr_(bpr)
{
	bbcProfilerReadTsc(&start_);
}

inline bbcProfiler::~bbcProfiler() {
	uint64_t end;

	bbcProfilerReadTsc(&end);

	++bpr_->count;
	bpr_->total+=end-start_;
}

extern bbcProfileRecord bbc_profile_records_root;

//pass 0 to pfn to use bbcPrintf.
void bbcProfilerPrintRecords(bbcProfilerPrintFn pfn,void *context);

// call after main() has started.
void bbcProfilerSetupTree();

//
void bbcProfilerGetAllProfileRecords(bbcProfileRecord ***records,int *num_records);


#endif//bbcENABLE_PROFILER

#endif
