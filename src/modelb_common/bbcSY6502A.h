#ifndef IOP6502_H_
#define IOP6502_H_

#ifndef t65CPU_MANAGES_I_FLAG
#error "t65CPU_MANAGES_I_FLAG is mandatory"
#endif

//References:
//
//[0]	64doc
//		(1.8 1994/06/03 19:50:04 jopi Exp $)
//[1]	6502 Undocumented Opcodes, Based on the Atari 8-bit 6502C (CO14806)
//		Freddy Offenga (F.Offenga@student.kun.nl)
//[2]	Extra instructions of the 65xx cpu
//		Adam Vardy (abe0084@infonet.st-johns.nf.ca)
//		(http://www.s-direktnet.de/homepages/k_nadj/opcodes.html)

//#include "bbcComputer.h"//  [9/11/2003]
#include <t65Generic6502InstructionSet.h>
#include <t65Generic6502.h>
#include <t65Sim6502.h>
#include <t65WarningsPush.h>

//hmm wish I'd done this AGES ago.
#ifdef START_INSTR
//hack for VC7 -- to be investigated.
#undef START_INSTR
#endif
#ifdef END_INSTR
//hack for VC7 -- to be investigated.
#undef END_INSTR
#endif
#define START_INSTR(N) struct Instr##N {static const char *Mnemonic() {return #N "*";}
#define END_INSTR() };

#define DOUBLE_INSTR(M,I0,I1)\
	struct Instr##M:\
	public DoubleInstr<I0,I1>\
	{\
		static const char *Mnemonic() {\
			return #M "*";\
		}\
	}

template<class T>
struct bbcSY6502A:
public t65::Generic6502<T>
{
	typedef t65::Generic6502<T> B;
	typedef t65TYPENAME t65::Generic6502<T>::MachineType MachineType;

	//////////////////////////////////////////////////////////////////////////
	// Type class for undocumented instructions that are equivalent to a RMW
	// instr followed by a read one.
	template<class DI,class AM>
	struct RmwThenRead {
		typedef AM AddrModeType;
		typedef DI InstrType;

		static FINLINE void t65_CALL Execute(MachineType &state) {
			t65::Word addr=AM::WriteModeEA(state);
			t65::byte val=AM::ReadOperand(state,addr);
			AM::WriteOperand(state,addr,val);
			val=DI::InstrRmw::Execute(state,val);
			AM::WriteOperand(state,addr,val);
			//According to "6502 Undocumented opcodes" (F.Offenga@student.kun.nl,
			//Version 3.1, 6/28/1999) the read instruction doesn't take any extra time
			//compared to just doing the RMW one.
			//
			//Weird.
			DI::InstrRead::Execute(state,val);
		}
	};

	//Instruction class for use with the above.
	template<class I1,class I2>
	struct DoubleInstr {
		typedef I1 InstrRmw;
		typedef I2 InstrRead;
	};

	//Instructions that work like that
	DOUBLE_INSTR(ISB,t65TYPENAME t65::Generic6502<T>::InstrINC,t65TYPENAME t65::Generic6502<T>::InstrSBC);
	DOUBLE_INSTR(RLA,t65TYPENAME t65::Generic6502<T>::InstrROL,t65TYPENAME t65::Generic6502<T>::InstrAND);
	DOUBLE_INSTR(RRA,t65TYPENAME t65::Generic6502<T>::InstrROR,t65TYPENAME t65::Generic6502<T>::InstrADC);
	DOUBLE_INSTR(SLO,t65TYPENAME t65::Generic6502<T>::InstrASL,t65TYPENAME t65::Generic6502<T>::InstrORA);
	DOUBLE_INSTR(SRE,t65TYPENAME t65::Generic6502<T>::InstrLSR,t65TYPENAME t65::Generic6502<T>::InstrEOR);

	//////////////////////////////////////////////////////////////////////////
	//AND byte with accumulator. If result is negative then carry is
	//set. Status flags: N,Z,C
	START_INSTR(AAC)
		static FINLINE void t65_CALL Execute(MachineType &state,t65::byte val) {
			B::InstrAND::Execute(state,val);
			state.cpustate.p|=state.cpustate.a>>7;
		}
	END_INSTR()
		
	//////////////////////////////////////////////////////////////////////////
	//AND X register with accumulator and store result in memory.
	//Status flags: N,Z
	START_INSTR(AAX)
		static FINLINE t65::byte Execute(MachineType &state) {
			t65::byte v=state.cpustate.x&state.cpustate.a;
			//SetNZ(state,v);//see 6502 test suite and [2]
			return v;
		}
	END_INSTR()
		
	//////////////////////////////////////////////////////////////////////////
	//AND byte with accumulator, then shift right one bit in accumu-
	//lator. Status flags: N,Z,C
	START_INSTR(ASR)
		static FINLINE void t65_CALL Execute(MachineType &state,t65::byte val) {
			B::InstrAND::Execute(state,val);
			state.cpustate.a=B::InstrLSR::Execute(state,state.cpustate.a);
		}
	END_INSTR()

	//////////////////////////////////////////////////////////////////////////
	//STZ -- store zero
	//
	//allegedly...
	//AND Y register with the high byte of the target address of the
	//argument + 1. Store the result in memory.
	//
	//I've always seen it given as STZ for the Beeb, though...
	START_INSTR(STZ)
		static FINLINE t65::byte Execute(MachineType &) {
			return 0;
		}
	END_INSTR()

	//////////////////////////////////////////////////////////////////////////
	// LAX -- load A and X simultaneously, setting NZ.
	START_INSTR(LAX)
		static FINLINE void t65_CALL Execute(MachineType &state,t65::byte val) {
			state.cpustate.x=state.cpustate.a=val;
#ifdef bbcENABLE_BEEB_ICE
			state.ice.OnChangeX();
			state.ice.OnChangeA();
#endif
			SetNZ(state,val);
		}
	END_INSTR()

	//////////////////////////////////////////////////////////////////////////
	// DCP -- subtract 1 from memory without borrow
	// Only affects C flag -- can this REALLY be right?
	// TODO check. I assume C works like SBC, so set based on val!=0.
//	START_INSTR(DCP)
//		static FINLINE t65::byte Execute(MachineType &state,t65::byte val) {
//			state.cpustate.p&=~t65::C_MASK;
//			state.cpustate.p|=val!=0;//NB true/false 1/0 value used
//			return val-1;
//		}
//	END_INSTR()
	DOUBLE_INSTR(DCP,t65TYPENAME t65::Generic6502<T>::InstrDEC,t65TYPENAME t65::Generic6502<T>::InstrCMP);

	//////////////////////////////////////////////////////////////////////////
	// ARR -- AND followed by ROR A, with some wierd flag settings! 64doc
	// reckons it has some ADC aspects and is thus affected by the D flag.
	// I don't bother with that...
	//
	// (V ends up as per bit 6 of result. C set if (bit 5) xor (bit 6).)
	START_INSTR(ARR)
		static FINLINE void t65_CALL Execute(MachineType &state,t65::byte val) {
			state.cpustate.a=B::InstrROR::Execute(state,state.cpustate.a&val);
#ifdef bbcENABLE_BEEB_ICE
			state.ice.OnChangeA();
#endif
			//ROR set N+Z
			state.cpustate.p&=~(t65::C_MASK|t65::V_MASK);
			state.cpustate.p|=state.cpustate.p&t65::V_MASK;
			state.cpustate.p|=((state.cpustate.a^(state.cpustate.a<<1))>>6)&1;
#ifdef bbcENABLE_BEEB_ICE
			state.ice.OnChangeP();
#endif
		}
	END_INSTR()

	//////////////////////////////////////////////////////////////////////////
	// XAS	store (X&msb(EA))
	// SXA	store (X&msb(EA))
	// AXA	64doc says store (A&X&(msb(EA)+1))
	//		[1] says store (A&X&7) (also notes other instructions previously
	//		believed to &7 actually &(msb(EA)+1), this one got away?)
	// 
	// These are not easily done right now, because instructions don't have
	// access to the current effective address.
	// 
	// Since 9C ends up STZ, I'm assuming that this high byte of EA thing is
	// specific to cheap and crappy computers, and the quality workmanship
	// used for the Beeb means that it ends up 0. (STZ, 9C, [1] says
	// Y&msb(EA)+1))
	//
	// So, pending further testing these are ending up as STZ!

	//////////////////////////////////////////////////////////////////////////
	// AXS [1]/SBX 64doc -- X=(A&X)-M
	//
	// Sets N,Z,C like CMP, 64doc reckons a hideous amalgam of CMP and DEX!
	// (But without the DE bit.)
	START_INSTR(SBX)
		static FINLINE void t65_CALL Execute(MachineType &state,t65::byte val) {
			Compare(state,state.cpustate.a&state.cpustate.x,val);
			state.cpustate.x=(state.cpustate.a&state.cpustate.x)-val;
#ifdef bbcENABLE_BEEB_ICE
			state.ice.OnChangeX();
#endif
		}
	END_INSTR()

	//////////////////////////////////////////////////////////////////////////
	// LAR -- A=X=S=S&M, SetNZ(A)
	START_INSTR(LAR)
		static FINLINE void t65_CALL Execute(MachineType &state,t65::byte val) {
			state.cpustate.a=state.cpustate.x=state.cpustate.s.l=state.cpustate.s.l&val;
#ifdef bbcENABLE_BEEB_ICE
			state.ice.OnChangeA();
			state.ice.OnChangeX();
			state.ice.OnChangeS();
#endif
			SetNZ(state,state.cpustate.a);
		}
	END_INSTR()

	//////////////////////////////////////////////////////////////////////////
	// Unusual Instructions
	//
	// 8B (XAA [1]/ANE [0])
	// AB (ATX [1]/LXA [0])
	//
	// 64doc says A=(A|0xEE)&X&V for both (always 0xEE on 2MHz C128)
	// [1] says ATX is X=A=A&V,SetNZ(X); XAA is unknown.
	// 
	// Hmm... pending further testing will leave AB as per [1] and 8B as per [0].
	START_INSTR(ANE)
		static FINLINE void t65_CALL Execute(MachineType &state,t65::byte val) {
			state.cpustate.a=(state.cpustate.a|0xEE)&state.cpustate.x&val;
#ifdef bbcENABLE_BEEB_ICE
			state.ice.OnChangeA();
#endif
			SetNZ(state,state.cpustate.a);//I assume it does this!
		}
	END_INSTR()

	START_INSTR(ATX)
		static FINLINE void t65_CALL Execute(MachineType &state,t65::byte val) {
			state.cpustate.x=state.cpustate.a=state.cpustate.a&val;
#ifdef bbcENABLE_BEEB_ICE
			state.ice.OnChangeX();
#endif
			SetNZ(state,state.cpustate.x);
		}
	END_INSTR()
};

template<class T>
struct bbcSY6502AInstructionSet:
public t65::Generic6502InstructionSet<T>
{
	typedef t65TYPENAME t65::Generic6502InstructionSet<T>::P P;
	//Crazy undocumented opcodes

	//ISB -- INC-SBC
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrISB,t65TYPENAME P::ModeZPG> OpcodeE7;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrISB,t65TYPENAME P::ModeZPX> OpcodeF7;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrISB,t65TYPENAME P::ModeABS> OpcodeEF;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrISB,t65TYPENAME P::ModeABX> OpcodeFF;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrISB,t65TYPENAME P::ModeABY> OpcodeFB;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrISB,t65TYPENAME P::ModeINX> OpcodeE3;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrISB,t65TYPENAME P::ModeINY> OpcodeF3;

	//RLA -- ROL memory then ORA memory
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrRLA,t65TYPENAME P::ModeZPG> Opcode27;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrRLA,t65TYPENAME P::ModeZPX> Opcode37;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrRLA,t65TYPENAME P::ModeABS> Opcode2F;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrRLA,t65TYPENAME P::ModeABX> Opcode3F;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrRLA,t65TYPENAME P::ModeABY> Opcode3B;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrRLA,t65TYPENAME P::ModeINX> Opcode23;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrRLA,t65TYPENAME P::ModeINY> Opcode33;

	//RRA -- ROR memory then ADC memory
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrRRA,t65TYPENAME P::ModeZPG> Opcode67;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrRRA,t65TYPENAME P::ModeZPX> Opcode77;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrRRA,t65TYPENAME P::ModeABS> Opcode6F;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrRRA,t65TYPENAME P::ModeABX> Opcode7F;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrRRA,t65TYPENAME P::ModeABY> Opcode7B;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrRRA,t65TYPENAME P::ModeINX> Opcode63;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrRRA,t65TYPENAME P::ModeINY> Opcode73;

	//SBC -- Alternative form
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrSBC,t65TYPENAME P::ModeIMM> OpcodeEB;

	//SLO -- ASL memory then ORA memory
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrSLO,t65TYPENAME P::ModeZPG> Opcode07;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrSLO,t65TYPENAME P::ModeZPX> Opcode17;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrSLO,t65TYPENAME P::ModeABS> Opcode0F;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrSLO,t65TYPENAME P::ModeABX> Opcode1F;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrSLO,t65TYPENAME P::ModeABY> Opcode1B;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrSLO,t65TYPENAME P::ModeINX> Opcode03;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrSLO,t65TYPENAME P::ModeINY> Opcode13;

	//SRE -- LSR memory then EOR memory
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrSRE,t65TYPENAME P::ModeZPG> Opcode47;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrSRE,t65TYPENAME P::ModeZPX> Opcode57;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrSRE,t65TYPENAME P::ModeABS> Opcode4F;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrSRE,t65TYPENAME P::ModeABX> Opcode5F;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrSRE,t65TYPENAME P::ModeABY> Opcode5B;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrSRE,t65TYPENAME P::ModeINX> Opcode43;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrSRE,t65TYPENAME P::ModeINY> Opcode53;

	//NOP -- extra ones with funny modes!
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeZPG> Opcode04;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeZPX> Opcode14;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeZPX> Opcode34;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeZPG> Opcode44;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeZPX> Opcode54;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeZPG> Opcode64;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeZPX> Opcode74;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeIMM> Opcode80;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeIMM> Opcode82;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeIMM> Opcode89;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeIMM> OpcodeC2;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeZPX> OpcodeD4;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeIMM> OpcodeE2;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeZPX> OpcodeF4;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeABS> Opcode0C;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeABX> Opcode1C;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeABX> Opcode3C;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeABX> Opcode5C;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeABX> Opcode7C;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeABX> OpcodeDC;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrNOP,t65TYPENAME P::ModeABX> OpcodeFC;

	//NOP -- aliases for the normal one.
	typedef t65TYPENAME t65::Generic6502InstructionSet<T>::OpcodeEA Opcode1A;
	typedef t65TYPENAME t65::Generic6502InstructionSet<T>::OpcodeEA Opcode3A;
	typedef t65TYPENAME t65::Generic6502InstructionSet<T>::OpcodeEA Opcode5A;
	typedef t65TYPENAME t65::Generic6502InstructionSet<T>::OpcodeEA Opcode7A;
	typedef t65TYPENAME t65::Generic6502InstructionSet<T>::OpcodeEA OpcodeDA;
	typedef t65TYPENAME t65::Generic6502InstructionSet<T>::OpcodeEA OpcodeFA;
	
	//AAC
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrAAC,t65TYPENAME P::ModeIMM> Opcode0B;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrAAC,t65TYPENAME P::ModeIMM> Opcode2B;

	//AAX
	typedef t65TYPENAME P::template Write<t65TYPENAME P::InstrAAX,t65TYPENAME P::ModeZPG> Opcode87;
	typedef t65TYPENAME P::template Write<t65TYPENAME P::InstrAAX,t65TYPENAME P::ModeZPY> Opcode97;
	typedef t65TYPENAME P::template Write<t65TYPENAME P::InstrAAX,t65TYPENAME P::ModeINX> Opcode83;
	typedef t65TYPENAME P::template Write<t65TYPENAME P::InstrAAX,t65TYPENAME P::ModeABS> Opcode8F;

	//ASR
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrASR,t65TYPENAME P::ModeIMM> Opcode4B;

	//STZ
	typedef t65TYPENAME P::template Write<t65TYPENAME P::InstrSTZ,t65TYPENAME P::ModeABS> Opcode9C;

	//LAX
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrLAX,t65TYPENAME P::ModeINX> OpcodeA3;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrLAX,t65TYPENAME P::ModeZPG> OpcodeA7;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrLAX,t65TYPENAME P::ModeABS> OpcodeAF;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrLAX,t65TYPENAME P::ModeINY> OpcodeB3;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrLAX,t65TYPENAME P::ModeZPY> OpcodeB7;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrLAX,t65TYPENAME P::ModeABY> OpcodeBF;

	//DCP
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrDCP,t65TYPENAME P::ModeINX> OpcodeC3;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrDCP,t65TYPENAME P::ModeZPG> OpcodeC7;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrDCP,t65TYPENAME P::ModeABS> OpcodeCF;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrDCP,t65TYPENAME P::ModeINY> OpcodeD3;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrDCP,t65TYPENAME P::ModeZPX> OpcodeD7;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrDCP,t65TYPENAME P::ModeABY> OpcodeDB;
	typedef t65TYPENAME P::template RmwThenRead<t65TYPENAME P::InstrDCP,t65TYPENAME P::ModeABX> OpcodeDF;

	//ARR
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrARR,t65TYPENAME P::ModeIMM> Opcode6B;

	//XAS, SXA, AXA -- STZ on the Beeb I guess!
	typedef t65TYPENAME P::template Write<t65TYPENAME P::InstrSTZ,t65TYPENAME P::ModeINY> Opcode93;//AXA INY
	typedef t65TYPENAME P::template Write<t65TYPENAME P::InstrSTZ,t65TYPENAME P::ModeABY> Opcode9B;//XAS ABY
	typedef t65TYPENAME P::template Write<t65TYPENAME P::InstrSTZ,t65TYPENAME P::ModeABY> Opcode9E;//SXA ABY
	typedef t65TYPENAME P::template Write<t65TYPENAME P::InstrSTZ,t65TYPENAME P::ModeABY> Opcode9F;//AXA ABY

	//SBX
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrSBX,t65TYPENAME P::ModeIMM> OpcodeCB;

	//LAR
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrLAR,t65TYPENAME P::ModeIMM> OpcodeBB;

	//ANE/ATX/XAA/wierd
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrANE,t65TYPENAME P::ModeIMM> Opcode8B;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrATX,t65TYPENAME P::ModeIMM> OpcodeAB;
};

#include <t65WarningsPop.h>

#endif
