#ifndef FDCOPUSCHALLENGER_H_
#define FDCOPUSCHALLENGER_H_

#include "bbc1770Interface.h"

struct bbc1770OpusChallenger {
public:
	static const bbc1770Interface disc_interface_256k;
	static const bbc1770Interface disc_interface_512k;
	
	static void Init256k();
	static void Init512k();
	
	static t65::byte ReadJim(t65::word offset);
	static void WriteJim(t65::word offset,t65::byte val);
	static t65::byte ReadPagingLsb(t65::word);
	static void WritePagingLsb(t65::word,t65::byte val);
	static t65::byte ReadPagingMsb(t65::word);
	static void WritePagingMsb(t65::word,t65::byte val);
	static t65::byte ReadUnknownFred(t65::word);
	static void WriteUnknownFred(t65::word,t65::byte val);

	static void GetByteFromDriveControl(const bbc1770DriveControl &src,
		t65::byte *dest);
	static void GetDriveControlFromByte(t65::byte src,bbc1770DriveControl *dest);
	//private:
	static t65::Word paging_;
	static t65::byte ram_[];
	static unsigned ram_size_;

private:
	static int ByteIdx(t65::byte offset);
	static void InitCommon();

	static bbcSaveState::LoadStatus LoadSaveState(const bbcSaveState *save_state);
	static bbcSaveState::SaveStatus SaveSaveState(bbcSaveState *save_state);
};

#endif
