#ifndef SYSTEMVIA_H_
#define SYSTEMVIA_H_

#include "bbcVIA.h"
#include "bbcKeys.h"
#include "bbcSaveState.h"

struct bbcSystemVIASaveState1;

class bbcSystemVIA:
public bbcVIA
{
public:
	bbcSystemVIA();

	static t65::byte MmioRead(t65::word addr);
	static void MmioWrite(t65::word addr,t65::byte val);
	
	bool KeyState(bbcKey key);
	void SetKeyState(bbcKey key,bool state);
	void SetJoystickButtonState(int joystick,bool state);
	void ReadingPa();
    void ReadingPb();
    void WrittenPa();
    void WrittenPb();
	void DoKeyboardIrq();
	
	bool CapslockLedLit();
	bool ShiftlockLedLit();
protected:
	bbcSaveState::LoadStatus LoadExtraSaveState(const bbcSaveState *save_state);
	bbcSaveState::SaveStatus SaveExtraSaveState(bbcSaveState *save_state) const;
private:
	t65::byte latch_;
    static const t65::word wraptbl_[4];         // table for screen wrap sizes
	bool joystick_buttons_[2];

	void SlowWrite(t65::byte value);
};

extern bbcSystemVIA bbc_system_via;

// annoying thunks
bbcSaveState::LoadStatus bbcLoadSystemVIASaveState(const bbcSaveState *save_state);
bbcSaveState::SaveStatus bbcSaveSystemVIASaveState(bbcSaveState *save_state);

#endif
