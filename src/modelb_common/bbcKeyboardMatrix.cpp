#include "pch.h"
#include "bbcKeyboardMatrix.h"
#include "bbcKeys.h"
#include <utility>
#include <algorithm>

//#define VERBOSE_KEY_PRESSES

bool bbcKeyboardMatrix::keyflags_[256];
bool bbcKeyboardMatrix::force_shift_pressed_;
int bbcKeyboardMatrix::num_keys_down_;
bool bbcKeyboardMatrix::write_enabled_;
t65::byte bbcKeyboardMatrix::input_;
bool bbcKeyboardMatrix::manual_scan_flag_;

void bbcKeyboardMatrix::Init() {
	std::fill(keyflags_,keyflags_+sizeof keyflags_/sizeof keyflags_[0],false);
	force_shift_pressed_=false;
	num_keys_down_=0;
	write_enabled_=false;
	input_=0;
	manual_scan_flag_=false;
}

/*
// Originally from James Bonfield's emulator. Nearly 3 years later I guess
// it should be fixed, but I am in no hurry -- it seems to work OK.
void write_keyboard_matrix(BYTE *pa) {
    BYTE row,col,i;
    col=(*pa) & 0x0F;
    row=((*pa)>>4) & 7;
    // if matrix[row][col] is set, then PA7 is set
    if(bbc_key_flags[row][col]) {
        (*pa)|=0x80;
    } else {
        (*pa)&=0x7f;
    }
    //When a certain column has been selected via PA, a CA2 interrupt will
	//be generated. Of course by the time the OS is messing with PA, CA2
	//has been disabled. However the CA2 bit in the IFR still gets set so
	//the OS has an idea of what's going on
    for(i=1;i<16;i++) {
        if(bbc_key_flags[i][col]) {
            system_via.CA2(true);
        }
    }
}
*/

//return true if interrupt required.
//*value is port A pins, so bit 7 is set/cleared based on the up/down state
//of the key. in addition, if any of the keys in the selected column is
//pressed, a ca2 is generated.
//
//TODO	i suspect the keyboard scan behaviour uses this (or vice versa) so
//		there should be some merging of code at some point.

bool bbcKeyboardMatrix::NeedsIrq() {
	if(!write_enabled_) {
		//Auto scan
		//IRQ if owt pressed
		return NumKeysDown()>0;
	} else {
		//Manual scan
		//IRQ if any pressed in this column
		//Oh and set bit 7 too
		t65::byte code=input_&0x7f;

		if(force_shift_pressed_&&code==bbcKEY_SHIFT) {
			manual_scan_flag_=true;
		} else if(keyflags_[code]) {
			manual_scan_flag_=true;
		} else {
			manual_scan_flag_=false;
		}
		t65::byte basecode=input_&0xF;
		for(unsigned i=1;i<8;++i) {
			if(keyflags_[basecode|(i<<4)]) {
				return true;
			}
		}
		return false;
	}
}

/*
bool bbcKeyboardMatrix::Write(byte *value) {
	//set/clear bit7
	t65::byte code=*value&0x7f;
	if(force_boot_keylink_&&code==BBC_KEYLINKS+4) {
		*value|=0x80;
		force_boot_keylink_=false;
	} else if(keyflags_[code]) {
		*value|=0x80;
	} else {
		*value&=0x7F;
	}
	//ca2 if necessary. note row 0 does not affect ca2, just like the normal
	//scan.
	byte basecode=*value&0xF;
	for(unsigned i=1;i<8;++i) {
		if(keyflags_[basecode|(i<<4)]) {
			return true;
		}
	}
	return false;
}
*/
void bbcKeyboardMatrix::SetKeyState(t65::byte code,bool state) {
	int transition=(state?2:0)|(keyflags_[code]?1:0);
	keyflags_[code]=state;
	if(!(code&0x80)) {
		if(transition==2) {
			if(code&0xf0) {
				++num_keys_down_;
			}
#ifdef VERBOSE_KEY_PRESSES
			bbcPrintf("%s pressed.\n",bbcKeyNameFromCode(bbcKey(code)));
#endif
		} else if(transition==1) {
			//was pressed, isn't pressed
			if(code&0xf0) {
				--num_keys_down_;
				BASSERT(num_keys_down_>=0);
			}
#ifdef VERBOSE_KEY_PRESSES
			bbcPrintf("%s released.\n",bbcKeyNameFromCode(bbcKey(code)));
#endif
		}
	}

	//check
#ifdef bbcDEBUG_ENABLE
	int n=0;
	for(unsigned i=0;i<127;++i) {
		if(i&0xf0) {
			if(keyflags_[i]) {
				++n;
			}
		}
	}
	BASSERT(n==num_keys_down_);
#endif

	if(num_keys_down_>0) {
		SetForceShiftPressed(false);
	}
}

//////////////////////////////////////////////////////////////////////////

struct bbcKeyboardMatrixSaveState1 {
	bbcSaveStateByte keyflags[256];
	bbcSaveStateByte manual_scan_flag;
	bbcSaveStateByte write_enabled;
	bbcSaveStateByte input;
	bbcSaveStateByte force_shift_pressed;
	bbcSaveStateInt32 num_keys_down;
};

static const bbcSaveStateItemId bbc_keyboard_matrix_save_state_item_id("keybdmtx");

bbcSaveState::LoadStatus bbcKeyboardMatrix::LoadSaveState(const bbcSaveState *save_state) {
	bbcSaveState::LoadStatus status;
	if(const bbcKeyboardMatrixSaveState1 *data=save_state->FindItem<bbcKeyboardMatrixSaveState1>(
		bbc_keyboard_matrix_save_state_item_id,0,1,&status))
	{
		for(int i=0;i<256;++i) {
			keyflags_[i]=!!data->keyflags[i];
		}

		manual_scan_flag_=!!data->manual_scan_flag;
		write_enabled_=!!data->write_enabled;
		input_=data->input;
		force_shift_pressed_=!!data->force_shift_pressed;
		num_keys_down_=data->num_keys_down;
	} else {
		return status;
	}

	return bbcSaveState::LS_OK;
}

bbcSaveState::SaveStatus bbcKeyboardMatrix::SaveSaveState(bbcSaveState *save_state) {
	bbcKeyboardMatrixSaveState1 data;

	for(int i=0;i<256;++i) {
		data.keyflags[i]=keyflags_[i];
	}

	data.manual_scan_flag=manual_scan_flag_;
	data.write_enabled=write_enabled_;
	data.input=input_;
	data.force_shift_pressed=force_shift_pressed_;
	data.num_keys_down=num_keys_down_;

	save_state->SetItem(bbc_keyboard_matrix_save_state_item_id,0,1,&data);

	return bbcSaveState::SS_OK;
}
