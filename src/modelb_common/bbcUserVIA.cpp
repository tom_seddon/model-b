#include "pch.h"
#include "bbcComputer.h"
#include "bbcUserVIA.h"
#include <stdlib.h>

static const bbcSaveStateItemId bbc_user_via_save_state_item_id("busr_via");
bbcUserVIA bbc_user_via;

bbcUserVIA::bbcUserVIA():
bbcVIA(IRQ_USERVIA,"User")
{
}

t65::byte bbcUserVIA::MmioRead(t65::word addr) {
	return bbc_user_via.Read(addr);
}

void bbcUserVIA::MmioWrite(t65::word addr,t65::byte val) {
	bbc_user_via.Write(addr,val);
}

#ifdef bbcUSER_VIA_PB_RANDOMNESS
void bbcUserVIA::ReadingPb() {
	pb_=rand()&0xff;
}
#endif

bbcSaveState::LoadStatus bbcLoadUserVIASaveState(const bbcSaveState *save_state) {
	return bbc_user_via.LoadSaveState(save_state);
}

bbcSaveState::SaveStatus bbcSaveUserVIASaveState(bbcSaveState *save_state) {
	return bbc_user_via.SaveSaveState(save_state);
}
