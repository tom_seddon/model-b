#include "pch.h"

// deprecated

//#include "bbcCmos.h"
//#include <memory.h>
//#include <stdlib.h>
//#include <stdio.h>
//
//#define FALLING_EDGE(hist) (((hist)&3)==2)
//#define RISING_EDGE(hist) (((hist)&3)==1)
//
//t65::byte bbcCmos::contents_[64];
//int bbcCmos::sec_;
//int bbcCmos::min_;
//int bbcCmos::hr24_;
//int bbcCmos::weekday_;
//int bbcCmos::day_;
//int bbcCmos::month_;
//int bbcCmos::year_;
//bool bbcCmos::is_24_hr_;
//bool bbcCmos::is_bcd_;
//t65::dword bbcCmos::ds_history_;
//t65::dword bbcCmos::as_history_;
//t65::byte bbcCmos::addr_;
//bool bbcCmos::ce_;
//bool bbcCmos::is_rd_op_;
//
//int *const bbcCmos::clock_registers_[10]={
//	&sec_,
//	0,
//	&min_,
//	0,
//	&hr24_,
//	0,
//	&weekday_,
//	&day_,
//	&month_,
//	&year_,
//};
//
//void bbcCmos::Init() {
//	memset(contents_,0,sizeof contents_);
//
//	sec_=0;
//	min_=0;
//	hr24_=0;
//	weekday_=1;
//	day_=1;
//	month_=1;
//	year_=0;
//
//	is_bcd_=true;
//	is_24_hr_=true;
//
//	ds_history_=0;
//	as_history_=0;
//
//	is_rd_op_=true;
//	ce_=false;
//
//	addr_=0;
//}
//
//void bbcCmos::SetTime(int sec,int min,int hr24,int weekday,int day,int month,int year) {
//	sec_=sec;
//	min_=min;
//	hr24_=hr24;
//	weekday_=weekday;
//	day_=day;
//	month_=month;
//	year_=year;
//}
//
//t65::byte bbcCmos::DataPins() {
//	t65::byte result=0;
//
//	if(ce_) {
//		if(is_rd_op_) {
//			if(ds_history_&1) {//RISING_EDGE(ds_history_)) {
//				BASSERT(!(addr_&(~63)));
//				if(addr_<10) {
//					t65::byte setbits=0;
//					int v=0;
//
//					//fetch value, if there is one
//					if(clock_registers_[addr_]) {
//						v=*clock_registers_[addr_];
//					}
//					
//					//fix up for 12 hour mode if required
//					if(addr_==4&&!is_24_hr_) {
//						++v;
//						if(v>12) {
//							setbits=0x80;
//							v-=12;
//						}
//					}
//
//					if(is_bcd_) {
//						int lsn=abs(v%10);
//						int msn=abs(v/10%10*16);
//
//						v=lsn|msn;
//					}
//
//					result=v;
//
//				} else if(addr_>=14) {
//					result=contents_[addr_];
//				}
//				printf("CMOS: +%u: read %u\n",addr_,result);
//			}
//		}
//	}
//
//	return result;
//}
//
//void bbcCmos::SetDataPins(t65::byte pins) {
//	if(!ce_) {
//		return;
//	}
//
//	if(as_history_&1) {//RISING_EDGE(as_history_)) {
//		addr_=pins&63;
//		printf("CMOS: addr now %u\n",addr_);
//	}
//
//	if(!is_rd_op_) {
//		if(!(ds_history_)) {//FALLING_EDGE(ds_history_)) {
//			//Latch data
//			printf("CMOS: +%u: write %u\n",addr_,pins);
//			if(addr_>=14) {
//				contents_[addr_]=pins;
//			}
//		}
//	}
//}
//
///*
//void bbcCmos::SetInputs(bool ds,bool as,bool is_rd,bool chipen,t65::byte pins) {
//	if(!chipen) {
//		return;
//	}
//
//	//ds edges
//	if(ds&&!ds_) {
//		//+ -- present data on pins
//	} else if(!ds&&ds_) {
//		//- -- latch data
//		if(addr_>=14) {
//			contents_[addr]=pins;
//		}
//	}
//
//	//as edges
//	if(as&&!as_) {
//		//+ -- if !is_rd, latch address
//		if(!is_rd) {
//			addr_=pins;
//			addr_&=63;
//		}
//	} else if(!as&&as_) {
//		//- -- nothing?
//	}
//
//	ds_=ds;
//	as_=as;
//}
//*/
