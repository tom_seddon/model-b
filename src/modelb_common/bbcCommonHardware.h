#ifndef bbcCOMMONHARDWARE_H_
#define bbcCOMMONHARDWARE_H_

// includes common stuff needed by derived classes for bbcModel::Update
// ho hum.

#include "bbcVideo.h"
#include "bbcSystemVIA.h"
#include "bbcUserVIA.h"
#include "bbc1770.h"
#include "bbcAdc.h"
#ifdef bbcSERIAL_ENABLE
#include "bbcAcia.h"
#endif

#endif
