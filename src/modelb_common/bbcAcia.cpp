#include "pch.h"
#include "bbcAcia.h"
#include "bbcComputer.h"
#include <stdio.h>
#include <ctype.h>

//0x56
//%0101 0110 = 0 10 101 10
//
//	clock/64 (300 baud)
//	8 bits + 1 stop bit
//	RTS=high, transmitting IRQ disabled
//	Receive interrupt disabled.

//////////////////////////////////////////////////////////////////////////
// SR bits
enum {
	SR_RDRF=1,//Receive Data Register Full
	SR_TDRE=2,//Transmit Data Register Empty
	SR_DCD=4,//Data Carrier Detect
	SR_CTS=8,//Clear To Send
	SR_FE=16,//Framing Error
	SR_OVRN=32,//receiver OVeRruN
	SR_PE=64,//Parity Error
	SR_IRQ=128,//Interrupt ReQuest
};

//////////////////////////////////////////////////////////////////////////
// Parity
enum Parity {
	NO_PARITY,
	EVEN_PARITY,
	ODD_PARITY,
};

static const char *parity_names[]={
	"none",
	"even",
	"odd",
};

//////////////////////////////////////////////////////////////////////////
// Byte encoding
struct Encoding {
	int num_bits;
	Parity parity;
	int num_stop_bits;
};

static const Encoding acia_encodings[8]={
	{7,EVEN_PARITY,2},
	{7,ODD_PARITY,2},
	{7,EVEN_PARITY,1},
	{7,ODD_PARITY,1},
	{8,NO_PARITY,2},
	{8,NO_PARITY,1},
	{8,EVEN_PARITY,1},
	{8,ODD_PARITY,1},
};

#define CURRENT_ENCODING() (&acia_encodings[(cr>>2)&7])
#define ENC_EXTRA_BITS(E) ((E)->num_stop_bits+((E)->parity==NO_PARITY?0:1)))
#define ENC_NUM_BITS(E) ((E)->num_bits+ENC_EXTRA_BITS(E))

//////////////////////////////////////////////////////////////////////////
// Lookup tables

//see AUG p392
//note that the AUG table is the wrong way round.
static const int ula_baud_rates[8]={
	19200,//%000
	1200,//%001
	4800,//%010
	150,//%011
	9600,//%100
	300,//%101
	2400,//%110
	75,//%111
};

static const int acia_clock_divisors[4]={
	1,
	16,
	64,
	0,//master reset (erm... don't divide by this :)
};

//////////////////////////////////////////////////////////////////////////
enum State {
	STATE_RX_RS423,
	STATE_TX_RS423,
	STATE_RX_TAPE,
	STATE_TX_TAPE,
	STATE_RESET,
	STATE_IDLE,
};

typedef void (*StateUpdateFn)(int cycles);

static void UpdateStateReset(int cycles);
static void UpdateStateRxRs423(int cycles);
static void UpdateStateTxRs423(int cycles);
static void UpdateStateRxTape(int cycles);
static void UpdateStateTxTape(int cycles);
static void UpdateStateIdle(int cycles);

static const StateUpdateFn state_update_fns[]={
	&UpdateStateRxRs423,
	&UpdateStateTxRs423,
	&UpdateStateRxTape,
	&UpdateStateTxTape,
	&UpdateStateReset,
	&UpdateStateIdle,
};

//////////////////////////////////////////////////////////////////////////

struct ShiftState {
	t65::byte val;
	int stop_left;
	int data_left;
	int start_left;
	Parity parity;
};

static bool tape;
static t65::byte tdr;
static t65::byte rdr;
static t65::byte cr;
static t65::byte status;
static t65::byte ula;
static bool even_parity[256];
static int next_stop;
static ShiftState shift;
static State state;

//////////////////////////////////////////////////////////////////////////

static void ResetShiftState() {
	shift.val=0;
	shift.stop_left=0;
	shift.start_left=0;
	shift.parity=NO_PARITY;
	shift.data_left=0;
}

static void SetNextStop(int when) {
	next_stop=when;
	bbcComputer::SetNextStop(next_stop);
}

static void SetState(State new_state) {
	if(new_state!=state) {
		bbcPrintf("%s: setstate: %d -> %d\n",__FUNCTION__,state,new_state);
	}
	state=new_state;
	//SetNextStop(bbcComputer::Cycles2MHz());
}

//TODO this is so silly, should be 2 functions.
static void SetIrqState(bool v) {
	if(v) {
		bbcComputer::irq_flags_|=IRQ_ACIA;
		status|=SR_IRQ;
	} else {
		bbcComputer::irq_flags_&=~IRQ_ACIA;
		status&=~SR_IRQ;
	}
}

void bbcAciaInit() {
	for(unsigned i=0;i<256;++i) {
		unsigned nset=0;

		for(unsigned j=0;j<8;++j) {
			nset+=(i>>j)&1;
		}

		even_parity[i]=!(nset&1);
	}
}

void bbcAciaReset() {
	ula=0;
	tape=false;

	SetState(STATE_RESET);
}

void bbcAciaUpdate() {
	const int cycles=bbcComputer::Cycles2MHz();//11/12/2004 -- 
	if(cycles-next_stop>=0) {
		(*state_update_fns[state])(cycles);
	} else {
		//Resubmit the stop point. A bit grim.
		SetNextStop(next_stop);
	}
}

t65::byte bbcAciaReadStatus(t65::word addr) {
	(void)addr;

	return status;
}

void bbcAciaWriteControl(t65::word addr,t65::byte val) {
	(void)addr;

	bool change=cr!=val;

	cr=val;
	if(change) {
		bbcPrintf("%s: @ %04X: wrote 0x%02X (%03d): ",__FUNCTION__,bbcComputer::cpustate.pc.w,val,val);
		const Encoding *enc=CURRENT_ENCODING();
		bbcPrintf("bits: %d+%d parity: %-4s ",enc->num_bits,enc->num_stop_bits,parity_names[enc->parity]);
		bbcPrintf("clock: /%d",acia_clock_divisors[val&3]);
		bbcPrintf("\n");
	}
	cr=val;

//	const Encoding *enc=CURRENT_ENCODING();
	if(acia_clock_divisors[val&3]==0) {
		//Master reset
		bbcPrintf("%s: Master reset\n",__FUNCTION__);

		SetState(STATE_RESET);
		SetNextStop(bbcComputer::Cycles2MHz());
		return;
	}

	bool rts=false;
	switch((cr>>5)&3) {
	case 3:
		if(change) {
			bbcPrintf("%s: transmitting break level\n",__FUNCTION__);
		}
		break;
	case 2:
		rts=true;
		break;
	case 1:
		if(change) {
			bbcPrintf("%s: transmit irq enabled\n",__FUNCTION__);
		}
		break;
	}

	if(change) {
		bbcPrintf("%s: RTS line is %d\n",__FUNCTION__,rts);
	}
}

t65::byte bbcAciaReadRdr(t65::word addr) {
	(void)addr;

	status&=~(SR_RDRF|SR_OVRN);
	return rdr;
}

void bbcAciaWriteTdr(t65::word addr,t65::byte val) {
	(void)addr;

	bbcPrintf("%s: @ %04X: wrote 0x%02X (%03d) ('%c')\n",__FUNCTION__,bbcComputer::cpustate.pc.w,val,val,isprint(val)?val:' ');
	if(status&SR_TDRE) {
		tdr=val;
		status&=~SR_TDRE;
		SetIrqState(false);
		SetState(tape?STATE_TX_TAPE:STATE_TX_RS423);
	}
}

//blah: wrote 0x%02X (000) Tx: 19200 baud Rx: 19200 baud Input: RS423 Motor: OFF
void bbcAciaWriteSerialUla(t65::word addr,t65::byte val) {
	(void)addr;

	bbcPrintf("%s: @ %04X: wrote 0x%02X (%03d): Tx: %-5d baud Rx: %-5d baud Input: %-5s Motor: %-3s\n",
		__FUNCTION__,bbcComputer::cpustate.pc.w,val,val,ula_baud_rates[val&7],ula_baud_rates[(val>>3)&7],
		val&0x40?"RS423":"tape",val&0x80?"on":"off");

	ula=val;
	tape=!(ula&0x40);
	if(tape) {
		status&=~SR_CTS;
	} else {
		status&=~SR_DCD;
	}
}

static void UpdateStateReset(int cycles) {
	(void)cycles;

	bbcPrintf("%s\n",__FUNCTION__);
	cr=0;
	tdr=0;
	rdr=0;
	status=SR_TDRE;
	SetIrqState(false);
	ResetShiftState();

	state=STATE_IDLE;
}

static void UpdateStateRxRs423(int cycles) {
	(void)cycles;
}

static void Output(bool bit,const char *what) {
	bbcPrintf("Output: level=%d what=%s\n",bit,what);
}

static void UpdateStateTxRs423(int cycles) {
	if(shift.start_left>0) {
		Output(1,"start bit");
		--shift.start_left;
	} else if(shift.parity!=NO_PARITY) {
		if(shift.parity==EVEN_PARITY) {
			Output(even_parity[shift.val],"parity");
		} else {
			Output(!even_parity[shift.val],"parity");
		}
		shift.parity=NO_PARITY;
	} else if(shift.data_left>0) {
		Output(shift.val&1,"data");
		shift.val>>=1;
		--shift.data_left;
	} else if(shift.stop_left>0) {
		Output(1,"stop");
		--shift.stop_left;
	} else {
//		Output(0,"no output");
		if(!(status&SR_TDRE)) {
			const Encoding *enc=CURRENT_ENCODING();

			shift.data_left=enc->num_bits;
			shift.parity=enc->parity;
			shift.start_left=1;
			shift.stop_left=enc->num_stop_bits;
			shift.val=tdr;
			status|=SR_TDRE;
			if(((cr>>5)&3)==1) {
				bbcPrintf("%s: setting ACIA irq #%d\n",__FUNCTION__,cycles);
				SetIrqState(true);
			}

			bbcPrintf("%s: about to transfer %d (0x%02X) ('%c')\n",__FUNCTION__,tdr,tdr,isprint(tdr)?tdr:' ');
		}
//		else {
//			bbcPrintf(__FUNCTION__ ": returning to idle #%d\n",cycles);
//			ResetShiftState();
//			SetState(STATE_IDLE);
//			return;
//		}
	}
//	bbcPrintf(__FUNCTION__ ": next stop #+%d\n",2000000/ula_baud_rates[ula&7]);
	SetNextStop(cycles+2000000/ula_baud_rates[ula&7]);
}

static void UpdateStateRxTape(int cycles) {
	(void)cycles;
}

static void UpdateStateTxTape(int cycles) {
	(void)cycles;
}

static void UpdateStateIdle(int cycles) {
	//Do nothing.
	//It is idle.
	SetIrqState(false);
	SetNextStop(cycles+2000000);
}

#ifndef bbcSERIAL_ENABLE
t65::byte bbcAciaReadStatusDummy(t65::word addr) {
	(void)addr;
	return 2;
}
#endif
