#ifndef bbcDISCINTERFACE_H_
#define bbcDISCINTERFACE_H_

//#include "bbcComputer.h"
//#include <vector>
//
//// Disc interface for beeb
////
//// good for 8271 or 1770 we hope.
//class bbcDiscInterface {
//public:
//	virtual ~bbcDiscInterface();
//
//	//GetMmioFnInits
//	//Get all the MmioFnInits this interface requires.
//	//This will probably be all those required for the basic type of
//	//disc interface chip followed by any other ones (eg challenger ram)
//	virtual void GetMmioFnInits(std::vector<bbcMmioFnInit> *fn_inits)=0;
//	
//	//Name and long name
//	//'name' is how it is referred to in the INI file
//	//'long_name' is more descriptive.
//	const char *const name;
//	const char *const long_name;
//protected:
//	bbcDiscInterface(const char *name,const char *long_name);
//private:
//	//noncopyable by virtue of the consts.
//};
//
////Returns a pointer to one of the above given the name of an interface type.
//bbcDiscInterface *bbcDiscInterfaceGetFromName(const char *name);

#endif
