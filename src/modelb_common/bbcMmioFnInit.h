#ifndef BBCMMIOFNINIT_H_
#define BBCMMIOFNINIT_H_

//////////////////////////////////////////////////////////////////////////
// functions for reading and writing memory mapped i/o

typedef void (*bbcWriteMmioFn)(t65::word addr,t65::byte value);
typedef t65::byte (*bbcReadMmioFn)(t65::word addr);

struct bbcMmioFnInit {
	t65::word start;
	unsigned count;//count==0 for last entry
	bbcReadMmioFn read_fn;
	bbcWriteMmioFn write_fn;
};

#endif
