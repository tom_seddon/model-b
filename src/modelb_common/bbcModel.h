#ifndef bbcMODEL_H_
#define bbcMODEL_H_

#include "bbcMmioFnInit.h"
//#include "bbcDebugTrace.h"
#include "bbcEventRecord.h"
#include <t65Disassembler6502.h>
#include "bbcSaveState.h"

enum bbcModelFlags {
	bbcMODEL_NO_KEYBOARD_LINKS=1,//not sure this is the best place for it ("no" to minimize number of changes)
};

//////////////////////////////////////////////////////////////////////////
// bbcModel events.
#ifdef bbcDEBUG_TRACE_EVENTS

#include "bbcVIA.h"

//cpu event

class bbcModelCpuEventHandler:
public bbcEventHandler
{
public:
	struct Data {
		t65::Word pc;//pc
		t65::byte bytes[3];//bytes making up this instruction
		t65::byte a,x,y,p,s;//registers
	};

	static const bbcModelCpuEventHandler handler;

	bbcModelCpuEventHandler();

	void FillEventData(void *event) const;

	void AppendEventStr(const bbcEventInfo *info,const void *event,int indent,void *output) const;
protected:
private:
};

class bbcModelIrqEventHandler:
public bbcEventHandler
{
public:
	struct Data {
		int irqflags;
		bbcVIA::TraceState sys,usr;
		t65::byte fdc_status;//about 40 (!!!!!) bytes total
	};

	static const bbcModelIrqEventHandler handler;

	bbcModelIrqEventHandler();

	void FillEventData(void *event) const;

	void AppendEventStr(const bbcEventInfo *info,const void *event,int indent,void *output) const;
protected:
private:
	void AppendViaState(const bbcVIA::TraceState &state,const char *name,int indent,void *output) const;
	void AppendViaFlagsState(t65::byte val,const char *name,int indent,void *output) const;
};

#endif//bbcDEBUG_TRACE_EVENTS

//////////////////////////////////////////////////////////////////////////
//
struct bbcInstrInfo {
	const t65::DisassemblerInstrInfo *dii;
	unsigned execution_count;
};

//////////////////////////////////////////////////////////////////////////
// bbcModel
//
// Base class for a BBC type.
class bbcModel {
public:
	const unsigned flags;

	virtual ~bbcModel();

	virtual void Init() const=0;
	//5/12/2004 -- removing InitialiseCpuState (suspect real 6502 starts up with gibberish)
//	virtual void InitialiseCpuState() const=0;
	virtual void Reset6502() const=0;
	virtual void Run() const=0;
#ifdef bbcUPDATE_IN_BBCMODEL
	virtual void Update() const=0;
#endif

	//disassembly
	virtual t65::Word Disassemble(t65::Word base,char *opcode_buf,char *operand_buf,t65::byte *bytes_buf) const=0;
	virtual int DisassembleBytes(const t65::byte *bytes,t65::Word bytes_addr,char *opcode_buf,char *operand_buf) const=0;

	//return pointer to table containing extra MMIO fns for this type of BBC. They're added on top of the usual
	//BBC suspects (VIA, ADC, etc.)
	virtual const bbcMmioFnInit *ExtraMmioFns() const=0;

	//read byte
	virtual t65::byte ReadByte(t65::Word addr) const=0;
	t65::byte ReadByte(t65::word addr) const;

	//retrieve information about the given instruction for this type's instruction set.
	virtual void GetInstrInfo(t65::byte instr,bbcInstrInfo *instr_info) const=0;

//	virtual bbcSaveState::LoadStatus LoadSaveState(const bbcSaveState *save_state);
//	virtual bbcSaveState::SaveStatus SaveSaveState(bbcSaveState *save_state);

	virtual const bbcSaveStateHandler *GetSaveStateHandlers() const;

protected:
	bbcModel(unsigned flags);
private:
	bbcModel(const bbcModel &);
	bbcModel &operator=(const bbcModel &);
};

inline t65::byte bbcModel::ReadByte(t65::word addr) const {
	t65::Word tmp;

	tmp.w=addr;
	return this->ReadByte(tmp);
}

#endif
