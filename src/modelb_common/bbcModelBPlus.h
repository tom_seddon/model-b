#ifndef bbcMODELBPLUS_H_
#define bbcMODELBPLUS_H_

//B+ support
//
//The ram_ memory map is as follows:
//
//	+0x00000	BBC RAM
//	+0x08000	12K paged RAM
//	+0x0B000	Shadow video RAM (@ 3000-7FFF incl)
//	+0x10000	Unused
//	+0x20000	END

#include "bbcSY6502A.h"
#include "bbcModelGeneric.h"
#include "bbcComputer.h"

struct bbcModelBPlusConfig {
	typedef bbcComputer MachineType;

	//////////////////////////////////////////////////////////////////////////
	// Memory access -- which form of memory access should be used for each
	// access category?

	// ReadZP: how the processor reads bytes from zero page
	typedef MachineType::ReadNoHW ReadZP;

	// WriteZP: how the processor writes bytes to zero page
	typedef MachineType::WriteNoHW WriteZP;

	// ReadStack: how the processor reads bytes from the stack
	typedef MachineType::ReadNoHW ReadStack;

	// WriteStack: how the processor write bytes to the stack
	typedef MachineType::WriteNoHW WriteStack;

	// FetchInstr: how the processor reads bytes whilst fetching the bytes
	// that make up an instruction
	typedef MachineType::ReadExecute FetchInstr;

	// FetchAddress: how the processor reads an indirect address from
	// main memory, e.g. during IRQ processing or when doing the indirect
	// memory mode.
	typedef MachineType::ReadWithHwAndShadowRam FetchAddress;

	// ReadOperand: how the processor reads bytes when reading an
	// instruction's operand
	typedef MachineType::ReadWithHwAndShadowRam ReadOperand;

	// WriteOperand: how the processor writes bytes when writing an
	// instruction's operand
	typedef MachineType::WriteWithHwAndShadowRam WriteOperand;

	// ReadDebug: how to read a byte in debug mode (no update hardware
	// or signal interrupts etc.)
	typedef MachineType::ReadNoHW ReadDebug;

	// WriteDebug: how to write a byte in debug mode (no update hardware
	// or signal interrupts etc.)
	typedef MachineType::WriteNoHW WriteDebug;

};

typedef bbcSY6502A<bbcModelBPlusConfig> bbcModelBPlusCpu;
typedef bbcSY6502AInstructionSet<bbcModelBPlusCpu> bbcModelBPlusInstructionSet;
typedef t65::Sim6502<bbcModelBPlusCpu,bbcModelBPlusInstructionSet> bbcModelBPlusSim;

class bbcModelBPlus:
public bbcModelGeneric<bbcModelBPlusSim>
{
public:
#ifdef bbcUPDATE_IN_BBCMODEL
	void Update() const;
#endif
	const bbcMmioFnInit *ExtraMmioFns() const;

	static const bbcModelBPlus model;
};

#endif
