#ifndef BBCACIA_H_
#define BBCACIA_H_

//trying out new pluginy style.

void bbcAciaInit();
void bbcAciaReset();
void bbcAciaUpdate();

t65::byte bbcAciaReadStatus(t65::word addr);
void bbcAciaWriteControl(t65::word addr,t65::byte val);
t65::byte bbcAciaReadRdr(t65::word addr);
void bbcAciaWriteTdr(t65::word addr,t65::byte val);
void bbcAciaWriteSerialUla(t65::word addr,t65::byte val);

#ifndef bbcSERIAL_ENABLE
t65::byte bbcAciaReadStatusDummy(t65::word addr);
#endif

#endif
