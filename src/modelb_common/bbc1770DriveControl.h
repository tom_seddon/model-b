#ifndef BEEB1770DRIVECONTROL_H_
#define BEEB1770DRIVECONTROL_H_

struct bbc1770DriveControl {
	int drive;
	int side;
	bool is_double_density;
	bool disable_irq;
	
	bbc1770DriveControl();
};

#endif
