#include "pch.h"
#include "bbcComputer.h"
#include "bbcSystemVIA.h"
#include "bbcSound.h"
#include "bbcKeys.h"
#include "bbcKeyboardMatrix.h"
#include "bbcVideo.h"
#include <time.h>

static const bbcSaveStateItemId bbc_system_via_save_state_item_id("bsys_via");

bbcSystemVIA bbc_system_via;

const t65::word bbcSystemVIA::wraptbl_[4]={
    0x4000,
    0x2000,
    0x5000,
    0x2800,
};

bbcSystemVIA::bbcSystemVIA():
bbcVIA(IRQ_SYSVIA,"SysB")
{
	latch_=0;

	joystick_buttons_[0]=false;
	joystick_buttons_[1]=false;
}

// The top 4 input bits on PB are for the joystick and speech system.
// %1111 means 'do nothing'.
void bbcSystemVIA::ReadingPb() {
	//speech -- do nothing
	pb_|=0xC0;

	//AUG doesn't say which way round these are :)
	pb_&=~0x30;
	if(!joystick_buttons_[0]) {
		pb_|=0x10;
	}
	if(!joystick_buttons_[1]) {
		pb_|=0x20;
	}
}

void bbcSystemVIA::ReadingPa() {
	if(!(latch_&8)) {
		pa_&=0x7F;
		if(bbcKeyboardMatrix::ManualScanFlag()) {
			pa_|=0x80;
		}
	}
}

// Presumably multiple devices can be written to?
void bbcSystemVIA::WrittenPa() {
	this->SlowWrite(pa_);
}

// Port B on the system VIA is attached to the addressable latch. PB[012]
// supply the bit number, 0-7, and PB3 supplies the new value for that
// bit.
// This code is based on the code from James Bonfield's emulator.
void bbcSystemVIA::WrittenPb() {
	//Firstly, handle latch.
	t65::byte mask=1<<(pb_&7);
	latch_&=~mask;
	if(pb_&8) {
		latch_|=mask;
	}

	//Set keyboard mode from that
	bbcKeyboardMatrix::SetWriteEnabled(!(latch_&8));

	bbcVideo::SetScreenWrapSize(wraptbl_[(latch_>>4)&3]);

	this->SlowWrite(pa_);
}

void bbcSystemVIA::SlowWrite(t65::byte value) {
	//sound
	if(!(latch_&1)) {
		bbcSound::Write(value);
	}

	//keyboard
	if(!(latch_&8)) {
		bbcKeyboardMatrix::SetInput(value);

		if(bbcKeyboardMatrix::NeedsIrq()) {
			this->CA2(true);
		}
	}
}

bool bbcSystemVIA::CapslockLedLit() {
	return !(latch_&64);
}

bool bbcSystemVIA::ShiftlockLedLit() {
	return !(latch_&128);
}


//this is the keyboard scan type affair -- it only occurs when the keyboard is
//not enabled for write.
//causes a +ve active edge on CA2 if the keyboard wants it.
//don't ask me how i know it's a +ve one -- i think i looked at the relevant
//via register to work out what's required to get the interrupt to fire.
//
// fix -- keystate updated at any time, only when disabled for write will
// the interrupt fire though. [19/3/2003]
void bbcSystemVIA::SetKeyState(bbcKey key,bool state) {
	bbcKeyboardMatrix::SetKeyState((t65::byte)key,state);
//	if(latch[3]&&KeyboardMatrix::NeedsIrq()) {
//		CA2(true);
//	}
}

//	if(latch[3]) {
//		if(KeyboardMatrix::SetKeyState(key,state)) {
//			CA2(true);
//		}
//	}
//}

void bbcSystemVIA::DoKeyboardIrq() {
	if(bbcKeyboardMatrix::NeedsIrq()) {
		this->CA2(true);
	}
}

void bbcSystemVIA::SetJoystickButtonState(int joystick,bool state) {
	BASSERT(joystick==0||joystick==1);
	joystick_buttons_[joystick]=state;
}

t65::byte bbcSystemVIA::MmioRead(t65::word addr) {
	return bbc_system_via.Read(addr);
}

void bbcSystemVIA::MmioWrite(t65::word addr,t65::byte val) {
	bbc_system_via.Write(addr,val);
}

//////////////////////////////////////////////////////////////////////////

//	t65::byte latch_;
//    static const t65::word wraptbl_[4];         // table for screen wrap sizes
//	bool joystick_buttons_[2];

#pragma pack(push,1)
struct bbcSystemVIASaveState1 {
	bbcSaveStateByte latch;
	bbcSaveStateByte joystick_buttons;//bit N is button N
};
#pragma pack(pop)

bbcSaveState::LoadStatus bbcSystemVIA::LoadExtraSaveState(const bbcSaveState *save_state) {
	const bbcSaveStateItem *item=save_state->GetItem(bbc_system_via_save_state_item_id,0);
	if(!item) {
		return bbcSaveState::LS_MISSING;
	}

	switch(item->version) {
	default:
		return bbcSaveState::LS_BAD_VERSION;

	case 1:
		if(item->data.size()!=sizeof(bbcSystemVIASaveState1)) {
			return bbcSaveState::LS_BAD_DATA;
		} else {
			const bbcSystemVIASaveState1 *data=reinterpret_cast<const bbcSystemVIASaveState1 *>(&item->data[0]);

			latch_=data->latch;
			joystick_buttons_[0]=!!(data->joystick_buttons&1);
			joystick_buttons_[1]=!!(data->joystick_buttons&2);
		}
		break;
	}

	return bbcSaveState::LS_OK;
}

bbcSaveState::SaveStatus bbcSystemVIA::SaveExtraSaveState(bbcSaveState *save_state) const {
	bbcSystemVIASaveState1 data;

	data.latch=latch_;

	data.joystick_buttons=0;
	if(joystick_buttons_[0]) {
		data.joystick_buttons|=1;
	}
	if(joystick_buttons_[1]) {
		data.joystick_buttons|=2;
	}

	save_state->SetItem(bbc_system_via_save_state_item_id,0,1,&data);

	return bbcSaveState::SS_OK;
}

//////////////////////////////////////////////////////////////////////////

bbcSaveState::LoadStatus bbcLoadSystemVIASaveState(const bbcSaveState *save_state) {
	return bbc_system_via.LoadSaveState(save_state);
}

bbcSaveState::SaveStatus bbcSaveSystemVIASaveState(bbcSaveState *save_state) {
	return bbc_system_via.SaveSaveState(save_state);
}
