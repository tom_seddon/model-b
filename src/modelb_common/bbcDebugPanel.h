#ifndef bbcDEBUGPANEL_H_
#define bbcDEBUGPANEL_H_

#ifdef bbcDEBUG_PANELS

//#include <stdarg.h>
//#include <vector>

class bbcDebugPanel {
public:
	bbcDebugPanel();
	virtual ~bbcDebugPanel();

	int Width() const;
	int Height() const;

	void Print(int x,int y,const char *fmt,...);
	void SetSize(int new_width,int new_height);
	char *const *Lines();
	const char *const *LinesReadOnly() const;

	//use for whatever.
	bool changed;
protected:
private:
	bbcDebugPanel(const bbcDebugPanel &);
	bbcDebugPanel &operator=(const bbcDebugPanel &);

	int width_,height_;
	char *buf_;
	char **lines_;
//	std::vector<char> buf_;
//	std::vector<char *> lines_;
};

inline int bbcDebugPanel::Width() const {
	return width_;
}

inline int bbcDebugPanel::Height() const {
	return height_;
}

inline char *const *bbcDebugPanel::Lines() {
	this->changed=true;
	return &lines_[0];
}

inline const char *const *bbcDebugPanel::LinesReadOnly() const {
	return &lines_[0];
}

#endif//bbcDEBUG_PANELS

#endif
