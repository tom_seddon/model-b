#include "pch.h"
#include "bbcBeebIce.h"

#include "bbcComputer.h"

#ifdef bbcENABLE_BEEB_ICE

bbcBeebIce::BrkStatus bbcBeebIce::bs_;
t65::byte bbcBeebIce::regbrks_[256];

bbcBeebIce::BrkStatus::BrkStatus() {
	this->flags.all=0;
}

void bbcBeebIce::Init() {
	for(int i=0;i<256;++i) {
		regbrks_[i]=0;
	}
}

void bbcBeebIce::OnChangeA() {
	if(regbrks_[bbcComputer::cpustate.a]&RBT_A) {
		bs_.flags.reg|=RBT_A;
	}
}

void bbcBeebIce::OnChangeX() {
	if(regbrks_[bbcComputer::cpustate.x]&RBT_X) {
		bs_.flags.reg|=RBT_X;
	}
}

void bbcBeebIce::OnChangeY() {
	if(regbrks_[bbcComputer::cpustate.y]&RBT_Y) {
		bs_.flags.reg|=RBT_Y;
	}
}

void bbcBeebIce::OnChangeP() {
	if(regbrks_[bbcComputer::cpustate.p]&RBT_P) {
		bs_.flags.reg|=RBT_P;
	}
}

void bbcBeebIce::OnChangeS() {
	if(regbrks_[bbcComputer::cpustate.s.l]&RBT_S) {
		bs_.flags.reg|=RBT_S;
	}
}

void bbcBeebIce::OnReadBrk(t65::Word addr) {
	bs_.flags.mem|=MBT_READ;
	bs_.read_addr=addr;
}

void bbcBeebIce::OnWriteBrk(t65::Word addr,t65::byte val) {
	bs_.flags.mem|=MBT_WRITE;
	bs_.write_addr=addr;
	bs_.write_val=val;
}

void bbcBeebIce::OnExecuteBrk(t65::Word addr) {
	bs_.flags.mem|=MBT_EXECUTE;
	bs_.execute_addr=addr;
}

#endif//bbcENABLE_BEEB_ICE
