#ifndef BBCMODELMASTER128CONFIG_H_
#define BBCMODELMASTER128CONFIG_H_

struct bbcModelMaster128Config {
	typedef bbcComputer MachineType;

	//////////////////////////////////////////////////////////////////////////
	// Memory access -- which form of memory access should be used for each
	// access category?

	// ReadZP: how the processor reads bytes from zero page
	typedef MachineType::ReadNoHW ReadZP;

	// WriteZP: how the processor writes bytes to zero page
	typedef MachineType::WriteNoHW WriteZP;

	// ReadStack: how the processor reads bytes from the stack
	typedef MachineType::ReadNoHW ReadStack;

	// WriteStack: how the processor write bytes to the stack
	typedef MachineType::WriteNoHW WriteStack;

	// FetchInstr: how the processor reads bytes whilst fetching the bytes
	// that make up an instruction
	typedef MachineType::ReadExecute FetchInstr;

	// FetchAddress: how the processor reads an indirect address from
	// main memory, e.g. during IRQ processing or when doing the indirect
	// memory mode.
	typedef MachineType::ReadWithHwAndShadowRam FetchAddress;

	// ReadOperand: how the processor reads bytes when reading an
	// instruction's operand
	typedef MachineType::ReadWithHwAndShadowRam ReadOperand;

	// WriteOperand: how the processor writes bytes when writing an
	// instruction's operand
	typedef MachineType::WriteWithHwAndShadowRam WriteOperand;

	// ReadDebug: how to read a byte in debug mode (no update hardware
	// or signal interrupts etc.)
	typedef MachineType::ReadNoHW ReadDebug;

	// WriteDebug: how to write a byte in debug mode (no update hardware
	// or signal interrupts etc.)
	typedef MachineType::WriteNoHW WriteDebug;

	static const bbcReadMmioFn romsel_read_fn;
	static const bbcWriteMmioFn romsel_write_fn;
};

#endif
