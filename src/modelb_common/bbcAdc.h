#ifndef bbcADC_H_
#define bbcADC_H_

#include "bbcDebugPanel.h"
#include "bbcSaveState.h"

class bbcAdc {
public:
	static void Init();
	static void Reset();

	static void Update();

	static t65::byte Read(t65::word addr);
	static void Write(t65::word addr,t65::byte val);

	static void SetJoystick(int joystick,float x,float y);
//	static void SetChannelValue(int channel,float value);

#ifdef bbcDEBUG_PANELS
	static void InitDebugPanel(bbcDebugPanel *panel);
	static void UpdateDebugPanel(bbcDebugPanel *panel);
#endif

	static bbcSaveState::LoadStatus LoadSaveState(const bbcSaveState *save_state);
	static bbcSaveState::SaveStatus SaveSaveState(bbcSaveState *save_state);
protected:
private:
	static int next_stop_at_;
	static bool busy_;
	static bool is_8bit_;
	static int input_channel_;
	static t65::word channels_[4];
	static t65::Word last_conversion_;
	static bool conversion_complete_;
};

//inline void bbcAdc::SetChannelValue(int channel,float value) {
//	BASSERT(channel>=0&&channel<4);
//	channels_[channel]=value;
//}

#endif
