// (disable C++ exceptions in STL)
#define _HAS_EXCEPTIONS 1
//#define _STATIC_CPPLIB

#include "../port.h"

#ifdef _MSC_VER
#pragma warning(disable:4201)//C4201: nonstandard extension used : nameless struct/union
#endif

#include <t65.h>
#include "bbcCommon.h"

#include <vector>

#ifndef _MSC_VER
#define _cpp_max max
#define _cpp_min min
#endif

