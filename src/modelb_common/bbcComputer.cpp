#include "pch.h"
#include "bbcComputer.h"
#include "bbcUserVIA.h"
#include "bbcSystemVIA.h"
#include "bbc1770.h"
#include "bbcSound.h"
#include "bbcKeyboardMatrix.h"
#include "bbcTeletextFont.h"
#include "bbcAdc.h"
#include "bbcCmos.h"
#include "bbcAcia.h"
#include "bbcVideo.h"
#include <ctype.h>

//////////////////////////////////////////////////////////////////////////
//
#ifdef bbcENABLE_BEEB_ICE
bbcBeebIce bbcComputer::ice;
#endif//bbcENABLE_BEEB_ICE
int bbcComputer::next_stop=0;
bbcRomSlot bbcComputer::rom_slots_[16];
bbcRomSlot bbcComputer::os_rom_;
t65::byte bbcComputer::romsel_=0;
t65::byte bbcComputer::watford_romsel_=0;
//No ROM board by default.
//t65::byte bbcComputer::romsel_mask_;
//t65::byte bbcComputer::romsel_fixed_bits_;
t65::byte *bbcComputer::read_pages_[256];
t65::byte *bbcComputer::write_pages_[256];
t65::byte bbcComputer::ram_[64*1024];//TODO MAke private
bbcComputer::StateType bbcComputer::cpustate;
int bbcComputer::cpucycles;
unsigned bbcComputer::irq_flags_;
bbcWriteMmioFn bbcComputer::mmio_write_fns_[0x400];
bbcReadMmioFn bbcComputer::mmio_read_fns_[0x400];
#ifdef bbcDEBUG_COUNT_MMIO_ACCESSES
unsigned bbcComputer::mmio_writes_count_[0x400];
unsigned bbcComputer::mmio_reads_count_[0x400];
#endif
const bbcModel *bbcComputer::model_;
bool bbcComputer::vdusel_;
t65::byte *bbcComputer::shadow_read_pages_[256];
t65::byte *bbcComputer::shadow_write_pages_[256];
t65::byte **bbcComputer::pc_read_pages_[256];
t65::byte **bbcComputer::pc_write_pages_[256];
bool bbcComputer::paged_ram_sel_;//bit 7 of FE30, B+ only.
const bbc1770Interface *bbcComputer::disc_interface_;
bool bbcComputer::andy_set_;//bit 7 of FE30, M128 only
t65::byte bbcComputer::acccon_;//FE34, M128 only
bbcEventRecord bbcComputer::event_record;
#ifdef bbcDEBUG_TRACE_EVENTS
bool bbcComputer::trace_cpu_events=false;
#endif
#ifdef bbcENABLE_BEEB_ICE
t65::byte *bbcComputer::ice_brk_pages_[256];
t65::byte *bbcComputer::ice_shadow_brk_pages_[256];
t65::byte **bbcComputer::ice_pc_brk_pages_[256];
t65::byte bbcComputer::ice_brk_[128*1024];
#endif//bbcENABLE_BEEB_ICE

//////////////////////////////////////////////////////////////////////////
//

static void NullWriteMmio(t65::word,t65::byte) {
}

static t65::byte NullReadMmio(t65::word) {
	return 0;
}

const bbcMmioFnInit bbcComputer::mmio_fn_inits_[]={
	//Video -- 6845 and ULA
	{0xFE00,8,&bbcVideo::Read6845,&bbcVideo::Write6845},
	{0xFE20,1,0,&bbcVideo::WriteUla},
	{0xFE21,1,0,&bbcVideo::WritePalette},

	//ADC
	{0xFEC0,3,&bbcAdc::Read,&bbcAdc::Write},

	//System VIA
	{0xFE40,32,&bbcSystemVIA::MmioRead,&bbcSystemVIA::MmioWrite},

	//User VIA
	{0xFE60,32,&bbcUserVIA::MmioRead,&bbcUserVIA::MmioWrite},

	//Serial ACIA
#ifndef bbcSERIAL_ENABLE
	{0xFE08,1,&bbcAciaReadStatusDummy,0},
#else
	{0xFE08,1,&bbcAciaReadStatus,&bbcAciaWriteControl},
	{0xFE09,1,&bbcAciaReadRdr,&bbcAciaWriteTdr},
	{0xFE10,1,0,&bbcAciaWriteSerialUla},
#endif
	//
	{0,0,0,0,}
};

//static t65::byte reads_black_hole[256]={0,};
static t65::byte writes_black_hole[256];

void bbcComputer::SetMmioFn(t65::word addr,bbcReadMmioFn read_fn,
	bbcWriteMmioFn write_fn)
{
	BASSERT(addr>=0xfc00);
	addr-=0xfc00;
	if(read_fn) {
		mmio_read_fns_[addr]=read_fn;
	}
	if(write_fn) {
		mmio_write_fns_[addr]=write_fn;
	}
}

void bbcComputer::SetDiscInterface(const bbc1770Interface *disc_interface) {
	disc_interface_=disc_interface;
	RefreshMmioFns();
	if(disc_interface_) {
		if(disc_interface_->init_fn) {
			(*disc_interface_->init_fn)();
		}
		bbc1770::Init(*disc_interface_);
	}
}

void bbcComputer::SetModel(const bbcModel *model) {
	int i;

	BASSERT(model);
	model_=model;
	RefreshMmioFns();

	for(i=0;i<256;++i) {
		read_pages_[i]=0;
		write_pages_[i]=0;
		shadow_read_pages_[i]=0;
		shadow_write_pages_[i]=0;
		pc_read_pages_[i]=0;
		pc_write_pages_[i]=0;
#ifdef bbcENABLE_BEEB_ICE
		ice_brk_pages_[i]=0;
		ice_shadow_brk_pages_[i]=0;
		ice_pc_brk_pages_[i]=0;
#endif
	}
	
	for(i=0;i<0x80;++i) {
		read_pages_[i]=&ram_[i<<8];
		write_pages_[i]=&ram_[i<<8];
#ifdef bbcENABLE_BEEB_ICE
		ice_brk_pages_[i]=&ice_brk_[i<<8];
#endif
	}
	
	for(i=0;i<0xC0;++i) {
		if(i<0x30) {
			//Bottom of RAM
			shadow_read_pages_[i]=read_pages_[i];
			shadow_write_pages_[i]=write_pages_[i];
#ifdef bbcENABLE_BEEB_ICE
			ice_shadow_brk_pages_[i]=ice_brk_pages_[i];
#endif
		} else if(i<0x80) {
			//shadow area
			shadow_read_pages_[i]=&ram_[0x8000+(i<<8)];
			shadow_write_pages_[i]=&ram_[0x8000+(i<<8)];
#ifdef bbcENABLE_BEEB_ICE
			ice_brk_pages_[i]=&ice_brk_[0x8000+(i<<8)];
#endif
		}
	}
	
	//fill in OS ROM area
	os_rom_.GetPages(&read_pages_[0xC0],&write_pages_[0xC0]);
	os_rom_.GetPages(&shadow_read_pages_[0xC0],&shadow_write_pages_[0xC0]);
#ifdef bbcENABLE_BEEB_ICE
	os_rom_.GetBrkPages(&ice_brk_pages_[0xC0]);
	os_rom_.GetBrkPages(&ice_shadow_brk_pages_[0xC0]);
#endif
	
	write_pages_[0xfc]=writes_black_hole;
	write_pages_[0xfd]=writes_black_hole;

	model_->Init();
	//5/12/2004 -- removing InitialiseCpuState (suspect real 6502 starts up with gibberish)
//	model_->InitialiseCpuState();
	
	for(i=0;i<256;++i) {
		BASSERT(read_pages_[i]);
		BASSERT(write_pages_[i]);

		//the exact setup isn't easily determinable. (ok, so it could be, but...)
		//so this is gone.
//		BASSERT(!!shadow_read_pages_[i]==!!shadow_read_pages_[0]);
//		BASSERT(!!shadow_write_pages_[i]==!!shadow_write_pages_[0]);
//		BASSERT(!!pc_read_pages_[i]==!!pc_read_pages_[0]);
//		BASSERT(!!pc_write_pages_[i]==!!pc_write_pages_[0]);
	}

	//But it doesn't clear the RAM. This may be useful.
	bbcComputer::ResetHard();
}

void bbcComputer::RefreshMmioFns() {
	int i;

	ResetMmioFns();
	AddMmioFns(mmio_fn_inits_);
	if(disc_interface_) {
		for(i=0;i<4;++i) {
			SetMmioFn((t65::word)(disc_interface_->fdc_start+i),&bbc1770::ReadFdc,&bbc1770::WriteFdc);
		}
		SetMmioFn(disc_interface_->drive_ctrl,&bbc1770::ReadControl,&bbc1770::WriteControl);
		AddMmioFns(disc_interface_->extra_mmio_fns);
	}
	if(model_) {
		AddMmioFns(model_->ExtraMmioFns());
	}
}


//void bbcComputer::Init(const bbc1770Interface *disc_interface,int video_width,
//	int video_height,bbcModel *bbc_model)
void bbcComputer::Init() {
	model_=0;
	disc_interface_=0;
	
	memset(ram_,0,sizeof ram_);

#ifdef bbcENABLE_BEEB_ICE
	bbcBeebIce::Init();
#endif
	bbcSound::Init();
	bbcKeyboardMatrix::Init();
	bbcTeletextFont::Init();
	bbcVideo::Init();
	bbcAdc::Init();
	bbcAciaInit();
}

void bbcComputer::Shutdown() {
	bbcVideo::Shutdown();
	bbcFdd::Shutdown();
}


void bbcComputer::ResetMmioFns() {
	for(unsigned i=0;i<0x400;++i) {
		mmio_read_fns_[i]=&NullReadMmio;
		mmio_write_fns_[i]=&NullWriteMmio;

#ifdef bbcDEBUG_COUNT_MMIO_ACCESSES
		mmio_reads_count_[i]=0;
		mmio_writes_count_[i]=0;
#endif
	}

}

void bbcComputer::AddMmioFns(const bbcMmioFnInit *inits) {
	if(inits) {
		for(unsigned i=0;inits[i].count>0;++i) {
			const bbcMmioFnInit *p=&inits[i];
			for(unsigned j=0;j<p->count;++j) {
				SetMmioFn((t65::word)(p->start+j),p->read_fn,p->write_fn);
			}
		}
	}
}

void bbcComputer::SetRomContents(int slot,const t65::byte *contents,unsigned size_contents) {
	BASSERT(slot>=0&&slot<16);
//	if(PhysicalRomSlot(slot)!=slot) {
//		Log::w.Logf("ROM in slot %d not accessible (slot %d will be seen instead)\n",slot,PhysicalRomSlot(slot));
//	}
	rom_slots_[slot].ResetContents();
	rom_slots_[slot].SetContents(contents,size_contents);
}

void bbcComputer::DebugDumpMemory() {
	printf("bbcComputer::DebugDumpMemory: ");
	FILE *h=fopen(".\\memory_dump.txt","wt");
	if(!h) {
		printf("failed to open output file.\n");
		return;
	}
	int buffer[16];
	t65::Word addr;
	for(unsigned i=0;i<65536;i+=16) {
		fprintf(h,"%04X: ",i);
		unsigned j;
		for(j=0;j<16;++j) {
			addr.w=(t65::word)(i+j);
			if((addr.h)!=0xfe) {
				buffer[j]=bbcComputer::read_pages_[addr.h][addr.l];
			} else {
				buffer[j]=-1;
			}
		}
		for(j=0;j<16;++j) {
			if(buffer[j]>=0) {
				fprintf(h,"%02X",buffer[j]);
			} else {
				fprintf(h,"**");
			}
			fprintf(h," ");
		}
		for(j=0;j<16;++j) {
			char c;
			if(buffer[j]<0) {
				c=' ';
			} else if(isprint(buffer[j])) {
				c=(char)buffer[j];
			} else {
				c='.';
			}
			fputc(c,h);
		}
		fprintf(h,"\n");
	}
	fclose(h);
	printf("ok.\n");
}

#ifndef bbcUPDATE_IN_BBCMODEL
void bbcComputer::Update() {
	bbcVideo::Update(cpucycles);
	bbc_system_via.Update(cpucycles);
	bbc_user_via.Update(cpucycles);
	bbc1770::Update(cpucycles);
	bbcAdc::Update(cpucycles);
#ifdef bbcSERIAL_ENABLE
	bbcAciaUpdate(cpucycles);
#endif
}
#endif

void bbcComputer::WriteMmio(t65::Word addr,t65::byte val) {
	BASSERT(addr.w>=0xFC00);

#ifdef bbcDEBUG_COUNT_MMIO_ACCESSES
	++mmio_writes_count_[addr.w-0xFC00];
#endif
	bbcComputer::SyncClocks();
	(*mmio_write_fns_[addr.w-0xFC00])(addr.w,val);
}

t65::byte bbcComputer::ReadMmio(t65::Word addr) {
	BASSERT(addr.w>=0xFC00);

#ifdef bbcDEBUG_COUNT_MMIO_ACCESSES
	++mmio_reads_count_[addr.w-0xFC00];
#endif
	bbcComputer::SyncClocks();
	return (*mmio_read_fns_[addr.w-0xFC00])(addr.w);
}

//////////////////////////////////////////////////////////////////////////
//

static const bbcSaveStateItemId rom_sel_b_save_state_item_id("b_romsel");

struct bbcComputerRomselBState1 {
	t65::byte romsel;
};

t65::byte bbcComputer::ReadRomSelB(t65::word addr) {
	(void)addr;

	return 0;
}

void bbcComputer::WriteRomSelB(t65::word addr,t65::byte value) {
	(void)addr;

	romsel_=value&0xf;
	WrittenRomSelB();
}

void bbcComputer::WrittenRomSelB() {
	rom_slots_[romsel_].GetPages(&read_pages_[0x80],&write_pages_[0x80]);
#ifdef bbcENABLE_BEEB_ICE
	rom_slots_[romsel_].GetBrkPages(&ice_brk_pages_[0x80]);
#endif
}

bbcSaveState::LoadStatus bbcComputer::LoadRomSelBSaveState(const bbcSaveState *save_state) {
	const bbcSaveStateItem *item=save_state->GetItem(rom_sel_b_save_state_item_id,0);
	if(!item) {
		return bbcSaveState::LS_MISSING;
	}

	switch(item->version)
	{
	case 1:
		if(item->data.size()!=sizeof(bbcComputerRomselBState1)) {
			return bbcSaveState::LS_BAD_DATA;
		} else {
			const bbcComputerRomselBState1 *data=reinterpret_cast<const bbcComputerRomselBState1 *>(&item->data[0]);
			if(data->romsel&0xf0) {
				return bbcSaveState::LS_BAD_DATA;
			}

			romsel_=data->romsel;
			WrittenRomSelB();
		}
		break;

	default:
		return bbcSaveState::LS_BAD_VERSION;
	}

	return bbcSaveState::LS_OK;
}

bbcSaveState::SaveStatus bbcComputer::SaveRomSelBSaveState(bbcSaveState *save_state) {
	bbcComputerRomselBState1 data;
	data.romsel=romsel_;

	save_state->SetItem(rom_sel_b_save_state_item_id,0,1,&data);

	return bbcSaveState::SS_OK;
}

//////////////////////////////////////////////////////////////////////////
//
t65::byte bbcComputer::ReadRomSelBWatford(t65::word addr) {
	(void)addr;

	return 0;
}

void bbcComputer::WriteRomSelBWatford(t65::word addr,t65::byte value) {
	(void)addr;

	romsel_=value&0xf;
	rom_slots_[romsel_].GetReadPages(&read_pages_[0x80]);
	rom_slots_[watford_romsel_].GetWritePages(&write_pages_[0x80]);
#ifdef bbcENABLE_BEEB_ICE
	rom_slots_[romsel_].GetBrkPages(&ice_brk_pages_[0x80]);//hmmm!!!
//	rom_slots_[watford_romsel_].GetWritePages(&write_pages_[0x80]);
#endif

	//bbcPrintf(__FUNCTION__ ": addr=&%04X, romsel=%d, watford_romsel=%d\n",addr,romsel_,watford_romsel_);
}

void bbcComputer::WriteWatfordRomBoard(t65::word addr,t65::byte value) {
	(void)value;

	watford_romsel_=addr&0xf;
	rom_slots_[watford_romsel_].GetWritePages(&write_pages_[0x80]);

	//bbcPrintf(__FUNCTION__ ": addr=&%04X, watford_romsel=%d\n",addr,watford_romsel_);
}

//////////////////////////////////////////////////////////////////////////
//
t65::byte bbcComputer::ReadRomSelBPlus(t65::word addr) {
	if((addr&0xf)==0x04) {
		return vdusel_?0x80:0;
	} else {
		return romsel_|(paged_ram_sel_?0x80:0);
	}
}

void bbcComputer::WriteRomSelBPlus(t65::word addr,t65::byte value) {
	unsigned i;

	if((addr&0xf)==0x04) {
		vdusel_=!!(value&0x80);
	} else {
		romsel_=value&0xf;
		paged_ram_sel_=!!(value&0x80);
	}
	//TODO this only needs to be done once really.
	for(i=0;i<256;++i) {
		pc_write_pages_[i]=write_pages_;
		pc_read_pages_[i]=read_pages_;
#ifdef bbcENABLE_BEEB_ICE
		ice_pc_brk_pages_[i]=ice_brk_pages_;
#endif
	}
	if(vdusel_) {
		//First 0x20 pages of OS ROM area are considered VDU driver
		for(i=0xC0;i<0xE0;++i) {
			pc_write_pages_[i]=shadow_write_pages_;
			pc_read_pages_[i]=shadow_read_pages_;
#ifdef bbcENABLE_BEEB_ICE
			ice_pc_brk_pages_[i]=ice_shadow_brk_pages_;
#endif
		}
		//If paged RAM is selected too, pages A0-AF incl. are too
		if(paged_ram_sel_) {
			for(i=0;i<16;++i) {
				pc_write_pages_[0xA0+i]=shadow_write_pages_;
				pc_read_pages_[0xA0+i]=shadow_read_pages_;
#ifdef bbcENABLE_BEEB_ICE
				ice_pc_brk_pages_[0xA0+i]=ice_shadow_brk_pages_;
#endif
			}
		}
	}
	rom_slots_[romsel_].GetPages(&read_pages_[0x80],&write_pages_[0x80]);
	if(paged_ram_sel_) {
		for(i=0x80;i<0xB0;++i) {
			read_pages_[i]=write_pages_[i]=&ram_[0x8000+(i<<8)];
#ifdef bbcENABLE_BEEB_ICE
			ice_brk_pages_[i]=&ice_brk_[0x8000+(i<<8)];
#endif
		}
	}

	//If vdusel_, some pc_[write|read]_pages will point to the shadow page set.
	//So, copy what's in the sideways area to the shadow pages so that the CPU's
	//view of this area of memory is consistent.
	for(i=0x80;i<0xC0;++i) {
		shadow_write_pages_[i]=write_pages_[i];
		shadow_read_pages_[i]=read_pages_[i];
#ifdef bbcENABLE_BEEB_ICE
		ice_shadow_brk_pages_[i]=ice_brk_pages_[i];
#endif
	}

	//Finally, get the video display from shadow or normal memory depending.
	bbcVideo::SetVideoRam(vdusel_?&ram_[0x8000]:&ram_[0]);
}

//////////////////////////////////////////////////////////////////////////
//
t65::byte bbcComputer::ReadRomSelMaster128(t65::word addr) {
	if((addr&0xf)==0x04) {
		return acccon_;
//		return vdusel_?0x80:0;
	} else {
		return romsel_|(andy_set_?0x80:0);
//		return romsel_|(paged_ram_sel_?0x80:0);
	}
}

enum {
	ACCCON_CRT_LYNNE=1,//D
	ACCCON_BPLUS_SHADOW=2,//E
	ACCCON_CPU_LYNNE=4,//X
	ACCCON_HAZEL=8,//Y
};

void bbcComputer::WriteRomSelMaster128(t65::word addr,t65::byte value) {
	unsigned i;

	if((addr&0xf)==0x04) {
		//bbcPrintf("Write ACCCON: 0x%02X\n",value);
		acccon_=value;
	} else {
		//bbcPrintf("Write ROMSEL: 0x%02X\n",value);
		romsel_=value&0xf;
		andy_set_=!!(value&0x80);
	}

	//if ACCCON bit 2 set, shadow RAM paged in at 3000-7FFF else normal RAM.
	//TODO this only needs to be done once really.
	t65::byte **write_pages=acccon_&ACCCON_CPU_LYNNE?shadow_write_pages_:write_pages_;
	t65::byte **read_pages=acccon_&ACCCON_CPU_LYNNE?shadow_read_pages_:read_pages_;
	for(i=0;i<0x100;++i) {
		pc_write_pages_[i]=write_pages;
		pc_read_pages_[i]=read_pages;
#ifdef bbcENABLE_BEEB_ICE
		ice_pc_brk_pages_[i]=ice_brk_pages_;
#endif
	}

	//if bit 0 set, video comes from shadow ram.
	if(acccon_&ACCCON_CRT_LYNNE) {
		bbcVideo::SetVideoRam(&ram_[0x8000]);
	} else {
		bbcVideo::SetVideoRam(&ram_[0]);
	}

	//if bit 1 set, b+-style shadow ram--if PC in C000-DFFF, writes
	//to shadow else normal.
	if(acccon_&ACCCON_BPLUS_SHADOW) {
		//B+-style shadow RAM
		
		//First 0x20 pages of OS ROM area are considered VDU driver
		for(unsigned i=0xC0;i<0xE0;++i) {
			pc_write_pages_[i]=shadow_write_pages_;
			pc_read_pages_[i]=shadow_read_pages_;
#ifdef bbcENABLE_BEEB_ICE
			ice_pc_brk_pages_[i]=ice_shadow_brk_pages_;
#endif
		}
	}

	//page in OS ROM (need to do this more often than on B due to
	//HAZEL)
	os_rom_.GetPages(&read_pages_[0xC0],&write_pages_[0xC0]);

	//if bit 3 set, page HAZEL at C000-DFFF
	if(acccon_&ACCCON_HAZEL) {
		//Page in HAZEL
		for(unsigned i=0xC0;i<0xE0;++i) {
			t65::word src=(t65::word)(0x9000+(i-0xC0)*256);
			read_pages_[i]=&ram_[src];
			write_pages_[i]=&ram_[src];
#ifdef bbcENABLE_BEEB_ICE
			ice_brk_pages_[i]=&ice_brk_[src];
#endif
		}
	}

	//Page in the ROM
	rom_slots_[romsel_].GetPages(&read_pages_[0x80],&write_pages_[0x80]);
#ifdef bbcENABLE_BEEB_ICE
	rom_slots_[romsel_].GetBrkPages(&ice_brk_pages_[0x80]);
#endif
	
	//Page ANDY on top of ROM if required
	if(andy_set_) {
		//Page in ANDY
		for(unsigned i=0x80;i<0x90;++i) {
			t65::word src=(t65::word)(0x8000+(i-0x80)*256);
			read_pages_[i]=&ram_[src];
			write_pages_[i]=&ram_[src];
#ifdef bbcENABLE_BEEB_ICE
			ice_brk_pages_[i]=&ice_brk_[src];
#endif
		}
	}

	//copy what's in the ROM area of the non-shadow pages to the shadow pages
	//so that all views of this area are consistent.
	for(i=0x80;i<0x100;++i) {
		shadow_write_pages_[i]=write_pages_[i];
		shadow_read_pages_[i]=read_pages_[i];
#ifdef bbcENABLE_BEEB_ICE
		ice_shadow_brk_pages_[i]=ice_brk_pages_[i];
#endif
	}
}

//////////////////////////////////////////////////////////////////////////
//
void bbcComputer::ResetHard() {
	cpucycles=0;
	next_stop=cpucycles;
	irq_flags_=0;

	memset(ram_,0,sizeof ram_);

	bbcVideo::Reset();
	bbcVideo::SetVideoRam(&ram_[0]);

	bbc_system_via.Reset();
	bbc_user_via.Reset();
	bbc1770::Reset();
	bbcAdc::Reset();
	bbcAciaReset();

	ResetSoft();
}

void bbcComputer::ResetSoft() {
	model_->Reset6502();
}

//////////////////////////////////////////////////////////////////////////
//

void bbcComputer::SetPagedRom(int slot) {
	rom_slots_[slot].GetPages(&read_pages_[0x80],&write_pages_[0x80]);
	//TODO add to shadow pages.
}

void bbcComputer::SetCrtcBase(t65::word base) {
	(void)base;
}

void bbcComputer::SetShadowRamAccessPages(const Range *ranges,unsigned num_ranges) {
	for(unsigned i=0;i<256;++i) {
		pc_read_pages_[i]=read_pages_;
		pc_write_pages_[i]=write_pages_;
	}

	for(unsigned i=0;i<num_ranges;++i) {
		const Range *range=&ranges[i];

		for(unsigned j=0;j<range->num;++j) {
			unsigned page=range->start+j;

			pc_read_pages_[page]=shadow_read_pages_;
			pc_write_pages_[page]=shadow_write_pages_;
		}
	}
}

void bbcComputer::SetMappedReadWriteRomPages(const Range *ranges,const t65::word *offsets,unsigned num_ranges) {
	for(unsigned i=0;i<num_ranges;++i) {
		const Range *range=&ranges[i];

		for(unsigned j=0;j<range->num;++j) {
			unsigned page=range->start+j;
			t65::byte *dest=&ram_[offsets[j]];

			read_pages_[page]=dest;
			write_pages_[page]=dest;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
#ifdef bbcDEBUG_ENABLE
void bbcComputer::WriteRamDisassembly(FILE *h) {
	static const char *vector_names[]={
		"USERV","BRKV","IRQ1V","IRQ2V","CLIV","BYTEV","WORDV","WRCHV","RDCHV","FILEV","ARGSV","BGETV","BPUTV","GPBPV",
		"FINDV","FSCV","EVENTV","UPTV","NETV","VDUV","KEYV","INSV","REMV","CNPV","IND1V","IND2V","IND3V",
	};
	static const unsigned num_vectors=sizeof vector_names/sizeof vector_names[0];
	
	static t65::Word vector_dests[num_vectors];
	for(unsigned i=0;i<num_vectors;++i) {
		t65::word vector=(t65::word)(0x200+i*2);

		vector_dests[i].l=bbcComputer::model_->ReadByte(vector);
		vector_dests[i].h=bbcComputer::model_->ReadByte(vector+1);
	}

	char opcode_buf[10],operand_buf[15];
	t65::byte bytes_buf[3];

	int address=0;
	while(address<=0xFFFF) {
		t65::Word begin;
		begin.w=t65::word(address);
		int len;

		fprintf(h,"%04X: ",begin.w);

		if(begin.w>=0xFC00&&begin.w<=0xFEFF) {
			fprintf(h,"---");
			len=1;
		} else {
			t65::Word end=bbcComputer::Model()->Disassemble(begin,opcode_buf,operand_buf,bytes_buf);
			len=end.w-begin.w;
			if(len<0) {
				//Went off the end of memory.
				//Never mind.
				break;
			}


			fprintf(h,"%-5s %-10s",opcode_buf,operand_buf);
			for(int i=0;i<3;++i) {
				if(i<len) {
					fprintf(h," %02X",bytes_buf[i]);
				} else {
					fprintf(h,"   ");
				}
			}
		}

		//In case anyone is crazy enough to point a vector at the I/O space...
		for(unsigned i=0;i<num_vectors;++i) {
			if(begin.w==vector_dests[i].w) {
				fprintf(h," ; %s",vector_names[i]);
			}
		}

		fprintf(h,"\n");
			
		address+=len;
	}
}
#endif

//////////////////////////////////////////////////////////////////////////

static const bbcSaveStateItemId bbc_computer_save_state_item_id("bbc_comp");

#pragma pack(push,1)
struct bbcComputerState1 {
	bbcSaveStateByte a,x,y,p,s;
	bbcSaveStateWord pc;
	bbcSaveStateInt32 cycles;
	bbcSaveStateDword irq_flags;

	t65::byte ram[65536];
	t65::byte os_rom_contents[16384];
	t65::byte roms_contents[16][16384];
	t65::word roms_ram;//roms_ram&(1<<i) if slot i is RAM
};

struct bbcComputerRomselBWatfordState1 {
	t65::byte romsel;
	t65::byte romsel_watford;
};

struct bbcComputerRomselBPlusState1 {
	t65::byte romsel;
	t65::byte vdusel_bool;
	t65::byte paged_ram_sel_bool;
};

struct bbcComputerRomselMaster128State1 {
	t65::byte romsel;
	t65::byte vdusel_bool;
	t65::byte paged_ram_sel_bool;
	t65::byte andy_set_bool;
	t65::byte acccon;
};

#pragma pack(pop)

bbcSaveState::LoadStatus bbcComputer::LoadBbcComputerSaveState(const bbcSaveState *save_state) {
	bbcSaveState::LoadStatus status;
	if(const bbcComputerState1 *data=save_state->FindItem<bbcComputerState1>(bbc_computer_save_state_item_id,0,1,&status)) {
		cpustate.a=data->a;
		cpustate.x=data->x;
		cpustate.y=data->y;
		cpustate.p=data->p;
		cpustate.s.l=data->s;
		cpustate.pc.w=data->pc;
		cpucycles=data->cycles;
		irq_flags_=data->irq_flags;

		memcpy(ram_,data->ram,65536);//sometimes this is too much. oh well...

		os_rom_.SetContents(data->os_rom_contents,16384);

		for(int i=0;i<16;++i) {
			rom_slots_[i].SetContents(data->roms_contents[i],16384);
			rom_slots_[i].SetWriteable(!!(data->roms_ram&(1<<i)));
		}
	} else {
		return status;
	}

	return bbcSaveState::LS_OK;
}

bbcSaveState::SaveStatus bbcComputer::SaveBbcComputerSaveState(bbcSaveState *save_state) {
	bbcComputerState1 *state=save_state->PrepareItem<bbcComputerState1>(bbc_computer_save_state_item_id,0,1);

	state->a=cpustate.a;
	state->x=cpustate.x;
	state->y=cpustate.y;
	state->p=cpustate.p;
	state->s=cpustate.s.l;
	state->pc=cpustate.pc.w;
	state->cycles=cpucycles;
	state->irq_flags=irq_flags_;

	memcpy(state->ram,ram_,65536);//sometimes this is too much. oh well...

	memcpy(state->os_rom_contents,os_rom_.GetContents(),16384);

	state->roms_ram=0;
	for(int i=0;i<16;++i) {
		memcpy(state->roms_contents[i],rom_slots_[i].GetContents(),16384);
		if(rom_slots_[i].GetWriteable()) {
			state->roms_ram|=1<<i;
		}
	}

	return bbcSaveState::SS_OK;
}
