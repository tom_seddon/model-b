#ifndef VIA_H_
#define VIA_H_

//TODO merge all customisation into common.h (or something)
//#include "bbcVideo.h"
#include "bbcDebugPanel.h"
#include "bbcSaveState.h"

//This must be defined.
//TODO remove this!
//#define VIA_2MHZ_TIMERS

#define VIA_METRICS

#include <stdio.h>

class bbcVIA {
public:
    // Reset
    void Reset();
    // Update
    void Update();
    // Assert interrupts
    void CA1(bool);
	void CA2(bool);
	void CB1(bool);
	void CB2(bool);
    // Read/Write
    t65::byte Read(t65::word addr);//,int *cycles);
    void Write(t65::word addr,t65::byte value);//,int *cycles);
	
	//void Init();
	
	void DebugDump(FILE *h);

//	t65::byte Ier() const;
//	t65::byte Ifr() const;
//	t65::byte Acr() const;
	
#ifdef VIA_METRICS
	unsigned t1_count;
	unsigned t2_count;
	
	void ResetMetrics();
#endif

#ifdef bbcDEBUG_PANELS
	void InitDebugPanel(bbcDebugPanel *panel);
	void UpdateDebugPanel(bbcDebugPanel *panel);
#endif

#ifdef bbcDEBUG_TRACE_EVENTS
	struct TraceState {
		t65::byte ier,ifr,acr;
		t65::Word t1l;
		int t1c,t2c;
	};
	void FillTraceState(TraceState *state);
#endif

	bbcSaveState::LoadStatus LoadSaveState(const bbcSaveState *save_state);
	bbcSaveState::SaveStatus SaveSaveState(bbcSaveState *save_state) const;

protected:
	bbcVIA(unsigned int_mask,const char *type_name);
	t65::byte pa_,pb_;
	virtual void WrittenPa();
	virtual void WrittenPb();
	virtual void ReadingPa();
	virtual void ReadingPb();

	virtual bbcSaveState::LoadStatus LoadExtraSaveState(const bbcSaveState *save_state);
	virtual bbcSaveState::SaveStatus SaveExtraSaveState(bbcSaveState *save_state) const;
private:
	const unsigned int_mask_;
    // The registers
    t65::byte orb_,irb_,ora_,ira_;// Output|Input registers
    t65::byte ddra_,ddrb_;// Data Direction Registers
    bool t1_shot_,t2_shot_;// T[12] shot flags (for one-shot modes)
	int t1c_,t2c_;
	t65::Word t1l_;
    t65::byte t2l_l_;// Timer 1 latches, Timer 2 latches
    t65::byte sr_;// Shift Register
    t65::byte acr_;// Auxiliary Control Register
    t65::byte pcr_;// Peripheral Control Register
    t65::byte ier_,ifr_;// Int Enable Reg, Int Flag Reg

    // Masks for various registers
    enum {
        ACR_PA_LATCHING=1,
		ACR_PB_LATCHING=2,// P[AB] latching
		ACR_T2_MODE=32,// T2 control
		ACR_T1_MODE=64,
		ACR_T1_PB7=128,// T1 control
		// Masks for I[EF]R
		CA2_INT=1,
		CA1_INT=2,
		SR_INT=4,
		CB2_INT=8,
		CB1_INT=16,
		T2_INT=32,
		T1_INT=64,
    };
    // Assert timer interrupts
    void T1();
	void T2();
    // Update IFR
    void UpdateIfr();
	
	int last_update_cycles_;

	void ScheduleNextStop();

//	VIA(const VIA &);
//	VIA &operator=(const VIA &);
	
#ifdef bbcDEBUG_VIDEO
	friend class bbcVideo;
#endif

	void DebugDumpAllFormats(FILE *h,t65::byte b);
	void DebugDumpPort(FILE *h,char port,t65::byte outreg,t65::byte inreg,
		t65::byte p,t65::byte ddr);

	char type_name_[5];

	t65::dword GetSaveStateSubType() const;

	bbcVIA(const bbcVIA &);
	bbcVIA &operator=(const bbcVIA &);
};

//inline t65::byte bbcVIA::Ier() const {
//	return ier_;
//}
//
//inline t65::byte bbcVIA::Ifr() const {
//	return ifr_;
//}
//
//inline t65::byte bbcVIA::Acr() const {
//	return acr_;
//}

#endif
