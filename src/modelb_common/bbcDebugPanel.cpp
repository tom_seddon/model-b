#include "pch.h"
#ifdef bbcDEBUG_PANELS
#include "bbcDebugPanel.h"
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

bbcDebugPanel::bbcDebugPanel():
changed(false),
width_(0),
height_(0),
buf_(0),
lines_(0)
{
}

bbcDebugPanel::~bbcDebugPanel() {
	delete[] buf_;
	delete[] lines_;
}

void bbcDebugPanel::SetSize(int new_width,int new_height) {
	if(width_!=new_width||height_!=new_height) {
		width_=new_width;
		height_=new_height;

		delete[] buf_;
		delete[] lines_;

		int buf_size=new_height*(new_width+1);
		buf_=new char[buf_size];
		memset(buf_,' ',buf_size);
		//ensures &lines_[0] is always OK!
		if(new_height==0) {
			new_height=1;
		}

		lines_=new char *[new_height];
		for(int i=0;i<new_height;++i) {
			lines_[i]=&buf_[i*(new_width+1)];
			lines_[i][new_width]=0;
		}
		this->changed=true;
	}
}

void bbcDebugPanel::Print(int x,int y,const char *fmt,...) {
	BASSERT(x>=0&&x<width_&&y>=0&&y<height_);
	char buf[100];
	va_list v;
	va_start(v,fmt);
	_vsnprintf(buf,sizeof buf,fmt,v);
	va_end(v);
	const char *p=buf;
	while(*p&&x<width_) {
		lines_[y][x++]=*p++;
	}
	this->changed=true;
}
#endif//bbcDEBUG_PANELS
