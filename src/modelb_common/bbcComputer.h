#ifndef BBCMICRO_H_
#define BBCMICRO_H_

#include "bbcRomSlot.h"
#include "bbcKeys.h"
#include "bbcDebugPanel.h"
//#include "bbc1770Interface.h"
#include "bbcModel.h"
//#include "bbcDebugTrace.h"
//#include "bbcVideo.h"
#include "bbcEventRecord.h"
#ifdef bbcENABLE_BEEB_ICE
#include "bbcBeebIce.h"
#endif//bbcENABLE_BEEB_ICE

struct bbc1770Interface;

struct bbcComputer {
	//////////////////////////////////////////////////////////////////////////
	// Interface to rest of emulator
	static void Init();
	static void Shutdown();

#ifdef bbcENABLE_BEEB_ICE
	static bbcBeebIce ice;
#endif//bbcENABLE_BEEB_ICE

#ifdef bbcUPDATE_IN_BBCMODEL
	static inline void Update() {
		model_->Update();
	}
#else
	static void Update();
#endif
	static void SetOsRomContents(const t65::byte *contents,unsigned size_contents);
	static void SetRomContents(int slot,const t65::byte *contents,unsigned size_contents);
	static void SetRomWriteable(int slot,bool writeable);
//	static void SetHasRomBoard(bool has_rom_board);
	static void DebugDumpMemory();
	static void ResetSoft();//press break
	static void ResetHard();//power on reset
	
	//runs 6502 for 0 or more cycles
	//13/3/04 inline for empty FINLINE
	static FINLINE void Run6502() {
		model_->Run();
	}

	//SetDiscInterface
	//SetModel
	//
	//These may be called at any point. (Best reset first, or straight after,
	//though.)
	//
	//If the disc interface is changed, the new interface is reset.
	//
	//If the model is changed, the computer is hard reset.
	static void SetDiscInterface(const bbc1770Interface *disc_interface);
	static void SetModel(const bbcModel *model);
	static const bbcModel *Model();
	
	//////////////////////////////////////////////////////////////////////////
	// event recorder
#ifdef bbcDEBUG_TRACE_EVENTS
	static bool trace_cpu_events;
#endif
	static bbcEventRecord event_record;

	//////////////////////////////////////////////////////////////////////////
	// debug
/*
#ifdef bbcDEBUG_PANELS
	static void InitDebugPanel(bbcDebugPanel *panel);
	static void UpdateDebugPanel(bbcDebugPanel *panel);
#endif
*/
	static int DisassembleBytes(const t65::byte *bytes,t65::Word bytes_addr,
		char *opcode_buf,char *operand_buf);
#ifdef bbcDEBUG_ENABLE
	static void WriteRamDisassembly(FILE *h);
#endif
	
	//////////////////////////////////////////////////////////////////////////
	// Interface to 6502 bits
	static void WriteMmio(t65::Word addr,t65::byte val);
	static t65::byte ReadMmio(t65::Word addr);

	//SetNextStop
	//
	//Set the point (absolute or relative) at which the 6502 should next stop
	//to do some updating type stuff.
	static void SetNextStop(int cycles);
	static void SetNextStopDelta(int delta);

	//SetNextIrqStop
	//SetNextIrqStopDelta
	//
	//Set the point (absolute or relative) at which the 6502 should next stop
	//to receive an IRQ.
	static void SetNextIrqStop(int cycles);
	static void SetNextIrqStopDelta(int delta);

	//ResetNMI
	//SetNMI
	//
	//Set or clear the NMI flag. Don't set irq_flags_ directly, because this
	//won't NMI immediately if interrupts are disabled.
	static void ResetNMI();
	static void SetNMI();

	static int next_stop;//cycle count at which to next stop to do some updating.
	static unsigned irq_flags_;//Interrupts

	//////////////////////////////////////////////////////////////////////////
	// 6502 simulator requirements
	typedef t65::State65xx StateType;
	static StateType cpustate;
	static int cpucycles;
	
	//memory access categories
	struct WriteNoHW {};
	struct ReadNoHW {};

	struct WriteWithHW {};
	struct ReadWithHW {};

	//grim B+ crap
	struct WriteWithHwAndShadowRam {};
	struct ReadWithHwAndShadowRam {};
	struct WriteWithHWWatford {};

	struct ReadExecute {};
	
	static inline int Cycles2MHz() {
		return cpucycles;
	}

	static FINLINE void IrqsReenabled() {
		SetNextStop(cpucycles);
	}

	static FINLINE t65::byte ReadByte(t65::Word addr,const ReadWithHW *const) {
#ifdef bbcENABLE_BEEB_ICE
		if(ice_brk_pages_[addr.h][addr.l]&bbcBeebIce::MBT_READ) {
			ice.OnReadBrk(addr);
		}
#endif
		if(t65::byte(addr.h-0xfc)>=3) {
			return read_pages_[addr.h][addr.l];
		} else {
			return ReadMmio(addr);
		}
	}

	static FINLINE t65::byte ReadByte(t65::Word addr,const ReadNoHW *const) {
#ifdef bbcENABLE_BEEB_ICE
		if(ice_brk_pages_[addr.h][addr.l]&bbcBeebIce::MBT_READ) {
			ice.OnReadBrk(addr);
		}
#endif
		return read_pages_[addr.h][addr.l];
	}
	
	static FINLINE void WriteByte(t65::Word addr,t65::byte val,const WriteWithHW *const) {
#ifdef bbcENABLE_BEEB_ICE
		if(ice_brk_pages_[addr.h][addr.l]&bbcBeebIce::MBT_WRITE) {
			ice.OnWriteBrk(addr,val);
		}
#endif
		if(t65::byte(addr.h-0xfc)>=3) {
			write_pages_[addr.h][addr.l]=val;
		} else {
			WriteMmio(addr,val);
		}
	}

	static FINLINE void WriteByte(t65::Word addr,t65::byte val,const WriteWithHWWatford *const) {
#ifdef bbcENABLE_BEEB_ICE
		if(ice_brk_pages_[addr.h][addr.l]&bbcBeebIce::MBT_WRITE) {
			ice.OnWriteBrk(addr,val);
		}
#endif
		if(t65::byte(addr.h-0xfc)>=4) {
			write_pages_[addr.h][addr.l]=val;
		} else {
			WriteMmio(addr,val);
		}
	}

	static FINLINE void WriteByte(t65::Word addr,t65::byte val,const WriteNoHW *const) {
#ifdef bbcENABLE_BEEB_ICE
		if(ice_brk_pages_[addr.h][addr.l]&bbcBeebIce::MBT_WRITE) {
			ice.OnWriteBrk(addr,val);
		}
#endif
		write_pages_[addr.h][addr.l]=val;
	}

	static FINLINE void WriteByte(t65::Word addr,t65::byte val,const WriteWithHwAndShadowRam *const) {
#ifdef bbcENABLE_BEEB_ICE
		if(ice_pc_brk_pages_[cpustate.pc.h][addr.h][addr.l]&bbcBeebIce::MBT_WRITE) {
			ice.OnWriteBrk(addr,val);
		}
#endif
		if(t65::byte(addr.h-0xfc)>=3) {
			pc_write_pages_[cpustate.pc.h][addr.h][addr.l]=val;
		} else {
			WriteMmio(addr,val);
		}
	}

	static FINLINE t65::byte ReadByte(t65::Word addr,const ReadWithHwAndShadowRam *const) {
#ifdef bbcENABLE_BEEB_ICE
		if(ice_pc_brk_pages_[cpustate.pc.h][addr.h][addr.l]&bbcBeebIce::MBT_READ) {
			ice.OnReadBrk(addr);
		}
#endif
		if(t65::byte(addr.h-0xfc)>=3) {
			return pc_read_pages_[cpustate.pc.h][addr.h][addr.l];
		} else {
			return ReadMmio(addr);
		}
	}

	static FINLINE t65::byte ReadByte(t65::Word addr,const ReadExecute *const) {
#ifdef bbcENABLE_BEEB_ICE
		if(ice_brk_pages_[addr.h][addr.l]&bbcBeebIce::MBT_EXECUTE) {
			ice.OnExecuteBrk(addr);
		}
#endif
		return read_pages_[addr.h][addr.l];
	}
	
	static void SyncClocks();//int *cycle_counter);
	//This is the RAM for everything
	//Certain models will use less of it.
	static t65::byte ram_[64*1024];//TODO MAke private

	static bbcSaveState::LoadStatus LoadBbcComputerSaveState(const bbcSaveState *save_state);
	static bbcSaveState::SaveStatus SaveBbcComputerSaveState(bbcSaveState *save_state);

	//B romsel
	static t65::byte ReadRomSelB(t65::word addr);
	static void WriteRomSelB(t65::word addr,t65::byte value);
	static void WrittenRomSelB();
	static bbcSaveState::LoadStatus LoadRomSelBSaveState(const bbcSaveState *save_state);
	static bbcSaveState::SaveStatus SaveRomSelBSaveState(bbcSaveState *save_state);

	//B with Watford ROM stuff
	static t65::byte ReadRomSelBWatford(t65::word addr);
	static void WriteRomSelBWatford(t65::word addr,t65::byte value);
	static void WriteWatfordRomBoard(t65::word addr,t65::byte value);
	
	//B+ romsel
	static t65::byte ReadRomSelBPlus(t65::word addr);
	static void WriteRomSelBPlus(t65::word addr,t65::byte value);

	//Master ROMSEL
	static t65::byte ReadRomSelMaster128(t65::word addr);
	static void WriteRomSelMaster128(t65::word addr,t65::byte value);
private:
	static const bbcModel *model_;
	static const bbc1770Interface *disc_interface_;
//	static t65::byte PhysicalRomSlot(t65::byte logical_rom_slot);
	static bbcRomSlot rom_slots_[16];
	static bbcRomSlot os_rom_;
	static t65::byte romsel_;
	static t65::byte watford_romsel_;

	//These are only for reading the register
	static bool vdusel_;//bit 7 of FE34, B+ only.
	static bool paged_ram_sel_;//bit 7 of FE30, B+ only.
	static bool andy_set_;//bit 7 of FE30, M128 only
	static t65::byte acccon_;//FE34, M128 only
//	static t65::byte romsel_mask_,romsel_fixed_bits_;//TODO Remove, overly complex.

	//////////////////////////////////////////////////////////////////////////
	// read_pages_
	// write_pages_
	// These are used for B+ and BBC B. They generally stay fixed. The Master
	// pages will be elsewhere since they get changed a bit more (and in a more
	// straightforward manner).
	//
	//These are always the read_pages for a default Beeb.
	//That is, <blah>_pages[X] points to the Xth page in normal RAM.
	//No shadow or anything like that!
	static t65::byte *read_pages_[256];
	static t65::byte *write_pages_[256];

	//These are used for shadow RAM mode (VDUSEL set)
	//They are the same as the normal pages, except that pages between 0x30
	//and 0x7F (inclusive) point at the shadow RAM area.
	static t65::byte *shadow_read_pages_[256];
	static t65::byte *shadow_write_pages_[256];

	//According to MSB of PC (and VDUSEL of course) each entry here points
	//either to X_pages_ or shadow_X_pages_.
	static t65::byte **pc_read_pages_[256];
	static t65::byte **pc_write_pages_[256];

#ifdef bbcENABLE_BEEB_ICE
	static t65::byte *ice_brk_pages_[256];
	static t65::byte *ice_shadow_brk_pages_[256];
	static t65::byte **ice_pc_brk_pages_[256];
	static t65::byte ice_brk_[128*1024];
#endif//bbcENABLE_BEEB_ICE

	//////////////////////////////////////////////////////////////////////////
	// Experimental
	//
	// A bit of a testbed for the exciting new Fully Plugin-based Architecture!
	//
	struct Range {
		t65::byte start,num;
	};

	static void SetPagedRom(int slot);
	static void SetCrtcBase(t65::word base);
	static void SetShadowRamAccessPages(const Range *ranges,unsigned num_ranges);
	static void SetMappedReadWriteRomPages(const Range *ranges,const t65::word *offsets,unsigned num_ranges);

	//////////////////////////////////////////////////////////////////////////
	// Memory mapped I/O functions
	//
	// These handle reads and writes to the area 0xFC00-0xFFFF inclusive.
	// 1/3/04 extendend up to FFFF for Watford ROM board support (rom selector at FF30+n)
	static bbcWriteMmioFn mmio_write_fns_[0x400];
	static bbcReadMmioFn mmio_read_fns_[0x400];
#ifdef bbcDEBUG_COUNT_MMIO_ACCESSES
public://TODO work out what to do with this :)
	static unsigned mmio_writes_count_[0x400];
	static unsigned mmio_reads_count_[0x400];
private:
#endif
	
	static const bbcMmioFnInit mmio_fn_inits_[];

	static void ResetMmioFns();
	static void AddMmioFns(const bbcMmioFnInit *inits);
	static void SetMmioFn(t65::word addr,bbcReadMmioFn read_fn,
		bbcWriteMmioFn write_fn);

	//refreshes all I/O functions according to current config
	static void RefreshMmioFns();
};
/*
inline t65::byte bbcComputer::PhysicalRomSlot(t65::byte logical_rom_slot) {
	return (logical_rom_slot&romsel_mask_)|romsel_fixed_bits_;
}
*/
inline void bbcComputer::SetRomWriteable(int slot,bool writeable) {
	rom_slots_[slot].SetWriteable(writeable);
}

inline void bbcComputer::SetOsRomContents(const t65::byte *contents,unsigned size_contents) {
	os_rom_.SetWriteable(false);
	os_rom_.SetContents(contents,size_contents);
}

//this synchronizes the 1mhz and 2mhz clocks.
inline void bbcComputer::SyncClocks() {
	if(cpucycles&1) {
		//++cpucycles;//test
		cpucycles+=2;//original
	} else {
		//cpucycles+=2;//test
		++cpucycles;//original
	}
}

inline void bbcComputer::SetNextStop(int new_next_stop) {
	if(int(new_next_stop-next_stop)<0) {//cycles<next_stop) {
		next_stop=new_next_stop;
//		BASSERT(next_stop>=cpucycles);
	}
}

inline void bbcComputer::SetNextStopDelta(int delta) {
	if(int(cpucycles+delta-next_stop)<0) {//cycles+delta<next_stop) {
		next_stop=cpucycles+delta;
//		BASSERT(next_stop>=cpucycles);
	}
}

inline void bbcComputer::SetNextIrqStop(int new_next_stop) {
	if(!(cpustate.p&t65::I_MASK)&&int(new_next_stop-next_stop)<0) {//cycles<next_stop) {
		next_stop=new_next_stop;
//		BASSERT(next_stop>=cpucycles);
	}
}

inline void bbcComputer::SetNextIrqStopDelta(int delta) {
	if(!(cpustate.p&t65::I_MASK)&&int(cpucycles+delta-next_stop)<0) {//cycles+delta<next_stop) {
		next_stop=cpucycles+delta;
//		BASSERT(next_stop>=cpucycles);
	}
}

inline void bbcComputer::ResetNMI() {
	irq_flags_&=~IRQ_NMI;
}

inline void bbcComputer::SetNMI() {
	irq_flags_|=IRQ_NMI;
	next_stop=cpucycles;//stop now
}

inline int bbcComputer::DisassembleBytes(const t65::byte *bytes,t65::Word bytes_addr,
	char *opcode_buf,char *operand_buf)
{
	return model_->DisassembleBytes(bytes,bytes_addr,opcode_buf,operand_buf);
}

inline const bbcModel *bbcComputer::Model() {
	return model_;
}

//////////////////////////////////////////////////////////////////////////

#endif
