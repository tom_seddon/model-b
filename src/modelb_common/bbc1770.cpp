#include "pch.h"
#include "bbc1770.h"
#include "bbcComputer.h"
#include "bbcFdd.h"
#include "bbcKeyboardMatrix.h"
#include <string>
//#include <vector>
#include <stdarg.h>
#include <map>

// WD1770 emulator -- 3rd time lucky :)
//
// State machine. 'cmd_update_fn_' is current state fn, 0 if idle state.
//
//	CmdUpdateWinddown();
//		Do NMI.
//		Idle.
//
//	CmdUpdateSeek();
//		Seek to (cmd_track,cmd_sector).
//		Idle.
//
//	CmdUpdateReadSector();
//		If DRQ, data lost error.
//		Place byte at (cmd_track,cmd_sector,+cmd_cur_offset) in data register.
//		Set DRQ.
//		Do NMI.
//		Advance to next byte.
//		If end of track, idle.
//		If not end of track, CmdUpdateReadSector.
//
//	CmdUpdateWriteSectorSetDrq();
//		If DRQ, data lost error.
//		Set DRQ.
//		Do NMI.
//		CmdUpdateWriteSectorWriteData.	
//
//	CmdUpdateWriteSectorWriteData();
//		Write data register to (cmd_track,cmd_sector,+cmd_cur_offset).
//		Advance to next byte.
//		If end of track, idle state.
//		If not end of track, CmdUpdateWriteSectorSetDrq.
//
//	CmdUpdateReadAddress();
//		Return cmd_cur_offset'th byte of address in data register.
//		Advance to next byte.
//		If end of data, idle state.
//		If not end of data, CmdUpdateReadAddress.
//
// Bugs:
//
// 1.	No attempt at timing. I didn't even bother.
// 2.	Read Track/Write Track not supported. Without a decent Beeb disc
//		image format there's no point. Silently failing on Write Track allows
//		*FORMAT to pretty much work OK.
// 3.	Read Address doesn't produce a valid CRC.
//
// Issues:
//
// 1.	Write Sector has a two byte delay for each byte written. Without this,
//		Watford DDFS plain wouldn't work. There must be a delay between writing
//		the byte and INTRQ for the next one because Watford DDFS delays for a
//		certain period of time after it's written the last byte of a write before
//		cancelling the command.
// 2.	Double density disabled. I see no reason why it shouldn't work. Disc
//		image format issues, this.
//
//define to disable double density mode -- recommended for now, because the
//disc drive stuff doesn't cope with it yet.
//#define bbc1770_DISABLE_DOUBLE_DENSITY

#ifdef bbc1770_LOG

static FILE *log_h=0;

#define LOG(X) X

//I trust printf to say "(null)" if looking at a nonexistent entry...
//std::map<bbc1770::CmdUpdateFn,std::string> cmd_names;

//const char *CURCMDNAME()
//#define CURCMDNAME() (cmd_names[cmd_update_fn_].c_str())

#define YES_NO_OF(X) ((X)?"yes":"no")

static void FdcLog(const char *fmt,...) {
	fprintf(log_h,"%-9d: ",bbcComputer::Cycles2MHz());
	va_list v;
	va_start(v,fmt);
	vfprintf(log_h,fmt,v);
	va_end(v);
}

#else

#define LOG(X) (void(0))

#endif

#define CYCLES_FROM_MS(N) ((N)*(2000000/1000))

static const int cycles_per_byte=256;

//1770 registers
t65::byte bbc1770::status_;
t65::byte bbc1770::track_;
t65::byte bbc1770::sector_;
t65::byte bbc1770::data_;

//Physical drive thingies.
int bbc1770::phys_track_;
int bbc1770::phys_sector_;
int bbc1770::last_step_;

//Externally controlled
bbc1770DriveControl bbc1770::drive_control_;

//Command processing stuff
bbc1770::CmdUpdateMode bbc1770::cmd_update_mode_;//bbc1770::CmdUpdateFn bbc1770::cmd_update_fn_;
unsigned bbc1770::cmd_flags_;
int bbc1770::cmd_track_;
int bbc1770::cmd_sector_;
int bbc1770::cmd_num_sectors_left_;
int bbc1770::cmd_cur_offset_;
int bbc1770::cmd_next_byte_at_;
t65::byte bbc1770::cmd_address_[6];

//
const bbc1770::CmdUpdateFn bbc1770::cmd_update_fns_[bbc1770::UPDATE_MAX]={
	0,//UPDATE_IDLE,
	&bbc1770::CmdUpdateWinddown,//UPDATE_WINDDOWN,
	&bbc1770::CmdUpdateSeek,//UPDATE_SEEK,
	&bbc1770::CmdUpdateReadSector,//UPDATE_READ_SECTOR,
	&bbc1770::CmdUpdateWriteSectorSetDrq,//UPDATE_WRITE_SECTOR_SET_DRQ,
	&bbc1770::CmdUpdateWriteSectorWriteData,//UPDATE_WRITE_SECTOR_WRITE_DATA,
	&bbc1770::CmdUpdateReadAddress,//UPDATE_READ_ADDRESS,
};

#ifdef bbc1770_LOG

static const char *cmd_update_names[bbc1770::UPDATE_MAX]={
	"UPDATE_IDLE",
	"UPDATE_WINDDOWN",
	"UPDATE_SEEK",
	"UPDATE_READ_SECTOR",
	"UPDATE_WRITE_SECTOR_SET_DRQ",
	"UPDATE_WRITE_SECTOR_WRITE_DATA",
	"UPDATE_READ_ADDRESS",
};

#define CURCMDNAME() (cmd_update_names[cmd_update_mode_])
#endif

const bbc1770::FdcAction bbc1770::fdc_action_from_update_mode_[bbc1770::UPDATE_MAX]={
	ACTION_IDLE,//UPDATE_IDLE,
	ACTION_OTHER,//UPDATE_WINDDOWN,
	ACTION_OTHER,//UPDATE_SEEK,
	ACTION_READ,//UPDATE_READ_SECTOR,
	ACTION_WRITE,//UPDATE_WRITE_SECTOR_SET_DRQ,
	ACTION_WRITE,//UPDATE_WRITE_SECTOR_WRITE_DATA,
	ACTION_OTHER,//UPDATE_READ_ADDRESS,
};

//Interface
bbc1770Interface bbc1770::disc_interface_;

//Convert bool to SINGLE_DENSITY/DOUBLE_DENSITY
#define DENSITY_OF(IS_DD) ((IS_DD)?bbcFdd::DENSITY_DOUBLE:bbcFdd::DENSITY_SINGLE)

//////////////////////////////////////////////////////////////////////////

#ifdef bbc1770_LOG
static const char *GetBinary(t65::byte byte,char *binary) {
	char *dest=binary;
	for(int i=0;i<8;++i) {
		*dest++=byte&0x80?'1':'0';
		byte<<=1;
	}
	*dest=0;
	return binary;
}
#endif//bbc1770_LOG

void bbc1770::Init(const bbc1770Interface &disc_interface) {
#ifdef bbc1770_LOG
	for(unsigned i=0;i<UPDATE_MAX;++i) {
		BASSERT(strncmp(cmd_update_names[i],"UPDATE_",7)==0);//this will catch any I miss out when (if) adding new ones.
	}
#endif

	disc_interface_=disc_interface;

#ifdef bbc1770_LOG
	log_h=fopen("bbc1770.txt","wt");
	if(!log_h) {
		log_h=stdout;
	}
	fprintf(log_h,"Beeb 1770: interface type \"%s\"\n",disc_interface_.name);
#endif

	Reset();

	(*disc_interface_.get_drive_control_from_byte_fn)(0,&drive_control_);
		
//	SetInterfaceType(ACORN);
}

void bbc1770::Reset() {
	track_=0;
	sector_=0;
	data_=0;
	
	//0 is just 'ok'
	status_=0;
	
	phys_track_=0;
	phys_sector_=0;
	cmd_update_mode_=UPDATE_IDLE;//cmd_update_fn_=0;
	
	last_step_=0;
	
	cmd_flags_=0;
}

void bbc1770::SetNMI() {
	bbcComputer::SetNMI();
	LOG(FdcLog("**** NMI ****\n"));
}

//static int SignOf(int n) {
	//if(n<0) {
		//return -1;
	//} else if(n>0) {
		//return 1;
	//}
	//return 0;
//}

void bbc1770::WriteCommandRegister(t65::byte value) {
//	FdcCmdType old_current_cmd=current_cmd_;
	LOG(FdcLog("\tWrite Command Register\n"));
	if(cmd_update_mode_!=UPDATE_IDLE) {
		//Ignore everything except a Force Interrupt
		static const unsigned forceirq_setbits=0x80+0x40+0x10;
		static const unsigned forceirq_mask=~(4u+8u);
		if((value&forceirq_mask)==forceirq_setbits) {
			LOG(FdcLog("\tCommand: Force IRQ (D0) during other command\n"));
			bool i2=!!(value&4);
			bool i3=!!(value&8);
			//This doesn't work correctly, it just stops right now (regardless of i2).
			//It does however interrupt or not, depending on i3.
			if(i2) {
				//Should acknowledge 'at next index pulse' (???)
				//int brk=0;
				LOG(FdcLog("\t\tForce IRQ: i2 set\n"));
			}

			if(i3) {
				SetNMI();
				LOG(FdcLog("\t\tForce IRQ: i3 set, NMI.\n"));
			} 
			status_=0;
			cmd_update_mode_=UPDATE_IDLE;
		}
	} else if(!(value&0x80)) {
		cmd_flags_=0;
		//Type I
		//These all end up moving the disc head around, hence SEEK
		t65::byte cmd=(value>>5)&3;
		switch(cmd) {
		case 0:
			//Restore/Seek
			if(value&0x10) {
				//Seek
				//phys_track_=data_;
				cmd_track_=data_;
				//cmd_update_track_=true;
				cmd_flags_|=CMD_UPDATE_TRACK;
				LOG(FdcLog("\tCommand: Seek\n"));
				LOG(FdcLog("\t\tSeek: track %u\n",track_));
				LOG(FdcLog("\t\tSeek: update=%s\n",YES_NO_OF(cmd_flags_&CMD_UPDATE_TRACK)));

				if(disc_interface_.flags&bbc1770Interface::DISABLE_RESTORE_IRQ) {
					cmd_flags_|=CMD_NO_IRQ;//Challenger
				}
			} else {
				//Restore
				//phys_track_=0;
				//track_=0;
				cmd_track_=0;
				//cmd_update_track_=true;
				cmd_flags_|=CMD_UPDATE_TRACK;
				if(disc_interface_.flags&bbc1770Interface::DISABLE_RESTORE_IRQ) {
					cmd_flags_|=CMD_NO_IRQ;//Challenger
				}
				LOG(FdcLog("\tCommand: Restore\n"));
				LOG(FdcLog("\t\tRestore: update track=%s\n",YES_NO_OF(cmd_flags_&CMD_UPDATE_TRACK)));
				LOG(FdcLog("\t\tRestore: no_irq=%s\n",YES_NO_OF(cmd_flags_&CMD_NO_IRQ)));
				//cmd_flags_|=CMD_SEEK_RESTORE;
			}
			break;
		case 1:
		case 2:
		case 3:
			//Step
			if(cmd==2) {
				LOG(FdcLog("\tCommand: Step in (track+)\n"));
				//Step in
				//last_step_=SignOf(76-phys_track_);
				//14/3/04 I think the above is wrong; by "76" the datasheet actually means "biggest track" :)
				last_step_=1;
			} else if(cmd==3) {
				LOG(FdcLog("\tCommand: Step out (track-)\n"));
				//Step to 76
				//last_step_=SignOf(0-phys_track_);
				//14/3/04 see comment for step in.
				last_step_=-1;
			} else {
				LOG(FdcLog("\tCommand: Step last\n"));
				LOG(FdcLog("\t\tStep last: last_step=%d\n",last_step_));
			}
			cmd_track_=phys_track_+last_step_;
			//cmd_update_track_=!!(value&0x10);
			if(value&0x10) {
				cmd_flags_|=CMD_UPDATE_TRACK;
			}
			LOG(FdcLog("\t\tType I Step: update track=%s\n",
				YES_NO_OF(cmd_flags_&CMD_UPDATE_TRACK)));
			break;
		}
		if(value&0x4) {
			cmd_flags_|=CMD_SEEK_VERIFY;
		}
		//See datasheet 1-7
		cmd_sector_=0;
		status_=0;
		StartCmd(UPDATE_SEEK,0);//0);//2000000);//TODO arbitrary delay for motor spin up.
	} else if((value&0xC0)==0x80) {
		//Type II

		//See datasheet 1-9 for what happens to the bits!
		status_=0;
		
		//CmdUpdateFn update_fn;
		CmdUpdateMode new_mode;
		const char *type;
		if(!(value&0x20)) {
			//Read
			type="Read";
			new_mode=UPDATE_READ_SECTOR;//update_fn=&CmdUpdateReadSector;
			LOG(FdcLog("\tWrite Command: Read sector(s)\n"));
			bbcKeyboardMatrix::SetForceShiftPressed(false);
		} else {
			//Write
			type="Write";
			new_mode=UPDATE_WRITE_SECTOR_SET_DRQ;//update_fn=&CmdUpdateWriteSectorSetDrq;
			LOG(FdcLog("\tWrite Command: Write sector(s)\n"));
		}
		cmd_num_sectors_left_=1;
		if(value&0x10) {
			cmd_num_sectors_left_=INT_MAX;//10-sector_;
		}
			//StartCmd(cmdtype,nsectors);

		cmd_track_=track_;
		cmd_sector_=sector_;
		cmd_cur_offset_=0;

		status_=0;

		LOG(FdcLog("\t\tType II: num sectors left=%d\n",cmd_num_sectors_left_));
		LOG(FdcLog("\t\tType II: track=%d\n",cmd_track_));
		LOG(FdcLog("\t\tType II: sector=%d\n",cmd_sector_));
		
		StartCmd(new_mode,30000);//(value&4)?30000:0);
	} else if((value&0xC0)==0xC0) {
		//Type III/IV
		t65::byte cmd=(value>>4)&3;
		switch(cmd) {
		case 0:
			cmd_address_[0]=track_;
			cmd_address_[1]=(t65::byte)drive_control_.side;
			cmd_address_[2]=sector_;
			cmd_address_[3]=1;//256 bytes per sector
			cmd_address_[4]=0;//This is the CRC.
			cmd_address_[5]=0;//I hope noone checks it!
			LOG(FdcLog("\tWrite Command: Read Address\n"));
			LOG(FdcLog("\t\tRead Address: track=%d\n",cmd_address_[0]));
			LOG(FdcLog("\t\tRead Address: phys side=%d\n",cmd_address_[1]));
			LOG(FdcLog("\t\tRead Address: sector=%d\n",cmd_address_[2]));
			cmd_cur_offset_=0;
			//Read Address
			StartCmd(UPDATE_READ_ADDRESS,0);
			break;
		case 3:
			//Write Track.
			LOG(FdcLog("\tWrite Command: Write Track\n"));
			LOG(FdcLog("\t\tWrite Track: UNSUPPORTED\n"));
			break;
		case 1:
			//Reset Now
			switch(value&0xf) {
			case 0:
				status_=0;
				LOG(FdcLog("\tWrite Command: D0 reset\n"));
				cmd_update_mode_=UPDATE_IDLE;//cmd_update_fn_=0;
				LOG(FdcLog("\t\tReset: command now NOWT.\n"));
				break;
			case 4:
				//Used by Superior Collection *INIT command
				LOG(FdcLog("\tWrite Command: D4 IRQ on next index pulse\n"));
				cmd_update_mode_=UPDATE_WINDDOWN;//cmd_update_fn_=&CmdUpdateWinddown;
				LOG(FdcLog("\tReset: wind down soon.\n"));
				break;
			default:
				goto unknown;
			}
			break;
unknown:
		default:
			{
				LOG(FdcLog("\tWrite Command: UNSUPPORTED COMMAND.\n"));
				//int brk=0;
			}
			break;
		}
	}
}

t65::byte bbc1770::ReadStatusRegister() {
	//TODO This should reset just INTRQ. B+ service manual says (p21):
	//
	//"Two interrupt signals come from a 1770, pins 27 [DRQ] and 28 [INTRQ].
	//The two interrupts are inverted and wire NORed on to the notNMI line
	//by two parts of IC7 (quad NAND gate). "
	//
	//So this should reset NMI only if not DRQ.
	//
	//Opus DDOS does this:
	//
	//16229941  (@16229951 ) 0D36: LDA   &FE80      AD 80 FE -> A:81 X:00 Y:00 S:DD P:N------C
	//16229946  (@16229951 ) 0D39: EOR   #&00       49 00    -> A:81 X:00 Y:00 S:DD P:N------C
	//16229948  (@16229951 ) 0D3B: AND   #&01       29 01    -> A:01 X:00 Y:00 S:DD P:-------C
	//16229950  (@16229951 ) 0D3D: BNE   &0D36      D0 F7    -> A:01 X:00 Y:00 S:DD P:-------C
	//
	//whilst waiting for Read Sector to be done, obviously this cocks up
	//the NMI processing if reading status resets NMI so for now reading
	//the status register does nothing.
//	bbcComputer::ResetNMI();//bbcComputer::irq_flags_&=~IRQ_NMI;
	return status_|STATUS_MOTOR_ON;//TODO possibly this is why Opus/Challenger can't autodetect DD??
}

t65::byte bbc1770::ReadTrackRegister() {
	return track_;
}

void bbc1770::WriteTrackRegister(t65::byte value) {
	track_=value;
	LOG(FdcLog("\tWrite Track Register\n"));
	LOG(FdcLog("\tTrack register=%d\n",track_));
}

t65::byte bbc1770::ReadSectorRegister() {
	return sector_;
}

void bbc1770::WriteSectorRegister(t65::byte value) {
	sector_=value;
	LOG(FdcLog("\tWrite Sector Register\n"));
	LOG(FdcLog("\tSector register=%u\n",sector_));
}

t65::byte bbc1770::ReadDataRegister() {
	status_&=~STATUS_TYPE23_DATAREQUEST;
	LOG(FdcLog("Read Data Register\n"));
	LOG(FdcLog("\tContents=%d (&%02X) ('%c')\n",data_,data_,isprint(data_)?data_:'_'));
	return data_;
}

void bbc1770::WriteDataRegister(t65::byte value) {
	data_=value;
	status_&=~STATUS_TYPE23_DATAREQUEST;
	LOG(FdcLog("\tWrite Data Register\n"));
	LOG(char status_binary[9]);
	LOG(GetBinary(status_,status_binary));
	LOG(FdcLog("\tStatus=%d &%02X %%%s\n",status_,status_,status_binary));
}

//////////////////////////////////////////////////////////////////////////
//Address offset      Contains on read    on write
//------------------------------------------------------
//0                 Status         Command
//1                 ------- Track --------
//2                 ------- Sector -------
//3                 ------- Data ---------

t65::byte bbc1770::ReadFdc(t65::word addr) {
	LOG(const char *reg="unknown");
	t65::byte result=0;
	switch(addr&0x03) {
	case 0:
		LOG(reg="status");
		result=ReadStatusRegister();
		break;
	case 1:
		LOG(reg="track");
		result=ReadTrackRegister();
		break;
	case 2:
		LOG(reg="sector");
		result=ReadSectorRegister();
		break;
	case 3:
		LOG(reg="data");
		result=ReadDataRegister();
		break;
	}
	return result;
}

#define SHOWSTATUS()\
	LOG(GetBinary(status_,binary_val));\
	LOG(FdcLog("\tStatus register=%%%s (&%02X) (%d)\n",binary_val,status_,status_))

void bbc1770::WriteFdc(t65::word addr,t65::byte val) {
	LOG(char binary_val[9]);
	LOG(GetBinary(val,binary_val));
	LOG(FdcLog("Write 1770: addr=0x%04X val=%d (&%02X) (%%%s)\n",addr,val,val,binary_val));
	switch(addr&0x03) {
	case 0:
		SHOWSTATUS();
		WriteCommandRegister(val);
		SHOWSTATUS();
		break;
	case 1:
		WriteTrackRegister(val);
		break;
	case 2:
		WriteSectorRegister(val);
		break;
	case 3:
		SHOWSTATUS();
		WriteDataRegister(val);
		SHOWSTATUS();
		break;
	}
}

//////////////////////////////////////////////////////////////////////////
// Drive control stuff.
#ifdef bbc1770_LOG
static void LogDriveControl(FILE *h,const bbc1770DriveControl &ctl) {
	fprintf(h,"drv=%d side=%d double density=%s disable irq=%s",
		ctl.drive,ctl.side,YES_NO_OF(ctl.is_double_density),
		YES_NO_OF(ctl.disable_irq));
}
#endif

t65::byte bbc1770::ReadControl(t65::word addr) {
	(void)addr;

	t65::byte result;
	(*disc_interface_.get_byte_from_drive_control_fn)(drive_control_,&result);
	return result;
}

void bbc1770::WriteControl(t65::word addr,t65::byte val) {
	(void)addr;

	(*disc_interface_.get_drive_control_from_byte_fn)(val,&drive_control_);
	LOG(char val_binary[9]);
	LOG(GetBinary(val,val_binary));
	LOG(FdcLog("Write Control: val=%d (&%02X) (%%%s)\n",val,val,val_binary));
	LOG(FdcLog("\tDrive=%d\n",drive_control_.drive));
	LOG(FdcLog("\tSide=%d\n",drive_control_.side));
	LOG(FdcLog("\tDouble density=%s\n",YES_NO_OF(drive_control_.is_double_density)));
	LOG(FdcLog("\tDisable IRQ=%s\n",YES_NO_OF(drive_control_.disable_irq)));
}

bool bbc1770::IsCurrentCmdAddressValid(bbcFdd::DiscDensity density) {
	return bbcFdd::IsOkAddressFdc(drive_control_.drive,drive_control_.side,
		cmd_track_,cmd_sector_,density);//drive_control_.is_double_density);
}

//////////////////////////////////////////////////////////////////////////
//
void bbc1770::Update() {
	const int cycles=bbcComputer::Cycles2MHz();//11/12/2004 -- 
	if(cmd_update_mode_!=UPDATE_IDLE) {
		if(cycles-cmd_next_byte_at_<0) {//bbcComputer::cycles<cmd_next_byte_at_) {
			bbcComputer::SetNextStop(cmd_next_byte_at_);
			return;
		}
		LOG(FdcLog("Update Command: '%s': phys_track=%d phys_sector=%d\n",
			CURCMDNAME(),phys_track_,phys_sector_));
		if(cmd_update_fns_[cmd_update_mode_]) {
			(*cmd_update_fns_[cmd_update_mode_])();//(*cmd_update_fn_)();
		}

		//was ACTION_IDLE. think this was what I must have meant, though... right? safe change, both are 0 (19/7/04)
		if(cmd_update_mode_!=UPDATE_IDLE) {
			//more to come!
			cmd_next_byte_at_+=cycles_per_byte;
			bbcComputer::SetNextStop(cmd_next_byte_at_);
		}
		LOG(fflush(log_h));
	}
}

//////////////////////////////////////////////////////////////////////////
// Update WINDDOWN command
void bbc1770::CmdUpdateWinddown() {
	LOG(FdcLog("Update Wind Down:\n"));
	LOG(FdcLog("\tphys_drive=%d phys_side=%d phys_track=%d phys_sector=%d track=%d sector=%d status=&%02X\n",
		drive_control_.drive,drive_control_.side,phys_track_,phys_sector_,track_,sector_,status_));
	status_&=~STATUS_BUSY;
	if(cmd_flags_&CMD_NO_IRQ) {
		LOG(FdcLog("\tno_irq flag specified.\n"));
	} else {
		LOG(FdcLog("\t6502 NMI.\n"));
		SetNMI();
	}
	cmd_update_mode_=UPDATE_IDLE;
}

//////////////////////////////////////////////////////////////////////////
// Update SEEK command
void bbc1770::CmdUpdateSeek() {
	LOG(FdcLog("Update Seek:\n"));
	if(cmd_flags_&CMD_SEEK_RESTORE) {
		//TODO -- this is UGLY
		phys_track_=0;

		status_=0xA4;//challenger does this...

		if(disc_interface_.flags&bbc1770Interface::INVERT_TRACK0) {
			//nonstandard Watford DDFS behaviour says BeebEm!
			StatusFlagIf(STATUS_TYPE1_NOTTRACKZERO,phys_track_==0);//temp
		} else {
			StatusFlagIf(STATUS_TYPE1_NOTTRACKZERO,phys_track_!=0);
		}

		LOG(FdcLog("\trestore, status=0x%02X\n",status_));
	} else {
		if((cmd_flags_&CMD_SEEK_VERIFY)&&
			!IsCurrentCmdAddressValid(DENSITY_OF(drive_control_.is_double_density))) {//IsValidAddress(drive_control_.drive,drive_control_.side,cmd_track_,0)) {
			//Aaargh
			//The datasheet wibbles about the 'seek error' bit. There doesn't
			//appear to be one of these! I'm assuming this is the 'record
			//not found' one.
			//I guess the track # may be updated with the track it DID find,
			//whatever I don't bother with that.
			//status_|=STATUS_RECORDNOTFOUND;
			status_=STATUS_MOTOR_ON|STATUS_RECORDNOTFOUND;//tested on CHALLENGER
			LOG(FdcLog("\t(dr%d s%d t%d sec%d) bad: RECORDNOTFOUND (status=&%02X)\n",
				drive_control_.drive,drive_control_.side,cmd_track_,cmd_sector_,status_));
//			cmd_flags_|=CMD_NO_IRQ;
		} else {
			//Hurrah, all ok.
			phys_track_=cmd_track_;
			if(cmd_flags_&CMD_UPDATE_TRACK) {
				track_=(t65::byte)phys_track_;
			}
			if(disc_interface_.flags&bbc1770Interface::INVERT_TRACK0) {
				//nonstandard Watford DDFS behaviour says BeebEm!
				StatusFlagIf(STATUS_TYPE1_NOTTRACKZERO,phys_track_==0);//temp
			} else {
				StatusFlagIf(STATUS_TYPE1_NOTTRACKZERO,phys_track_!=0);
			}
			LOG(FdcLog("\t(dr%d s%d t%d sec%d) ok\n",
				drive_control_.drive,drive_control_.side,cmd_track_,cmd_sector_,status_));

			status_|=STATUS_TYPE1_SPINUPENDED;
		}
	}
	
	cmd_update_mode_=UPDATE_WINDDOWN;//cmd_update_fn_=&CmdUpdateWinddown;
}

//////////////////////////////////////////////////////////////////////////
// Update READ SECTOR command
//
// For each byte, 1770 presents byte in data register and asserts DRQ.
// BBC gets NMI, reads byte, 1770 sends the next one.
void bbc1770::CmdUpdateReadSector() {
	LOG(FdcLog("\tUpdate Read Sector:\n"));
	if(status_&STATUS_TYPE23_DATAREQUEST) {
		LOG(FdcLog("\t\tdata lost.\n"));
		status_=STATUS_TYPE23_LOSTDATA;
		cmd_update_mode_=UPDATE_WINDDOWN;//cmd_update_fn_=&CmdUpdateWinddown;
	} else {
#ifdef bbc1770_LOG
		int real_drive=bbcFdd::DriveFromFdcAddress(drive_control_.drive,drive_control_.side);
#endif
		if(!bbcFdd::GetByteFdc(drive_control_.drive,drive_control_.side,cmd_track_,
			cmd_sector_,cmd_cur_offset_,DENSITY_OF(drive_control_.is_double_density),&data_))
		{
			status_|=STATUS_TYPE23_NOTREADY;
			status_|=STATUS_RECORDNOTFOUND;
			cmd_flags_|=CMD_NO_IRQ;
			cmd_update_mode_=UPDATE_WINDDOWN;//cmd_update_fn_=&CmdUpdateWinddown;
			LOG(FdcLog("\t\tRecord not found at: [D%d T%d S%d+%d]\n",
				real_drive,cmd_track_,cmd_sector_,cmd_cur_offset_));
		} else {
			LOG(FdcLog("\t\tByte: 0x%02X (%d) (%c) [D%d T%d S%d+%d]\n",
				data_,data_,isprint(data_)?char(data_):'_',
				real_drive,cmd_track_,cmd_sector_,cmd_cur_offset_));
			status_|=STATUS_TYPE23_DATAREQUEST;
			SetNMI();
			if(!CmdNextByte()) {
				cmd_update_mode_=UPDATE_WINDDOWN;//cmd_update_fn_=&CmdUpdateWinddown;
			}

//			if(CmdNextByte()) {
//				//Signal more data for CPU
//			} else {
//				cmd_update_fn_=&CmdUpdateWinddown;
//			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Update WRITE SECTOR command
//
// At last, this works!
//
void bbc1770::CmdUpdateWriteSectorSetDrq() {
	LOG(FdcLog("\tUpdate Write Sector Set DRQ:\n"));
	status_|=STATUS_TYPE23_DATAREQUEST;
	SetNMI();
	cmd_update_mode_=UPDATE_WRITE_SECTOR_WRITE_DATA;//cmd_update_fn_=&CmdUpdateWriteSectorWriteData;
//	cmd_next_byte_at_=bbcComputer::cycles+
}

void bbc1770::CmdUpdateWriteSectorWriteData() {
	LOG(FdcLog("\tUpdate Write Sector:\n"));
	if(status_&STATUS_TYPE23_DATAREQUEST) {
		status_|=STATUS_TYPE23_LOSTDATA;
		cmd_update_mode_=UPDATE_WINDDOWN;//cmd_update_fn_=&CmdUpdateWinddown;
		LOG(FdcLog("data lost.\n"));
	} else {
#ifdef bbc1770_LOG
		int real_drive=bbcFdd::DriveFromFdcAddress(drive_control_.drive,
			drive_control_.side);
#endif
		if(!bbcFdd::SetByteFdc(drive_control_.drive,drive_control_.side,cmd_track_,
			cmd_sector_,cmd_cur_offset_,DENSITY_OF(drive_control_.is_double_density),data_))
		{
			status_|=STATUS_RECORDNOTFOUND;
			cmd_update_mode_=UPDATE_WINDDOWN;//cmd_update_fn_=&CmdUpdateWinddown;
			LOG(FdcLog("\t\tRecord not found at: [D%d T%d S%d+%d]\n",
				real_drive,cmd_track_,cmd_sector_,cmd_cur_offset_));
		} else {
			LOG(FdcLog("\t\tByte: 0x%02X (%d) (%c) [D%d T%d S%d+%d]\n",
				data_,data_,isprint(data_)?char(data_):'_',
				real_drive,cmd_track_,cmd_sector_,cmd_cur_offset_));
			
			if(!CmdNextByte()) {
				//No data left.
//				status_&=~STATUS_BUSY;//? 1/11/2003 test -- ddos double density broken!
//				status_&=~STATUS_TYPE23_DATAREQUEST;
				cmd_update_mode_=UPDATE_WINDDOWN;//cmd_update_fn_=&CmdUpdateWinddown;
			} else {
				//Still more to come.
//				status_|=STATUS_TYPE23_DATAREQUEST;
				cmd_update_mode_=UPDATE_WRITE_SECTOR_SET_DRQ;//cmd_update_fn_=&CmdUpdateWriteSectorSetDrq;
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Update READ ADDRESS command.
void bbc1770::CmdUpdateReadAddress() {
	//Next byte to CPU.
	data_=cmd_address_[cmd_cur_offset_];
	SetNMI();
				
	//If done that's that
	++cmd_cur_offset_;
	if(cmd_cur_offset_>=6) {
		//The data sheet says to copy the track address into the sector
		//register. That sounds pretty wierd but there you go.
		sector_=cmd_address_[0];//track_;

		cmd_update_mode_=UPDATE_WINDDOWN;//cmd_update_fn_=&CmdUpdateWinddown;
	}
}

//////////////////////////////////////////////////////////////////////////
// Advance to next byte of a read/write sector command.
//
// Returns true if the command should continue, or false if the last byte was
// just processed. (And if false returned, don't call it again!!)
bool bbc1770::CmdNextByte() {
	bool result=true;

	++cmd_cur_offset_;
	if(cmd_cur_offset_>=256) {
		cmd_cur_offset_=0;
		++cmd_sector_;
		--cmd_num_sectors_left_;

		//This is to fix Watford DDFS, which otherwise reads
		//too many bytes. I think that a better value would be approx
		//# bytes between data portions of adjacent sectors, but
		//10 seems to work for now...
		//13/3/04 upped it to 100, a very lazy very random very hacky attempt to fix ADFS.
		cmd_next_byte_at_+=100*cycles_per_byte;

		bool is_valid=IsCurrentCmdAddressValid(bbcFdd::DENSITY_ANY);
		if(cmd_num_sectors_left_==0||!is_valid) {
			result=false;
		}
	}
	phys_track_=track_=(t65::byte)cmd_track_;
	phys_sector_=sector_=(t65::byte)cmd_sector_;
	return result;
}

//////////////////////////////////////////////////////////////////////////
//
void bbc1770::StartCmd(CmdUpdateMode start_mode,int delay) {
	BASSERT(cmd_update_mode_==UPDATE_IDLE);//!cmd_update_fn_);
#ifdef bbc1770_DISABLE_DOUBLE_DENSITY
	if(drive_control_.is_double_density) {
		status_=STATUS_BUSY|STATUS_RECORDNOTFOUND;
		LOG(FdcLog("StartCmd: DD mode disabled: disc fault.\n"));
		cmd_update_mode_=UPDATE_WINDDOWN;//cmd_update_fn_=&CmdUpdateWinddown;
		cmd_flags_=0;
		return;
	}
#endif

	//current_cmd_=cmd;
	cmd_update_mode_=start_mode;//cmd_update_fn_=update_fn;
	cmd_next_byte_at_=bbcComputer::Cycles2MHz()+delay+cycles_per_byte;
	bbcComputer::SetNextStop(cmd_next_byte_at_);
	status_|=STATUS_BUSY;
	//cmd_is_1st_=true;
	LOG(FdcLog("StartCmd:\n"));
	LOG(FdcLog("\tcmd='%s': next stop in %d\n",	CURCMDNAME(),cmd_next_byte_at_-bbcComputer::Cycles2MHz()));
	LOG(FdcLog("\tstatus_=0x%02X\n",status_));
	LOG(FdcLog("\tcmd: trk=%d sec=%d numleft=%d curoffset=%d\n",cmd_track_,cmd_sector_,cmd_num_sectors_left_,cmd_cur_offset_));
	LOG(FdcLog("\tupdate_track=%s no_irq=%s\n",YES_NO_OF(cmd_flags_&CMD_UPDATE_TRACK),YES_NO_OF(cmd_flags_&CMD_NO_IRQ)));
}

void bbc1770::StatusFlagIf(t65::byte mask,bool setting) {
	status_&=~mask;
	if(setting) {
		status_|=mask;
	}
}

bool bbc1770::GetCurrentAction(FdcAction *action,int *drive,int *side,int *track,
	int *sector)
{
	BASSERT(action&&drive&&side&&track&&sector);
	*action=fdc_action_from_update_mode_[cmd_update_mode_];
	*track=phys_track_;
	*sector=phys_sector_;
	*side=drive_control_.side;
	*drive=drive_control_.drive;
	
	return true;
}

t65::byte bbc1770::Status() {
	return status_;
}

//////////////////////////////////////////////////////////////////////////

//	static t65::byte status_;
//	static t65::byte track_;
//	static t65::byte sector_;
//	static t65::byte data_;
//
//	//Where the drive head actually is.
//	static int phys_track_;
//	static int phys_sector_;
//
//	static bbc1770DriveControl drive_control_;
//
//	//Direction of the last Step In/Step Out command.
//	static int last_step_;

//	int drive;
//	int side;
//	bool is_double_density;
//	bool disable_irq;


static const bbcSaveStateItemId bbc_1770_save_state_item_id("wd1770__");


#pragma pack(push,1)
struct bbc1770SaveState1 {
	bbcSaveStateByte status;
	bbcSaveStateByte track;
	bbcSaveStateByte sector;
	bbcSaveStateByte data;
	bbcSaveStateInt32 phys_track;
	bbcSaveStateInt32 phys_sector;
	bbcSaveStateInt32 last_step;

	bbcSaveStateInt32 ctrl_drive;
	bbcSaveStateInt32 ctrl_side;
	bbcSaveStateByte ctrl_is_double_density;
	bbcSaveStateByte ctrl_disable_irq;

	bbcSaveStateInt32 cmd_update_mode;
	bbcSaveStateDword cmd_flags;
	bbcSaveStateInt32 cmd_track;
	bbcSaveStateInt32 cmd_sector;
	bbcSaveStateInt32 cmd_num_sectors_left;
	bbcSaveStateInt32 cmd_cur_offset;
	bbcSaveStateInt32 cmd_next_byte_at;
	bbcSaveStateByte cmd_address[6];
};
#pragma pack(pop)

bbcSaveState::LoadStatus bbc1770::LoadSaveState(const bbcSaveState *save_state) {
	const bbcSaveStateItem *item=save_state->GetItem(bbc_1770_save_state_item_id,0);
	if(!item) {
		return bbcSaveState::LS_MISSING;
	}

	switch(item->version) {
	default:
		return bbcSaveState::LS_BAD_VERSION;

	case 1:
		if(item->data.size()!=sizeof(bbc1770SaveState1)) {
			return bbcSaveState::LS_BAD_DATA;
		} else {
			const bbc1770SaveState1 *data=reinterpret_cast<const bbc1770SaveState1 *>(&item->data[0]);

			if(data->cmd_update_mode<0||data->cmd_update_mode>=UPDATE_MAX) {
				return bbcSaveState::LS_BAD_DATA;
			}

			status_=data->status;
			track_=data->track;
			sector_=data->sector;
			data_=data->data;
			phys_track_=data->phys_track;
			phys_sector_=data->phys_sector;
			last_step_=data->last_step;

			drive_control_.drive=data->ctrl_drive;
			drive_control_.side=data->ctrl_side;
			drive_control_.is_double_density=!!data->ctrl_is_double_density;
			drive_control_.disable_irq=!!data->ctrl_disable_irq;

			cmd_update_mode_=CmdUpdateMode(data->cmd_update_mode);
			cmd_flags_=data->cmd_flags;
			cmd_track_=data->cmd_track;
			cmd_sector_=data->cmd_sector;
			cmd_num_sectors_left_=data->cmd_num_sectors_left;
			cmd_cur_offset_=data->cmd_cur_offset;
			cmd_next_byte_at_=data->cmd_next_byte_at;
			memcpy(cmd_address_,data->cmd_address,6);
		}
		break;
	}

	if(disc_interface_.save_state_handler.load_fn) {
		bbcSaveState::LoadStatus status=(*disc_interface_.save_state_handler.load_fn)(save_state);
		if(status!=bbcSaveState::LS_OK) {
			return status;
		}
	}

	return bbcSaveState::LS_OK;
}

bbcSaveState::SaveStatus bbc1770::SaveSaveState(bbcSaveState *save_state) {
	bbc1770SaveState1 data;

	data.status=status_;
	data.track=track_;
	data.sector=sector_;
	data.data=data_;
	data.phys_track=phys_track_;
	data.phys_sector=phys_sector_;
	data.last_step=last_step_;

	data.ctrl_drive=drive_control_.drive;
	data.ctrl_side=drive_control_.side;
	data.ctrl_is_double_density=drive_control_.is_double_density;
	data.ctrl_disable_irq=drive_control_.disable_irq;

	data.cmd_update_mode=cmd_update_mode_;
	data.cmd_flags=cmd_flags_;
	data.cmd_track=cmd_track_;
	data.cmd_sector=cmd_sector_;
	data.cmd_num_sectors_left=cmd_num_sectors_left_;
	data.cmd_cur_offset=cmd_cur_offset_;
	data.cmd_next_byte_at=cmd_next_byte_at_;
	memcpy(data.cmd_address,cmd_address_,6);

	save_state->SetItem(bbc_1770_save_state_item_id,0,1,&data);

	if(disc_interface_.save_state_handler.save_fn) {
		bbcSaveState::SaveStatus status=(*disc_interface_.save_state_handler.save_fn)(save_state);
		if(status!=bbcSaveState::SS_OK) {
			return status;
		}
	}

	return bbcSaveState::SS_OK;
}
