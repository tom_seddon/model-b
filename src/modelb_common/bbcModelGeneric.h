#ifndef bbcMODELGENERIC_H_
#define bbcMODELGENERIC_H_

#include "bbcComputer.h"
#include "bbcModel.h"

//////////////////////////////////////////////////////////////////////////
// more templates. This creates a suitable derived class for a specific model, with the
// minimum of hassle. (though it does look a bit scary.)
template<class Sim>
class bbcModelGeneric:
public bbcModel
{
public:
	bbcModelGeneric(unsigned flags=0):
	bbcModel(flags)
	{
	}

	void Init() const {
		Sim::Init();

		// Reset ROMSEL
		for(const bbcMmioFnInit *mmio_fns=this->ExtraMmioFns();mmio_fns->count!=0;++mmio_fns) {
			for(unsigned i=0;i<mmio_fns->count;++i) {
				(*mmio_fns->write_fn)(t65::word(mmio_fns->start+i),0x00);
			}
		}
	}

	void Reset6502() const {
		Sim::Reset6502(the_machine_);
	}

	void Run() const {
		typedef t65TYPENAME Sim::MachineType Machine;
		typedef t65TYPENAME Sim::CPUType Cpu;

		//Set to true if we run any instructions with I enabled this time round.
		//Then, if an interrupt is required, and I is set, but irqs_ever_enabled
		//is true, we know to do the interrupt anyway since the last instruction
		//that ran with I clear was a SEI or something.
		//
		//TODO This might not be _quite_ right, though. If an SEI is run just at
		//the right moment, an IRQ could occur just after the SEI. But maybe this
		//happens on a real Beeb too?
		bool irqs_ever_enabled=false;

		if(Machine::cpustate.p&t65::I_MASK) {
			//Interrupts disabled. Run until next stop.
			do {
				BASSERT((Machine::cpustate.p&t65::I_MASK));
#ifdef bbcDEBUG_TRACE_EVENTS
				bbcModelCpuEventHandler::Data *instr=0;
				if(bbcComputer::trace_cpu_events) {
					instr=static_cast<bbcModelCpuEventHandler::Data *>(bbcComputer::event_record.AddEvent(
						&bbcModelCpuEventHandler::handler));
					instr->pc=bbcComputer::cpustate.pc;
					t65::Word addr=instr->pc;
					t65TYPENAME Sim::ByteReader brtmp;//damn BC++
					instr->bytes[0]=brtmp(addr);
					++addr.w;
					instr->bytes[1]=brtmp(addr);
					++addr.w;
					instr->bytes[2]=brtmp(addr);
				}
#endif
				Sim::RunSingleInstruction(the_machine_);
#ifdef bbcDEBUG_TRACE_EVENTS
				if(instr) {
					instr->a=Machine::cpustate.a;
					instr->p=Machine::cpustate.p;
					instr->x=Machine::cpustate.x;
					instr->y=Machine::cpustate.y;
					instr->s=Machine::cpustate.s.l;
				}
#endif
			} while(int(Machine::cpucycles-Machine::next_stop)<0);
		}

		//9/3/04 trying to fix empire strikes back (again) -- run another instruction
		//if IRQs just reenabled.
		if(!(Machine::cpustate.p&t65::I_MASK)) {
			irqs_ever_enabled=true;
			//interrupts enabled. Run until next stop or until interrupt happens. Will
			//stop straight away if irq_flags_ set.
			do {
#ifdef bbcDEBUG_TRACE_EVENTS
				bbcModelCpuEventHandler::Data *instr=0;
				if(bbcComputer::trace_cpu_events) {
					instr=static_cast<bbcModelCpuEventHandler::Data *>(bbcComputer::event_record.AddEvent(
						&bbcModelCpuEventHandler::handler));
					instr->pc=bbcComputer::cpustate.pc;
					t65::Word addr=instr->pc;
					t65TYPENAME Sim::ByteReader brtmp;//damn BC++
					instr->bytes[0]=brtmp(addr);
					++addr.w;
					instr->bytes[1]=brtmp(addr);
					++addr.w;
					instr->bytes[2]=brtmp(addr);
				}
#endif
				Sim::RunSingleInstruction(the_machine_);
				//BASSERT(!Machine::irq_flags_||int(Machine::cycles-Machine::next_stop)<0);
#ifdef bbcDEBUG_TRACE_EVENTS
				if(instr) {
					instr->a=Machine::cpustate.a;
					instr->p=Machine::cpustate.p;
					instr->x=Machine::cpustate.x;
					instr->y=Machine::cpustate.y;
					instr->s=Machine::cpustate.s.l;
				}
#endif
			} while(!Machine::irq_flags_&&int(Machine::cpucycles-Machine::next_stop)<0);
		}

		// 8/3/04 same for both types of main loop now.
		if(Machine::irq_flags_) {
#ifdef bbcDEBUG_TRACE_EVENTS
			if(bbcComputer::trace_cpu_events) {
				bbcComputer::event_record.AddEvent(&bbcModelIrqEventHandler::handler);
			}
#endif
			if(Machine::irq_flags_&IRQ_NMI) {
				Machine::irq_flags_&=~IRQ_NMI;
				Cpu::NMI(the_machine_);
			} else if(!(Machine::cpustate.p&t65::I_MASK)||irqs_ever_enabled) {
				Cpu::IRQ(the_machine_);
			}
		}
	}

	t65::Word Disassemble(t65::Word base,char *opcode_buf,char *operand_buf,t65::byte *bytes_buf) const {
		return Sim::Disassemble(the_machine_,base,opcode_buf,operand_buf,bytes_buf);
	}

	int DisassembleBytes(const t65::byte *bytes,t65::Word bytes_addr,char *opcode_buf,char *operand_buf) const {
		return Sim::DisassemblerType::Disassemble(bytes_addr,bytes,opcode_buf,
			operand_buf);
	}

	t65::byte ReadByte(t65::Word addr) const {
		t65TYPENAME Sim::ByteReader brtmp;
		return brtmp(addr);
	}

	void GetInstrInfo(t65::byte op,bbcInstrInfo *instr_info) const {
		instr_info->dii=&Sim::DisassemblerType::dis_table[op];
#ifdef t65_COUNT_INSTRUCTION_FREQUENCIES
		instr_info->execution_count=Sim::CPUType::instr_freqs[op];
#else
		instr_info->execution_count=0;
#endif
	}
private:
	static t65TYPENAME Sim::MachineType the_machine_;
};


template<class Sim>
t65TYPENAME Sim::MachineType bbcModelGeneric<Sim>::the_machine_;

#endif
