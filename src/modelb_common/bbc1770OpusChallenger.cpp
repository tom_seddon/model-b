//////////////////////////////////////////////////////////////////////////
// Opus Challenger 3in1
//
#include "pch.h"
#include "bbc1770OpusChallenger.h"
#include "bbc1770.h"

//////////////////////////////////////////////////////////////////////////
//
const bbcMmioFnInit mmio_fns[]={
#ifdef bbcDEBUG_ENABLE
	{0xfce0,0x18,&bbc1770OpusChallenger::ReadUnknownFred,&bbc1770OpusChallenger::WriteUnknownFred},
	{0xfcfd,1,&bbc1770OpusChallenger::ReadUnknownFred,&bbc1770OpusChallenger::WriteUnknownFred},
#endif
	{0xfcfe,1,&bbc1770OpusChallenger::ReadPagingMsb,&bbc1770OpusChallenger::WritePagingMsb,},
	{0xfcff,1,&bbc1770OpusChallenger::ReadPagingLsb,&bbc1770OpusChallenger::WritePagingLsb,},
	{0xfd00,256,&bbc1770OpusChallenger::ReadJim,&bbc1770OpusChallenger::WriteJim},
	{0,0,0,0,},
};

const bbc1770Interface bbc1770OpusChallenger::disc_interface_256k={
	"Opus CHALLENGER 256K",
	0xfcf8,
	0xfcfc,
	&bbc1770OpusChallenger::GetDriveControlFromByte,
	&bbc1770OpusChallenger::GetByteFromDriveControl,
	&bbc1770OpusChallenger::Init256k,
	bbc1770Interface::DISABLE_RESTORE_IRQ|bbc1770Interface::INVERT_TRACK0,//disable_restore_irq,
	mmio_fns,
	{
		bbc1770OpusChallenger::LoadSaveState,//save_state_handler.load_fn
		bbc1770OpusChallenger::SaveSaveState,//save_state_handler.save_fn
	},
};

const bbc1770Interface bbc1770OpusChallenger::disc_interface_512k={
	"Opus CHALLENGER 512K",
	0xfcf8,
	0xfcfc,
	&bbc1770OpusChallenger::GetDriveControlFromByte,
	&bbc1770OpusChallenger::GetByteFromDriveControl,
	&bbc1770OpusChallenger::Init512k,
	bbc1770Interface::DISABLE_RESTORE_IRQ|bbc1770Interface::INVERT_TRACK0,//disable_restore_irq,
	mmio_fns,
	{
		bbc1770OpusChallenger::LoadSaveState,//save_state_handler.load_fn
		bbc1770OpusChallenger::SaveSaveState,//save_state_handler.save_fn
	},
};

t65::Word bbc1770OpusChallenger::paging_;
t65::byte bbc1770OpusChallenger::ram_[524288];
unsigned bbc1770OpusChallenger::ram_size_;

void bbc1770OpusChallenger::Init256k() {
	ram_size_=256*1024;
	InitCommon();
}

void bbc1770OpusChallenger::Init512k() {
	ram_size_=512*1024;
	InitCommon();
}

void bbc1770OpusChallenger::InitCommon() {
	paging_.l=0;
	paging_.h=0;
	memset(ram_,0,ram_size_);
}

t65::byte bbc1770OpusChallenger::ReadJim(t65::word addr) {
	unsigned idx=(paging_.w<<8)|(addr&0xFF);
	if(idx<ram_size_) {
		return ram_[idx];
	} else {
		return 0xFF;
	}
}

void bbc1770OpusChallenger::WriteJim(t65::word addr,t65::byte val) {
	unsigned idx=(paging_.w<<8)|(addr&0xFF);
	if(idx<ram_size_) {
		ram_[idx]=val;
	}
}

t65::byte bbc1770OpusChallenger::ReadPagingLsb(t65::word) {
	return paging_.l;
}

void bbc1770OpusChallenger::WritePagingLsb(t65::word,t65::byte val) {
	paging_.l=val;
}

t65::byte bbc1770OpusChallenger::ReadPagingMsb(t65::word) {
	return paging_.h;
}

void bbc1770OpusChallenger::WritePagingMsb(t65::word,t65::byte val) {
	paging_.h=val;
}

t65::byte bbc1770OpusChallenger::ReadUnknownFred(t65::word addr) {
	printf("bbc1770OpusChallenger::ReadUnknownFred: addr=%04X\n",addr);
	return 0xFF;
}

void bbc1770OpusChallenger::WriteUnknownFred(t65::word addr,t65::byte val) {
	printf("bbc1770OpusChallenger::WriteUnknownFred: addr=%04X val=%02X\n",addr,val);
}

//////////////////////////////////////////////////////////////////////////
//
// Drive control format guessed as:
//
// Bit
//	7 0x80
//	6 0x40
//	5 0x20  Unknown (always set?)
//	4 0x10  Density select -- 0=double 1=single (guessed using *FORMAT)
//	3 0x08
//	2 0x04	Select drive 1 (external)
//	1 0x02	Select drive 0 (internal to Challenger)
//	0 0x01	Physical side -- 0=0, 1=1
void bbc1770OpusChallenger::GetByteFromDriveControl(const bbc1770DriveControl &src,
	t65::byte *dest)
{
	(void)src;

	//Seems to be write only on the Beeb...
	*dest=0xff;
}

void bbc1770OpusChallenger::GetDriveControlFromByte(t65::byte src,
	bbc1770DriveControl *dest)
{
	dest->drive=0;//TODO should be -ve for 'none selected'
	if(src&2) {
		dest->drive=0;
	} else if(src&4) {
		dest->drive=1;
	}
	dest->side=src&1;
	dest->is_double_density=!(src&0x20);
	dest->disable_irq=false;
}

//////////////////////////////////////////////////////////////////////////

static const bbcSaveStateItemId chal_save_state_item_id("opuschal");

#pragma pack(push,1)

struct bbc1770OpusChallengerSaveState1 {
	bbcSaveStateWord paging;
	bbcSaveStateByte ram[1];
};

#pragma pack(pop)

bbcSaveState::LoadStatus bbc1770OpusChallenger::LoadSaveState(const bbcSaveState *save_state) {
	const bbcSaveStateItem *item=save_state->GetItem(chal_save_state_item_id,ram_size_);
	if(!item) {
		return bbcSaveState::LS_MISSING;
	}

	switch(item->version) {
	default:
		return bbcSaveState::LS_BAD_DATA;

	case 1:
		if(item->data.size()!=sizeof(bbc1770OpusChallengerSaveState1)+ram_size_-1) {
			return bbcSaveState::LS_BAD_DATA;
		} else {
			const bbc1770OpusChallengerSaveState1 *data=reinterpret_cast<const bbc1770OpusChallengerSaveState1 *>(
				&item->data[0]);

			paging_.w=data->paging;
			memcpy(ram_,data->ram,ram_size_);
		}
		break;
	}

	return bbcSaveState::LS_OK;
}

bbcSaveState::SaveStatus bbc1770OpusChallenger::SaveSaveState(bbcSaveState *save_state) {
	void *raw_data=save_state->PrepareItem(chal_save_state_item_id,ram_size_,1,
		sizeof(bbc1770OpusChallengerSaveState1)+ram_size_-1);

	bbc1770OpusChallengerSaveState1 *data=static_cast<bbc1770OpusChallengerSaveState1 *>(raw_data);

	data->paging=paging_.w;
	memcpy(data->ram,ram_,ram_size_);

	return bbcSaveState::SS_OK;
}
