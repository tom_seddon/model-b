#ifndef BBCMODELBWATFORD_H_
#define BBCMODELBWATFORD_H_

#include "bbcSY6502A.h"
#include "bbcModelGeneric.h"
#include "bbcComputer.h"

struct bbcModelBWatfordConfig {
	typedef bbcComputer MachineType;

	//////////////////////////////////////////////////////////////////////////
	// Memory access -- which form of memory access should be used for each
	// access category?

	// ReadZP: how the processor reads bytes from zero page
	typedef MachineType::ReadNoHW ReadZP;

	// WriteZP: how the processor writes bytes to zero page
	typedef MachineType::WriteNoHW WriteZP;

	// ReadStack: how the processor reads bytes from the stack
	typedef MachineType::ReadNoHW ReadStack;

	// WriteStack: how the processor write bytes to the stack
	typedef MachineType::WriteNoHW WriteStack;

	// FetchInstr: how the processor reads bytes whilst fetching the bytes
	// that make up an instruction
	typedef MachineType::ReadExecute FetchInstr;

	// FetchAddress: how the processor reads an indirect address from
	// main memory, e.g. during IRQ processing or when doing the indirect
	// memory mode.
	typedef MachineType::ReadNoHW FetchAddress;

	// ReadOperand: how the processor reads bytes when reading an
	// instruction's operand
	typedef MachineType::ReadWithHW ReadOperand;

	// WriteOperand: how the processor writes bytes when writing an
	// instruction's operand
	typedef MachineType::WriteWithHWWatford WriteOperand;

	// ReadDebug: how to read a byte in debug mode (no update hardware
	// or signal interrupts etc.)
	typedef MachineType::ReadNoHW ReadDebug;

	// WriteDebug: how to write a byte in debug mode (no update hardware
	// or signal interrupts etc.)
	typedef MachineType::WriteNoHW WriteDebug;

	static const bbcReadMmioFn romsel_read_fn;
	static const bbcWriteMmioFn romsel_write_fn;
};

typedef bbcSY6502A<bbcModelBWatfordConfig> bbcModelBWatfordCpu;
typedef bbcSY6502AInstructionSet<bbcModelBWatfordCpu> bbcModelBWatfordInstructionSet;
typedef t65::Sim6502<bbcModelBWatfordCpu,bbcModelBWatfordInstructionSet> bbcModelBWatfordSim;

class bbcModelBWatford:
public bbcModelGeneric<bbcModelBWatfordSim>
{
public:
#ifdef bbcUPDATE_IN_BBCMODEL
	void Update() const;
#endif
	const bbcMmioFnInit *ExtraMmioFns() const;

	static const bbcModelBWatford model;
};

#endif
