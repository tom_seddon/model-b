#include "pch.h"
#include <ctype.h>
#include <string.h>

#ifdef bbcENABLE_PROFILER
#include "bbcProfiler.h"
#include <list>

//by having a global list, and having each record its parent rather than its children,
//static initialization order problems are avoided. of course, this makes it slower and
//a bit fiddler.

typedef std::vector<bbcProfileRecord *> PRL;

static PRL *all_profile_records=0;

static PRL &GlobalPRL() {
	static PRL global_prl;

	all_profile_records=&global_prl;
	return global_prl;
}

void bbcProfileRecord::GetChildrenByParent(bbcProfileRecord *parent,std::vector<bbcProfileRecord *> *prs) {
	PRL &global_prl=GlobalPRL();

	prs->clear();
	for(PRL::const_iterator it=global_prl.begin();it!=global_prl.end();++it) {
		if((*it)->parent==parent) {
			prs->push_back(*it);
		}
	}
}

bbcProfileRecord bbc_profile_records_root("root",&bbc_profile_records_root);

#ifdef _MSC_VER
#pragma warning(disable:4355)
#endif

bbcProfileRecord::bbcProfileRecord(const char *name_in,bbcProfileRecord *parentin):
count(0),
total(0),
name(name_in),
parent(parentin?(parentin==this?0:parentin):&bbc_profile_records_root),
first_child_(0),
next_sibling_(0)
{
	GlobalPRL().push_back(this);
	bbcPrintf("%s: this=%p, name=\"%s\", num=%u\n",__FUNCTION__,this,this->name,GlobalPRL().size());
}

#ifdef _MSC_VER
#pragma warning(default:4355)
#endif

void bbcProfileRecord::GetImmediateChildren(std::vector<bbcProfileRecord *> *prs) {
	GetChildrenByParent(this,prs);
}

static void InsertCommas(char *p,size_t p_size) {
	//skip nondigits (- etc.)
	size_t a=0;
	while(p[a]!=0&&!isdigit(p[a])) {
		++a;
	}
	if(p[a]==0) {
		return;
	}

	//find end of digits (cater for . etc.)
	size_t b=a;
	while(p[b]!=0&&isdigit(p[b])) {
		++b;
	}

	//determine # commas, stop if 0 or if there's not enough space to add them.
	int n=b-a;
	int ncommas=(n-1)/3;
	if(ncommas==0) {
		return;
	}

	//find length of string
	size_t len=b;
	while(p[len]!=0) {
		++len;
	}

	//skip digits preceding first comma
	int nskip=n%3;
	if(nskip==0) {
		nskip=3;
	}
	a+=nskip;

	//a bit lame... ah well.
	while(a<b&&len<p_size) {
		memmove(&p[a+1],&p[a],(len+1)-a);//+1 due to '\x0'
		
		p[a]=',';
		
		++len;
		++b;
		a+=4;
	}
	BASSERT(len==p_size||a==b);
}

void bbcProfileRecord::RecursivePrint(bbcProfilerPrintFn pfn,int indent,void *context) {
	char total_str[40];
	_snprintf(total_str,sizeof total_str,"%" PORT_INT64_FMT "u",this->total);
	InsertCommas(total_str,sizeof total_str);

	char count_str[40];
	_snprintf(count_str,sizeof count_str,"%u",this->count);
	InsertCommas(count_str,sizeof count_str);

	double avg=this->total==0?0.0:this->total/double(this->count);
	char avg_str[40];
	_snprintf(avg_str,sizeof avg_str,"%.1f",avg);
	InsertCommas(avg_str,sizeof avg_str);

	char tmp[200];
	_snprintf(tmp,sizeof tmp,"%-*s%-*s %20s total %20sx %20s avg\n",
		indent,"",25-indent,this->name,total_str,count_str,avg_str);
	(*pfn)(context,tmp);

//	_snprintf(tmp,sizeof tmp,"(%I64u %.3f)\n",this->total,avg);
//	(*pfn)(context,tmp);

	std::vector<bbcProfileRecord *> children;
	this->GetImmediateChildren(&children);
	for(unsigned i=0;i<children.size();++i) {
		children[i]->RecursivePrint(pfn,indent+2,context);
	}
}

static void bbcPrintfProfilerPrint(void *,const char *str) {
	bbcPrintf("%s",str);
}

void bbcProfilerPrintRecords(bbcProfilerPrintFn pfn,void *context) {
	std::vector<bbcProfileRecord *> roots;

	if(!pfn) {
		pfn=&bbcPrintfProfilerPrint;
	}

	bbcProfileRecord::GetChildrenByParent(0,&roots);
	for(unsigned i=0;i<roots.size();++i) {
		roots[i]->RecursivePrint(pfn,0,context);
	}
}

void bbcProfilerSetupTree() {
	for(PRL::iterator it=GlobalPRL().begin();it!=GlobalPRL().end();++it) {
		if((*it)->parent) {
			bbcProfileRecord **nextptr=&(*it)->parent->first_child_;
			while(*nextptr) {
				nextptr=&(*nextptr)->next_sibling_;
			}
			*nextptr=*it;
		}
	}

	for(PRL::iterator it=GlobalPRL().begin();it!=GlobalPRL().end();++it) {
		if((*it)->parent) {
			BASSERT((*it)->parent->first_child_);

			bbcProfileRecord *pr;
			for(pr=(*it)->parent->first_child_;pr;pr=pr->next_sibling_) {
				if(pr==*it) {
					break;
				}
			}
			BASSERT(pr);
		}
	}
}

void bbcProfilerGetAllProfileRecords(bbcProfileRecord ***records,int *num_records) {
	*records=&GlobalPRL()[0];
	*num_records=GlobalPRL().size();
}

#endif//bbcENABLE_PROFILER

