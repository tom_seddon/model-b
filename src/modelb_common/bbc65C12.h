#ifndef bbc65C12_H_
#define bbc65C12_H_

// TODO different memory accesses on ABX and ABY. (see dabs master guide p35)
// TODO bugger! was it actually a 65C02?! :(

//Master 128 65C12
//
//Same as t65::Generic6502, but:
//
//1.	BRA/CLR/DEA/INA/PHX/PHY/PLX/PLY/TRB/TSB
//2.	Illegal opcodes are NOP
//
//and (64doc):
//
/*
Different CPU types

  The Rockwell data booklet 29651N52 (technical information about R65C00 
  microprocessors, dated October 1984), lists the following differences between
  NMOS R6502 microprocessor and CMOS R65C00 family:
  
1. Indexed addressing across page boundary. 
NMOS: Extra read of invalid address.
CMOS: Extra read of last instruction byte. TODO doubt this matters much... right?

2. Execution of invalid op codes.
NMOS: Some terminate only by reset. Results are undefined.
CMOS: All are NOPs (reserved for future use). DONE

3. Jump indirect, operand = XXFF.
NMOS: Page address does not increment.
CMOS: Page address increments and adds one additional cycle. DONE TODO extra read?

4. Read/modify/write instructions at effective address.
NMOS: One read and two write cycles.
CMOS: Two read and one write cycle. TODO sort this if it becomes necessary!

5. Decimal flag.
NMOS: Indeterminate after reset.
CMOS: Initialized to binary mode (D=0) after reset and interrupts. TODO what, after IRQ as well?!?!?!

6. Flags after decimal operation.
NMOS: Invalid N, V and Z flags.
CMOS: Valid flag adds one additional cycle. TODO do this.

7. Interrupt after fetch of BRK instruction.
NMOS: Interrupt vector is loaded, BRK vector is ignored.
CMOS: BRK is executed, then interrupt is executed. TODO can ignore this I think.
				
*/
//#include <t65Generic6502.h>
#include <t65Generic6502InstructionSet.h>
#include <t65Generic6502.h>
#include <t65Sim6502.h>
#include <t65WarningsPush.h>

//TODO this copied from bbcSY6502A.H, to be transferred into tom6502 one day...
#ifdef START_INSTR
//hack for VC7 -- to be investigated.
#undef START_INSTR
#endif
#ifdef END_INSTR
//hack for VC7 -- to be investigated.
#undef END_INSTR
#endif
#define START_INSTR(N) struct Instr##N {static const char *Mnemonic() {return #N "+";}
#define END_INSTR() };

template<class T>
struct bbc65C12:
public t65::Generic6502<T>
{
public:
	typedef t65::Generic6502<T> B;
	typedef t65TYPENAME t65::Generic6502<T>::MachineType MachineType;

	//////////////////////////////////////////////////////////////////////////
	//Illegal opcodes are NOP.
	struct InstrILL65C12:
	public t65::Generic6502<T>::InstrNOP
	{
		static const char *Mnemonic() {
			return "NOP*";
		}
	};
	typedef InstrILL65C12 InstrILL;//I'm not sure that I like this.

	//////////////////////////////////////////////////////////////////////////
	//New instructions
	//BRA -- branch unconditional
	struct InstrBRA {
		static const char *Mnemonic() {
			return "BRA+";
		}
		//This is a copy and paste from InstrBranch
		//icl choked on inheriting from either InstrBranchIfClear<0> or
		//InstrBranch<0,0>!
		static FINLINE void t65_CALL ExecuteRelative(MachineType &state) {
			t65::Word lsbresult;
			
			// 2nd cycle -- fetch operand, increment PC
			t65::int8 operand=t65::int8(state.ReadByte(state.cpustate.pc,B::catFetchInstr));
			++state.cpustate.pc.w;
			++state.cpucycles;
			
			// 3rd cycle -- read PC, add operand to PCL
			state.ReadByte(state.cpustate.pc,B::catFetchInstr);
			lsbresult.w=state.cpustate.pc.w+operand;
			state.cpustate.pc.l=lsbresult.l;
			++state.cpucycles;
			
			// If PC needs a fixup, read from (incorrect) PC whilst fixing
			// it up. If PC doesn't need fixing, we drop out here and let
			// the main loop do the PC read.
			if(lsbresult.h!=state.cpustate.pc.h) {
				// 4th cycle -- read PC, fix PCH, ++PC.
				state.ReadByte(state.cpustate.pc,B::catFetchInstr);
				state.cpustate.pc.h=lsbresult.h;
				++state.cpucycles;
				//++state.cpustate.pc.w;
			}
		}
	};

	//TRB -- RMW, M=(A^0xFF)&M, Z according to (A&M)
	//TODO (TSB is same) MasterAdvRef says "Z if (A&M)==0" -- is it only ever
	//set? 
	START_INSTR(TRB)
		static FINLINE t65::byte Execute(MachineType &state,t65::byte val) {
//			state.cpustate.p&=~(t65::Z_MASK|t65::N_MASK|t65::V_MASK);
//			state.cpustate.p|=((state.cpustate.a&val)==0)<<1;//NB true/false used
//			state.cpustate.p|=val&0xC0;

			// dabs master guide says NV changed on Rockwell only...?

			state.cpustate.p&=~t65::Z_MASK;
			state.cpustate.p|=((state.cpustate.a&val)==0)<<1;//NB true/false used
#ifdef bbcENABLE_BEEB_ICE
			state.ice.OnChangeP();
#endif
			return state.cpustate.a&~val;
		}
	END_INSTR()

	//TSB -- RMW, M=(A|M), Z according to (A&M)
	START_INSTR(TSB)
		static FINLINE t65::byte Execute(MachineType &state,t65::byte val) {
			(void)state;

//			state.cpustate.p&=~(t65::Z_MASK|t65::N_MASK|t65::V_MASK);
//			state.cpustate.p|=((state.cpustate.a&val)==0)<<1;//NB true/false used
//			state.cpustate.p|=val&0xC0;

			// see TRB

			state.cpustate.p&=~t65::Z_MASK;
			state.cpustate.p|=((state.cpustate.a&val)==0)<<1;//NB true/false used
#ifdef bbcENABLE_BEEB_ICE
			state.ice.OnChangeP();
#endif

			return state.cpustate.a|val;
		}
	END_INSTR()

	//CLR/STZ -- store 0 to memory
	START_INSTR(STZ)
		static FINLINE t65::byte Execute(MachineType &state) {
			return 0;
		}
	END_INSTR()

	//PLX/PLY/PHX/PHY -- push and pop X and Y.
	START_INSTR(PLX)
		static FINLINE void t65_CALL Execute(MachineType &state) {
			state.cpustate.x=PopValueInstr(state);
#ifdef bbcENABLE_BEEB_ICE
			state.ice.OnChangeX();
#endif
		}
	END_INSTR()

	START_INSTR(PLY)
		static FINLINE void t65_CALL Execute(MachineType &state) {
			state.cpustate.y=PopValueInstr(state);
#ifdef bbcENABLE_BEEB_ICE
			state.ice.OnChangeY();
#endif
		}
	END_INSTR()

	START_INSTR(PHX)
		static FINLINE void t65_CALL Execute(MachineType &state) {
			PushRegContentsInstr(state,state.cpustate.x);
		}
	END_INSTR()

	START_INSTR(PHY)
		static FINLINE void t65_CALL Execute(MachineType &state) {
			PushRegContentsInstr(state,state.cpustate.y);
		}
	END_INSTR()

	//////////////////////////////////////////////////////////////////////////
	// New or otherwise altered addressing modes

	//Indirect absolute indexed (changed from zero page 24/3/04)
	//
	struct ModeIAX:
	public t65::Generic6502<T>::ModeABS
	{
		static char *Disassemble(char *buf,const t65::byte *bytes,t65::Word v) {
			return AddIndex(ModeIND::Disassemble(buf,bytes,v),'X');
		}

		static const char *AddrModeName() {
			return "IAX";
		}

		// # cycles is 6, guessing:
		// (read instr)
		// read ptrl, add X
		// read ptrh, do carry
		// read eal 
		// read eah
		// ???
		static FINLINE t65::Word ReadModeEA(MachineType &state) {
			t65::Word addr,ea;
			
			addr.l=state.ReadByte(state.cpustate.pc,B::catFetchInstr);
			++state.cpustate.pc.w;
			++state.cpucycles;
			addr.h=state.ReadByte(state.cpustate.pc,B::catFetchInstr);
			++state.cpustate.pc.w;
			++state.cpucycles;
			
			addr.w+=state.cpustate.x;

			ea.l=state.ReadByte(addr,B::catFetchAddress);
			++addr.w;//hmm, assume page wraparound
			++state.cpucycles;

			ea.h=state.ReadByte(addr,B::catFetchAddress);
			++state.cpucycles;

			return ea;
		}
	};

	struct ModeINZ {
		enum {operand_size=1,};
		static char *Disassemble(char *buf,const t65::byte *bytes,t65::Word) {
			*buf++='(';
			buf=B::AddHexByte(buf,bytes[0]);
			*buf++=')';
			*buf=0;
			return buf;
		}

		static const char *AddrModeName() {
			return "INZ";
		}

		static FINLINE t65::Word ReadModeEA(MachineType &state) {
			t65::Word ptr,addr;
			
			ptr.w=state.ReadByte(state.cpustate.pc,B::catFetchInstr);
			++state.cpustate.pc.w;
			++state.cpucycles;
			state.ReadByte(ptr,B::catReadZP);
			//ptr.l+=state.cpustate.x;
			++state.cpucycles;
			addr.l=state.ReadByte(ptr,B::catReadZP);
			++state.cpucycles;
			++ptr.l;
			addr.h=state.ReadByte(ptr,B::catReadZP);
			++state.cpucycles;
			return addr;
		}
		
		static FINLINE t65::Word WriteModeEA(MachineType &state) {
			t65::Word ptr,addr;
			
			ptr.w=state.ReadByte(state.cpustate.pc,B::catFetchInstr);
			++state.cpustate.pc.w;
			++state.cpucycles;
			state.ReadByte(ptr,B::catReadZP);
			//ptr.l+=state.cpustate.x;
			++state.cpucycles;
			addr.l=state.ReadByte(ptr,B::catReadZP);
			++ptr.l;
			++state.cpucycles;
			addr.h=state.ReadByte(ptr,B::catReadZP);
			++state.cpucycles;
			return addr;
		}
		
		static FINLINE t65::byte ReadOperand(MachineType &state,t65::Word addr) {
			t65::byte b=state.ReadByte(addr,B::catReadOperand);
			++state.cpucycles;
			return b;
		}
		
		static FINLINE void t65_CALL WriteOperand(MachineType &state,t65::Word addr,t65::byte val) {
			state.WriteByte(addr,val,B::catWriteOperand);
			++state.cpucycles;
		}
	};

	//Indirect -- handles page boundary crossings correctly
	//Unclear where the read during the fixup cycle comes from.
	//TODO so there isn't one, but the cycle timing is correct.
	struct ModeIND {
		enum {operand_size=2,};

		static char *Disassemble(char *buf,const t65::byte *bytes,t65::Word) {
			*buf++='(';
			buf=B::AddHexWord(buf,t65::MakeWord(bytes[0],bytes[1]));
			*buf++=')';
			*buf=0;
			return buf;
		}

		static const char *AddrModeName() {
			return "IND";
		}

		static FINLINE t65::Word ReadModeEA(MachineType &state) {
			t65::Word ptr,addr;
			
			ptr.l=state.ReadByte(state.cpustate.pc,B::catFetchInstr);
			++state.cpustate.pc.w;//2
			++state.cpucycles;
			ptr.h=state.ReadByte(state.cpustate.pc,B::catFetchInstr);
			++state.cpustate.pc.w;//3
			++state.cpucycles;
			addr.l=state.ReadByte(ptr,B::catFetchAddress);
			++ptr.l;
			++state.cpucycles;//4
			if(ptr.l==0) {
				//carry.
				++ptr.h;
				++state.cpucycles;//5
			}
			addr.h=state.ReadByte(ptr,B::catFetchAddress);
			++state.cpucycles;//5 or 6
			return addr;
		}
	};
};

//TODO s/P::/t65TYPENAME P::/
template<class T>
struct bbc65C12InstructionSet:
public t65::Generic6502InstructionSet<T>
{
	typedef t65TYPENAME t65::Generic6502InstructionSet<T>::P P;

	//BRA
	typedef t65TYPENAME P::template Relative<t65TYPENAME P::InstrBRA,t65TYPENAME P::ModeREL> Opcode80;

	//STZ
	typedef t65TYPENAME P::template Write<t65TYPENAME P::InstrSTZ,t65TYPENAME P::ModeZPG> Opcode64;
	typedef t65TYPENAME P::template Write<t65TYPENAME P::InstrSTZ,t65TYPENAME P::ModeZPX> Opcode74;
	typedef t65TYPENAME P::template Write<t65TYPENAME P::InstrSTZ,t65TYPENAME P::ModeABS> Opcode9C;
	typedef t65TYPENAME P::template Write<t65TYPENAME P::InstrSTZ,t65TYPENAME P::ModeABX> Opcode9E;
	
	//DEA/INA
	typedef t65TYPENAME P::template RMW<t65TYPENAME P::InstrDEC,t65TYPENAME P::ModeACC> Opcode3A;
	typedef t65TYPENAME P::template RMW<t65TYPENAME P::InstrINC,t65TYPENAME P::ModeACC> Opcode1A;

	//PHX etc.
	typedef t65TYPENAME P::template Implied<t65TYPENAME P::InstrPHX,t65TYPENAME P::ModeIMP> OpcodeDA;
	typedef t65TYPENAME P::template Implied<t65TYPENAME P::InstrPHY,t65TYPENAME P::ModeIMP> Opcode5A;
	typedef t65TYPENAME P::template Implied<t65TYPENAME P::InstrPLX,t65TYPENAME P::ModeIMP> OpcodeFA;
	typedef t65TYPENAME P::template Implied<t65TYPENAME P::InstrPLY,t65TYPENAME P::ModeIMP> Opcode7A;

	//TRB and TSB
	typedef t65TYPENAME P::template RMW<t65TYPENAME P::InstrTRB,t65TYPENAME P::ModeZPG> Opcode14;
	typedef t65TYPENAME P::template RMW<t65TYPENAME P::InstrTRB,t65TYPENAME P::ModeABS> Opcode1C;
	typedef t65TYPENAME P::template RMW<t65TYPENAME P::InstrTSB,t65TYPENAME P::ModeZPG> Opcode04;
	typedef t65TYPENAME P::template RMW<t65TYPENAME P::InstrTSB,t65TYPENAME P::ModeABS> Opcode0C;

	//New zero page indirect instructions.
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrADC,t65TYPENAME P::ModeINZ> Opcode72;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrAND,t65TYPENAME P::ModeINZ> Opcode32;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrCMP,t65TYPENAME P::ModeINZ> OpcodeD2;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrEOR,t65TYPENAME P::ModeINZ> Opcode52;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrLDA,t65TYPENAME P::ModeINZ> OpcodeB2;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrORA,t65TYPENAME P::ModeINZ> Opcode12;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrSBC,t65TYPENAME P::ModeINZ> OpcodeF2;
	typedef t65TYPENAME P::template Write<t65TYPENAME P::InstrSTA,t65TYPENAME P::ModeINZ> Opcode92;

	//JMP-IAX
	typedef t65TYPENAME P::template Special<t65TYPENAME P::InstrJMP,t65TYPENAME P::ModeIAX> Opcode7C;
	
	//New BIT forms
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrBIT,t65TYPENAME P::ModeZPX> Opcode34;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrBIT,t65TYPENAME P::ModeABX> Opcode3C;
	typedef t65TYPENAME P::template Read<t65TYPENAME P::InstrBIT,t65TYPENAME P::ModeIMM> Opcode89;
};

#include <t65WarningsPop.h>

#endif
