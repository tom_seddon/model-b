#include <base/base.h>

#include <stdlib.h>
#include <stdio.h>

#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#endif//_WIN32

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#ifdef _WIN32

int B_IsDebuggerPresent(void)
{
	return IsDebuggerPresent();
}

#else

// http://developer.apple.com/library/mac/#qa/qa1361/_index.html
int B_IsDebuggerPresent(void)
{
	struct kinfo_proc info;
	info.kp_proc.p_flag=0;

	int mib[4]={
		CTL_KERN,
		KERN_PROC,
		KERN_PROC_PID,
		getpid(),
	};

	size_t size=sizeof(info);
	if(sysctl(mib,sizeof mib/sizeof mib[0],&info,&size,NULL,0)!=0)
		return 0;

	if(!(info.kp_proc.p_flag&P_TRACED))
		return 0;

	return 1;
}

#endif

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#ifdef _WIN32

void B_OutputDebugString(const char *str)
{
	OutputDebugStringA(str);
}

#endif

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void B_OutputDebugStringv(const char *fmt,va_list v)
{
	char tmp[8192];//assumed to be large enough for the majority of cases
	char *buf=0;
	va_list v2;

	B_va_copy(v2,v);
	int n=B_vsnprintf(tmp,sizeof tmp,fmt,v);
	va_end(v2);

	if(n<sizeof tmp-1)
		B_OutputDebugString(tmp);
	else
	{
		B_va_copy(v2,v);
		// assume overflow
		n=B_vasprintf(&buf,fmt,v);
		va_end(v2);

		B_OutputDebugString(buf);

		free(buf);
		buf=0;
	}
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void B_OutputDebugStringf(const char *fmt,...)
{
	va_list v;
	va_start(v,fmt);
	B_OutputDebugStringv(fmt,v);
	va_end(v);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

int B_snprintf(char *buf,size_t buf_size,const char *fmt,...)
{
	va_list v;
	int r;

	va_start(v,fmt);
	r=B_vsnprintf(buf,buf_size,fmt,v);
	va_end(v);

	return r;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER

int B_vsnprintf(char *buf,size_t buf_size,const char *fmt,va_list v)
{
	int n;

	n=_vsnprintf(buf,buf_size,fmt,v);

	if(n<0)
	{
		// overflow...
		buf[buf_size-1]=0;

		n=buf_size-1;
	}

	return n;
}

#else//_MSC_VER

int B_vsnprintf(char *buf,size_t buf_size,const char *fmt,va_list v)
{
	int n=vsnprintf(buf,buf_size,fmt,v);

	if(n>0)
	{
		if(buf_size>0)
		{
			if((size_t)(n-1)>=buf_size)
			{
				// Return value suggests buffer is too small; return
				// number of chars actually written.
				if(buf_size>INT_MAX)
					n=INT_MAX;//AARGH WTF
				else
					n=(int)(buf_size-1);
			}
		}
	}

	return n;
}

#endif//_MSC_VER

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
int B_asprintf(char **buf,const char *fmt,...)
{
	va_list v;
	int r;

	va_start(v,fmt);
	r=B_vasprintf(buf,fmt,v);
	va_end(v);

	return r;
}
#endif//_MSC_VER

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
int B_vasprintf(char **buf,const char *fmt,va_list v)
{
	size_t buf_size=16384;
	int r;

	for(;;)
	{
		va_list v2=v;//this is OK for VC++.

		*buf=(char *)malloc(buf_size);
		if(!*buf)
		{
			*buf=NULL;
			return -1;
		}

		r=_vsnprintf(*buf,buf_size,fmt,v2);
		if(r>=0)
			break;

		// grow buffer and try again.
		free(*buf);
		*buf=NULL;

		buf_size+=buf_size/2;
	}

	*buf=(char *)realloc(*buf,r+1);
	return r;
}
#endif//_MSC_VER

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
// http://www.opensource.apple.com/source/mail_cmds/mail_cmds-24/mail/strlcpy.c
size_t B_strlcpy(char *dest,const char *src,size_t size)
{
	char *d=dest;
	const char *s=src;
	size_t n=size;

	/* Copy as many bytes as will fit */
	if (n != 0 && --n != 0) {
		do {
			if ((*d++ = *s++) == 0)
				break;
		} while (--n != 0);
	}

	/* Not enough room in dst, add NUL and traverse rest of src */
	if (n == 0) {
		if (size != 0)
			*d = '\0';		/* NUL-terminate dst */
		while (*s++)
			;
	}

	return(s - src - 1);	/* count does not include NUL */
}
#endif//_MSC_VER

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
// http://www.opensource.apple.com/source/apache_mod_php/apache_mod_php-4.3/php/main/strlcat.c
size_t B_strlcat(char *dest,const char *src,size_t size)
{
	char *d = dest;
	register const char *s = src;
	register size_t n = size;
	size_t dlen;

	/* Find the end of dst and adjust bytes left but don't go past end */
	while (*d != '\0' && n-- != 0)
		d++;
	dlen = d - dest;
	n = size - dlen;

	if (n == 0)
		return(dlen + strlen(s));
	while (*s != '\0') {
		if (n != 1) {
			*d++ = *s;
			n--;
		}
		s++;
	}
	*d = '\0';

	return(dlen + (s - src));	/* count does not include NUL */
}
#endif//_MSC_VER

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
// http://www.opensource.apple.com/source/xnu/xnu-1456.1.26/bsd/libkern/strsep.c
char *B_strsep(char **stringp, const char *delim)
{
	char *s;
	const char *spanp;
	int c, sc;
	char *tok;

	if ((s = *stringp) == NULL)
		return (NULL);
	for (tok = s;;) {
		c = *s++;
		spanp = delim;
		do {
			if ((sc = *spanp++) == c) {
				if (c == 0)
					s = NULL;
				else
					s[-1] = 0;
				*stringp = s;
				return (tok);
			}
		} while (sc != 0);
	}
	/* NOTREACHED */
}
#endif//_MSC_VER

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

char *B_strdup(const char *str)
{
	if(!str)
		return 0;

#ifdef _MSC_VER
	char *dup=_strdup(str);
#else
	char *dup=strdup(str);
#endif

	return dup;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
