#ifndef HEADER_278D9DC3D5F64FE9B13152ED615086F6
#define HEADER_278D9DC3D5F64FE9B13152ED615086F6

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

class B_LogHandler
{
public:
	B_LogHandler();
	virtual ~B_LogHandler();

	// print string, verbatim.
	virtual void Print(const char *str)=0;

	// called by log, periodically, for the benefit of handlers that
	// buffer things up.
	//
	// default impl does nothing.
	virtual void Flush();
protected:
private:
	B_LogHandler(const B_LogHandler &);
	B_LogHandler &operator=(const B_LogHandler &);
};

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

class B_LogToStdoutHandler:
	public B_LogHandler
{
public:
	static B_LogToStdoutHandler handler;

	void Print(const char *str);
protected:
private:
};

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

class B_LogToDebuggerHandler:
	public B_LogHandler
{
public:
	static B_LogToDebuggerHandler handler;

	void Print(const char *str);
protected:
private:
};

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

class B_LogToHandlersHandler:
	public B_LogHandler
{
public:
	B_LogToHandlersHandler();
	B_LogToHandlersHandler(B_LogHandler *handler0,...);//terminate list with NULL.

	void Print(const char *str);

	void AddHandler(B_LogHandler *handler);
	void RemoveHandler(B_LogHandler *handler);
protected:
private:
	static const size_t MAX_NUM_HANDLERS=10;
	B_LogHandler *m_handlers[MAX_NUM_HANDLERS];
	size_t m_num_handlers;

	void Construct();
};

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

class B_LogToBufferHandler:
	public B_LogHandler
{
public:
	char *buffer;
	size_t buffer_size;

	// out of range indexes aren't written, but index is still updated.
	size_t index;

	B_LogToBufferHandler();

	void Print(const char *str);
protected:
private:
	void Construct(char *buffer,size_t buffer_size);
};

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

class B_Log
{
public:
	B_Log();
	explicit B_Log(B_LogHandler *handler);
	explicit B_Log(const char *prefix,bool add_to_global_list=false);
	B_Log(B_LogHandler *handler,const char *prefix);
	virtual ~B_Log();

	static B_Log *GetFirstLog();
	B_Log *GetNextLog();

	B_LogHandler *GetHandler() const;
	void SetHandler(B_LogHandler *handler);

	const char *GetName() const;

	const char *GetLinePrefix() const;
	void SetLinePrefix(const char *prefix);

	void Indent(int n=4);
	void Unindent();

	// A leading '\t' causes the prefix to be replaced by spaces on that line.
	int pf(const char *fmt,...) B_PRINTF_LIKE(2,3);
	int pv(const char *fmt,va_list v);
	void p(const char *str);

	// prints out '\n', if the last char printed wasn't '\n' itself.
	void EnsureNewLine();

	//    void PrintHeader(const char *fmt,...) B_PRINTF_LIKE(2,3);

	// These use a ref-counting style system.
	//
	// The (signed) enable count is initially 1.
	//
	// If it is >0, the dump is enabled; else, disabled.
	void Enable();
	void Disable();
	bool IsEnabled() const;
protected:
private:
	int m_indent;
	bool m_last_was_newline;
	char *m_prefix;
	int m_prefix_len;
	int m_enable_count;
	B_LogHandler *m_handler;
	static const size_t MAX_INDENT_STACK_DEPTH=10;
	int m_indent_stack[MAX_INDENT_STACK_DEPTH];
	size_t m_indent_stack_depth;

	bool m_in_global_list;
	B_Log *m_next;

	static const size_t BUF_SIZE=100;
	char m_buf[BUF_SIZE];
	size_t m_buf_index;

	void PrintChar(char c);
	void Construct(bool add_to_global_list);

	B_Log(const B_Log &);
	B_Log &operator=(const B_Log &);
};

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

class B_LogIndenter
{
public:
	explicit B_LogIndenter(B_Log *log,int n=4);
	B_LogIndenter(B_Log *log,const char *fmt,...) B_PRINTF_LIKE(3,4);
	~B_LogIndenter();
protected:
private:
	B_Log *m_log;

	void Construct(B_Log *log,int n);

	B_LogIndenter(const B_LogIndenter &);
	B_LogIndenter &operator=(const B_LogIndenter &);
};

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// call Disable() on all logs
void B_DisableAllLogs();

// the default log handler will be used for any log whose handler is
// NULL.
void B_SetDefaultLogHandler(B_LogHandler *handler);

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#define B_LOG_PTR(X) (&(g_log_##X))
#define B_LOG_DEF(X) B_Log g_log_##X(#X,true)
#define B_LOG_DECL(X) extern B_Log g_log_##X

#define B_LOG_ENABLE(X,FLAG)  \
	B_BEGIN_MACRO                 \
{                             \
	extern B_Log g_log_##X;   \
	\
	B_COND_CONSTANT(if(FLAG)) \
{                         \
	g_log_##X.Enable();   \
}                         \
	else                      \
{                         \
	g_log_##X.Disable();  \
}                         \
}                             \
	B_END_MACRO

#define B_LOG(X,...)               \
	B_BEGIN_MACRO                      \
{                                  \
	extern B_Log g_log_##X;        \
	\
	if(g_log_##X.IsEnabled())      \
	g_log_##X.pf(__VA_ARGS__); \
}                                  \
	B_END_MACRO

#define B_LOG_PUTS(X,STR)     \
	B_BEGIN_MACRO                 \
{                             \
	extern B_Log g_log_##X;   \
	\
	if(g_log_##X.IsEnabled()) \
	g_log_##X.p(STR);     \
}                             \
	B_END_MACRO

#define B_LOGV(X,FMT,V)          \
	B_BEGIN_MACRO                    \
{                                \
	extern B_Log g_log_##X;      \
	\
	if(g_log_##X.IsEnabled())    \
	g_log_##X.pv((FMT),(V)); \
}                                \
	B_END_MACRO

#define B_LOGI(X,...) B_LOGI2(__LINE__,X,__VA_ARGS__)
#define B_LOGI2(F,X,...) B_LOGI3(F,X,__VA_ARGS__)
#define B_LOGI3(F,X,...) B_LogIndenter indent##F(&g_log_##X,__VA_ARGS__)

#define B_LOGI_N(X,N) B_LOGI2(__LINE__,X,(N))

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#endif//HEADER_278D9DC3D5F64FE9B13152ED615086F6
