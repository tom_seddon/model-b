#ifndef HEADER_1DC3B574BB3B4FCFB04C9FEBCD11A3E3
#define HEADER_1DC3B574BB3B4FCFB04C9FEBCD11A3E3

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER

#define _CRT_SECURE_NO_WARNINGS
#define _CRT_NONSTDC_NO_DEPRECATE

// stdint.h replacement
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned uint32_t;
typedef unsigned __int64 uint64_t;

typedef char int8_t;
typedef short int16_t;
typedef int int32_t;
typedef __int64 int64_t;

/* 7.18.2.1 Limits of exact-width integer types */
#define INT8_MAX (127)
#define INT16_MAX (32767)
#define INT32_MAX (2147483647)
#define INT64_MAX (9223372036854775807LL)

#define UINT8_MAX (255)
#define UINT16_MAX (65535)
#define UINT32_MAX (4294967295U)
#define UINT64_MAX (18446744073709551615ULL)

#endif//_MSC_VER

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#include <stdarg.h>

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// System define.

#ifdef _WIN32

#define B_WINDOWS 1

#else

#error Unknown system.

#endif

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// CPU define.

#if __GNUC__

// gcc possibilities.

#ifdef __i386

#define B_X86 1

#elif (defined __x86_64)

#define B_X64 1

#elif (defined __arm__)

#define B_ARM 1

#else

#error Unknown gcc CPU

#endif

#elif (defined _MSC_VER)

// VC++ possibilities.

#ifdef _M_IX86

#define B_X86 1

#elif (defined _M_X64)

#define B_X64 1

#else

#error Unknown VC++ CPU

#endif

#else

// Unknown compiler.
#error Unknown compiler.

#endif

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// B_COND_CONSTANT - wrap statement that might include a constant conditional,
// to avoid the VC++ warning.

#ifdef _MSC_VER

#define B_COND_CONSTANT(X) __pragma(warning(push)) __pragma(warning(disable:4127)) X __pragma(warning(pop))

#else

#define B_COND_CONSTANT(X) X

#endif

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// Make statementlike macros without warnings.

#define B_BEGIN_MACRO do
#define B_END_MACRO while((0,0))

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// B_PRINTF_LIKE() - mark up functions as being like printf.

#ifdef _MSC_VER

#define B_PRINTF_LIKE(A,B)

#else

#define B_PRINTF_LIKE(A,B) __attribute__((format(printf,(A),(B))))

#ifdef __OBJC__

// There's probably somewhere better for this.
#define B_NSLOG_LIKE(A,B) __attribute__((format(__NSString__,(A),(B))))

#endif//__OBJC__

#endif

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

int B_IsDebuggerPresent(void);

void B_OutputDebugString(const char *str);
void B_OutputDebugStringv(const char *fmt,va_list v);
void B_OutputDebugStringf(const char *fmt,...);

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// strstuff.

#ifdef _MSC_VER

#define B_strcasecmp (_stricmp)
#define B_strncasecmp (_strnicmp)

size_t B_strlcpy(char *dest,const char *src,size_t size);
size_t B_strlcat(char *dest,const char *src,size_t size);

char *B_strsep(char **stringp,const char *delim);

#else

#define B_strcasecmp (strcasecmp)
#define B_strncasecmp (strncasecmp)
#define B_strlcpy (strlcpy)
#define B_strlcat (strlcat)
#define B_strsep (strsep)

#endif

// B_strdup returns NULL if !str.
char *B_strdup(const char *str);

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER

#define B_va_copy(DEST,SRC) ((DEST)=(SRC))

#else//_MSC_VER

#define B_va_copy(DEST,SRC) va_copy(DEST,SRC)

#endif//_MSC_VER

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// B_snprintf, B_vsnprintf - these are sort of like the ISO ones, but not
// quite.
//
// If the output is truncated, B_snprintf and B_vsnprintf return the
// truncated length.
//
// This is marginally more useful than the ISO semantics, and easier to
// support with VC++. B_(v)asprintf will make a buffer of the right size
// automatically so there's not much need to know how big it should be
// when using (v)snprintf.

int B_snprintf(char *buf,size_t buf_size,const char *fmt,...) B_PRINTF_LIKE(3,4);
int B_vsnprintf(char *buf,size_t buf_size,const char *fmt,va_list v);

#ifdef _MSC_VER

int B_asprintf(char **buf,const char *fmt,...) B_PRINTF_LIKE(2,3);
int B_vasprintf(char **buf,const char *fmt,va_list v);

#else//_MSC_VER

#include <stdio.h>

#define B_asprintf(...) (asprintf(__VA_ARGS__))
#define B_vasprintf(...) (vasprintf(__VA_ARGS__))

#endif//_MSC_VER

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER

#define B_DEBUG_BREAK() (__debugbreak())

#elif (defined B_X86)||(defined B_X64)

#define B_DEBUG_BREAK()                  \
	do                                       \
{                                        \
	__asm__ __volatile__(                \
	"int $3\n"      \
	"nop\n");       \
}                                        \
	while(0)

#elif (defined B_ARM)

#define B_DEBUG_BREAK()                                          \
	do                                                               \
{                                                                \
	__asm__ __volatile__(                                        \
	"mov r0,%0\n"                           \
	"mov r1,%1\n"                           \
	"mov r12,%2\n"                          \
	"swi 128\n"                             \
	"nop\n"                                 \
	:                                       \
	:"r"(getpid()),"r"(SIGINT),"r"(37)      \
	:"r12","r0","r1","cc");                 \
}                                                                \
	while(0)

#else

#error B_DEBUG_BREAK - Unknown CPU.

#endif

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

typedef int (*B_AssertFn)(const char *file,int line,const char *cond,void *context);

void B_SetAssertFn(B_AssertFn assert_fn,void *context);

#if B_ASSERT_ENABLED

int B_OnAssert(const char *file,int line,const char *cond);

#define B_ASSERT(COND)								\
	B_BEGIN_MACRO                                   \
	{                                               \
		B_COND_CONSTANT(if(!(COND)))                \
		{                                           \
			if(S_OnAssert(__FILE__,__LINE__,#COND)) \
			{                                       \
				B_DEBUG_BREAK();                    \
			}                                       \
		}                                           \
	}                                               \
	B_END_MACRO

#else//S_ASSERT_ENABLED

#define B_ASSERT(COND) ((void)0)

#endif//S_ASSERT_ENABLED

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#endif//HEADER_1DC3B574BB3B4FCFB04C9FEBCD11A3E3
