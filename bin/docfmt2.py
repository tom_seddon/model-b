import sys
import xml.dom
import xml.dom.minidom
import textwrap

# Lesson learnt: DOM sucks only slightly less than SAX.
# Next time, look at XSLT (I think?)

def opening_tag_of(xml_element):
    result="<"+xml_element.nodeName
    for (key,value) in xml_element.attributes.items():
        result+=' %s="%s"'%(key,value,)
    result+=">"
    return result
    
def closing_tag_of(xml_element):
    return "</"+xml_element.nodeName+">"

def leaf_node_text(xml_node):
    """returns string, text contained in this (leaf) node. any elements are put verbatim into
    the string."""
    result=""
    
    for node in xml_node.childNodes:
        if node.nodeType==xml.dom.Node.TEXT_NODE:
            result+=node.data
        elif node.nodeType==xml.dom.Node.ELEMENT_NODE:
            result+=opening_tag_of(node)
            result+=leaf_node_text(node)
            result+=closing_tag_of(node)
        
    return result

class Node_p:
    def __init__(self,xml_node):
        self.text=leaf_node_text(xml_node).strip()
        
    def html_out(self,f):
        f.write("<p>\n")
        f.write(self.text+"\n")
        f.write("</p>\n")
        
    def get_contents_entries(self):
        return []

    def get_children(self):
        return []

class Node_sec:
    def __init__(self,xml_node):
        global node_handlers
        
        self.titles=[]
        self.title_anchors=[]
        self.children=[]
        
        for (key,value) in xml_node.attributes.items():
            if key.startswith("title"):
                self.titles.append(value)
        
        for node in xml_node.childNodes:
            if node.nodeType==xml.dom.Node.ELEMENT_NODE:
                assert node.nodeName in node_handlers.keys()
                child_node=node_handlers[node.nodeName](node)
                self.children.append(child_node)
        
        self.title_anchors=["" for title in self.titles]
        
    def get_contents_entries(self):
        return self.titles
        
    def get_children(self):
        return self.children
        
    def html_out(self,f):
        heading="<h3>"
        for i in range(len(self.titles)):
            if i>0:
                heading+="; "
            heading+=self.titles[i]
        heading+="</h3>"
        f.write(heading+"\n")
        f.write("<blockquote>\n")
        for child in self.children:
            child.html_out(f)
        f.write("</blockquote>\n")
        
class Node_generic_list:
    def __init__(self,xml_node):
        self.items=[]
        for node in xml_node.childNodes:
            if node.nodeType==xml.dom.Node.ELEMENT_NODE:
                assert node.nodeName=="item"
                self.items.append(leaf_node_text(node.childNodes[0]))

    def html_out(self,f):
        pass

    def get_contents_entries(self):
        return []

    def get_children(self):
        return []

class Node_list(Node_generic_list):
    def __init__(self,xml_node):
        Node_generic_list.__init__(self,xml_node)

class Node_numlist(Node_generic_list):
    def __init__(self,xml_node):
        Node_generic_list.__init__(self,xml_node)

node_handlers={
    "sec":Node_sec,
    "p":Node_p,
    "numlist":Node_numlist,
    "list":Node_list,
}

def fmt_to_file(input_name,output_file):
    doc=xml.dom.minidom.parse(input_name)
    #doc.writexml(sys.stdout)

    #should contain xml node
    #sec node within that (root of document)
    assert len(doc.childNodes)==1
    assert doc.childNodes[0].nodeName=="xml"
    
    root_secs=[node for node in doc.childNodes[0].childNodes if node.nodeType==xml.dom.Node.ELEMENT_NODE]
    assert len(root_secs)==1
    assert root_secs[0].nodeName=="sec"
    
    root_sec=Node_sec(root_secs[0])
    
    root_sec.html_out(output_file)
    
def main():
    if len(sys.argv)<1:
        print>>sys.stderr,"FATAL: must specify input file."
    else:
        fmt_to_file(sys.argv[1],sys.stdout)

if __name__=="__main__":
    main()

