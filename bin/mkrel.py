# notes:
# * zipping src is lame!
#imports{{{
import sys
import os
import os.path
import shutil
import time
import zipfile
import copy
import msvcrt
import docfmt

have_pywin=True
try:
    import win32file
except ImportError:
    have_pywin=False
#}}}

def MakeWriteable(fname):#{{{
    if not have_pywin:
        return
    #0=GetFileExInfoStandard
    attr=win32file.GetFileAttributesEx(fname,0)[0]
    attr&=~win32file.FILE_ATTRIBUTE_READONLY
    win32file.SetFileAttributes(fname,attr)
#}}}

def TargetDir():#{{{
    date=time.strftime("%Y-%m-%d")
    return "..\\releases\\"+date
#}}}

class Cfg:
    pass

def ActionFmt(src,dest):#{{{
    print "FMT: "+src+"->"+dest+"..."
    return docfmt.fmt(src,dest)
#}}}

def ActionUpx(src,dest):#{{{
    return ActionCopy(src,dest)
#}}}

def ActionCopy(src,dest):#{{{
    print "COPY: "+src+"->"+dest+"..."
    #shutil.copy(src,dest)
    f=file(src,"rb")
    contents=f.read()
    f.close()
    f=file(dest,"wb")
    f.write(contents)
    f.close()
    return 1
#}}}

def ActionUntab(src,dest):#{{{
    print "UNTAB: "+src+"->"+dest+"..."
    srcfile=file(src,"rt")
    lines=srcfile.readlines()
    srcfile.close()
    destfile=file(dest,"wt")
    linenum=0
    for line in lines:
        untabbed=""
        for char in line:
            if char!="\t":
                untabbed+=char
            else:
                for j in range(4-len(untabbed)%4):
                    untabbed+=" "
        destfile.write(untabbed)
        linenum+=1
    destfile.close()
    return 1
#}}}

class File:
    def __init__(self,src,dest,action):
        self.src=src
        self.dest=dest
        self.action=action

# cfgs{{{
all=Cfg()
all.dest=TargetDir()
all.files={
    "etc/changes.txt":("*",ActionUntab),
    "etc/readme.xml":("readme.txt",ActionFmt),
    "blank.ssd":("*",ActionCopy),
    "blank.dsd":("*",ActionCopy),
    "etc/default.beeb.ini":("beeb.ini",ActionCopy),
    "run/roms/b+mos.rom":("roms/*",ActionCopy),
    "run/roms/os12.rom":("roms/*",ActionCopy),
    "run/roms/basic2.rom":("roms/*",ActionCopy),
    "run/roms/acorn/dfs-2.26.rom":("roms/acorn/*",ActionCopy),
    "run/roms/acorn/adfs-1.30.rom":("roms/acorn/*",ActionCopy),
    "run/roms/opus/challenger-1.01.rom":("roms/opus/*",ActionCopy),
    "run/roms/opus/OPUS-DDOS-3.45.rom":("roms/opus/*",ActionCopy),
    "run/roms/watford/ddfs-1.54t.rom":("roms/watford/*",ActionCopy),
#    "$WXWIN/lib/wxmsw24.dll":("*",ActionCopy),
}

dx8=Cfg()
dx8.files={
    "modelb_wx/Release/modelb.exe":("*",ActionUpx),
}
dx8.dest="dx8"

dx3=Cfg()
dx3.files={
    "modelb_wx/Release DX3/modelb.exe":("*",ActionUpx),
}
dx3.dest="dx3"

debug=Cfg()
debug.files={
    "modelb_wx/Release (Debug)/modelb.exe":("modelb_debug.exe",ActionUpx),
    "etc/readme_debug.txt":("*",ActionUntab),
}
debug.dest="debug"
#}}}

#location of SS.EXE and the sourcesafe database
ssexe="c:\\program files\\vss\\win32\\ss.exe"
ssdir="z:\\vss"

def Merge(parent,child):#{{{
    result=Cfg()
    result.files=copy.copy(parent.files)
    for i in child.files.keys():
        result.files[i]=child.files[i]
    result.dest=os.path.join(parent.dest,child.dest)
    return result
#}}}

def DoInstall(config,zip_file_name):#{{{
    print "Installing to \""+config.dest+"\"..."
    dest_names={}
    zf=None
    
    for src_orig in config.files.keys():
        src=os.path.expandvars(src_orig)
        dest=config.files[src_orig][0]
        action=config.files[src_orig][1]
        if dest[-1]=="*":
            dest=os.path.join(dest[:-1],os.path.split(src)[1])
        dest_zip_name=dest
        dest=os.path.join(config.dest,dest)
        dest_path=os.path.split(dest)[0]
        if not os.path.isdir(dest_path):
            os.makedirs(dest_path)
        ret=action(src,dest)
        if not ret:
            print >>sys.stderr,"FATAL: action failed."
            sys.exit(1)
        if zf==None:
            #delayed creation so dir is made
            zf=zipfile.ZipFile(zip_file_name,"w")
        zf.write(dest,dest_zip_name,zipfile.ZIP_DEFLATED)
    zf.close()
#}}}

all_dx3=Merge(all,dx3)
all_dx8=Merge(all,dx8)
all_debug=Merge(all,debug)
target=TargetDir()
full_target=os.path.abspath(target)

def main():
    DoInstall(all_dx3,os.path.join(target,"modelb-dx3.zip"))
    DoInstall(all_dx8,os.path.join(target,"modelb-dx8.zip"))
    DoInstall(all_debug,os.path.join(target,"modelb-debug.zip"))

    os.system("attrib -r \""+os.path.join(full_target,"*.*")+"\" /s")
    
    src_target=os.path.join(full_target,"src")
    print "Target is \""+src_target+"\"..."

    #do get latest
    if not os.path.isdir(src_target):
        os.makedirs(src_target)
    oldcwd=os.getcwd()
    os.chdir(src_target)
    os.putenv("SSDIR",ssdir)
    ret=os.spawnl(os.P_WAIT,ssexe,'"'+ssexe+'"',"Get","$/beeb/src","-R","-W","-I-")
    if ret!=0:
        print >>sys.stderr,"FATAL: VSS get failed"
        sys.exit(1)
    os.chdir(oldcwd)
    
    #zip that up.    
    zf=zipfile.ZipFile(os.path.join(target,"modelb-src.zip"),"w")
    print "omomomom"
    def tmpfunc(arg,dirname,names):
        #arg[0] -> target, arg[1] -> zip file
        for name in names:
            file_name=os.path.join(dirname,name)
            MakeWriteable(file_name)
            zip_name=file_name[len(arg[0])+1:]#+1 to skip '\\'
            if os.path.isfile(file_name):
                print "src: "+file_name+" is "+zip_name
                arg[1].write(file_name,zip_name,zipfile.ZIP_DEFLATED)
    os.path.walk(src_target,tmpfunc,(src_target,zf))
    zf.close()

if __name__=="__main__":
    main()
    
# :indentSize=4:lineSeparator=\n:noTabs=true:tabSize=4:

