##int16 sresult=int8(state.cpustate.a)+int8(val)+(state.cpustate.p&1);
##if(((result.l&128)>0)^(sresult<0)) {
##state.cpustate.p|=V_MASK;
##}
###else
##//retro 1996 code from model-b: (think it works -- this was
##//what fixed cholo and os1.20 sound system.)
##//V=(~(A^(j)) & (A^ADCResult) & 0x80)>>1;
##state.cpustate.p|=(~(state.cpustate.a^val)&(state.cpustate.a^result.l)&0x80)>>1;

def signed(n):
    if n&0x80:
        return (-(n&0x7f))+128
    else:
        return n

for c in range(2):
    for m in range(256):
        for a in range(256):
            uresult=a+m+c

            #beebem
            sresult=signed(a)+signed(m)+c
            uflag=(uresult&0x80)>0
            sflag=(sresult<0)>0
            v_beebem=uflag!=sflag

            #model-b
            v_mb=(~(a^m)&(a^uresult)&0x80)>0

            if v_beebem!=v_mb:
                print "c=%d m=%d a=%d v_beebem=%d v_mb=%d"%(c,m,a,v_beebem,v_mb,)
                