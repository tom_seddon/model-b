import sys
import xml.sax
import textwrap

def print_list_entry(indent,first_line_prefix,lines):
    result_lines=[]
    for i in range(len(lines)):
        output=indent
        
        if i==0:
            output+=first_line_prefix
        else:
            output+=len(first_line_prefix)*" "

        output+=" "
        output+=lines[i]

        #print >>outf,output
        result_lines.append(output)
        
    #print>>sys.stderr,result_lines
    return result_lines

class UnorderedList:
    def print_item(self,indent,lines):
        return print_list_entry(indent,"*",lines)

class OrderedList:
    def __init__(self):
        self.num=1

    def print_item(self,indent,lines):
        result=print_list_entry(indent,str(self.num)+".",lines)
        self.num+=1
        return result

class Section:
    def __init__(self,indent_level):
        self.indent_level=indent_level
        self.titles=[]
        
    def add_title(self,title):
        self.titles.append(title)

class Handler(xml.sax.handler.ContentHandler):
    #def get_contents(self):
        #return "\n".join(self.result_lines)
    
    def __init__(self):
        xml.sax.handler.ContentHandler.__init__(self)
        self.contents=[]
        self.left_margin=0
        self.right_margin=74
        self.sec_indent=2
        self.list_handlers_stack=[]
        self.eating_initial_whitespace=True

        #self.outf=outf
        
        self.result_lines=[]
        self.sections=[]

##    def start_xml(self,attrs):
##        pass
##    def end_xml(self):
##        pass

    def start_item(self,attrs):
        self.contents.append("")
    def end_item(self):
        lines=textwrap.wrap(self.contents[-1],self.right_margin-self.left_margin-2)
        #print >>sys.stderr,lines
        self.result_lines+=self.list_handlers_stack[-1].print_item(self.indent(),lines)
        del self.contents[-1]

    def list_ending(self):
        del self.list_handlers_stack[-1]
        #print >>self.outf
        self.result_lines.append("")

    def start_numlist(self,attrs):
        self.list_handlers_stack.append(OrderedList())
    def end_numlist(self):
        self.list_ending()
    
    def start_list(self,attrs):
        self.list_handlers_stack.append(UnorderedList())
    def end_list(self):
        self.list_ending()

    def start_p(self,attrs):
        self.contents.append("")
    def end_p(self):
        l=self.contents[-1].replace("\t","").strip()
        lines=textwrap.wrap(l,self.right_margin-self.left_margin)
        
        for line in lines:
            self.result_lines.append(self.indent()+line)
        self.result_lines.append("")
        #~ for line in lines:
            #~ print >>self.outf,self.indent()+line
        #~ print >>self.outf
        del self.contents[-1]

    def indent(self):
        return self.left_margin*" "
    
    def start_sec(self,attrs):
        attrs_names_lc=[attr.lower() for attr in attrs.getNames()]
        this_section=Section(self.left_margin)
        for name in attrs.getNames():
            if name.lower().startswith("title"):
                #print >>self.outf,self.indent()+attrs.getValue(name)
                self.result_lines.append(self.indent()+attrs.getValue(name))
                this_section.add_title(attrs.getValue(name))
        self.result_lines.append("")#print >>self.outf
        self.left_margin+=self.sec_indent
        self.sections.append(this_section)
    def end_sec(self):
        self.left_margin-=self.sec_indent
        assert self.left_margin>=0
    
    def startElement(self,name,attrs):
        try:
            getattr(self,"start_"+name.lower())(attrs)
        except AttributeError:
            pass

    def endElement(self,name):
        try:
            getattr(self,"end_"+name.lower())()
        except AttributeError:
            pass

    def characters(self,content):
        if len(self.contents)>0:
            for ch in content:
                if ch=='\n':
                    self.eating_initial_whitespace=True
                elif ch.isspace():
                    if self.eating_initial_whitespace:
                        continue
                else:
                    self.eating_initial_whitespace=False
                self.contents[-1]+=ch

def indented(lines,indent):
    return [" "*indent+line for line in lines]

def fmt_to_file(input_name,output_file):
    parser=xml.sax.make_parser()
    handler=Handler()
    
    parser.setContentHandler(handler)
    parser.parse(input_name)
    
    contents=[]
    for section in handler.sections:
        contents+=[" "*section.indent_level+title for title in section.titles]
    contents=indented(contents,handler.sec_indent)
    contents=["CONTENTS",""]+contents
    contents=indented(contents,handler.sec_indent)
    
    result=handler.result_lines[:0]+contents+handler.result_lines[1:]#hmmm.
    
    for line in result:
        print>>output_file,line

def fmt(input_name,output_name):
    outf=open(output_name,"wt")
    fmt_to_file(input_name,outf)
    outf.close()
    
    return True
    
def main():
    if len(sys.argv)<1:
        print>>sys.stderr,"FATAL: must specify input file."
    else:
        print fmt_to_file(sys.argv[1],sys.stdout)#fmt_to_file(sys.argv[1],sys.stdout)

if __name__=="__main__":
    main()

