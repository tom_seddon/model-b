# vcdepsmunge.py
import sys

if len(sys.argv)!=4:
    print>>sys.stderr,'FATAL: Syntax: vcdepsmunge CPPNAME DNAME ONAME'
    sys.exit(1)

cpp_name,d_name,o_name=sys.argv[1:]

#print>>sys.stderr,len(sys.stdin.readlines())

def GetSafePathName(unsafe_path_name):
    """returns unsafe_path_name, made "safe" (not really) by stripping out drive letter."""
    unsafe_path_name=unsafe_path_name.replace('\\','/')
    if len(unsafe_path_name)>3 and unsafe_path_name[0].isalpha() and unsafe_path_name[1]==':':
        return unsafe_path_name[0].upper()+unsafe_path_name[1:]
    else:
        return unsafe_path_name

prefix='Note: including file:'
headers={}
for line in sys.stdin.readlines():
    if line.startswith(prefix):
        header=line[len(prefix):].strip()
        headers[header]=None

#~ sys.stdout.write('"'+d_name+'" "'+o_name+'":')

deps='"'+cpp_name+'"'
for header in headers.keys():
    deps+=' "'+GetSafePathName(header)+'"'
print '"'+d_name+'" "'+o_name+'" : '+deps

#~ sys.stdout.write(d_name+" "+o_name+": ")
#~ sys.stdout.write('"'+cpp_name+'"')
#~ sys.stdout.write('\n')

