import math

def Dx(n):
    return -10000/math.log(1/100.)*math.log(n/100.0)

def v(t):
    t=t*t*t
    
    if t>1:
        t=1

    if t<=math.pow(10,-10000/-2000.0):
        v=-10000
    else:
        v=-2000.*math.log10(t)

    return v

n=100
for x in range(n):
    t=x/(n-1.0)
    print "x=%d t=%f -> v=%f"%(x,t,Dx(x+1))
