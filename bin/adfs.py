class DiscImage:
    def __init__(self,contents):
        assert len(contents)%256==0
        self.contents=contents

    def sector(self,n):
        assert n*256+256<=len(self.contents)
        return [ord(c) for c in self.contents[n*256:n*256+256]]

    def get_bytes(self,sector,offset,num):
        assert offset>=0 and offset<=256-num

        r=0        
        for j in range(num):
            r|=self.byte(sector,offset+j)<<(j*8L)
        
        return r

    def byte(self,n,d):
        assert d>=0 and d<256
        return self.sector(n)[d]

    def word(self,n,d):
        return self.get_bytes(self,n,d,2)

    def three(self,n,d):
        return self.get_bytes(self,n,d,3)

    def dword(self,n,d):
        return self.get_bytes(self,n,d,4)    

f=open("Z:\\beeb\\src\\run\\8bs1.adl","rb")
img=DiscImage(f.read())
f.close()

##def get1(str,i):
##    return ord(str[i])
##
##def get2(str,i):
##    return ord(str[i])|(ord(str[i+1])<<8L)
##
##def get3(str,i):
##    return get2(str,i)|(get1(str,i+2)<<16L)
##
##def get4(str,i):
##    return get2(str,i)|(get2(str,i+2)<<16L)
##
##class FreeSpaceEntry:
##    pass
##
##class DirEntry:
##    pass
##
##class AdfsDir:
##    pass
##
###i is byte offset of start of dir
##def getdir(str,i):
##    dir=AdfsDir()
##
##    dir.seq=get1(str,i)
##    assert str[i+1:i+5]=="Hugo"
##
##    dir.entries=[]
##    for i in range(47):
##        ent=DirEntry()
##        entstart=6+i*26
##
##        ent.name=""
##        for j in range(10):
##            c=get1(str,entstart+j)&0x7f
##            if c!=0:
##                ent.name+=chr(c)
##
##        ent.r=get1(str,entstart+0)&0x80
##        ent.w=get1(str,entstart+1)&0x80
##        ent.l=get1(str,entstart+2)&0x80
##        ent.d=get1(str,entstart+3)&0x80
##
##        ent.load=get4(str,entstart+10)
##        ent.ex=get4(str,entstart+14)
##        ent.len=get4(str,entstart+18)
##        ent.start=get4(str,entstart+22)
##        seqbcd=get1(str,entstart+25)
##        ent.seq=(seqbcd&0xf)+(seqbcd<<4)*10
##
##        dir.entries.append(ent)        
##
##    return dir
##
#disc details.
nsectors=img.three(0,252)
discid=img.byte(1,251)
bootopt=img.byte(1,253)
free_map_size=get1(img,256+254)

#examine free space map.
free_map=[]
for i in range(82):
    fse=FreeSpaceEntry()

    fse.start=get3(img,i*3)
    fse.len=get3(img,256+i*3)

    #print "fse %d start=%d len=%d"%(i,fse.start,fse.len)

    free_map.append(fse)

print "option %d"%(bootopt,)
print "discid %d"%(discid,)
print "nsectors %d"%(nsectors,)
print "free_map_size %d"%(free_map_size,)
assert free_map_size%3==0

##print "free map:"
##for i in range(free_map_size/3):
##    print "    %d: start %d len %d"%(i,free_map[i].start,free_map[i].len,)
##
##root=getdir(img,512)
