#read it in.
sln=open("./mbwx.sln","rt")
slnlines=[line.strip() for line in sln.readlines()]
sln.close()

def unquoted_list(xs):
    r=[]
    for x in xs:
        x=x.strip()
        if x.startswith('"'):
            x=x[1:]
        if x.endswith('"'):
            x=x[:-1]
        r.append(x)
    return r

class Project:
    def __init__(self,name,vcproj,guid):
        self.name=name
        self.vcproj=vcproj
        self.guid=guid
        self.depends_on=[]

projects=[]

globalsection_prefix="GlobalSection("

def handle_unknown_section(line):
    global handle_normal_line
    global handler
    
    if line=="EndGlobalSection":
        handler=handle_normal_line

def handle_normal_line(line):
    global global_section_handlers
    global handler
    
    if line.startswith('Project("'):
        eq=line.find("=")
        name,vcproj,guid=unquoted_list(line[eq+1:].split(","))
        projects.append(Project(name,vcproj,guid))
    elif line.startswith(globalsection_prefix):
        type=line[len(globalsection_prefix):]
        type=type[:type.find(")")]
        if type not in global_section_handlers:
            print "(no handler for GlobalSection type %s)"%(type,)
            handler=handle_unknown_section
        else:
            handler=global_section_handlers[type]

def handle_project_dependencies(line):
    pass
    
def handle_project_configuration(line):
    pass

global_section_handlers={
    "ProjectDependencies":handle_project_dependencies,
    "ProjectConfiguration":handle_project_configuration,
}

handler=handle_normal_line

for line in slnlines:
    handler(line)
