typedef void (*HostGfx_ConvertFn)($params$);
typedef void (*HostGfx_RefreshConvertTableFn)(HostGfxState *state);

#pragma pack(push,1)
struct threebytes_t {
	unsigned char b[3];
};
#pragma pack(pop)

typedef struct threebytes_t threebytes_t;
//h
void HostGfx_Convert$pm$To$nb$($params$);
void HostGfx_RefreshConvertTable$pm$To$nb$(HostGfxState *state);
//c
//
//for rgb->yuv conversion, see
//http://astronomy.swin.edu.au/~pbourke/colour/convert/
void HostGfx_RefreshConvertTable$pm$To$nb$(HostGfxState *state) {
	DWORD *masks[3];
	int i,j;
	$convertlookuptype$ *entry;

	masks[0]=&state->primary_desc.ddpfPixelFormat.dwRBitMask;
	masks[1]=&state->primary_desc.ddpfPixelFormat.dwGBitMask;
	masks[2]=&state->primary_desc.ddpfPixelFormat.dwBBitMask;

	memset(state->convert_table,0,sizeof state->convert_table);

	for(i=0;i<8;++i) {
		int index;

		index=0;
		for(j=0;j<3;++j) {
			if(i&(1<<j)) {
				index|=state->best_fmt.elems[j].mask;
			}
		}

		index*=sizeof($convertlookuptype$);//nonpremultonly

		entry=($convertlookuptype$ *)((char *)state->convert_table+index);

		for(j=0;j<3;++j) {
			if(i&(1<<j)) {
				*entry|=*masks[j];
			}
		}
	}
}

void HostGfx_Convert$pm$To$nb$($params$) {
	int num_lines;
	int y,i,x;
	const unsigned char *src;
	char *dest;

	(void)present_hints;

	dest=(char *)dest_bitmap+y_first*dest_pitch;
	src=src_bitmap;

	num_lines=src_height*(line_height+gap_height);
	for(y=0;y<src_height;++y) {
		for(i=0;i<line_height;++i) {
			for(x=0;x<src_width;++x) {
				(($desttype$ *)dest)[x]=*($desttype$ *)(($convertlookuptype$ *)state->convert_table+src[x]);
			}
// 			for(x=0;x<src_width;x+=8) {
// 				(($desttype$ *)dest)[x+0]=*($desttype$ *)(($convertlookuptype$ *)state->convert_table+src[x+0]);
// 				(($desttype$ *)dest)[x+1]=*($desttype$ *)(($convertlookuptype$ *)state->convert_table+src[x+1]);
// 				(($desttype$ *)dest)[x+2]=*($desttype$ *)(($convertlookuptype$ *)state->convert_table+src[x+2]);
// 				(($desttype$ *)dest)[x+3]=*($desttype$ *)(($convertlookuptype$ *)state->convert_table+src[x+3]);
// 				(($desttype$ *)dest)[x+4]=*($desttype$ *)(($convertlookuptype$ *)state->convert_table+src[x+4]);
// 				(($desttype$ *)dest)[x+5]=*($desttype$ *)(($convertlookuptype$ *)state->convert_table+src[x+5]);
// 				(($desttype$ *)dest)[x+6]=*($desttype$ *)(($convertlookuptype$ *)state->convert_table+src[x+6]);
// 				(($desttype$ *)dest)[x+7]=*($desttype$ *)(($convertlookuptype$ *)state->convert_table+src[x+7]);
// 			}

			for(;x<src_width;++x) {
				(($desttype$ *)dest)[x]=*($desttype$ *)(($convertlookuptype$ *)state->convert_table+src[x]);
			}

			dest+=dest_pitch;
		}
		dest+=gap_height*dest_pitch;
		src+=src_pitch;
	}
}

