struct PluginDescription;

struct PluginMmioFn {
	const char *description;
	word base;
	word len;
	byte (*read_fn)(PluginDescription *descr,word addr);
	void (*write_fn)(PluginDescription *descr,word addr,byte val);
};

enum PluginFlags {
	//this plugin can generate IRQs and NMIs. (A separate NMI flags bit
	//is created for each plugin if these flags are set.)
	PLUGIN_IRQS=1,
	PLUGIN_NMIS=2,
	
	//if set, this plugin produces display. display plugins have a window
	//created for them.
	PLUGIN_DISPLAY=4,

	//if set, this plugin produces sound. its generate_sound_fn function will
	//be called whenever the emulator needs to produce sound.
	PLUGIN_SOUND=8,
};

enum PluginOptionType {
	PLUGIN_OPTION_INT,
	PLUGIN_OPTION_KEYVAL,
	PLUGIN_OPTION_FILENAME,
	PLUGIN_OPTION_META,
};

struct PluginOptionInt {
	int value;
	int mini,maxi;
};

struct PluginOptionKeyValPair {
	const char *key;
	int val;
};

struct PluginOptionKeyVal {
	int value;
	unsigned num_pairs;
	const PluginOptionKeyValPair *pairs;
};

struct PluginOptionFilename {
	char *value;
};

//Contains more options. Used for hierarchies of options. I imagine emulator
//will use this for creating StaticBox objects in the UI, subsections in an
//INI file, etc.
struct PluginOptionMeta {
	unsigned num_options;
	PluginOption *options;
};

struct PluginOption {
	PluginOptionType type;
	const char *key;
	enum {
		PluginOptionInt i;
		PluginOptionKeyVal kv;
		PluginOptionFilename f;
		PluginOptionMeta m;
	};
};

typedef unsigned PluginDeviceId;

struct PluginEmulatorData;

struct PluginDescription {
	//Don't change these. They're set by the emulator.
	PluginEmulatorData *data;

	//These details are filled in by the plugin.
	const char *name;
	const char *description;

	unsigned num_mmio_fns;
	const PluginMmioFn *mmio_fns;

	unsigned flags;
};

//////////////////////////////////////////////////////////////////////////
//exported by plugin.

//fill in the given plugin description. Store a pointer to it, and use
//that as the PluginDescription * parameter for the emulator calls.
void Exported_PluginGetDescription(PluginDescription *descr);

//Retrieves a list of options. These can be used by the emulator to generate UI
//and/or items for saving and reloading the config.
const PluginOptionMeta *Exported_PluginOptions();

//called when the bbc is hard reset. 
void Exported_PluginReset();

//Called to stop the plugin. Destroy any resources attached to the given description,
//but not the description itself.
void Exported_PluginStop();

//Called to update the device. Set new next-stop point in cycles during this call. If
//no next-stop point is set, the device goes to sleep, and update_fn is not called any
//more, until another is set.
void Exported_PluginUpdate();

//Reallocates memory. This is used for creating space for filenames. This ensures
//the plugin's heap can be used rather than than of the emulator. Should work
//just as realloc: if !base, malloc; if new_size==0, free; else resize.
void *Exported_PluginRealloc(void *p,size_t size);

//The emulator calls this to generate sound. should only be set if plugin flags include
//PLUGIN_SOUND.
//
//this is called whenever more sound is needed. for each i in 0..num_buffers,
//fill in buffer_sizes[i] bytes at buffers[i].
void Exported_PluginGenerateSound(int hz,int bits,int stereo,void **buffers,unsigned *buffer_sizes,
	unsigned num_buffers);

//called to generate debug rendering on top of the given display.
//3bpp. pitch is w.
void Exported_PluginDebugRender(t65::byte *buffer,int w,int h);

//////////////////////////////////////////////////////////////////////////
//called by plugin

//
// * cpu control *
//

//retrieves current 2MHz cycle count.
int Plugin2MHzCycleCount();

//Sets next stop point for this plugin.
void PluginSetNextStop(PluginDescription *descr,int cycles);

//Sets or resets the CPU's IRQ or NMI line as appropriate. IRQs and NMIs occur
//on leading edge. can be used only if the given plugin has the relevant interrupt
//flag set.
void PluginSetIrq(PluginDescription *descr,bool level);
void PluginSetNmi(PluginDescription *descr,bool level);

//
// * keyboard and joystick input *
//

//returns an array of 256 bytes indicating which keys are currently pressed.
//index the array by BBC key code, including those registered by plugins.
//
//the state includes key presses caused by joykeys.
const byte *PluginGetKeysPressed();

//gets state for the given joystick. returns !0 and fills in *x, *y and *button,
//or returns 0 indicating that joystick isn't plugged in.
int PluginGetJoystickState(int index,float *x,float *y,int *button);

//
// * disc *
//

//read and write disc byte. returns 0 on failure, !0 on success.
//read fills in *result on success.
int PluginReadDiscByte(int fdc_drive,int fdc_side,int fdc_track,int fdc_sector,
					   int fdc_offset,DiscDensity density,t65::byte *result);
int PluginWriteDiscByte(int fdc_drive,int fdc_side,int fdc_track,int fdc_sector,
					   int fdc_offset,DiscDensity density,t65::byte value);

//returns true or false: whether the given disc address is valid.
int PluginIsValidDiscAddress(int fdc_drive,int fdc_side,int fdc_track,
						   int fdc_sector,DiscDensity density);

//
// * special keys *
//
//special keys - a plugin may register a special key. this is then added
//to the list of known keys, and you can configure it in the keyboard dialog.
//you can query its state by using its code as an index into the array filled
//by PluginGetKeysPressed.

//registers a new special key. returns key value (top bit set). desired_key_name
//is just what it says. the given key name may be different. use the next function
//to find out what it is.
t65::byte PluginRegisterSpecialKey(const char *desired_key_name);

//returns name for given code. key_code can be bbc code or special key.
const char *PluginKeyName(t65::byte key_code);

//
// * graphics *
//

//present new 3bpp graphics data. does everything necessary. if allow_debug_render,
//also calls any plugins' debug_render functions. (it's intended that only the
//video emulation plugin will do this, but whatever.) can only be called if
//PLUGIN_DISPLAY flag is set.
void PluginPresent(PluginDescription *descr,t65::byte *data,int w,int h,bool allow_debug_render);

//returns %FFFFF000, the value to be ORed with each byte to be presented with
//Present. Each byte you present must be %FFFFFFBGR. This is for efficient support
//of palettized displays for which the first 8 entries are not usually accessible.
//(For example, DirectX full screen.)
//
//You can OR this value in at the end of generating the display, or more sensibly
//maintain it and check it for changes as you go along. It won't change that often.
t65::byte PluginPresentationFixedBits();

/*
Thoughts

I think realloc_fn will prevent any problems with plugin and emulator using
different heaps. This was needed for the filename option, as the memory may
need resizing. I don't fancy limiting plugins in this way, since you aren't
restricted by MAX_PATH any more on Windows at least.

You can't add video or sound system with this plugin. I don't consider this
a great problem.

There's no support for multiple beebs in the same process. This is by design.
I don't see the point of supporting this. If several beebs need to be run at
once, better to work out some network-transparent IPC mechanism. It's more
useful to run multiple beebs networked than on the same machine. And if you're
going to do that, one beeb per process is not a problem.

*/
