MODELB DEBUG

	This version of the EXE has some simple debugging features. I've
	put these features in as I've had a need for them, and they're often
	rough and ready and they might interact in strange ways!
	
	Still, you might find them useful.
    
    You'll need DirectX8. It's pretty much like the normal version,
    except for the following.
    
INCOMPLETE FEATURES (Optional)

	There may be some incomplete or unimplemented features.
    
DEBUG MENU

	Start Disassembly
  	Stop Disassembly
    Reboot with disassembly

		produces a running disassembly of each instruction executed.
        Select "Start" or "Reboot with" to start the emulator saving
        data about each instruction executed. Then select "Stop" to save
        the data to a text file. (Cancel the file selector if you decide
        not to bother.)
        
        The text format is like this:

20644193  (@20644224 )E3D : BIT   &FE60      2C 60 FE -> A:00 X:00 Y:13 S:E8 P:------Z-

		It should be pretty clear. The first value is the current cycle
        count. The second is the 'next-stop' value, which you can
        ignore. (In case you're wondering: it is the soonest point, in
        cycles, when the CPU will definitely have to stop because an IRQ
        is coming or some hardware needs polling.)
        
		Then is program counter, instruction, dump of instruction bytes,
        and finally the registers after the instruction has run.
        
        There are instruction suffixes too:
        
        +		65C12 only
        *		Undocumented/unassigned instruction
        
        IRQs are also indicated, with a brief breakdown of the states of
        the VIAs and the 1770.
        
        Saving this data makes the emulator run a bit slower. It also doesn't
        take long to produce extremely large files that are a bit painful to
        view. I've had some success using SciTE to examine them, though it can
        take a bit to start up, and moving around the doc can be sluggish.
        
        For really large files, try ListXP (www.listxp.com) -- this is a bit
        better, but you'll need to make a cup of tea (or five) while it scans
        the file.
        
	Disassemble RAM
    
    	disassembles all of RAM and saves it to a file.

	Save Challenger RAM
    
    	saves the 256K or 512K RAM in the challenger to a file.
        
    System VIA debug panel
    User VIA debug panel
    Video debug panel
    ADC debug panel
    
    	debug panels for particular aspects of the beeb. COnstantly
        updated. Video one not done yet!
        
    Show all
    
    	Forces a very strange palette (and a few other things) so you
        can see what's _really_ on the screen! Strangely entertaining,
        this is!
        
    Log a few frames
    Log a few frames (w/ disasm)
    
    	Logs records for a few frames about the contents of each
        scanline, and various other events useful when debugging the
        video system (VIA T1 timeouts mainly).
        
        The "w/ disasm" one also switches on running disassembly, if it
        isn't already.
        
        Regardless, when logging is done, if running disassembly
        was active (be it through "Start disassembly" or "w/disasm"
        option) the output is appended to the specified file. No progres
        indicator will appear for this, so the emulator might appear to
        hang if there's a lot to be saved.
        
	Show sys (R)/usr (G) T1 timeouts
    
    	Show system and user VIA T1 timeouts on the screen. The red bar
        indicates sytsem VIA. The green bar indicates user VIA. The line
        will be yellow should they coincide.
        
        If you have this enabled, the scanline log will show T1 timeout
        messages from the video system as well as from the VIA itself.
        
MULTITASKING

	It plays nicer with Windows so it's not a pain to debug. You'll find
    Windows has use of F10, Alt, windows keys, etc., and the keyboard
    response might not be so immediate.
    
CONSOLE

	The debug version is a Win32 console program. The console window
    displays any debugging information that I might have left in.

BBC1770.TXT (Optional)

	If I forgot to undefine it, you'll get a file called 'bbc1770.txt'
    listing what the 1770 has been doing. 

BEEBICE

	BeebIce is the ultimate BBC debugger. At the moment, it doesn't do
	much, but eventually it will be very, very good.