# vim:foldmethod=marker
STD_CFLAGS:=
LDFLAGS:=-lstdc++

#########################################################################
#
# Configuration section
#

#
# model-b
#

DEBUG?=1
OPTIMIZED?=0

#
# Compiler
#

# gcc 3.4
CC:=gcc-3.4
STD_CFLAGS+=-DPORT_TEMPLATE=template
PCH:=yes

# gcc 3.3
#STD_CFLAGS+=-DPORT_TEMPLATE

#
# wxWidgets
#

# Point this at the wx-config for the build of wxWidgets you want to use
ifeq ($(DEBUG),0)
WX_CONFIG:=~/wxWidgets-2.6.2/buildgtk34/wx-config
else
WX_CONFIG:=~/wxWidgets-2.6.2/buildgtkd34/wx-config
endif

#########################################################################
#
#
#

#
# Make build name
#

ifneq ($(DEBUG),0)
BUILD_DEBUG:=debug-
endif

ifeq ($(OPTIMIZED),0)
BUILD_OPTIMIZED:=unopt
else
BUILD_OPTIMIZED:=opt
endif

BUILD_NAME:=$(BUILD_DEBUG)$(BUILD_OPTIMIZED)-$(shell $(WX_CONFIG) --basename)-$(CC)
STD_CFLAGS+=-g -Wall -DPORT_X86 -DPORT_DEFINE__SNPRINTF -DPORT_HAS_STDINT_H -DPORT_STRICMP=strcasecmp -fmessage-length=0 -DPORT__STRTOI64=strtoll -DPORT_USE_WX_KEYBOARD -DPORT_INT64_FMT=\"ll\" -DPORT_X

ifeq ($(OPTIMIZED),0)
STD_CFLAGS+=-O0
else
STD_CFLAGS+=-O2
endif

ifneq ($(DEBUG),0)
STD_CFLAGS+=-DbbcDEBUG_ENABLE
endif

LDFLAGS+=-lXv
LDFLAGS+=$(shell $(WX_CONFIG) --libs)

WX_CFLAGS:=$(shell $(WX_CONFIG) --cxxflags) $(shell pkg-config gdk-2.0 --cflags) $(shell pkg-config atk --cflags)

LDFLAGS+=$(shell pkg-config gdk-2.0 --libs) $(shell pkg-config atk --libs)

ALL_OBJS:=
ALL_DEPS:=
CTAGS_FILES:=

INTERM_DIR:=$(BUILD_NAME)
BIN_NAME:=./model-b-$(BUILD_NAME)

define template
$(1)_INTDIR:=$$($(1)_SRCDIR)/output/$(INTERM_DIR)

$(1)_FULL_CFLAGS=$$($(1)_CFLAGS) -MD -MT "$$($(1)_INTDIR)/$$*.d"

$(1)_MKDIR=mkdir -p "$$($(1)_INTDIR)"

$$($(1)_INTDIR)/%.s:$$($(1)_SRCDIR)/%.$(2)
	$$($(1)_MKDIR)
	$(CC) $$($(1)_CFLAGS) -dAp -o "$$@" -S "$$<"

$$($(1)_INTDIR)/%.d $$($(1)_INTDIR)/%.o:$$($(1)_SRCDIR)/%.$(2)
	$$($(1)_MKDIR)
	$(CC) $$($(1)_FULL_CFLAGS) -o "$$($(1)_INTDIR)/$$*.o" -c "$$<"

$(1)_OBJS:=$$(patsubst %,$$($(1)_INTDIR)/%,$$($(1)_NAMES:.$(2)=.o))
$(1)_ASMS:=$$($(1)_OBJS:.o=.s)
$(1)_DEPS:=$$($(1)_INTDIR)/*.d

.PHONY: $(1)_clean
$(1)_clean:
	$(RM) $$($(1)_OBJS) $$($(1)_DEPS)

.PHONY: $(1)_cleanall
$(1)_cleanall: $(1)_clean
	$(RM) -R $$($(1)_SRCDIR)/output

ALL_OBJS:=$$(ALL_OBJS) $$($(1)_OBJS)
ALL_DEPS:=$$(ALL_DEPS) $$($(1)_DEPS)
CTAGS_FILES:=$$(CTAGS_FILES) $$(patsubst %,$$($(1)_SRCDIR)/%,$$($(1)_NAMES)) $$(wildcard $$($(1)_SRCDIR)/*.h) $$(wildcard $$($(1)_SRCDIR)/*.inl)

endef

#
# default target
#

default_target:$(BIN_NAME) tags

#
# HostShared
#

HS_CFLAGS:=$(STD_CFLAGS)
HS_SRCDIR:=./HostShared
HS_NAMES:=\
	HostGfxShared.c

#
# HostLinux
#

HL_CFLAGS:=$(STD_CFLAGS) -IHostShared
HL_SRCDIR:=./HostLinux
HL_NAMES:=\
	Host.c\
	HostGfx.c\
	HostSnd.c\
	HostInp.c\
	HostTmr.c

#
# modelb_common
#
MBC_CFLAGS:=$(STD_CFLAGS) -IHostShared -Itom6502
MBC_SRCDIR:=./modelb_common
ifdef PCH
MBC_PCH:=pch.h
endif
# MBC_NAMES{{{
MBC_NAMES:=\
	pch.cpp\
	bbc1770DriveControl.cpp\
	bbcFdd.cpp\
	bbcAcia.cpp\
	bbcSaveState.cpp\
	bbc65C12.cpp\
	bbcDiscInterface.cpp\
	bbc1770Opus.cpp\
	bbcVideo.cpp\
	bbc1770OpusChallenger.cpp\
	bbcModelMaster128Config.cpp\
	bbc1770.cpp\
	bbcComputer.cpp\
	bbcRomSlot.cpp\
	bbcCmos.cpp\
	bbcModelB.cpp\
	bbc1770Interface.cpp\
	bbcSY6502A.cpp\
	bbc1770Master128.cpp\
	bbcSystemVIA.cpp\
	bbcAdc.cpp\
	bbcModelMaster128.cpp\
	bbcModelBPLus.cpp\
	bbcDebugPanel.cpp\
	bbcKeyboardMatrix.cpp\
	bbc1770Watford.cpp\
	bbcModelBWatford.cpp\
	bbcCommon.cpp\
	bbcSound.cpp\
	bbcModel.cpp\
	bbcBeebIce.cpp\
	bbcKeys.cpp\
	bbcUserVIA.cpp\
	bbcUef.cpp\
	bbcVIA.cpp\
	bbcEventRecord.cpp\
	bbc1770Acorn.cpp\
	bbcProfiler.cpp\
	bbcTeletextFont.cpp
#}}}
#
# modelb_wx
#
MBWX_CFLAGS:=$(STD_CFLAGS) -IHostShared -IHostLinux -Itom6502 -Imodelb_common $(WX_CFLAGS)
MBWX_SRCDIR:=./modelb_wx
ifdef PCH
MBWX_PCH:=pch.h
endif
# MBWX_NAMES{{{
MBWX_NAMES:=\
	mbKeyMap.cpp\
	mbTestMode.cpp\
	pch.cpp\
	mbRomsConfigDialog.cpp\
	mbDiscFormat.cpp\
	mbSoundConfigDialog.cpp\
	mbVideoConfigDialog.cpp\
	mbProfilesDialog.cpp\
	mbDiscInterface.cpp\
	main.cpp\
	mbDebugPanel.cpp\
	mbStatusBar.cpp\
	mbKeyboardConfigDialog.cpp\
	mbQuickstart.cpp\
	mbApp.cpp\
	twDebug.cpp\
	twArgs.cpp\
	mbMisc.cpp\
	mbJoysticksConfigDialog.cpp\
	mbJoyKeys.cpp\
	mbMainFrame.cpp\
	mbModel.cpp\
	mbConfigFile.cpp\
	mbConfig.cpp\
	mbIceFrame.cpp\
	mbKeys.cpp\
	mbHardwareDialog.cpp
#}}}

#
# expand templates
#

ifdef VBMK
$(warning $(call template,HS,c))
$(warning $(call template,HL,c))
$(warning $(call template,MBC,cpp))
$(warning $(call template,MBWX,cpp))
endif

$(eval $(call template,HS,c))
$(eval $(call template,HL,c))
$(eval $(call template,MBC,cpp))
$(eval $(call template,MBWX,cpp))

#
# tags
#
tags: $(CTAGS_FILES)
	@echo "ctags..."
	@ctags --exclude="*/vms_x_fix.h" --c-kinds=+p --c++-kinds=+p $(shell find "/usr/X11R6/include/" -name "*.h") $(shell find "/usr/local/include" -name "*.h") $(shell find "/usr/include/gtk-2.0" -name "*.h") $(CTAGS_FILES)
	@echo "   ctags done."

#
# modelb_compile
#
$(BIN_NAME): $(ALL_OBJS)
	$(CC) $(LDFLAGS) -o $(BIN_NAME) $(ALL_OBJS) 

.PHONY: clean
clean:HS_clean HL_clean MBC_clean MBWX_clean

.PHONY:cleanall
cleanall:HS_cleanall HL_cleanall MBC_cleanall MBWX_cleanall

ifneq ($(MAKECMDGOALS),clean)
ifneq ($(MAKECMDGOALS),cleanall)
-include $(ALL_DEPS)
endif
endif

